//
//  RequestManager.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/27.
//

import UIKit
import Alamofire

private let NetworlAPIBaseURL = BASE_URL
typealias NetworkRequestResult = Result<Data,Error>
typealias NetworkRequestCompletion = (NetworkRequestResult) -> Void

class RequestManager {
    static let shared = RequestManager()
    var commonHeaders: HTTPHeaders {
        [
            "Content-type" : "application/json",
            "Authorization" : LLUserDefaultsManager.shared.readLoginInfosTK() ?? ""
        ]
    }
    
    private init(){}
//    @discardableResult
//    func fetchingGetData(
//            path: String?,
//            params: Parameters?,
//            completionCallBack: @escaping NetworkRequestCompletion){
//                var url:NSURL = NSURL(string: BASE_URL + path!)!
//                var request:NSMutableURLRequest = NSMutableURLRequest(URL: url as URL)
//                request.httpMethod = "POST"
//                request.httpBody = params
//                request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
//                request.setValue("application/json", forHTTPHeaderField: "Accept")
//
//               var reponseError: NSError?
//               var response: NSURLResponse?
//
//               var urlData: NSData? = NSURLConnection.sendSynchronousRequest(request, returningResponse:&response, error:&reponseError)
//
//                   if ( urlData != nil ) {
////                       let res = response as NSHTTPURLResponse!;
//
////                       NSLog("Response code: %ld", res.statusCode);
//
//                       if (res.statusCode >= 200 && res.statusCode < 300)
//                       {
////                           var responseData:NSString  = NSString(data:urlData!, encoding:NSUTF8StringEncoding)!
////
////                           NSLog("Response ==> %@", responseData);
////
////                           var error: NSError?
////
////                           let jsonData:NSDictionary = NSJSONSerialization.JSONObjectWithData(urlData!, options:NSJSONReadingOptions.MutableContainers , error: &error) as NSDictionary
////
////                           let success:NSInteger = jsonData.valueForKey("error") as NSInteger
////
////                           NSLog("Success: %ld", success);
////
////                           if(success == 0)
////                           {
////                               NSLog("Login SUCCESS");
////
////                               self.dataArr = jsonData.valueForKey("data") as NSMutableArray
////                               self.table.reloadData()
////
////                           } else {
////
////                               NSLog("Login failed1");
////                               ZAActivityBar.showErrorWithStatus("error", forAction: "Action2")
////                           }
//
//                       } else {
//                           NSLog("Login failed2")
//                       }
//                   } else {
//                       NSLog("Login failed3")
//                   }
//               }
//  
//        }
    
    
    @discardableResult
    func fetchingGet(
            path: String,
            params: Parameters?,
            completionCallBack: @escaping NetworkRequestCompletion)
        -> DataRequest {
            Alamofire.AF.request(NetworlAPIBaseURL + path,
                       parameters: params,
                       headers: commonHeaders,
                       requestModifier: {$0.timeoutInterval = 3})
                .responseData { response in
                    switch response.result {
                    case let .success(data):
                        completionCallBack(.success(data))
                        break
                    case let .failure(error):
                        completionCallBack(self.handleError(error))
                        break
                    }
                }
        }
    
    @discardableResult
    func fetchingPost(
        path: String,
        params: Parameters?,
        completionCallBack: @escaping NetworkRequestCompletion)-> DataRequest {
            Alamofire.AF.request(NetworlAPIBaseURL + path, method: .post,parameters: params,encoding: JSONEncoding.prettyPrinted,headers: commonHeaders,requestModifier: {$0.timeoutInterval = 3})
                .responseData { response in
                    switch response.result {
                    case let .success(data):
                        completionCallBack(.success(data))
                        break
                    case let .failure(error):
                        completionCallBack(self.handleError(error))
                        break
                    }
                }
        }
    
    private func handleError(_ error: AFError) -> NetworkRequestResult {
        if let underlyingError = error.underlyingError {
            let nserror = underlyingError as NSError
            let code = nserror.code
            if  code == NSURLErrorNotConnectedToInternet ||
                code == NSURLErrorTimedOut ||
                code == NSURLErrorInternationalRoamingOff ||
                code == NSURLErrorDataNotAllowed ||
                code == NSURLErrorCannotFindHost ||
                code == NSURLErrorCannotConnectToHost ||
                code == NSURLErrorNetworkConnectionLost {
                var userInfo = nserror.userInfo
                userInfo[NSLocalizedDescriptionKey] = "网络连接有问题喔～"
                let currentError = NSError(domain: nserror.domain, code: code, userInfo: userInfo)
                return .failure(currentError)
            }
        }
        return .failure(error)
    }
}
