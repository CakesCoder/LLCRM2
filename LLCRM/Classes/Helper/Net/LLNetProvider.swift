//
//  LLNetProvider.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/15.
//

import UIKit
import Moya

let LLNetProvider = MoyaProvider<LLNet>()

public enum LLNet {
    case login([String: Any])
    case phoneCode([String: Any])
    case userInfos([String:Any])
    case lineMan([String:Any])
    case addLineMan([String:Any])
    case clientDetail([String:Any])
    case personalSelling([String:Any])
    case homeCommercial([String:Any])
    case homeSchedule([String:Any])
    case homeClue([String:Any])
    case addClient([String:Any])
    case getCompanyInfos([String:Any])
    case getPersonBusiness([String:Any])
    case getPricsList([String:Any])
    case getBusinessLineMan([String:Any])
    case priceDetail([String:Any])
    case commitPrice([String:Any])
    case marketData([String:Any])
    case getBusinessList([String:Any])
    case addBusiness([String:Any])
    case deviceSummary([String:Any])
    case getBusinessDetail([String:Any])
    case deviceSelectionSave([String:Any])
    case priceList([String:Any])
    case priceContrast([String:Any])
    case clientComplaintProductList([String:Any])
    case clientComplaintProductDetail([String:Any])
    case addComplaintProduct([String:Any])
    case getAllVisitInfo([String:Any])
    case getAllClientIdName([String:Any])
    case getPriceProductList([String:Any])
    case getAllContactByClientId([String:Any])
    case getNicheClientIdName([String:Any])
    case offerMaterialList([String:Any])
    case sampleDel([String:Any])
    case sampleAdd([String:Any])
    case clientDevelopmentDel([String:Any])
    case addClientSelected([String:Any])
    case clientSelectConfig([String:Any])
    case addClientDepartMent([String:Any])
    case addClientArchives([String:Any])
    case addClientPricePerson([String:Any])
    case addClientBussinessNumber([String:Any])
    case addClientBussinessClientName([String:Any])
    case addClientBussinessNature([String:Any])
    case addClientBussinessPhase([String:Any])
    case addClientBussinessFlower([String:Any])
    case addClientBussinessCommitStatus([String:Any])
    case addClientSellLeads([String:Any])
    case addClientSampleTechnology([String:Any])
    case addClientSamleCode([String:Any])
    case addClientCertificationAsk([String:Any])
    case addClientVisitMyClient([String:Any])
    case addVisitPerson([String:Any])
    case getAddClientInfos([String:Any])
    case getAddClinetInfosDetail([String:Any])
}

extension LLNet: TargetType{
    public var baseURL: URL {
        return URL(string: BASE_URL)!
    }
    
    public var path: String {
        switch self {
        case .addVisitPerson(_):
            return "/api/crm/clientDevelopment/addVisitInfo"
        case .login(_):
            return "/api/admin/login"
        case .phoneCode(_):
            return "/api/admin/user/codes/{phone}"
        case .userInfos(_):
            return "/api/admin/user/info"
        case .lineMan(_):
            return "/api/crm/clientDevelopment/getContactsInfo"
        case .addLineMan(_):
            return "/api/crm/clientDevelopment/addClientClueContact"
        case .clientDetail(_):
            return "/api/crm/clientDevelopment/getAllClientClueInfo"
        case .personalSelling(_):
            return "/api/crm/saleData/getPersonSaleStatistics"
        case .homeCommercial(_):
            return "/api/crm/clientDevelopmentCommercial/getCommercialFollow"
        case .homeSchedule(_):
            return "/api/crm/local/check/plan/detail"
        case .homeClue(_):
            return "/api/crm/clientDevelopment/getClientClueById"
        case .addClient(_):
            return "/api/crm/clientDevelopment/addClientClue"
        case .getCompanyInfos(_):
            return "/api/crm/clientDevelopment/getClientClueById"
        case .getPersonBusiness(_):
            return "/api/crm/clientDevelopmentCommercial/getCommercialInfoByClientId"
        case .getPricsList(_):
            return "/api/crm/quotationInfo/list"
        case .getBusinessLineMan(_):
            return "clientDevelopmentCommercial/getCommercialInfoById"
        case .priceDetail(_):
            return "/api/crm/quotationInfo/getQuotationInfo"
        case .commitPrice(_):
            return "/api/crm/quotationInfo/saveQuotationInfo"
        case .marketData(_):
            return "/api/crm/saleData/getSaleData"
        case .getBusinessList(_):
            return "/api/crm/clientDevelopmentCommercial/findCommercialInfoByName"
        case .addBusiness(_):
            return "/api/crm/clientDevelopmentCommercial/addCommercialInfo"
        case .deviceSummary(_):
            return "/api/crm/deviceSelection/deviceSummary"
        case .getBusinessDetail(_):
            return "/api/crm/clientDevelopmentCommercial/getCommercialInfoById"
        case .deviceSelectionSave(_):
            return "/api/crm/deviceSelection/save"
        case .priceList(_):
            return "/api/crm/offer/manage/cus/list"
        case .priceContrast(_):
            return "/api/crm/offer/manage/price/contrast"
        case .clientComplaintProductList(_):
            return "/api/crm/clientComplaintProduct/list"
        case .clientComplaintProductDetail(_):
            return "/api/crm/clientComplaint/getComplaintById"
        case .addComplaintProduct(_):
            return "/api/crm/clientComplaintProduct/addComplaintProduct"
        case .getAllVisitInfo(_):
            return "/api/crm/clientDevelopment/getAllVisitInfo"
        case .getAllClientIdName(_):
            return "/api/crm/clientDevelopmentCommercial/getAllClientIdName"
        case .getPriceProductList(_):
            return "/api/crm/product/manage/partNumber/list"
        case .getAllContactByClientId(_):
            return "/api/crm/clientDevelopment/getAllContactByClientId"
        case .getNicheClientIdName(_):
            return "/api/crm/clientDevelopment/getNicheClientIdName"
        case .offerMaterialList(_):
            return "/api/crm/offer/manage/offerMaterialList"
        case .sampleDel(_):
            return "/api/crm/device/sample/handle/del"
        case .sampleAdd(_):
            return "/api/crm/device/sample/handle/add"
        case .clientDevelopmentDel(_):
            return "/api/crm/clientDevelopment/delete"
        case .addClientSelected(_):
            return "/api/crm/clientDevelopmentCommercial/getAllUserByCompanyId"
        case .clientSelectConfig(_):
            return "/api/crm/clientSelectConfig/list"
        case .addClientDepartMent(_):
            return "/api/admin/departments/listTree"
        case .addClientArchives(_):
            return "/api/crm/clientArchives/list"
        case .addClientPricePerson(_):
            return "/api/admin/user/getUserList"
        case .addClientBussinessNumber(_):
            return "/api/admin/number/getNumber"
        case .addClientBussinessClientName(_):
            return "/api/crm/clientDevelopment/getAllClientIdName"
        case .addClientBussinessNature(_):
            return "/api/crm/finished/product/list"
        case .addClientBussinessPhase(_):
            return "/api/crm/clientDevelopmentCommercial/getCommercialStage"
        case .addClientBussinessFlower(_):
            return "/api/crm/clientDevelopmentCommercial/getAllUserByCompanyId"
        case .addClientBussinessCommitStatus(_):
            return "/api/crm/clientSelectConfig/list"
        case .addClientSellLeads(_):
            return "/api/admin/enterprise/base/message/detail"
        case .addClientSampleTechnology(_):
            return "/api/crm/finished/product/specs/list/80"
        case .addClientSamleCode(_):
            return "/api/crm/product/manage/partNumber/list"
        case .addClientCertificationAsk(_):
            return "/api/crm/certificationAskConfig/list"
        case .addClientVisitMyClient(_):
            return "/api/admin/user/getUserList"
        case .getAddClientInfos(_):
            return "api/admin/enterprise/base/message/base"
        case .getAddClinetInfosDetail(_):
            return "api/admin/enterprise/base/message/detail"
        }
    }
    
    public var method: Moya.Method {
        switch self {
        case .addVisitPerson(_):
            return .post
        case .login(_):
            return .post
        case .phoneCode(_):
            return .post
        case .userInfos(_):
            return .get
        case .lineMan(_):
            return .get
        case .addLineMan(_):
            return .post
        case .clientDetail(_):
            return .get
        case .personalSelling(_):
            return .get
        case .homeCommercial(_):
            return .get
        case .homeSchedule(_):
            return .get
        case .homeClue(_):
            return .get
        case .addClient(_):
            return .post
        case .getCompanyInfos(_):
            return .get
        case .getPersonBusiness(_):
            return .get
        case .getPricsList(_):
            return .post
        case .getBusinessLineMan(_):
            return .get
        case .priceDetail(_):
            return .get
        case .commitPrice(_):
            return .post
        case .marketData(_):
            return .get
        case .getBusinessList(_):
            return .get
        case .addBusiness(_):
            return .post
        case .deviceSummary(_):
            return .get
        case .getBusinessDetail(_):
            return .get
        case .deviceSelectionSave(_):
            return .post
        case .priceList(_):
            return .get
        case .priceContrast(_):
            return .get
        case .clientComplaintProductList(_):
            return .get
        case .clientComplaintProductDetail(_):
            return .post
        case .addComplaintProduct(_):
            return .post
        case .getAllVisitInfo(_):
            return .get
        case .getAllClientIdName(_):
            return .get
        case .getPriceProductList(_):
            return .get
        case .getAllContactByClientId(_):
            return .get
        case .getNicheClientIdName(_):
            return .get
        case .offerMaterialList(_):
            return .get
        case .sampleDel(_):
            return .post
        case .sampleAdd(_):
            return .post
        case .clientDevelopmentDel(_):
            return .post
        case .addClientSelected(_):
            return .get
        case .clientSelectConfig(_):
            return .get
        case .addClientDepartMent(_):
            return .get
        case .addClientArchives(_):
            return .get
        case .addClientPricePerson(_):
            return .get
        case .addClientBussinessNumber(_):
            return .get
        case .addClientBussinessClientName(_):
            return .get
        case .addClientBussinessNature(_):
            return .get
        case .addClientBussinessPhase(_):
            return .get
        case .addClientBussinessFlower(_):
            return .get
        case .addClientBussinessCommitStatus(_):
            return .get
        case .addClientSellLeads(_):
            return .get
        case .addClientSampleTechnology(_):
            return .get
        case .addClientSamleCode(_):
            return .get
        case .addClientCertificationAsk(_):
            return .get
        case .addClientVisitMyClient(_):
            return .get
        case .getAddClientInfos(_):
            return .get
        case .getAddClinetInfosDetail(_):
            return .get
        }
    }
    
    public var task: Moya.Task {
        switch self {
        case .login(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .phoneCode(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .userInfos(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .lineMan(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addLineMan(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .clientDetail(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .personalSelling(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .homeCommercial(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .homeSchedule(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .homeClue(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClient(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .getCompanyInfos(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getPersonBusiness(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getPricsList(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .getBusinessLineMan(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .priceDetail(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .commitPrice(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .marketData(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getBusinessList(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addBusiness(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .deviceSummary(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getBusinessDetail(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .deviceSelectionSave(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .priceList(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .priceContrast(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .clientComplaintProductList(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .clientComplaintProductDetail(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .addComplaintProduct(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .getAllVisitInfo(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getAllClientIdName(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getPriceProductList(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getAllContactByClientId(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getNicheClientIdName(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .offerMaterialList(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .sampleDel(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .sampleAdd(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .clientDevelopmentDel(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientSelected(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .clientSelectConfig(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientDepartMent(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientArchives(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientPricePerson(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientBussinessNumber(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientBussinessClientName(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientBussinessNature(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientBussinessPhase(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientBussinessFlower(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientBussinessCommitStatus(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientSellLeads(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientSampleTechnology(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientSamleCode(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientCertificationAsk(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addClientVisitMyClient(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .addVisitPerson(let dict):
            return .requestData(jsonToData(jsonDic: dict)!)
        case .getAddClientInfos(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        case .getAddClinetInfosDetail(let dict):
            return .requestParameters(parameters: dict, encoding: URLEncoding.default)
        }
    }
    
    public var headers: [String : String]? {
        return [
            "Content-type" : "application/json",
            "Authorization" : LLUserDefaultsManager.shared.readLoginInfosTK() ?? ""
        ]
    }
    
    public var sampleData: Data {
        return Data()
    }
}

