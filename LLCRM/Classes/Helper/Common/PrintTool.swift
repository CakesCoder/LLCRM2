//
//  PrintTool.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import Foundation

func printTest<T>(_ message: T) {
    let date = Date()
    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss SSSS"
    
    let stringTime = dateFormatter.string(from: date)
    print("\(stringTime) \(message)")
}
