//
//  LLCache.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

dynamic let localCache = LLCache.shared

class LLCache: NSObject { 
    public static let shared = LLCache()
    
    @objc class func sharedInstance() -> LLCache {
        return LLCache.shared
    }
    
    override init() {
        super.init()
    }
}
