//
//  LLUserDefaults.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import SwiftyUserDefaults

extension DefaultsKeys {
    
    var closeGuideKey: DefaultsKey<String?> {
        .init("closeGuide",defaultValue: "")
    }
    
    var userInfoLoginKey: DefaultsKey<[String:Any]?> {
        .init("userInfoLoginKey")
    }
    
    var userInfosTKKey: DefaultsKey<String?> {
        .init("userInfosTKKey",defaultValue: "")
    }
    
    var userInfosKey: DefaultsKey<[String:Any]?> {
        .init("userInfosKey")
    }
    
    var userInfosNameKey: DefaultsKey<String?> {
        .init("userInfosNameKey")
    }
    
    var userInfosDepartmentNameKey: DefaultsKey<String?> {
        .init("userInfosDepartmentNameKey")
    }
    
    var userInfosCompanyNameKey: DefaultsKey<String?> {
        .init("userInfosCompanyNameKey")
    }
    
    var userInfosEmailKey: DefaultsKey<String?> {
        .init("userInfosEmailKey")
    }
    
    var userInfosPhoneKey: DefaultsKey<String?> {
        .init("userInfosPhoneKey")
    }
    
    var creditCodeKey: DefaultsKey<String?> {
        .init("creditCodeKey")
    }
}

@objc
class LLUserDefaultsManager: NSObject {
    @objc static let shared = LLUserDefaultsManager()
    
    @objc func cacheIsShowGudide(){
        Defaults[keyPath: \.closeGuideKey] = "Y"
    }
    
    @objc func readCacheShowGudide()->String{
        guard let isShow = Defaults[keyPath: \.closeGuideKey] else { return "" }
        return isShow
    }
    
    @objc func clearCachedUserInfo() {
        Defaults.remove(\.closeGuideKey)
    }
    
    func saveLoginInfos(infos : [String : Any]?){
        Defaults[keyPath: \.userInfoLoginKey] = infos
    }

    func readCacheLogin()->[String : Any]?{
        guard let infos = Defaults[keyPath: \.userInfoLoginKey] else{
            return nil
        }
        return infos
    }
    
    func saveLoginInfosTK(tk : String?){
        Defaults[keyPath: \.userInfosTKKey] = tk
    }

    func readLoginInfosTK()->String?{
        guard let tk = Defaults[keyPath: \.userInfosTKKey] else{
            return ""
        }
        return tk
    }
    
    func removeTK() {
        Defaults.remove(\.userInfosTKKey)
    }
    
    func saveUserInfos(user : [String:Any]){
        Defaults[keyPath: \.userInfosKey] = user
    }

    func readUserInfos()->[String:Any]?{
        guard let users = Defaults[keyPath: \.userInfosKey] else{
            return nil
        }
        return users
    }
    
    func saveUserNameInfos(userName : String?){
        Defaults[keyPath: \.userInfosNameKey] = userName
    }

    func readUserNameInfos()->String?{
        guard let userName = Defaults[keyPath: \.userInfosNameKey] else{
            return ""
        }
        return userName
    }
    
    func saveUserDepartmentNameInfos(userName : String?){
        Defaults[keyPath: \.userInfosDepartmentNameKey] = userName
    }

    func readUserDepartmentNameInfos()->String?{
        guard let userDepartmentName = Defaults[keyPath: \.userInfosDepartmentNameKey] else{
            return ""
        }
        return userDepartmentName
    }
    
    func saveUserCompanyNameInfos(userName : String?){
        Defaults[keyPath: \.userInfosCompanyNameKey] = userName
    }

    func readUserCompanyNameInfos()->String?{
        guard let userDepartmentName = Defaults[keyPath: \.userInfosCompanyNameKey] else{
            return ""
        }
        return userDepartmentName
    }
    
    func saveUserEmailInfos(emil: String?){
        Defaults[keyPath: \.userInfosEmailKey] = emil
    }

    func readUserEmailInfos()->String?{
        guard let userEmail = Defaults[keyPath: \.userInfosEmailKey] else{
            return ""
        }
        return userEmail
    }
    
    func saveUserPhoneInfos(phone : String?){
        Defaults[keyPath: \.userInfosPhoneKey] = phone
    }

    func readUserPhoneInfos()->String?{
        guard let userPhone = Defaults[keyPath: \.userInfosPhoneKey] else{
            return ""
        }
        return userPhone
    }
    
    func saveUserCreditCodeInfos(code : String?){
        Defaults[keyPath: \.creditCodeKey] = code
    }

    func readUserCreditCodeInfos()->String?{
        guard let userCode = Defaults[keyPath: \.creditCodeKey] else{
            return ""
        }
        return userCode
    }
}

