//
//  Global.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/12.
//

import UIKit
import Kingfisher
import SnapKit


//MARK: -根据视觉稿单位适配
@discardableResult
func kPT(_ val:CGFloat) -> CGFloat {
    return val * (UIScreen.main.bounds.width / 375.0)
}

//MARK: - UIFont
@discardableResult
func kMediumFont(_ fontSize:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: fontSize, weight:UIFont.Weight.medium)
}

@discardableResult
func kBoldFont(_ fontSize:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: fontSize, weight:UIFont.Weight.semibold)
}

@discardableResult
func kLightFont(_ fontSize:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: fontSize, weight:UIFont.Weight.light)
}

@discardableResult
func kThinFont(_ fontSize:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: fontSize, weight:UIFont.Weight.thin)
}

@discardableResult
func kRegularFont(_ fontSize:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: fontSize, weight:UIFont.Weight.regular)
}

//MARK: - UIFont - Adapter, 根据视觉稿宽度适配
@discardableResult
func kAMediumFont(_ fontSize:CGFloat) -> UIFont {
    return kMediumFont(kPT(fontSize))
}

@discardableResult
func kABoldFont(_ fontSize:CGFloat) -> UIFont {
    return kBoldFont(kPT(fontSize))
}

@discardableResult
func kALightFont(_ fontSize:CGFloat) -> UIFont {
    return kLightFont(kPT(fontSize))
}

@discardableResult
func kAThinFont(_ fontSize:CGFloat) -> UIFont {
    return kThinFont(kPT(fontSize))
}

@discardableResult
func kARegularFont(_ fontSize:CGFloat) -> UIFont {
    return kRegularFont(kPT(fontSize))
}
