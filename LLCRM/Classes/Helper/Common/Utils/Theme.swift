//
//  Theme.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/26.
//

import UIKit

let themeColor = UIColor(0x4871C0)

let blackTextColor = UIColor(0x1E1F24)
let blackTextColor2 = UIColor(0x333333)
let blackTextColor3 = UIColor(0x181818)
let blackTextColor4 = UIColor(0x656665)
let blackTextColor5 = UIColor(0x000000)
let blackTextColor6 = UIColor(0xC0C4CC)
let blackTextColor7 = UIColor(0x606266)
let blackTextColor8 = UIColor(0x141416)
let blackTextColor9 = UIColor(0x1D1D1D)
let blackTextColor10 = UIColor(0x505050)
let blackTextColor11 = UIColor(0x3B3B3B)
//let blackTextColor12 = UIColor(0x181818)
let blackTextColor13 = UIColor(0x171717)
let blackTextColor14 = UIColor(0x333334)
let blackTextColor15 = UIColor(0x131416)
let blackTextColor16 = UIColor(0x0F0F0F)
let blackTextColor17 = UIColor(0x4A4A4A)
let blackTextColor18 = UIColor(0xCCCCCC)
let blackTextColor19 = UIColor(0xF6F7F9)
let blackTextColor20 = UIColor(0x939393)
let blackTextColor21 = UIColor(0x8F8F8F)
let blackTextColor22 = UIColor(0xC4C4C6)
let blackTextColor23 = UIColor(0x5D5D5D)
let blackTextColor24 = UIColor(0x8E8E8E)
let blackTextColor25 = UIColor(0xCECED3)
let blackTextColor26 = UIColor(0x414141)
let blackTextColor27 = UIColor(0xF95151)
let blackTextColor28 = UIColor(0x868686)
let blackTextColor29 = UIColor(0x636363)
let blackTextColor30 = UIColor(0xCECECE)
let blackTextColor31 = UIColor(0x676767)
let blackTextColor32 = UIColor(0x9F9F9F)
let blackTextColor33 = UIColor(0x020202)
let blacktextColor34 = UIColor(0x6C6C6C)
let blackTextColor35 = UIColor(0xC6C6C6)
let blackTextColor36 = UIColor(0xF56C6C)
let blackTextColor37 = UIColor(0x7B7B7B)
let blackTextColor38 = UIColor(0x313131)
let blackTextColor39 = UIColor(0x848484)
let blackTextColor40 = UIColor(0x989898)
let blackTextColor41 = UIColor(0xAFAFAF)
let blackTextColor42 = UIColor(0x030303)
let blackTextColor43 = UIColor(0xACACAC)
let blackTextColor44 = UIColor(0xE5A23A)
let blackTextColor45 = UIColor(0xBFBFBF)
let blackTextColor46 = UIColor(0xB1B1B1)
let blackTextColor47 = UIColor(0x5B5B5B)
let blackTextColor48 = UIColor(0x121212)
let blackTextColor49 = UIColor(0x0B0B0B)
let blackTextColor50 = UIColor(0x699AF1)
let blackTextColor51 = UIColor(0xA7A7A7)
let blacktextColor52 = UIColor(0x6F6F6F)
let blackTextColor53 = UIColor(0x262626)
let blackTextColor54 = UIColor(0x9C9C9C)
let blackTextColor55 = UIColor(0x5A5A5A)
let blackTextColor56 = UIColor(0xB7B7B7)
let blackTextColor57 = UIColor(0xA9A9A9)
let blackTextColor58 = UIColor(0x464646)
let blackTextColor59 = UIColor(0x5C5C5C)
let blackTextColor60 = UIColor(0x888888)
let blackTextColor61 = UIColor(0x58A3FF)
let blackTextColor62 = UIColor(0x747474)
let blackTextColor63 = UIColor(0x535967)
let blackTextColor64 = UIColor(0x3283FF)
let blackTextColor65 = UIColor(0x141414)
let blackTextColor66 = UIColor(0x808080)
let blackTextColor67 = UIColor(0x333333)
let blackTextColor68 = UIColor(0x7E7E7E)
let blackTextColor69 = UIColor(0x5A8CFF)
let blackTextColor70 = UIColor(0x999999)
let blackTextColor71 = UIColor(0x007AFF)
let blackTextColor72 = UIColor(0xA3A3A3)
let blackTextColor73 = UIColor(0x5F5F5F)
let blackTextColor74 = UIColor(0x717171)
let blackTextColor75 = UIColor(0xBEBEBE)
let blackTextColor76 = UIColor(0xD5D5D5)
let blackTextColor77 = UIColor(0x434343)
let blackTextColor78 = UIColor(0x2B2B2B)
let blackTextColor79 = UIColor(0x838383)
let blackTextColor80 = UIColor(0x292929)
let blackTextColor81 = UIColor(0x151515)
let blackTextColor82 = UIColor(0x646464)
let blackTextColor83 = UIColor(0x3E3E3E)
let blackTextColor84 = UIColor(0x555555)
let blackTextColor85 = UIColor(0x696969)
let blackTextColor86 = UIColor(0x272727)
let blackTextColor87 = UIColor(0x1F1F1F)
let blackTextColor88 = UIColor(0x525252)
let blackTextColor89 = UIColor(0x444444)
let blackTextColor90 = UIColor(0x141416)

let dateColor = UIColor(0xB2B2B2)

let bluebgColor = UIColor(0x409EFF)

let bgColor = UIColor(0xF4F6F9)
let bgColor2 = UIColor(0xFFFCF7)
let bgColor3 = UIColor(0xF8F8F8)
let bgColor4 = UIColor(0xF2F8FF)
let bgColor5 = UIColor(0xF4F5F7)
let bgColor6 = UIColor(0xFFFDF7)
let bgColor7 = UIColor(0xFFFDF8)
let bgColor8 = UIColor(0x4A445C)
let bgColor9 = UIColor(0x87C355)
let bgColor10 = UIColor(0xDEDEDE)
let bgColor11 = UIColor(0xF7F7F7)
let bgColor12 = UIColor(0xFFE0E0)
let bgColor13 = UIColor(0xEFF7FF)
let bgColor14 = UIColor(0xF3FFEA)
let bgColor15 = UIColor(0xFFEAEA)
let bgColor16 = UIColor(0xFFF1DB)
let bgColor17 = UIColor(0x1989FA)
let bgColor18 = UIColor(0xECF2E7)
let bgColor19 = UIColor(0xE5A23B)
let bgColor20 = UIColor(0xE3F1FF)
let bgColor21 = UIColor(0xF3F4F4)
let bgColor22 = UIColor(0xD3D3D3)
let bgColor23 = UIColor(0xFFFEFC)

let workTagColor = UIColor(0x2F88FF)

let lineColor = UIColor(0xF3F5F9)
let lineColor2 = UIColor(0xE6E6E6)
let lineColor3 = UIColor(0xDCDFE6)
let lineColor4 = UIColor(0xF5F5F5)
let lineColor5 = UIColor(0xEFEFEF)
let lineColor6 = UIColor(0xF0F0F0)
let lineColor7 = UIColor(0xF4F4F4)
let lineColor8 = UIColor(0xF7F7F7)
let lineColor9 = UIColor(0xE5E5E5)
let lineColor10 = UIColor(0xEDEDED)
let lineColor11 = UIColor(0xE9EAEC)
let lineColor12 = UIColor(0x84B7F3)
let lineColor13 = UIColor(0xE3E3E3)
let lineColor14 = UIColor(0xEBEBEB)
let lineColor15 = UIColor(0xEAEAEA)
let lineColor16 = UIColor(0xFAFAFA)
let lineColor17 = UIColor(0xDBDBDB)
let lineColor18 = UIColor(0xF2F2F2)
let lineColor19 = UIColor(0xC9C9C9)
let lineColor20 = UIColor(0xF2F2F2)
let lineColor21 = UIColor(0x606060)
let lineColor22 = UIColor(0xF4F5F7)
let lineColor23 = UIColor(0xD9D9D9)
let lineColor24 = UIColor(0x3C3C43)
let lineColor25 = UIColor(0x86C354)
let lineColor26 = UIColor(0xF56C6D)
let lineColor27 = UIColor(0xBABABA)
let lineColor28 = UIColor(0xD5D8DE)
let lineColor29 = UIColor(0xF6F6F7)
let lineCColor30 = UIColor(0xD1D1D1)

