//
//  StringTool.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/25.
//

import UIKit

func toJSONString(dict:[String:Any])->String{
    let data = try? JSONSerialization.data(withJSONObject: dict, options: JSONSerialization.WritingOptions.prettyPrinted)
    let strJson = NSString(data: data!, encoding: NSUTF8StringEncoding)
    return strJson! as String
}

