//
//  MyError.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import Foundation

enum MyError: Error {
    case requestError
}
