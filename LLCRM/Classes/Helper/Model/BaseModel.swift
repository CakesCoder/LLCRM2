//
//  BaseModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/17.
//

import UIKit
import HandyJSON

class BaseModel: HandyJSON {
    required init() {}
}
