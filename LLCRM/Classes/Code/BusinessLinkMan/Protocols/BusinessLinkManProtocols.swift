//
//  BusinessLinkManProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/6.
//

import UIKit

class BusinessLinkManProtocols: NSObject {

}

protocol BusinessLinkManViewProtocols: AnyObject {
    var presenter: BusinessLinkManPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetBusinessLinkManPresenterReceiveData(by params: Any?)
    func didGetBusinessLinkManPresenterReceiveError(error: MyError?)
}

protocol BusinessLinkManPresenterProtocols: AnyObject{
    var view: BusinessLinkManViewProtocols? { get set }
    var router: BusinessLinkManRouterProtocols? { get set }
    var interactor: BusinessLinkManInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetBusinessLinkMan(by params: Any?)
    func didGetBusinessLinkManInteractorReceiveData(by params: Any?)
    func didGetBusinessLinkManInteractorReceiveError(error: MyError?)
}

protocol BusinessLinkManInteractorProtocols: AnyObject {
    var presenter: BusinessLinkManPresenterProtocols? { get set }
    var entity: BusinessLinkManEntityProtocols? { get set }
    
    func presenterRequestBusinessLinkMan(by params: Any?)
    func didEntityBusinessLinkManReceiveData(by params: Any?)
    func didEntityBusinessLinkManReceiveError(error: MyError?)
}

protocol BusinessLinkManEntityProtocols: AnyObject {
    var interactor: BusinessLinkManInteractorProtocols? { get set }
    
    func didBusinessLinkManReceiveData(by params: Any?)
    func didBusinessLinkManReceiveError(error: MyError?)
    func getBusinessLinkManRequest(by params: Any?)
}

protocol BusinessLinkManRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController)
}

