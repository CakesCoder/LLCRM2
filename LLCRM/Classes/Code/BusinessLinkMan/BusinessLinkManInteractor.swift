//
//  BusinessLinkManInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/6.
//

import UIKit

class BusinessLinkManInteractor: BusinessLinkManInteractorProtocols {
    var presenter: BusinessLinkManPresenterProtocols?
    
    var entity: BusinessLinkManEntityProtocols?
    
    func presenterRequestBusinessLinkMan(by params: Any?) {
        entity?.getBusinessLinkManRequest(by: params)
    }
    
    func didEntityBusinessLinkManReceiveData(by params: Any?) {
        presenter?.didGetBusinessLinkManInteractorReceiveData(by: params)
    }
    
    func didEntityBusinessLinkManReceiveError(error: MyError?) {
        presenter?.didGetBusinessLinkManInteractorReceiveError(error: error)
    }
    
    
}
