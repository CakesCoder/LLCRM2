//
//  BusinessLinkManViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit
//import JXPagingView
import HandyJSON
import PKHUD

extension BusinessLinkManViewController: UIScrollViewDelegate {
//    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.listViewDidScrollCallback?(scrollView)
//    }
}

extension BusinessLinkManViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class BusinessLinkManViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var presenter: BusinessLinkManPresenterProtocols?
    var model:ClientPerosonDetailModel?
    var infosModel:ClientPerosnBusinessModel?
    
    var nameLabel:UILabel?
    var jobLabel:UILabel?
    var phoneLabel:UILabel?
    var emailLabel:UILabel?
    
    
    lazy var noDataView:UIView = {
        let noDataV = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let noDataImageV = UIImageView().then { obj in
            noDataV.addSubview(obj)
            obj.image = UIImage(named: "detail_noData")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(120)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            noDataV.addSubview(obj)
            obj.text = "暂无数据"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor38
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(300)
                make.centerX.equalToSuperview()
            }
        }
        return noDataV
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "商机联系人"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let addButton = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
//            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let systemInfos = ["姓名：", "职位：", "手机号：", "电话号：", "邮箱："]
        
        let systemContentInfos = ["王小虎/男", "采购经理", "138 9209 9839", "0393-9009 9090", "628983128@qq.com"]
        
        for i in 0..<systemInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:systemInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                var contentStr = ""
                if i == 0{
                    contentStr = (model?.clientClueContactSimpleParamList?[0].name ?? "--") + "/" + (model?.clientClueContactSimpleParamList?[0].gender ?? "--")
                }else if i == 1{
                    contentStr = model?.clientClueContactSimpleParamList?[0].position ?? "--"
                }else if i == 2{
                    contentStr = model?.clientClueContactSimpleParamList?[0].phone ?? "--"
                }else if i == 3{
                    contentStr = "--"
                }else if i == 4{
                    contentStr = model?.clientClueContactSimpleParamList?[0].email ?? "--"
                }
                let hintAttribute = NSMutableAttributedString(string:contentStr)
                if i == systemInfos.count - 1{
                    hintAttribute.yy_color = bluebgColor
                }else{
                    hintAttribute.yy_color = blackTextColor81
                }
//                if i == 0{
//                    nameLabel = obj
//                }else if i == 1{
//                    jobLabel = obj
//                }else if i == 2{
//                    phoneLabel = obj
//                }else if i == 4{
//                    emailLabel = obj
//                }
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i == 2{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = .red
                    obj.snp.makeConstraints { make in
                        make.left.equalTo(contentLabel.snp.right).offset(15)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })

        // Do any additional setup after loading the view.
        cofing()
    }
    
    func cofing(){
        let router = BusinessLinkManRouter()
        
        presenter = BusinessLinkManPresenter()
        
        presenter?.router = router
        
        let entity = BusinessLinkManEntity()
        
        let interactor = BusinessLinkManInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        printTest("asdasdasdasdasdasdasdasdasdasd")
//        print(model?.clientName)
//        presenter?.presenterRequestGetBusinessLinkMan(by: ["clientId":model?.id])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension BusinessLinkManViewController:BusinessLinkManViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetBusinessLinkManPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if dataDic != nil{
                    if let model = JSONDeserializer<ClientPerosnBusinessModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                       infosModel = model
//                       businessNameLabel?.text = infosModel?.commercialName ?? "--"
//                       moneyLabel?.text = infosModel?.commercialMoneyUnit ?? "--"
//                       commitDateLabel?.text = infosModel?.productDate ?? "--"
//                       nameLabel?.text = infosModel?.createrName ?? "--"
                   }
                }else{
//                    businessNameLabel?.text = "--"
//                    moneyLabel?.text = "--"
//                    commitDateLabel?.text = "--"
//                    nameLabel?.text = "--"
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetBusinessLinkManPresenterReceiveError(error: MyError?) {
        
    }
    
}
