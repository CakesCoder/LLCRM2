//
//  BusinessLinkManPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/6.
//

import UIKit

class BusinessLinkManPresenter: BusinessLinkManPresenterProtocols {
    var view: BusinessLinkManViewProtocols?
    
    var router: BusinessLinkManRouterProtocols?
    
    var interactor: BusinessLinkManInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetBusinessLinkMan(by params: Any?) {
        interactor?.presenterRequestBusinessLinkMan(by: params)
    }
    
    func didGetBusinessLinkManInteractorReceiveData(by params: Any?) {
        view?.didGetBusinessLinkManPresenterReceiveData(by: params)
    }
    
    func didGetBusinessLinkManInteractorReceiveError(error: MyError?) {
        view?.didGetBusinessLinkManPresenterReceiveError(error: error)
    }
    
    
}
