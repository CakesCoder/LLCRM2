//
//  BusinessLinkManEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/6.
//

import UIKit

class BusinessLinkManEntity: BusinessLinkManEntityProtocols {
    var interactor: BusinessLinkManInteractorProtocols?
    
    func didBusinessLinkManReceiveData(by params: Any?) {
        interactor?.didEntityBusinessLinkManReceiveData(by: params)
    }
    
    func didBusinessLinkManReceiveError(error: MyError?) {
        interactor?.didEntityBusinessLinkManReceiveError(error: error)
    }
    
    func getBusinessLinkManRequest(by params: Any?) {
        LLNetProvider.request(.getPersonBusiness(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didBusinessLinkManReceiveData(by:jsonData)
            case .failure(_):
                self.didBusinessLinkManReceiveError(error: .requestError)
            }
        }
    }
    

}
