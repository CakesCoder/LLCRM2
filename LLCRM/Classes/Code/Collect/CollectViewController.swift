//
//  CollectViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class CollectViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "我的收藏"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(CollectViewController.blackClick))
        
        let searchView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(UIDevice.xp_navigationFullHeight())
                make.height.equalTo(50)
            }
        }
        
        let searchContentView = UIView().then { obj in
            searchView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.height.equalTo(30)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchContentView.addSubview(obj)
            obj.placeholder = "搜索"
            obj.font = kARegularFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-10)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            if #available(iOS 11.0, *) {
                obj.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
            } else {
                // Fallback on earlier versions
            }
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
            obj.backgroundColor = .white
            obj.register(CollectRecordTableViewCell.self, forCellReuseIdentifier: "CollectRecordTableViewCell")
            obj.register(CollectImageTableViewCell.self, forCellReuseIdentifier: "CollectImageTableViewCell")
            obj.register(CollectRecordTableViewCell2.self, forCellReuseIdentifier: "CollectRecordTableViewCell2")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(searchView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    
    @objc func blackClick(){
        navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CollectViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectImageTableViewCell", for: indexPath) as! CollectImageTableViewCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CollectRecordTableViewCell2", for: indexPath) as! CollectRecordTableViewCell2
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "CollectRecordTableViewCell", for: indexPath) as! CollectRecordTableViewCell
        return cell
    }
    
    
}
