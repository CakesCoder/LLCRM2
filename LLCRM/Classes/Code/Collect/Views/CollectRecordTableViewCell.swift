//
//  CollectRecordTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class CollectRecordTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.layer.cornerRadius = 20
            obj.layer.masksToBounds = true
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
                make.width.height.equalTo(40)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            obj.textColor = blackTextColor
//            obj.backgroundColor = .red
            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(8)
                make.centerY.equalTo(imageV)
            }
        }
        
        let moreImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(imageV)
                make.width.equalTo(20)
                make.height.equalTo(20)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "2022年05月25日  12:20"
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor43
            
            obj.snp.makeConstraints { make in
                make.right.equalTo(moreImageV.snp.left).offset(-20)
                make.centerY.equalTo(imageV)
            }
        }
        
        let shareView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(imageV.snp.bottom).offset(10)
                make.height.equalTo(60)
            }
        }
        
        let shareImageV = UIImageView().then { obj in
            shareView.addSubview(obj)
            obj.image = UIImage(named: "share_record")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(19)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let shareTitleLabel = UILabel().then { obj in
            shareView.addSubview(obj)
            obj.text = "销售记录"
            obj.font = kMediumFont(10)
            obj.textColor = blackTextColor16
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(shareImageV.snp.bottom).offset(4)
                make.centerX.equalTo(shareImageV)
            }
        }
        
        let shareLineView = UIView().then { obj in
            shareView.addSubview(obj)
            obj.backgroundColor = lineColor19
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(60)
                make.top.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-15)
                make.width.equalTo(0.5)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            shareView.addSubview(obj)
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor16
            obj.text = "客户"
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(shareLineView.snp.right).offset(10)
                make.top.equalToSuperview().offset(11)
            }
        }
        
        let shareContentLabel = UILabel().then { obj in
            shareView.addSubview(obj)
            obj.text = "跟进中"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor44
            obj.snp.makeConstraints { make in
                make.left.equalTo(shareLineView.snp.right).offset(10)
                make.top.equalTo(titleLabel.snp.bottom).offset(5)
            }
        }
        
        let bottomView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor16
            obj.snp.makeConstraints { make in
                make.bottom.equalToSuperview().offset(-10)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let bottomImageV = UIImageView().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = .green
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(16)
            }
        }
        
        let addTagLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "添加标签"
            obj.font = kMediumFont(10)
            obj.textColor = blackTextColor43
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(bottomImageV.snp.right).offset(8)
                make.centerY.equalToSuperview()
            }
        }
        
        let bottomLineView = UIView().then { obj in
            obj.backgroundColor = lineColor6
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
    }
}
