//
//  MyBusinessListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit
//import JXSegmentedView
//import JXPagingView
import JFPopup

class MyBusinessListViewController: BaseViewController {
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "商机"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MyBusinessListViewController.blackClick))
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        view.addSubview(self.segmentedView!)
        
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(headView.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["进行中","已完成","我的商机","全部"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
//        segmentedView?.delegate = self
//        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(MyBusinessListCell.self, forCellReuseIdentifier: "MyBusinessListCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(segmentedView!.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        self.popup.bottomSheet { [weak self] in
            let showView = MyBusinessMoneyShowView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            showView.closeBlock = { [weak self] in
                self?.popup.dismissPopup()
            }
//            showView.closeBlock = { [weak self] in
//                self?.presenter?.router?.pushToApprovalInformation(from: self!)
//                self?.popup.dismissPopup()
//            }
//            showView.featureBlock = {[weak self] tag in
//                if tag == 1000{
//                    self?.presenter?.router?.pushToNextVC(from: self!)
//                }else if tag == 1001{
//                    self?.presenter?.router?.pushAddPlan(from: self!)
//                }else if tag == 1002{
//                    self?.presenter?.router?.pushAddApproval(from: self!)
//                }else if tag == 1003{
//                    self?.presenter?.router?.pushAddTask(from: self!)
//                }else if tag == 1004{
//                    self?.presenter?.router?.pushSchedule(from: self!)
//                }
//                else{
//
//                }
//                self?.popup.dismissPopup()
//            }
            return showView
        }
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension MyBusinessListViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
//        self.searchText = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
}

extension MyBusinessListViewController: JXSegmentedViewDelegate{
//    func pagingView(_ pagingView: JXPagingView.JXPagingView, initListAtIndex index: Int) -> JXPagingView.JXPagingViewListViewDelegate {
//        return nil
//    }
//
//    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
//        return 0
//    }r
//
//    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
//        return 0
//    }
//
//    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func numberOfLists(in pagingView: JXPagingView) -> Int {
//        return 4
//    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
    
    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}

extension MyBusinessListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 76
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return (presenter?.params!.count)!
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyBusinessListCell", for: indexPath) as! MyBusinessListCell
//        cell.model = (presenter?.params?[indexPath.row] as! LinkManModel)ss
//        cell.backgroundColor = .red
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.presenter?.router?.pushToAddLinkMan(from: self)
//        self.tableViewDidSeletedCallback?(indexPath.row)
    }
}
