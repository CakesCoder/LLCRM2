//
//  MyBusinessListProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

protocol MyBusinessListViewProtocols: AnyObject {
    var presenter: MyBusinessListPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetMyBusinessListPresenterReceiveData(by params: Any?)
    func didGetMyBusinessListPresenterReceiveError(error: MyError?)
}

protocol MyBusinessListPresenterProtocols: AnyObject{
    var view: MyBusinessListViewProtocols? { get set }
    var router: MyBusinessListRouterProtocols? { get set }
    var interactor: MyBusinessListInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetMyBusinessList(by params: Any?)
    func didGetMyBusinessListInteractorReceiveData(by params: Any?)
    func didGetMyBusinessListInteractorReceiveError(error: MyError?)
}

protocol MyBusinessListInteractorProtocols: AnyObject {
    var presenter: MyBusinessListPresenterProtocols? { get set }
    var entity: MyBusinessListEntityProtocols? { get set }
    
    func presenterRequestMyBusinessList(by params: Any?)
    func didEntityMyBusinessListReceiveData(by params: Any?)
    func didEntityMyBusinessListReceiveError(error: MyError?)
}

protocol MyBusinessListEntityProtocols: AnyObject {
    var interactor: MyBusinessListInteractorProtocols? { get set }
    
    func didMyBusinessListReceiveData(by params: Any?)
    func didMyBusinessListReceiveError(error: MyError?)
    func getMyBusinessListRequest(by params: Any?)
}

protocol MyBusinessListRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController)
}


