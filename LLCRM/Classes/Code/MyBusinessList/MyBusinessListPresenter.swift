//
//  MyBusinessListPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class MyBusinessListPresenter: MyBusinessListPresenterProtocols {
    var view: MyBusinessListViewProtocols?
    
    var router: MyBusinessListRouterProtocols?
    
    var interactor: MyBusinessListInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetMyBusinessList(by params: Any?) {
        
    }
    
    func didGetMyBusinessListInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetMyBusinessListInteractorReceiveError(error: MyError?) {
        
    }
    

}
