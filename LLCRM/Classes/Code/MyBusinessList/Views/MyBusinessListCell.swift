//
//  MyBusinessListCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class MyBusinessListCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
//        let tagImageV = UIImageView().then { obj in
//            contentView.addSubview(obj)
//            obj.image = UIImage(named: "business_money")
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(13)
//            }
//        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor78
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
//                make.left.equalTo(tagImageV.snp.right).offset(3)
//                make.centerY.equalTo(tagImageV)
            }
        }
        
//        let tagLabel = UILabel().then { obj in
//            contentView.addSubview(obj)
//            obj.backgroundColor = bgColor9
//            obj.text = "赢单"
//            obj.font = kRegularFont(8)
//            obj.textAlignment = .center
//            obj.snp.makeConstraints { make in
//                make.left.equalTo(nameLabel.snp.right).offset(5)
//                make.centerY.equalTo(tagImageV)
//                make.width.equalTo(26)
//                make.height.equalTo(15)
//            }
//        }
        
//        let codeLabel = UILabel().then { obj in
//            contentView.addSubview(obj)
//
//            let hintAttribute = NSMutableAttributedString(string:"商机编号：")
//            hintAttribute.yy_color = blackTextColor60
//            hintAttribute.yy_font = kMediumFont(12)
//
//            let personAttribute = NSMutableAttributedString(string: "BO230427001")
//            personAttribute.yy_color = bluebgColor
//            personAttribute.yy_font = kMediumFont(12)
//
//            hintAttribute.append(personAttribute)
//
//
//            obj.attributedText = hintAttribute
//
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(35)
//                make.top.equalTo(nameLabel.snp.bottom).offset(10)
//            }
//        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
            obj.text = "日期：2023.04.27  09:30"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-15)
                
            }
        }
        
        let nameLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.font = kMediumFont(12)
            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel)
            }
        }
        
        let moneyLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor36
            obj.text = "￥5000.00"
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(timeLabel)
            }
        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        }
    }

}
