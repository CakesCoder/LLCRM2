//
//  MyBusinessMoneyShowView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class MyBusinessMoneyShowView: UIView {
    
    var closeBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let bgView = UIView().then { obj in
            self.addSubview(obj)
            //            obj.backgroundColor = .blue
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(221)
            }
        }
        
        let hintBusiness = ["商机总额", "已成交商机金额", "输单商机金额"]
        let businessContents = ["40,000.00元", "0元", "0元"]
        
        for i in 0..<hintBusiness.count{
            let v = UIView().then { obj in
                bottomView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.top.equalToSuperview().offset(20+i*50)
                    make.height.equalTo(50)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.text = hintBusiness[i]
                obj.font = kMediumFont(13)
                obj.textColor = blackTextColor60
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(27)
                    make.centerY.equalToSuperview()
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.text = hintBusiness[i]
                obj.font = kMediumFont(13)
                obj.textColor = blackTextColor86
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-27)
                    make.centerY.equalToSuperview()
                }
            }
            
            let cancelButton = UIButton().then { obj in
                bottomView.addSubview(obj)
                obj.setTitle("取消", for: .normal)
                obj.setTitleColor(.black, for: .normal)
                obj.titleLabel?.font = kMediumFont(16)
                obj.addTarget(self, action: #selector(cancelClick), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(50)
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @objc func cancelClick(){
        self.closeBlock?()
    }
}
