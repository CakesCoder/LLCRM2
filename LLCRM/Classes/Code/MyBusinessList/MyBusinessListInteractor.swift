//
//  MyBusinessListInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class MyBusinessListInteractor: MyBusinessListInteractorProtocols {
    var presenter: MyBusinessListPresenterProtocols?
    
    var entity: MyBusinessListEntityProtocols?
    
    func presenterRequestMyBusinessList(by params: Any?) {
        
    }
    
    func didEntityMyBusinessListReceiveData(by params: Any?) {
        
    }
    
    func didEntityMyBusinessListReceiveError(error: MyError?) {
        
    }
    
    
}
