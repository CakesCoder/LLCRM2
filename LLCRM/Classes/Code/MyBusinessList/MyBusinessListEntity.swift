//
//  MyBusinessListEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class MyBusinessListEntity: MyBusinessListEntityProtocols {
    var interactor: MyBusinessListInteractorProtocols?
    
    func didMyBusinessListReceiveData(by params: Any?) {
        
    }
    
    func didMyBusinessListReceiveError(error: MyError?) {
        
    }
    
    func getMyBusinessListRequest(by params: Any?) {
        
    }
    
    
}
