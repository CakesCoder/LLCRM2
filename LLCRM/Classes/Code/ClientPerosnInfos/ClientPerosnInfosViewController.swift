//
//  ClientPerosnInfosViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit
//import JXPagingView

extension ClientPerosnInfosViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ClientPerosnInfosViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ClientPerosnInfosViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var model:ClientPerosonDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 25*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(13*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "基本信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["客户名称：", "客户属性：", "来源：", "主要联系人：", "电话：", "跟进阶段：", "客户类型：", "跟进状态：", "客户大概情况：", "备注：", "负责人：", "联系人："]
        
//        let contentInfos = ["零瓴软件技术(深圳)有限公司", "客户属性属性", "客户推荐", "王小虎", "13829289891", "潜在客户", "— —", "跟进中", "— —", "— —", "夏淼淼", "夏淼淼"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                 model?.clientClueContactSimpleParamList?[0]
                var contentStr = ""
                if i == 0{
                    contentStr = model?.clientName ?? "--"
                }else if i == 1{
                    contentStr = "--"
                }else if i == 2{
                    contentStr = model?.source ?? "--"
                }else if i == 3{
                    contentStr = model?.clientClueContactSimpleParamList?[0].name ?? "--"
                }else if i == 4{
                    contentStr = model?.phone ?? "--"
                }else if i == 5{
                    contentStr = "--"
                }else if i == 6{
                    contentStr = "--"
                }else if i == 7{
                    contentStr = "--"
                }else if i == 8{
                    contentStr = "--"
                }else if i == 9{
                    contentStr = "--"
                }else if i == 10{
                    contentStr = model?.claimantName ?? "--"
                }else if i == 11{
                    contentStr = model?.clientName ?? "--"
                }
                let hintAttribute = NSMutableAttributedString(string:contentStr)
                hintAttribute.yy_color = blackTextColor81
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
            }
            
            
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let personInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(baseInfosView.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(9*45)
            }
        }
        
        let personInfosHeadView = UIView().then { obj in
            personInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        _ = UIView().then { obj in
            personInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            personInfosHeadView.addSubview(obj)
            obj.text = "联系人信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        
        let personInfos = ["客户名称：", "职务：", "手机号：", "座机：", "电子邮箱：", "性别：", "出生年月日：", "微信："]
        
//        let personContentInfos = ["王小虎", "采购经理", "182292819281", "0393-9892918", "8908980@qq.com", "女", "1998.01.10", "182292819281"]
        
        for i in 0..<personInfos.count{
            let v = UIView().then { obj in
                personInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:personInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                var contentStr = ""
                if i == 0{
                    contentStr = model?.clientClueContactSimpleParamList?[0].name ?? "--"
                }else if i == 1{
                    contentStr = model?.clientClueContactSimpleParamList?[0].position ?? "--"
                }else if i == 2{
                    contentStr = model?.clientClueContactSimpleParamList?[0].phone ?? "--"
                }else if i == 3{
                    contentStr = model?.clientClueContactSimpleParamList?[0].name ?? "--"
                }else if i == 4{
                    contentStr = "--"
                }else if i == 5{
                    contentStr = model?.clientClueContactSimpleParamList?[0].email ?? "--"
                }else if i == 6{
                    contentStr = model?.clientClueContactSimpleParamList?[0].gender ?? "--"
                }else if i == 7{
                    contentStr = "--"
                }else if i == 8{
                    contentStr = "--"
                }
                let hintAttribute = NSMutableAttributedString(string:contentStr)
                hintAttribute.yy_color = blackTextColor81
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
            }
            
            
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
