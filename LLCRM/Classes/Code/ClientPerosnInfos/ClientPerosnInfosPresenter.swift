//
//  ClientPerosnInfosPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnInfosPresenter: ClientPerosnInfosPresenterProtocols {
    var view: ClientPerosnInfosViewProtocols?
    
    var router: ClientPerosnInfosRouterProtocols?
    
    var interactor: ClientPerosnInfosInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddClient(by params: Any?) {
        
    }
    
    func didGetAddClientInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetAddClientInteractorReceiveError(error: MyError?) {
        
    }
    

}
