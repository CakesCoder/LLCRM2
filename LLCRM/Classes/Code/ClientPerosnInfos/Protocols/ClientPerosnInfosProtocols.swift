//
//  ClientPerosnInfosProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

protocol ClientPerosnInfosViewProtocols: AnyObject {
    var presenter: ClientPerosnInfosPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddClientPresenterReceiveData(by params: Any?)
    func didGetAddClientPresenterReceiveError(error: MyError?)
}

protocol ClientPerosnInfosPresenterProtocols: AnyObject{
    var view: ClientPerosnInfosViewProtocols? { get set }
    var router: ClientPerosnInfosRouterProtocols? { get set }
    var interactor: ClientPerosnInfosInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddClient(by params: Any?)
    func didGetAddClientInteractorReceiveData(by params: Any?)
    func didGetAddClientInteractorReceiveError(error: MyError?)
}

protocol ClientPerosnInfosInteractorProtocols: AnyObject {
    var presenter: ClientPerosnInfosPresenterProtocols? { get set }
    var entity: ClientPerosnInfosEntityProtocols? { get set }
    
    func presenterRequestAddClient(by params: Any?)
    func didEntityAddClientReceiveData(by params: Any?)
    func didEntityAddClientReceiveError(error: MyError?)
}

protocol ClientPerosnInfosEntityProtocols: AnyObject {
    var interactor: ClientPerosnInfosInteractorProtocols? { get set }
    
    func didAddClientnReceiveData(by params: Any?)
    func didAddClientReceiveError(error: MyError?)
    func getAddClientRequest(by params: Any?)
}

protocol ClientPerosnInfosRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
}


