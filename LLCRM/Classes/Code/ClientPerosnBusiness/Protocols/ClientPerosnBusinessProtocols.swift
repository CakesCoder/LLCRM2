//
//  ClientPerosnBusinessProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit


protocol ClientPerosnBusinessViewProtocols: AnyObject {
    var presenter: ClientPerosnBusinessPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetClientPerosnBusinessPresenterReceiveData(by params: Any?)
    func didGetClientPerosnBusinessPresenterReceiveError(error: MyError?)
}

protocol ClientPerosnBusinessPresenterProtocols: AnyObject{
    var view: ClientPerosnBusinessViewProtocols? { get set }
    var router: ClientPerosnBusinessRouterProtocols? { get set }
    var interactor: ClientPerosnBusinessInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetClientPerosnBusiness(by params: Any?)
    func didGetClientPerosnBusinessInteractorReceiveData(by params: Any?)
    func didGetClientPerosnBusinessInteractorReceiveError(error: MyError?)
}

protocol ClientPerosnBusinessInteractorProtocols: AnyObject {
    var presenter: ClientPerosnBusinessPresenterProtocols? { get set }
    var entity: ClientPerosnBusinessEntityProtocols? { get set }
    
    func presenterRequestClientPerosnBusiness(by params: Any?)
    func didEntityClientPerosnBusinessReceiveData(by params: Any?)
    func didEntityClientPerosnBusinessReceiveError(error: MyError?)
}

protocol ClientPerosnBusinessEntityProtocols: AnyObject {
    var interactor: ClientPerosnBusinessInteractorProtocols? { get set }
    
    func didClientPerosnBusinessReceiveData(by params: Any?)
    func didClientPerosnBusinessReceiveError(error: MyError?)
    func getClientPerosnBusinessRequest(by params: Any?)
}

protocol ClientPerosnBusinessRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
    func pushToAddPrice(from previousView: UIViewController)
}
