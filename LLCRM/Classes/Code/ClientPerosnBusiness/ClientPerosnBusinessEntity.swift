//
//  ClientPerosnBusinessEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnBusinessEntity: ClientPerosnBusinessEntityProtocols {
    var interactor: ClientPerosnBusinessInteractorProtocols?
    
    func didClientPerosnBusinessReceiveData(by params: Any?) {
        interactor?.didEntityClientPerosnBusinessReceiveData(by: params)
    }
    
    func didClientPerosnBusinessReceiveError(error: MyError?) {
        interactor?.didEntityClientPerosnBusinessReceiveError(error: error)
    }
    
    func getClientPerosnBusinessRequest(by params: Any?) {
        LLNetProvider.request(.getPersonBusiness(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didClientPerosnBusinessReceiveData(by:jsonData)
            case .failure(_):
                self.didClientPerosnBusinessReceiveError(error: .requestError)
            }
        }
    }

}
