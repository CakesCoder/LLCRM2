//
//  ClientPerosnBusinessModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/5.
//

import UIKit
import HandyJSON

class ClientPerosnBusinessModel: BaseModel {
    var accessoryList:[accessoryModel]?
    var amountUnit:String?
    var businessOpportunity:String?
    var clientClueContactSimpleParam:clientClueContactSimpleParam?
    var clientId:String?
    var clientName:String?
    var commercialExtraInfo:commercialExtraInfo?
    var commercialInfo:String?
    var commercialMoney:String?
    var commercialMoneyUnit:String?
    var commercialName:String?
    var commercialTimes:String?
    var createDate:String?
    var createDept:String?
    var createrId:String?
    var createrName:String?
    var customerDealStatus:String?
    var followerId:Int?
    var followerName:String?
    var id:String?
    var interventionStage:String?
    var joinPeople:String?
    var moneyUnit:String?
    var probability:String?
    var productAmount:String?
    var productDate:String?
    var productInfo:String?
    var productMoney:String?
    var productNameL:String?
    var productStage:Int?
    var productTypeName:String?
    var property:String?
    var result:String?
    var shareType:Int?
    var source:String?
    var stageName:String?
    var version:String?
    var winnerMoney:String?
    var stageVoList:stageVoList?
    required init() {}
}

class stageVoList: BaseModel{
    var companyId:Int?
    var deleted:Int?
    var id:Int?
    var orderNumber:Int?
    var stageName:String?
    var version:String?
    required init() {}
}

class commercialExtraInfo: BaseModel{
    var commercialId: Int?
    var competitor:String?
    var cooperateBuy:String?
    var goCompany:String?
    var id:String?
    var otherInfo:String?
    var publicist:String?
    var remark:String?
    var trackState:String?
    required init() {}
}

class clientClueContactSimpleParam: BaseModel{
    var address:String?
    var clientId:Int?
    var email:String?
    var gender:String?
    var id:String?
    var name:String?
    var phone:String?
    var position:String?
    required init() {}
}

class accessoryModel: BaseModel{
    var name: String?
    var url: String?
    required init() {}
}
