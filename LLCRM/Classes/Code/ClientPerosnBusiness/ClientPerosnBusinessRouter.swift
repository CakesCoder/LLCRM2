//
//  ClientPerosnBusinessRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnBusinessRouter: ClientPerosnBusinessRouterProtocols {
    func pushToAddPrice(from previousView: UIViewController){
        let nextView = AddPriceViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
