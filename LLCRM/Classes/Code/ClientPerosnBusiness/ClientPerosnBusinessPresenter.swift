//
//  ClientPerosnBusinessPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnBusinessPresenter: ClientPerosnBusinessPresenterProtocols {
    var view: ClientPerosnBusinessViewProtocols?
    
    var router: ClientPerosnBusinessRouterProtocols?
    
    var interactor: ClientPerosnBusinessInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetClientPerosnBusiness(by params: Any?) {
        interactor?.entity?.getClientPerosnBusinessRequest(by: params)
    }
    
    func didGetClientPerosnBusinessInteractorReceiveData(by params: Any?) {
        view?.didGetClientPerosnBusinessPresenterReceiveData(by: params)
    }
    
    func didGetClientPerosnBusinessInteractorReceiveError(error: MyError?) {
        view?.didGetClientPerosnBusinessPresenterReceiveError(error: error)
    }
    
    
}
