//
//  ClientPerosnBusinessInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnBusinessInteractor: ClientPerosnBusinessInteractorProtocols {
    var presenter: ClientPerosnBusinessPresenterProtocols?
    
    var entity: ClientPerosnBusinessEntityProtocols?
    
    func presenterRequestClientPerosnBusiness(by params: Any?) {
        entity?.getClientPerosnBusinessRequest(by: params)
    }
    
    func didEntityClientPerosnBusinessReceiveData(by params: Any?) {
        presenter?.didGetClientPerosnBusinessInteractorReceiveData(by: params)
    }
    
    func didEntityClientPerosnBusinessReceiveError(error: MyError?) {
        presenter?.didGetClientPerosnBusinessInteractorReceiveError(error: error)
    }
}
