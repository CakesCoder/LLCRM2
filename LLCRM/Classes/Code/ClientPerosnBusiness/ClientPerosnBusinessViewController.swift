//
//  ClientPerosnBusinessViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit
//import JXPagingView
import HandyJSON
import PKHUD

extension ClientPerosnBusinessViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ClientPerosnBusinessViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ClientPerosnBusinessViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var presenter: ClientPerosnBusinessPresenterProtocols?
    var pushAddPriceCallback: (() -> ())?
    var model:ClientDetailDataModel?
    var infosModel:ClientPerosnBusinessModel?
    var businessNameLabel:UILabel?
    var nameLabel:UILabel?
    var moneyLabel:UILabel?
    var commitDateLabel:UILabel?
    var mainLineLabel:UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "商机"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["商机名称：", "客户名称：", "商机金额（ 万元）：", "预计成交日期：", "主线索：", "进度："]
        
//        let contentInfos = ["切割-20190103184129", "零瓴软件技术(深圳)有限公司", "￥1,000.00", "2023-05-23", "—"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            if i == baseInfos.count-1{
                
                let progressView = UIView().then { obj in
                    v.addSubview(obj)
                    obj.layer.cornerRadius = 8
                    obj.backgroundColor = lineColor7
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(120)
                        make.centerY.equalToSuperview()
                        make.width.equalTo(100)
                        make.height.equalTo(10)
                    }
                }
                
                let progressContentView = UIView().then { obj in
                    v.addSubview(obj)
                    obj.layer.cornerRadius = 8
                    obj.backgroundColor = bgColor9
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(120)
                        make.centerY.equalToSuperview()
                        make.width.equalTo(0)
                        make.height.equalTo(10)
                    }
                }
                
                let progressHintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    obj.textColor = bgColor9
                    obj.font = kRegularFont(12)
                    obj.text = "0% "
                    obj.snp.makeConstraints { make in
                        make.left.equalTo(progressView.snp.right).offset(5)
                        make.centerY.equalToSuperview()
                    }
                }
                
            }else{
                let contentLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    obj.font = kMediumFont(12)
                    obj.textColor = blackTextColor81
                    if i == 0{
                        businessNameLabel = obj
                    }else if i == 1{
                        nameLabel = obj
                    }else if i == 2{
                        moneyLabel = obj
                    }else if i == 3{
                        commitDateLabel = obj
                    }else if i == 4{
                        obj.text = "--"
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(120)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        
        cofing()
    }
    
    func cofing(){
        let router = ClientPerosnBusinessRouter()
        
        presenter = ClientPerosnBusinessPresenter()
        
        presenter?.router = router
        
        let entity = ClientPerosnBusinessEntity()
        
        let interactor = ClientPerosnBusinessInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        printTest("asdasdasdasdasdasdads")
//        printTest(model?.id)
//        presenter?.presenterRequestGetClientPerosnBusiness(by: ["clientId": model?.id])
    }
    
    @objc func addClick(_ button:UIButton){
//        printTest("123123123123")
//        presenter?.router?.pushToAddPrice(from: self)
        self.pushAddPriceCallback?()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientPerosnBusinessViewController: ClientPerosnBusinessViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetClientPerosnBusinessPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if dataDic != nil{
                    if let model = JSONDeserializer<ClientPerosnBusinessModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                       infosModel = model
                       businessNameLabel?.text = infosModel?.commercialName ?? "--"
                       moneyLabel?.text = infosModel?.commercialMoneyUnit ?? "--"
                       commitDateLabel?.text = infosModel?.productDate ?? "--"
                       nameLabel?.text = infosModel?.createrName ?? "--"
                   }
                }else{
                    businessNameLabel?.text = "--"
                    moneyLabel?.text = "--"
                    commitDateLabel?.text = "--"
                    nameLabel?.text = "--"
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetClientPerosnBusinessPresenterReceiveError(error: MyError?) {
        
    }
    
    
}
