//
//  MarketDataEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class MarketDataEntity: MarketDataEntityProtocols {
    var interactor: MarketDataInteractorProtocols?
    
    func didMarketDataReceiveData(by params: Any?) {
        interactor?.didEntityMarketDataReceiveData(by: params)
    }
    
    func didMarketDataReceiveError(error: MyError?) {
        interactor?.didEntityMarketDataReceiveError(error: error)
    }
    
    func getMarketDataRequest(by params: Any?) {
        LLNetProvider.request(.marketData(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didMarketDataReceiveData(by:jsonData)
            case .failure(_):
                self.didMarketDataReceiveError(error: .requestError)
            }
        }
    }
    

}
