//
//  ShowMarketView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class ShowMarketModel:BaseModel{
    
    var name:String?
    var isSelected:Bool? = false
    
    required init() {}
}

class ShowMarketCell:UITableViewCell{
    
    var nameLabel:UILabel?
    var button:UIButton?
    var model:ShowMarketModel?{
        didSet{
            if model?.isSelected == true{
                button?.backgroundColor = .blue
                nameLabel?.textColor = bluebgColor
            }else{
                button?.backgroundColor = .red
                nameLabel?.textColor = blackTextColor13
            }
            
            nameLabel?.text = model?.name ?? "--"
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
//        self.backgroundColor = .blue
        
        button = UIButton().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        })
        
        nameLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor13
            obj.snp.makeConstraints { make in
                make.left.equalTo(button!.snp.right).offset(10)
                make.centerY.equalToSuperview()
            }
        })
        
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor29
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(40)
                make.right.equalToSuperview().offset(-40)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
}

class ShowMarketView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var selectedIndex:Int? = 0
    var dataList:[ShowMarketModel]? = []
    var closeBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 40+UIDevice.xp_navigationFullHeight(), width: kScreenWidth, height: kScreenHeight))
        
        for i in 0..<5{
            let model = ShowMarketModel()
            if i == 0{
                model.name = "销售漏斗"
                model.isSelected = true
            }else if i == 1{
                model.name = "柱形"
                model.isSelected = false
            }else if i == 2{
                model.name = "折线图"
                model.isSelected = false
            }else if i == 3{
                model.name = "饼图"
                model.isSelected = false
            }else if i == 4{
                model.name = "统计表"
                model.isSelected = false
            }
            dataList?.append(model)
        }
        
        build()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func build(){
        let v = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalToSuperview().offset(200)
            }
        }
        
        _ = UITableView().then({ obj in
            v.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(ShowMarketCell.self, forCellReuseIdentifier: "ShowMarketCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        _ = UIButton().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = blackTextColor16
            obj.alpha = 0.5
            obj.addTarget(self, action: #selector(closeClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(200)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc func closeClick(){
        closeBlock?()
    }
}

extension ShowMarketView:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList?.count ?? 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowMarketCell", for: indexPath) as! ShowMarketCell
        cell.model = dataList?[indexPath.row] as! ShowMarketModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<(dataList?.count ?? 0){
            let model = dataList?[i] as! ShowMarketModel
            model.isSelected = false
        }
        let model = dataList?[indexPath.row] as! ShowMarketModel
        model.isSelected = true
        tableView.reloadData()
        self.isHidden = true
    }
}
