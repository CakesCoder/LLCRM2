//
//  ShowMarketSceneView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class ShowMarketSceneView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var closeBlock: (() -> ())?
    var dataList:[ShowMarketModel]? = []
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 40+UIDevice.xp_navigationFullHeight(), width: kScreenWidth, height: kScreenHeight))
//        self.isOpaque = false
//        self.backgroundColor = .clear
        buildUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        for i in 0..<7{
            let model = ShowMarketModel()
            if i == 0{
                model.name = "全部"
                model.isSelected = true
            }else if i == 1{
                model.name = "我负责的"
                model.isSelected = false
            }else if i == 2{
                model.name = "我参与的"
                model.isSelected = false
            }else if i == 3{
                model.name = "我负责部门的"
                model.isSelected = false
            }else if i == 4{
                model.name = "我下属参与的"
                model.isSelected = false
            }else if i == 5{
                model.name = "我下属负责的"
                model.isSelected = false
            }else if i == 6{
                model.name = "共享给我的"
                model.isSelected = false
            }
            dataList?.append(model)
        }
        
        
        
        let v = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalToSuperview().offset(500)
            }
        }
        
        _ = UITableView().then({ obj in
            v.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(ShowMarketCell.self, forCellReuseIdentifier: "ShowMarketCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(100)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(450)
            }
        })
        
        let leftView = UIView().then { obj in
            v.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.height.equalTo(450)
                make.width.equalTo(100)
            }
        }
        
        let thmeLabel = UILabel().then { obj in
            leftView.addSubview(obj)
            obj.backgroundColor = .white
            obj.textAlignment = .center
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor13
            obj.text = "筛选场景"
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.height.equalTo(40)
                make.right.equalToSuperview().offset(-0)
            }
        }
        
        let commitButton = UIButton().then { obj in
            v.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = bluebgColor
            obj.setTitle("完成", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kRegularFont(11)
//            obj.setTitleColor(blackTextColor, for: .normal)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(465)
                make.right.equalToSuperview().offset(-15)
                make.width.equalTo(70)
                make.height.equalTo(30)
            }
        }
        
        _ = UIButton().then { obj in
            v.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = .white
            obj.setTitle("重置", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kRegularFont(11)
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 0.5
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.layer.borderColor = lineColor14.cgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(465)
                make.right.equalToSuperview().offset(-100)
                make.width.equalTo(70)
                make.height.equalTo(30)
            }

        }
        
        _ = UIButton().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = blackTextColor16
            obj.alpha = 0.5
            obj.addTarget(self, action: #selector(closeClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(500)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc func closeClick(){
        closeBlock?()
    }
}

extension ShowMarketSceneView:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList?.count ?? 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowMarketCell", for: indexPath) as! ShowMarketCell
        cell.model = dataList?[indexPath.row] as! ShowMarketModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<(dataList?.count ?? 0){
            let model = dataList?[i] as! ShowMarketModel
            model.isSelected = false
        }
        let model = dataList?[indexPath.row] as! ShowMarketModel
        model.isSelected = true
        tableView.reloadData()
//        self.isHidden = true
    }
}

