//
//  MarketDataProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/14.
//

import UIKit

protocol MarketDataViewProtocols: AnyObject {
    var presenter: MarketDataPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetMarketDataPresenterReceiveData(by params: Any?)
    func didGetMarketDataPresenterReceiveError(error: MyError?)
}

protocol MarketDataPresenterProtocols: AnyObject{
    var view: MarketDataViewProtocols? { get set }
    var router: MarketDataRouterProtocols? { get set }
    var interactor: MarketDataInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetMarketData(by params: Any?)
    func didGetMarketDataInteractorReceiveData(by params: Any?)
    func didGetMarketDataInteractorReceiveError(error: MyError?)
}

protocol MarketDataInteractorProtocols: AnyObject {
    var presenter: MarketDataPresenterProtocols? { get set }
    var entity: MarketDataEntityProtocols? { get set }
    
    func presenterRequestMarketData(by params: Any?)
    func didEntityMarketDataReceiveData(by params: Any?)
    func didEntityMarketDataReceiveError(error: MyError?)
}

protocol MarketDataEntityProtocols: AnyObject {
    var interactor: MarketDataInteractorProtocols? { get set }
    
    func didMarketDataReceiveData(by params: Any?)
    func didMarketDataReceiveError(error: MyError?)
    func getMarketDataRequest(by params: Any?)
}

protocol MarketDataRouterProtocols: AnyObject {
    func pushToAddd(from previousView: UIViewController) 
}



