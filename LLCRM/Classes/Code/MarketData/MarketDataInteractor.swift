//
//  MarketDataInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class MarketDataInteractor: MarketDataInteractorProtocols {
    var presenter: MarketDataPresenterProtocols?
    
    var entity: MarketDataEntityProtocols?
    
    func presenterRequestMarketData(by params: Any?) {
        entity?.getMarketDataRequest(by: params)
    }
    
    func didEntityMarketDataReceiveData(by params: Any?) {
        presenter?.didGetMarketDataInteractorReceiveData(by: params)
    }
    
    func didEntityMarketDataReceiveError(error: MyError?) {
        presenter?.didGetMarketDataInteractorReceiveError(error: error)
    }
    

}
