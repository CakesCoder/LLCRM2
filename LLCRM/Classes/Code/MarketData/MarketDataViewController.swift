//
//  MarketDataViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/14.
//

import UIKit
//import AAInfographics
import HandyJSON
import PKHUD

class MarketDataViewController: BaseViewController {
    var aaChartView: AAChartView?
    var presenter: MarketDataPresenterProtocols?
    var colorList:[String]? = ["#3082FF", "#5EB8FE", "#16B5AE", "#54CEC7", "#36C777", "#71DBA1", "#9ACC50", "#B4DE88","#FCD232", "#5EB8FA", "#71DBA5", "#36C787"]
    var headView:UIView?
    var showView:ShowMarketView?
    var showView2:ShowMarketSceneView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "销售漏斗（商机金额）"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MarketDataViewController.backClick))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.createAddBarbuttonItem(name: "+", target: self, action: #selector(MarketDataViewController.addClick))
        
        let titles = ["场景", "数据范围", "报表类型"]
        
        let titleView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(40)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
            }
        }
        
        let w = kScreenWidth/3.0
        
        for i in 0..<titles.count{
            let button = UIButton().then { obj in
                titleView.addSubview(obj)
                obj.titleLabel?.font = kMediumFont(12)
                obj.setTitleColor(blackTextColor38, for: .normal)
                obj.setTitle(titles[i], for: .normal)
                obj.tag = 1000+i
                obj.addTarget(self, action: #selector(titleClick(_:)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(CGFloat(i)*w)
                    make.top.equalToSuperview().offset(0)
                    make.width.equalTo(w)
                    make.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        _ = UIView().then({ obj in
            titleView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 750)
            obj.backgroundColor = .white
        }
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 470
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 15,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        // 设置 aaChartView 的内容高度(content height)
        headView!.addSubview(aaChartView!)
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView!
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(titleView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        let bottomView = UIView().then({ obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight())
            }
        })
        
        
        let button = UIButton().then({ obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.top.equalToSuperview().offset(5)
                make.width.equalTo(15)
                make.height.equalTo(15)
            }
        })
        
        _ = UILabel().then({ obj in
            bottomView.addSubview(obj)
            obj.text = "显示指标为0的维度"
            obj.font = kRegularFont(10)
            obj.textColor = blackTextColor88
            obj.snp.makeConstraints { make in
                make.left.equalTo(button.snp.right).offset(5)
                make.centerY.equalTo(button)
            }
        })
        
        _ = UIButton().then({ obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-20)
                make.centerY.equalTo(button)
                make.width.height.equalTo(18)
            }
        })
        
        showView = ShowMarketView()
        showView?.isHidden = true
        showView?.closeBlock = {[weak self] in
            self?.showView?.isHidden = true
        }
        view.addSubview(showView!)
        
        showView2 = ShowMarketSceneView()
        view.addSubview(showView2!)
        showView2?.isHidden = true
        showView2?.closeBlock = {[weak self] in
            self?.showView2?.isHidden = true
        }
        cofing()
    }
    
    @objc func addClick(){
        presenter?.router?.pushToAddd(from: self)
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func titleClick(_ button:UIButton){
        if button.tag == 1000{
            self.showView2?.isHidden = !self.showView2!.isHidden
        }else if button.tag == 1001{
            
        }else{
            self.showView?.isHidden = !self.showView!.isHidden
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if self.showView?.isHidden == false{
            self.showView?.isHidden = true
        }
        if self .showView2?.isHidden == false{
            self.showView2?.isHidden = true
        }
    }
    
    func cofing(){
        let router = MarketDataRouter()
        
        presenter = MarketDataPresenter()
        
        presenter?.router = router
        
        let entity =  MarketDataEntity()
        
        let interactor =  MarketDataInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MarketDataViewController:MarketDataViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetMarketDataPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if dataDic != nil{
                    if let model = JSONDeserializer<MarkDataModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                        if model.personSaleList != nil{
                            var dataList = [] as! [Any]
                            for i in 0..<model.personSaleList!.count{
                                let personSaleModel = model.personSaleList?[i] as! personSaleListModel
                                var dataInfos:[Any] = []
                                dataInfos.append(personSaleModel.saleMonth)
                                let money = Float(personSaleModel.realityMoney!)
                                dataInfos.append(money)
                                dataList.append(dataInfos)
                            }
                            let chartModel = AAChartModel()
                                .chartType(.funnel)
                                .yAxisVisible(true)
                                .margin(right: kScreenWidth/2.0)
                                .series([
                                    AASeriesElement()
                                        .colors(colorList!)
                                        .dataLabels(AADataLabels()
                                            .x(1)
                                            .enabled(false)
                                            .verticalAlign(.middle)
                                            .color(AAColor.black)
                                            .softConnector(false)
                                            .style(AAStyle()
                                                .fontSize(12)
                                            )
                                            )

                                    .data([
                                        dataList
                                    ])
                                    .toDic()!,
                                ])
                            
                            /*图表视图对象调用图表模型对象,绘制最终图形*/
                            aaChartView?.aa_drawChartWithChartModel(chartModel)
                            
                            if  model.personSaleList!.count > 12{
                                for j in 0..<(self.colorList?.count ?? 0){
//                                    let index = i as! Int
                                    let view = UIView().then { obj in
                                        headView?.addSubview(obj)
                                        obj.backgroundColor = UIColor(hex:colorList?[j] ?? "")
                                        obj.snp.makeConstraints { make in
                                            make.left.equalToSuperview().offset(kScreenWidth/2.0 + 20)
                                            make.top.equalToSuperview().offset(35+30*j)
                                            make.width.equalTo(20)
                                            make.height.equalTo(10)
                                        }
                                    }
                                    
                                    let texts = dataList[j] as? [Any]
                                    _ = UILabel().then({ obj in
                                        headView?.addSubview(obj)
                                        obj.text = texts?[0] as? String
                                        obj.textColor = .black
                                        obj.font = kRegularFont(0)
                                        obj.snp.makeConstraints { make in
                                            make.left.equalTo(view.snp.right).offset(10)
                                            make.centerY.equalTo(view)
                                        }
                                    })
                                }
                            }else{
                                for j in 0..<(model.personSaleList?.count ?? 0){
                                    let view = UIView().then { obj in
                                        headView?.addSubview(obj)
                                        obj.backgroundColor = UIColor(hex:colorList?[j] ?? "")
                                        obj.snp.makeConstraints { make in
                                            make.left.equalToSuperview().offset(kScreenWidth/2.0 + 20)
                                            make.top.equalToSuperview().offset(35+30*j)
                                            make.width.equalTo(20)
                                            make.height.equalTo(10)
                                        }
                                    }
                                    
                                    let texts = dataList[j] as? [Any]
                                    _ = UILabel().then({ obj in
                                        headView?.addSubview(obj)
                                        obj.text = texts?[0] as? String
                                        obj.textColor = .black
                                        obj.font = kRegularFont(0)
                                        obj.snp.makeConstraints { make in
                                            make.left.equalTo(view.snp.right).offset(10)
                                            make.centerY.equalTo(view)
                                        }
                                    })
                                }
                            }
                            
                            let infos = ["新增客户", "新增商机", "新增联系人", "新增报价", "新增客户拜访", "总额"]
                            
                            let infosView = UIView().then { obj in
                                headView?.addSubview(obj)
                                obj.layer.cornerRadius = 2
                                obj.layer.borderWidth = 0.5
                                obj.layer.borderColor = lineColor5.cgColor
    //                            obj.backgroundColor = .red
                                obj.snp.makeConstraints { make in
                                    make.top.equalToSuperview().offset(475+15)
                                    make.left.equalToSuperview().offset(20)
                                    make.right.equalToSuperview().offset(-20)
                                    make.height.equalTo(6*30)
                                }
                            }
                            
                            for i in 0..<infos.count{
                                let v = UIView().then { obj in
                                    infosView.addSubview(obj)
                                    obj.snp.makeConstraints { make in
                                        make.left.equalToSuperview().offset(0)
                                        make.top.equalToSuperview().offset(30*i)
                                        make.height.equalTo(30)
                                        make.right.equalToSuperview().offset(-0)
                                    }
                                }
                                
                                let hintLabel = UILabel().then { obj in
                                    v.addSubview(obj)
                                    obj.text = infos[i]
                                    obj.textAlignment = .center
                                    obj.font = kRegularFont(10)
                                    obj.textColor = blackTextColor11
                                    obj.snp.makeConstraints { make in
                                        make.left.top.equalToSuperview().offset(0)
                                        make.bottom.equalToSuperview().offset(-0)
                                        make.width.equalTo(100)
                                    }
                                }
                                
                                _ = UIView().then({ obj in
                                    v.addSubview(obj)
                                    obj.backgroundColor = lineColor5
                                    obj.snp.makeConstraints { make in
                                        make.left.equalTo(hintLabel.snp.right).offset(0)
                                        make.top.equalToSuperview().offset(0)
                                        make.bottom.equalToSuperview().offset(-0)
                                        make.width.equalTo(0.5)
                                    }
                                })
                                
                                let contentLabel = UILabel().then { obj in
                                    v.addSubview(obj)
                                    obj.textAlignment = .center
                                    obj.font = kRegularFont(10)
                                    obj.textColor = blackTextColor11
                                    if i == 0{
                                        obj.text = model.clientCount ?? "--"
                                    }else if i == 1{
                                        obj.text = model.commercialCount ?? "--"
                                    }else if i == 2{
                                        obj.text = model.contactCount ?? "--"
                                    }else if i == 3{
                                        obj.text = model.quotationCount ?? "--"
                                    }else if i == 4{
                                        obj.text = model.visitCount ?? "--"
                                    }else if i == 5{
                                        obj.text = model.total ?? "--"
                                    }
                                    obj.snp.makeConstraints { make in
                                        make.left.equalToSuperview().offset(101)
                                        make.top.equalToSuperview().offset(0)
                                        make.bottom.right.equalToSuperview().offset(-0)
                                    }
                                }
                            }
                        }else{
                            let chartModel = AAChartModel()
                                .chartType(.funnel)
                                .yAxisVisible(true)
                                .margin(right: kScreenWidth/2.0)
                                .series([
                                    AASeriesElement()
                                        .colors(colorList!)
                                        .dataLabels(AADataLabels()
                                            .x(1)
                                            .enabled(false)
                                            .verticalAlign(.middle)
                                            .color(AAColor.black)
                                            .softConnector(false)
                                            .style(AAStyle()
                                                .fontSize(12)
                                            )
                                            )

                                    .data([
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                        ["",1] as [Any],
                                    ])
                                    .toDic()!,
                                ])
                            
                            /*图表视图对象调用图表模型对象,绘制最终图形*/
                            aaChartView?.aa_drawChartWithChartModel(chartModel)
                            
                            for i in 0..<(self.colorList?.count ?? 0){
                                let view = UIView().then { obj in
                                    headView?.addSubview(obj)
                                    obj.backgroundColor = UIColor(hex:colorList?[i] ?? "")
                                    obj.snp.makeConstraints { make in
                                        make.left.equalToSuperview().offset(kScreenWidth/2.0 + 20)
                                        make.top.equalToSuperview().offset(35+30*i)
                                        make.width.equalTo(20)
                                        make.height.equalTo(10)
                                    }
                                }
                                
                                _ = UILabel().then({ obj in
                                    headView?.addSubview(obj)
                                    obj.text = "--"
                                    obj.textColor = .black
                                    obj.font = kRegularFont(0)
                                    obj.snp.makeConstraints { make in
                                        make.left.equalTo(view.snp.right).offset(10)
                                        make.centerY.equalTo(view)
                                    }
                                })
                            }
                        }
                        
                        let infos = ["新增客户", "新增商机", "新增联系人", "新增报价", "新增客户拜访", "总额"]
                        
                        let infosView = UIView().then { obj in
                            headView?.addSubview(obj)
                            obj.layer.cornerRadius = 2
                            obj.layer.borderWidth = 0.5
                            obj.layer.borderColor = lineColor5.cgColor
//                            obj.backgroundColor = .red
                            obj.snp.makeConstraints { make in
                                make.top.equalToSuperview().offset(475+15)
                                make.left.equalToSuperview().offset(20)
                                make.right.equalToSuperview().offset(-20)
                                make.height.equalTo(6*30)
                            }
                        }
                        
                        for i in 0..<infos.count{
                            let v = UIView().then { obj in
                                infosView.addSubview(obj)
                                obj.snp.makeConstraints { make in
                                    make.left.equalToSuperview().offset(0)
                                    make.top.equalToSuperview().offset(30*i)
                                    make.height.equalTo(30)
                                    make.right.equalToSuperview().offset(-0)
                                }
                            }
                            
                            let hintLabel = UILabel().then { obj in
                                v.addSubview(obj)
                                obj.text = infos[i]
                                obj.textAlignment = .center
                                obj.font = kRegularFont(10)
                                obj.textColor = blackTextColor11
                                obj.snp.makeConstraints { make in
                                    make.left.top.equalToSuperview().offset(0)
                                    make.bottom.equalToSuperview().offset(-0)
                                    make.width.equalTo(100)
                                }
                            }
                            
                            _ = UIView().then({ obj in
                                v.addSubview(obj)
                                obj.backgroundColor = lineColor5
                                obj.snp.makeConstraints { make in
                                    make.left.equalTo(hintLabel.snp.right).offset(0)
                                    make.top.equalToSuperview().offset(0)
                                    make.bottom.equalToSuperview().offset(-0)
                                    make.width.equalTo(0.5)
                                }
                            })
                            
                            let contentLabel = UILabel().then { obj in
                                v.addSubview(obj)
                                obj.textAlignment = .center
                                obj.font = kRegularFont(10)
                                obj.textColor = blackTextColor11
//                                if i == 0{
                                    obj.text = "--"
//                                }else if i == 1{
//
//                                }else if i == 2{
//
//                                }else if i == 3{
//
//                                }else if i == 4{
//
//                                }else if i == 5{
//
//                                }
                                obj.snp.makeConstraints { make in
                                    make.left.equalToSuperview().offset(101)
                                    make.top.equalToSuperview().offset(0)
                                    make.bottom.right.equalToSuperview().offset(-0)
                                }
                            }
                        }
                        
                    }
                }else{
                    let chartModel = AAChartModel()
                        .chartType(.funnel)
                        .yAxisVisible(true)
                        .margin(right: kScreenWidth/2.0)
                        .series([
                            AASeriesElement()
                                .colors(colorList!)
                                .dataLabels(AADataLabels()
                                    .x(1)
                                    .enabled(false)
                                    .verticalAlign(.middle)
                                    .color(AAColor.black)
                                    .softConnector(false)
                                    .style(AAStyle()
                                        .fontSize(12)
                                    )
                                    )

                            .data([
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                                ["",1] as [Any],
                            ])
                            .toDic()!,
                        ])
                    
                    /*图表视图对象调用图表模型对象,绘制最终图形*/
                    aaChartView?.aa_drawChartWithChartModel(chartModel)
                    
                    for i in 0..<(self.colorList?.count ?? 0){
                        let view = UIView().then { obj in
                            headView?.addSubview(obj)
                            obj.backgroundColor = UIColor(hex: self.colorList?[i] ?? "")
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(kScreenWidth/2.0 + 20)
                                make.top.equalToSuperview().offset(35+30*i)
                                make.width.equalTo(20)
                                make.height.equalTo(10)
                            }
                        }
                        
                        _ = UILabel().then({ obj in
                            headView?.addSubview(obj)
                            obj.text = "--"
                            obj.textColor = .black
                            obj.font = kRegularFont(0)
                            obj.snp.makeConstraints { make in
                                make.left.equalTo(view.snp.right).offset(10)
                                make.centerY.equalTo(view)
                            }
                        })
                    }
                    
                    let infos = ["新增客户", "新增商机", "新增联系人", "新增报价", "新增客户拜访", "总额"]
                    
                    let infosView = UIView().then { obj in
                        headView?.addSubview(obj)
                        obj.layer.cornerRadius = 2
                        obj.layer.borderWidth = 0.5
                        obj.layer.borderColor = lineColor5.cgColor
//                            obj.backgroundColor = .red
                        obj.snp.makeConstraints { make in
                            make.top.equalToSuperview().offset(475+15)
                            make.left.equalToSuperview().offset(20)
                            make.right.equalToSuperview().offset(-20)
                            make.height.equalTo(6*30)
                        }
                    }
                    
                    for i in 0..<infos.count{
                        let v = UIView().then { obj in
                            infosView.addSubview(obj)
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(0)
                                make.top.equalToSuperview().offset(30*i)
                                make.height.equalTo(30)
                                make.right.equalToSuperview().offset(-0)
                            }
                        }
                        
                        let hintLabel = UILabel().then { obj in
                            v.addSubview(obj)
                            obj.text = infos[i]
                            obj.textAlignment = .center
                            obj.font = kRegularFont(10)
                            obj.textColor = blackTextColor11
                            obj.snp.makeConstraints { make in
                                make.left.top.equalToSuperview().offset(0)
                                make.bottom.equalToSuperview().offset(-0)
                                make.width.equalTo(100)
                            }
                        }
                        
                        _ = UIView().then({ obj in
                            v.addSubview(obj)
                            obj.backgroundColor = lineColor5
                            obj.snp.makeConstraints { make in
                                make.left.equalTo(hintLabel.snp.right).offset(0)
                                make.top.equalToSuperview().offset(0)
                                make.bottom.equalToSuperview().offset(-0)
                                make.width.equalTo(0.5)
                            }
                        })
                        
                        let contentLabel = UILabel().then { obj in
                            v.addSubview(obj)
                            obj.textAlignment = .center
                            obj.font = kRegularFont(10)
                            obj.textColor = blackTextColor11
//                                if i == 0{
                                obj.text = "--"
//                                }else if i == 1{
//
//                                }else if i == 2{
//
//                                }else if i == 3{
//
//                                }else if i == 4{
//
//                                }else if i == 5{
//
//                                }
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(101)
                                make.top.equalToSuperview().offset(0)
                                make.bottom.right.equalToSuperview().offset(-0)
                            }
                        }
                    }
                    
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetMarketDataPresenterReceiveError(error: MyError?) {
        
    }
    
    
}
