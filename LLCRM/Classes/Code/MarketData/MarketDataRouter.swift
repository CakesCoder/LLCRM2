//
//  MarketDataRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class MarketDataRouter: MarketDataRouterProtocols {
    func pushToAddd(from previousView: UIViewController) {
        let nextView = MarketDataStatusViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
