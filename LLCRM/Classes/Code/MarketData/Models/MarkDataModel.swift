//
//  MarkDataModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit
import HandyJSON

class MarkDataModel: BaseModel {
    var clientCount:String?
    var commercialCount:String?
    var commercialMoney:[commercialMoneyModel]?
    var contactCount:String?
    var personSaleList:[personSaleListModel]?
    var quotationCount:String?
    var visitCount:String?
    var total:String?
    var teamSaleList:[teamSaleListModel]?
    
    required init() {}
}

class teamSaleListModel:BaseModel{
    var decomposeWay:Int?
    var diffMoney:Int?
    var id:String?
    var radio:String?
    var rate:String?
    var realityMoney:String?
    var saleMonth:Int?
    var salePersonId:Int?
    var salePersonName:String?
    var saleTargetId:Int?
    var saleTargetMoney:String?

    required init() {}
}

class commercialMoneyModel: BaseModel{
    var money:String?
    var name:String?
    required init() {}
}

class personSaleListModel: BaseModel{
    var decomposeWay:Int?
    var diffMoney:String?
    var id:String?
    var radio:String?
    var rate:String?
    var realityMoney:String?
    var saleMonth:String?
    var salePersonId:Int?
    var salePersonName:String?
    var saleTargetId:Int?
    var saleTargetMoney:String?
    
    required init() {}
}
