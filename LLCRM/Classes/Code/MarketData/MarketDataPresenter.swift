//
//  MarketDataPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class MarketDataPresenter: MarketDataPresenterProtocols {
    var view: MarketDataViewProtocols?
    
    var router: MarketDataRouterProtocols?
    
    var interactor: MarketDataInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = dateFormatter.string(from: now)
        presenterRequestGetMarketData(by: ["dateTime":dateString])
    }
    
    func presenterRequestGetMarketData(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestMarketData(by: params)
    }
    
    func didGetMarketDataInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetMarketDataPresenterReceiveData(by: params)
    }
    
    func didGetMarketDataInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetMarketDataPresenterReceiveError(error: error)
    }
    

}
