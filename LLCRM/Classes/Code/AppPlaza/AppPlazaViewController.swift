//
//  AppPlazaViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/22.
//

import UIKit

class AppPlazaViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "应用广场"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AppPlazaViewController.backClick))
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let names = ["CRM", "网盘", "营销通", "网盘", "员工积分系统", "纷享福利社", "假期管理", "培训助手"]
        let hints = ["客户全生命周期、数字化管理解……", "企业专属的公共网盘与个人网盘", "营销通是纷享销客为企业提供的……", "企业专属的公共网盘与个人网盘", "通过员工积分系统，管理员可以……", "纷享福利社模块中，会定期为纷……", "企业专属的公共网盘与个人网盘", "企业专属的公共网盘与个人网盘"]
        for i in 0..<8{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: i*70, width: Int(kScreenWidth), height: 70)
            }
            
            let imageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.image = UIImage(named: "app_\(i+1)")
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let nameLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.text = names[i]
                obj.font = kMediumFont(16)
                obj.textColor = blackTextColor67
                obj.snp.makeConstraints { make in
                    make.left.equalTo(imageV.snp.right).offset(13)
                    make.top.equalToSuperview().offset(13)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.textColor = blackTextColor45
                obj.font = kMediumFont(12)
                obj.text = hints[i]
                obj.snp.makeConstraints { make in
                    make.left.equalTo(imageV.snp.right).offset(13)
                    make.bottom.equalToSuperview().offset(-13)
                }
            }
            
            let focusButton = UIButton().then { obj in
                v.addSubview(obj)
                obj.setTitle("添加", for: .normal)
                obj.titleLabel?.font = kMediumFont(11)
                obj.backgroundColor = bluebgColor
                obj.layer.cornerRadius = 2
                obj.addTarget(self, action: #selector(focusClick(_:)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(52)
                    make.height.equalTo(26)
                }
            }
            
            _ = UIView().then({ obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(73)
                    make.bottom.equalToSuperview().offset(-0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc func focusClick(_ button:UIButton){
        button.isSelected = !button.isSelected
        
        if button.isSelected{
            button.backgroundColor = lineColor6
            button.setTitleColor(blackTextColor75, for: .normal)
            button.setTitle("已添加", for: .normal)
        }else{
            button.backgroundColor = bluebgColor
            button.setTitleColor(.white, for: .normal)
            button.setTitle("添加", for: .normal)
        }
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
