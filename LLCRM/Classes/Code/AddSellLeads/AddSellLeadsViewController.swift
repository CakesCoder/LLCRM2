//
//  AddSellLeadsViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit
import YYText_swift
import PKHUD
import HandyJSON

class AddSellLeadsViewController: BaseViewController, MyDateViewDelegate {
    
    var tableView: UITableView?
    var normalHeadView: UIView?
    var simpHeadView: UIView?
    var isSimp:Bool? = false
    var showSwitch:UISwitch?
    
    var presenter: AddSellLeadsPresenterProtocols?
    
    var companyDateView: MyDateView!
    var clientTextFiled: UITextField?
//    var sourcceTextFiled2: UITextField?
    
    var addressTextField: UITextField?
//    var lastTimeTextField: UITextField?
    var creatTimeTextFiled:UITextField?
    
//    var linkManModel: LinkManModel?
    var clientModel: MyClientModelData?
    
//    let baseInfos = [["*","客户名称："],["隶属集团："],["所属公海："],["客户级别："],["属性："],["来源："],["产业："],["隶属行业："],["规模："]]
    
    //客户名称
    var clientName:String? = ""
    var clientName2:String? = ""
    //隶属集团
    var clientSoureGroup:String? = ""
    //所属公海
    var sourceHighSeas:String? = ""
    //客户级别
    var clientLeave:String? = ""
    //属性
    var clientBelongg:String? = ""
    //来源
    var clientSource:String? = ""
    //产业
    var clientProduct:String? = ""
    //隶属行业
    var clientInddustry:String? = ""
    //规模
    var clientScale:String? = ""
    
    var sourceHighSeasModel:AddClientSelectedModel?
    var sourceHighSeasTextFiled:UITextField?
    var clientLeaveModel:AddClientSelectedModel?
    var clientLeaveTextFiled:UITextField?
    var clientBelonggModel:AddClientSelectedModel?
    var clientBelonggTextFiled:UITextField?
    var clientSourceModel:AddClientSelectedModel?
    var clientSourceTextFiled:UITextField?
    var clientProductModel:AddClientSelectedModel?
    var clientProductTextFiled:UITextField?
    var clientInddustryModel:AddClientSelectedModel?
    var clientInddustryTextFiled:UITextField?
    var clientScaleModel:AddClientSelectedModel?
    var clientScaleTextFiled:UITextField?
    
//    ["法定代表人：", "经营状态：", "注册资本：", "统一社会信用代码：", "工商注册号：", "组织机构代码：", "成立日期：", "企业类型：","营业期限：", "纳税人资质：", "所属行业：", "所属地区：", "登记机关：", "人员规模：","英文名：", "注册地址："]
    
    //法定代表人
    var clientLegal:String? = ""
    var clientLegalTextFiled:UITextField?
    //经营状态
    var clientStatus:String? = ""
    var clientStatusTextFiled:UITextField?
    //注册资本
    var registerMoney:String? = ""
    var registerMoneyTextFiled:UITextField?
    //统一社会信用代码
    var clientCode:String? = ""
    var clientCodeTextFiled:UITextField?
    //工商注册号
    var businessAccount:String? = ""
    var businessAccountTextFiled:UITextField?
    //组织机构代码
    var organizationCode:String? = ""
    var organizationCodeTextFiled:UITextField?
    //成立日期
    var companyCreatTime:String? = ""
    var companyCreatTimeTextFiled:UITextField?
    //企业类型
    var companyType:String? = ""
    var companyTypeTextFiled:UITextField?
    //营业期限
    var dateEnd:String? = ""
    var dateEndTextFiled:UITextField?
    //纳税人资质
    var clientCertification:String? = ""
    var clientCertificationTextFiled:UITextField?
    //所属行业
    var belongIndustry:String? = ""
    var belongIndustryTextFiled:UITextField?
    //所属地区
    var positionStr:String? = ""
    var positionStrTextFiled:UITextField?
    //登记机关
    var clientUnits:String? = ""
    var clientUnitsTextFiled:UITextField?
    //详细地址
    var detailLocationStr:String? = ""
    var detailLocationStrTextFiled:UITextField?
    //人员规模
    var personSccale:String? = ""
    var personSccaleTextfiled:UITextField?
    //英文名
    var englishName:String? = ""
    var englishNameTextFiled:UITextField?
    //注册地址
    var registerLocation:String? = ""
    var registerLocationTextFiled:UITextField?
    //定位
    var localStr:String? = ""
    //城市
    var addressStr:String? = ""
    //详细地址
    var addressDetail:String? = ""
    //提交后处理类型
    var commitDidStatus:Int? = 0
    //省
    var provinceStr:String? = ""
    //市
    var cityStr:String? = ""
    //区
    var areaStr:String? = ""
    
    //  选中哪个日期
    var dateSelectedIndex:Int? = 0
    
    var requestModel:AddSellLeadsModel?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "添加线索"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddSellLeadsViewController.backClick))

        simpHeadView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = bgColor
        }
        
        let simpbaseInfosView = UIView().then { obj in
            simpHeadView!.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(595)
            }
        }
        
        let simpbaseInfosHeadView = UIView().then { obj in
            simpbaseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(45)
            }
        }
        
        let simpheadTagView = UIView().then { obj in
            simpbaseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let simpheadLabel = UILabel().then { obj in
            simpbaseInfosHeadView.addSubview(obj)
            obj.text = "基本信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(simpheadTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let simpbaseInfos = [["*","客户名称："]]
        let simpbaseInfoPlaceholders = ["请输入"]
        
        for i in 0..<simpbaseInfos.count{
            let v = UIView().then { obj in
                simpbaseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(55+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<simpbaseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:simpbaseInfos[i][j])
                    if simpbaseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = simpbaseInfoPlaceholders[i]
                obj.font = kMediumFont(12)
                obj.tag = 2000+i
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-30)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i < 3 || i == 10 || i == 6{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.isUserInteractionEnabled = true
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
            }
            
            if i == 2{
                _ = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.addTarget(self, action: #selector(mainLineManClick), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.bottom.right.equalToSuperview().offset(-0)
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        normalHeadView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 1365)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "基本信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = [["*","客户名称："],["隶属集团："],["所属公海："],["客户级别："],["属性："],["来源："],["产业："],["隶属行业："],["规模："]]
        let baseInfoPlaceholders = ["请输入","请输入", "请选择", "请选择","请选择", "请选择", "请选择", "请选择", "请选择"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(55+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
//            if i == 3{
//                mainLinkManTextFiled2 = UITextField().then { obj in
//                    v.addSubview(obj)
//                    obj.placeholder = baseInfoPlaceholders[i]
//                    obj.font = kMediumFont(12)
//                    obj.tag = 1000+i
//                    obj.delegate = self
//                    obj.isEnabled = false
//                    obj.snp.makeConstraints { make in
//                        make.right.equalToSuperview().offset(-30)
//                        make.centerY.equalToSuperview()
//                    }
//                }
//            }else{
                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = baseInfoPlaceholders[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    obj.delegate = self
                    if i == 0{
                        clientTextFiled = obj
                        if let userInfosName = LLUserDefaultsManager.shared.readUserNameInfos(){
                            clientTextFiled?.text  = userInfosName
                            clientCode = userInfosName
                            clientName = userInfosName
                        }
                    }else if i == 2{
                        obj.isEnabled = false
                        sourceHighSeasTextFiled = obj
                    }else if i == 3{
                        obj.isEnabled = false
                        clientLeaveTextFiled = obj
                    }else if i == 4{
                        obj.isEnabled = false
                        clientBelonggTextFiled = obj
                    }else if i == 5{
                        obj.isEnabled = false
                        clientSourceTextFiled = obj
                    }else if i == 6{
                        obj.isEnabled = false
                        clientProductTextFiled = obj
                    }else if i == 7{
                        obj.isEnabled = false
                        clientInddustryTextFiled = obj
                    }else if i == 8{
                        obj.isEnabled = false
                        clientScaleTextFiled = obj
                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-30)
                        make.centerY.equalToSuperview()
                    }
                }
            
            if (i > 1){
                let button = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.tag = 1000+i
                    obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.bottom.right.equalToSuperview().offset(-0)
                    }
                }
            }
        
            
            if i > 1{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.isUserInteractionEnabled = true
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let businessList = ["法定代表人：", "经营状态：", "注册资本：", "统一社会信用代码：", "工商注册号：", "组织机构代码：", "成立日期：", "企业类型：","营业期限：", "纳税人资质：", "所属行业：", "所属地区：", "登记机关：", "人员规模：","英文名：", "注册地址："]
        
        let businessView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(baseInfosView.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo((businessList.count+1) * 45)
            }
        }
        
        let businessHeadView = UIView().then { obj in
            businessView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let businessTagView = UIView().then { obj in
            businessHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let businessheadLabel = UILabel().then { obj in
            businessHeadView.addSubview(obj)
            obj.text = "工商信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(businessTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        for i in 0..<businessList.count{
            let v = UIView().then { obj in
                businessView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:businessList[i])
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let textFiled = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = "自动"
                obj.font = kMediumFont(12)
                obj.tag = 3000+i
                obj.textAlignment = .right
                obj.isEnabled = false
                if i == 0{
                    //法定代表
                    clientLegalTextFiled = obj
                }else if i == 1{
                    //经营状态
                    clientStatusTextFiled = obj
                }else if i == 2{
                    //注册资本
                    registerMoneyTextFiled = obj
                }else if i == 3{
                    //统一社会信用代码
                    clientCodeTextFiled = obj
                }else if i == 4{
                    //工商注册号
                    businessAccountTextFiled = obj
                }else if i == 5{
                    //组织机构代码
                    organizationCodeTextFiled = obj
                }else if i == 6{
                    //成立日期
                    companyCreatTimeTextFiled = obj
                }else if i == 7{
                    //企业类型
                    companyTypeTextFiled = obj
                }else if i == 8{
                    //经营期限
                    dateEndTextFiled = obj
                }else if i == 9{
                    //纳税人资质
                    clientCertificationTextFiled = obj
                }else if i == 10{
                    //所属行业
                    belongIndustryTextFiled = obj
                }else if i == 11{
                    positionStrTextFiled = obj
                }else if i == 12{
                    // 登陆机关
                    clientUnitsTextFiled = obj
                }else if i == 13{
                    // 人员规模
                    personSccaleTextfiled = obj
                }else if i == 14{
                    // 英文名
                    englishNameTextFiled = obj
                }else if i == 15{
                    registerLocationTextFiled = obj
                }
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-30)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(200)
                }
            }
            
            if i == 6 {
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
//                    obj.backgroundColor = .red
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
                
                _ = UIButton().then({ obj in
                    v.addSubview(obj)
                    obj.tag = 1000
                    obj.addTarget(self, action: #selector(companyClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                    }
                })
            }
        }
        
        let localView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(businessView.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(180)
            }
        }
        
        let localHeadView = UIView().then { obj in
            localView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let localTagView = UIView().then { obj in
            localHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let localheadLabel = UILabel().then { obj in
            localHeadView.addSubview(obj)
            obj.text = "地区定位"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(businessTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let locals = ["定位：", "国家/省/市/区：", "详细地址："]
        
        for i in 0..<locals.count{
            let v = UIView().then { obj in
                localView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:locals[i])
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let textFiled = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = baseInfoPlaceholders[i]
                obj.font = kMediumFont(12)
                obj.tag = 4000+i
                obj.delegate = self
                obj.placeholder = "请输入"
                if i == 1{
                    obj.isEnabled = false
                    addressTextField = obj
                }
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-30)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i < 2 {
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
            }
            
            if i == 1{
                _ = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.addTarget(self, action: #selector(locationClick), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.top.left.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                    }
                }
            }
            
            if i < 2{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.isUserInteractionEnabled = true
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
        }
        
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = normalHeadView!
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
//                make.top.equalTo(showView.snp.bottom).offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        view.backgroundColor = bgColor
        
        let commitButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("保存", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kMediumFont(11)
            obj.tag = 1000
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(68)
                make.height.equalTo(30)
            }
        }
        
        let commitAndBuildButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("提交并新建", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kMediumFont(11)
            obj.tag = 1001
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(commitButton.snp.left).offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(88)
                make.height.equalTo(30)
            }
        }
        
        let saveAnddraftButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("保存草稿", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.backgroundColor = .white
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor14.cgColor
            obj.tag = 1002
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(commitAndBuildButton.snp.left).offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(88)
                make.height.equalTo(30)
            }
        }
        
        let saveAndbuildPersonButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("提交并新建联系人", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.backgroundColor = .white
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor14.cgColor
            obj.tag = 1003
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(saveAnddraftButton.snp.left).offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(100)
                make.height.equalTo(30)
            }
        }
        
        // 日期选择
        companyDateView = MyDateView.init()
        //myDateView.isShowDay = false
        companyDateView.delegate = self
        
        // Do any additional setup after loading the view.
        
        cofing()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedClientLinkManAction(_:)), name: Notification.Name(rawValue: "ClientListDetailNotificationKey"), object: nil)
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addPersonClick(_ btn:UIButton){
        if btn.tag == 1002{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 21
            vc.addClientDataBlock = {[self] model in
                sourceHighSeasModel = model
                sourceHighSeasTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1003{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 22
            vc.addClientDataBlock = {[self] model in
                clientLeaveModel = model
                clientLeaveTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1004{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 23
            vc.addClientDataBlock = {[self] model in
                clientBelonggModel = model
                clientBelonggTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1005{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 24
            vc.addClientDataBlock = {[self] model in
                clientSourceModel = model
                clientSourceTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1006{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 25
            vc.addClientDataBlock = {[self] model in
                clientProductModel = model
                clientProductTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1007{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 26
            vc.addClientDataBlock = {[self] model in
                clientInddustryModel = model
                clientInddustryTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1008{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 27
            vc.addClientDataBlock = {[self] model in
                clientScaleModel = model
                clientScaleTextFiled?.text = model.attrName
            }
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func cofing(){
        let router = AddSellLeadsRouter()
        
        presenter = AddSellLeadsPresenter()
        
        presenter?.router = router
        
        let entity = AddSellLeadsEntity()
        
        let interactor = AddSellLeadsInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        if let creditCode = LLUserDefaultsManager.shared.readUserCreditCodeInfos(){
            printTest("asdasdasdasdasd")
            printTest(creditCode)
            presenter?.presenterRequestGetAddSellLeadsDetail(by: ["creditCode":creditCode])
        }
    }
    
    func pickDateView(year: Int, month: Int, day: Int) {
        creatTimeTextFiled?.text = "\(year)年\(month)月\(day)日"
        companyCreatTime = "\(year)年\(month)月\(day)日"
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func companyClick(_ button:UIButton){
        companyDateView.showView()
    }
    
    @objc func didSelectedClientLinkManAction(_ notification:NSNotification){
        printTest(notification)
//        if let notifiModel = notification.userInfo{
//            linkManModel = notifiModel["ClietLineManModel"] as! LinkManModel
//
//            mainLinkManTextFiled?.text = linkManModel!.name!
//            mainLinkManTextFiled2?.text = linkManModel!.name!
//        }
    }
    
    @objc func switchChange(){
        showSwitch?.isSelected = !showSwitch!.isSelected
        tableView?.tableHeaderView = nil
        if showSwitch?.isSelected == true {
            tableView?.tableHeaderView = simpHeadView!
        }else{
            tableView?.tableHeaderView = normalHeadView!
        }
    }
    
    @objc func saveClick(_ button:UIButton){
        if button.tag == 1000{
            commitDidStatus = 1
//            presenter?.presenterRequestGetAddSellLeads(by: ["clientName":clientModel?.clientName, "parentCompany":clientSoureGroup, "highSeas":sourceHighSeas,"clientLevel":clientLeave, "companyProperty":clientBelongg, "source":clientSource, "industry":clientInddustry, "scale":clientScale,"area":areaStr,"city":cityStr,"address":addressDetail,"type":2,"creditCode":clientCode])
            presenter?.presenterRequestGetAddSellLeads(by:["clientName":clientName, "parentCompany":clientSoureGroup, "highSeas":sourceHighSeasModel?.attrName,"clientLevel":clientLeaveModel?.attrName, "companyProperty":clientBelonggModel?.attrName, "source":clientSourceModel?.attrName, "industry":clientInddustryModel?.attrName, "scale":clientScaleModel?.attrName,"area":areaStr,"city":cityStr,"address":addressDetail,"type":2])
        }else if button.tag == 1001{
            commitDidStatus = 2
//            presenter?.presenterRequestGetAddSellLeads(by: ["clientName":clientModel?.clientName, "parentCompany":clientSoureGroup, "highSeas":sourceHighSeas,"clientLevel":clientLeave, "companyProperty":clientBelongg, "source":clientSource, "industry":clientInddustry, "scale":clientScale,"area":areaStr,"city":cityStr,"address":addressDetail,"type":2,"creditCode":clientCode])
            presenter?.presenterRequestGetAddSellLeads(by:["clientName":clientName, "parentCompany":clientSoureGroup, "highSeas":sourceHighSeasModel?.attrName,"clientLevel":clientLeaveModel?.attrName, "companyProperty":clientBelonggModel?.attrName, "source":clientSourceModel?.attrName, "industry":clientInddustryModel?.attrName, "scale":clientScaleModel?.attrName,"area":areaStr,"city":cityStr,"address":addressDetail,"type":2])
        }else if button.tag == 1002{
            HUD.flash(.label("保存草稿成功"), delay: 2)
        }else{
            commitDidStatus = 4
            //新建联系人
//            presenter?.presenterRequestGetAddSellLeads(by:["clientClueInfoParam":["clientName":clientName, "parentCompany":clientSoureGroup, "highSeas":sourceHighSeasModel?.attrName,"clientLevel":clientLeaveModel?.attrName, "companyProperty":clientBelonggModel?.attrName, "source":clientSourceModel?.attrName, "industry":clientInddustryModel?.attrName, "scale":clientScaleModel?.attrName, "parentIndustry    ":"","area":areaStr,"city":cityStr,"address":addressDetail,"type":2,"creditCode":clientCode]])
            presenter?.presenterRequestGetAddSellLeads(by:["clientName":clientName, "parentCompany":clientSoureGroup, "highSeas":sourceHighSeasModel?.attrName,"clientLevel":clientLeaveModel?.attrName, "companyProperty":clientBelonggModel?.attrName, "source":clientSourceModel?.attrName, "industry":clientInddustryModel?.attrName, "scale":clientScaleModel?.attrName,"area":areaStr,"city":cityStr,"address":addressDetail,"type":2])
        }
        
    }
    
    @objc func locationClick(){
        let pickerMode: BWAddressPickerMode = .area
        let addressPickerView = BWAddressPickerView.init(autoSeleceted: true, mode: pickerMode, regions: ("浙江省", "湖州市", "长兴县")) { [self] (provinceModel, cityModel, areaModel) in
            print("block回调-----省：\(provinceModel.title)----市：\(cityModel?.title)----区县：\(areaModel?.title)")
            provinceStr = provinceModel.title
            cityStr = cityModel?.title
            areaStr = areaModel?.title
            
            addressTextField?.text = provinceModel.title! + (cityModel?.title)! + (areaModel?.title)!
        }
        addressPickerView.animationShow()
    }
    
    @objc func mainLineManClick(){
//        presenter?.router?.pushToClientPersonDetail(from: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddSellLeadsViewController: AddSellLeadsViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetAddSellLeadsPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if commitDidStatus == 1{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SellLeadsListNotificationKey"), object: nil, userInfo: nil)
                    navigationController?.popViewController(animated: true)
                }else if commitDidStatus == 2{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SellLeadsListNotificationKey"), object: nil, userInfo: nil)
                    presenter?.router?.pushToAddSellLeads(from: self)
                }else if commitDidStatus == 3{
                    
                }else{
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SellLeadsListNotificationKey"), object: nil, userInfo: nil)
                    presenter?.router?.pushToAddLinkMan(from: self)
                }
                
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddSellLeadsPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddSellLeadsDetailPresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddSellLeadsModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    requestModel = dataModel
                    //                    clientLegalTextFiled?.text = "自动"
                    clientCodeTextFiled?.text = dataModel.CreditCode
                    companyTypeTextFiled?.text = dataModel.EntType
                    englishNameTextFiled?.text = dataModel.EnglishName
                    belongIndustryTextFiled?.text = dataModel.industry
                    businessAccountTextFiled?.text = dataModel.No
                    clientStatusTextFiled?.text = dataModel.jyzt
                    clientLegalTextFiled?.text = dataModel.OperName
                    registerMoneyTextFiled?.text = dataModel.RegistCapi
                    organizationCodeTextFiled?.text = dataModel.OrgNo
                    companyCreatTimeTextFiled?.text = dataModel.StartDate
                    dateEndTextFiled?.text = dataModel.TeamEnd
                    clientCertificationTextFiled?.text = dataModel.TaxpayerType
                    clientUnitsTextFiled?.text = dataModel.BelongOrg
                    personSccaleTextfiled?.text = dataModel.rygm
                    registerLocationTextFiled?.text = dataModel.Address
                    positionStrTextFiled?.text =  (dataModel.Area?.County ?? "")+(dataModel.Area?.City ?? "")+(dataModel.Area?.Province ?? "")
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddSellLeadsDetailPresenterReceiveError(error: MyError?){
        
    }
    
    
}

extension AddSellLeadsViewController: BWAddressPickerViewDelegate{
    func addressPickerView(_ pickerView: BWAddressPickerView, didSelectWithProvinceModel provinceModel: BWAddressModel, cityModel: BWAddressCityModel?, areaModel: BWAddressAreaModel?) {
        print("代理回调-----省：\(provinceModel.title)----市：\(cityModel?.title)----区县：\(areaModel?.title)")
        provinceStr = provinceModel.title
        cityStr = cityModel?.title
        areaStr = areaModel?.title
        
        addressTextField?.text = provinceModel.title! + (cityModel?.title)! + (areaModel?.title)!
    }
}


extension AddSellLeadsViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            clientName = textField.text!
        }else if textField.tag == 1001{
            clientSoureGroup = textField.text!
        }else if textField.tag == 1002{
            sourceHighSeas = textField.text!
        }else if textField.tag == 1003{
            clientLeave = textField.text!
        }else if textField.tag == 1004{
            clientBelongg = textField.text!
        }else if textField.tag == 1005{
            clientSource = textField.text!
        }else if textField.tag == 1006{
            clientProduct = textField.text!
        }else if textField.tag == 1007{
            clientInddustry = textField.text!
        }else if textField.tag == 1008{
            clientScale = textField.text!
        }else if textField.tag == 3000{
            clientLegal = textField.text!
        }else if textField.tag == 3001{
            clientStatus = textField.text!
        }else if textField.tag == 3002{
            registerMoney = textField.text!
        }else if textField.tag == 3003{
            clientCode = textField.text!
        }else if textField.tag == 3004{
            businessAccount = textField.text!
        }else if textField.tag == 3005{
            organizationCode = textField.text!
        }else if textField.tag == 3006{
            companyCreatTime = textField.text!
        }else if textField.tag == 3007{
            companyType = textField.text
        }else if textField.tag == 3008{
            dateEnd = textField.text!
        }else if textField.tag == 3009{
            clientCertification = textField.text!
        }else if textField.tag == 3010{
            belongIndustry = textField.text!
        }else if textField.tag == 3011{
            positionStr = textField.text!
        }else if textField.tag == 3012{
            clientUnits = textField.text!
        }else if textField.tag == 3013{
            detailLocationStr = textField.text!
        }else if textField.tag == 3014{
            personSccale = textField.text!
        }else if textField.tag == 3015{
            englishName = textField.text
        }else if textField.tag == 3016{
            registerLocation = textField.text
        }else if textField.tag == 4000{
            localStr = textField.text!
        }else if textField.tag == 4001{
            addressStr = textField.text!
        }else if textField.tag == 4002{
            addressDetail = textField.text!
        }
    }
}
