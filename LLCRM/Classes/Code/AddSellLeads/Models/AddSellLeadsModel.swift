//
//  AddSellLeadsModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2024/1/4.
//

import UIKit



class AddSellLeadsModel: BaseModel {
    
    var Address:String?
    var Area:AddSellLeadsAreaModel?
    var AreaCode:String?
    var BelongOrg:String?
    var CreditCode:String?
    var industry:String?
    var EconKind:String?
    var EndDate:String?
    var EntType:String?
    var ImageUrl:String?
    var IsOnStock:String?
    var KeyNo:String?
    var Name:String?
    var No:String?
    var OperId:String?
    var OperName:String?
    var OrgNo:String?
    var OriginalName:[Any]?
    var Province:String?
    var RecCap:String?
    var RegistCapi:String?
    var RevokeInfo:String?
    var Scope:String?
    var StartDate:String?
    var Status:String?
    var StockNumber:String?
    var StockType:String?
    var TeamEnd:String?
    var TermStart:String?
    var UpdatedDate:String?
    var EnglishName:String?
    var jyzt:String?
    var TaxpayerType:String?
    var rygm:String?
//    var Area:String?
    
    required init() {}
}

class AddSellLeadsAreaModel: BaseModel{
    var City:String?
    var County:String?
    var Province:String?
    required init() {}
}
