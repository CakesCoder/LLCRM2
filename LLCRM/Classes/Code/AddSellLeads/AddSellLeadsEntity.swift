//
//  AddSellLeadsEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class AddSellLeadsEntity: AddSellLeadsEntityProtocols {
    var interactor: AddSellLeadsInteractorProtocols?
    
    func didAddSellLeadsReceiveData(by params: Any?) {
        interactor?.didEntityAddSellLeadsReceiveData(by: params)
    }
    
    func didAddSellLeadsReceiveError(error: MyError?) {
        interactor?.didEntityAddSellLeadsReceiveError(error: error)
    }
    
    func getAddSellLeadsRequest(by params: Any?) {
        LLNetProvider.request(.addClient(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddSellLeadsReceiveData(by:jsonData)
            case .failure(_):
                self.didAddSellLeadsReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddSellLeadsDetailReceiveData(by params: Any?) {
        interactor?.didEntityAddSellLeadsDetailReceiveData(by: params)
    }
    
    func didAddSellLeadsDetailReceiveError(error: MyError?) {
        interactor?.didEntityAddSellLeadsDetailReceiveError(error: error)
    }
    
    func getAddSellLeadsDetailRequest(by params: Any?) {
        LLNetProvider.request(.addClientSellLeads(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddSellLeadsDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didAddSellLeadsDetailReceiveError(error: .requestError)
            }
        }
    }
}
