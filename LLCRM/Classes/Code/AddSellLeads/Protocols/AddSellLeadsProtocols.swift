//
//  AddSellLeadsProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class AddSellLeadsProtocols: NSObject {

}

protocol AddSellLeadsViewProtocols: AnyObject {
    var presenter: AddSellLeadsPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    
    func didGetAddSellLeadsPresenterReceiveData(by params: Any?)
    func didGetAddSellLeadsPresenterReceiveError(error: MyError?)
    
    func didGetAddSellLeadsDetailPresenterReceiveData(by params: Any?)
    func didGetAddSellLeadsDetailPresenterReceiveError(error: MyError?)
}

protocol AddSellLeadsPresenterProtocols: AnyObject{
    var view: AddSellLeadsViewProtocols? { get set }
    var router: AddSellLeadsRouterProtocols? { get set }
    var interactor: AddSellLeadsInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddSellLeads(by params: Any?)
    func didGetAddSellLeadsInteractorReceiveData(by params: Any?)
    func didGetAddSellLeadsInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddSellLeadsDetail(by params: Any?)
    func didGetAddSellLeadsDetailInteractorReceiveData(by params: Any?)
    func didGetAddSellLeadsDetailInteractorReceiveError(error: MyError?)
}

protocol AddSellLeadsInteractorProtocols: AnyObject {
    var presenter: AddSellLeadsPresenterProtocols? { get set }
    var entity: AddSellLeadsEntityProtocols? { get set }
    
    func presenterRequestAddSellLeads(by params: Any?)
    func didEntityAddSellLeadsReceiveData(by params: Any?)
    func didEntityAddSellLeadsReceiveError(error: MyError?)
    
    func presenterRequestAddSellLeadsDetail(by params: Any?)
    func didEntityAddSellLeadsDetailReceiveData(by params: Any?)
    func didEntityAddSellLeadsDetailReceiveError(error: MyError?)
    
}

protocol AddSellLeadsEntityProtocols: AnyObject {
    var interactor: AddSellLeadsInteractorProtocols? { get set }
    
    func didAddSellLeadsReceiveData(by params: Any?)
    func didAddSellLeadsReceiveError(error: MyError?)
    func getAddSellLeadsRequest(by params: Any?)
    
    func didAddSellLeadsDetailReceiveData(by params: Any?)
    func didAddSellLeadsDetailReceiveError(error: MyError?)
    func getAddSellLeadsDetailRequest(by params: Any?)
}

protocol AddSellLeadsRouterProtocols: AnyObject {
    func pushToSellLeadsDetail(from previousView: UIViewController)
    func pushToAddSellLeads(from previousView: UIViewController)
    func pushToAddLinkMan(from previousView: UIViewController) 
}



