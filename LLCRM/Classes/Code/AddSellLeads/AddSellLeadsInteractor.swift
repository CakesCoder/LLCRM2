//
//  AddSellLeadsInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class AddSellLeadsInteractor: AddSellLeadsInteractorProtocols {
    var presenter: AddSellLeadsPresenterProtocols?
    
    var entity: AddSellLeadsEntityProtocols?
    
    func presenterRequestAddSellLeads(by params: Any?) {
        entity?.getAddSellLeadsRequest(by: params)
    }
    
    func didEntityAddSellLeadsReceiveData(by params: Any?) {
        presenter?.didGetAddSellLeadsInteractorReceiveData(by: params)
    }
    
    func didEntityAddSellLeadsReceiveError(error: MyError?) {
        presenter?.didGetAddSellLeadsInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddSellLeadsDetail(by params: Any?){
        entity?.getAddSellLeadsDetailRequest(by: params)
    }
    
    func didEntityAddSellLeadsDetailReceiveData(by params: Any?){
        presenter?.didGetAddSellLeadsDetailInteractorReceiveData(by: params)
    }
    
    func didEntityAddSellLeadsDetailReceiveError(error: MyError?){
        presenter?.didGetAddSellLeadsDetailInteractorReceiveError(error: error)
    }
}
