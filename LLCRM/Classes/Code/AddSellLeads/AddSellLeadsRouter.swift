//
//  AddSellLeadsRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class AddSellLeadsRouter: AddSellLeadsRouterProtocols {
    func pushToSellLeadsDetail(from previousView: UIViewController) {
        
    }
    
    func pushToAddSellLeads(from previousView: UIViewController) {
        let nextView = AddSellLeadsViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToAddLinkMan(from previousView: UIViewController) {
        let nextView = AddClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
