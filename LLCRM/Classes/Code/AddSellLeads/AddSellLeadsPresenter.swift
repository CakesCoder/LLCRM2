//
//  AddSellLeadsPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class AddSellLeadsPresenter: AddSellLeadsPresenterProtocols {
    var view: AddSellLeadsViewProtocols?
    
    var router: AddSellLeadsRouterProtocols?
    
    var interactor: AddSellLeadsInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddSellLeads(by params: Any?) {
        interactor?.presenterRequestAddSellLeads(by: params)
    }
    
    func didGetAddSellLeadsInteractorReceiveData(by params: Any?) {
//        view?.hideLoading()
        view?.didGetAddSellLeadsPresenterReceiveData(by: params)
    }
    
    func didGetAddSellLeadsInteractorReceiveError(error: MyError?) {
//        view?.hideLoading()
        view?.didGetAddSellLeadsPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddSellLeadsDetail(by params: Any?){
        interactor?.presenterRequestAddSellLeadsDetail(by: params)
    }
    
    func didGetAddSellLeadsDetailInteractorReceiveData(by params: Any?){
        view?.didGetAddSellLeadsDetailPresenterReceiveData(by: params)
    }
    
    func didGetAddSellLeadsDetailInteractorReceiveError(error: MyError?){
        view?.didGetAddSellLeadsDetailPresenterReceiveError(error: error)
    }

}
