//
//  ProductAttachementViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit
//import JXPagingView

extension ProductAttachementViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ProductAttachementViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ProductAttachementViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let headTagView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.centerY.equalToSuperview()
                make.top.equalToSuperview().offset(20)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "附件"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
//                make.top.equalToSuperview().offset(20)
                make.centerY.equalTo(headTagView)
            }
        }
        
//        for i in 0..<2{
//            let v = UIView().then { obj in
//                headView.addSubview(obj)
//                obj.backgroundColor = bgColor13
//                obj.layer.cornerRadius = 2
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.top.equalToSuperview().offset(56+36*i)
//                    make.right.equalToSuperview().offset(-100)
//                    make.height.equalTo(26)
//                }
//            }
//            
//            let imageV = UIImageView().then { obj in
//                v.addSubview(obj)
////                obj.backgroundColor = .blue
//                obj.image = UIImage(named: "flie_pdf")
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(14)
//                    make.centerY.equalToSuperview()
//                    make.width.height.equalTo(17)
//                }
//            }
//            
//            let namelabel = UILabel().then { obj in
//                v.addSubview(obj)
//                obj.text = "报价信息表.PDF"
//                obj.textColor = bluebgColor
//                obj.font = kMediumFont(12)
//                obj.snp.makeConstraints { make in
//                    make.left.equalTo(imageV.snp.right).offset(10)
//                    make.centerY.equalToSuperview()
//                }
//            }
//            
//            _ = UIImageView().then { obj in
//                v.addSubview(obj)
////                obj.backgroundColor = .blue
//                obj.image = UIImage(named: "flie_delete")
//                obj.snp.makeConstraints { make in
//                    make.right.equalToSuperview().offset(-14)
//                    make.centerY.equalToSuperview()
//                    make.width.height.equalTo(17)
//                }
//            }
//        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
