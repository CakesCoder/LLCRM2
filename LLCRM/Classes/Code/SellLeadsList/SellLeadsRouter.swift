//
//  SellLeadsRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsRouter: SellLeadsRouterProtocols {
    func pushToSellLeadsDetail(from previousView: UIViewController, forModel model: Any) {
        let nextView = SellLeadListsDetailViewController()
        nextView.model = model as? ClientDetailDataModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToAddSellLeads(from previousView: UIViewController) {
        let nextView = AddSellLeadsViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
