//
//  SellLeadsPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsPresenter: SellLeadsPresenterProtocols {
    var view: SellLeadsViewProtocols?
    
    var router: SellLeadsRouterProtocols?
    
    var interactor: SellLeadsInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetSellLeads(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestSellLeads(by: params)
    }
    
    func didGetSellLeadsInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetSellLeadsPresenterReceiveData(by: params)
    }
    
    func didGetSellLeadsInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetSellLeadsPresenterReceiveError(error: error)
    }
    

}
