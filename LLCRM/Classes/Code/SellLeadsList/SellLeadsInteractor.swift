//
//  SellLeadsInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsInteractor: SellLeadsInteractorProtocols {
    var presenter: SellLeadsPresenterProtocols?
    
    var entity: SellLeadsEntityProtocols?
    
    func presenterRequestSellLeads(by params: Any?) {
        entity?.getSellLeadsRequest(by: params)
    }
    
    func didEntitySellLeadsReceiveData(by params: Any?) {
        presenter?.didGetSellLeadsInteractorReceiveData(by: params)
    }
    
    func didEntitySellLeadsReceiveError(error: MyError?) {
        presenter?.didGetSellLeadsInteractorReceiveError(error: error)
    }
    

}
