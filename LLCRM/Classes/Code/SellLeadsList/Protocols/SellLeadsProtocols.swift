//
//  SellLeadsProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsProtocols: NSObject {

}

protocol SellLeadsViewProtocols: AnyObject {
    var presenter: SellLeadsPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetSellLeadsPresenterReceiveData(by params: Any?)
    func didGetSellLeadsPresenterReceiveError(error: MyError?)
}

protocol SellLeadsPresenterProtocols: AnyObject{
    var view: SellLeadsViewProtocols? { get set }
    var router: SellLeadsRouterProtocols? { get set }
    var interactor: SellLeadsInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetSellLeads(by params: Any?)
    func didGetSellLeadsInteractorReceiveData(by params: Any?)
    func didGetSellLeadsInteractorReceiveError(error: MyError?)
}

protocol SellLeadsInteractorProtocols: AnyObject {
    var presenter: SellLeadsPresenterProtocols? { get set }
    var entity: SellLeadsEntityProtocols? { get set }
    
    func presenterRequestSellLeads(by params: Any?)
    func didEntitySellLeadsReceiveData(by params: Any?)
    func didEntitySellLeadsReceiveError(error: MyError?)
}

protocol SellLeadsEntityProtocols: AnyObject {
    var interactor: SellLeadsInteractorProtocols? { get set }
    
    func didSellLeadsReceiveData(by params: Any?)
    func didSellLeadsReceiveError(error: MyError?)
    func getSellLeadsRequest(by params: Any?)
}

protocol SellLeadsRouterProtocols: AnyObject {
    func pushToSellLeadsDetail(from previousView: UIViewController, forModel model: Any) 
    func pushToAddSellLeads(from previousView: UIViewController)
}



