//
//  SellLeadsEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsEntity: SellLeadsEntityProtocols {
    var interactor: SellLeadsInteractorProtocols?
    
    func didSellLeadsReceiveData(by params: Any?) {
        interactor?.didEntitySellLeadsReceiveData(by: params)
    }
    
    func didSellLeadsReceiveError(error: MyError?) {
        interactor?.didEntitySellLeadsReceiveError(error: error)
    }
    
    func getSellLeadsRequest(by params: Any?) {
        LLNetProvider.request(.clientDetail(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didSellLeadsReceiveData(by:jsonData)
            case .failure(_):
                self.didSellLeadsReceiveError(error: .requestError)
            }
        }
    }
}
