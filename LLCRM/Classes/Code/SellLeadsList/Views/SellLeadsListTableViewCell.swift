//
//  SellLeadsListTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsListTableViewCell: UITableViewCell {
    
    var model:ClientDetailDataModel?{
        didSet{
            nameLabel2?.text = "暂无"
            hintLabel?.text = model?.parentIndustry ?? "暂无"
            timeLabel?.text = "创建时间：暂无 "
            nameLabel?.text = model?.clientName ?? "暂无"
        }
    }
    
    var nameLabel:UILabel?
    var timeLabel:UILabel?
    var nameLabel2:UILabel?
    var hintLabel:UILabel?
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "sell_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        }
        
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor78
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let tagLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor44
            obj.text = "草稿"
            obj.textColor = .white
            obj.font = kRegularFont(8)
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel!.snp.right).offset(15)
                make.centerY.equalTo(nameLabel!)
                make.width.equalTo(26)
                make.height.equalTo(15)
            }
        }
        
        timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(15)
            }
        }
        
        nameLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.font = kMediumFont(12)
//            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel!)
            }
        }
        
        hintLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
//            obj.text = "制造业"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(timeLabel!)
            }
        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        }
    }


}
