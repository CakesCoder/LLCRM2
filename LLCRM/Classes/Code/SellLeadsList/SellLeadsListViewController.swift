//
//  SellLeadsListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh

extension SellLeadsListViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SellLeadsListViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class SellLeadsListViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var sellLeadsListdidSelectedCallback: ((ClientDetailDataModel) -> ())?
    
    var tableView:UITableView?
    
    var presenter: SellLeadsPresenterProtocols?
    var current:Int? = 1
    var dataList:[ClientDetailDataModel]? = []
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(SellLeadsListTableViewCell.self, forCellReuseIdentifier: "SellLeadsListTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
            presenter?.presenterRequestGetSellLeads(by: ["current":current, "size":10])
            tableView?.stopFooterRefreshing()
        }))
        
        // Do any additional setup after loading the view.
        cofing()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedSellLeadsListAction(_:)), name: Notification.Name(rawValue: "SellLeadsListNotificationKey"), object: nil)
    }
    
    @objc func didSelectedSellLeadsListAction(_ notification:NSNotification){
        current = 1
        presenter?.presenterRequestGetSellLeads(by: ["current":current, "size":10])
    }
    
    
    func cofing(){
        let router = SellLeadsRouter()
        
        presenter = SellLeadsPresenter()
        
        presenter?.router = router
        
        let entity =  SellLeadsEntity()
        
        let interactor =  SellLeadsInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
        current = 1
        presenter?.presenterRequestGetSellLeads(by: ["current":current, "size":10])
    }
}

extension SellLeadsListViewController: SellLeadsViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetSellLeadsPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if current == 1{
                    dataList?.removeAll()
                }
                if let datas = JSONDeserializer<HomeClientDetailModel>.deserializeFrom(json:toJSONString(dict:dataDic ?? ["":""])) {
                    printTest(datas.dataList)
                    dataList?.append(contentsOf: datas.dataList ?? [])
                    tableView?.reloadData()
                    current! += 1
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetSellLeadsPresenterReceiveError(error: MyError?) {
        
    }
}

extension SellLeadsListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SellLeadsListTableViewCell", for: indexPath) as! SellLeadsListTableViewCell
        cell.model = dataList?[indexPath.row] as? ClientDetailDataModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.sellLeadsListdidSelectedCallback?((dataList?[indexPath.row] as? ClientDetailDataModel)!)
    }
}
