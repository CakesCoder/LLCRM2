//
//  BasicNavViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit

class BasicNavViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
        setupCofing()
        // Do any additional setup after loading the view.
    }
    
    func setupCofing(){
        self.whiteNavTheme()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension UINavigationController{
    public func blueNavTheme(){
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = themeColor
            
            //设置取消按钮的字
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: blackTextColor3]
            navigationBar.standardAppearance = appearance
            navigationBar.scrollEdgeAppearance = navigationBar.standardAppearance
        } else {
            UINavigationBar.appearance().barTintColor = themeColor
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: blackTextColor3]
            UINavigationBar.appearance().tintColor = blackTextColor3
        }
    }
    
    public func whiteNavTheme(){
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = .white
            
            //设置取消按钮的字
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: blackTextColor3]
            navigationBar.standardAppearance = appearance
            navigationBar.scrollEdgeAppearance = navigationBar.standardAppearance
        } else {
            UINavigationBar.appearance().barTintColor = .white
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: blackTextColor3]
            UINavigationBar.appearance().tintColor = blackTextColor3
        }
    }
}

extension BasicNavViewController
{
    override func pushViewController(_ viewController: UIViewController, animated: Bool)
    {
        if children.count > 0 {
            viewController.hidesBottomBarWhenPushed = true
        }
        super.pushViewController(viewController, animated: animated)
    }
}

extension UIBarButtonItem{
    class func createBarbuttonItem(name: String,target: Any?, action: Selector) -> UIBarButtonItem{

        let rightBtn = UIButton()
        rightBtn.setImage(UIImage(named: name), for: .normal)
        rightBtn.setImage(UIImage(named: name), for: .highlighted)
        // button自适应大小
        rightBtn.sizeToFit()
        rightBtn.addTarget(target, action: action, for: .touchUpInside)
        return UIBarButtonItem(customView:rightBtn)
    }
    
    class func createAddBarbuttonItem(name: String,target: Any?, action: Selector) -> UIBarButtonItem{

        let rightBtn = UIButton()
        rightBtn.setTitle(name, for: .normal)
        rightBtn.setTitle(name, for: .highlighted)
        rightBtn.titleLabel?.font = kMediumFont(25)
        rightBtn.setTitleColor(.black, for: .normal)
        // button自适应大小
        rightBtn.sizeToFit()
        rightBtn.addTarget(target, action: action, for: .touchUpInside)
        return UIBarButtonItem(customView:rightBtn)
    }
    
    class func createcommitBarbuttonItem(name: String,target: Any?, action: Selector) -> UIBarButtonItem{

        let rightBtn = UIButton()
        rightBtn.setTitle(name, for: .normal)
        rightBtn.setTitle(name, for: .highlighted)
        rightBtn.titleLabel?.font = kMediumFont(16)
        rightBtn.setTitleColor(.black, for: .normal)
        // button自适应大小
        rightBtn.sizeToFit()
        rightBtn.addTarget(target, action: action, for: .touchUpInside)
        return UIBarButtonItem(customView:rightBtn)
    }
}
