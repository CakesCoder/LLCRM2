//
//  QXChatNavigationViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit

class QXChatNavigationViewController: BasicNavViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            let appearance = UINavigationBarAppearance()
            appearance.configureWithOpaqueBackground()
            appearance.backgroundColor = lineColor

            //设置取消按钮的字
            appearance.titleTextAttributes = [NSAttributedString.Key.foregroundColor: blackTextColor3]
            navigationBar.standardAppearance = appearance
            navigationBar.scrollEdgeAppearance = navigationBar.standardAppearance
        } else {
            UINavigationBar.appearance().barTintColor = .white
            UINavigationBar.appearance().titleTextAttributes = [.foregroundColor: blackTextColor16]
            UINavigationBar.appearance().tintColor = blackTextColor16
        }

    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
