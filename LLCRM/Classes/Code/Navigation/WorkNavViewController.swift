//
//  WorkNavViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit

class WorkNavViewController: BasicNavViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let vc = WorkViewController()
        self.addChild(vc)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
