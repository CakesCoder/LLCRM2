//
//  HomeNavigationViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import UIKit

class HomeNavigationViewController: BasicNavViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        self.navigationItem.title = "CRM"
        self.title = "CRM"
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.navigationController?.navigationItem.title = "CRM"
        self.blueNavTheme()
        
        let vc = HomeViewController()
        self.addChild(vc)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
