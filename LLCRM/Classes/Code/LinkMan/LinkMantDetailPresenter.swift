//
//  LinkMantDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit


class LinkMantDetailPresenter: LinkMantDetailPresenterProtocols {
    
    var view: LinkManDetailViewProtocols?
    
    var router: LinkManDetailRouterProtocols?
    
    var interactor: LinkManDetailInteractorProtocols?
    
    var params:[Any]? = []
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetLinkManDetail(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestLinkMan(by: params)
    }
        
    func didGetLinkManDetailInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetLinkManDetailPresenterReceiveData(by: params)
    }
    
    func didGetLinkManDetailInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetLinkManDetailPresenterReceiveError(error: error)
    }
}
