//
//  LinkManTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class LinkManTableViewCell: UITableViewCell {
    
    var titleLabel:UILabel?
    var contentLabel: UILabel?
    var contentLabel1: UILabel?
    var contentLabel2: UILabel?
    var contentLabel3: UILabel?
    
    var model:LinkManModel?{
        didSet{
            titleLabel?.text = "---"
            
            let hint1 = NSMutableAttributedString(string:"负责人：")
            hint1.yy_color = blackTextColor68
            hint1.yy_font = kMediumFont(12)
            
            let content1 = NSMutableAttributedString(string: "- -")
            content1.yy_color = blackTextColor14
            content1.yy_font = kMediumFont(12)
            hint1.append(content1)
            
            contentLabel?.attributedText = hint1
            
            let hint2 = NSMutableAttributedString(string:"姓名：")
            hint2.yy_color = blackTextColor68
            hint2.yy_font = kMediumFont(12)
            
            var title = "---"
            if ((model?.department) != nil){
                title = "\(model?.name ?? "") / \(model?.department ?? "")"
            }else{
                title = "\(model?.name ?? "")"
            }
            
            let content2 = NSMutableAttributedString(string: title)
            content2.yy_color = blackTextColor14
            content2.yy_font = kMediumFont(12)
            hint2.append(content2)
            
            contentLabel1?.attributedText = hint2
            
            let hint3 = NSMutableAttributedString(string:"电话：")
            hint3.yy_color = blackTextColor68
            hint3.yy_font = kMediumFont(12)
            
            let content3 = NSMutableAttributedString(string: model?.phone ?? "")
            content3.yy_color = blackTextColor14
            content3.yy_font = kMediumFont(12)
            hint3.append(content3)
            
            contentLabel2?.attributedText = hint3
            
            let hint4 = NSMutableAttributedString(string:"最后跟进时间：")
            hint4.yy_color = blackTextColor68
            hint4.yy_font = kMediumFont(12)
            
            let content4 = NSMutableAttributedString(string: model?.createDate ?? "")
            content4.yy_color = blackTextColor14
            content4.yy_font = kMediumFont(12)
            hint4.append(content4)
            
            contentLabel3?.attributedText = hint4
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "client_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        })
        
        titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.textColor = blackTextColor78
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
//        let statusLabel = UILabel().then { obj in
//            contentView.addSubview(obj)
//            obj.backgroundColor = bgColor12
//            obj.layer.cornerRadius = 2
//            obj.layer.borderWidth = 0.5
//            obj.layer.borderColor = lineColor26.cgColor
//            obj.text = "未成交"
//            obj.textColor = blackTextColor36
//            obj.font = kRegularFont(12)
//            obj.textAlignment = .center
//            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalTo(tagImageV)
//                make.width.equalTo(52)
//                make.height.equalTo(22)
//            }
//        }
        
        contentLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(titleLabel!.snp.bottom).offset(15)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        contentLabel1 = UILabel().then { obj in
            contentView.addSubview(obj)
            
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(contentLabel!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        contentLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(contentLabel1!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        contentLabel3 = UILabel().then { obj in
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(contentLabel2!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
    }

}
