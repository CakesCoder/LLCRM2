//
//  LinkManDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class LinkManDetailEntity: LinkManDetailEntityProtocols {
    var interactor: LinkManDetailInteractorProtocols?
    
    func didLinkManReceiveData(by params: Any?) {
        interactor?.didEntityLinkManReceiveData(by: params)
    }
    
    func didLinkManReceiveError(error: MyError?) {
        interactor?.didEntityLinkManReceiveError(error: error)
    }
    
    func getLinkManRequest(by params: Any?) {
        LLNetProvider.request(.lineMan(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didLinkManReceiveData(by:jsonData)
            case .failure(_):
                self.didLinkManReceiveError(error: .requestError)
            }
        }
    }
}
