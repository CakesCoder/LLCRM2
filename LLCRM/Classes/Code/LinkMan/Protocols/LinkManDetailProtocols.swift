//
//  LinkManDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

protocol LinkManDetailViewProtocols: AnyObject {
    var presenter: LinkMantDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetLinkManDetailPresenterReceiveData(by params: Any?)
    func didGetLinkManDetailPresenterReceiveError(error: MyError?)
}

protocol LinkMantDetailPresenterProtocols: AnyObject{
    var view: LinkManDetailViewProtocols? { get set }
    var router: LinkManDetailRouterProtocols? { get set }
    var interactor: LinkManDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetLinkManDetail(by params: Any?)
    func didGetLinkManDetailInteractorReceiveData(by params: Any?)
    func didGetLinkManDetailInteractorReceiveError(error: MyError?)
}

protocol LinkManDetailInteractorProtocols: AnyObject {
    var presenter: LinkMantDetailPresenterProtocols? { get set }
    var entity: LinkManDetailEntityProtocols? { get set }
    
    func presenterRequestLinkMan(by params: Any?)
    func didEntityLinkManReceiveData(by params: Any?)
    func didEntityLinkManReceiveError(error: MyError?)
}

protocol LinkManDetailEntityProtocols: AnyObject {
    var interactor: LinkManDetailInteractorProtocols? { get set }
    
    func didLinkManReceiveData(by params: Any?)
    func didLinkManReceiveError(error: MyError?)
    func getLinkManRequest(by params: Any?)
}

protocol LinkManDetailRouterProtocols: AnyObject {
    func pushToAddLinkMan(from previousView: UIViewController)
}

