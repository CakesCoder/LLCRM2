//
//  LinkManDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class LinkManDetailInteractor: LinkManDetailInteractorProtocols {
    var presenter: LinkMantDetailPresenterProtocols?
    
    var entity: LinkManDetailEntityProtocols?
    
    func presenterRequestLinkMan(by params: Any?){
        entity?.getLinkManRequest(by: params)
    }
    
    func didEntityLinkManReceiveData(by params: Any?){
        presenter?.didGetLinkManDetailInteractorReceiveData(by: params)
    }
    
    func didEntityLinkManReceiveError(error: MyError?){
        presenter?.didGetLinkManDetailInteractorReceiveError(error: error)
    }
}
