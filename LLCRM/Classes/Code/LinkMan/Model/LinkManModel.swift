//
//  LinkManDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

class LinkManDataModel: BaseModel{
    var code: Int?
    var data:[LinkManModel]?
    var message:String?
}

class LinkManModel: BaseModel {
    var id: String?
    var name: String?
    var clientId: String?
    var gender: String?
    var department: String?
    var phone: String?
    var email: String?
    var address: String?
    var status: String?
    var background: String?
    var companyId: Int?
    var createDate: String?
    var deleted: Int?
    var info: String?
    var photoPath: String?
    var photoUrl: String?
    var position: String?
    
    required init() {}
}
