//
//  LinkManDetailRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class LinkManDetailRouter: LinkManDetailRouterProtocols {
    func pushToAddLinkMan(from previousView: UIViewController) {
        let nextView = AddLinkManViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
}
