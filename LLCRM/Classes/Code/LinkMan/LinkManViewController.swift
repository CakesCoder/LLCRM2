//
//  LinkManViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit
import PKHUD
import HandyJSON

class LinkManViewController: BaseViewController {
    
    var presenter: LinkMantDetailPresenterProtocols?
    var tableView: UITableView?
    var searchText: String? = ""
//    var isSelected:Bool?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "联系人详情"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(LinkManViewController.blackClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem.createAddBarbuttonItem(name: "+", target: self, action: #selector(LinkManViewController.addDataClick))
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(LinkManTableViewCell.self, forCellReuseIdentifier: "LinkManTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(headView.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
//        if isSelected == false{
//            let addButton = UIButton().then { obj in
//                view.addSubview(obj)
//                obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
//                obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
//                obj.snp.makeConstraints { make in
//                    make.right.equalToSuperview().offset(-0)
//                    make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
//                }
//            }
//        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedLinkManAction(_:)), name: Notification.Name(rawValue: "LinkManNotificationKey"), object: nil)
        
        cofing()
        
        presenter?.presenterRequestGetLinkManDetail(by: [:] )
    }
    
    @objc func didSelectedLinkManAction(_ notification:NSNotification){
        presenter?.presenterRequestGetLinkManDetail(by: [:] )
    }
    
    @objc func addClick(_ button:UIButton){
        self.presenter?.router?.pushToAddLinkMan(from: self)
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addDataClick(){
        self.presenter?.router?.pushToAddLinkMan(from: self)
    }
    
    func cofing(){
        let router = LinkManDetailRouter()
        
        presenter = LinkMantDetailPresenter()
        
        presenter?.router = router
        
        let entity = LinkManDetailEntity()
        
        let interactor = LinkManDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LinkManViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (presenter?.params!.count)!
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LinkManTableViewCell", for: indexPath) as! LinkManTableViewCell
        cell.model = (presenter?.params?[indexPath.row] as! LinkManModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if isSelected == true{
            let linkManModel =  (presenter?.params?[indexPath.row] as! LinkManModel)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "ClientListDetailNotificationKey"), object: nil, userInfo: ["ClietLineManModel": linkManModel])
//            self.navigationController?.popViewController(animated: true)
//        }
    }
}

extension LinkManViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        self.searchText = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if self.searchText!.length > 0{
            presenter?.presenterRequestGetLinkManDetail(by: ["search":self.searchText] )
        }else{
            presenter?.presenterRequestGetLinkManDetail(by: [:] )
        }
        
        return true
    }
}

extension LinkManViewController: LinkManDetailViewProtocols{
    func didGetLinkManDetailPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if let datas = JSONDeserializer<LinkManDataModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    presenter?.params?.removeAll()
                    presenter?.params?.append(contentsOf: datas.data ?? [])
                    tableView?.reloadData()
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetLinkManDetailPresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
}

