//
//  BusinessDetailInfosViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/11.
//

import UIKit
//import JXPagingView

extension BusinessDetailInfosViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension BusinessDetailInfosViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}
class BusinessDetailInfosViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var detailInfosLabel:UILabel?
    var possibilityLabel:UILabel?
    var perosonLabel:UILabel?
    var commitDateLabel:UILabel?
    var productTypeLabel:UILabel?
    var productNameLabel:UILabel?
    var productNumLabel:UILabel?
    var productMoneyLabel:UILabel?
    
    var model:BusinessListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "详细信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["商机信息：", "可能性：", "跟随人：", "期望完成时间：", "产品类别：", "产品名称：", "预计采购数量：", "预计采购金额（元）："]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
//            if i == baseInfos.count-1{
//
//                let progressView = UIView().then { obj in
//                    v.addSubview(obj)
//                    obj.layer.cornerRadius = 8
//                    obj.backgroundColor = lineColor7
//                    obj.snp.makeConstraints { make in
//                        make.left.equalToSuperview().offset(120)
//                        make.centerY.equalToSuperview()
//                        make.width.equalTo(100)
//                        make.height.equalTo(10)
//                    }
//                }
//
//                let progressContentView = UIView().then { obj in
//                    v.addSubview(obj)
//                    obj.layer.cornerRadius = 8
//                    obj.backgroundColor = bgColor9
//                    obj.snp.makeConstraints { make in
//                        make.left.equalToSuperview().offset(120)
//                        make.centerY.equalToSuperview()
//                        make.width.equalTo(0)
//                        make.height.equalTo(10)
//                    }
//                }
//
//                let progressHintLabel = UILabel().then { obj in
//                    v.addSubview(obj)
//                    obj.textColor = bgColor9
//                    obj.font = kRegularFont(12)
//                    obj.text = "0% "
//                    obj.snp.makeConstraints { make in
//                        make.left.equalTo(progressView.snp.right).offset(5)
//                        make.centerY.equalToSuperview()
//                    }
//                }
//
//            }else{
                let contentLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    obj.font = kMediumFont(12)
                    obj.textColor = blackTextColor81
                    if i == 0{
                        detailInfosLabel = obj
                        obj.text = model?.commercialInfo ?? "--"
                    }else if i == 1{
                        possibilityLabel = obj
                        obj.text = (model?.probability ?? "--")+"%"
                    }else if i == 2{
                        perosonLabel = obj
                        obj.text = model?.followerName ?? "--"
                    }else if i == 3{
                        commitDateLabel = obj
                        obj.text = model?.productDate ?? "--"
                    }else if i == 4{
                        productTypeLabel = obj
                        obj.text = model?.productTypeName ?? "--"
                    }else if i == 5{
                        productNameLabel = obj
                        obj.text = model?.productName ?? "--"
                    }else if i == 6{
                        productNumLabel = obj
                        obj.text = model?.productAmount ?? "--"
                    }else if i == 7{
                        productMoneyLabel = obj
                        obj.text = (model?.commercialMoney ?? "--")
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(120)
                        make.centerY.equalToSuperview()
                    }
                }
//            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        

        // Do any additional setup after loading the view.
    }
    
    @objc func addClick(_ button:UIButton){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
