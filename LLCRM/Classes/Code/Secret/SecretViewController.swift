//
//  SecretViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class SecretViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "隐私"
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let hints = ["通讯录隐私设置", "工作提醒设置", "日程同步设置"]
        for i in 0..<hints.count{
            let bgView = UIView().then { obj in
                headView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: i*50, width: Int(kScreenWidth), height: 50)
            }
            
            let hintLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.text = hints[i]
                obj.font = kMediumFont(16)
                obj.textColor = blackTextColor49
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(16)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor20
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
