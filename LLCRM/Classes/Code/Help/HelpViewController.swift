//
//  HelpViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class HelpViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "帮助中心"
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let topView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.height.equalTo(190)
                make.width.equalTo(kScreenWidth)
            }
        }
        
        let names = ["零瓴客服", "服务热线"]
        let titles = ["快速入门", "常见问题", "意见反馈", "系统状态"]
        
        for i in 0..<2{
            let bgView = UIView().then { obj in
                topView.addSubview(obj)
                obj.frame = CGRect(x: (kScreenWidth/2.0)*CGFloat(i), y: 0, width: kScreenWidth/2.0, height: 190)
            }
            
            let imageV = UIImageView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = .red
                obj.layer.cornerRadius = 30
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(49)
                    make.centerX.equalToSuperview()
                    make.width.height.equalTo(60)
                }
            }
            
            let nameLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.text = names[i]
                obj.textColor = .white
                obj.font = kARegularFont(14)
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(10)
                    make.centerX.equalToSuperview()
                }
            }
        }
        
        let listView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(topView.snp.bottom).offset(0)
                make.height.equalTo(200)
            }
        }
        
        for i in 0..<4{
            let listContentView = UIView().then { obj in
                listView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: i*50, width: Int(kScreenWidth), height: 50)
            }
            
            let nameLabel = UILabel().then { obj in
                listContentView.addSubview(obj)
                obj.text = titles[i]
                obj.textColor = blackTextColor42
                obj.font = kARegularFont(16)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                listContentView.addSubview(obj)
                obj.backgroundColor = lineColor18
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.right.equalToSuperview().offset(-25)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let button = UIButton().then { obj in
                listContentView.addSubview(obj)
                obj.tag = 1000+i
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
