//
//  CheckOpinionTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class CheckOpinionTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initUI(){
        self.selectionStyle = .none
        let lineView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        let outPointView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.layer.cornerRadius = 7.5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.top.equalToSuperview().offset(0)
                make.width.height.equalTo(15)
            }
        }
        
        let pointView = UIView().then { obj in
            outPointView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 3.5
            obj.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.width.height.equalTo(7)
            }
        }
        
        let infosLabel = UILabel().then { obj in
            contentView.addSubview(obj)

            let hintAttribute = NSMutableAttributedString(string:"发起人 ")
            hintAttribute.yy_color = blackTextColor8
            hintAttribute.yy_font = kMediumFont(13)
            
            let personAttribute = NSMutableAttributedString(string: "王小虎/研发部")
            personAttribute.yy_color = blackTextColor82
            personAttribute.yy_font = kMediumFont(12)
            
            hintAttribute.append(personAttribute)
            
            
            obj.attributedText = hintAttribute
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(outPointView.snp.right).offset(5)
                make.centerY.equalTo(outPointView)
            }
        }
        
        let recordContentView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor19
            obj.layer.cornerRadius = 6
            obj.snp.makeConstraints { make in
                make.left.equalTo(lineView.snp.right).offset(27)
                make.right.equalToSuperview().offset(-30)
                make.top.equalTo(infosLabel.snp.bottom).offset(10)
                make.bottom.equalToSuperview().offset(-15)
            }
        }
        
        let contentLabel = UILabel().then { obj in
            recordContentView.addSubview(obj)
            obj.text = "由 王小虎 提交数据 2022.05.16  10:30"
            obj.numberOfLines = 2
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor33
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(10)
                make.right.bottom.equalToSuperview().offset(-10)
            }
        }
    }

}
