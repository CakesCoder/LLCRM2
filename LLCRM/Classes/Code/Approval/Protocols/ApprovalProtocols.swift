//
//  ApprovalProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class ApprovalProtocols: NSObject {

}

protocol ApprovalViewProtocols: AnyObject {
    var presenter: ApprovalPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterApprovalViewReceiveData()
    func didPresenterApprovalViewReceiveError()
}

protocol ApprovalPresenterProtocols: AnyObject{
    var view: ApprovalViewProtocols? { get set }
    var router: ApprovalRouterProtocols? { get set }
    var interactor: ApprovalInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorApprovalReceiveData()
    func didInteractorApprovalReceiveError()
}

protocol ApprovalInteractorProtocols: AnyObject {
    var presenter: ApprovalPresenterProtocols? { get set }
    var entity: ApprovalEntityProtocols? { get set }
    
    func interactorApprovalRetrieveData()
    func entityApprovalRetrieveData()
    func didEntityApprovalReceiveData()
    func didEntityApprovalReceiveError()
}

protocol ApprovalEntityProtocols: AnyObject {
    var interactor: ApprovalInteractorProtocols? { get set }
    
    func retrieveApprovalData()
    func didApprovalReceiveData()
}

protocol ApprovalRouterProtocols: AnyObject {
    func pushToAddApprpval(from previousView: UIViewController)
}
