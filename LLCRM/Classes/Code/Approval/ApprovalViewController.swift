//
//  ApprovalViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit
import YYText_swift

class ApprovalViewController: BaseViewController {
    
    var presenter: ApprovalPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        buildUI()
    }
    
    func buildUI(){
        self.navigationItem.title = "审批"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ApprovalViewController.backClick))
//        navigationItem.rightBarButtonItem = UIBarButtonItem.createAddBarbuttonItem(name: "提交", target: self, action: #selector(ApprovalViewController.addClick))
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let searchHintLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "发送一个审批"
            obj.textColor = blackTextColor26
            obj.font = kAMediumFont(18)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(topLineView.snp.bottom).offset(15)
            }
        }
        
        let searchView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor5
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(searchHintLabel.snp.bottom).offset(15)
                make.height.equalTo(35)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(30)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.backgroundColor = bgColor5
            obj.font = kRegularFont(12)
            obj.placeholder = "搜索"
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(0)
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
                make.height.equalTo(30)
            }
        }
        
        let tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
            obj.register(ApprovalTableViewCell.self, forCellReuseIdentifier: "ApprovalTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(searchView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
    }
    
    func cofing(){
        let router = ApprovalRouter()
        
        presenter = ApprovalPresenter()
        
        presenter?.router = router
        
        let entity = ApprovalEntity()
        
        let interactor = ApprovalInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
//    @objc func addClick(){
//
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ApprovalViewController: ApprovalViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterApprovalViewReceiveData() {
        
    }
    
    func didPresenterApprovalViewReceiveError() {
        
    }
}

extension ApprovalViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ApprovalTableViewCell", for: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.router?.pushToAddApprpval(from: self)
    }
}
