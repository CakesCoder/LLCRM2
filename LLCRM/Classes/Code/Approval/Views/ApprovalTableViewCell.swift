//
//  ApprovalTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit

class ApprovalTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "客户来司住宿申请-零瓴软件"
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.centerY.equalToSuperview()
            }
        }
        
        
    }
}
