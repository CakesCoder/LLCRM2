//
//  ApprovalRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class ApprovalRouter: ApprovalRouterProtocols {
    
    func pushToAddApprpval(from previousView: UIViewController) {
        let nextView = AddApprpvalViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }

}
