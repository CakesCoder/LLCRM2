//
//  ApprovalInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class ApprovalInteractor: ApprovalInteractorProtocols {
    var presenter: ApprovalPresenterProtocols?
    
    var entity: ApprovalEntityProtocols?
    
    func interactorApprovalRetrieveData() {
        
    }
    
    func entityApprovalRetrieveData() {
        
    }
    
    func didEntityApprovalReceiveData() {
        
    }
    
    func didEntityApprovalReceiveError() {
        
    }
    

}
