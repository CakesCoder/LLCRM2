//
//  ApprovalPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class ApprovalPresenter: ApprovalPresenterProtocols {
    var view: ApprovalViewProtocols?
    
    var router: ApprovalRouterProtocols?
    
    var interactor: ApprovalInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorApprovalReceiveData() {
        
    }
    
    func didInteractorApprovalReceiveError() {
        
    }
}
