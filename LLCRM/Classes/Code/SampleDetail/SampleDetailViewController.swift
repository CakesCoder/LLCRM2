//
//  SampleDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
//import JXSegmentedView
//import JXPagingView
import HandyJSON
import PKHUD

class SampleDetailViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
//    var presenter: PriceDetailPresenterProtocols?
    
    var nameLabel:UILabel?
    var codeLabel:UILabel?
    var personNameLabel:UILabel?
    var departmentLabel:UILabel?
    var timeLabel:UILabel?
    var model:DeviceSampleVoListModel?
    var dataModel:SampleListModel?
    
    var presenter: SampleDetailPresenterProtocols?

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        navigationItem.title = "样品信息详情"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SampleDetailViewController.backClick))

        // Do any additional setup after loading the view.
        
        view.backgroundColor = lineColor
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = model?.customerName ?? "暂无"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
            }
        }

        
        codeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let codeHintAttribute = NSMutableAttributedString(string:"选型单号：")
            codeHintAttribute.yy_color = blacktextColor52
            codeHintAttribute.yy_font = kMediumFont(13)
            
            let codeCotentAttribute = NSMutableAttributedString(string: model?.sampleNumber ?? "暂无")
            codeCotentAttribute.yy_color = bluebgColor
            codeCotentAttribute.yy_font = kMediumFont(13)
            codeHintAttribute.append(codeCotentAttribute)
            obj.attributedText = codeHintAttribute
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        personNameLabel = UILabel().then { obj in
            headView.addSubview(obj)

            let personNameHintAttribute = NSMutableAttributedString(string:"创建人：")
            personNameHintAttribute.yy_color = blacktextColor52
            personNameHintAttribute.yy_font = kMediumFont(13)

            let personNameCotentAttribute = NSMutableAttributedString(string: model?.RFQCreateBy ?? "暂无")
            personNameCotentAttribute.yy_color = blackTextColor80
            personNameCotentAttribute.yy_font = kMediumFont(13)
            personNameHintAttribute.append(personNameCotentAttribute)
            obj.attributedText = personNameHintAttribute

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(codeLabel!.snp.bottom).offset(10)
            }
        }
        
        departmentLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let clientNameHintAttribute = NSMutableAttributedString(string:"创建部门：")
            clientNameHintAttribute.yy_color = blacktextColor52
            clientNameHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let clientNameCotentAttribute = NSMutableAttributedString(string:model.productList?[0].productName ?? "--")
//                clientNameCotentAttribute.yy_color = bluebgColor
//                clientNameCotentAttribute.yy_font = kMediumFont(13)
//                clientNameHintAttribute.append(clientNameCotentAttribute)
//                obj.attributedText = clientNameHintAttribute
//            }else{
            let clientNameCotentAttribute = NSMutableAttributedString(string: model?.RFQCreateDept ?? "暂无")
                clientNameCotentAttribute.yy_color = bluebgColor
                clientNameCotentAttribute.yy_font = kMediumFont(13)
                clientNameHintAttribute.append(clientNameCotentAttribute)
                obj.attributedText = clientNameHintAttribute
//            }
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(personNameLabel!.snp.bottom).offset(10)
            }
        }
        
        timeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let timeHintAttribute = NSMutableAttributedString(string:"最后修改时间：")
            timeHintAttribute.yy_color = blacktextColor52
            timeHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let timeCotentAttribute = NSMutableAttributedString(string:model.productList?[0].validTimeStart ?? "--")
//                timeCotentAttribute.yy_color = blackTextColor80
//                timeCotentAttribute.yy_font = kMediumFont(13)
//                timeHintAttribute.append(timeCotentAttribute)
//                obj.attributedText = timeHintAttribute
//            }else{
            let timeCotentAttribute = NSMutableAttributedString(string:model?.RFQCreateDept ?? "暂无")
                timeCotentAttribute.yy_color = blackTextColor80
                timeCotentAttribute.yy_font = kMediumFont(13)
                timeHintAttribute.append(timeCotentAttribute)
                obj.attributedText = timeHintAttribute
//            }
          
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(departmentLabel!.snp.bottom).offset(10)
            }
        }
        
        
        let lineView2 = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(timeLabel!.snp.bottom).offset(10 )
                make.height.equalTo(0.5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        headView.addSubview(self.segmentedView!)
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(lineView2.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["样品信息", "技术规格", "项目信息", "联系人信息", "环保认证", "样品进度", "样品反馈"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
//        segmentedView?.delegate = self
//        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        headView.addSubview(pagingView!)
        
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
        
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        _ = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        _ = UIView().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight())
            }
        })
        
        let editorImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "simple_detail_editor")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview().offset(-45)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let editorLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "编辑"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(editorImageV.snp.bottom).offset(5)
                make.centerX.equalToSuperview().offset(-45)
            }
        }
        
        let editorButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalToSuperview().offset(-45)
            }
        }
        
        let delImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "simple_detail_delete")
            obj.snp.makeConstraints { make in
//                make.left.equalTo(editorImageV.snp.right).offset(68)
                make.centerX.equalToSuperview().offset(45)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let delLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "删除"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(delImageV.snp.bottom).offset(5)
                make.centerX.equalTo(delImageV)
            }
        }
        
        let delButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.addTarget(self, action: #selector(delClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(delImageV)
            }
        }
        cofing()
    }
    
    @objc func delClick(){
        presenter?.presenterRequestGetSampleDetailDel(by: ["id":model?.id ?? ""])
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = SampleDetailRouter()
        
        presenter = SampleDetailPresenter()
        
        presenter?.router = router
        
        let entity = SampleDetailEntity()
        
        let interactor = SampleDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        presenter?.presenterRequestGetSampleList(by: ["current":current, "size":10])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SampleDetailViewController:SampleDetailViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetSampleDetailPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataDic = dict["data"] as? [String:Any]{
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetSampleDetailPresenterReceiveError(error: MyError?) {
        
    }
}

extension SampleDetailViewController: JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = SampleInfosDetailViewController()
            vc.model = model
            return vc
        }else if index == 1{
            let vc = SampleSkillDetailViewController()
            vc.model = model
            return vc
        }else if index == 2{
            let vc = SampleProductInfosDetailViewController()
            vc.model = model
            return vc
        }else if index == 3{
            let vc = SempleLinkManeDetailViewController()
            vc.model = model
            return vc
        }else if index == 4{
            let vc = SampleEnvironmentDetailViewController()
            return vc
        }else if index == 5{
            let vc = SempleProgressDetailViewController()
            return vc
        }
        let vc = SempleFeedbackDetailViewController()
        return vc
        
        
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 7
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }

    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}
