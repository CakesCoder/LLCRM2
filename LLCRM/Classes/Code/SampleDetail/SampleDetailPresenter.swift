//
//  SampleDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class SampleDetailPresenter: SampleDetailPresenterProtocols {
    var view: SampleDetailViewProtocols?
    
    var router: SampleDetailRouterProtocols?
    
    var interactor: SampleDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetSampleDetailDel(by params: Any?) {
        interactor?.presenterRequestSampleDetailDel(by: params)
    }
    
    func didGetSampleDetailDelInteractorReceiveData(by params: Any?) {
        view?.didGetSampleDetailPresenterReceiveData(by: params)
    }
    
    func didGetSampleDetailDelInteractorReceiveError(error: MyError?) {
        view?.didGetSampleDetailPresenterReceiveError(error: error)
    }
}
