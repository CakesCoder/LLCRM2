//
//  SampleDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class SampleDetailEntity: SampleDetailEntityProtocols {
    var interactor: SampleDetailInteractorProtocols?
    
    func didSampleDetailDelReceiveData(by params: Any?) {
        interactor?.didEntitySampleDetailDelReceiveData(by: params)
    }
    
    func didSampleDetailDelReceiveError(error: MyError?) {
        interactor?.didEntitySampleDetailDelReceiveError(error: error)
    }
    
    func getSamplDetailDelRequest(by params: Any?) {
        LLNetProvider.request(.sampleDel(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didSampleDetailDelReceiveData(by:jsonData)
            case .failure(_):
                self.didSampleDetailDelReceiveError(error: .requestError)
            }
        }
    }
    
    
}
