//
//  SampleDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class SampleDetailInteractor: SampleDetailInteractorProtocols {
    var presenter: SampleDetailPresenterProtocols?
    
    var entity: SampleDetailEntityProtocols?
    
    func presenterRequestSampleDetailDel(by params: Any?) {
        entity?.getSamplDetailDelRequest(by: params)
    }
    
    func didEntitySampleDetailDelReceiveData(by params: Any?) {
        presenter?.didGetSampleDetailDelInteractorReceiveData(by: params)
    }
    
    func didEntitySampleDetailDelReceiveError(error: MyError?) {
        presenter?.didGetSampleDetailDelInteractorReceiveError(error: error)
    }
    
    
}
