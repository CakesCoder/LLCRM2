//
//  SampleDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class SampleDetailProtocols: NSObject {

}

protocol SampleDetailViewProtocols: AnyObject {
    var presenter: SampleDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetSampleDetailPresenterReceiveData(by params: Any?)
    func didGetSampleDetailPresenterReceiveError(error: MyError?)
}

protocol SampleDetailPresenterProtocols: AnyObject{
    var view: SampleDetailViewProtocols? { get set }
    var router: SampleDetailRouterProtocols? { get set }
    var interactor: SampleDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetSampleDetailDel(by params: Any?)
    func didGetSampleDetailDelInteractorReceiveData(by params: Any?)
    func didGetSampleDetailDelInteractorReceiveError(error: MyError?)
}

protocol SampleDetailInteractorProtocols: AnyObject {
    var presenter: SampleDetailPresenterProtocols? { get set }
    var entity: SampleDetailEntityProtocols? { get set }
    
    func presenterRequestSampleDetailDel(by params: Any?)
    func didEntitySampleDetailDelReceiveData(by params: Any?)
    func didEntitySampleDetailDelReceiveError(error: MyError?)
}

protocol SampleDetailEntityProtocols: AnyObject {
    var interactor: SampleDetailInteractorProtocols? { get set }
    
    func didSampleDetailDelReceiveData(by params: Any?)
    func didSampleDetailDelReceiveError(error: MyError?)
    func getSamplDetailDelRequest(by params: Any?)
}

protocol SampleDetailRouterProtocols: AnyObject {
    func pushToAddSample(from previousView: UIViewController)
    func pushToSampleDetail(from previousView: UIViewController)
}
