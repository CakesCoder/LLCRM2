//
//  AttendanceRuleViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/22.
//

import UIKit

class AttendanceRuleViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "考勤规则"
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let hintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "考勤"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        }
        
        let contentLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            obj.text = "打卡人员：全部人员"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.numberOfLines = 0
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(50)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        }
        let contentLabel2 = UILabel().then { obj in
            headView.addSubview(obj)
            
            obj.text = "打卡范围：星期一、星期二、星期三、星期四、星期五"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.numberOfLines = 0
            obj.snp.makeConstraints { make in
                make.top.equalTo(contentLabel.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let contentLabel3 = UILabel().then { obj in
            headView.addSubview(obj)
            
            obj.text = "打卡时间：09:00:00-18:00:00"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.numberOfLines = 0
            obj.snp.makeConstraints { make in
                make.top.equalTo(contentLabel2.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let contentLabel4 = UILabel().then { obj in
            headView.addSubview(obj)
            
            obj.text = "打卡地点：深圳市南山区桃源街道坪山一路3号大园工业区北区"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.numberOfLines = 0
            obj.snp.makeConstraints { make in
                make.top.equalTo(contentLabel3.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let contentLabel5 = UILabel().then { obj in
            headView.addSubview(obj)
            
            obj.text = "打卡wifi：linglingsfot-5G"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.numberOfLines = 0
            obj.snp.makeConstraints { make in
                make.top.equalTo(contentLabel4.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
