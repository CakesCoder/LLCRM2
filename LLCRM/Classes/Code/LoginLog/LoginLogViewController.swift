//
//  LoginLogViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class LoginLogViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "登录日志"
        // Do any additional setup after loading the view.
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            if #available(iOS 11.0, *) {
                obj.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
            } else {
                // Fallback on earlier versions
            }
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
            obj.backgroundColor = .white
            obj.register(LoginLogTableViewCell.self, forCellReuseIdentifier: "LoginLogTableViewCell")
//            obj.register(CollectImageTableViewCell.self, forCellReuseIdentifier: "CollectImageTableViewCell")
//            obj.register(CollectRecordTableViewCell2.self, forCellReuseIdentifier: "CollectRecordTableViewCell2")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginLogViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 105
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LoginLogTableViewCell", for: indexPath) as! LoginLogTableViewCell
        return cell
    }
    
    
}
