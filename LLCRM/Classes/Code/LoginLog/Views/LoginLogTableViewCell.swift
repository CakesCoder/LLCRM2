//
//  LoginLogTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class LoginLogTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
                make.width.height.equalTo(20)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "Windows桌面客户端"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor48
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(12)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let subLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "操作：通过扫码登录"
            obj.textColor = blackTextColor47
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(48)
                make.top.equalTo(titleLabel.snp.bottom).offset(15)
            }
        }
        
        _ = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "登录时间：2022-05-27  09:30"
            obj.textColor = blackTextColor47
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(48)
                make.top.equalTo(subLabel.snp.bottom).offset(5)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
