//
//  MyClientModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit

class MyClientModel: BaseModel {
//    var isSelected:Bool? = false
    var code:Int?
    var data:[MyClientModelData]?
    var message:String?
    
    required init() {
    }
}

class MyClientModelData: BaseModel{
    var clientId:String?
    var clientName:String?
    var isSelected:Bool? = false
    var noCloseCount:String?
    
    required init(){}
}
