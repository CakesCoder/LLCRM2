//
//  MyClientEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class MyClientEntity: MyClientEntityProtocols {
    var interactor: MyClientInteractorProtocols?
    
    func didMyClientReceiveData(by params: Any?) {
        interactor?.didEntityMyClientReceiveData(by: params)
    }
    
    func didMyClientReceiveError(error: MyError?) {
        interactor?.didEntityMyClientReceiveError(error: error)
    }
    
    func getMyClientRequest(by params: Any?) {
        LLNetProvider.request(.getAllClientIdName(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didMyClientReceiveData(by:jsonData)
            case .failure(_):
                self.didMyClientReceiveError(error: .requestError)
            }
        }
    }
    
    func getBusinessRequest(by params: Any?) {
        LLNetProvider.request(.getAllClientIdName(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didMyClientReceiveData(by:jsonData)
            case .failure(_):
                self.didMyClientReceiveError(error: .requestError)
            }
        }
    }
    
    func getLinkManRequest(by params: Any?) {
        LLNetProvider.request(.getAllClientIdName(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didMyClientReceiveData(by:jsonData)
            case .failure(_):
                self.didMyClientReceiveError(error: .requestError)
            }
        }
    }
    
    func getPriceProductRequest(by params: Any?) {
        LLNetProvider.request(.getPriceProductList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didMyClientReceiveData(by:jsonData)
            case .failure(_):
                self.didMyClientReceiveError(error: .requestError)
            }
        }
    }
    
    

}
