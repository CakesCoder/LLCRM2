//
//  MyClientPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class MyClientPresenter: MyClientPresenterProtocols {
    var view: MyClientViewProtocols?
    
    var router: MyClientRouterProtocols?
    
    var interactor: MyClientInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetMyClient(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestMyClient(by: params)
    }
    
    func presenterRequestGetMyBusiness(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestMytBusiness(by: params)
    }
    
    func presenterRequestGetMyLinkMan(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestMyLinkMan(by: params)
    }
    
    func presenterRequestGetPriceProduct(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestPriceProduct(by: params)
    }
    
    func didGetMyClientInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetMyClientPresenterReceiveData(by: params)
    }
    
    func didGetMyClientInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetMyClientPresenterReceiveError(error: error)
    }
    
    
}
