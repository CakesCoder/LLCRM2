//
//  MyClientProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class MyClientProtocols: NSObject {

}

protocol MyClientViewProtocols: AnyObject {
    var presenter: MyClientPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetMyClientPresenterReceiveData(by params: Any?)
    func didGetMyClientPresenterReceiveError(error: MyError?)
}

protocol MyClientPresenterProtocols: AnyObject{
    var view: MyClientViewProtocols? { get set }
    var router: MyClientRouterProtocols? { get set }
    var interactor: MyClientInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetMyClient(by params: Any?)
    func presenterRequestGetMyBusiness(by params: Any?)
    func presenterRequestGetMyLinkMan(by params: Any?)
    func presenterRequestGetPriceProduct(by params: Any?)
    func didGetMyClientInteractorReceiveData(by params: Any?)
    func didGetMyClientInteractorReceiveError(error: MyError?)
}

protocol MyClientInteractorProtocols: AnyObject {
    var presenter: MyClientPresenterProtocols? { get set }
    var entity: MyClientEntityProtocols? { get set }
    
    func presenterRequestMyClient(by params: Any?)
    func didEntityMyClientReceiveData(by params: Any?)
    func didEntityMyClientReceiveError(error: MyError?)
    func presenterRequestMytBusiness(by params: Any?)
    func presenterRequestMyLinkMan(by params: Any?)
    func presenterRequestPriceProduct(by params: Any?)
}

protocol MyClientEntityProtocols: AnyObject {
    var interactor: MyClientInteractorProtocols? { get set }
    
    func didMyClientReceiveData(by params: Any?)
    func didMyClientReceiveError(error: MyError?)
    func getMyClientRequest(by params: Any?)
    func getBusinessRequest(by params: Any?)
    func getLinkManRequest(by params: Any?)
    func getPriceProductRequest(by params: Any?)

}

protocol MyClientRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController)
}
