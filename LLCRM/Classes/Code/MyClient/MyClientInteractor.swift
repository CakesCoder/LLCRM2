//
//  MyClientInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class MyClientInteractor: MyClientInteractorProtocols {
    var presenter: MyClientPresenterProtocols?
    
    var entity: MyClientEntityProtocols?
    
    func presenterRequestMyClient(by params: Any?) {
        entity?.getMyClientRequest(by: params)
    }
    
    func didEntityMyClientReceiveData(by params: Any?) {
        presenter?.didGetMyClientInteractorReceiveData(by: params)
    }
    
    func didEntityMyClientReceiveError(error: MyError?) {
        presenter?.didGetMyClientInteractorReceiveError(error: error)
    }
    
    func presenterRequestMytBusiness(by params: Any?){
        entity?.getBusinessRequest(by: params)
    }
    
    func presenterRequestMyLinkMan(by params: Any?){
        entity?.getLinkManRequest(by: params)
    }
    
    func presenterRequestPriceProduct(by params: Any?){
        entity?.getPriceProductRequest(by: params)
    }
}
