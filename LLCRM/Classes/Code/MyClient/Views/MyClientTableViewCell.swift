//
//  MyClientTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit

class MyClientTableViewCell: UITableViewCell {
    
    var selectedButton:UIImageView?
    var titleLabel:UILabel?
    var userNameLabel:UILabel?
    
    var addClinetNameDataModel:AddClinetNameDataModel?{
        didSet{
            if addClinetNameDataModel?.isSelected == true{
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClinetNameDataModel?.Name ?? "暂无内容"
            userNameLabel?.text = addClinetNameDataModel?.StartDate ?? ""
        }
    }
    
    var addClientVisitAddPersonModel:AddClientVisitAddPersonModel?{
        didSet{
//            if addClientVisitAddPersonModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientVisitAddPersonModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientVisitAddPersonModel?.companyName ?? "暂无内容"
            userNameLabel?.text = addClientVisitAddPersonModel?.createTime ?? ""
        }
    }
    
    var productmodel:MyClientModelData?{
        didSet{
//            if productmodel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if productmodel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = productmodel?.clientName ?? "暂无内容"
            userNameLabel?.text = ""
        }
    }
    
    var addClientSelectedTechnologyModel:AddClientSelectedTechnologyModel?{
        didSet{
//            if addClientSelectedTechnologyModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelectedTechnologyModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelectedTechnologyModel?.name ?? "暂无内容"
            userNameLabel?.text = ""
        }
    }
    
    var addClientSelectedBussinessNameModel:AddClientSelectedBussinessNameModel?{
        didSet{
//            if addClientSelectedBussinessNameModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelectedBussinessNameModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelectedBussinessNameModel?.clientName ?? "暂无内容"
            userNameLabel?.text = ""
        }
    }
    
    var addClientSelectedPhaseModel:AddClientSelectedPhaseModel?{
        didSet{
//            if addClientSelectedPhaseModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelectedPhaseModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelectedPhaseModel?.stageName ?? "暂无内容"
            userNameLabel?.text = ""
        }
    }
    
    var addClientSelectedNatureModel:AddClientSelectedNatureModel?{
        didSet{
//            if addClientSelectedNatureModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelectedNatureModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelectedNatureModel?.name ?? "暂无内容"
            userNameLabel?.text = addClientSelectedNatureModel?.createTime ?? ""
        }
    }
    
    var addClientSelecteArchivesModel: AddClientSelecteArchivesModel?{
        didSet{
//            if addClientSelecteArchivesModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelecteArchivesModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelecteArchivesModel?.clientName ?? "暂无内容"
            userNameLabel?.text = addClientSelecteArchivesModel?.createTime ?? ""
        }
    }
    
    var addClientSelectedDepartMentModel: AddClientSelectedDepartMentModel?{
        didSet{
//            if addClientSelectedDepartMentModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelectedDepartMentModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelectedDepartMentModel?.deptName ?? "暂无内容"
            userNameLabel?.text = addClientSelectedDepartMentModel?.createTime ?? ""
        }
    }
    
    
    var addClientSelectedDepartMentModel2: AddClientSelectedDepartMentModel?{
        didSet{
//            if addClientSelectedDepartMentModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientSelectedDepartMentModel2?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientSelectedDepartMentModel?.name ?? "暂无内容"
            userNameLabel?.text = addClientSelectedDepartMentModel?.createTime ?? ""
        }
    }
    
    var addClientPersonModel:AddClientSelectedPersonModel?{
        didSet{
//            if addClientPersonModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if addClientPersonModel?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientPersonModel?.name ?? "暂无内容"
            userNameLabel?.text = addClientPersonModel?.userName ?? ""
        }
    }
    
    var model:MyClientModelData?{
        didSet{
//            if model?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
//            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
//            }
            if model?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = model?.clientName ?? "暂无内容"
            userNameLabel?.text = ""
        }
    }
    
    var addClientModel:AddClientSelectedModel?{
        didSet{
            if model?.isSelected == true{
//                selectedButton?.setImage(UIImage(named: "btnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "btnSelcted")
            }else{
//                selectedButton?.setImage(UIImage(named: "unBtnSelcted"), for: .normal)
                selectedButton?.image = UIImage(named: "unBtnSelcted")
            }
            titleLabel?.text = addClientModel?.attrName ?? "暂无内容"
            userNameLabel?.text = ""
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        selectedButton = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        })
        
        titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "零瓴软件技术（深圳）有限公司"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor8
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(50)
                make.top.equalToSuperview().offset(20)
                make.right.equalToSuperview().offset(-53)
                make.height.equalTo(15)
            }
        }
        
        userNameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor8
            obj.numberOfLines = 2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(50)
                make.top.equalTo(titleLabel!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-53)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(40)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
