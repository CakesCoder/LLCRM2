//
//  SempleProgressDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
//import JXPagingView

extension SempleProgressDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SempleProgressDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class SempleProgressDetailViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    lazy var popMenView: UIView = {
        let menView = UIView().then { obj in
            obj.frame = CGRect(x: kScreenWidth - 15 - 98, y: 30, width: 81, height: 98)
            // shadowCode
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.09).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = .blue
        }
        
//        let arrowView=UIView(frame: CGRect(x: 0, y: 10, width: 50, height: 50))
        
//        let context = UIGraphicsGetCurrentContext()
//        context?.beginPath()
//        context?.move(to: CGPoint(x: 20, y: 50))
//        context?.addLine(to: CGPoint(x: 10, y: 30))
//        context?.addLine(to: CGPoint(x: 60, y: 20))
//        context?.closePath()
//        context?.fillPath()
//        context?.drawPath(using: .stroke)
        
//        for i in 0..<4{
//            let popButton = UIButton().then { obj in
//                menView.addSubview(obj)
//                obj.frame = CGRect(x: 0, y: i*98, width: 81, height: 98)
//                if i == 0{
//                    obj.setTitle("全部", for: .normal)
//                }else if i == 1{
//                    obj.setTitle("我发出的", for: .normal)
//                }else if i == 2{
//                    obj.setTitle("部门发出", for: .normal)
//                }else{
//                    obj.setTitle("部门收到", for: .normal)
//                }
//
//            }
//        }
        
        return menView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, 45)
        }
        
        let headTagView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "样品进度"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setImage(UIImage(named: "work_status"), for: .normal)
            obj.setImage(UIImage(named: "work_status"), for: .selected)
            obj.titleLabel?.font = kMediumFont(24)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let popView = popMenView.then { obj in
            obj.frame = CGRect(x: kScreenWidth - 15 - 81, y: 30, width: 81, height: 98)
            obj.backgroundColor = .red
            headView.addSubview(obj)
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.tableHeaderView = headView
            obj.register(SempleProgressDetailTableViewCell.self, forCellReuseIdentifier: "SempleProgressDetailTableViewCell")
            obj.register(SempleProgressDetailNoDataTableViewCell.self, forCellReuseIdentifier: "SempleProgressDetailNoDataTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SempleProgressDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 108
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return (presenter?.params!.count)!
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "SempleProgressDetailNoDataTableViewCell", for: indexPath) as! SempleProgressDetailNoDataTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "SempleProgressDetailTableViewCell", for: indexPath) as! SempleProgressDetailTableViewCell
        return cell
    }
}
