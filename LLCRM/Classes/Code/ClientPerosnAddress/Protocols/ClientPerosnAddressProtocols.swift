//
//  ClientPerosnAddressProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

protocol ClientPerosnAddressViewProtocols: AnyObject {
    var presenter: ClientPerosnAddressPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddClientPresenterReceiveData(by params: Any?)
    func didGetAddClientPresenterReceiveError(error: MyError?)
}

protocol ClientPerosnAddressPresenterProtocols: AnyObject{
    var view: ClientPerosnAddressViewProtocols? { get set }
    var router: ClientPerosnAddressRouterProtocols? { get set }
    var interactor: ClientPerosnAddressInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddClient(by params: Any?)
    func didGetAddClientInteractorReceiveData(by params: Any?)
    func didGetAddClientInteractorReceiveError(error: MyError?)
}

protocol ClientPerosnAddressInteractorProtocols: AnyObject {
    var presenter: ClientPerosnAddressPresenterProtocols? { get set }
    var entity: ClientPerosnAddressEntityProtocols? { get set }
    
    func presenterRequestAddClient(by params: Any?)
    func didEntityAddClientReceiveData(by params: Any?)
    func didEntityAddClientReceiveError(error: MyError?)
}

protocol ClientPerosnAddressEntityProtocols: AnyObject {
    var interactor: ClientPerosnAddressInteractorProtocols? { get set }
    
    func didAddClientnReceiveData(by params: Any?)
    func didAddClientReceiveError(error: MyError?)
    func getAddClientRequest(by params: Any?)
}

protocol ClientPerosnAddressRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
}


