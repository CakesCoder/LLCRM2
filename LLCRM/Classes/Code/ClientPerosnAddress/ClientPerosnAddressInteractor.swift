//
//  ClientPerosnAddressInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnAddressInteractor: ClientPerosnAddressInteractorProtocols {
    var presenter: ClientPerosnAddressPresenterProtocols?
    
    var entity: ClientPerosnAddressEntityProtocols?
    
    func presenterRequestAddClient(by params: Any?) {
        
    }
    
    func didEntityAddClientReceiveData(by params: Any?) {
        
    }
    
    func didEntityAddClientReceiveError(error: MyError?) {
        
    }
    

}
