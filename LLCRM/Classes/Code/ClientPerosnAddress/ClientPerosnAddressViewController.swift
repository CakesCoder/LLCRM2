//
//  ClientPerosnAddressViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit
//import JXPagingView

extension ClientPerosnAddressViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ClientPerosnAddressViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ClientPerosnAddressViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var model:ClientPerosonDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "预设业务类型"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["办公地址：", "", "联系人：", "联系方式："]
        
//        let contentInfos = ["广东省深圳市南山区桃源街道平山社区", "--", "王小虎", "138 2938 9893"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                var contentStr = ""
                if i == 0{
                    contentStr = model?.clientClueContactSimpleParamList?[0].address ?? "--"
                }else if i == 1{
                    contentStr = "--"
                }else if i == 2{
                    contentStr = model?.clientClueContactSimpleParamList?[0].name ?? "--"
                }else if i == 3{
                    contentStr = model?.clientClueContactSimpleParamList?[0].phone ?? "--"
                }
                let hintAttribute = NSMutableAttributedString(string:contentStr)
                hintAttribute.yy_color = blackTextColor81
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
            }
            
            
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
