//
//  ClientPerosnAddressPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnAddressPresenter: ClientPerosnAddressPresenterProtocols {
    var view: ClientPerosnAddressViewProtocols?
    
    var router: ClientPerosnAddressRouterProtocols?
    
    var interactor: ClientPerosnAddressInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddClient(by params: Any?) {
        
    }
    
    func didGetAddClientInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetAddClientInteractorReceiveError(error: MyError?) {
        
    }
}
