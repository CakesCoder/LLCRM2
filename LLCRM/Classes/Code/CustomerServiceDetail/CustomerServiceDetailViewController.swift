//
//  CustomerServiceDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit
//import JXSegmentedView
//import JXPagingViewr
import HandyJSON
import PKHUD
import JFPopup

class CustomerServiceDetailViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var presenter: CustomerServiceDetailPresenterProtocols?
    var recordModel: CustomerServiceRecordsModel?
    
    var nameLabel:UILabel?
    var statusLabel:UILabel?
    var codeLabel:UILabel?
    var productLabel:UILabel?
    var personNameLabel:UILabel?
    var timeLabel:UILabel?
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "客诉详情"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(CustomerServiceDetailViewController.backClick))
        
        _ = UIView().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.height.equalTo(10)
            }
        })
        
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = recordModel?.clientName ?? "暂无"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        statusLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = blackTextColor44
            obj.text = "未完成"
            obj.textAlignment = .center
            obj.font = kRegularFont(9)
            obj.textColor = .white
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel!)
                make.width.equalTo(50)
                make.height.equalTo(22)
            }
        }
        
        codeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let codeHintAttribute = NSMutableAttributedString(string:"客诉单号：")
            codeHintAttribute.yy_color = blacktextColor52
            codeHintAttribute.yy_font = kMediumFont(13)
            let codeCotentAttribute = NSMutableAttributedString(string: recordModel?.clientDiscardNo ?? "暂无")
            codeCotentAttribute.yy_color = bluebgColor
            codeCotentAttribute.yy_font = kMediumFont(13)
            codeHintAttribute.append(codeCotentAttribute)
            obj.attributedText = codeHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        productLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let productHintAttribute = NSMutableAttributedString(string:"客诉主题：")
            productHintAttribute.yy_color = blacktextColor52
            productHintAttribute.yy_font = kMediumFont(13)
            let productCotentAttribute = NSMutableAttributedString(string: recordModel?.specDesc ?? "暂无")
            productCotentAttribute.yy_color = blackTextColor80
            productCotentAttribute.yy_font = kMediumFont(13)
            productHintAttribute.append(productCotentAttribute)
            obj.attributedText = productHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(codeLabel!.snp.bottom).offset(10)
            }
        }
        
        personNameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let personNameHintAttribute = NSMutableAttributedString(string:"创建人：")
            personNameHintAttribute.yy_color = blacktextColor52
            personNameHintAttribute.yy_font = kMediumFont(13)
            let personNameCotentAttribute = NSMutableAttributedString(string:"暂无")
            personNameCotentAttribute.yy_color = blackTextColor80
            personNameCotentAttribute.yy_font = kMediumFont(13)
            personNameHintAttribute.append(personNameCotentAttribute)
            obj.attributedText = personNameHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(productLabel!.snp.bottom).offset(10)
            }
        }
        
        timeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let timeHintAttribute = NSMutableAttributedString(string:"创建时间：")
            timeHintAttribute.yy_color = blacktextColor52
            timeHintAttribute.yy_font = kMediumFont(13)
            let timeCotentAttribute = NSMutableAttributedString(string: recordModel?.createTime ?? "暂无")
            timeCotentAttribute.yy_color = blackTextColor80
            timeCotentAttribute.yy_font = kMediumFont(13)
            timeHintAttribute.append(timeCotentAttribute)
            obj.attributedText = timeHintAttribute
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(personNameLabel!.snp.bottom).offset(10)
            }
        }
        
        //        lastTimeLabel = UILabel().then { obj in
        //            headView.addSubview(obj)
        //
        //            obj.snp.makeConstraints { make in
        //                make.left.equalToSuperview().offset(15)
        //                make.right.equalToSuperview().offset(-15)
        //                make.top.equalTo(timeLabel!.snp.bottom).offset(10)
        //            }
        //        }
        
        let lineView2 = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(timeLabel!.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        headView.addSubview(self.segmentedView!)
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(lineView2.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["客诉内容", "客退明细", "分析报告", "结案确认"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        //        segmentedView?.delegate = self
        //        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        
        headView.addSubview(pagingView!)
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
        
        _ = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                //                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        //        let editorImageV = UIImageView().then { obj in
        //            view.addSubview(obj)
        //            obj.image = UIImage(named: "detail_editor")
        //            obj.snp.makeConstraints { make in
        //                make.centerX.equalToSuperview()
        //                make.top.equalTo(tableView.snp.bottom).offset(5)
        //            }
        //        }
        //
        //        let editorLabel = UILabel().then { obj in
        //            view.addSubview(obj)
        //            obj.text = "编辑"
        //            obj.textColor = blackTextColor53
        //            obj.font = kMediumFont(10   )
        //            obj.snp.makeConstraints { make in
        //                make.top.equalTo(editorImageV.snp.bottom).offset(5)
        //                make.centerX.equalToSuperview()
        //            }
        //        }
        //
        //        let editorButton = UIButton().then { obj in
        //            view.addSubview(obj)
        //            obj.snp.makeConstraints { make in
        //                make.top.equalTo(tableView.snp.bottom).offset(5)
        //                make.bottom.equalToSuperview().offset(-10)
        //                make.width.equalTo(50)
        //                make.centerX.equalToSuperview()
        //            }
        //        }
        //
        //        let recordImageV = UIImageView().then { obj in
        //            view.addSubview(obj)
        //            obj.image = UIImage(named: "detail_record")
        //            obj.snp.makeConstraints { make in
        //                make.right.equalTo(editorImageV.snp.left).offset(-68)
        //                make.top.equalTo(tableView.snp.bottom).offset(5)
        //            }
        //        }
        //
        //        let recordLabel = UILabel().then { obj in
        //            view.addSubview(obj)
        //            obj.text = "沟通记录"
        //            obj.textColor = blackTextColor53
        //            obj.font = kMediumFont(10   )
        //            obj.snp.makeConstraints { make in
        //                make.top.equalTo(recordImageV.snp.bottom).offset(5)
        //                make.centerX.equalTo(recordImageV)
        //            }
        //        }
        //
        //        let recordButton = UIButton().then { obj in
        //            view.addSubview(obj)
        //            obj.snp.makeConstraints { make in
        //                make.top.equalTo(tableView.snp.bottom).offset(5)
        //                make.bottom.equalToSuperview().offset(-10)
        //                make.width.equalTo(50)
        //                make.centerX.equalTo(recordImageV)
        //            }
        //        }
        //
        //
        //        let moreImageV = UIImageView().then { obj in
        //            view.addSubview(obj)
        //            obj.image = UIImage(named: "detail_more")
        //            obj.snp.makeConstraints { make in
        //                make.left.equalTo(editorImageV.snp.right).offset(68)
        //                make.top.equalTo(tableView.snp.bottom).offset(5)
        //            }
        //        }
        //
        //        let moreLabel = UILabel().then { obj in
        //            view.addSubview(obj)
        //            obj.text = "更多"
        //            obj.textColor = blackTextColor53
        //            obj.font = kMediumFont(10   )
        //            obj.snp.makeConstraints { make in
        //                make.top.equalTo(moreImageV.snp.bottom).offset(5)
        //                make.centerX.equalTo(moreImageV)
        //            }
        //        }
        //
        //        let moreButton = UIButton().then { obj in
        //            view.addSubview(obj)
        //            obj.snp.makeConstraints { make in
        //                make.top.equalTo(tableView.snp.bottom).offset(5)
        //                make.bottom.equalToSuperview().offset(-10)
        //                make.width.equalTo(50)
        //                make.centerX.equalTo(moreImageV)
        //            }
        //        }
        
        cofing()
            
        presenter?.presenterRequestGetCustomerServiceDetail(by: ["id": recordModel?.complaintId])
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = CustomerServiceDetailRouter()
        
        presenter = CustomerServiceDetailPresenter()
        
        presenter?.router = router
        
        let entity = CustomerServiceDetailEntity()
        
        let interactor = CustomerServiceDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
}

extension CustomerServiceDetailViewController: CustomerServiceDetailViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetCustomerServiceDetailPresenterReceiveData(by params: Any?) {
        printTest(params)
//        if let dict = params as? [String:Any]{
//            if dict["code"] as! Int == 200{
//                if let dataModel = JSONDeserializer<CustomerServiceDetailModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
//                    printTest(dataModel)
//                }
//            }else{
//                HUD.flash(.label(dict["message"] as? String), delay: 2)
//            }
//        }else{
//            HUD.flash(.label("data为nil"), delay: 2)
//        }
    }
    
    func didGetCustomerServiceDetailPresenterReceiveError(error: MyError?) {
        
    }
}

extension CustomerServiceDetailViewController : JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = CustomerServiceContentViewController()
            vc.recordModel = recordModel
//            vc.model = self.model
            return vc
        }else if index == 1{
            let vc = CustomerServiceInfosDdetailViewController()
            vc.recordModel = recordModel
//            vc.model = self.model
            return vc
        }else if index == 2{
            let vc = CustomerServiceRepoetViewController()
            vc.recordModel = recordModel
            return vc
        }
        let vc = CustomerServiceCommitViewController()
        vc.recordModel = recordModel
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 4
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
//    zZ
    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}
