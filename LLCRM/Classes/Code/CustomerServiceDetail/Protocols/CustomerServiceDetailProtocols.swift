//
//  CustomerServiceDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit

class CustomerServiceDetailProtocols: NSObject {

}

protocol CustomerServiceDetailViewProtocols: AnyObject {
    var presenter: CustomerServiceDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetCustomerServiceDetailPresenterReceiveData(by params: Any?)
    func didGetCustomerServiceDetailPresenterReceiveError(error: MyError?)
}

protocol CustomerServiceDetailPresenterProtocols: AnyObject{
    var view: CustomerServiceDetailViewProtocols? { get set }
    var router: CustomerServiceDetailRouterProtocols? { get set }
    var interactor: CustomerServiceDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetCustomerServiceDetail(by params: Any?)
    func didGetCustomerServiceDetailInteractorReceiveData(by params: Any?)
    func didGetCustomerServiceDetailInteractorReceiveError(error: MyError?)
}

protocol CustomerServiceDetailInteractorProtocols: AnyObject {
    var presenter: CustomerServiceDetailPresenterProtocols? { get set }
    var entity: CustomerServiceDetailEntityProtocols? { get set }
    
    func presenterRequestCustomerServiceDetail(by params: Any?)
    func didEntityCustomerServiceDetailReceiveData(by params: Any?)
    func didEntityCustomerDetailServiceReceiveError(error: MyError?)
}

protocol CustomerServiceDetailEntityProtocols: AnyObject {
    var interactor: CustomerServiceDetailInteractorProtocols? { get set }
    
    func didCustomerServiceDetailReceiveData(by params: Any?)
    func didCustomerServiceDetailReceiveError(error: MyError?)
    func getCustomerServiceDetailRequest(by params: Any?)
}

protocol CustomerServiceDetailRouterProtocols: AnyObject {
    func pushToAddCustomerService(from previousView: UIViewController)
    func pushToCreatCustomerService(from previousView: UIViewController)
    func pushToCustomerServiceDetail(from previousView: UIViewController)
}



