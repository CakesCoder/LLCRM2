//
//  CustomerServiceDetailModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CustomerServiceDetailModel: BaseModel {
    
    var baseUnit:String?
    var clientComplaintNo:String?
    var clientId:String?
    var clientName:String?
    var closeRemark:String?
    var closeResult:String?
    var closeTime:String?
    var complaintNo:String?
    var complaintTheme:String?
    var complaintTypeId:String?
    var complaintTypeName:String?
    var createName:String?
    var createTime:String?
    var deleted:Int?
    var discardCount:String?
    var file:String?
    var handleName:String?
    var id:String?
    var lastReplyTime:String?
    var probDesc:String?
    var productList:[CustomerServiceDetailProductListModel]?
    var submitter:String?
    var submitterReport:String?
    var takeTime:String?
    var templateJsons:String?
    
    required init() {}
}

class CustomerServiceDetailProductListModel:BaseModel{
    
    var badDesc:String?
    var badType:String?
    var baseUnit:String?
    var clientDiscardNo:String?
    var clientId:String?
    var clientName:String?
    var complaintId:String?
    var complaintNo:String?
    var createTime:String?
    var deleted:Int?
    var discardCount:String?
    var id:String?
    var productCode:String?
    var productModel:String?
    var productName:String?
    var productTypeName:String?
    var source:String?
    var specDesc:String?
    
    
    required init() {}
}
