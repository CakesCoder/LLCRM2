//
//  CustomerServiceDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit

class CustomerServiceDetailEntity: CustomerServiceDetailEntityProtocols {
    var interactor: CustomerServiceDetailInteractorProtocols?
    
    func didCustomerServiceDetailReceiveData(by params: Any?) {
        interactor?.didEntityCustomerServiceDetailReceiveData(by: params)
    }
    
    func didCustomerServiceDetailReceiveError(error: MyError?) {
        interactor?.didEntityCustomerDetailServiceReceiveError(error: error)
    }
    
    func getCustomerServiceDetailRequest(by params: Any?) {
        LLNetProvider.request(.clientComplaintProductDetail(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didCustomerServiceDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didCustomerServiceDetailReceiveError(error: .requestError)
            }
        }
    }
    
}
