//
//  CustomerServiceDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit

class CustomerServiceDetailInteractor: CustomerServiceDetailInteractorProtocols {
    var presenter: CustomerServiceDetailPresenterProtocols?
    
    var entity: CustomerServiceDetailEntityProtocols?
    
    func presenterRequestCustomerServiceDetail(by params: Any?) {
        entity?.getCustomerServiceDetailRequest(by: params)
    }
    
    func didEntityCustomerServiceDetailReceiveData(by params: Any?) {
        presenter?.didGetCustomerServiceDetailInteractorReceiveData(by: params)
    }
    
    func didEntityCustomerDetailServiceReceiveError(error: MyError?) {
        presenter?.didGetCustomerServiceDetailInteractorReceiveError(error: error)
    }
    

}
