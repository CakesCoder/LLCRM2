//
//  CustomerServiceDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit

class CustomerServiceDetailPresenter: CustomerServiceDetailPresenterProtocols {
    var view: CustomerServiceDetailViewProtocols?
    
    var router: CustomerServiceDetailRouterProtocols?
    
    var interactor: CustomerServiceDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetCustomerServiceDetail(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestCustomerServiceDetail(by: params)
    }
    
    func didGetCustomerServiceDetailInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetCustomerServiceDetailPresenterReceiveData(by: params)
    }
    
    func didGetCustomerServiceDetailInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetCustomerServiceDetailPresenterReceiveError(error: error)
    }
}
