//
//  SampleInfosDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
//import JXPagingView

extension SampleInfosDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SampleInfosDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class SampleInfosDetailViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var followDateLabel:UILabel?
    var followThemeLabel:UILabel?
    var followContent:UILabel?
    var perosonLabel:UILabel?
    var commitDateLabel:UILabel?
    var possibilityLabel:UILabel?
    
    var model:DeviceSampleVoListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 18*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfos = [["*","选型原因："],["*","需求优先级别："],["*","物料类别："],["*","物料编码："],["物料名称："],["制造商料号： "],["规格描述："],["*","需求日期："],["*","需求数量："],["*","跟随人："],["*","单位："],["*","物料品牌："]]

        
        let contentInfos = ["新项目", "紧急", "电解电容", "89832972819289", "PC板", "230524001", "这是一段很长的文字……", "跟随人", "单位","2023-05-23", "200个", "华为"]
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor4
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor4
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
//                obj.text = contentInfos[i]
                if i == 0{
                    obj.text = model?.materialApplyCause ?? "--"
                }else if i == 1{
                    obj.text = model?.demandPriority ?? "--"
                }else if i == 2{
                    obj.text = model?.materialCategory ?? "--"
                }else if i == 3{
                    obj.text = model?.sampleNumber ?? "--"
                }else if i == 4{
                    obj.text = model?.materialName ?? "--"
                }else if i == 5{
                    obj.text = model?.specs ?? "--"
                }else if i == 6{
                    obj.text = model?.demandDate ?? "--"
                }else if i == 7{
                    obj.text = model?.demandQuantity ?? "--"
                }else if i == 8{
                    obj.text = model?.principalBy ?? "--"
                }else if i == 9{
                    obj.text = model?.unit ?? "--"
                }else{
                    obj.text = model?.manufacturerName ?? "--"
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(120)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    
    @objc func addClick(_ button:UIButton){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
