//
//  ScheduleDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class ScheduleDetailViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "日程详情"
        // Do any additional setup after loading the view.
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ScheduleDetailViewController.backClick))
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let icon = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.image = UIImage(named: "schedule_icon")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(25)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "会议"
            obj.textColor = blackTextColor3
            obj.font = kMediumFont(16)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(icon.snp.right).offset(10)
                make.top.equalToSuperview().offset(25)
            }
        }
        
        let createDateLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.font = kMediumFont(13)
            obj.textColor = blackTextColor51
            obj.text = "创建时间：2022 06.20 14:30"
            obj.snp.makeConstraints { make in
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
                make.left.equalTo(icon.snp.right).offset(10)
            }
        }
        
        let uploadDateLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.font = kMediumFont(13)
            obj.textColor = blackTextColor51
            obj.text = "最近更新时间：2022 06.20  14:30"
            obj.snp.makeConstraints { make in
                make.top.equalTo(createDateLabel.snp.bottom).offset(5)
                make.left.equalTo(icon.snp.right).offset(10)
            }
        }
        
        let lineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(uploadDateLabel.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        let scheduleContentHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "日程内容："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(lineView.snp.bottom).offset(20)
            }
        }
        
        let scheduleContentLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "讨论APP页面设计及交互的跳转"
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalTo(scheduleContentHintLabel.snp.right).offset(0)
                make.centerY.equalTo(scheduleContentHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let scheduleTypeHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "日程类型："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(scheduleContentHintLabel.snp.bottom).offset(10)
            }
        }
        
        let scheduleTypeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "会议"
            obj.snp.makeConstraints { make in
                make.left.equalTo(scheduleTypeHintLabel.snp.right).offset(0)
                make.centerY.equalTo(scheduleTypeHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let addHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "参与人："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(scheduleTypeHintLabel.snp.bottom).offset(10)
            }
        }
        
        let addLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "王小虎，夏淼淼，夏凉生"
            obj.snp.makeConstraints { make in
                make.left.equalTo(addHintLabel.snp.right).offset(13)
                make.centerY.equalTo(addHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let allDayHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "是否全天："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(addHintLabel.snp.bottom).offset(10)
            }
        }
        
        let allDayLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "是"
            obj.snp.makeConstraints { make in
                make.left.equalTo(allDayHintLabel.snp.right).offset(0)
                make.centerY.equalTo(allDayHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let startTimeHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "开始时间："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(allDayHintLabel.snp.bottom).offset(10)
            }
        }
        
        let startTimeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "2022-05-07  13:46:55"
            obj.snp.makeConstraints { make in
                make.left.equalTo(startTimeHintLabel.snp.right).offset(0)
                make.centerY.equalTo(startTimeHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let endTimeHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "结束时间："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(startTimeHintLabel.snp.bottom).offset(10)
            }
        }
        
        let endTimeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "2022-05-07  14:46:55"
            obj.snp.makeConstraints { make in
                make.left.equalTo(endTimeHintLabel.snp.right).offset(0)
                make.centerY.equalTo(endTimeHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let secretHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "是否私密："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(endTimeHintLabel.snp.bottom).offset(10)
            }
        }
        
        let secretLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "否"
            obj.snp.makeConstraints { make in
                make.left.equalTo(secretHintLabel.snp.right).offset(0)
                make.centerY.equalTo(secretHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let remindHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "提醒时间："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(secretHintLabel.snp.bottom).offset(10)
            }
        }
        
        let repeatLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "5分钟前"
            obj.snp.makeConstraints { make in
                make.left.equalTo(remindHintLabel.snp.right).offset(0)
                make.centerY.equalTo(remindHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let repeatHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "循环设置："
            obj.font = kMediumFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(remindHintLabel.snp.bottom).offset(10)
            }
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor9
            obj.font = kMediumFont(13)
            obj.text = "不重复"
            obj.snp.makeConstraints { make in
                make.left.equalTo(repeatHintLabel.snp.right).offset(0)
                make.centerY.equalTo(repeatHintLabel)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(20)
            }
        }
        
        let bottomLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-108)
            }
        }
        
        let bottomView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.right.equalToSuperview().offset(-30)
                make.top.equalTo(bottomLineView.snp.bottom).offset(0)
                make.height.equalTo(108)
            }
        }
        
        let images = ["schedule_detail_editor", "schedule_detail_delete"]
        let titles = ["编辑", "删除"]
        for i in 0..<images.count{
            let bgView = UIView().then { obj in
                bottomView.addSubview(obj)
                obj.frame = CGRect(x: CGFloat(i)*((kScreenWidth - 60)/2), y: 0, width: (kScreenWidth - 60)/2, height: 108)
            }
            
            let imageV = UIImageView().then { obj in
                bottomView.addSubview(obj)
                obj.image = UIImage(named: images[i])
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(16)
                    make.centerX.equalTo(bgView)
                }
            }
            
            _ = UILabel().then({ obj in
                bottomView.addSubview(obj)
                obj.textColor = blackTextColor53
                obj.font = kMediumFont(13)
                obj.text = titles[i]
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(5)
                    make.centerX.equalTo(bgView)
                }
            })
            
            _ = UIButton().then({ obj in
                bottomView.addSubview(obj)
                
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
