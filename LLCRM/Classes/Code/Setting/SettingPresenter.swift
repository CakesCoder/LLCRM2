//
//  SettingPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class SettingPresenter: SettingPresenterProtocols {
    var view: SettingViewProtocols?
    
    var router: SettingRouterProtocols?
    
    var interactor: SettingInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetSetting(by params: Any?) {
        
    }
    
    func didGetSettingInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetSettingInteractorReceiveError(by params: Any?) {
        
    }
    
    
}
