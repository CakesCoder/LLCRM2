//
//  SettingEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class SettingEntity: SettingEntityProtocols {
    var interactor: SettingInteractorProtocols?
    
    func getSettingRequest(by params: Any?) {
        
    }
    
    func didSettingReceiveData(by params: Any?) {
        
    }
    
    func didSettingReceiveError(error: MyError?) {
        
    }
    
    
}
