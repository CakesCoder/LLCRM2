//
//  SettingViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit
import HandyJSON
import PKHUD

class SettingViewController: BaseViewController {
    
    var presenter: SettingPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "设置"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SettingViewController.backClick))
        
        let headView = UIView().then { obj in
            obj.backgroundColor = bgColor
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 500)
        }
        
        let topLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        let hints = ["绑定手机号", "外部名片", "账号与安全", "新消息通知", "隐私", "常规", "关于零瓴软件"]
        for i in 0..<2{
            let bgView = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.frame = CGRect(x: 0, y: 10 + i*50, width: Int(kScreenWidth), height: 50)
            }
            
            let hintLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.font = kRegularFont(16)
                obj.textColor = blackTextColor
                obj.text = hints[i]
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(28)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor18
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                    make.right.equalToSuperview().offset(-0)
                }
            })
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                obj.tag = 1000+i
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        let twoLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
                make.top.equalToSuperview().offset(110)
            }
        }
        
        let accountView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 10 + 110, width: Int(kScreenWidth), height: 50)
        }
        
        let accountHintLabel = UILabel().then { obj in
            accountView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = hints[2]
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            accountView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
                make.right.equalToSuperview().offset(-0)
            }
        })
        
        let threeLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
                make.top.equalToSuperview().offset(170)
            }
        }
        
        let accountButton = UIButton().then { obj in
            accountView.addSubview(obj)
            obj.tag = 1000+2
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        for i in 3..<6{
            let bgView = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.frame = CGRect(x: 0, y: 30 + i*50, width: Int(kScreenWidth), height: 50)
            }
            
            let hintLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.font = kRegularFont(16)
                obj.textColor = blackTextColor
                obj.text = hints[i]
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(28)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor18
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                    make.right.equalToSuperview().offset(-0)
                }
            })
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                obj.tag = 1000+i
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        let fourLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
                make.top.equalToSuperview().offset(330)
            }
        }
        
        let aboutView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 10 + 330, width: Int(kScreenWidth), height: 50)
        }
        
        let aboutHintLabel = UILabel().then { obj in
            aboutView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = hints[2]
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            aboutView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
                make.right.equalToSuperview().offset(-0)
            }
        })
        
        let button = UIButton().then { obj in
            aboutView.addSubview(obj)
            obj.tag = 1000+5
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let footView = UIView().then { obj in
            obj.backgroundColor = bgColor
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 200)
        }
        
        let outButton = UIButton().then { obj in
            footView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.backgroundColor = blackTextColor36
            obj.setTitle("退出登录", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kMediumFont(15)
            obj.addTarget(self, action: #selector(outLogin), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.tableFooterView = footView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
        
        
        cofing()

        // Do any additional setup after loading the view.
    }
    
    func cofing(){
        let router = SettingRouter()
        
        presenter = SettingPresenter()
        
        presenter?.router = router
        
        let entity = SettingEntity()
        
        let interactor = SettingInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func outLogin(){
        LLUserDefaultsManager.shared.removeTK()
        presenter?.router?.pushToLogin(from: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SettingViewController: SettingViewProtocols{
 
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
}
