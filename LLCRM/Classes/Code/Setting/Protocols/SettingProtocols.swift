//
//  SettingProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class SettingProtocols: NSObject {

}

protocol SettingViewProtocols: AnyObject {
    // 持有一个presenter，帮我做事
    var presenter: SettingPresenterProtocols? { get set }
    
    /*
     这些方法，都是UI回调相关的方法
     让我的presenter帮我做事情，等presenter给我回调结果就好了
     */
    func showLoading()
    func showError()
    func hideLoading()
}

protocol SettingPresenterProtocols: AnyObject{
    var view: SettingViewProtocols? { get set }
    var router: SettingRouterProtocols? { get set }
    var interactor: SettingInteractorProtocols? { get set }
    var params: Any? { get set }
    func viewDidLoad()
    func presenterRequestGetSetting(by params: Any?)
    func didGetSettingInteractorReceiveData(by params: Any?)
    func didGetSettingInteractorReceiveError(by params: Any?)
}

protocol SettingInteractorProtocols: AnyObject {
    var presenter: SettingPresenterProtocols? { get set }
    var entity: SettingEntityProtocols? { get set }
    
    func entityGetSettingReceiveData(by params: Any?)
    func didEntityGetSettingReceiveData(by params: Any?)
    func didEntitySettingReceiveError(error: MyError?)
}

protocol SettingEntityProtocols: AnyObject {
    var interactor: SettingInteractorProtocols? { get set }
    
    func getSettingRequest(by params: Any?)
    
    func didSettingReceiveData(by params: Any?)
    func didSettingReceiveError(error: MyError?)
    
}

protocol SettingRouterProtocols: AnyObject {
    func pushToLogin(from previousView: UIViewController)
    func pushToGuide(from previousView: UIViewController)
}
