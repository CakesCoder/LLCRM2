//
//  SettingInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class SettingInteractor: SettingInteractorProtocols {
    var presenter: SettingPresenterProtocols?
    
    var entity: SettingEntityProtocols?
    
    func entityGetSettingReceiveData(by params: Any?) {
        
    }
    
    func didEntityGetSettingReceiveData(by params: Any?) {
        
    }
    
    func didEntitySettingReceiveError(error: MyError?) {
        
    }
    
    
}
