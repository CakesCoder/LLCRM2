//
//  SettingRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class SettingRouter: SettingRouterProtocols {
    func pushToLogin(from previousView: UIViewController) {
        let vc = LoginViewController()
        vc.modalPresentationStyle =  UIModalPresentationStyle.fullScreen
        previousView.present(vc, animated: true)
    }
    
    func pushToGuide(from previousView: UIViewController) {
        let vc = LLGuideViewContoller()
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        previousView.present(nav, animated: true)
    }
}
