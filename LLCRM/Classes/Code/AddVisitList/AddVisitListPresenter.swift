//
//  AddVisitListPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddVisitListPresenter: AddVisitListPresenterProtocols {
    var view: AddVisitListViewProtocols?
    
    var router: AddVisitListRouterProtocols?
    
    var interactor: AddVisitListInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddVisitList(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddVisitList(by: params)
    }
    
    func didGetAddVisitListInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddVisitListPresenterReceiveData(by: params)
    }
    
    func didGetAddVisitListInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddVisitListPresenterReceiveError(error: error)
    }
    
//    func presenterRequestGetInfos(by params: Any?) {
//        view?.showLoading()
//        interactor?.presenterRequestAddInfos(by: params)
//    }
//    
//    func didGetInfosInteractorReceiveData(by params: Any?) {
//        view?.hideLoading()
//        view?.didGetInfosPresenterReceiveData(by: params)
//    }
//    
//    func didGetInfosInteractorReceiveError(error: MyError?) {
//        view?.hideLoading()
//        view?.showError()
//        view?.didGetInfosPresenterReceiveError(error: error)
//    }
    
}
