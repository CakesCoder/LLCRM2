//
//  AddVisitListEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddVisitListEntity: AddVisitListEntityProtocols {
    var interactor: AddVisitListInteractorProtocols?
    
    func didAddVisitListReceiveData(by params: Any?) {
        interactor?.didEntityAddVisitListReceiveData(by: params)
    }
    
    func didAddVisitListReceiveError(error: MyError?) {
        interactor?.didEntityAddVisitListReceiveError(error: error)
    }
    
    func getAddVisitListRequest(by params: Any?) {
        LLNetProvider.request(.addVisitPerson(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddVisitListReceiveData(by:jsonData)
            case .failure(_):
                self.didAddVisitListReceiveError(error: .requestError)
            }
        }
    }
    
//    func didAddVisitListReceiveData(by params: Any?) {
//        interactor?.didEntityAddVisitListReceiveData(by: params)
//    }
//    
//    func didAddVisitListReceiveError(error: MyError?) {
//        interactor?.didEntityAddVisitListReceiveError(error: error)
//    }
//    
//    func getAddVisitListRequest(by params: Any?) {
//        LLNetProvider.request(.addVisitPerson(params as! [String : Any])) { result in
//        switch result{
//            case let .success(response):
//                let data = response.data
//                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
//                self.didAddVisitListReceiveData(by:jsonData)
//            case .failure(_):
//                self.didAddVisitListReceiveError(error: .requestError)
//            }
//        }
//    }
    
//    func didGetInfosReceiveData(by params: Any?) {
//        interactor?.didAddInfosReceiveData(by: params)
//    }
//    
//    func didGetInfosReceiveError(error: MyError?) {
//        interactor?.didAddInfosReceiveError(error: error)
//    }
//    
//    func getInfostRequest(by params: Any?) {
//        LLNetProvider.request(.getAddClientInfos(params as! [String : Any])) { result in
//        switch result{
//            case let .success(response):
//                let data = response.data
//                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
//                self.didGetInfosReceiveData(by:jsonData)
//            case .failure(_):
//                self.didGetInfosReceiveError(error: .requestError)
//            }
//        }
//    }
}
