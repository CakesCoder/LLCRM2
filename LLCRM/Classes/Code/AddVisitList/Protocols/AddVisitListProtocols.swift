//
//  AddVisitListProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddVisitListProtocols: NSObject {

}

protocol AddVisitListViewProtocols: AnyObject {
    var presenter: AddVisitListPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddVisitListPresenterReceiveData(by params: Any?)
    func didGetAddVisitListPresenterReceiveError(error: MyError?)
    
//    func didGetInfosPresenterReceiveData(by params: Any?)
//    func didGetInfosPresenterReceiveError(error: MyError?)
}

protocol AddVisitListPresenterProtocols: AnyObject{
    var view: AddVisitListViewProtocols? { get set }
    var router: AddVisitListRouterProtocols? { get set }
    var interactor: AddVisitListInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddVisitList(by params: Any?)
    func didGetAddVisitListInteractorReceiveData(by params: Any?)
    func didGetAddVisitListInteractorReceiveError(error: MyError?)
    
    
//    func presenterRequestGetInfos(by params: Any?)
//    func didGetInfosInteractorReceiveData(by params: Any?)
//    func didGetInfosInteractorReceiveError(error: MyError?)
}

protocol AddVisitListInteractorProtocols: AnyObject {
    var presenter: AddVisitListPresenterProtocols? { get set }
    var entity: AddVisitListEntityProtocols? { get set }
    
    func presenterRequestAddVisitList(by params: Any?)
    func didEntityAddVisitListReceiveData(by params: Any?)
    func didEntityAddVisitListReceiveError(error: MyError?)
    
//    func presenterRequestAddInfos(by params: Any?)
//    func didAddInfosReceiveData(by params: Any?)
//    func didAddInfosReceiveError(error: MyError?)
}

protocol AddVisitListEntityProtocols: AnyObject {
    var interactor: AddVisitListInteractorProtocols? { get set }
    
    func didAddVisitListReceiveData(by params: Any?)
    func didAddVisitListReceiveError(error: MyError?)
    func getAddVisitListRequest(by params: Any?)
    
//    func didGetInfosReceiveData(by params: Any?)
//    func didGetInfosReceiveError(error: MyError?)
//    func getInfostRequest(by params: Any?)
}

protocol AddVisitListRouterProtocols: AnyObject {
    func pushToMyClient(from previousView: UIViewController)
    func pushToVisitDetail(from previousView: UIViewController)
}



