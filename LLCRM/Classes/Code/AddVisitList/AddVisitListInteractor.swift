//
//  AddVisitListInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddVisitListInteractor: AddVisitListInteractorProtocols {
    var presenter: AddVisitListPresenterProtocols?
    
    var entity: AddVisitListEntityProtocols?
    
    func presenterRequestAddVisitList(by params: Any?) {
        entity?.getAddVisitListRequest(by: params)
    }
    
    func didEntityAddVisitListReceiveData(by params: Any?) {
        presenter?.didGetAddVisitListInteractorReceiveData(by: params)
    }
    
    func didEntityAddVisitListReceiveError(error: MyError?) {
        presenter?.didGetAddVisitListInteractorReceiveError(error: error)
    }
    
//    func presenterRequestAddInfos(by params: Any?) {
//        entity?.getInfostRequest(by: params)
//    }
//    
//    func didAddInfosReceiveData(by params: Any?) {
//        presenter?.didGetInfosInteractorReceiveData(by: params)
//    }
//    
//    func didAddInfosReceiveError(error: MyError?) {
//        presenter?.didGetInfosInteractorReceiveError(error: error)
//    }
    
}
