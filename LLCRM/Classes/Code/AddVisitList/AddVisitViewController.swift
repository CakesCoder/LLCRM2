//
//  AddVisitViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit
import PKHUD
import HandyJSON
import ZLPhotoBrowser
import Photos

class AddVisitViewController: BaseViewController {
    
    var clientTextFiled:UITextField?
    var clientTypeTextFiled:UITextField?
    var clientThemeTextFiled:UITextField?
    var clientGoalTextFiled:UITextField?
    var personTextFiled:UITextField?
    var clientPersonTextFiled:UITextField?
    var clientAddPersonTextFiled:UITextField?
    var clientId:String?
    
    var pictureContent:UIView?
    var addButton:UIButton?
    
    var images:[Any]? = []
    
    var presenter: AddVisitListPresenterProtocols?
    
    var addClientSelectedNameModel:AddClientSelectedBussinessNameModel?
    var addClientSelectedPersonModel:AddClientSelectedPersonModel?
    var addClientSelectedClientNameModel:AddClientSelectedBussinessNameModel?
    var addClientVisitAddPersonModel:AddClientVisitAddPersonModel?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "客户拜访"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddVisitViewController.backClick))
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight - 200)
        }
        
        let tagImageV = UIImageView().then { obj in
            headView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "apply_go")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(30)
                make.width.height.equalTo(16)
            }
        }
        
        
        let visitHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "拜访对象"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor90
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(5)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let clientHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "客户"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor90
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let clientView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor5.cgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(68)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        let clientImageV = UIImageView().then { obj in
            clientView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(12)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(13)
            }
        }
        
        clientTextFiled = UITextField().then { obj in
            clientView.addSubview(obj)
            obj.placeholder = "添加客户"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor14
            obj.isEnabled = false
            obj.snp.makeConstraints { make in
                make.left.equalTo(clientImageV.snp.right).offset(5)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        let clientPushImageV = UIImageView().then { obj in
            clientView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then({ obj in
            clientView.addSubview(obj)
            obj.tag = 1000
            obj.addTarget(self, action: #selector(addClientClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let clientTypeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let clientTypeHintLabel = UILabel().then { obj in
            clientTypeView.addSubview(obj)
            let clientTypeHintAttribute = NSMutableAttributedString(string: "*")
            clientTypeHintAttribute.yy_color = blackTextColor27
            clientTypeHintAttribute.yy_font = kMediumFont(13)
            let clientTypeCotentAttribute = NSMutableAttributedString(string: "拜访类型")
            clientTypeCotentAttribute.yy_color = blackTextColor
            clientTypeCotentAttribute.yy_font = kMediumFont(14)
            clientTypeHintAttribute.append(clientTypeCotentAttribute)
            obj.attributedText = clientTypeHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        clientTypeTextFiled = UITextField().then { obj in
            clientTypeView.addSubview(obj)
            obj.placeholder = "请输入"
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(clientTypeHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        _ = UIView().then({ obj in
            clientView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let clientThemeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(clientTypeView.snp.bottom).offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(80)
            }
        }
        
        let clientThemeHintLabel = UILabel().then { obj in
            clientThemeView.addSubview(obj)
            
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor3
            obj.text = "拜访主题"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(60)
                make.height.equalTo(20)
            }
        }
        
        let clientThemeInputView = UIView().then { obj in
            clientThemeView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.layer.borderColor = lineColor7.cgColor
            obj.layer.borderWidth = 0.5
            obj.backgroundColor = bgColor23
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(40)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        clientThemeTextFiled = UITextField().then { obj in
            clientThemeInputView.addSubview(obj)
            obj.placeholder = "输入"
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.height.equalTo(35)
            }
        }
        
        let clientGoalView = UIView().then { obj in
            headView.addSubview(obj)
//            obj.backgroundColor = .green
            obj.snp.makeConstraints { make in
                make.top.equalTo(clientThemeView.snp.bottom).offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(80)
            }
        }
        
        let clientGoalHintLabel = UILabel().then { obj in
            clientGoalView.addSubview(obj)
            obj.text = "拜访目的"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(60)
                make.height.equalTo(20)
            }
        }
        
        let clientGoalInputView = UIView().then { obj in
            clientGoalView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.layer.borderColor = lineColor7.cgColor
            obj.layer.borderWidth = 0.5
            obj.backgroundColor = bgColor23
//            obj.backgroundColor = .reds
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(clientGoalHintLabel.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        clientGoalTextFiled = UITextField().then { obj in
            clientGoalInputView.addSubview(obj)
            obj.placeholder = "输入"
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let personView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientGoalView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let personHintLabel = UILabel().then { obj in
            personView.addSubview(obj)
            let personHintAttribute = NSMutableAttributedString(string: "*")
            personHintAttribute.yy_color = blackTextColor27
            personHintAttribute.yy_font = kMediumFont(13)
            let personCotentAttribute = NSMutableAttributedString(string: "负责人")
            personCotentAttribute.yy_color = blackTextColor
            personCotentAttribute.yy_font = kMediumFont(14)
            personHintAttribute.append(personCotentAttribute)
            obj.attributedText = personHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        personTextFiled = UITextField().then { obj in
            personView.addSubview(obj)
            obj.placeholder = "请选择"
            obj.font = kRegularFont(12)
            obj.isEnabled = false
            obj.snp.makeConstraints { make in
                make.left.equalTo(personHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        _ = UIButton().then({ obj in
            personView.addSubview(obj)
            obj.tag = 1002
            obj.addTarget(self, action: #selector(addClientClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientGoalView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        })
        
//        let personPushImageV = UIImageView().then { obj in
//            personView.addSubview(obj)
//            obj.image = UIImage(named: "me_push")
//            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalToSuperview()
//            }
//        }
        
        _ = UIView().then({ obj in
            personView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let clientPersonView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(personView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let clientPersonHintLabel = UILabel().then { obj in
            clientPersonView.addSubview(obj)
            let clientPersonHintAttribute = NSMutableAttributedString(string: "")
            clientPersonHintAttribute.yy_color = blackTextColor27
            clientPersonHintAttribute.yy_font = kMediumFont(13)
            let clientPersonCotentAttribute = NSMutableAttributedString(string: "客户人员")
            clientPersonCotentAttribute.yy_color = blackTextColor
            clientPersonCotentAttribute.yy_font = kMediumFont(14)
            clientPersonHintAttribute.append(clientPersonCotentAttribute)
            obj.attributedText = clientPersonHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        clientPersonTextFiled = UITextField().then { obj in
            clientPersonView.addSubview(obj)
            obj.placeholder = "请输入"
            obj.font = kRegularFont(12)
            obj.isEnabled = false
            obj.snp.makeConstraints { make in
                make.left.equalTo(clientPersonHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        _ = UIButton().then({ obj in
            clientPersonView.addSubview(obj)
            obj.tag = 1003
            obj.addTarget(self, action: #selector(addClientClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(personView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        })
        
        
//        let clientPersonPushImageV = UIImageView().then { obj in
//            clientPersonView.addSubview(obj)
//            obj.image = UIImage(named: "me_push")
//            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalToSuperview()
//            }
//        }
        
        _ = UIView().then({ obj in
            clientPersonView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let clientAddPersonView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientPersonView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let clientAdddPersonHintLabel = UILabel().then { obj in
            clientAddPersonView.addSubview(obj)
            let clientAddPersonHintAttribute = NSMutableAttributedString(string: "")
            clientAddPersonHintAttribute.yy_color = blackTextColor27
            clientAddPersonHintAttribute.yy_font = kMediumFont(13)
            let clientAddPersonCotentAttribute = NSMutableAttributedString(string: "参与人")
            clientAddPersonCotentAttribute.yy_color = blackTextColor
            clientAddPersonCotentAttribute.yy_font = kMediumFont(14)
            clientAddPersonHintAttribute.append(clientAddPersonCotentAttribute)
            obj.attributedText = clientAddPersonHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        clientAddPersonTextFiled = UITextField().then { obj in
            clientAddPersonView.addSubview(obj)
            obj.placeholder = "请选择"
            obj.font = kRegularFont(12)
            obj.isEnabled = false
            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(0)
                make.left.equalTo(clientAdddPersonHintLabel.snp.right).offset(40)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientPersonView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        _ = UIButton().then({ obj in
            clientAddPersonView.addSubview(obj)
            obj.tag = 1004
            obj.addTarget(self, action: #selector(addClientClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
//        let clientAddPersonPushImageV = UIImageView().then { obj in
//            clientAddPersonView.addSubview(obj)
//            obj.image = UIImage(named: "me_push")
//            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalToSuperview()
//            }
//        }
        
        _ = UIView().then({ obj in
            clientAddPersonView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
      
        
        let clientPictureHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let clientPictureHintAttribute = NSMutableAttributedString(string: "")
            clientPictureHintAttribute.yy_color = blackTextColor27
            clientPictureHintAttribute.yy_font = kMediumFont(13)
            let clientPictureCotentAttribute = NSMutableAttributedString(string: "上传附件")
            clientPictureCotentAttribute.yy_color = blackTextColor
            clientPictureCotentAttribute.yy_font = kMediumFont(14)
            clientPictureHintAttribute.append(clientPictureCotentAttribute)
            obj.attributedText = clientPictureHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(clientAddPersonView.snp.bottom).offset(15)
            }
        }
        
        let w = (kScreenWidth-50)/3
        pictureContent = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.right.equalToSuperview().offset(-25)
                make.top.equalTo(clientPictureHintLabel.snp.bottom).offset(10)
                make.height.equalTo(w)
            }
        }
        
        addButton = UIButton().then({ obj in
            pictureContent?.addSubview(obj)
            obj.frame = CGRectMake(0, 0, w, w)
            obj.layer.borderColor = lineColor23.cgColor
            obj.layer.borderWidth = 0.5
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(.black, for: .normal)
            obj.titleLabel?.font = kMediumFont(25)
            obj.tag = 2000
            obj.addTarget(self, action: #selector(addPhotoClick), for: .touchUpInside)
        })
        
        let footView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, 100)
//            obj.backgroundColor = .red
        }
        
        let saveButton = UIButton().then { obj in
            footView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("完成", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kMediumFont(11)
            obj.tag = 1000
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(10)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.tableFooterView = footView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedVisitAddAction(_:)), name: Notification.Name(rawValue: "VisitAddNotificationKey"), object: nil)
        
        cofing()
    }
    
    @objc func saveClick(_ button:UIButton){
        presenter?.presenterRequestGetAddVisitList(by: ["visitPeopleName":addClientSelectedNameModel?.clientName, "visitCategory":clientTypeTextFiled?.text, "visitTopic":clientThemeTextFiled?.text, "visitPurpose":clientGoalTextFiled?.text, "createrName":addClientSelectedPersonModel?.name, "clientPeoples":addClientSelectedClientNameModel?.clientName, "companyPeoples":addClientVisitAddPersonModel?.companyName,"id":addClientSelectedNameModel?.clientId])
    }
    
    @objc func addPhotoClick(){
        let ps = ZLPhotoPreviewSheet()
        ps.selectImageBlock = { [weak self] results, isOriginal in
            // your code
            if (self?.images!.count)! > 1{
                self?.addButton?.isHidden = true
            }else{
                let w = (kScreenWidth-50)/3
                for i in 0..<results.count{
                    if self?.images?.count ?? 0 >= 3{
                        self?.addButton?.isHidden = true
                        break
                    }
                    if i == 0{
                        for view in self?.pictureContent?.subviews as! [UIView]{
                            if view is UIButton{
                                continue
                            }
                            view.removeFromSuperview()
                        }
                    }
                    self?.images?.append(results[i].image)
                    self?.addButton?.isHidden = false
                    if i == 2{
                        self?.addButton?.isHidden = true
                    }else{
                        self?.addButton?.frame = CGRect(x: CGFloat(w)*CGFloat(i+1), y: 0, width: w, height: w)
                        self?.addButton?.isHidden = false
                    }
                    
                    _ = UIImageView().then { obj in
                        self?.pictureContent?.addSubview(obj)
                        obj.image = (self?.images![i] as! UIImage)
                        obj.frame = CGRect(x: CGFloat(w)*CGFloat(i), y: 0, width: w - 5, height: w)
                    }
                }
            }
        }
        ps.showPreview(animate: true, sender: self)
    }
    
    @objc func didSelectedVisitAddAction(_ notification:NSNotification){
        let dict = notification.userInfo as? [String:Any]
        let model = dict!["model"] as? MyClientModelData
        clientId = model?.clientId ?? ""
        clientTextFiled?.text = model?.clientName
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClientClick(_ button:UIButton){
//        presenter?.router?.pushToMyClient(from: self)
        if button.tag == 1000{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 10
            vc.addClientSelectedBussinessNameBlock = {[self] model in
                addClientSelectedNameModel = model
                clientTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1002{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 4
            vc.addClientPersonBlock = {[self] model in
                addClientSelectedPersonModel = model
                personTextFiled?.text = addClientSelectedPersonModel?.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1003{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 10
            vc.addClientSelectedBussinessNameBlock = {[self] model in
                addClientSelectedClientNameModel = model
                clientPersonTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1004{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 39
            vc.addClientVisitAddPersonBlock = {[self] model in
                addClientVisitAddPersonModel = model
                clientAddPersonTextFiled?.text = model.companyName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }
    
    func cofing(){
        let router = AddVisitListRouter()
        
        presenter = AddVisitListPresenter()
        
        presenter?.router = router
        
        let entity = AddVisitListEntity()
        
        let interactor = AddVisitListInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        presenter?.presenterRequestGetInfos(by: ["":""])
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddVisitViewController: AddVisitListViewProtocols{
//    func didGetInfosPresenterReceiveData(by params: Any?) {
//        printTest(params)
//    }
//    
//    func didGetInfosPresenterReceiveError(error: MyError?) {
//        printTest(error)
//    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetAddVisitListPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "VisitListNotificationKey"), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddVisitListPresenterReceiveError(error: MyError?) {
        
    }
    
    
}
