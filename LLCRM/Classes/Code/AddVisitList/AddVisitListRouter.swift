//
//  AddVisitListRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddVisitListRouter: AddVisitListRouterProtocols {
    func pushToMyClient(from previousView: UIViewController) {
        let nextView = MyClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToVisitDetail(from previousView: UIViewController) {
        
    }
    
    
}
