//
//  AddClientSearchViewProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2024/3/1.
//

import UIKit

//class AddClientSearchViewProtocols: AnyObject {
//
//}

protocol AddClientSearchViewProtocols: AnyObject {
    
    var presenter: AddClientSearchPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddClientSearchPresenterReceiveData(by params: Any?)
    func didGetAddClientSearchtPresenterReceiveError(error: MyError?)
}

protocol AddClientSearchPresenterProtocols: AnyObject{
    
    var view: AddClientSearchViewProtocols? { get set }
    var router: AddClientSearchRouterProtocols? { get set }
    var interactor: AddClientSearchInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddClientSearch(by params: Any?)
    func didGetAddClientSearchInteractorReceiveData(by params: Any?)
    func didGetAddClientSearchInteractorReceiveError(error: MyError?)
}

protocol AddClientSearchInteractorProtocols: AnyObject {
    
    var presenter: AddClientSearchPresenterProtocols? { get set }
    var entity: AddClientSearchEntityProtocols? { get set }
    
    func presenterRequestAddClientSearch(by params: Any?)
    func didEntityAddClientSearchReceiveData(by params: Any?)
    func didEntityAddClientSearchReceiveError(error: MyError?)
    
}

protocol AddClientSearchEntityProtocols: AnyObject {
    var interactor: AddClientSearchInteractorProtocols? { get set }
    
    func didAddClientSearchReceiveData(by params: Any?)
    func didAddClientSearchReceiveError(error: MyError?)
    func getAddClientSearchRequest(by params: Any?)
    
}

protocol AddClientSearchRouterProtocols: AnyObject {
//    func pushToMyClient(from previousView: UIViewController)
//    func pushToVisitDetail(from previousView: UIViewController)
}



