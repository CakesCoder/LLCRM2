//
//  AddClientSearchEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2024/3/1.
//

import UIKit

class AddClientSearchEntity: AddClientSearchEntityProtocols {
    
    var interactor: AddClientSearchInteractorProtocols?
    
    func didAddClientSearchReceiveData(by params: Any?) {
        interactor?.didEntityAddClientSearchReceiveData(by: params)
    }
    
    func didAddClientSearchReceiveError(error: MyError?) {
        interactor?.didEntityAddClientSearchReceiveError(error: error)
    }
    
    func getAddClientSearchRequest(by params: Any?) {
        LLNetProvider.request(.getAddClientInfos(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientSearchReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientSearchReceiveError(error: .requestError)
            }
        }
    }
}
