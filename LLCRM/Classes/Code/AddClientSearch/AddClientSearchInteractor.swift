//
//  AddClientSearchInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2024/3/1.
//

import UIKit

class AddClientSearchInteractor: AddClientSearchInteractorProtocols {
    
    var presenter: AddClientSearchPresenterProtocols?
    
    var entity: AddClientSearchEntityProtocols?
    
    func presenterRequestAddClientSearch(by params: Any?) {
        entity?.getAddClientSearchRequest(by: params)
    }
    
    func didEntityAddClientSearchReceiveData(by params: Any?) {
        presenter?.didGetAddClientSearchInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientSearchReceiveError(error: MyError?) {
        presenter?.didGetAddClientSearchInteractorReceiveError(error: error)
    }
    
    
}
