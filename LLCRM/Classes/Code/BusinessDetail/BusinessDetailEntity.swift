//
//  BusinessDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class BusinessDetailEntity: BusinessDetailEntityProtocols {
    var interactor: BusinessDetailInteractorProtocols?
    
    func didBusinessDetailReceiveData(by params: Any?) {
        interactor?.didEntityBusinessDetailReceiveData(by: params)
    }
    
    func didBusinessDetailReceiveError(error: MyError?) {
        interactor?.didEntityBusinessDetailReceiveError(error: error)
    }
    
    func getBusinessDetailRequest(by params: Any?) {
        LLNetProvider.request(.getBusinessList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didBusinessDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didBusinessDetailReceiveError(error: .requestError)
            }
        }
    }
}
