//
//  BusinessDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/10.
//

import UIKit
//import JXSegmentedView
//import JXPagingView
import HandyJSON
import PKHUD
import JFPopup

class BusinessDetailViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var presenter: BusinessDetailPresenterProtocols?
    var recordModel: recordsModel?
    
    var nameLabel:UILabel?
    var personNameLabel:UILabel?
    var businessNameLabel:UILabel?
    var clientNameLabel:UILabel?
    var codeLabel:UILabel?
    var timeLabel:UILabel?
    
    var model:BusinessListModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "商机"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(BusinessDetailViewController.blackClick))
        
        _ = UIView().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.height.equalTo(10)
            }
        })
        
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = model?.clientName ?? "--"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        personNameLabel = UILabel().then { obj in
            headView.addSubview(obj)

            let personNameHintAttribute = NSMutableAttributedString(string:"负责人：")
            personNameHintAttribute.yy_color = blacktextColor52
            personNameHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let personNameCotentAttribute = NSMutableAttributedString(string:model.productList?[0].maker ?? "--")
//                personNameCotentAttribute.yy_color = blackTextColor80
//                personNameCotentAttribute.yy_font = kMediumFont(13)
//                personNameHintAttribute.append(personNameCotentAttribute)
//                personNameLabel?.attributedText = personNameHintAttribute
//            }else{
            let personNameCotentAttribute = NSMutableAttributedString(string: model?.createrName ?? "--")
            personNameCotentAttribute.yy_color = blackTextColor80
            personNameCotentAttribute.yy_font = kMediumFont(13)
            personNameHintAttribute.append(personNameCotentAttribute)
            obj.attributedText = personNameHintAttribute
//            }
            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalTo(nameLabel!)
//                make.width.equalTo(50)
//                make.height.equalTo(22)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        businessNameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let businessNameHintAttribute = NSMutableAttributedString(string:"商机名称：")
            businessNameHintAttribute.yy_color = blacktextColor52
            businessNameHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let businessNameCotentAttribute = NSMutableAttributedString(string:model.productList?[0].productNo ?? "--")
//                businessNameCotentAttribute.yy_color = bluebgColor
//                businessNameCotentAttribute.yy_font = kMediumFont(13)
//                businessNameHintAttribute.append(businessNameCotentAttribute)
//                obj.attributedText = businessNameHintAttribute
//            }else{
            let businessNameCotentAttribute = NSMutableAttributedString(string:model?.commercialName ?? "--")
            businessNameCotentAttribute.yy_color = bluebgColor
            businessNameCotentAttribute.yy_font = kMediumFont(13)
            businessNameHintAttribute.append(businessNameCotentAttribute)
            obj.attributedText = businessNameHintAttribute
//            }
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(personNameLabel!.snp.bottom).offset(10)
            }
        }
        
        clientNameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let clientNameHintAttribute = NSMutableAttributedString(string:"客户名称：")
            clientNameHintAttribute.yy_color = blacktextColor52
            clientNameHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let clientNameCotentAttribute = NSMutableAttributedString(string:model.productList?[0].productName ?? "--")
//                clientNameCotentAttribute.yy_color = bluebgColor
//                clientNameCotentAttribute.yy_font = kMediumFont(13)
//                clientNameHintAttribute.append(clientNameCotentAttribute)
//                obj.attributedText = clientNameHintAttribute
//            }else{
            let clientNameCotentAttribute = NSMutableAttributedString(string: model?.clientName ?? "--")
                clientNameCotentAttribute.yy_color = bluebgColor
                clientNameCotentAttribute.yy_font = kMediumFont(13)
                clientNameHintAttribute.append(clientNameCotentAttribute)
                obj.attributedText = clientNameHintAttribute
//            }
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(businessNameLabel!.snp.bottom).offset(10)
            }
        }
        
        codeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let codeHintAttribute = NSMutableAttributedString(string:"商机编号：")
            codeHintAttribute.yy_color = blacktextColor52
            codeHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let codeCotentAttribute = NSMutableAttributedString(string:model.productList?[0].productName ?? "--")
//                codeCotentAttribute.yy_color = bluebgColor
//                codeCotentAttribute.yy_font = kMediumFont(13)
//                codeHintAttribute.append(codeCotentAttribute)
//                obj.attributedText = codeHintAttribute
//            }else{
            let codeCotentAttribute = NSMutableAttributedString(string: model?.businessOpportunity ?? "--")
                codeCotentAttribute.yy_color = bluebgColor
                codeCotentAttribute.yy_font = kMediumFont(13)
                codeHintAttribute.append(codeCotentAttribute)
                obj.attributedText = codeHintAttribute
//            }
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(clientNameLabel!.snp.bottom).offset(10)
            }
        }
        
        timeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let timeHintAttribute = NSMutableAttributedString(string:"创建时间：")
            timeHintAttribute.yy_color = blacktextColor52
            timeHintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let timeCotentAttribute = NSMutableAttributedString(string:model.productList?[0].validTimeStart ?? "--")
//                timeCotentAttribute.yy_color = blackTextColor80
//                timeCotentAttribute.yy_font = kMediumFont(13)
//                timeHintAttribute.append(timeCotentAttribute)
//                obj.attributedText = timeHintAttribute
//            }else{
            let timeCotentAttribute = NSMutableAttributedString(string:model?.createDate ?? "--")
                timeCotentAttribute.yy_color = blackTextColor80
                timeCotentAttribute.yy_font = kMediumFont(13)
                timeHintAttribute.append(timeCotentAttribute)
                obj.attributedText = timeHintAttribute
//            }
          
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(codeLabel!.snp.bottom).offset(10)
            }
        }
        
        let progressView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(timeLabel!.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(71)
            }
        }
        
        let progressHintLabel = UILabel().then { obj in
            progressView.addSubview(obj)
            obj.text = "进度："
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor9
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(10)
            }
        }
        
        
        let progressContentBgView = UIView().then { obj in
            progressView.addSubview(obj)
            obj.backgroundColor = bgColor18
            obj.snp.makeConstraints { make in
                make.left.equalTo(progressHintLabel.snp.right).offset(0)
                make.centerY.equalTo(progressHintLabel)
                make.right.equalToSuperview().offset(-120)
                make.height.equalTo(10)
            }
        }
        
        let progressContentView = UIView().then { obj in
            progressContentBgView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-160)
            }
        }
        
        
        let proggressNumberLabel = UILabel().then { obj in
            progressView.addSubview(obj)
            obj.text = "0%"
            obj.textColor = bgColor9
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(progressContentBgView.snp.right).offset(0)
                make.centerY.equalTo(progressContentBgView)
            }
        }
        
        let statusLabel = UILabel().then { obj in
            progressView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.textAlignment = .center
            obj.textColor = .white
            obj.text = "进行中"
            obj.backgroundColor = bgColor19
            obj.font = kRegularFont(8)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.top.equalToSuperview().offset(10)
                make.width.equalTo(38)
                make.height.equalTo(18)
            }
        }
        
        let phaseHintLabel = UILabel().then { obj in
            progressView.addSubview(obj)
            
            let phaseHintAttribute = NSMutableAttributedString(string:"阶段：")
            phaseHintAttribute.yy_color = blackTextColor9
            phaseHintAttribute.yy_font = kRegularFont(12)

            
            let phaseCotentAttribute = NSMutableAttributedString(string: (model?.stageName ?? "--")!)
            phaseCotentAttribute.yy_color = blackTextColor9
            phaseCotentAttribute.yy_font = kMediumFont(12)
            phaseHintAttribute.append(phaseCotentAttribute)
            obj.attributedText = phaseHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        
        let phaseTagLabel = UILabel().then { obj in
            progressView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = bgColor9.cgColor
            obj.textAlignment = .center
            obj.text = "阶段变更"
            obj.backgroundColor = bgColor14
            obj.font = kRegularFont(10)
            obj.snp.makeConstraints { make in
                make.left.equalTo(phaseHintLabel.snp.right).offset(3)
                make.centerY.equalTo(phaseHintLabel)
                make.width.equalTo(64)
                make.height.equalTo(22)
            }
        }
        
        let lineView2 = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(progressView.snp.bottom).offset(10 )
                make.height.equalTo(0.5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        headView.addSubview(self.segmentedView!)
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(lineView2.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["商机跟随", "详细信息", "商机联系人", "商机联系人"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        segmentedView?.indicators = [indicator]
        pagingView = JXPagingView(delegate: self)
        headView.addSubview(pagingView!)
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
        
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        _ = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        let editorImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_editor")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let editorLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "编辑"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(editorImageV.snp.bottom).offset(5)
                make.centerX.equalToSuperview()
            }
        }
        
        let editorButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalToSuperview()
            }
        }
        
        let recordImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_record")
            obj.snp.makeConstraints { make in
                make.right.equalTo(editorImageV.snp.left).offset(-68)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let recordLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "沟通记录"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(recordImageV.snp.bottom).offset(5)
                make.centerX.equalTo(recordImageV)
            }
        }
        
        let recordButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(recordImageV)
            }
        }
        
        
        let moreImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_more")
            obj.snp.makeConstraints { make in
                make.left.equalTo(editorImageV.snp.right).offset(68)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let moreLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "更多"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(moreImageV.snp.bottom).offset(5)
                make.centerX.equalTo(moreImageV)
            }
        }
        
        let moreButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(moreImageV)
            }
        }
        
        cofing()
        
//        self.popup.bottomSheet { [weak self] in
//            let showView = CompanyShowView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
//            showView.closeBlock = { [weak self] in
//                self?.popup.dismissPopup()
//            }
//            return showView
//        }
        
        // Do any additional setup after loading the view.
    }
    
    func cofing(){
        let router = BusinessDetailRouter()
        
        presenter = BusinessDetailPresenter()
        
        presenter?.router = router
        
        let entity = BusinessDetailEntity()
        
        let interactor = BusinessDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        
//        presenter?.presenterRequestGetBusinessDetail(by: ["id":model?.id])
    }
    
    @objc func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension BusinessDetailViewController : JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = BusinessFollowViewController()
            vc.model = self.model
            return vc
        }else if index == 1{
            let vc = BusinessDetailInfosViewController()
            vc.model = self.model
            return vc
        }else if index == 2{
            let vc = BusinessDetailLinkmanViewController()
            vc.model = self.model
            return vc
        }
        
        let vc = BusinessDetailOtherViewController()
        vc.model = self.model
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 4
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
//    zZ
    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}

extension BusinessDetailViewController: BusinessDetailViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    func didGetBusinessDetailPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            printTest(dict)
//            let dataDic = dict["data"] as? [String:Any]
//            if dict["code"] as! Int == 200{
//                if let model = JSONDeserializer<PriceDetailModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
//                    
////                    self.model = model
//                    
////                    nameLabel?.text = model.clientName ?? "--"
////
////                    let dataList = model.productList
//                    
//                    
////
////                    let accountHintAttribute = NSMutableAttributedString(string:"负责人：")
////                    accountHintAttribute.yy_color = blacktextColor52
////                    accountHintAttribute.yy_font = kMediumFont(13)
////                    let accoubtCotentAttribute = NSMutableAttributedString(string:model.clientClueContactSimpleParamList?[0].name ?? "暂无")
////                    accoubtCotentAttribute.yy_color = blackTextColor80
////                    accoubtCotentAttribute.yy_font = kMediumFont(13)
////                    accountHintAttribute.append(accoubtCotentAttribute)
////                    personNameLabel!.attributedText = accountHintAttribute
////
////                    let lastTimeHintAttribute = NSMutableAttributedString(string: "最后跟进时间：")
////                    lastTimeHintAttribute.yy_color = blacktextColor52
////                    lastTimeHintAttribute.yy_font = kMediumFont(13)
////                    let lastTimeCotentAttribute = NSMutableAttributedString(string: model.claimDate ?? "暂无")
////                    lastTimeCotentAttribute.yy_color = blackTextColor80
////                    lastTimeCotentAttribute.yy_font = kMediumFont(13)
////                    lastTimeHintAttribute.append(lastTimeCotentAttribute)
////                    lastTimeLabel!.attributedText = lastTimeHintAttribute
////
////                    infosModel = model
////
//                    pagingView?.reloadData()
//                }
//            }else{
//                HUD.flash(.label(dict["message"] as? String), delay: 2)
//            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetBusinessDetailPresenterReceiveError(error: MyError?) {
        
    }
    
    
}

