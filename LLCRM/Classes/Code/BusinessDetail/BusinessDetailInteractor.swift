//
//  BusinessDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class BusinessDetailInteractor: BusinessDetailInteractorProtocols {
    var presenter: BusinessDetailPresenterProtocols?
    
    var entity: BusinessDetailEntityProtocols?
    
    func presenterRequestBusinessDetail(by params: Any?) {
        entity?.getBusinessDetailRequest(by: params)
    }
    
    func didEntityBusinessDetailReceiveData(by params: Any?) {
        presenter?.didGetBusinessDetailInteractorReceiveData(by: params)
    }
    
    func didEntityBusinessDetailReceiveError(error: MyError?) {
        presenter?.didGetBusinessDetailInteractorReceiveError(error: error)
    }
    
    
}
