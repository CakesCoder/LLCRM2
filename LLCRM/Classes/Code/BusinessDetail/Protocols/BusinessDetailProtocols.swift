//
//  BusinessDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class BusinessDetailProtocols: NSObject {

}

protocol BusinessDetailViewProtocols: AnyObject {
    var presenter: BusinessDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetBusinessDetailPresenterReceiveData(by params: Any?)
    func didGetBusinessDetailPresenterReceiveError(error: MyError?)
}

protocol BusinessDetailPresenterProtocols: AnyObject{
    var view: BusinessDetailViewProtocols? { get set }
    var router: BusinessDetailRouterProtocols? { get set }
    var interactor: BusinessDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetBusinessDetail(by params: Any?)
    func didGetBusinessDetailInteractorReceiveData(by params: Any?)
    func didGetBusinessDetailInteractorReceiveError(error: MyError?)
}

protocol BusinessDetailInteractorProtocols: AnyObject {
    var presenter: BusinessDetailPresenterProtocols? { get set }
    var entity: BusinessDetailEntityProtocols? { get set }
    
    func presenterRequestBusinessDetail(by params: Any?)
    func didEntityBusinessDetailReceiveData(by params: Any?)
    func didEntityBusinessDetailReceiveError(error: MyError?)
}

protocol BusinessDetailEntityProtocols: AnyObject {
    var interactor: BusinessDetailInteractorProtocols? { get set }
    
    func didBusinessDetailReceiveData(by params: Any?)
    func didBusinessDetailReceiveError(error: MyError?)
    func getBusinessDetailRequest(by params: Any?)
}

protocol BusinessDetailRouterProtocols: AnyObject {
    func pushToMyBusinessList(from previousView: UIViewController)
    func pushToMyAddBusinessList(from previousView: UIViewController)
    func pushToBusinessListDetail(from previousView: UIViewController , forModel model: Any)
}

