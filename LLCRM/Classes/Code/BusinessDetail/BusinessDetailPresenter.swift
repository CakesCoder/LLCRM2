//
//  BusinessDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class BusinessDetailPresenter: BusinessDetailPresenterProtocols {
    var view: BusinessDetailViewProtocols?
    
    var router: BusinessDetailRouterProtocols?
    
    var interactor: BusinessDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetBusinessDetail(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestBusinessDetail(by: params)
    }
    
    func didGetBusinessDetailInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetBusinessDetailPresenterReceiveData(by: params)
    }
    
    func didGetBusinessDetailInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetBusinessDetailPresenterReceiveError(error: error)
    }
}
