//
//  CompanyShowView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/11.
//

import UIKit

class ShowSelectedModel: BaseModel{
    var name:String?
    var selected: Bool?
    required init() {
    }
}

class CompanyShowView: UIView {
    
    var closeBlock: (() -> ())?
    var dataLIst:[Any]? = []
    var currentType:Int? = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let bgView = UIView().then { obj in
            self.addSubview(obj)
            //            obj.backgroundColor = .blue
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(340)
            }
        }
        if self.currentType == 0{
            let names =  ["零瓴软件技术（深圳）有限公司", "安高创新（深圳）科技有限公司", "深圳市安高体育运动服务有限公司","上海旺旺食品集团有限公司", "山东安高电气设备有限公司", "北京抖音信息服务有限公司"]
            for i in 0..<6{
                let model = ShowSelectedModel()
                model.name = names[i]
                if i == 0{
                    model.selected = true
                }else{
                    model.selected = false
                }
                dataLIst?.append(model)
            }
        }else if self.currentType == 1{
            let names =  ["老客户推荐", "老客户复购", "新客户成交"]
            for i in 0..<6{
                let model = ShowSelectedModel()
                model.name = names[i]
                if i == 0{
                    model.selected = true
                }else{
                    model.selected = false
                }
                dataLIst?.append(model)
            }
        }else if self.currentType == 2{
            let names =  ["阶段一", "阶段二", "阶段三", "阶段四", "阶段五", "阶段六"]
            for i in 0..<6{
                let model = ShowSelectedModel()
                model.name = names[i]
                if i == 0{
                    model.selected = true
                }else{
                    model.selected = false
                }
                dataLIst?.append(model)
            }
        }else if self.currentType == 3{
            let names =  ["战略客户", "大客户", "企业客户", "加工户", "代理商", "加工户"]
            for i in 0..<6{
                let model = ShowSelectedModel()
                model.name = names[i]
                if i == 0{
                    model.selected = true
                }else{
                    model.selected = false
                }
                dataLIst?.append(model)
            }
        }else{
            let names =  ["立项", "确认需求（非标/标准）", "非标方案申请", "询价", "试机", "商务谈判", "赢单", "输单", "无效"]
            for i in 0..<6{
                let model = ShowSelectedModel()
                model.name = names[i]
                if i == 0{
                    model.selected = true
                }else{
                    model.selected = false
                }
                dataLIst?.append(model)
            }
        }
        
        
        _ = UITableView().then({ obj in
            bottomView.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(ShowSelectedCell.self, forCellReuseIdentifier: "ShowSelectedCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(25)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(73)
            }
        })
        
        let cancelButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.setTitle("取消", for: .normal)
            obj.setTitleColor(.black, for: .normal)
            obj.titleLabel?.font = kMediumFont(16)
            obj.addTarget(self, action: #selector(cancelClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
    }
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @objc func cancelClick(){
        self.closeBlock?()
    }
    

}

extension CompanyShowView:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataLIst?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShowSelectedCell", for: indexPath) as! ShowSelectedCell
//        let model = dataLIst![indexPath.row] as? ShowSelectedModelss
        cell.model = dataLIst![indexPath.row] as? ShowSelectedModel
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<(dataLIst?.count)!{
            let model = dataLIst![i]  as? ShowSelectedModel
            model!.selected = false
        }
        let model = dataLIst![indexPath.row] as? ShowSelectedModel
        model?.selected = true
        tableView.reloadData()
    }
}

class ShowSelectedCell: UITableViewCell{
    
    var nameLabel:UILabel?
    var selectedButton:UIButton?
    var model:ShowSelectedModel?{
        didSet{
            if model?.selected == true{
                nameLabel?.textColor = bluebgColor
            }else{
                nameLabel?.textColor = blackTextColor13
            }
            
            if model?.selected == true{
                selectedButton?.isHidden = false
            }else{
                selectedButton?.isHidden = true
            }
            
            nameLabel?.text = model?.name
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = model?.name ?? ""
            
            obj.font = kMediumFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.centerY.equalToSuperview()
            }
        }
       
        
        selectedButton = UIButton().then({ obj in
            contentView.addSubview(obj)
            obj.layer.cornerRadius = 10
            obj.backgroundColor = .blue
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-35)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        })
        
        
       _ = UIView().then({ obj in
           contentView.addSubview(obj)
           obj.backgroundColor = lineColor7
           obj.snp.makeConstraints { make in
               make.left.equalToSuperview().offset(15)
               make.right.equalToSuperview().offset(-15)
               make.bottom.equalToSuperview().offset(-0)
               make.height.equalTo(0.5)
           }
       })
    }
}

