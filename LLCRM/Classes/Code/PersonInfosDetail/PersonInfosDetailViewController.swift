//
//  PersonInfosDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class PersonInfosDetailViewController: BaseViewController {
    var isShowFeature:Bool? = true
    
    var selectedBgView: UIView? = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "个人信息"
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(158)
            }
        }
        
        let imageV = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .red
            obj.layer.cornerRadius = 31.5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
                make.width.height.equalTo(63)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(20)
                make.left.equalTo(imageV.snp.right).offset(15)
            }
        }
        
        let subLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "客户主管2级-零瓴团队"
            obj.textColor = blackTextColor57
            obj.font = kMediumFont(14)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.top.equalTo(nameLabel.snp.bottom).offset(20)
            }
        }
        
        
        let lineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(imageV.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        let bottomView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lineView.snp.bottom).offset(0)
                make.height.equalTo(60)
            }
        }
        
        let w = kScreenWidth/3.0
        let names = ["全部","工作回复","工作文档"]
        for i in 0..<3{
            let x = i%3
            let y = i/3
            let bgView = UIView().then { obj in
                obj.frame = CGRect(x: w*CGFloat(x), y: 0, width: w, height: 60)
                bottomView.addSubview(obj)
            }
            
            let lineView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor21
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-0)
                    make.top.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-15)
                    make.width.equalTo(0.5)
                }
            }
            
            let nameLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.text = names[i]
                if i == 0{
                    obj.textColor = bluebgColor
                }else{
                    obj.textColor = blackTextColor58
                }
                obj.font = kMediumFont(14)
                obj.snp.makeConstraints { make in
                    make.center.equalToSuperview()
                }
            }
            
            if i == 0{
                let tagImageV = UIImageView().then { obj in
                    bgView.addSubview(obj)
                    obj.backgroundColor = .blue
                    obj.snp.makeConstraints { make in
                        make.centerX.equalTo(nameLabel)
                        make.top.equalTo(nameLabel.snp.bottom).offset(0)
                        make.width.height.equalTo(10)
                    }
                }
            }
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                obj.addTarget(self, action: #selector(showClick(_:)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
        }
        
        let bottomLineView = UIView().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                //                make.top.equalTo(bottomView.snp.bottom).offset(0)
                make.height.equalTo(0.5)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            if #available(iOS 11.0, *) {
                obj.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
            } else {
                // Fallback on earlier versions
            }
            obj.register(WorkTableViewCell.self, forCellReuseIdentifier: "WorkTableViewCell")
            obj.register(StaffTableViewCell.self, forCellReuseIdentifier: "StaffTableViewCell")
            obj.delegate = self
            obj.dataSource = self
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(bottomView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        
        selectedBgView = UIView().then({ obj in
            view.addSubview(obj)
            obj.isHidden = isShowFeature!
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
        
        let selectedView = UIView().then { obj in
            selectedBgView?.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(160+UIDevice.xp_navigationFullHeight())
                make.height.equalTo(185)
            }
        }
        
        let selectedContentView = UIView().then { obj in
            selectedView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let tags = ["全部", "分享", "日志", "审批", "任务", "日程", "指令", "销售记录", "外勤"]
        let tag_w = (kScreenWidth-30)/4
        for i in 0..<tags.count{
            let x = i%4
            let y = i/4
            let tagView = UIView().then { obj in
                selectedContentView.addSubview(obj)
                obj.frame = CGRect(x: Int(tag_w)*x, y: 60*y, width: Int(tag_w), height: 60)
            }
            
            let nameLabel = UILabel().then { obj in
                tagView.addSubview(obj)
                obj.text = tags[i]
                obj.textColor = .green
                obj.textAlignment = .center
                obj.textColor = blackTextColor59
                obj.font = kMediumFont(14)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
            
            if x != 3{
                _ = UIView().then { obj in
                    tagView.addSubview(obj)
                    obj.backgroundColor = lineColor21
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-0)
                        make.top.equalToSuperview().offset(15)
                        make.bottom.equalToSuperview().offset(-15)
                        make.width.equalTo(0.5)
                    }
                }
            }
            
            let btn = UIButton().then { obj in
                tagView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        selectedBgView?.isHidden = !isShowFeature!
        isShowFeature = !isShowFeature!
    }
    
    @objc func showClick(_ btn:UIButton){
        selectedBgView?.isHidden = !isShowFeature!
        isShowFeature = !isShowFeature!
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PersonInfosDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("self.dataSource.array.count = \(self.dataSource.array.count)")
//        return self.dataSource.array.count
        return 20
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 830
        }
        return 550
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkTableViewCell", for: indexPath) as! WorkTableViewCell
            cell.moreBlock = {
                self.popup.bottomSheet { [weak self] in
                    printTest(self)
                    let showView = WorkCreateView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                    showView.featureBlock = {[weak self] tag in
//                        if tag == 1000{
//                            self?.presenter?.router?.pushToNextVC(from: self!)
//                        }else if tag == 1001{
//                            self?.presenter?.router?.pushAddPlan(from: self!)
//                        }else if tag == 1002{
//                            self?.presenter?.router?.pushAddApproval(from: self!)
//                        }else if tag == 1003{
//                            self?.presenter?.router?.pushAddTask(from: self!)
//                        }else if tag == 1004{
//                            self?.presenter?.router?.pushSchedule(from: self!)
//                        }
//                        else{
//                            
//                        }
                        self?.popup.dismissPopup()
                    }
                    return showView
                }
            }
            cell.forwardBlock = {
                let showView = WorkMoreView()
                
               
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffTableViewCell", for: indexPath)
        return cell
    }
}
