//
//  SampleSkillDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
//import JXPagingView

extension SampleSkillDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SampleSkillDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}


class SampleSkillDetailViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var model:DeviceSampleVoListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let v = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(199)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            v.addSubview(obj)
//            var temp_hint = NSMutableAttributedString(string: "")
//            for j in 0..<baseInfos[i].count{
                let hintAttribute = NSMutableAttributedString(string:"规格参数名称：")
//                if baseInfos[i].count == 1{
                    hintAttribute.yy_color = blackTextColor4
                    hintAttribute.yy_font = kMediumFont(12)
                    obj.attributedText = hintAttribute
//                }else{
//                    if j == 0{
//                        hintAttribute.yy_color = blackTextColor27
//                        hintAttribute.yy_font = kMediumFont(12)
//                        temp_hint = hintAttribute
//                    }else{
//                        hintAttribute.yy_color = blackTextColor3
//                        hintAttribute.yy_font = kMediumFont(12)
//                        temp_hint.append(hintAttribute)
//                        obj.attributedText = temp_hint
//                    }
//                }
//            }
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(30)
            }
        }
        
        
        
        let contentLabel = UILabel().then { obj in
            v.addSubview(obj)
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor81
            obj.numberOfLines = 0
            let sampleSpecsList = model?.deviceSampleSpecsList ?? []
            var sampleSpecsModel:DeviceSampleSpecsList? = nil
            if sampleSpecsList != nil && sampleSpecsList.count > 0{
                sampleSpecsModel = sampleSpecsList[0]
            }
            obj.text = "规格 / = / \(sampleSpecsModel?.specsConfirm ?? "--")\n 参数/ < / \(sampleSpecsModel?.specsName ?? "--")\n 单位/ < / \(sampleSpecsModel?.unit ?? "--")"
//                if i == 0{
//                    followDateLabel = obj
//                }else if i == 1{
//                    followThemeLabel = obj
//                }else if i == 2{
//                    followContent = obj
//                }else if i == 3{
//                    perosonLabel = obj
//                }else if i == 4{
//                    commitDateLabel = obj
//                }else {
//                    possibilityLabel = obj
//                }
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(120)
//                make.centerY.equalToSuperview()
                make.top.equalToSuperview().offset(30)
            }
        }
            
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
