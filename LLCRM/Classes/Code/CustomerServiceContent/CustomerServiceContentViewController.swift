//
//  CustomerServiceContentViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit
//import JXPagingView

extension CustomerServiceContentViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension CustomerServiceContentViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class CustomerServiceContentViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var recordModel: CustomerServiceRecordsModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let headHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            headHintView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            headHintView.addSubview(obj)
            obj.text = "客诉内容"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = [["客诉单号："],["客户名称："],["客诉主题："],["主要联系人："],["客诉类别："],["客户投诉单号： "],["处理人："],["问题描述："]]

        
        let contentInfos = ["CC230516001", "零瓴软件技术(深圳)有限公司", "客户推荐", "主题主题主题主题主题主题主题", "类别类别", "230516001", "— —", "— —"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(100)
                    make.height.equalTo(20)
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
//                obj.text = contentInfos[i]
                if i == 0{
                    obj.text = recordModel?.clientDiscardNo ?? "--"
                }else if i == 1{
                    obj.text = recordModel?.clientName ?? "--"
                }else if i == 2{
                    obj.text = "--"
                }else if i == 3{
                    obj.text = "--"
                }else if i == 4{
                    obj.text = "--"
                }else if i == 5{
                    obj.text = "--"
                }else if i == 6{
                    obj.text = "--"
                }else if i == 7{
                    obj.text = recordModel?.specDesc ?? "--"
                }
                obj.snp.makeConstraints { make in
                    make.left.equalTo(hintLabel.snp.right).offset(15)
                    make.centerY.equalToSuperview()
                }
            }
    
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let attachemenetView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(baseInfos.count*45)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(102)
            }
        }
        
        let attacheMentHintLabel = UILabel().then { obj in
            attachemenetView.addSubview(obj)
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.text = "附件上传"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        }
        
        for i in 0..<2{
            let v = UIView().then { obj in
                attachemenetView.addSubview(obj)
                obj.backgroundColor = bgColor13
                obj.layer.cornerRadius = 2
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(43+36*i)
                    make.right.equalToSuperview().offset(-100)
                    make.height.equalTo(26)
                }
            }
            
            let imageV = UIImageView().then { obj in
                v.addSubview(obj)
//                obj.backgroundColor = .blue
                obj.image = UIImage(named: "flie_pdf")
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(14)
                    make.centerY.equalToSuperview()
                    make.width.height.equalTo(17)
                }
            }
            
            let namelabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.text = "报价信息表.PDF"
                obj.textColor = bluebgColor
                obj.font = kMediumFont(12)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(imageV.snp.right).offset(10)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIImageView().then { obj in
                v.addSubview(obj)
//                obj.backgroundColor = .blue
                obj.image = UIImage(named: "flie_delete")
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-14)
                    make.centerY.equalToSuperview()
                    make.width.height.equalTo(17)
                }
            }
        }
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
