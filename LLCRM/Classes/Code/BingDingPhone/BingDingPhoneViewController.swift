//
//  BingDingPhoneViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class BingDingPhoneViewController: BaseViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "绑定手机号"
        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let imageV = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(80)
                make.centerX.equalToSuperview()
                make.width.height.equalTo(118)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "当前手机号"
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(imageV.snp.bottom).offset(30)
                make.centerX.equalToSuperview()
            }
        }
        
        let phoneLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "13783719291"
            obj.font = kRegularFont(18)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.top.equalTo(hintLabel.snp.bottom).offset(30)
                make.centerX.equalToSuperview()
            }
        }
        
        let bottomHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "更换手机号，下次可使用新手机号登录。并会更新企业通讯录的号码信息"
            obj.numberOfLines = 2
            obj.font = kRegularFont(14)
            obj.textColor = blackTextColor
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.top.equalTo(phoneLabel.snp.bottom).offset(67)
//                make.centerX.equalToSuperview()
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let changeButton = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setTitle("更换手机号", for: .normal)
            obj.titleLabel?.font = kMediumFont(15)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 2
            obj.snp.makeConstraints { make in
                make.top.equalTo(bottomHintLabel.snp.bottom).offset(67)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
