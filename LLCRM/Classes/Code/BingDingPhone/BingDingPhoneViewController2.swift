//
//  BingDingPhoneViewController2.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class BingDingPhoneViewController2: BaseViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "绑定手机号"
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let hintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "绑定手机号"
            obj.font = kMediumFont(18)
            obj.textColor = blackTextColor17
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(28)
            }
        }
        
        let accountView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
                make.top.equalTo(hintLabel.snp.bottom).offset(40)
            }
        }
        
        let accountTextFiled = UITextField().then { obj in
            accountView.addSubview(obj)
            obj.placeholder = "手机号"
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-72)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let codeButton = UIButton().then { obj in
            accountView.addSubview(obj)
            
            obj.setTitle("获取验证码", for: .normal)
            obj.titleLabel?.font = kMediumFont(14)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.snp.makeConstraints { make in
                make.left.equalTo(accountTextFiled.snp.right).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-0)
            }
        }
        
        let accountLineView = UIView().then { obj in
            accountView.addSubview(obj)
            obj.backgroundColor = lineColor5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let psdView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
                make.top.equalTo(accountView.snp.bottom).offset(40)
            }
        }
        
        let psdTextFiled = UITextField().then { obj in
            psdView.addSubview(obj)
            obj.placeholder = "验证码"
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-72)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
    
        let psdLineView = UIView().then { obj in
            psdView.addSubview(obj)
            obj.backgroundColor = lineColor5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let okButton = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setTitle("绑定手机号", for: .normal)
            obj.titleLabel?.font = kMediumFont(15)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 2
            obj.snp.makeConstraints { make in
                make.top.equalTo(psdView.snp.bottom).offset(67)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
