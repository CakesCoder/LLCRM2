//
//  SampleEnvironmentDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
//import JXPagingView

extension SampleEnvironmentDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SampleEnvironmentDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class SampleEnvironmentDetailViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var model:DeviceSampleVoListModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 13*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfos = [["认证要求："],["认证类型："],["认证机构："],["性质："],["备注："],["环保要求： "],["要求类型："],["环保类型："]]
        
        let contentInfos = ["需要", "—", "—", "制造商", "这是一段很长的文字……", "需要", "必要", "环保环保环保"]
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor4
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor4
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
                let authList = model?.deviceSampleAuthList ?? []
                var authModel:DeviceSampleAuthListModel? = nil
                if authList != nil && authList.count > 0{
                    authModel = authList[0]
                }
                if i == 0{
                    obj.text = "--"
                }else if i == 1{
                    if authModel != nil{
                        obj.text = authModel?.authType ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 3{
                    if authModel != nil{
                        obj.text = authModel?.authMechanism ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 4{
                    if authModel != nil{
                        obj.text = authModel?.nature ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 5{
                    if authModel != nil{
                        obj.text = authModel?.remark ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 6{
//                    model?.environmentProtectionRequirement ??
//                    if authModel != nil{
                        obj.text = model?.environmentProtectionRequirement ?? "--"
//                    }else{
//                        obj.text = "--"
//                    }
                }else if i == 7{
                    obj.text = model?.askType ?? "--"
                }else if i == 8{
                    obj.text = model?.environmentProtectionType ?? "--"
                }
//                obj.text = contentInfos[i]
//                if i == 0{
//                    followDateLabel = obj
//                }else if i == 1{
//                    followThemeLabel = obj
//                }else if i == 2{
//                    followContent = obj
//                }else if i == 3{
//                    perosonLabel = obj
//                }else if i == 4{
//                    commitDateLabel = obj
//                }else {
//                    possibilityLabel = obj
//                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(120)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
