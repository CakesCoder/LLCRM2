//
//  PunchCardRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class PunchCardRouter: PunchCardRouterProtocols {
    func pushToVacationManage(from previousView: UIViewController) {
        let vc = VacationManageViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToRecords(from previousView: UIViewController){
        let vc = DateRecordsViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToRule(from previousView: UIViewController){
        let vc = RuleViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToChangePunchDate(from previousView: UIViewController){
        let vc = ChangePunchDateViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToStatistics(from previousView: UIViewController){
        let vc = StatisticsViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToAttendanceRule(from previousView: UIViewController){
        let vc = AttendanceRuleViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
}
