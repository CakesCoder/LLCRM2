//
//  PunchCardPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class PunchCardPresenter: PunchCardPresenterProtocols {
    var view: PunchCardViewProtocols?
    
    var router: PunchCardRouterProtocols?
    
    var interactor: PunchCardInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
