//
//  NoClockInTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/20.
//

import UIKit

class NoClockInTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let pointView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor10
            obj.layer.cornerRadius = 6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(27)
                make.top.equalToSuperview().offset(12)
                make.width.height.equalTo(12)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "上班打卡  09:00"
            obj.textColor = blackTextColor8
            obj.font = kMediumFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalTo(pointView.snp.right).offset(6)
                make.centerY.equalTo(pointView)
            }
        }
        
        let buttonView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor69
            obj.layer.cornerRadius = 65.5
            // shadowCode
            obj.layer.shadowColor = UIColor(red: 0.31, green: 0.52, blue: 1, alpha: 1).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 25
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(60)
                make.centerX.equalToSuperview()
                make.width.height.equalTo(131)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            buttonView.addSubview(obj)
            obj.textColor = .white
            obj.font = kMediumFont(15)
            obj.text = "上班打卡"
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(38)
                make.centerX.equalToSuperview()
            }
        }
        
        let timeLabel = UILabel().then { obj in
            buttonView.addSubview(obj)
            obj.text = "09:00:00"
            obj.textColor = .white
            obj.font = kMediumFont(24)
            obj.snp.makeConstraints { make in
                make.top.equalTo(hintLabel.snp.bottom).offset(4)
                make.centerX.equalToSuperview()
            }
        }
        
        let button = UIView().then { obj in
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(60)
                make.centerX.equalToSuperview()
                make.width.height.equalTo(131)
            }
        }
        
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "已进入打卡范围"
            obj.textColor = blackTextColor69
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(buttonView.snp.bottom).offset(10)
                make.centerX.equalToSuperview()
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor10
            obj.snp.makeConstraints { make in
                make.top.equalTo(pointView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.centerX.equalTo(pointView)
                make.width.equalTo(1)
            }
        })
    }

}
