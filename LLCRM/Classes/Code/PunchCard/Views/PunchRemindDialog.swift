//
//  PunchRemindDialog.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/20.
//

import UIKit
import YYText_swift

class PunchRemindDialog: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        
        let bgView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 14
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(209)
                make.centerX.equalTo(self)
                make.width.equalTo(268)
                make.height.equalTo(178)
            }
        }
        
        let remarkHintLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "备注"
            obj.font = kMediumFont(17)
            obj.textColor = blackTextColor5
//            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(10)
                make.centerX.equalToSuperview()
            }
        }
        
        let textbgView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor24.cgColor
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(remarkHintLabel.snp.bottom).offset(10)
                make.height.equalTo(77)
            }
        }
        
        let textView = YYTextView().then { obj in
            textbgView.addSubview(obj)
            obj.placeholderText = "请输入"
            obj.placeholderTextColor = blackTextColor3
            obj.placeholderFont = kRegularFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(5)
                make.right.equalToSuperview().offset(-5)
                make.top.equalToSuperview().offset(5)
                make.bottom.equalToSuperview().offset(-5)
            }
        }
        
        let lineView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = lineColor24
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(textbgView.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        let lineView2 = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = lineColor24
            obj.snp.makeConstraints { make in
                make.width.equalTo(0.5)
                make.centerX.equalToSuperview()
                make.bottom.equalToSuperview().offset(-0)
                make.top.equalTo(lineView.snp.bottom).offset(0)
            }
        }
        
        let cancelButton = UIButton().then { obj in
            bgView.addSubview(obj)
            obj.setTitle("取消", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.titleLabel?.font = kMediumFont(17)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(lineView.snp.bottom).offset(0)
                make.width.equalTo(134)
                make.height.equalTo(44)
//                make.bottom.equalToSuperview().offset(-0)
//                make.right.equalTo(lineView2.snp.left).offset(-0)
            }
        }
        
        let okButton = UIButton().then { obj in
            bgView.addSubview(obj)
            obj.setTitle("确定", for: .normal)
            obj.setTitleColor(blackTextColor71, for: .normal)
            obj.titleLabel?.font = kMediumFont(17)
            obj.snp.makeConstraints { make in
//                make.left.equalTo(lineView2.snp.right).offset(0)
//                make.top.equalTo(lineView.snp.bottom).offset(0)
                make.width.equalTo(134)
                make.height.equalTo(44)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-0)
            }
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
