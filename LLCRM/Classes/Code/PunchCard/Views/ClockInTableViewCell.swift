//
//  ClockInTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/20.
//

import UIKit

class ClockInTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let pointView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor69
            obj.layer.cornerRadius = 6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(27)
                make.top.equalToSuperview().offset(12)
                make.width.height.equalTo(12)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor10
            obj.snp.makeConstraints { make in
                make.top.equalTo(pointView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.centerX.equalTo(pointView)
                make.width.equalTo(1)
            }
        })
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "上班打卡  09:00"
            obj.textColor = blackTextColor8
            obj.font = kMediumFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalTo(pointView.snp.right).offset(6)
                make.centerY.equalTo(pointView)
            }
        }
        
        let tagLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor36
            obj.text = "迟到"
            obj.font = kRegularFont(8)
            obj.textColor = .white
            obj.layer.cornerRadius = 2
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel.snp.right).offset(6)
                make.centerY.equalTo(pointView)
                make.width.equalTo(30)
                make.height.equalTo(15)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "打卡时间：09:45:10"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor69
            obj.snp.makeConstraints { make in
                make.left.equalTo(pointView.snp.right).offset(6)
//                make.centerY.equalTo(pointView)
                make.top.equalTo(tagLabel.snp.bottom).offset(10)
            }
        }
        
        let butLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "原因：天气原因"
            obj.textColor = blackTextColor8
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(pointView.snp.right).offset(6)
                make.top.equalTo(timeLabel.snp.bottom).offset(10)
            }
        }
        
        let netImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalTo(pointView.snp.right).offset(6)
                make.top.equalTo(butLabel.snp.bottom).offset(10)
                make.width.height.equalTo(15)
            }
        }
        
        let netNameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "linglingsfot-5G"
            obj.textColor = blackTextColor22
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(netImageV.snp.right).offset(0)
                make.centerY.equalTo(netImageV)
            }
        }
        
        let requestLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "补卡申请 >"
            obj.textColor = blackTextColor69
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-20)
                make.bottom.equalToSuperview().offset(-5)
            }
        }
        
        let requestButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-20)
                make.bottom.equalToSuperview().offset(-5)
                make.width.equalTo(50)
                make.height.equalTo(20)
            }
        }
        
    }

}
