//
//  PunchCardProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

protocol PunchCardViewProtocols: AnyObject {
    var presenter: PunchCardPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol PunchCardPresenterProtocols: AnyObject{
    var view: PunchCardViewProtocols? { get set }
    var router: PunchCardRouterProtocols? { get set }
    var interactor: PunchCardInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol PunchCardInteractorProtocols: AnyObject {
    var presenter: PunchCardPresenterProtocols? { get set }
    var entity: PunchCardEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol PunchCardEntityProtocols: AnyObject {
    var interactor: PunchCardInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol PunchCardRouterProtocols: AnyObject {
    func pushToVacationManage(from previousView: UIViewController)
    func pushToRecords(from previousView: UIViewController)
    func pushToRule(from previousView: UIViewController)
    func pushToChangePunchDate(from previousView: UIViewController)
    func pushToStatistics(from previousView: UIViewController)
    func pushToAttendanceRule(from previousView: UIViewController)
}
