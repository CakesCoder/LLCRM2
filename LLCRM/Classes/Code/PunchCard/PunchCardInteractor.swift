//
//  PunchCardInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class PunchCardInteractor: PunchCardInteractorProtocols {
    var presenter: PunchCardPresenterProtocols?
    
    var entity: PunchCardEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
    

}
