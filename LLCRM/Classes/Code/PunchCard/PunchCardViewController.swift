//
//  PunchCardViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit
import JFPopup


class PunchCardViewController: BaseViewController {
    
    var presenter: PunchCardPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.top.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(245+UIDevice.xp_navigationFullHeight())
            }
        }
        
        let headImageV = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 30)
                make.bottom.equalToSuperview().offset(-25)
            }
        }
        
        let backButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "nav_back"), for: .normal)
            obj.addTarget(self, action: #selector(bakcClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(UIDevice.xp_statusBarHeight()+10)
            }
        }
        
        let rightButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.setTitle("记录", for: .normal)
            obj.setTitleColor(blackTextColor3, for: .normal)
            obj.titleLabel?.font = kRegularFont(14)
            obj.addTarget(self, action: #selector(recordsClick(_ :)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(UIDevice.xp_statusBarHeight()+10)
                make.width.equalTo(30)
                make.height.equalTo(20)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "你好，夏淼"
            obj.textColor = blackTextColor67
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(24)
                make.top.equalTo(backButton.snp.bottom).offset(30)
                make.right.equalToSuperview().offset(-24)
            }
        }
        
        let hintLabel2 = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "没有困难的工作，只有勇敢的打工人。"
            obj.textColor = blackTextColor67
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(24)
                make.top.equalTo(hintLabel.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-24)
            }
        }
        
        
        let localView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 25
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(50)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let localImageV = UIImageView().then { obj in
            localView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }
        
        let localLabel = UILabel().then { obj in
            localView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.text = "地点：大园工业区北区"
            obj.textColor = blackTextColor3
            obj.font = kRegularFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalTo(localImageV.snp.right).offset(10)
                make.top.equalToSuperview().offset(8)
            }
        }
        
        let networkLabel = UILabel().then { obj in
            localView.addSubview(obj)
            obj.text = "已在wifi打卡范围内"
            obj.textColor = blackTextColor68
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(localImageV.snp.right).offset(10)
                make.bottom.equalToSuperview().offset(-8)
            }
        }
        
        let reloadButton = UIButton().then { obj in
            localView.addSubview(obj)
            obj.setTitle("刷新定位", for: .normal)
            obj.titleLabel?.font = kRegularFont(13)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.addTarget(self, action: #selector(realoadClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-30)
                make.centerY.equalToSuperview()
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource =  self
            obj.separatorStyle = .none
            obj.register(NoClockInTableViewCell.self, forCellReuseIdentifier: "NoClockInTableViewCell")
            obj.register(ClockInTableViewCell.self, forCellReuseIdentifier: "ClockInTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(headView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_navigationFullHeight()-20)
                make.right.equalToSuperview().offset(-0)
            }
        })
        
        _ = UIView().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor10
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(tableView.snp.bottom).offset(0)
                make.height.equalTo(0.5)
            }
        })
        
        let w = kScreenWidth/3
        let titles = ["打卡", "统计", "设置"]
        for i in 0..<titles.count{
            let bgView = UIView().then { obj in
                view.addSubview(obj)
                obj.frame = CGRect(x: CGFloat(i)*w, y: kScreenHeight - UIDevice.xp_tabBarFullHeight() - 30, width: w, height: 50)
            }
            
            let imageV = UIImageView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(10)
                    make.centerX.equalTo(bgView)
                    make.width.height.equalTo(20)
                }
            }
            
            let nameLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.textColor = blackTextColor70
                obj.font = kMediumFont(10)
                obj.text = titles[i]
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(10)
                    make.centerX.equalTo(bgView)
                }
            }
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                obj.tag = 1000+i
                obj.addTarget(self, action: #selector(featureClick(_ :)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
        }
        
        cofing()
        // Do any additional setup after loading the view.
    }
    
    func cofing(){
        let router = PunchCardRouter()
        
        presenter = PunchCardPresenter()
        presenter?.router = router
        
        let entity = PunchCardEntity()
        
        let interactor = PunchCardInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
    }
    
    @objc func bakcClick(_ button:UIButton){
        self.navigationController?.popViewController(animated: true)
    }

    
    @objc func realoadClick(_ button:UIButton){
        
    }
    
    @objc func featureClick(_ button:UIButton){
        if button.tag == 1000{
//            self.popup.dialog {
//                let v = PunchRemindDialog()
//                return v
//            }
            self.presenter?.router?.pushToAttendanceRule(from: self)
        }else if button.tag == 1001{
            self.presenter?.router?.pushToStatistics(from: self)
        }else{
            self.presenter?.router?.pushToVacationManage(from: self)
        }
    }
    
    @objc func recordsClick(_ button:UIButton){
        self.presenter?.router?.pushToRecords(from: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension PunchCardViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.router?.pushToChangePunchDate(from: self)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1{
            return 142
        }
        return 231
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClockInTableViewCell", for: indexPath) as! ClockInTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "NoClockInTableViewCell", for: indexPath) as! NoClockInTableViewCell
        return cell
    }
}

extension PunchCardViewController: PunchCardViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterReceiveData() {
        
    }
    
    func didPresenterReceiveError() {
        
    }
}
