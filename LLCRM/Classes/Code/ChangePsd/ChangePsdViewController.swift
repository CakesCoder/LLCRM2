//
//  ChangePsdViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class ChangePsdViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "修改密码"
        
        let headView = UIView().then { obj in
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 120)
        }
        
        let hintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "请先获取短信验证码。您的手机号：139****9989"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor39
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
            }
        }
        
        let psdView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(hintLabel.snp.bottom).offset(10)
                make.height.equalTo(66)
            }
        }
        
        let psdHintLabel = UILabel().then { obj in
            psdView.addSubview(obj)
            obj.text = "短信验证码"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(80)
            }
        }
        
        let psdTextFiled = UITextField().then { obj in
            psdView.addSubview(obj)
            obj.font = kMediumFont(12)
            obj.placeholder = "请输入验证码"
            obj.snp.makeConstraints { make in
                make.left.equalTo(psdHintLabel.snp.right).offset(20)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-50)
            }
        }
        
        let psdCodeButton = UIButton().then { obj in
            psdView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 3
            obj.setTitle("获取验证码", for: .normal)
            obj.titleLabel?.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(15)
                make.bottom.right.equalToSuperview().offset(-15)
                make.width.equalTo(97)
            }
        }
        
        _ = UIView().then({ obj in
            psdView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let lastButton = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setTitle("下一步", for: .normal)
            obj.titleLabel?.font = kMediumFont(15)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 2
            obj.snp.makeConstraints { make in
                make.top.equalTo(psdView.snp.bottom).offset(71)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
