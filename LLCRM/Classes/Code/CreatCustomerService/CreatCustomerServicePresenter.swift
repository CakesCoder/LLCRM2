//
//  CreatCustomerServicePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CreatCustomerServicePresenter: CreatCustomerServicePresenterProtocols {
    var view: CreatCustomerServiceViewProtocols?
    
    var router: CreatCustomerServiceRouterProtocols?
    
    var interactor: CreatCustomerServiceInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetCreatCustomerService(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestCreatCustomerService(by: params)
    }
    
    func didGetCreatCustomerServiceInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetCreatCustomerServicePresenterReceiveData(by: params)
    }
    
    func didGetCreatCustomerServiceInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetCreatCustomerServicePresenterReceiveError(error: error)
    }
}
