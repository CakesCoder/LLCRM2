//
//  CreatCustomerServiceEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CreatCustomerServiceEntity: CreatCustomerServiceEntityProtocols {
    var interactor: CreatCustomerServiceInteractorProtocols?
    
    func didCreatCustomerServiceReceiveData(by params: Any?) {
        interactor?.didEntityCreatCustomerServiceReceiveData(by: params)
    }
    
    func didCreatCustomerServiceReceiveError(error: MyError?) {
        interactor?.didEntityCreatCustomerServiceReceiveError(error: error)
    }
    
    func getCreatCustomerServiceRequest(by params: Any?) {
        LLNetProvider.request(.addComplaintProduct(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didCreatCustomerServiceReceiveData(by:jsonData)
            case .failure(_):
                self.didCreatCustomerServiceReceiveError(error: .requestError)
            }
        }
    }
    
}
