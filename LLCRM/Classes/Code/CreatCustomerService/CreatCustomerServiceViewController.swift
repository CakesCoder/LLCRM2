//
//  CreatCustomerServiceViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit
import YYText_swift
import PKHUD
import HandyJSON
import ZLPhotoBrowser
import Photos

class CreatCustomerServiceViewController: BaseViewController {
    var pictureContent:UIView?
    var addButton:UIButton?
    var inputTextView:YYTextView?
    var clientNameTextFiled:UITextField?
    
    var complaintNo:String?
    var clientName:String?
    var complaintTheme:String?
    var complaintTypeName:String?
    var clientComplaintNo:String?
    var handleName:String?
    var probDesc:String?
    var images:[Any]? = []
    var presenter: CreatCustomerServicePresenterProtocols?
    var selectedIndex:Int = 0
    var clientId:String? = ""
    
    var addClientSelectedPersonModel:AddClientSelectedPersonModel?
    var chargeTextFiled:UITextField?
    
    var addClientSelectedTypeModel:AddClientSelectedModel?
    var typeTextFiled:UITextField?
    
    var addClientSelectedNameModel:AddClientSelectedBussinessNameModel?
//    var clientPersonTextFiled:UITextField?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        navigationItem.title = "新建客诉"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(CreatCustomerServiceViewController.backClick))
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedVisitAddAction(_:)), name: Notification.Name(rawValue: "VisitAddNotificationKey"), object: nil)
        
        view.backgroundColor = bgColor
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        let baseInfos = [["*","客诉单号："],["*","客户名称："],["*","客诉主题："],["客诉类别："],["客户投诉单号："],["*","处理人："],["*","问题描述："]]
        let contentInfos = ["请输入或自动", "请选择", "请输入", "请选择", "请输入", "请选择", ""]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    if i == 8 || i == 6{
                        make.height.equalTo(140)
                    }else{
                        make.height.equalTo(45)
                    }
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                        
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(15)
                }
            }
            
            if i == 1 || i == 3 || i == 5{
                let tagImageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-15)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            if i == baseInfos.count - 1{
                let textView = UIView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = bgColor7
                    obj.layer.cornerRadius = 4
                    obj.layer.borderWidth = 0.5
                    obj.layer.borderColor = lineColor7.cgColor
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(5)
                        make.right.equalToSuperview().offset(-15)
                        make.top.equalToSuperview().offset(36)
                        make.bottom.equalToSuperview().offset(-15)
                    }
                }
                inputTextView = YYTextView().then { obj in
                    textView.addSubview(obj)
                    obj.placeholderText = "输入"
                    obj.placeholderFont = kRegularFont(14)
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(10)
                        make.right.equalToSuperview().offset(-10)
                        make.top.equalToSuperview().offset(10)
                        make.bottom.equalToSuperview().offset(-10)
                    }
                }
            }else{
                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = contentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    if i == 1{
                        obj.isEnabled = false
                        clientNameTextFiled = obj
                    }else if i == 3{
                        obj.isEnabled = false
                        typeTextFiled = obj
                    }else if i == 5{
                        obj.isEnabled = false
                        chargeTextFiled = obj
                    }
                    obj.delegate = self
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 1 || i == 3 || i == 5{
                    _ = UIButton().then { obj in
                        v.addSubview(obj)
                        obj.tag = 1000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.bottom.right.equalToSuperview().offset(-0)
                        }
                    }
                }
            }

            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let pictureView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(6*45+140)
                make.height.equalTo(182)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            pictureView.addSubview(obj)
            let hintAttribute = NSMutableAttributedString(string:"附件上传")
            hintAttribute.yy_color = blackTextColor3
            hintAttribute.yy_font = kMediumFont(12)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let w = (kScreenWidth-50)/3
        pictureContent = UIView().then { obj in
            pictureView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.right.equalToSuperview().offset(-25)
                make.top.equalTo(hintLabel.snp.bottom).offset(10)
                make.height.equalTo(w)
            }
        }
        
        addButton = UIButton().then({ obj in
            pictureContent?.addSubview(obj)
            obj.frame = CGRectMake(0, 0, w, w)
            obj.layer.borderColor = lineColor23.cgColor
            obj.layer.borderWidth = 0.5
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(.black, for: .normal)
            obj.titleLabel?.font = kMediumFont(25)
            obj.tag = 2000
            obj.addTarget(self, action: #selector(addPhotoClick), for: .touchUpInside)
        })
        
        
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-130)
            }
        })
        
        let addButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("添加客退", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.cornerRadius = 2
            obj.tag = 1000
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-22)
                make.bottom.equalToSuperview().offset(-40)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
        }
        
        
        let commitButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("提交", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.cornerRadius = 2
            obj.tag = 1001
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(addButton.snp.left).offset(-10)
                make.bottom.equalToSuperview().offset(-40)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
        }
        
        let okButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.setTitle("确定", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor15.cgColor
            obj.tag = 1002
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(commitButton.snp.left).offset(-10)
                make.bottom.equalToSuperview().offset(-40)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
        }
        
        let cancelButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.setTitle("取消", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor15.cgColor
            obj.tag = 1003
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(okButton.snp.left).offset(-10)
                make.bottom.equalToSuperview().offset(-40)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
        }
        
        cofing()
    
    }
    
    @objc func didSelectedVisitAddAction(_ notification:NSNotification){
        let dict = notification.userInfo as? [String:Any]
        let model = dict!["model"] as? MyClientModelData
        clientId = model?.clientId ?? ""
        clientNameTextFiled?.text = model?.clientName ?? ""
        clientName = model?.clientName ?? ""
    }
    
    func cofing(){
        let router = CreatCustomerServiceRouter()
        
        presenter = CreatCustomerServicePresenter()
        
        presenter?.router = router
        
        let entity =  CreatCustomerServiceEntity()
        
        let interactor =  CreatCustomerServiceInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
//        presenter?.presenterRequestGetCustomerService(by: ["current":currentIndex,"size":20])
    }

    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(_ button:UIButton){
        
        if button.tag == 1000{
            self.selectedIndex = 1
            presenter?.presenterRequestGetCreatCustomerService(by: ["complaintNo":complaintNo,"clientName":addClientSelectedNameModel?.clientName,"complaintTheme":complaintTheme,"complaintTypeName":complaintTypeName,"clientComplaintNo":clientComplaintNo,"handleName":addClientSelectedPersonModel?.name,"probDesc":inputTextView?.text, "clientId":addClientSelectedNameModel?.clientId])
        }else if button.tag == 1001{
            self.selectedIndex = 2
            presenter?.presenterRequestGetCreatCustomerService(by: ["complaintNo":complaintNo,"clientName":addClientSelectedNameModel?.clientName,"complaintTheme":complaintTheme,"complaintTypeName":complaintTypeName,"clientComplaintNo":clientComplaintNo,"handleName":addClientSelectedPersonModel?.name,"probDesc":inputTextView?.text,"clientId":addClientSelectedNameModel?.clientId])
        }else if button.tag == 1002{
            self.selectedIndex = 3
            presenter?.presenterRequestGetCreatCustomerService(by: ["complaintNo":complaintNo,"clientName":addClientSelectedNameModel?.clientName,"complaintTheme":complaintTheme,"complaintTypeName":complaintTypeName,"clientComplaintNo":clientComplaintNo,"handleName":addClientSelectedPersonModel?.name,"probDesc":inputTextView?.text,"clientId":addClientSelectedNameModel?.clientId])
        }else{
            self.selectedIndex = 4
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func addPhotoClick(){
        let ps = ZLPhotoPreviewSheet()
        ps.selectImageBlock = { [weak self] results, isOriginal in
            // your code
            if (self?.images!.count)! > 1{
                self?.addButton?.isHidden = true
            }else{
                let w = (kScreenWidth-50)/3
                for i in 0..<results.count{
                    if self?.images?.count ?? 0 >= 3{
                        self?.addButton?.isHidden = true
                        break
                    }
                    if i == 0{
                        for view in self?.pictureContent?.subviews as! [UIView]{
                            if view is UIButton{
                                continue
                            }
                            view.removeFromSuperview()
                        }
                    }
                    self?.images?.append(results[i].image)
                    self?.addButton?.isHidden = false
                    if i == 2{
                        self?.addButton?.isHidden = true
                    }else{
                        self?.addButton?.frame = CGRect(x: CGFloat(w)*CGFloat(i+1), y: 0, width: w, height: w)
                        self?.addButton?.isHidden = false
                    }
                    
                    _ = UIImageView().then { obj in
                        self?.pictureContent?.addSubview(obj)
                        obj.image = (self?.images![i] as! UIImage)
                        obj.frame = CGRect(x: CGFloat(w)*CGFloat(i), y: 0, width: w - 5, height: w)
                    }
                }
            }
        }
        ps.showPreview(animate: true, sender: self)
    }
    
    @objc func addPersonClick(_ btn:UIButton){
        if btn.tag == 1001{
//            presenter?.router?.pushToMyClient(from: self)
//            let vc = AddClientSelectedViewController()
//            vc.clientSelectConfig = 39
//            vc.addClientDataBlock = {[self] model in
//                addClientSelectedTypeModel = model
//                typeTextFiled?.text = addClientSelectedTypeModel?.attrName
//            }
//            self.navigationController?.pushViewController(vc, animated: true)
            
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 10
            vc.addClientSelectedBussinessNameBlock = {[self] model in
                addClientSelectedNameModel = model
                clientNameTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1003{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 38
            vc.addClientDataBlock = {[self] model in
                addClientSelectedTypeModel = model
                typeTextFiled?.text = addClientSelectedTypeModel?.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if btn.tag == 1005{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 4
            vc.addClientPersonBlock = {[self] model in
                addClientSelectedPersonModel = model
                chargeTextFiled?.text = addClientSelectedPersonModel?.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
//        presenter?.router?.pushToMyClient(from: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CreatCustomerServiceViewController: CreatCustomerServiceViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetCreatCustomerServicePresenterReceiveData(by params: Any?) {
        printTest(params)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CustomerServiceNotificationKey"), object: nil, userInfo: nil)
        if  self.selectedIndex == 3 || self.selectedIndex == 2{
            self.navigationController?.popViewController(animated: true)
        }
        if self.selectedIndex == 1{
            presenter?.router?.pushToAddCustomerService(from: self)
        }
    }
    
    func didGetCreatCustomerServicePresenterReceiveError(error: MyError?) {
        
    }
    
    
}

extension CreatCustomerServiceViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            complaintNo = textField.text
        }else if textField.tag == 1001{
            clientName = textField.text
        }else if textField.tag == 1002{
            complaintTheme = textField.text
        }else if textField.tag == 1003{
            complaintTypeName = textField.text
        }else if textField.tag == 1004{
            clientComplaintNo = textField.text
        }else if textField.tag == 1005{
            handleName = textField.text
        }else if textField.tag == 1006{
            probDesc = textField.text
        }
    }
}

