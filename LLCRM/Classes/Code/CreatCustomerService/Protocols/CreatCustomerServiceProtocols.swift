//
//  CreatCustomerServiceProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CreatCustomerServiceProtocols: NSObject {

}

protocol CreatCustomerServiceViewProtocols: AnyObject {
    var presenter: CreatCustomerServicePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetCreatCustomerServicePresenterReceiveData(by params: Any?)
    func didGetCreatCustomerServicePresenterReceiveError(error: MyError?)
}

protocol CreatCustomerServicePresenterProtocols: AnyObject{
    var view: CreatCustomerServiceViewProtocols? { get set }
    var router: CreatCustomerServiceRouterProtocols? { get set }
    var interactor: CreatCustomerServiceInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetCreatCustomerService(by params: Any?)
    func didGetCreatCustomerServiceInteractorReceiveData(by params: Any?)
    func didGetCreatCustomerServiceInteractorReceiveError(error: MyError?)
}

protocol CreatCustomerServiceInteractorProtocols: AnyObject {
    var presenter: CreatCustomerServicePresenterProtocols? { get set }
    var entity: CreatCustomerServiceEntityProtocols? { get set }
    
    func presenterRequestCreatCustomerService(by params: Any?)
    func didEntityCreatCustomerServiceReceiveData(by params: Any?)
    func didEntityCreatCustomerServiceReceiveError(error: MyError?)
}

protocol CreatCustomerServiceEntityProtocols: AnyObject {
    var interactor: CreatCustomerServiceInteractorProtocols? { get set }
    
    func didCreatCustomerServiceReceiveData(by params: Any?)
    func didCreatCustomerServiceReceiveError(error: MyError?)
    func getCreatCustomerServiceRequest(by params: Any?)
}

protocol CreatCustomerServiceRouterProtocols: AnyObject {
    func pushToAddCustomerService(from previousView: UIViewController)
    func pushToCreatCustomerService(from previousView: UIViewController)
    func pushToCustomerServiceDetail(from previousView: UIViewController, forModel model: Any)
    func pushToMyClient(from previousView: UIViewController)
}



