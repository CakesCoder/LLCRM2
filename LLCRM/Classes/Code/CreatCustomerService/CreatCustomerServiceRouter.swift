//
//  CreatCustomerServiceRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CreatCustomerServiceRouter: CreatCustomerServiceRouterProtocols {
    func pushToAddCustomerService(from previousView: UIViewController) {
        let nextView = AddCustomerServiceViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToCreatCustomerService(from previousView: UIViewController) {
        
    }
    
    func pushToCustomerServiceDetail(from previousView: UIViewController, forModel model: Any) {
        
    }
    
    func pushToMyClient(from previousView: UIViewController) {
        let nextView = MyClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    

}
