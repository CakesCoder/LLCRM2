//
//  CreatCustomerServiceInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CreatCustomerServiceInteractor: CreatCustomerServiceInteractorProtocols {
    var presenter: CreatCustomerServicePresenterProtocols?
    
    var entity: CreatCustomerServiceEntityProtocols?
    
    func presenterRequestCreatCustomerService(by params: Any?) {
        entity?.didCreatCustomerServiceReceiveData(by: params)
    }
    
    func didEntityCreatCustomerServiceReceiveData(by params: Any?) {
        presenter?.didGetCreatCustomerServiceInteractorReceiveData(by: params)
    }
    
    func didEntityCreatCustomerServiceReceiveError(error: MyError?) {
        presenter?.didGetCreatCustomerServiceInteractorReceiveError(error: error)
    }
    
    
}
