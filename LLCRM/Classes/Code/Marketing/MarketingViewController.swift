//
//  MarketingViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/22.
//

import UIKit

class MarketingViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController!.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "营销通"
  
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MarketingViewController.blackClick))
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let headTopView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(201)
            }
        }
        
        let w = kScreenWidth/4
        let h = 70
        let titles = ["递微信名片", "获客文章", "会议邀请", "朋友圈海报","活动推广", "推广简报"]
        for i in 0..<6{
            let x = i%4
            let y = i/4
            let v = UIView().then { obj in
                headTopView.addSubview(obj)
                obj.frame = CGRectMake(w*CGFloat(x), CGFloat(h)*CGFloat(y), w, CGFloat(h))
            }
            
            let imageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.image = UIImage(named: "yinxiao_\(i+1)")
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(20)
                    make.centerX.equalToSuperview()
                }
            }
            
            let nameLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor
                obj.textAlignment = .center
                obj.text = titles[i]
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(10)
                    make.centerX.equalToSuperview()
                }
            }
            
            let button = UIButton().then { obj in
                v.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
        }
        
        let lineView2 = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(headTopView.snp.bottom).offset(0)
                make.height.equalTo(10)
            }
        }
        
        let raderView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lineView2.snp.bottom).offset(0)
                make.height.equalTo(287)
            }
        }
        
        let blueRaderView = UIView().then { obj in
            raderView.addSubview(obj)
            obj.layer.cornerRadius = 7
            obj.backgroundColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(5)
                make.height.equalTo(13)
            }
        }
        
        let raderLabel = UILabel().then { obj in
            raderView.addSubview(obj)
            obj.text = "雷达"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor67
            obj.snp.makeConstraints { make in
                make.left.equalTo(blueRaderView.snp.right).offset(3)
                make.centerY.equalTo(blueRaderView)
            }
        }
        
        let raderPushImageV = UIImageView().then { obj in
            raderView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(blueRaderView)
//                make.width.height.equalTo(15)
            }
        }
        
        let lineView3 = UIView().then { obj in
            raderView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(raderLabel.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let noDataView = UIImageView().then { obj in
            raderView.addSubview(obj)
            obj.image = UIImage(named: "nodata_1")
            obj.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
        }
        
        let noDataLabel = UILabel().then { obj in
            raderView.addSubview(obj)
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor76
            obj.textAlignment = .center
            obj.numberOfLines = 0
            obj.text = "您还没有做过推广\n请发起一个推广获取更多线索"
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalTo(noDataView.snp.bottom).offset(20)
            }
        }
        
        let lineView4 = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(raderView.snp.bottom).offset(0)
                make.height.equalTo(10)
            }
        }
        
        let dynamicView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lineView4.snp.bottom).offset(0)
                make.height.equalTo(287)
            }
        }
        
        let bluedynamicView = UIView().then { obj in
            dynamicView.addSubview(obj)
            obj.layer.cornerRadius = 7
            obj.backgroundColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(5)
                make.height.equalTo(13)
            }
        }
        
        let dynamicLabel = UILabel().then { obj in
            dynamicView.addSubview(obj)
            obj.text = "动态"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor67
            obj.snp.makeConstraints { make in
                make.left.equalTo(bluedynamicView.snp.right).offset(3)
                make.centerY.equalTo(bluedynamicView)
            }
        }
        
        
        let lineView7 = UIView().then { obj in
            dynamicView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(dynamicLabel.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
