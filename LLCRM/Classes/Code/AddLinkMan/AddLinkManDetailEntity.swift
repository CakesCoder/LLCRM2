//
//  AddLinkManDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/26.
//

import UIKit

class AddLinkManDetailEntity: AddLinkManDetailEntityProtocols {
    var interactor: AddLinkManDetailInteractorProtocols?
    
    func didAddLinkManReceiveData(by params: Any?) {
        interactor?.didEntityAddLinkManReceiveData(by: params)
    }
    
    func didAddLinkManReceiveError(error: MyError?) {
        interactor?.didEntityAddLinkManReceiveError(error: error)
    }
    
    func postAddLinkManRequest(by params: Any?) {
        LLNetProvider.request(.addLineMan(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddLinkManReceiveData(by:jsonData)
            case .failure(_):
                self.didAddLinkManReceiveError(error: .requestError)
            }
        }
    }
    

}
