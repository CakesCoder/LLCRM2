//
//  AddLinkManViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/25.
//

import UIKit
import YYText_swift
import PKHUD

class AddLinkManViewController: BaseViewController, MyDateViewDelegate{
    var oneSelfName: String? = ""
    var clientName: String? = ""
    var jobStr: String? = ""
    var phoneStr: String? = ""
    var phoneStr2: String? = ""
    var emailStr: String? = ""
    var addressStr: String? = ""
    var dateStr: String? = ""
    var wxStr: String? = ""
    var remarkStr: String? = ""
    var presenter: AddLinkMantDetailPresenterProtocols?
    
//    var label: UIButton!
    var myDateView: MyDateView!
//    var myTimeView: MyTimeView!
//    var myDateTimeView: MyDateTimeView!
    
    var clientModel:MyClientModelData?
    var clientTextFiled:UITextField?
    var sexClientTextFiled:UITextField?
    var sexModel:AddClientSelectedModel?
    var dateClientTextFiled:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "添加联系人"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddLinkManViewController.blackClick))
        // Do any additional setup after loading the view.
        
        buildUI()
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func buildUI(){
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        
        let hints = [["*", "客户名称："],["*", "客户姓名："],["职务："],["*", "手机号："],["座机："],["*", "电子邮箱："],["性别："],["出生年月日："],["微信："]]
        
        let placeholders = ["选择客户", "请输入客户名字", "请输入职务", "请输入手机号", "请输入座机号", "请输入电子邮箱", "先生、女士", "请选择年月日", "请输入微信号"]
        
        for i in 0..<hints.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: i*45, width: Int(kScreenWidth), height: 45)
            }
            let hintContents:[String] = hints[i]
            var temp_hint = NSMutableAttributedString(string: "")
            _ = UILabel().then { obj in
                v.addSubview(obj)
                for j in 0..<hintContents.count{
                    let hint = NSMutableAttributedString(string:hintContents[j])
                    if hintContents.count == 1{
                        hint.yy_color = blackTextColor3
                        hint.yy_font = kMediumFont(12)
                        obj.attributedText = hint
                    }else{
                        if j == 0{
                            hint.yy_color = blackTextColor27
                            hint.yy_font = kMediumFont(12)
                            temp_hint = hint
                        }else{
                            hint.yy_color = blackTextColor3
                            hint.yy_font = kMediumFont(12)
                            temp_hint.append(hint)
                            obj.attributedText = temp_hint
                        }
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
                
//                if i == 7{
//                    _ = UIImageView().then { obj in
//                        v.addSubview(obj)
//                        obj.image = UIImage(named: "me_push")
//                        obj.snp.makeConstraints { make in
//                            make.right.equalToSuperview().offset(-05)
//                            make.centerY.equalToSuperview()
////                            make.width.height.equalTo(15)
//                        }
//                    }
//
//                    let textFiled = UITextField().then { obj in
//                        v.addSubview(obj)
//                        obj.placeholder = placeholders[i]
//                        obj.font = kMediumFont(12)
//                        obj.textAlignment = .right
//                        obj.delegate = self
//                        obj.tag = 1000+i
//                        obj.snp.makeConstraints { make in
//                            make.right.equalToSuperview().offset(-30)
//                            make.centerY.equalToSuperview()
//                            make.width.equalTo(120)
//                            make.height.equalTo(80)
//                        }
//                    }
//
//                    let button = UIButton().then { obj in
//                        v.addSubview(obj)
//                        obj.addTarget(self, action: #selector(dateClick(_ :)), for: .touchUpInside)
//                        obj.snp.makeConstraints { make in
//                            make.top.left.equalToSuperview().offset(0)
//                            make.bottom.right.equalToSuperview().offset(-0)
//                        }
//                    }
//                }else{
                    let textFiled = UITextField().then { obj in
                        v.addSubview(obj)
                        obj.placeholder = placeholders[i]
                        obj.font = kMediumFont(12)
                        obj.textAlignment = .right
                        obj.delegate = self
                        obj.tag = 1000+i
                        if i == 0{
                            obj.isEnabled = false
                            clientTextFiled = obj
                        }else if i == 6{
                            obj.isEnabled = false
                            sexClientTextFiled = obj
                        }else if i == 7{
                            obj.isEnabled = false
                            dateClientTextFiled = obj
                        }
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-30)
                            make.centerY.equalToSuperview()
                            make.width.equalTo(120)
                            make.height.equalTo(30)
                        }
                    }
                }
                
//            }
            
            if i == 0 || i == 5 || i == 6{
                _ = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            if i == 0 || i == 6 || i == 7{
//                v.backgroundColor = .blue
                let button = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.tag = 1000+i
//                    obj.backgroundColor = .yellow
                    obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let remarkView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(400)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(200)
            }
        }
        
        let remarkHintLabel = UILabel().then { obj in
            remarkView.addSubview(obj)
            obj.textColor = blackTextColor3
            obj.font = kMediumFont(12)
            obj.text = "备注"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
            }
        }
        
        let inputView = UIView().then { obj in
            remarkView.addSubview(obj)
            obj.backgroundColor = bgColor7
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor7.cgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(remarkHintLabel.snp.bottom).offset(5)
                make.height.equalTo(87)
            }
        }
        
        _ = YYTextView().then { obj in
            inputView.addSubview(obj)
            obj.placeholderText = "请输入"
            obj.placeholderTextColor = blackTextColor3
            obj.placeholderFont = kRegularFont(12)
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ = UIButton().then { obj in
            headView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = bluebgColor
            obj.setTitle("保存", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(inputView.snp.bottom).offset(20)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationBarHeight())
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
//        label = UIButton.init(frame: CGRect.init(x: 20, y: 100, width: 100, height: 50))
//        label.setTitleColor(UIColor.red, for: .normal)
//        label.setTitle("选择时间", for: .normal)
//        label.backgroundColor = UIColor.cyan
//        label.addTarget(self, action: #selector(chooseDate(button:)), for: .touchUpInside)
//        self.view.addSubview(label)
        
        // 日期选择
        myDateView = MyDateView.init()
        //myDateView.isShowDay = false
        myDateView.delegate = self
        //myDateView.setDefaultDate(year: 2000, month: 2, day: 4)
        
//        // 时间选择
//        myTimeView = MyTimeView.init()
//        myTimeView.delegate = self
//        //myTimeView.isShowSecond = false
//        //myTimeView.setDefaultTime(hour: 7, minute: 2, second: 56)
//
//        // 日期时间选择器
//        myDateTimeView = MyDateTimeView.init()
//        myDateTimeView.delegate = self
//        //myDateTimeView.isShowSecond = false
//        //myDateTimeView.setDefaultDateTime(year: 2000, month: 3, day: 12, hour: 11, minute: 4, second: 0)
        
        cofing()
    }
    
    @objc func addClick(_ button:UIButton){
        printTest(button.tag)
        if button.tag == 1000{
            let vc = MyClientViewController()
            vc.addClientPersonBlock = {[self] model in
                clientModel = model
                clientTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1006{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 17
            vc.addClientDataBlock = {[self] model in
                sexModel = model
                sexClientTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1007{
            myDateView.showView()
        }
        
    }
    
    @objc func saveClick(_ button:UIButton){
        if clientName?.count == 0{
            HUD.flash(.label("请输入客户名字"), delay: 2)
            return
        }
        
        if jobStr?.count == 0{
            HUD.flash(.label("请输入职务"), delay: 2)
            return
        }
        
        if emailStr?.count == 0{
            HUD.flash(.label("请输入邮箱"), delay: 2)
            return
        }
        
        if phoneStr?.count == 0{
            HUD.flash(.label("请输入手机号"), delay: 2)
            return
        }
        
        presenter?.presenterRequestPOSTAddLinkManDetail(by: ["position": jobStr ?? "" , "email": emailStr ?? "", "phone": phoneStr ?? "", "name": clientName ?? "", "clientId":clientModel?.clientId])
    }
    
    func cofing(){
        let router = AddLinkManDetailRouter()
        
        presenter = AddLinkMantDetailPresenter()
        
        presenter?.router = router
        
        let entity =  AddLinkManDetailEntity()
        
        let interactor =  AddLinkManDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
//    @objc func dateClick(_ button:UIButton){
//        myDateView.showView()
//    }
    
//    @objc func chooseDate(button:UIButton) {
////        myDateView.showView()
////        myTimeView.showView()
//        myDateTimeView.showView()
//    }

    func pickDateView(year: Int, month: Int, day: Int) {
        print("year \(year),month \(month),day \(day)")
        dateStr = "\(year)年\(month)月\(day)日"
        dateClientTextFiled?.text = dateStr
    }
    
//    func pickTimeView(hour: Int, minute: Int, second: Int) {
//        print("hour \(hour),minute \(minute),second \(second)")
//    }
//
//    func pickDateTimeView(year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int) {
//        print("year \(year),month \(month),day \(day),hour \(hour),minute \(minute),second \(second)")
//    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddLinkManViewController: AddLinkManDetailViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetAddLinkManDetailPresenterReceiveData(by params: Any?) {
        if params != nil{
            if let dict = params as? [String:Any]{
                let dataDic = dict["data"] as? [String:Any]
                if dict["code"] as! Int == 200{
                    HUD.flash(.label("提交成功"), delay: 2)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "LinkManNotificationKey"), object: nil, userInfo: nil)
                    self.navigationController?.popViewController(animated: true)
                }else{
                    HUD.flash(.label(dict["message"] as? String), delay: 2)
                }
                printTest(dict["message"])
            }else{
                HUD.flash(.label("data为nil"), delay: 2)
            }
        }
    }
    
    func didGetAddLinkManDetailPresenterReceiveError(error: MyError?) {
        
    }
    
}

extension AddLinkManViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
//            printTest("名称：\(String(describing: textField.text))")
//            oneSelfName = String(describing: textField.text)
        }else if textField.tag == 1001{
//            printTest("姓名：\(String(describing: textField.text))")
            clientName = textField.text!
        }else if textField.tag == 1002{
//            printTest("职务：\(String(describing: textField.text))")
            jobStr = textField.text!
        }else if textField.tag == 1003{
//            printTest("手机号：\(String(describing: textField.text))")
            phoneStr = textField.text!
        }else if textField.tag == 1004{
//            printTest("座机：：\(String(describing: textField.text))")
            phoneStr2 = String(describing: textField.text)
        }else if textField.tag == 1005{
//            printTest("*电子邮箱：\(String(describing: textField.text))")
            emailStr = textField.text!
        }else if textField.tag == 1006{
//            printTest("尊称: \(String(describing: textField.text))")
            addressStr = String(describing: textField.text)
        }else if textField.tag == 1007  {
//            printTest("出生年月日：\(String(describing: textField.text))")
            dateStr = String(describing: textField.text)
        }else if textField.tag == 1008 {
//            printTest("微信：\(String(describing: textField.text))")
            wxStr = String(describing: textField.text)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

extension AddLinkManViewController: YYTextViewDelegate{
    func textViewDidChange(_ textView: YYTextView) {
        
    }
}
