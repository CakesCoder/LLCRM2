//
//  AddLinkMantDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/26.
//

import UIKit

class AddLinkMantDetailPresenter: AddLinkMantDetailPresenterProtocols {
    var view: AddLinkManDetailViewProtocols?
    
    var router: AddLinkManDetailRouterProtocols?
    
    var interactor: AddLinkManDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestPOSTAddLinkManDetail(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddLinkMan(by: params)
    }
    
    func didGetAddLinkManDetailInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddLinkManDetailPresenterReceiveData(by: params)
    }
    
    func didGetAddLinkManDetailInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddLinkManDetailPresenterReceiveError(error: error)
    }
    

}
