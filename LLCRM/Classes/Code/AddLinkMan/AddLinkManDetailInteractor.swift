//
//  AddLinkManDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/26.
//

import UIKit

class AddLinkManDetailInteractor: AddLinkManDetailInteractorProtocols {
    
    var presenter: AddLinkMantDetailPresenterProtocols?
    
    var entity: AddLinkManDetailEntityProtocols?
    
    func presenterRequestAddLinkMan(by params: Any?) {
        entity?.postAddLinkManRequest(by: params)
    }
    
    func didEntityAddLinkManReceiveData(by params: Any?) {
        presenter?.didGetAddLinkManDetailInteractorReceiveData(by: params)
    }
    
    func didEntityAddLinkManReceiveError(error: MyError?) {
        presenter?.didGetAddLinkManDetailInteractorReceiveError(error: error)
    }
    

}
