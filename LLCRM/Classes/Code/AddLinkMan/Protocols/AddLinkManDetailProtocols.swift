//
//  AddLinkManDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/26.
//

import UIKit

protocol AddLinkManDetailViewProtocols: AnyObject {
    var presenter: AddLinkMantDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddLinkManDetailPresenterReceiveData(by params: Any?)
    func didGetAddLinkManDetailPresenterReceiveError(error: MyError?)
}

protocol AddLinkMantDetailPresenterProtocols: AnyObject{
    var view: AddLinkManDetailViewProtocols? { get set }
    var router: AddLinkManDetailRouterProtocols? { get set }
    var interactor: AddLinkManDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestPOSTAddLinkManDetail(by params: Any?)
    func didGetAddLinkManDetailInteractorReceiveData(by params: Any?)
    func didGetAddLinkManDetailInteractorReceiveError(error: MyError?)
}

protocol AddLinkManDetailInteractorProtocols: AnyObject {
    var presenter: AddLinkMantDetailPresenterProtocols? { get set }
    var entity: AddLinkManDetailEntityProtocols? { get set }
    
    func presenterRequestAddLinkMan(by params: Any?)
    func didEntityAddLinkManReceiveData(by params: Any?)
    func didEntityAddLinkManReceiveError(error: MyError?)
}

protocol AddLinkManDetailEntityProtocols: AnyObject {
    var interactor: AddLinkManDetailInteractorProtocols? { get set }
    
    func didAddLinkManReceiveData(by params: Any?)
    func didAddLinkManReceiveError(error: MyError?)
    func postAddLinkManRequest(by params: Any?)
}

protocol AddLinkManDetailRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
}

