//
//  CRMMessageProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

protocol CRMMessageViewProtocols: AnyObject {
    var presenter: CRMMessagePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetCRMMessagePresenterReceiveData(by params: Any?)
    func didGetCRMMessagePresenterReceiveError(error: MyError?)
}

protocol CRMMessagePresenterProtocols: AnyObject{
    var view: CRMMessageViewProtocols? { get set }
    var router: CRMMessageRouterProtocols? { get set }
    var interactor: CRMMessageInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetCRMMessage(by params: Any?)
    func didGetCRMMessageInteractorReceiveData(by params: Any?)
    func didGetCRMMessageInteractorReceiveError(error: MyError?)
}

protocol CRMMessageInteractorProtocols: AnyObject {
    var presenter: CRMMessagePresenterProtocols? { get set }
    var entity: CRMMessageEntityProtocols? { get set }
    
    func presenterRequestCRMMessage(by params: Any?)
    func didEntityCRMMessageReceiveData(by params: Any?)
    func didEntityCRMMessageReceiveError(error: MyError?)
}

protocol CRMMessageEntityProtocols: AnyObject {
    var interactor: CRMMessageInteractorProtocols? { get set }
    
    func didCRMMessageReceiveData(by params: Any?)
    func didCRMMessageReceiveError(error: MyError?)
    func getCRMMessageRequest(by params: Any?)
}

protocol CRMMessageRouterProtocols: AnyObject {
    func pushToWorkMessage(from previousView: UIViewController) 
    func pushToMyAddBusinessList(from previousView: UIViewController)
    func pushToBusinessListDetail(from previousView: UIViewController)
    func pushToCRMNotificationList(from previousView: UIViewController)
}

