//
//  CRMMessageCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class CRMMessageCell: UITableViewCell {
    var imageV:UIImageView?
    var titleLabel:UILabel?
    var subLabel:UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        imageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(45)
                
            }
        })
        
        titleLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor2
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV!.snp.right).offset(15)
                make.top.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
            }
        })
        
        subLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor45
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV!.snp.right).offset(15)
                make.bottom.equalToSuperview().offset(-13)
                make.right.equalToSuperview().offset(-15)
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
