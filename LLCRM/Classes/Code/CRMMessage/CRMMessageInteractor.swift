//
//  CRMMessageInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class CRMMessageInteractor: CRMMessageInteractorProtocols {
    var presenter: CRMMessagePresenterProtocols?
    
    var entity: CRMMessageEntityProtocols?
    
    func presenterRequestCRMMessage(by params: Any?) {
        
    }
    
    func didEntityCRMMessageReceiveData(by params: Any?) {
        
    }
    
    func didEntityCRMMessageReceiveError(error: MyError?) {
        
    }
    

}
