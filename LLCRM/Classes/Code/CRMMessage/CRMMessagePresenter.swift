//
//  CRMMessagePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class CRMMessagePresenter: CRMMessagePresenterProtocols {
    var view: CRMMessageViewProtocols?
    
    var router: CRMMessageRouterProtocols?
    
    var interactor: CRMMessageInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetCRMMessage(by params: Any?) {
        
    }
    
    func didGetCRMMessageInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetCRMMessageInteractorReceiveError(error: MyError?) {
        
    }
    

}
