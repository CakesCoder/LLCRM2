//
//  CRMMessageViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class CRMMessageViewController: BaseViewController {
    
    var presenter: CRMMessagePresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "CRM消息"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(CRMMessageViewController.backClick))

        // Do any additional setup after loading the view.
        
        view.backgroundColor = lineColor
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(CRMMessageCell.self, forCellReuseIdentifier: "CRMMessageCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(73)
            }
        })
        
        cofing()
    }

    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = CRMMessageRouter()
        
        presenter = CRMMessagePresenter()
        
        presenter?.router = router
        
        let entity = CRMMessageEntity()
        
        let interactor = CRMMessageInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CRMMessageViewController:CRMMessageViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetCRMMessagePresenterReceiveData(by params: Any?) {
        
    }
    
    func didGetCRMMessagePresenterReceiveError(error: MyError?) {
        
    }
    
    
}

extension CRMMessageViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CRMMessageCell", for: indexPath) as! CRMMessageCell
        if indexPath.row == 0{
            cell.imageV?.image =  UIImage(named: "crm_1")
            cell.titleLabel?.text = "CRM通知"
            cell.subLabel?.text = "公海客户提醒提醒……"
        }else if indexPath.row == 1{
            cell.imageV?.image =  UIImage(named: "crm_2")
            cell.titleLabel?.text = "工作流消息"
            cell.subLabel?.text = "工作流消息工作流消息工作流消息工作流消息"
        }else if indexPath.row == 2{
            cell.imageV?.image =  UIImage(named: "crm_3")
            cell.titleLabel?.text = "发起的审批流程"
            cell.subLabel?.text = "发起的审批流程发起的审批流程发起的审批流程"
        }else if indexPath.row == 3{
            cell.imageV?.image =  UIImage(named: "crm_4")
            cell.titleLabel?.text = "审批消息"
            cell.subLabel?.text = "您发起的审批：报销审批  已同意，请知晓"
        }else if indexPath.row == 4{
            cell.imageV?.image =  UIImage(named: "crm_5")
            cell.titleLabel?.text = "导入导出消息"
            cell.subLabel?.text = "导入导出消息导入导出消息导入导出消息导入导出"
        }else if indexPath.row == 5{
            cell.imageV?.image =  UIImage(named: "crm_6")
            cell.titleLabel?.text = "客诉消息处理"
            cell.subLabel?.text = "消息处理消息处理消息处理消息处理消息处理消"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.presenter?.router?.pushToCRMNotificationList(from: self)
        }else if indexPath.row == 1{
            self.presenter?.router?.pushToWorkMessage(from: self)
        }else{
            
        }
    }
}
