//
//  CRMMessageRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class CRMMessageRouter: CRMMessageRouterProtocols {
    func pushToWorkMessage(from previousView: UIViewController) {
        let nextView = WorkMessageViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToCRMNotificationList(from previousView: UIViewController) {
        let vc = CRMNotificationViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToMyAddBusinessList(from previousView: UIViewController) {
        
    }
    
    func pushToBusinessListDetail(from previousView: UIViewController) {
        
    }
}
