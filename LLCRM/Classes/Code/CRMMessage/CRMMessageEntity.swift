//
//  CRMMessageEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class CRMMessageEntity: CRMMessageEntityProtocols {
    var interactor: CRMMessageInteractorProtocols?
    
    func didCRMMessageReceiveData(by params: Any?) {
        
    }
    
    func didCRMMessageReceiveError(error: MyError?) {
        
    }
    
    func getCRMMessageRequest(by params: Any?) {
        
    }
    

}
