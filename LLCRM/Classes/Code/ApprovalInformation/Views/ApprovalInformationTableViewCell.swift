//
//  ApprovalInformationTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class ApprovalInformationTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
            
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "approval_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "报销审批（2022-05-16  16：30）"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor78
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let flowLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "流程关联对象：费用报销单"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(35)
            }
        }
        
        let dataLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "发起流程数据：202205160001"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(flowLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
            }
        }
        
        let statuLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "流程状态："
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(dataLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
            }
        }
        
        let statuCotentLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor15
            obj.textColor = blackTextColor36
            obj.text = "未通过"
            obj.textAlignment = .center
            obj.layer.borderColor = blackTextColor36.cgColor
            obj.layer.borderWidth = 0.5
            obj.layer.cornerRadius = 2
            obj.font = kRegularFont(10)
            obj.snp.makeConstraints { make in
                make.left.equalTo(statuLabel.snp.right).offset(0)
                make.centerY.equalTo(statuLabel)
                make.width.equalTo(35)
                make.height.equalTo(15)
            }
        }
        
        let personLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "当前处理人："
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(statuLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
            }
        }
        
        let personCotentLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.textColor = blackTextColor85
            obj.text = "未选择"
            obj.textAlignment = .center
            obj.layer.borderColor = blackTextColor85.cgColor
            obj.layer.borderWidth = 0.5
            obj.layer.cornerRadius = 2
            obj.font = kRegularFont(10)
            obj.snp.makeConstraints { make in
                make.left.equalTo(personLabel.snp.right).offset(0)
                make.centerY.equalTo(personLabel)
                make.width.equalTo(35)
                make.height.equalTo(15)
            }
        }
        
        let fuzeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "负责人：王小虎"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(personLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "流程开始时间：2022.05.16  09:30"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(fuzeLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        })
    }
}
