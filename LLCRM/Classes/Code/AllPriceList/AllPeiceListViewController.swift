//
//  AllPeiceListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh
//import JXPagingView

extension AllPeiceListViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension AllPeiceListViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class AllPeiceListViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
//    var currentIndex:Int = 0
    var dataList:[PriceDataModel] = []
    var tableView:UITableView?
    var priceListdidSelectedCallback:((PriceDataModel) -> ())?
    var presenter: PriceListPresenterProtocols?
    var currentIndex:Int? = 1
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(PriceListProceedTableViewCell.self, forCellReuseIdentifier: "PriceListProceedTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
//            currentIndex = 1
            presenter?.presenterRequestGetPriceList(by: ["complete":"2", "current":currentIndex, "size":20])
            
            tableView?.stopFooterRefreshing()
        }))
        
        cofing()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedVisitAddAction(_:)), name: Notification.Name(rawValue: "PriceListNotificationKey"), object: nil)
    }
    
    @objc func didSelectedVisitAddAction(_ notification:NSNotification){
        currentIndex = 1
        presenter?.presenterRequestGetPriceList(by: ["complete":"2", "current":currentIndex, "size":20])
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func cofing(){
        let router = PriceListRouter()
        
        presenter = PriceListPresenter()
        
        presenter?.router = router
        
        let entity =  PriceListEntity()
        
        let interactor =  PriceListInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
        presenter?.presenterRequestGetPriceList(by: ["complete":"2", "current":currentIndex, "size":20])
    }
}

extension AllPeiceListViewController:PriceListViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetPriceListPresenterReceiveData(by params: Any?) {
        if currentIndex == 1{
            self.dataList = []
        }
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                printTest(params)
//                HUD.flash(.label("添加成功"), delay: 2)
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CustomerServiceNotificationKey"), object: nil, userInfo: nil)
                if let dataModel = JSONDeserializer<PriceListModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    printTest(dataModel.dataList)
                    self.dataList.append(contentsOf: dataModel.dataList!)
                }
                currentIndex! += 1
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetPriceListPresenterReceiveError(error: MyError?) {
        
    }
    
    
}


extension AllPeiceListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PriceListProceedTableViewCell", for: indexPath) as! PriceListProceedTableViewCell
        cell.model = dataList[indexPath.row] as? PriceDataModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.priceListdidSelectedCallback?(dataList[indexPath.row])
    }
}

