//
//  MyCodeViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class MyCodeViewController: BaseViewController {
    
    var userName:String? = ""
    var userDepartmentName:String? = ""
    var userCompanyNameName:String? = ""
    var userEmail:String = ""
    var userPhone:String = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.whiteNavTheme()
    }
    
    @objc func blackClick(){
        navigationController?.popViewController(animated: true)
        
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MyCodeViewController.blackClick))
        
        buildUI()
        // Do any additional setup after loading the view.
    }
    
    func buildUI(){
        self.navigationItem.title = "我的名片"
        
        if let userInfosName = LLUserDefaultsManager.shared.readUserNameInfos(){
            userName = userInfosName
        }
        
        if let userInfosDepartmentName = LLUserDefaultsManager.shared.readUserDepartmentNameInfos(){
            userDepartmentName = userInfosDepartmentName
        }
        
        if let userInfosCompanyNameName = LLUserDefaultsManager.shared.readUserCompanyNameInfos(){
            userCompanyNameName = userInfosCompanyNameName
        }
        
        if let userInfosEmail = LLUserDefaultsManager.shared.readUserEmailInfos(){
            userEmail = userInfosEmail
        }
        
        if let userInfosPhone = LLUserDefaultsManager.shared.readUserPhoneInfos(){
            userPhone = userInfosPhone
        }
        
        let headView = UIView().then { obj in
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let topView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.layer.cornerRadius = 2
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(40)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(168)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            topView.addSubview(obj)
            obj.text = userName ?? "--"
            obj.textColor = blackTextColor
            obj.font = kAMediumFont(20)
            obj.snp.makeConstraints { make in
                make.top.left.equalToSuperview().offset(25)
            }
        }
        
        let subNameLabel = UILabel().then { obj in
            topView.addSubview(obj)
            obj.text = "/"+(userDepartmentName ?? "--")
            obj.font = kARegularFont(14)
            obj.textColor = blackTextColor37
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel.snp.right).offset(0)
                make.top.equalToSuperview().offset(33)
            }
        }
        
        let companyLabel = UILabel().then { obj in
            topView.addSubview(obj)
            
            obj.text = userCompanyNameName ?? "--"
            obj.textColor = blackTextColor38
            obj.font = kARegularFont(14)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalTo(nameLabel.snp.bottom).offset(22)
            }
        }
        
        let phoneLabel = UILabel().then { obj in
            topView.addSubview(obj)
            
            obj.text = userPhone ?? "--"
            obj.textColor = blackTextColor38
            obj.font = kARegularFont(14)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalTo(companyLabel.snp.bottom).offset(5)
            }
        }
        
        let emailLabel = UILabel().then { obj in
            topView.addSubview(obj)
            
            obj.text = userEmail ?? "--"
            obj.textColor = blackTextColor38
            obj.font = kARegularFont(14)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalTo(phoneLabel.snp.bottom).offset(5)
            }
        }
        
        let imageV = UIImageView().then { obj in
            topView.addSubview(obj)
//            obj.backgroundColor = .cyan
            obj.image = UIImage(named: "me_head")
            obj.layer.cornerRadius = 31.5
            obj.layer.borderWidth = 1
            obj.layer.borderColor = lineColor17.cgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-25)
                make.top.equalToSuperview().offset(25)
                make.width.height.equalTo(63)
            }
        }
        
        let bottomView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(topView.snp.bottom).offset(0)
                make.height.equalTo(304)
            }
        }
        
        let codeImageV = UIImageView().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(27)
                make.centerX.equalToSuperview()
                make.width.height.equalTo(180)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "扫一扫，加我为朋友"
            obj.font = kARegularFont(12)
            obj.textColor = blackTextColor39
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(codeImageV.snp.bottom).offset(10)
                make.centerX.equalToSuperview()
            }
        }
        
        
        let shareButton = UIButton().then { obj in
            headView.addSubview(obj)
            
            obj.backgroundColor = bluebgColor
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kARegularFont(15)
            obj.setTitle("分享名片", for: .normal)
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(bottomView.snp.bottom).offset(63)
//                make.centerX.equalToSuperview()
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
