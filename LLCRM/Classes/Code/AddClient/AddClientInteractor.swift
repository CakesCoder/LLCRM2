//
//  AddClientInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit

class AddClientInteractor: AddClientInteractorProtocols {
    var presenter: AddClientPresenterProtocols?
    
    var entity: AddClientEntityProtocols?
    
    func presenterRequestAddClient(by params: Any?) {
        entity?.getAddClientRequest(by: params)
    }
    
    func didEntityAddClientReceiveData(by params: Any?) {
        presenter?.didGetAddClientInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientReceiveError(error: MyError?) {
        presenter?.didGetAddClientInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddInfos(by params: Any?) {
        entity?.getInfostRequest(by: params)
    }

    func didAddInfosReceiveData(by params: Any?) {
        presenter?.didGetInfosInteractorReceiveData(by: params)
    }

    func didAddInfosReceiveError(error: MyError?) {
        presenter?.didGetInfosInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddInfosDetail(by params: Any?) {
        entity?.getInfosDetailRequest(by: params)
    }

    func didAddInfosDetailReceiveData(by params: Any?) {
        presenter?.didGetInfosDetailInteractorReceiveData(by: params)
    }

    func didAddInfosDetailReceiveError(error: MyError?) {
        presenter?.didGetInfosInteractorReceiveError(error: error)
    }
}
