//
//  AddClientViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit
import YYText_swift
import PKHUD
import HandyJSON

class AddClientViewController: BaseViewController, MyDateViewDelegate {
    
    var tableView: UITableView?
    var normalHeadView: UIView?
    var simpHeadView: UIView?
    var isSimp:Bool? = false
    var showSwitch:UISwitch?
    
    var presenter: AddClientPresenterProtocols?
    var companyDateView: MyDateView!
    
    var clientTextFiled:UITextField?
    var clientTextFiled2:UITextField?
    
    var statsTextFiled:UITextField?
    var clientTypeTextFiled:UITextField?
    var clientTypeTextFiled2:UITextField?
    
    var sourceTextFiled:UITextField?
    var sourceTextFiled2:UITextField?
    
    var mainLinkManTextFiled: UITextField?
    var mainLinkManTextFiled2: UITextField?
    
    var chargeTextFiled:UITextField?
    var chargeTextFiled2:UITextField?
    
    var linkManTextFiled:UITextField?
    
    var addressTextField: UITextField?
    var lastTimeTextField: UITextField?
    var companyTextFiled:UITextField?
    var departMentTextFiled:UITextField?
    
//    var mainlinkManModel: MyClientModelData?
//    var mainLinkManModel2: MyClientModelData?
    
//    var linkManModel: MyClientModelData?
//    var linkManModel2: MyClientModelData?
    
//    var clientModel: MyClientModelData?
//    var clientModel2: MyClientModelData?
    
//    var sourceModel: MyClientModelData?
//    var sourceModel2: MyClientModelData?
    
//    var chargPersoneModdel: MyClientModelData?
//    var chargPersoneModdel2: MyClientModelData?
    
    var attributeModel:AddClientSelectedModel?
    var sourceModel:AddClientSelectedModel?
    var clientTypeModel:AddClientSelectedModel?
    var addClientSelectedPersonModel:AddClientSelectedPersonModel?
    var addClientSelectedDepartMentModel:AddClientSelectedDepartMentModel?
    
    var requestID:String?
    
    //客户名称
    var clientName:String? = ""
    var clientName2:String? = ""
    //客户属性
    var clientAttribute:String? = ""
    //来源
    var sourceStr:String? = ""
    var sourceStr2:String? = ""
    //联系人
    var mainLinekMnaStr:String? = ""
    var mainLineMnaStr2:String? = ""
    //电话
    var phoneStr:String? = ""
    var phoneStr2:String? = ""
    //阶段
    var phaseStr:String? = ""
    //客户类型
    var clientTyoeStr:String? = ""
    var clientTyoeStr2:String? = ""
    //状态
    var phaseStatus:String? = ""
    var phaseStatus2:String? = ""
    //客户大概状态
    var clientSituation:String? = ""
    var clientSituation2:String? = ""
    //备注
    var clientRemark:String? = ""
    //负责人
    var chargePerson:String? = ""
    var chargePerson2:String? = ""
    //联系人
    var limkManStr:String? = ""
    //网址
    var urlStr:String? = ""
    var urlTextFiled:UITextField?
    //邮箱
    var emailStr:String? = ""
    var emailTextFiled:UITextField?
    //注册资本
    var registerMoney:String? = ""
    var registerMoneyTextFiled:UITextField?
    //企业成立时间
    var companyCreatTime:String? = ""
//    var companyCreatTimeTextFiled:UITextField?
    //行业
    var tradeStr:String? = ""
    var tradeTextFiled:UITextField?
    //法人代表
    var legalPerson:String? = ""
    var legalTextFiled:UITextField?
    //工商电话
    var businessPhone:String? = ""
    var businessTextFiled:UITextField?
    //经营范围
    var businessScope:String? = ""
    var businessScopeTextFiled:UITextField?
    //定位
    var positionStr:String? = ""
    //位置
    var locationStr:String? = ""
    //详细地址
    var detailLocationStr:String? = ""
    //部门
    var departmentStr:String? = ""
    //距离上次跟进时间
    var lastTime:String? = ""
    
    //提交后处理类型
    var commitDidStatus:Int? = 0
    
    //省
    var provinceStr:String? = ""
    //市
    var cityStr:String? = ""
    //区
    var areaStr:String? = ""
    
    //  选中哪个日期
    var dateSelectedIndex:Int? = 0
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "添加客户"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddClientViewController.blackClick))
        
        let showView = UIView().then { obj in
            obj.backgroundColor = .white
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.height.equalTo(50)
            }
        }
        
        showSwitch = UISwitch().then { obj in
            showView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.addTarget(self, action: #selector(switchChange), for: .valueChanged)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let showHintLabel = UILabel().then { obj in
            showView.addSubview(obj)
            obj.text = "仅显示必填字段"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor79
            obj.snp.makeConstraints { make in
                make.right.equalTo(showSwitch!.snp.left).offset(-10)
                make.centerY.equalToSuperview()
            }
        }
        
        simpHeadView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = bgColor
        }
        
        let simpbaseInfosView = UIView().then { obj in
            simpHeadView!.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(595)
            }
        }
        
        let simpbaseInfosHeadView = UIView().then { obj in
            simpbaseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(45)
            }
        }
        
        let simpheadTagView = UIView().then { obj in
            simpbaseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let simpheadLabel = UILabel().then { obj in
            simpbaseInfosHeadView.addSubview(obj)
            obj.text = "基本信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(simpheadTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let simpbaseInfos = [["*","客户名称："],["*","来源："],["*","主要联系人："],["*","电话："],["*","客户类型："],["*","跟进状态："],["*","客户大概情况："],["*","负责人："]]
        let simpbaseInfoPlaceholders = ["请输入", "请选择", "请输入", "请输入", "请选择", "潜在客户（只读）", "请输入", "请选择"]
        
        for i in 0..<simpbaseInfos.count{
            let v = UIView().then { obj in
                simpbaseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(55+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<simpbaseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:simpbaseInfos[i][j])
                    if simpbaseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
//            if i == 2{
//                mainLinkManTextFiled = UITextField().then { obj in
//                    v.addSubview(obj)
//                    obj.placeholder = simpbaseInfoPlaceholders[i]
//                    obj.font = kMediumFont(12)
//                    obj.tag = 2000+i
//                    obj.delegate = self
//                    obj.textColor = .black
//                    obj.isEnabled = false
//                    obj.snp.makeConstraints { make in
//                        make.right.equalToSuperview().offset(-30)
//                        make.centerY.equalToSuperview()
//                    }
//                }
//            }else{
                _ = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = simpbaseInfoPlaceholders[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 2000+i
                    obj.delegate = self
                    if i == 0{
                        clientTextFiled2 = obj
                    }else if i == 1{
                        sourceTextFiled2 = obj
                        obj.isEnabled = false
                    }else if i == 4{
                        obj.isEnabled = false
                        clientTypeTextFiled2 = obj
                    }else if i == 7{
                        obj.isEnabled = false
                        chargeTextFiled2 = obj
                    }else if i == 5{
                        obj.isEnabled = false
                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-30)
                        make.centerY.equalToSuperview()
                    }
                }
//            }
            
            
            if  i == 0 || i == 1 || i == 4 || i == 7{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.isUserInteractionEnabled = true
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 0{
                    _ = UIButton().then { obj in
                        imageV.addSubview(obj)
                        obj.tag = 2000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.bottom.right.equalToSuperview().offset(-0)
                        }
                    }
                }
            }
            
            if i == 1 || i == 4 || i == 7{
                _ = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.tag = 2000+i
                    obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.bottom.right.equalToSuperview().offset(-0)
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        normalHeadView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 1365)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(595)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "基本信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = [["*","客户名称："],["客户属性："],["*","来源："],["*","主要联系人："],["*","电话："],["跟进阶段："],["*","客户类型："],["*","跟进状态："],["*","客户大概情况："],["备注："],["*","负责人："],["联系人："]]
        let baseInfoPlaceholders = ["请输入", "请选择", "请选择", "请输入", "请输入", "潜在客户（只读）", "请选择", "请输入", "请输入", "请输入", "请输入", "请输入"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(55+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
//            if i == 3{
//                mainLinkManTextFiled2 = UITextField().then { obj in
//                    v.addSubview(obj)
//                    obj.placeholder = baseInfoPlaceholders[i]
//                    obj.font = kMediumFont(12)
//                    obj.tag = 1000+i
//                    obj.delegate = self
//                    obj.isEnabled = false
//                    obj.snp.makeConstraints { make in
//                        make.right.equalToSuperview().offset(-30)
//                        make.centerY.equalToSuperview()
//                    }
//                }
//            }else{
                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = baseInfoPlaceholders[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    obj.delegate = self
                    if i == 0{
                        clientTextFiled = obj
                    }else if i == 1{
                        obj.isEnabled = false
                        statsTextFiled = obj
//                        clientTextFiled = obj
                    }else if i == 2{
                        obj.isEnabled = false
                        sourceTextFiled = obj
                    }else if i == 6{
                        obj.isEnabled = false
                        clientTypeTextFiled = obj
                    }else if i == 5{
                        obj.isEnabled = false
                    }else if i == 10{
                        obj.isEnabled = false
                        chargeTextFiled = obj
                    }
//                    else if i == 11{
//                        obj.isEnabled = false
//                        linkManTextFiled = obj
//                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-30)
                        make.centerY.equalToSuperview()
                    }
                }
//            }
            
            if i < 3 || i == 10 || i == 6{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.isUserInteractionEnabled = true
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 0{
                    _ = UIButton().then { obj in
                        imageV.addSubview(obj)
                        obj.tag = 1000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.bottom.right.equalToSuperview().offset(-0)
                        }
                    }
                }
            }
            
            if i == 1 || i == 2 || i == 6 || i == 10{
                _ = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.tag = 1000+i
                    obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.bottom.right.equalToSuperview().offset(-0)
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let businessList = ["网址：", "邮件：", "注册资本：", "企业成立时间：", "所属行业：", "法定代表人：", "工商联系电话：", "经营范围："]
        
        let businessView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(baseInfosView.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(405)
            }
        }
        
        let businessHeadView = UIView().then { obj in
            businessView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let businessTagView = UIView().then { obj in
            businessHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let businessheadLabel = UILabel().then { obj in
            businessHeadView.addSubview(obj)
            obj.text = "工商信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(businessTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        for i in 0..<businessList.count{
            let v = UIView().then { obj in
                businessView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:businessList[i])
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let textFiled = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = baseInfoPlaceholders[i]
                obj.font = kMediumFont(12)
                obj.tag = 3000+i
                obj.textAlignment = .right
                obj.placeholder = "请输入"
                if i == 0{
                    urlTextFiled = obj
                }else if i == 1{
                    emailTextFiled = obj
                }else if i == 2{
                    registerMoneyTextFiled = obj
                }else if i == 3{
                    obj.isEnabled = false
                    companyTextFiled = obj
                }else if i == 4{
                    tradeTextFiled = obj
                }else if i == 5{
                    legalTextFiled = obj
                }else if i == 6{
                    businessTextFiled = obj
                }else if i == 7{
                    businessScopeTextFiled = obj
                }
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-30)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(200)
                    make.height.equalTo(30)
                }
            }
            
            if i == 3 {
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
                
                _ = UIButton().then({ obj in
                    v.addSubview(obj)
                    obj.tag = 1000
                    obj.addTarget(self, action: #selector(companyClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                    }
                })
            }
        }
        
        let localView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(businessView.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(180)
            }
        }
        
        let localHeadView = UIView().then { obj in
            localView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let localTagView = UIView().then { obj in
            localHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let localheadLabel = UILabel().then { obj in
            localHeadView.addSubview(obj)
            obj.text = "地区定位"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(businessTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let locals = ["定位：", "国家/省/市/区：", "详细地址："]
        
        for i in 0..<locals.count{
            let v = UIView().then { obj in
                localView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:locals[i])
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let textFiled = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = baseInfoPlaceholders[i]
                obj.font = kMediumFont(12)
                obj.tag = 4000+i
                obj.delegate = self
                obj.placeholder = "请输入"
                if i == 1{
                    addressTextField = obj
                    obj.isEnabled = false
                }
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-30)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i < 2 {
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            if i == 1{
                _ = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.addTarget(self, action: #selector(locationClick), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.top.left.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                    }
                }
            }
        }
        
        let systemView = UIView().then { obj in
            normalHeadView!.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(localView.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(180)
            }
        }
        
        let systemHeadView = UIView().then { obj in
            systemView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(45)
            }
        }
        
        let systemTagView = UIView().then { obj in
            systemHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let systemheadLabel = UILabel().then { obj in
            systemHeadView.addSubview(obj)
            obj.text = "系统信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(systemTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let systeminfos = ["归属部门：", "距离上次跟进时间："]
        
        for i in 0..<systeminfos.count{
            let v = UIView().then { obj in
                systemView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:systeminfos[i])
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let textFiled = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = baseInfoPlaceholders[i]
                obj.font = kMediumFont(12)
                obj.tag = 5000+i
                obj.placeholder = "请输入"
                if i == 1{
                    obj.isEnabled = false
                    lastTimeTextField = obj
                }else {
                    obj.isEnabled = false
                    departMentTextFiled = obj
                }
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-30)
                    make.centerY.equalToSuperview()
                }
            }
            
            let imageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.image = UIImage(named: "me_push")
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-10)
                    make.centerY.equalToSuperview()
//                    make.width.height.equalTo(15)
                }
            }
            
            _ = UIButton().then({ obj in
                v.addSubview(obj)
                if i == 1{
                    obj.tag = 1001
                    obj.addTarget(self, action: #selector(companyClick(_ :)), for: .touchUpInside)
                }else{
                    obj.tag = 1011
                    obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                }
                
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            })
        }
        
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = normalHeadView!
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(showView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        view.backgroundColor = bgColor
        
        let commitButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("保存", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kMediumFont(11)
            obj.tag = 1000
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(68)
                make.height.equalTo(30)
            }
        }
        
        let commitAndBuildButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("提交并新建", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kMediumFont(11)
            obj.tag = 1001
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(commitButton.snp.left).offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(88)
                make.height.equalTo(30)
            }
        }
        
        let saveAnddraftButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("保存草稿", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.backgroundColor = .white
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor14.cgColor
            obj.tag = 1002
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(commitAndBuildButton.snp.left).offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(88)
                make.height.equalTo(30)
            }
        }
        
        let saveAndbuildPersonButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.setTitle("提交并新建联系人", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.backgroundColor = .white
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor14.cgColor
            obj.tag = 1003
            obj.addTarget(self, action: #selector(saveClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(saveAnddraftButton.snp.left).offset(-10)
                make.top.equalTo(tableView!.snp.bottom).offset(20)
                make.width.equalTo(100)
                make.height.equalTo(30)
            }
        }
        
        // 日期选择
        companyDateView = MyDateView.init()
        companyDateView.delegate = self
        
        cofing()
    }
    
    func cofing(){
        let router = AddClientRouter()
        
        presenter = AddClientPresenter()
        
        presenter?.router = router
        
        let entity = AddClientEntity()
        
        let interactor = AddClientInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        presenter?.presenterRequestGetInfos(by: ["current":1,"size":50])
        
//        if let creditCode = LLUserDefaultsManager.shared.readUserCreditCodeInfos(){
////            printTest("asdasdasdasdasd")
//            printTest(creditCode)
//            presenter?.presenterRequestGetInfosDetail(by: ["creditCode":creditCode])
//        }
        
        
    }
    
    func pickDateView(year: Int, month: Int, day: Int) {
        if dateSelectedIndex == 1{
            companyTextFiled?.text = "\(year)年\(month)月\(day)日"
        }else{
            lastTimeTextField?.text = "\(year)年\(month)月\(day)日"
        }
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func companyClick(_ button:UIButton){
        if button.tag == 1000{
            dateSelectedIndex = 1
        }else{
            dateSelectedIndex = 2
        }
        companyDateView.showView()
    }
    
//    @objc func didSelectedClientLinkManAction(_ notification:NSNotification){
//        printTest(notification)
//        if let notifiModel = notification.userInfo{
////            linkManModel = notifiModel["ClietLineManModel"] as! LinkManModel
//
////            mainLinkManTextFiled?.text = linkManModel!.!
////            mainLinkManTextFiled2?.text = linkManModel!.name!
//        }
//    }
    
    @objc func switchChange(){
        showSwitch?.isSelected = !showSwitch!.isSelected
        tableView?.tableHeaderView = nil
        if showSwitch?.isSelected == true {
            tableView?.tableHeaderView = simpHeadView!
        }else{
            tableView?.tableHeaderView = normalHeadView!
        }
    }
    
    @objc func saveClick(_ button:UIButton){
        if button.tag == 1000{
            commitDidStatus = 1
            if showSwitch?.isSelected == true{
                presenter?.presenterRequestGetAddClient(by: ["clientName":clientName2, "source":sourceModel?.attrName, "phone":phoneStr,"clientClueContactSimpleParamList":[["name":mainLinekMnaStr]]])
            }else{
                presenter?.presenterRequestGetAddClient(by: ["address":provinceStr, "area":areaStr, "city":cityStr, "clientName":clientName, "companyProperty":clientAttribute, "industry":tradeStr, "source":sourceModel?.attrName, "website":urlStr, "phone":phoneStr, "clientClueContactSimpleParamList":[["name":mainLinekMnaStr]]])
            }
        }else if button.tag == 1001{
            commitDidStatus = 2
            if showSwitch?.isSelected == true{
//                presenter?.presenterRequestGetAddClient(by: ["clientName":clientModel2?.clientName, "source":sourceModel2?.clientName, "phone":phoneStr])
                presenter?.presenterRequestGetAddClient(by: ["clientName":clientName2, "source":sourceModel?.attrName, "phone":phoneStr,"clientClueContactSimpleParamList":[["name":mainLinekMnaStr]]])
            }else{
//                presenter?.presenterRequestGetAddClient(by: ["address":provinceStr, "area":areaStr, "city":cityStr, "clientName":clientModel?.clientName,"clientId":clientModel?.clientId, "companyProperty":clientAttribute, "industry":tradeStr, "source":sourceModel?.clientName, "website":urlStr, "phone":phoneStr, "clientClueContactSimpleParamList":[[ "name":mainlinkManModel?.clientName, "clientId":mainlinkManModel?.clientId]]])
                presenter?.presenterRequestGetAddClient(by: ["address":provinceStr, "area":areaStr, "city":cityStr, "clientName":clientName, "companyProperty":clientAttribute, "industry":tradeStr, "source":sourceModel?.attrName, "website":urlStr, "phone":phoneStr, "clientClueContactSimpleParamList":[["name":mainLinekMnaStr]]])
            }
        }else if button.tag == 1002{
            HUD.flash(.label("保存草稿成功"), delay: 2)
        }else{
            commitDidStatus = 4
            if showSwitch?.isSelected == true{
//                presenter?.presenterRequestGetAddClient(by: ["clientName":clientModel2?.clientName, "source":sourceModel2?.clientName, "phone":phoneStr])
                presenter?.presenterRequestGetAddClient(by: ["clientName":clientName2, "source":sourceModel?.attrName, "phone":phoneStr, "clientClueContactSimpleParamList":[["name":mainLinekMnaStr]]])
            }else{
//                presenter?.presenterRequestGetAddClient(by: ["address":provinceStr, "area":areaStr, "city":cityStr, "clientName":clientModel?.clientName,"clientId":clientModel?.clientId, "companyProperty":clientAttribute, "industry":tradeStr, "source":sourceModel?.clientName, "website":urlStr, "phone":phoneStr, "clientClueContactSimpleParamList":[[ "name":mainlinkManModel?.clientName, "clientId":mainlinkManModel?.clientId]]])
                presenter?.presenterRequestGetAddClient(by: ["address":provinceStr, "area":areaStr, "city":cityStr, "clientName":clientName, "companyProperty":clientAttribute, "industry":tradeStr, "source":sourceModel?.attrName, "website":urlStr, "phone":phoneStr, "clientClueContactSimpleParamList":[["name":mainLinekMnaStr]]])
            }
        }
    }
    
    @objc func locationClick(){
        let pickerMode: BWAddressPickerMode = .area
        let addressPickerView = BWAddressPickerView.init(autoSeleceted: true, mode: pickerMode, regions: ("浙江省", "湖州市", "长兴县")) { [self] (provinceModel, cityModel, areaModel) in
            print("block回调-----省：\(provinceModel.title)----市：\(cityModel?.title)----区县：\(areaModel?.title)")
            provinceStr = provinceModel.title
            cityStr = cityModel?.title
            areaStr = areaModel?.title
            
            addressTextField?.text = provinceModel.title! + (cityModel?.title)! + (areaModel?.title)!
        }
        addressPickerView.animationShow()
    }
    
    @objc func addPersonClick(_ button:UIButton){
        if showSwitch?.isSelected == false{
            if button.tag == 1000{
                let vc = AddClientSearchViewController()
                vc.addClientSearchBlock = {[self] model in
                    clientTextFiled?.text = model.Name ?? ""
                    clientTextFiled2?.text = model.Name ?? ""
                    
                    clientName = model.Name ?? ""
                    clientName2 = model.Name ?? ""
                    
                    presenter?.presenterRequestGetInfosDetail(by: ["creditCode":model.CreditCode])
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 1001{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 1
                vc.addClientDataBlock = {[self] model in
                    attributeModel = model
                    statsTextFiled?.text = model.attrName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 1002{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 2
                vc.addClientDataBlock = {[self] model in
                    sourceModel = model
                    sourceTextFiled?.text =  model.attrName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 1006{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 3
                vc.addClientDataBlock = {[self] model in
                    clientTypeModel = model
                    clientTypeTextFiled?.text =  model.attrName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 1010{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 4
                vc.addClientPersonBlock = {[self] model in
                    addClientSelectedPersonModel = model
                    chargeTextFiled?.text = addClientSelectedPersonModel?.name
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 1011{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 5
                vc.addClientSelectedDepartMentBlock = {[self] model in
                    addClientSelectedDepartMentModel = model
                    departMentTextFiled?.text = addClientSelectedDepartMentModel?.deptName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }else{
            
            if button.tag == 2000{
                let vc = AddClientSearchViewController()
                vc.addClientSearchBlock = {[self] model in
                    clientTextFiled?.text = model.Name ?? ""
                    clientTextFiled2?.text = model.Name ?? ""
                    
                    clientName = model.Name ?? ""
                    clientName2 = model.Name ?? ""
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 2001{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 2
                vc.addClientDataBlock = {[self] model in
                    sourceModel = model
                    sourceTextFiled2?.text =  model.attrName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 2004{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 3
                vc.addClientDataBlock = {[self] model in
                    clientTypeModel = model
                    clientTypeTextFiled2?.text =  model.attrName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }else if button.tag == 2007{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 4
                vc.addClientPersonBlock = {[self] model in
                    addClientSelectedPersonModel = model
                    chargeTextFiled2?.text = addClientSelectedPersonModel?.name
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
       
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddClientViewController: AddClientViewProtocols{
    
    func didGetInfosDetailPresenterReceiveData(by params: Any?){
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddSellLeadsModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    urlTextFiled?.text = dataModel.ImageUrl
                    urlStr = dataModel.ImageUrl
                    legalTextFiled?.text = dataModel.OperName
                    legalPerson = dataModel.OperName
                    companyTextFiled?.text = dataModel.StartDate
                    companyCreatTime = dataModel.StartDate
                    registerMoneyTextFiled?.text = dataModel.RegistCapi
                    registerMoney = dataModel.RegistCapi
                    businessScopeTextFiled?.text = dataModel.Scope
                    businessScope = dataModel.Scope
                }
//                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetInfosDetailPresenterReceiveError(error: MyError?){
        printTest("222222222222222")
    }
    
    func didGetInfosPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClinetNameModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    clientTextFiled?.text = dataModel.data?[0].Name ?? ""
                    clientTextFiled2?.text = dataModel.data?[0].Name ?? ""
                    
                    clientName = dataModel.data?[0].Name ?? ""
                    clientName2 = dataModel.data?[0].Name ?? ""
                }
//                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetInfosPresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetAddClientPresenterReceiveData(by params: Any?) {
        if params != nil{
            if let dict = params as? [String:Any]{
                let dataDic = dict["data"] as? [String:Any]
                if dict["code"] as! Int == 200{
                    HUD.flash(.label("提交成功"), delay: 2)
                    if commitDidStatus == 1{
                        self.navigationController?.popViewController(animated: true)
                    }else if commitDidStatus == 2{
                        presenter?.router?.pushToAddLinkMan(from: self)
                    }else if commitDidStatus == 4{
                        presenter?.router?.pushToAddClient(from: self)
                    }
                }else{
                    HUD.flash(.label(dict["message"] as? String), delay: 2)
                }
                printTest(dict["message"])
            }else{
                HUD.flash(.label("data为nil"), delay: 2)
            }
        }
        commitDidStatus = 0
    }
    
    func didGetAddClientPresenterReceiveError(error: MyError?) {
        commitDidStatus = 0
    }

}

extension AddClientViewController: BWAddressPickerViewDelegate{
    func addressPickerView(_ pickerView: BWAddressPickerView, didSelectWithProvinceModel provinceModel: BWAddressModel, cityModel: BWAddressCityModel?, areaModel: BWAddressAreaModel?) {
        print("代理回调-----省：\(provinceModel.title)----市：\(cityModel?.title)----区县：\(areaModel?.title)")
        provinceStr = provinceModel.title
        cityStr = cityModel?.title
        areaStr = areaModel?.title
        
        addressTextField?.text = provinceModel.title! + (cityModel?.title)! + (areaModel?.title)!
    }
}


extension AddClientViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            clientName = textField.text!
        }else if textField.tag == 1001{
            clientAttribute = textField.text!
        }else if textField.tag == 1002{
            sourceStr = textField.text!
        }else if textField.tag == 1003{
            mainLinekMnaStr = textField.text!
//            printTest(mainLinekMnaStr)
        }else if textField.tag == 1004{
            phoneStr = textField.text!
        }else if textField.tag == 1006{
            clientTyoeStr = textField.text!
        }else if textField.tag == 1007{
            phaseStatus = textField.text!
        }else if textField.tag == 1008{
            clientSituation = textField.text!
        }else if textField.tag == 1009{
            clientRemark = textField.text!
        }else if textField.tag == 1010{
            chargePerson = textField.text!
        }else if textField.tag == 1011{
            limkManStr = textField.text!
        }else if textField.tag == 2000{
            clientName2 = textField.text!
        }else if textField.tag == 2001{
            sourceStr2 = textField.text!
        }else if textField.tag == 2002{
            mainLineMnaStr2 = textField.text!
        }else if textField.tag == 2003{
            phoneStr2 = textField.text!
        }else if textField.tag == 2004{
            clientTyoeStr2 = textField.text!
        }else if textField.tag == 2005{
            phaseStatus2 = textField.text!
        }else if textField.tag == 2006{
            clientSituation2 = textField.text!
        }else if textField.tag == 2007{
            chargePerson2 = textField.text!
        }else if textField.tag == 3000{
            urlStr = textField.text!
        }else if textField.tag == 3001{
            emailStr = textField.text!
        }else if textField.tag == 3002{
            registerMoney = textField.text!
        }else if textField.tag == 3003{
            companyCreatTime = textField.text!
        }else if textField.tag == 3004{
            tradeStr = textField.text!
        }else if textField.tag == 3005{
            legalPerson = textField.text!
        }else if textField.tag == 3006{
            businessPhone = textField.text!
        }else if textField.tag == 3007{
            businessScope = textField.text
        }else if textField.tag == 4002{
            detailLocationStr = textField.text!
        }else if textField.tag == 5000{
            lastTime = textField.text!
        }
    }
}
