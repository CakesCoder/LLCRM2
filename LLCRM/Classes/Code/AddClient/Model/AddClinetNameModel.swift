//
//  AddClinetNameModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2024/2/29.
//

import UIKit
import HandyJSON

class AddClinetNameModel: BaseModel {
    var code:Int?
    var data:[AddClinetNameDataModel]?
    var message:String?
    
    required init() {}
}

class AddClinetNameDataModel: BaseModel{
    var CreditCode:String?
    var Dimension:String?
    var Name:String?
    var No:String?
    var OperName:String?
    var StartDate:String?
    var Status:String?
    var companyInfo:String?
    var enterpriseId:String?
    var keyNo:String?
    
    var isSelected:Bool?
    
    required init() {}
}
