//
//  AddClientPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit

class AddClientPresenter: AddClientPresenterProtocols {
    var view: AddClientViewProtocols?
    
    var router: AddClientRouterProtocols?
    
    var interactor: AddClientInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddClient(by params: Any?) {
        view?.showLoading()
        interactor?.entity?.getAddClientRequest(by: params)
    }
    
    func didGetAddClientInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddClientPresenterReceiveData(by: params)
    }
    
    func didGetAddClientInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetInfos(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddInfos(by: params)
    }

    func didGetInfosInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetInfosPresenterReceiveData(by: params)
    }

    func didGetInfosInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetInfosPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetInfosDetail(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddInfosDetail(by: params)
    }

    func didGetInfosDetailInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetInfosDetailPresenterReceiveData(by: params)
    }

    func didGetInfoDetailsInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetInfosDetailPresenterReceiveError(error: error)
    }

}
