//
//  AddClientEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit

class AddClientEntity: AddClientEntityProtocols {
    var interactor: AddClientInteractorProtocols?
    
    func didAddClientnReceiveData(by params: Any?) {
        interactor?.didEntityAddClientReceiveData(by: params)
    }
    
    func didAddClientReceiveError(error: MyError?) {
        interactor?.didEntityAddClientReceiveError(error: error)
    }
    
    func getAddClientRequest(by params: Any?) {
        LLNetProvider.request(.addClient(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientnReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientReceiveError(error: .requestError)
            }
        }
    }
    
    func didGetInfosReceiveData(by params: Any?) {
        interactor?.didAddInfosReceiveData(by: params)
    }

    func didGetInfosReceiveError(error: MyError?) {
        interactor?.didAddInfosReceiveError(error: error)
    }

    func getInfostRequest(by params: Any?) {
        LLNetProvider.request(.getAddClientInfos(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didGetInfosReceiveData(by:jsonData)
            case .failure(_):
                self.didGetInfosReceiveError(error: .requestError)
            }
        }
    }
    
    func didGetInfosDetailReceiveData(by params: Any?) {
        interactor?.didAddInfosDetailReceiveData(by: params)
    }

    func didGetInfosDetailReceiveError(error: MyError?) {
        interactor?.didAddInfosDetailReceiveError(error: error)
    }

    func getInfosDetailRequest(by params: Any?) {
        LLNetProvider.request(.getAddClinetInfosDetail(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didGetInfosDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didGetInfosDetailReceiveError(error: .requestError)
            }
        }
    }
}
