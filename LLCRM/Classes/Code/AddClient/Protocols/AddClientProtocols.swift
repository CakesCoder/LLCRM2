//
//  AddClientProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit

protocol AddClientViewProtocols: AnyObject {
    var presenter: AddClientPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddClientPresenterReceiveData(by params: Any?)
    func didGetAddClientPresenterReceiveError(error: MyError?)
    
    func didGetInfosPresenterReceiveData(by params: Any?)
    func didGetInfosPresenterReceiveError(error: MyError?)
    
    func didGetInfosDetailPresenterReceiveData(by params: Any?)
    func didGetInfosDetailPresenterReceiveError(error: MyError?)
}

protocol AddClientPresenterProtocols: AnyObject{
    var view: AddClientViewProtocols? { get set }
    var router: AddClientRouterProtocols? { get set }
    var interactor: AddClientInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddClient(by params: Any?)
    func didGetAddClientInteractorReceiveData(by params: Any?)
    func didGetAddClientInteractorReceiveError(error: MyError?)
    
    
    func presenterRequestGetInfos(by params: Any?)
    func didGetInfosInteractorReceiveData(by params: Any?)
    func didGetInfosInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetInfosDetail(by params: Any?)
    func didGetInfosDetailInteractorReceiveData(by params: Any?)
    func didGetInfoDetailsInteractorReceiveError(error: MyError?)
}

protocol AddClientInteractorProtocols: AnyObject {
    var presenter: AddClientPresenterProtocols? { get set }
    var entity: AddClientEntityProtocols? { get set }
    
    func presenterRequestAddClient(by params: Any?)
    func didEntityAddClientReceiveData(by params: Any?)
    func didEntityAddClientReceiveError(error: MyError?)
    
    func presenterRequestAddInfos(by params: Any?)
    func didAddInfosReceiveData(by params: Any?)
    func didAddInfosReceiveError(error: MyError?)
    
    func presenterRequestAddInfosDetail(by params: Any?)
    func didAddInfosDetailReceiveData(by params: Any?)
    func didAddInfosDetailReceiveError(error: MyError?)
    
}

protocol AddClientEntityProtocols: AnyObject {
    var interactor: AddClientInteractorProtocols? { get set }
    
    func didAddClientnReceiveData(by params: Any?)
    func didAddClientReceiveError(error: MyError?)
    func getAddClientRequest(by params: Any?)
    
    func didGetInfosReceiveData(by params: Any?)
    func didGetInfosReceiveError(error: MyError?)
    func getInfostRequest(by params: Any?)
    
    func didGetInfosDetailReceiveData(by params: Any?)
    func didGetInfosDetailReceiveError(error: MyError?)
    func getInfosDetailRequest(by params: Any?)
}

protocol AddClientRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
    func pushToAddClient(from previousView: UIViewController)
    func pushToClientPersonDetail(from previousView: UIViewController)
    func pushToAddLinkMan(from previousView: UIViewController)
}


