//
//  AddClientRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit

class AddClientRouter: AddClientRouterProtocols {
    func pushToAddClient(from previousView: UIViewController) {
        let nextView = AddClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToClientPersonDetail(from previousView: UIViewController) {
        let nextView = MyLineManListViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToAddLinkMan(from previousView: UIViewController) {
        let nextView = AddClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
}
