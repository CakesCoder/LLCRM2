//
//  PersonInfosInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class PersonInfosInteractor: PersonInfosInteractorProtocols {
    var presenter: PersonInfosPresenterProtocols?
    
    var entity: PersonInfosEntityProtocols?
    
    func interactorRetrieveData() {
            
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
    
    }
    
    func didEntityReceiveError() {
        
    }
}
