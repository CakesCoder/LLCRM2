//
//  PersonInfosRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class PersonInfosRouter: PersonInfosRouterProtocols {
    
    func pushToMyCodeVC(from previousView: UIViewController) {
        let vc = MyCodeViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToMySelectedTime(from previousView: UIViewController) {
        let vc = PersonSelectedTimeViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToHelp(from previousView: UIViewController) {
        let vc = HelpViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToCollect(from previousView: UIViewController){
        let vc = CollectViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
