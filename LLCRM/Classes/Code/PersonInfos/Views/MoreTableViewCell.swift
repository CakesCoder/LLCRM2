//
//  MoreTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class MoreTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let morelabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "更多"
            obj.textColor = blackTextColor37
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
        }
    }

}
