//
//  HeadTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class HeadTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let namelabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "我的头像"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.centerY.equalToSuperview()
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-23)
                make.centerY.equalToSuperview()
                make.width.equalTo(12)
                make.height.equalTo(15)
            }
        }
        
        let headImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .yellow
            obj.isUserInteractionEnabled = true
            obj.layer.cornerRadius = 31.5
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.right.equalTo(tagImageV.snp.left).offset(-20)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(63)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(8)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-8)
                make.height.equalTo(1)
            }
        })
    }

}
