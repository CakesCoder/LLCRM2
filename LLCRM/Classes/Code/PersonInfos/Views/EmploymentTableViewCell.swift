//
//  EmploymentTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class EmploymentTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let namelabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "就业日期："
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.centerY.equalToSuperview().offset(10)
            }
        }
        
        let contentLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "未填写"
            obj.textColor = blackTextColor22
            obj.font = kARegularFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(namelabel.snp.right).offset(80)
                make.centerY.equalToSuperview().offset(10)
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-23)
                make.centerY.equalToSuperview()
                make.width.equalTo(12)
                make.height.equalTo(15)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(8)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-8)
                make.height.equalTo(1)
            }
        })
    }
}
