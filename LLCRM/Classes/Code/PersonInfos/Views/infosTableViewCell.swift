//
//  infosTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class infosTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        let topLineView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let bgView = UIView().then { obj in
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(70)
            }
        }
        
        let imageV = UIImageView().then { obj in
            bgView.addSubview(obj)
            
            obj.backgroundColor = .red
            obj.layer.cornerRadius = 28
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(56)
            }
        }
        
        self.selectionStyle = .none
        
        let namelabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "我的名片"
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(17)
                make.centerY.equalTo(imageV)
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-23)
                make.centerY.equalToSuperview()
                make.width.equalTo(12)
                make.height.equalTo(15)
            }
        }
        
        let codeImageV = UIImageView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.width.equalTo(23)
                make.height.equalTo(23)
                make.centerY.equalToSuperview()
                make.right.equalTo(tagImageV.snp.left).offset(-23)
            }
        }
        
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(bgView.snp.bottom).offset(0)
                make.height.equalTo(41)
            }
            
        })
    }

}
