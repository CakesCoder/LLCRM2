//
//  PerosonInfosRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

protocol PersonInfosViewProtocols: AnyObject {
    var presenter: PersonInfosPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol PersonInfosPresenterProtocols: AnyObject{
    var view: PersonInfosViewProtocols? { get set }
    var router: PersonInfosRouterProtocols? { get set }
    var interactor: PersonInfosEntityProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol PersonInfosInteractorProtocols: AnyObject {
    var presenter: PersonInfosPresenterProtocols? { get set }
    var entity: PersonInfosEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol PersonInfosEntityProtocols: AnyObject {
    var interactor: SecheduleInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol PersonInfosRouterProtocols: AnyObject {
    func pushToMyCodeVC(from previousView: UIViewController)
    func pushToHelp(from previousView: UIViewController) 
    func pushToMySelectedTime(from previousView: UIViewController)
    func pushToCollect(from previousView: UIViewController)
}
