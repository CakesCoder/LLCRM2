//
//  PersonInfosViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class PersonInfosViewController: BaseViewController {
    var presenter: PersonInfosPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "个人信息"
        
        view.backgroundColor = .white
        
        presenter = PersonInfosPresenter()
        presenter?.router = PersonInfosRouter()
        
        let footView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 100)
            obj.backgroundColor = bgColor
        }
        
        let saveButton = UIButton().then { obj in
            footView.addSubview(obj)
            obj.setTitle("保存", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kAMediumFont(15)
            obj.layer.contents = 2
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(20)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            if #available(iOS 11.0, *) {
                obj.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
            } else {
                // Fallback on earlier versions
            }
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
            obj.backgroundColor = .white
            obj.register(HeadTableViewCell.self, forCellReuseIdentifier: "HeadTableViewCell")
            obj.register(NameTableViewCell.self, forCellReuseIdentifier: "NameTableViewCell")
            obj.register(CompanyTableViewCell.self, forCellReuseIdentifier: "CompanyTableViewCell")
            obj.register(PositionTableViewCell.self, forCellReuseIdentifier: "PositionTableViewCell")
            obj.register(PhoneTableViewCell.self, forCellReuseIdentifier: "PhoneTableViewCell")
            obj.register(WechatTableViewCell.self, forCellReuseIdentifier: "WechatTableViewCell")
            obj.register(EmailTableViewCell.self, forCellReuseIdentifier: "EmailTableViewCell")
            obj.register(BirthDayTableViewCell.self, forCellReuseIdentifier: "BirthDayTableViewCell")
            obj.register(MoreTableViewCell.self, forCellReuseIdentifier: "MoreTableViewCell")
            obj.register(infosTableViewCell.self, forCellReuseIdentifier: "infosTableViewCell")
            obj.register(DepartmentTableViewCell.self, forCellReuseIdentifier: "DepartmentTableViewCell")
            obj.register(NumTableViewCell.self, forCellReuseIdentifier: "NumTableViewCell")
            obj.register(DateTableViewCell.self, forCellReuseIdentifier: "DateTableViewCell")
            obj.register(EmploymentTableViewCell.self, forCellReuseIdentifier: "EmploymentTableViewCell")
            obj.register(DesTableViewCell.self, forCellReuseIdentifier: "DesTableViewCell")
            obj.tableFooterView = footView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PersonInfosViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            self.presenter?.router?.pushToMyCodeVC(from: self)
        }else if indexPath.row == 1{
            self.presenter?.router?.pushToMySelectedTime(from: self)
        }else if indexPath.row == 2{
            self.presenter?.router?.pushToHelp(from: self)
        }else if indexPath.row == 3{
            self.presenter?.router?.pushToCollect(from: self)
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HeadTableViewCell", for: indexPath) as! HeadTableViewCell
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NameTableViewCell", for: indexPath) as! NameTableViewCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "CompanyTableViewCell", for: indexPath) as! CompanyTableViewCell
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PositionTableViewCell", for: indexPath)
            return cell as! PositionTableViewCell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneTableViewCell", for: indexPath) as! PhoneTableViewCell
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTableViewCell", for: indexPath) as! EmailTableViewCell
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WechatTableViewCell", for: indexPath) as! WechatTableViewCell
            return cell
        }else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "BirthDayTableViewCell", for: indexPath) as! BirthDayTableViewCell
            return cell
        }else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreTableViewCell", for: indexPath) as! MoreTableViewCell
            return cell
        }else if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "infosTableViewCell", for: indexPath) as! infosTableViewCell
            return cell
        }else if indexPath.row == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DepartmentTableViewCell", for: indexPath) as! DepartmentTableViewCell
            return cell
        }else if indexPath.row == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "NumTableViewCell", for: indexPath) as! NumTableViewCell
            return cell
        }else if indexPath.row == 12{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DateTableViewCell", for: indexPath) as! DateTableViewCell
            return cell
        }else if indexPath.row == 13{
            let cell = tableView.dequeueReusableCell(withIdentifier: "EmploymentTableViewCell", for: indexPath) as! EmploymentTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "DesTableViewCell", for: indexPath) as! DesTableViewCell
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 94
        }else if indexPath.row == 9{
            return 121
        }else if indexPath.row == 10{
            return 69
        }
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
}
