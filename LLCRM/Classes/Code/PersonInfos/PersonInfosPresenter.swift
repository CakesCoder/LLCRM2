//
//  PersonInfosPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class PersonInfosPresenter: PersonInfosPresenterProtocols {
    var view: PersonInfosViewProtocols?
    
    var router: PersonInfosRouterProtocols?
    
    var interactor: PersonInfosEntityProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
