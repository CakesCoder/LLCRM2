//
//  EquipmentManagerViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit
//import JXPagingView

extension ApplyDetailViewController: UIScrollViewDelegate {
//    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.listViewDidScrollCallback?(scrollView)
//    }
}

extension EquipmentManagerViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class EquipmentManagerViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    lazy var noDataView:UIView = {
        let noDataV = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let noDataImageV = UIImageView().then { obj in
            noDataV.addSubview(obj)
            obj.image = UIImage(named: "detail_noData")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(120)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            noDataV.addSubview(obj)
            obj.text = "暂无数据"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor38
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(300)
                make.centerX.equalToSuperview()
            }
        }
        return noDataV
    }()
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "设备管理"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let addButton = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
//            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        
        let noDataV = noDataView.then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(45)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
