//
//  PriceDetailModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/7.
//

import UIKit
import HandyJSON
class PriceDetailModel: BaseModel {
    var clientId:Int?
    var clientName:String?
    var companyId:Int?
    var contactEmail:String?
    var contactId:String?
    var contactName:String?
    var contactPhone:String?
    var deliveryCondition:String?
    var id:Int?
    var payCondition:String?
    var price:String?
    var printOperator:String?
    var printTime:String?
    var productList:[productPriceModel]?
    var productModel:String?
    var productName:String?
    var productNo:String?
    var productSpecs:String?
    var quotationNo:String?
    var quotationTime:String?
    var quotationUnit:String?
    var remark:String?
    var saleEmail:String?
    var saleName:String?
    var salePhone:String?
    var stampPicture:String?
    var taxRate:String?
    var validTimeEnd:String?
    var validTimeStart:String?
    
    required init() {}
}

class productPriceModel: BaseModel{
    var companyId:Int?
    var deleted:Int?
    var id:Int?
    var maker:String?
    var makerCode:String?
    var price:String?
    var productModel:String?
    var productName:String?
    var productNo:String?
    var productSpecs:String?
    var quotationId:String?
    var quotationUnit:String?
    var taxRate:String?
    var unit:String?
    var validTimeEnd:String?
    var validTimeStart:String?
    
    required init() {}
}
