//
//  PriceDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class PriceDetailPresenter: PriceDetailPresenterProtocols {
    var view: PriceDetailViewProtocols?
    
    var router: PriceDetailRouterProtocols?
    
    var interactor: PriceDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetPriceDetail(by params: Any?) {
        interactor?.presenterRequestPriceDetail(by: params)
    }
    
    func didGetPriceDetailInteractorReceiveData(by params: Any?) {
        view?.didGetPriceDetailPresenterReceiveData(by: params)
    }
    
    func didGetPriceDetailInteractorReceiveError(error: MyError?) {
        view?.didGetPriceDetailPresenterReceiveError(error: error)
    }
}
