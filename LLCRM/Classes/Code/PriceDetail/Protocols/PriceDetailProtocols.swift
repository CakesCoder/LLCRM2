//
//  PriceDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class PriceDetailProtocols: NSObject {

}

protocol PriceDetailViewProtocols: AnyObject {
    var presenter: PriceDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetPriceDetailPresenterReceiveData(by params: Any?)
    func didGetPriceDetailPresenterReceiveError(error: MyError?)
}

protocol PriceDetailPresenterProtocols: AnyObject{
    var view: PriceDetailViewProtocols? { get set }
    var router: PriceDetailRouterProtocols? { get set }
    var interactor: PriceDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetPriceDetail(by params: Any?)
    func didGetPriceDetailInteractorReceiveData(by params: Any?)
    func didGetPriceDetailInteractorReceiveError(error: MyError?)
}

protocol PriceDetailInteractorProtocols: AnyObject {
    var presenter: PriceDetailPresenterProtocols? { get set }
    var entity: PriceDetailEntityProtocols? { get set }
    
    func presenterRequestPriceDetail(by params: Any?)
    func didEntityPriceDetailReceiveData(by params: Any?)
    func didEntityPriceDetailReceiveError(error: MyError?)
}

protocol PriceDetailEntityProtocols: AnyObject {
    var interactor: PriceDetailInteractorProtocols? { get set }
    
    func didPriceDetailReceiveData(by params: Any?)
    func didPriceDetailReceiveError(error: MyError?)
    func getPriceDetailRequest(by params: Any?)
}

protocol PriceDetailRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController)
}
