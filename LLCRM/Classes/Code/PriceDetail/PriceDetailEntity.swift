//
//  PriceDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class PriceDetailEntity: PriceDetailEntityProtocols {
    var interactor: PriceDetailInteractorProtocols?
    
    func didPriceDetailReceiveData(by params: Any?) {
        interactor?.didEntityPriceDetailReceiveData(by: params)
    }
    
    func didPriceDetailReceiveError(error: MyError?) {
        interactor?.didEntityPriceDetailReceiveError(error: error)
    }
    
    func getPriceDetailRequest(by params: Any?) {
        LLNetProvider.request(.priceDetail(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didPriceDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didPriceDetailReceiveError(error: .requestError)
            }
        }
    }
}
