//
//  PriceDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class PriceDetailInteractor: PriceDetailInteractorProtocols {
    var presenter: PriceDetailPresenterProtocols?
    
    var entity: PriceDetailEntityProtocols?
    
    func presenterRequestPriceDetail(by params: Any?) {
        entity?.getPriceDetailRequest(by: params)
    }
    
    func didEntityPriceDetailReceiveData(by params: Any?) {
        presenter?.didGetPriceDetailInteractorReceiveData(by: params)
    }
    
    func didEntityPriceDetailReceiveError(error: MyError?) {
        presenter?.didGetPriceDetailInteractorReceiveError(error: error)
    }
    

}
