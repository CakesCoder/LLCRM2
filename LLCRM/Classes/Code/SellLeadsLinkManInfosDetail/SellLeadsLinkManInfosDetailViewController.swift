//
//  SellLeadsLinkManInfosDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
//import JXPagingView

extension SellLeadsLinkManInfosDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SellLeadsLinkManInfosDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class SellLeadsLinkManInfosDetailViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var model:ClientDetailDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 1000)
        }
        
        
        let baseInfos = ["姓名：", "职位：", "手机号：", "电话号：", "邮箱："]
        
        let contentInfos = ["--", "--", "--", "--", "--"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                var contentStr = contentInfos[i]
//                if i == 0{
//                    contentStr = (model?.clientClueContactSimpleParamList?[0].name ?? "--") + "/" + (model?.clientClueContactSimpleParamList?[0].gender ?? "--")
//                }else if i == 1{
//                    contentStr = model?.clientClueContactSimpleParamList?[0].position ?? "--"
//                }else if i == 2{
//                    contentStr = model?.clientClueContactSimpleParamList?[0].phone ?? "--"
//                }else if i == 3{
//                    contentStr = "--"
//                }else if i == 4{
//                    contentStr = model?.clientClueContactSimpleParamList?[0].email ?? "--"
//                }
                let hintAttribute = NSMutableAttributedString(string:contentStr)
                if i == baseInfos.count - 1{
                    hintAttribute.yy_color = bluebgColor
                }else{
                    hintAttribute.yy_color = blackTextColor81
                }
//                if i == 0{
//                    nameLabel = obj
//                }else if i == 1{
//                    jobLabel = obj
//                }else if i == 2{
//                    phoneLabel = obj
//                }else if i == 4{
//                    emailLabel = obj
//                }
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i == 2{
                let imageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = .red
                    obj.snp.makeConstraints { make in
                        make.left.equalTo(contentLabel.snp.right).offset(15)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
