//
//  AddBusinessProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/10.
//

import UIKit

protocol AddBusinessViewProtocols: AnyObject {
    var presenter: AddBusinessPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddBusinessPresenterReceiveData(by params: Any?)
    func didGetAddBusinessPresenterReceiveError(error: MyError?)
    
    func didGetAddBusinessNumberPresenterReceiveData(by params: Any?)
    func didGetAddBusinessNumberPresenterReceiveError(error: MyError?)
}

protocol AddBusinessPresenterProtocols: AnyObject{
    var view: AddBusinessViewProtocols? { get set }
    var router: AddBusinessRouterProtocols? { get set }
    var interactor: AddBusinessInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddBusiness(by params: Any?)
    func didGetAddBusinessInteractorReceiveData(by params: Any?)
    func didGetAddBusinessInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddBusinessNumber(by params: Any?)
    func didGetAddBusinessNumberInteractorReceiveData(by params: Any?)
    func didGetAddBusinessNumberInteractorReceiveError(error: MyError?)
}

protocol AddBusinessInteractorProtocols: AnyObject {
    var presenter: AddBusinessPresenterProtocols? { get set }
    var entity: AddBusinessEntityProtocols? { get set }
    
    func presenterRequestAddBusiness(by params: Any?)
    func didEntityAddBusinessReceiveData(by params: Any?)
    func didEntityAddBusinessReceiveError(error: MyError?)
    
    func presenterRequestAddBusinessNumber(by params: Any?)
    func didEntityAddBusinessNumberReceiveData(by params: Any?)
    func didEntityAddBusinessNumberReceiveError(error: MyError?)
    
}

protocol AddBusinessEntityProtocols: AnyObject {
    var interactor: AddBusinessInteractorProtocols? { get set }
    
    func didAddBusinessReceiveData(by params: Any?)
    func didAddBusinessReceiveError(error: MyError?)
    func getAddBusinessRequest(by params: Any?)
    
    func didAddBussinessNumberReceiveData(by params: Any?)
    func didAddBussinessNumberReceiveError(error: MyError?)
    func getAddBussinessNumberRequest(by params: Any?)
}

protocol AddBusinessRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController)
}


