//
//  AddBusinessPageViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/25.
//

import UIKit
import DNSPageView

class AddBusinessPageViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationItem.title = "添加商机"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddBusinessPageViewController.blackClick))
        
        let style = PageStyle()
        style.isShowBottomLine = true
        style.titleViewBackgroundColor = UIColor.clear
        style.isTitleViewScrollEnabled = false
        
        // 适配 dark mode
        if #available(iOS 13.0, *) {
            style.titleSelectedColor = workTagColor
            style.titleColor = blackTextColor
            style.titleFont = UIFont.systemFont(ofSize: 12)
        } else {
            style.titleSelectedColor = workTagColor
            style.titleColor = blackTextColor
            style.titleFont = UIFont.systemFont(ofSize: 12)
        }
        style.bottomLineColor = workTagColor
        style.bottomLineWidth = 33

        // 设置标题内容
        let titles = ["商机信息", "联系人信息", "其它信息"]

        // 创建每一页对应的 controller
//        let childViewControllers: [UIViewController] = titles.map { _ -> UIViewController in
//            let controller = AddSampleViewController()
//            controller.z
//            addChild(controller)
//            return controller
//        }
        
        // 创建每一页对应的 controller
        for i in 0..<titles.count {
            let controller = AddBusinessViewController()
            controller.index = i
            addChild(controller)
        }

        let y = UIApplication.shared.statusBarFrame.height + (navigationController?.navigationBar.frame.height ?? 0)
        let size = UIScreen.main.bounds.size

        // 创建对应的 PageView，并设置它的 frame
        
        // titleView 和 contentView 会连在一起
        let pageView = PageView(frame: CGRect(x: 0, y: y, width: size.width, height: size.height - y), style: style, titles: titles, childViewControllers: children)
//        pageView.backgroundColor = .red
        view.addSubview(pageView)
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
