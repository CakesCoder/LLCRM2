//
//  AddBusinessInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/27.
//

import UIKit

class AddBusinessInteractor: AddBusinessInteractorProtocols {
    var presenter: AddBusinessPresenterProtocols?
    
    var entity: AddBusinessEntityProtocols?
    
    func presenterRequestAddBusiness(by params: Any?) {
        entity?.getAddBusinessRequest(by: params)
    }
    
    func didEntityAddBusinessReceiveData(by params: Any?) {
        presenter?.didGetAddBusinessInteractorReceiveData(by: params)
    }
    
    func didEntityAddBusinessReceiveError(error: MyError?) {
        presenter?.didGetAddBusinessInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddBusinessNumber(by params: Any?){
        entity?.getAddBussinessNumberRequest(by: params)
    }
    
    func didEntityAddBusinessNumberReceiveData(by params: Any?){
        presenter?.didGetAddBusinessNumberInteractorReceiveData(by: params)
    }
    
    func didEntityAddBusinessNumberReceiveError(error: MyError?){
        presenter?.didGetAddBusinessNumberInteractorReceiveError(error: error)
    }
}
