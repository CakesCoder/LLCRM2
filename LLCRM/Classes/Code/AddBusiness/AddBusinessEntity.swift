//
//  AddBusinessEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/27.
//

import UIKit

class AddBusinessEntity: AddBusinessEntityProtocols {
    var interactor: AddBusinessInteractorProtocols?
    
    func didAddBusinessReceiveData(by params: Any?) {
        interactor?.didEntityAddBusinessReceiveData(by: params)
    }
    
    func didAddBusinessReceiveError(error: MyError?) {
        interactor?.didEntityAddBusinessReceiveError(error: error)
    }
    
    func getAddBusinessRequest(by params: Any?) {
        LLNetProvider.request(.addBusiness(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddBusinessReceiveData(by:jsonData)
            case .failure(_):
                self.didAddBusinessReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddBussinessNumberReceiveData(by params: Any?) {
        interactor?.didEntityAddBusinessNumberReceiveData(by: params)
    }
    
    func didAddBussinessNumberReceiveError(error: MyError?) {
        interactor?.didEntityAddBusinessNumberReceiveError(error: error)
    }
    
    func getAddBussinessNumberRequest(by params: Any?) {
        LLNetProvider.request(.addClientBussinessNumber(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddBussinessNumberReceiveData(by:jsonData)
            case .failure(_):
                self.didAddBussinessNumberReceiveError(error: .requestError)
            }
        }
    }
}
