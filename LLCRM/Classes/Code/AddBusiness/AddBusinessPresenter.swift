//
//  AddBusinessPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/27.
//

import UIKit

class AddBusinessPresenter: AddBusinessPresenterProtocols {
    var view: AddBusinessViewProtocols?
    
    var router: AddBusinessRouterProtocols?
    
    var interactor: AddBusinessInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddBusiness(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddBusiness(by: params)
    }
    
    func didGetAddBusinessInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddBusinessPresenterReceiveData(by: params)
    }
    
    func didGetAddBusinessInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddBusinessPresenterReceiveError(error: error)
    }
    
    
    func presenterRequestGetAddBusinessNumber(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddBusinessNumber(by: params)
    }
    
    func didGetAddBusinessNumberInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddBusinessNumberPresenterReceiveData(by: params)
    }
    func didGetAddBusinessNumberInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddBusinessNumberPresenterReceiveError(error: error)
    }
}
