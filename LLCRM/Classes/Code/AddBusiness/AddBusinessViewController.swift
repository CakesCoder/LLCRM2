//
//  AddBusinessViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit
import PKHUD
import HandyJSON
//import SwiftPullToRefresh
//import JXSegmentedView
//import JXPagingView

class AddBusinessViewController: BaseViewController,MyDateViewDelegate {
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var okStudyButton: UIButton?
    var noStudyButton: UIButton?
    var presenter: AddBusinessPresenterProtocols?
    
//    let baseInfos = [["商机编号："],["*","客户名称："],["*","商机名称："],["商机来源："],["*","商机性质："],["*","商机金额： "],["产品类型："],["*","介入阶段："],["客户成交状态："],["*","跟随人："],["*","期望完成时间："],["可能性："]]
    var businessCode:String? = ""
    var clientName:String? = ""
    var businessName:String? = ""
    var businessSoure:String? = ""
    var bussinessCharacter:String? = ""
    var businessMoney:String? = ""
    var productType:String? = ""
    var businessPhase:String? = ""
    var clientStatus:String? = ""
    var followPerson:String? = ""
    var businessTime:String? = ""
    var possibilityStr:String? = ""
    
//    let personInfos = [["*","姓名："],["性别："],["职位："],["*","电话："],["邮箱：： "],["地址："]]
    var nameStr:String? = ""
    var sexStr:String? = ""
    var jobStr:String? = ""
    var emailStr:String? = ""
    var phoneStr:String? = ""
    var addressStr:String? = ""
    
    // 其他信息
    var otherMsgStr:String? = ""
    //对受
    var opponentStr:String? = ""
    //已公关人员
    var relationsPerson:String? = ""
    //购买台数
    var buyNum:String? = ""
    //是否公司考察
    var isCome:Int? = 1
    //跟踪情况
    var trackStr:String? = ""
    //备注
    var remarkStr:String? = ""
    
    var clientTextFiled:UITextField?
    var timeTextFiled:UITextField?
    var clientModel:MyClientModelData?
    var companyDateView: MyDateView!
    
    var attributeModel:AddClientSelectedModel?
    var probablyModel:AddClientSelectedModel?
    var addClientSelectedNatureModel:AddClientSelectedNatureModel?
    var addClientSelectedPhaseModel:AddClientSelectedPhaseModel?
    var addClientSelectedBussinessNameModel:AddClientSelectedBussinessNameModel?
    var addClientSelectedBussinessNatureModel:AddClientSelectedModel?
    var addClientSelectedBussinessStatusModel:AddClientSelectedModel?
    var addClientSelectedPersonModel:AddClientSelectedPersonModel?
    
    
    var sourceTextFiled:UITextField?
    var clientNameTextFiled:UITextField?
    var clientNatureTextFiled:UITextField?
    var clientPhaseTextFiled:UITextField?
    var clientProblelyTextFiled:UITextField?
    var clientBussinessNatureTextFiled:UITextField?
    var clientBussinessStatusTextFiled:UITextField?
    var personTextFiled:UITextField?
    
    var index:Int?
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        view.backgroundColor = bgColor
        cofing()

        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        if index == 0{
            presenter?.presenterRequestGetAddBusinessNumber(by: ["prefix":"CC"])
            
            let baseInfos = [["商机编号："],["*","客户名称："],["*","商机名称："],["商机来源："],["*","商机性质："],["*","商机金额： "],["产品类型："],["*","介入阶段："],["客户成交状态："],["*","跟随人："],["*","期望完成时间："],["可能性："]]
            let contentInfos = ["请输入", "请选择客户", "请输入", "请选择", "请选择商机性质", "请输入商机金额（元）", "请选择", "请选择", "请选择", "请选择", "请选择预计成交日期", "请选择"]
            
            let baseInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(baseInfos.count*45)
                }
            }
            
            for i in 0..<baseInfos.count{
                let v = UIView().then { obj in
                    baseInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }
                
                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<baseInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                        if baseInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 1 || i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 10 || i == 11{
                    let tagImageV = UIImageView().then { obj in
                        v.addSubview(obj)
                        obj.image = UIImage(named: "me_push")
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-15)
                            make.centerY.equalToSuperview()
                        }
                    }
                }

                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = contentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    obj.delegate = self
                    obj.textAlignment = .right
                    if i == 0 {
                        obj.isEnabled  = false
                        clientTextFiled = obj
                    }else if i == 1{
                        obj.isEnabled  = false
                        clientNameTextFiled = obj
                    }else if i == 3{
                        obj.isEnabled  = false
                        sourceTextFiled = obj
                    }else if i == 4{
                        obj.isEnabled  = false
                        clientBussinessNatureTextFiled = obj
                    }else if i == 6{
                        obj.isEnabled  = false
                        clientNatureTextFiled = obj
                    }else if i == 7{
                        obj.isEnabled = false
                        clientPhaseTextFiled = obj
                    }else if i == 8{
                        obj.isEnabled = false
                        clientBussinessStatusTextFiled = obj
                    }else if i == 9{
                        obj.isEnabled = false
                        personTextFiled = obj
                    }else if i == 10{
                        obj.isEnabled  = false
                        timeTextFiled = obj
                    }else if i == 11{
                        obj.isEnabled = false
                        clientProblelyTextFiled = obj
                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 10 || i == 1 || i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 11{
                    let button = UIButton().then { obj in
                        v.addSubview(obj)
                        obj.tag = 1000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.right.bottom.equalToSuperview().offset(0)
                        }
                    }
                }

                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
            
        }else if index == 1{
            let personInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(7*45)
                }
            }
            
            let personInfos = [["*","姓名："],["性别："],["职位："],["*","电话："],["邮箱："],["地址："]]
            let personcontentInfos = ["请输入", "请选择", "请输入", "请输入", "请输入", "请输入"]
            
            for i in 0..<personInfos.count{
                let v = UIView().then { obj in
                    personInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }
                
                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<personInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:personInfos[i][j])
                        if personInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }

                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = personcontentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 2000+i
                    obj.delegate = self
                    obj.textAlignment = .right
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }

                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
        }else{
            let otherInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(600)
                }
            }
            
//            let otherHeadView = UIView().then { obj in
//                otherInfosView.addSubview(obj)
//                obj.backgroundColor = .white
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(0)
//                    make.right.equalToSuperview().offset(-0)
//                    make.top.equalToSuperview().offset(0)
//                    make.height.equalTo(45)
//                }
//            }
//
//            let otherheadTagView = UIView().then { obj in
//                otherHeadView.addSubview(obj)
//                obj.backgroundColor = workTagColor
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.centerY.equalToSuperview()
//                    make.width.equalTo(3)
//                    make.height.equalTo(11)
//                }
//            }
//
//            let otherheadLabel = UILabel().then { obj in
//                otherHeadView.addSubview(obj)
//                obj.text = "其它信息"
//                obj.textColor = blackTextColor
//                obj.font = kMediumFont(15)
//                obj.snp.makeConstraints { make in
//                    make.left.equalTo(otherheadTagView.snp.right).offset(5)
//                    make.centerY.equalToSuperview()
//                }
//            }
            
//            let otherInfosv = UIView().then { obj in
//                otherInfosView.addSubview(obj)
//                obj.backgroundColor = .white
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(0)
//                    make.top.equalToSuperview().offset(0)
//                    make.right.equalToSuperview().offset(-0)
//                    make.height.equalTo(45)
//                }
//            }
//
//            let otherHintLabel = UILabel().then { obj in
//                otherInfosv.addSubview(obj)
//                var temp_hint = NSMutableAttributedString(string: "")
//                let hintAttribute = NSMutableAttributedString(string:"其它信息：")
//                hintAttribute.yy_color = blackTextColor3
//                hintAttribute.yy_font = kMediumFont(12)
//                obj.attributedText = hintAttribute
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.centerY.equalToSuperview()
//                }
//            }
//
//            let otherInfostextFiled = UITextField().then { obj in
//                otherInfosv.addSubview(obj)
//                obj.placeholder = "请输入"
//                obj.font = kMediumFont(12)
//                obj.tag = 3000
//                obj.delegate = self
//                obj.textAlignment = .right
//                obj.snp.makeConstraints { make in
//                    make.right.equalToSuperview().offset(-45)
//                    make.centerY.equalToSuperview()
//                }
//            }
//
//            _ = UIView().then({ obj in
//                otherInfosv.addSubview(obj)
//                obj.backgroundColor = lineColor5
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.right.equalToSuperview().offset(-15)
//                    make.bottom.equalToSuperview().offset(-0)
//                    make.height.equalTo(0.5)
//                }
//            })
            
            let otherMainInfosv = UIView().then { obj in
                otherInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
//                    make.top.equalTo(otherInfosv.snp.bottom).offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(95)
                }
            }
            
            let otherMainHintLabel = UILabel().then { obj in
                otherMainInfosv.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:"本案主要竞争对手：")
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(15)
                }
            }
            
            let otherMainInfostextV = UIView().then { obj in
                otherMainInfosv.addSubview(obj)
                obj.layer.cornerRadius = 4
                obj.layer.borderColor = lineColor7.cgColor
                obj.layer.borderWidth = 0.5
                obj.snp.makeConstraints { make in
                    make.top.equalTo(otherMainHintLabel.snp.bottom).offset(5)
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(56)
                }
            }
            
            let otherMainInfostextFiled = UITextField().then { obj in
                otherMainInfostextV.addSubview(obj)
                obj.placeholder = "请输入"
                obj.font = kMediumFont(12)
                obj.tag = 3001
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(15)
                    make.right.bottom.equalToSuperview().offset(-15)
                }
            }

            _ = UIView().then({ obj in
                otherMainInfosv.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let otherStudyInfosv = UIView().then { obj in
                otherInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalTo(otherMainInfosv.snp.bottom).offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let otherStudyHintLabel = UILabel().then { obj in
                otherStudyInfosv.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:"已公关人员：")
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }

            let otherStudyInfostextFiled = UITextField().then { obj in
                otherStudyInfosv.addSubview(obj)
                obj.placeholder = "请输入"
                obj.font = kMediumFont(12)
                obj.tag = 3002
                obj.delegate = self
                obj.textAlignment = .right
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-45)
                    make.centerY.equalToSuperview()
                }
            }

            _ = UIView().then({ obj in
                otherStudyInfosv.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let otherBuyInfosv = UIView().then { obj in
                otherInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalTo(otherStudyInfosv.snp.bottom).offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(95)
                }
            }
            
            let otherBuyHintLabel = UILabel().then { obj in
                otherBuyInfosv.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:"已合作竞争对手购买台式：")
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(15)
                }
            }
            
            let otherBuyInfostextV = UIView().then { obj in
                otherBuyInfosv.addSubview(obj)
                obj.layer.cornerRadius = 4
                obj.layer.borderColor = lineColor7.cgColor
                obj.layer.borderWidth = 0.5
                obj.snp.makeConstraints { make in
                    make.top.equalTo(otherBuyHintLabel.snp.bottom).offset(5)
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(56)
                }
            }
            
            let otherBuyInfostextFiled = UITextField().then { obj in
                otherBuyInfostextV.addSubview(obj)
                obj.placeholder = "请输入"
                obj.font = kMediumFont(12)
                obj.tag = 3003
                obj.delegate = self
    //            obj.textAlignment = .right
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(15)
                    make.right.bottom.equalToSuperview().offset(-15)
                }
            }

            _ = UIView().then({ obj in
                otherBuyInfosv.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let otherisStudyInfosv = UIView().then { obj in
                otherInfosView.addSubview(obj)
    //            otherInfosView.backgroundColor = .green
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalTo(otherBuyInfosv.snp.bottom).offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let otherisStudyHintLabel = UILabel().then { obj in
                otherisStudyInfosv.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:"是否来公司考察过：")
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            okStudyButton = UIButton().then({ obj in
                otherisStudyInfosv.addSubview(obj)
                obj.layer.cornerRadius = 7.5
                obj.backgroundColor = bgColor17
                obj.addTarget(self, action: #selector(okStudyClick), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(otherisStudyHintLabel.snp.right).offset(0)
                    make.centerY.equalToSuperview()
                    make.width.height.equalTo(15)
                }
            })
            
            let okStudyLabel = UILabel().then({ obj in
                otherisStudyInfosv.addSubview(obj)
                obj.text = "是"
                obj.font = kMediumFont(14)
                obj.textColor = blackTextColor87
                obj.snp.makeConstraints { make in
                    make.left.equalTo((okStudyButton?.snp.right)!).offset(5)
                    make.centerY.equalToSuperview()
                }
            })
            
            noStudyButton = UIButton().then({ obj in
                otherisStudyInfosv.addSubview(obj)
                obj.layer.cornerRadius = 7.5
                obj.backgroundColor = .white
                obj.layer.borderWidth = 0.5
                obj.layer.borderColor = lineColor28.cgColor
                obj.addTarget(self, action: #selector(noStudyClick), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(okStudyLabel.snp.right).offset(30)
                    make.centerY.equalToSuperview()
                    make.width.height.equalTo(15)
                }
            })
            
            let noStudyLabel = UILabel().then({ obj in
                otherisStudyInfosv.addSubview(obj)
                obj.text = "否"
                obj.font = kMediumFont(14)
                obj.textColor = blackTextColor87
                obj.snp.makeConstraints { make in
                    make.left.equalTo((noStudyButton?.snp.right)!).offset(5)
                    make.centerY.equalToSuperview()
                }
            })

           

            _ = UIView().then({ obj in
                otherisStudyInfosv.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let otherConditionInfosv = UIView().then { obj in
                otherInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalTo(otherisStudyInfosv.snp.bottom).offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(95)
                }
            }
            
            let otherConditionHintLabel = UILabel().then { obj in
                otherConditionInfosv.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:"跟踪情况")
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(15)
                }
            }
            
            let otherConditionInfostextV = UIView().then { obj in
                otherConditionInfosv.addSubview(obj)
                obj.layer.cornerRadius = 4
                obj.layer.borderColor = lineColor7.cgColor
                obj.layer.borderWidth = 0.5
                obj.snp.makeConstraints { make in
                    make.top.equalTo(otherConditionHintLabel.snp.bottom).offset(5)
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(56)
                }
            }
            
            let otherConditionInfostextFiled = UITextField().then { obj in
                otherConditionInfostextV.addSubview(obj)
                obj.placeholder = "请输入"
                obj.font = kMediumFont(12)
                obj.tag = 3004
                obj.delegate = self
    //            obj.textAlignment = .right
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(15)
                    make.right.bottom.equalToSuperview().offset(-15)
                }
            }

            _ = UIView().then({ obj in
                otherConditionInfosv.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let otherRemarkInfosv = UIView().then { obj in
                otherInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalTo(otherConditionInfosv.snp.bottom).offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(95)
                }
            }
            
            let otherRemarkHintLabel = UILabel().then { obj in
                otherRemarkInfosv.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:"备注")
                hintAttribute.yy_color = blackTextColor3
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(15)
                }
            }
            
            let otheRemarkInfostextV = UIView().then { obj in
                otherRemarkInfosv.addSubview(obj)
                obj.layer.cornerRadius = 4
                obj.layer.borderColor = lineColor7.cgColor
                obj.layer.borderWidth = 0.5
                obj.snp.makeConstraints { make in
                    make.top.equalTo(otherRemarkHintLabel.snp.bottom).offset(5)
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(56)
                }
            }
            
            let otherRemarkInfostextFiled = UITextField().then { obj in
                otheRemarkInfostextV.addSubview(obj)
                obj.placeholder = "请输入"
                obj.font = kMediumFont(12)
                obj.tag = 3005
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(15)
                    make.right.bottom.equalToSuperview().offset(-15)
                }
            }

            _ = UIView().then({ obj in
                otherConditionInfosv.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        
        
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-20)
            }
        })
        
        let bottomView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight()+20)
            }
        }
        
        let isAgreementButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.height.equalTo(15)
            }
        }
        
        let agreementLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.textColor = blackTextColor3
            obj.font = kMediumFont(14)
            obj.text = "此商机共享"
            obj.snp.makeConstraints { make in
                make.left.equalTo(isAgreementButton.snp.right).offset(5)
                make.centerY.equalTo(isAgreementButton)
            }
        }
        
        let saveButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.setTitle("保存", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 4
            obj.addTarget(self, action:#selector(saveClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(50)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        
        
        // 日期选择
        companyDateView = MyDateView.init()
        //myDateView.isShowDay = false
        companyDateView.delegate = self
    }
    
    
    
    @objc func addPersonClick(_ button:UIButton){
        printTest(button.tag)
        if button.tag == 1001{
            // 商机客户名称
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 10
            vc.addClientSelectedBussinessNameBlock = {[self] model in
                addClientSelectedBussinessNameModel = model
                clientNameTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1003{
            // 商机来源
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 11
            vc.addClientDataBlock = {[self] model in
                attributeModel = model
                sourceTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1006{
            // 商机产品类型
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 12
            vc.addClientSelectedNatureBlock = {[self] model in
                addClientSelectedNatureModel = model
                clientNatureTextFiled?.text = model.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1007{
            // 阶段
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 13
            vc.addClientSelectedPhaseBlock = {[self] model in
                addClientSelectedPhaseModel = model
                clientPhaseTextFiled?.text = model.stageName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1008{
            //状态
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 16
            vc.addClientDataBlock = {[self] model in
                addClientSelectedBussinessStatusModel = model
                clientBussinessStatusTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1009{
            // 商机产品类型
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 14
            vc.addClientPersonBlock = {[self] model in
                addClientSelectedPersonModel = model
                personTextFiled?.text = addClientSelectedPersonModel?.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1010{
            companyDateView.showView()
        }else if button.tag == 1011{
            // 商机产品类型
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 15
            vc.addClientDataBlock = {[self] model in
                probablyModel = model
                clientProblelyTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1004{
            // 商机性质
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 32
            vc.addClientDataBlock = {[self] model in
                addClientSelectedBussinessNatureModel = model
                clientBussinessNatureTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func saveClick(){
        
        
        
        var clientClueContactSimpleParam = ["name":nameStr ?? "", "gender":sexStr ?? "", "position": jobStr ?? "", "phone":phoneStr ?? "", "email":emailStr ?? "", "address":addressStr ?? ""]
        var clientCommercialExtraInfoDTO = [ "competitor" : relationsPerson ?? "", "cooperateBuy": buyNum ?? "", "goCompany":isCome as Any, "trackState":trackStr ?? "", "remark": remarkStr ?? ""]
        
        presenter?.presenterRequestGetAddBusiness(by: ["businessOpportunity":businessCode,
                                                       "property":addClientSelectedBussinessNatureModel?.attrName,
                                                       "source":attributeModel?.attrName,
                                                       "clientId":addClientSelectedBussinessNameModel?.clientId,
                                                       "id":addClientSelectedBussinessNameModel?.clientId,
                                                       "commercialMoney":businessMoney,
                                                       "productTypeName":addClientSelectedBussinessNatureModel?.attrName,
                                                       "productTypeId":addClientSelectedBussinessNatureModel?.id,
                                                       "interventionStage":addClientSelectedPhaseModel?.stageName,
                                                       "customerDealStatus":addClientSelectedBussinessStatusModel?.attrName,
                                                       "productDate":businessTime,
                                                       "probability":probablyModel?.attrNum,
                                                       "clientClueContactSimpleParam":clientClueContactSimpleParam,
                                                       "clientCommercialExtraInfoDTO":clientCommercialExtraInfoDTO,
                                                       "followerId":addClientSelectedPersonModel?.userId,
//                                                       "commercialInfo":addClientSelectedBussinessNameModel?.clientName
                                                       ])
        printTest(["businessOpportunity":businessCode,
                   "property":addClientSelectedBussinessNatureModel?.attrName,
                   "source":attributeModel?.attrName,
                   "clientId":addClientSelectedBussinessNameModel?.clientId,
                   "commercialMoney":businessMoney,
                   "productTypeName":addClientSelectedBussinessNatureModel?.attrName,
                   "productTypeId":addClientSelectedBussinessNatureModel?.id,
                   "interventionStage":addClientSelectedPhaseModel?.stageName,
                   "customerDealStatus":addClientSelectedBussinessStatusModel?.attrName,
                   "productDate":businessTime,
                   "probability":probablyModel?.attrNum,
                   "clientClueContactSimpleParam":clientClueContactSimpleParam,
                   "clientCommercialExtraInfoDTO":clientCommercialExtraInfoDTO,
                   "followerId":addClientSelectedPersonModel?.userId
                   ])
    }
    
    func cofing(){
        let router = AddBusinessRouter()
        
        presenter = AddBusinessPresenter()
        
        presenter?.router = router
        
        let entity = AddBusinessEntity()
        
        let interactor = AddBusinessInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    func pickDateView(year: Int, month: Int, day: Int) {
//        if dateSelectedIndex == 1{
        businessTime = "\(year)-\(month)-\(day) 00:00:00"
        timeTextFiled?.text = "\(year)年\(month)月\(day)日 00:00:00"
//        }else{
//            lastTimeTextField?.text = "\(year)年\(month)月\(day)日"
//        }
    }
    
    
    @objc func okStudyClick(){
        okStudyButton?.layer.cornerRadius = 7.5
        okStudyButton?.backgroundColor = bgColor17
        
        noStudyButton?.layer.cornerRadius = 7.5
        noStudyButton?.backgroundColor = .white
        noStudyButton?.layer.borderWidth = 0.5
        noStudyButton?.layer.borderColor = lineColor28.cgColor
        
        isCome = 1
    }
    
    @objc func noStudyClick(){
        noStudyButton?.layer.cornerRadius = 7.5
        noStudyButton?.backgroundColor = bgColor17
        
        okStudyButton?.layer.cornerRadius = 7.5
        okStudyButton?.backgroundColor = .white
        okStudyButton?.layer.borderWidth = 0.5
        okStudyButton?.layer.borderColor = lineColor28.cgColor
        
        isCome = 0
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddBusinessViewController: AddBusinessViewProtocols{
    func didGetAddBusinessNumberPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BusinessListNotificationKey"), object: nil, userInfo: nil)
//                navigationController?.popViewController(animated: true)
                businessCode = (dict["data"] as! String)
                clientTextFiled?.text = businessCode
                
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddBusinessNumberPresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    
    func didGetAddBusinessPresenterReceiveData(by params: Any?) {
        printTest(params)
        // 400错误
        
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                printTest(params)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BusinessListNotificationKey"), object: nil, userInfo: nil)
                navigationController?.popViewController(animated: true)
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddBusinessPresenterReceiveError(error: MyError?) {
        
    }
}

//extension AddBusinessViewController: JXSegmentedViewDelegate,JXPagingViewDelegate{
//    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
//        return 0
//    }
//
//    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
//        return 0
//    }
//
//    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func numberOfLists(in pagingView: JXPagingView) -> Int {
//        return 3
//    }
//
//    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
//        return 43
//    }
//
//    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
//        return segmentedView!
//    }
//
//
//    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
//        let vc = AddBusinessInfosViewController()
//        return vc
//    }
//
////    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
////        return 0
////    }r
////
////    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
////        return UIView()
////    }
////
////    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
////        return 0
////    }
////
////    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
////        return UIView()
////    }
////
////    func numberOfLists(in pagingView: JXPagingView) -> Int {
////        return 4
////    }
////
////    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
////        return 43
////    }
////
////    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
////        return segmentedView!
////    }
////
//    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
////        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
////        printTest(ZZzz)
////        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
//    }
//}


extension AddBusinessViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            businessCode = textField.text
        }else if textField.tag == 1001{
            clientName = textField.text
        }else if textField.tag == 1002{
            businessName = textField.text
        }else if textField.tag == 1003{
            businessSoure = textField.text
        }else if textField.tag == 1004{
            bussinessCharacter = textField.text
        }else if textField.tag == 1005{
            businessMoney = textField.text
        }else if textField.tag == 1006{
            productType = textField.text
        }else if textField.tag == 1007{
            businessPhase = textField.text
        }else if textField.tag == 1008{
            clientStatus = textField.text
        }else if textField.tag == 1009{
            followPerson = textField.text
        }else if textField.tag == 1010{
            businessTime = textField.text
        }else if textField.tag == 1011{
            possibilityStr = textField.text
        }else if textField.tag == 2000{
            nameStr = textField.text
        }else if textField.tag == 2001{
            sexStr = textField.text
        }else if textField.tag == 2002{
            jobStr = textField.text
        }else if textField.tag == 2003{
            emailStr = textField.text
        }else if textField.tag == 2004{
            phoneStr = textField.text
        }else if textField.tag == 2005{
            addressStr = textField.text
        }else if textField.tag == 3000{
            otherMsgStr = textField.text
        }else if textField.tag == 3001{
            opponentStr = textField.text
        }else if textField.tag == 3002{
            relationsPerson = textField.text
        }else if textField.tag == 3003{
            buyNum = textField.text
        }else if textField.tag == 3004{
            trackStr = textField.text
        }else if textField.tag == 3005{
            remarkStr = textField.text
        }
    }
}
