//
//  WorkStatusEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class WorkStatusEntity: WorkStatusEntityProtocols {
    var interactor: WorkStatusInteractorProtocols?
    
    func didWorkStatusReceiveData(by params: Any?) {
        
    }
    
    func didWorkStatusReceiveError(error: MyError?) {
        
    }
    
    func getWorkStatusRequest(by params: Any?) {
        
    }
    
    
}
