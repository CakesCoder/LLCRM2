//
//  WorkStatusPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class WorkStatusPresenter: WorkStatusPresenterProtocols {
    var view: WorkStatusViewProtocols?
    
    var router: WorkStatusRouterProtocols?
    
    var interactor: WorkStatusInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetWorkStatus(by params: Any?) {
        
    }
    
    func didGetWorkStatusInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetWorkStatusInteractorReceiveError(error: MyError?) {
        
    }
    
    
}
