//
//  WorkStatusProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class WorkStatusProtocols: NSObject {

}

protocol WorkStatusViewProtocols: AnyObject {
    var presenter: WorkStatusPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetWorkStatusPresenterReceiveData(by params: Any?)
    func didGetWorkStatusPresenterReceiveError(error: MyError?)
}

protocol WorkStatusPresenterProtocols: AnyObject{
    var view: WorkStatusViewProtocols? { get set }
    var router: WorkStatusRouterProtocols? { get set }
    var interactor: WorkStatusInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetWorkStatus(by params: Any?)
    func didGetWorkStatusInteractorReceiveData(by params: Any?)
    func didGetWorkStatusInteractorReceiveError(error: MyError?)
}

protocol WorkStatusInteractorProtocols: AnyObject {
    var presenter: WorkStatusPresenterProtocols? { get set }
    var entity: WorkStatusEntityProtocols? { get set }
    
    func presenterRequestWorkStatus(by params: Any?)
    func didEntityWorkStatusReceiveData(by params: Any?)
    func didEntityWorkStatusReceiveError(error: MyError?)
}

protocol WorkStatusEntityProtocols: AnyObject {
    var interactor: WorkStatusInteractorProtocols? { get set }
    
    func didWorkStatusReceiveData(by params: Any?)
    func didWorkStatusReceiveError(error: MyError?)
    func getWorkStatusRequest(by params: Any?)
}

protocol WorkStatusRouterProtocols: AnyObject {
    func pushToSettingWorkStatus(from previousView: UIViewController)
    func pushToMyAddBusinessList(from previousView: UIViewController)
    func pushToBusinessListDetail(from previousView: UIViewController)
}

