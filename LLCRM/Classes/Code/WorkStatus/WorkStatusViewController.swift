//
//  WorkStatusViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class WorkStatusViewController: BaseViewController {
    
    var presenter: WorkStatusPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "工作状态"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(WorkStatusViewController.blackClick))
        
        let headView = UIView().then { obj in
            obj.backgroundColor = bgColor
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let statusView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(0)
            }
        }
        
        _ = UILabel().then { obj in
            statusView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "当前状态"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let settingView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(50)
                make.height.equalTo(50)
            }
        }
        
        let settingHintLabel = UILabel().then { obj in
            settingView.addSubview(obj)
            obj.text = "暂无状态，点击"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let settingLabel = UILabel().then { obj in
            settingView.addSubview(obj)
            obj.text = "设置"
            obj.textColor = blackTextColor50
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(settingHintLabel.snp.right).offset(10)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then({ obj in
            settingView.addSubview(obj)
            obj.addTarget(self, action: #selector(settingClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let wrokStatusView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(100)
                make.height.equalTo(50)
            }
        }
        
        let wrokStatusHintLabel = UILabel().then { obj in
            wrokStatusView.addSubview(obj)
            obj.text = "请假、出差自动同步为工作状态"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let wrokStatusSwitch = UISwitch().then { obj in
            wrokStatusView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let wrokStatusHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(150)
            }
        }
        
        _ = UILabel().then { obj in
            wrokStatusHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "开启后，再请假、出差事件在相应的时间段内，自动同步到我的工作状态"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let tellMeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(200)
                make.height.equalTo(50)
            }
        }
        
        let tellMeHintLabel = UILabel().then { obj in
            tellMeView.addSubview(obj)
            obj.text = "告知同事我可能无法立即回复"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let tellMeSwitch = UISwitch().then { obj in
            tellMeView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let tellMeHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(80)
                make.top.equalToSuperview().offset(250)
            }
        }
        
        _ = UILabel().then { obj in
            tellMeHintView.addSubview(obj)
            obj.numberOfLines = 3
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "开启后，当我在单人对话中收到消息或在群对话中收@我时，系统自动回复提示“王小虎当前状态为会议中，可能无法立即回复”。无状态时除外"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        // Do any additional setup after loading the view.
        
        cofing()
    }
    
    func cofing(){
        let router = WorkStatusRouter()
        
        presenter = WorkStatusPresenter()
        
        presenter?.router = router
        
        let entity = WorkStatusEntity()
        
        let interactor = WorkStatusInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    @objc func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func settingClick(){
        presenter?.router?.pushToSettingWorkStatus(from: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WorkStatusViewController: WorkStatusViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetWorkStatusPresenterReceiveData(by params: Any?) {
        
    }
    
    func didGetWorkStatusPresenterReceiveError(error: MyError?) {
        
    }
}
