//
//  WorkStatusRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class WorkStatusRouter: WorkStatusRouterProtocols {
    func pushToSettingWorkStatus(from previousView: UIViewController) {
        let nextView = SettingWorkStatusViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToMyAddBusinessList(from previousView: UIViewController) {
        
    }
    
    func pushToBusinessListDetail(from previousView: UIViewController) {
        
    }
    
    
}
