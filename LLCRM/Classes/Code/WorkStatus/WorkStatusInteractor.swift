//
//  WorkStatusInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class WorkStatusInteractor: WorkStatusInteractorProtocols {
    var presenter: WorkStatusPresenterProtocols?
    
    var entity: WorkStatusEntityProtocols?
    
    func presenterRequestWorkStatus(by params: Any?) {
        
    }
    
    func didEntityWorkStatusReceiveData(by params: Any?) {
        
    }
    
    func didEntityWorkStatusReceiveError(error: MyError?) {
        
    }
    
    
}
