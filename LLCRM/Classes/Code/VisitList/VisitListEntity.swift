//
//  VisitListEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListEntity: VisitListEntityProtocols {
    var interactor: VisitListInteractorProtocols?
    
    func didVisitListReceiveData(by params: Any?) {
        interactor?.didEntityVisitListReceiveData(by: params)
    }
    
    func didVisitListReceiveError(error: MyError?) {
        interactor?.didEntityVisitListReceiveError(error: error)
    }
    
    func getVisitListRequest(by params: Any?) {
        LLNetProvider.request(.getAllVisitInfo(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didVisitListReceiveData(by:jsonData)
            case .failure(_):
                self.didVisitListReceiveError(error: .requestError)
            }
        }
    }
    
    
}
