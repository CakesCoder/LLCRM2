//
//  VisitListRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListRouter: VisitListRouterProtocols {
    func pushToAddVisit(from previousView: UIViewController) {
        let nextView = AddVisitViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToVisitDetail(from previousView: UIViewController, forModel model: Any) {
        let nextView = VisitListDetailViewController()
        nextView.model = model as? VisiListDataModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
