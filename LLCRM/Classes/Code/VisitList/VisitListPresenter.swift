//
//  VisitListPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListPresenter: VisitListPresenterProtocols {
    var view: VisitListViewProtocols?
    
    var router: VisitListRouterProtocols?
    
    var interactor: VisitListInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetVisitList(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestVisitList(by: params)
    }
    
    func didGetVisitListInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetVisitListPresenterReceiveData(by: params)
    }
    
    func didGetVisitListInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetVisitListPresenterReceiveError(error: error)
    }
}
