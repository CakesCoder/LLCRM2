//
//  VisitListTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListTableViewCell: UITableViewCell {
    
    var model:VisiListDataModel?{
        didSet{
            nameLabel?.text = model?.visitComName ?? "--"
            tagLabel?.text = (model?.visitTimes ?? "-")+"次拜访"
            typeLabel?.text = model?.visitCategory ?? "--"
            personLabel?.text = model?.visitPeopleName ?? "--"
            timeLabel?.text = model?.createDate ?? "--"
        }
    }
    
    var tagLabel:UILabel?
    var nameLabel:UILabel?
    var typeLabel:UILabel?
    var personLabel:UILabel?
    var timeLabel:UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor8
//            obj.text = "零瓴软件技术（深圳）有限公司"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(27)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        tagLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor44
            obj.font = kRegularFont(9)
            obj.textColor = .white
            obj.textAlignment = .center
            obj.text = "初次拜访"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel!)
                make.width.equalTo(54)
                make.height.equalTo(24)
            }
        }
        
        typeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "拜访类型：实地考察"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(27)
                make.top.equalTo(nameLabel!.snp.bottom).offset(15)
            }
        }
        
        personLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "负责人：王小虎"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.snp.makeConstraints { make in
                make.top.equalTo(typeLabel!.snp.bottom).offset(2)
                make.left.equalToSuperview().offset(27)
            }
        }
        
        timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "创建时间：2022.05.06  15：30：45"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor8
            obj.snp.makeConstraints { make in
                make.top.equalTo(personLabel!.snp.bottom).offset(2)
                make.left.equalToSuperview().offset(27)
            }
        }
    }

}
