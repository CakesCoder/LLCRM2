//
//  VIsitListModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class VisitListModel: BaseModel {
    var code:String?
    var data:[VisiListDataModel]?
    var message:String?
        
    required init() {}
}

class VisiListDataModel:BaseModel{
    var clientPeoples:String?
    var companyPeoples:String?
    var conclusion:String?
    var createDate:String?
    var createDept:String?
    var createrId:String?
    var createrName:String?
    var endTime:String?
    var id:String?
    var meetingRecordJson:String?
    var problem:String?
    var realityEndTime:String?
    var realityStartTime:String?
    var startTime:String?
    var visitCategory:String?
    var visitComId:String?
    var visitComName:String?
    var visitDept:String?
    var visitNumber:String?
    var visitPeople:String?
    var visitPeopleName:String?
    var visitPurpose:String?
    var visitTimes:String?
    var visitTopic:String?
    
    
    required init() {}
}

