//
//  VisitListInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListInteractor: VisitListInteractorProtocols {
    var presenter: VisitListPresenterProtocols?
    
    var entity: VisitListEntityProtocols?
    
    func presenterRequestVisitList(by params: Any?) {
        entity?.getVisitListRequest(by: params)
    }
    
    func didEntityVisitListReceiveData(by params: Any?) {
        presenter?.didGetVisitListInteractorReceiveData(by: params)
    }
    
    func didEntityVisitListReceiveError(error: MyError?) {
        presenter?.didGetVisitListInteractorReceiveError(error: error)
    }
    

}
