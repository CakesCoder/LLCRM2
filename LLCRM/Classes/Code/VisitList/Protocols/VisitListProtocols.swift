//
//  VisitListProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListProtocols: NSObject {

}

protocol VisitListViewProtocols: AnyObject {
    var presenter: VisitListPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetVisitListPresenterReceiveData(by params: Any?)
    func didGetVisitListPresenterReceiveError(error: MyError?)
}

protocol VisitListPresenterProtocols: AnyObject{
    var view: VisitListViewProtocols? { get set }
    var router: VisitListRouterProtocols? { get set }
    var interactor: VisitListInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetVisitList(by params: Any?)
    func didGetVisitListInteractorReceiveData(by params: Any?)
    func didGetVisitListInteractorReceiveError(error: MyError?)
}

protocol VisitListInteractorProtocols: AnyObject {
    var presenter: VisitListPresenterProtocols? { get set }
    var entity: VisitListEntityProtocols? { get set }
    
    func presenterRequestVisitList(by params: Any?)
    func didEntityVisitListReceiveData(by params: Any?)
    func didEntityVisitListReceiveError(error: MyError?)
}

protocol VisitListEntityProtocols: AnyObject {
    var interactor: VisitListInteractorProtocols? { get set }
    
    func didVisitListReceiveData(by params: Any?)
    func didVisitListReceiveError(error: MyError?)
    func getVisitListRequest(by params: Any?)
}

protocol VisitListRouterProtocols: AnyObject {
    func pushToAddVisit(from previousView: UIViewController)
    func pushToVisitDetail(from previousView: UIViewController, forModel model: Any)
}



