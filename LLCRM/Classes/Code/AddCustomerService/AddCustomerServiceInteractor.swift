//
//  AddCustomerServiceInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddCustomerServiceInteractor: AddCustomerServiceInteractorProtocols {
    var presenter: AddCustomerServicePresenterProtocols?
    
    var entity: AddCustomerServiceEntityProtocols?
    
    func presenterRequestAddCustomerService(by params: Any?) {
        entity?.getAddCustomerServiceRequest(by: params)
    }
    
    func didEntityAddCustomerServiceReceiveData(by params: Any?) {
        presenter?.didGetAddCustomerServiceInteractorReceiveData(by: params)
    }
    
    func didEntityAddCustomerServiceReceiveError(error: MyError?) {
        presenter?.didGetAddCustomerServiceInteractorReceiveError(error: error)
        
    }
    
    
}
