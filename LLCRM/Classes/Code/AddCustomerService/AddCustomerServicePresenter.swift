//
//  AddCustomerServicePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddCustomerServicePresenter: AddCustomerServicePresenterProtocols {
    var view: AddCustomerServiceViewProtocols?
    
    var router: AddCustomerServiceRouterProtocols?
    
    var interactor: AddCustomerServiceInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddCustomerService(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddCustomerService(by: params)
    }
    
    func didGetAddCustomerServiceInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddCustomerServicePresenterReceiveData(by: params)
    }
    
    func didGetAddCustomerServiceInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddCustomerServicePresenterReceiveError(error: error)
    }
    
    
}
