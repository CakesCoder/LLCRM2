//
//  AddCustomerServiceProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddCustomerServiceProtocols: NSObject {

}

protocol AddCustomerServiceViewProtocols: AnyObject {
    var presenter: AddCustomerServicePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddCustomerServicePresenterReceiveData(by params: Any?)
    func didGetAddCustomerServicePresenterReceiveError(error: MyError?)
}

protocol AddCustomerServicePresenterProtocols: AnyObject{
    var view: AddCustomerServiceViewProtocols? { get set }
    var router: AddCustomerServiceRouterProtocols? { get set }
    var interactor: AddCustomerServiceInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddCustomerService(by params: Any?)
    func didGetAddCustomerServiceInteractorReceiveData(by params: Any?)
    func didGetAddCustomerServiceInteractorReceiveError(error: MyError?)
}

protocol AddCustomerServiceInteractorProtocols: AnyObject {
    var presenter: AddCustomerServicePresenterProtocols? { get set }
    var entity: AddCustomerServiceEntityProtocols? { get set }
    
    func presenterRequestAddCustomerService(by params: Any?)
    func didEntityAddCustomerServiceReceiveData(by params: Any?)
    func didEntityAddCustomerServiceReceiveError(error: MyError?)
}

protocol AddCustomerServiceEntityProtocols: AnyObject {
    var interactor: AddCustomerServiceInteractorProtocols? { get set }
    
    func didAddCustomerServiceReceiveData(by params: Any?)
    func didAddCustomerServiceReceiveError(error: MyError?)
    func getAddCustomerServiceRequest(by params: Any?)
}

protocol AddCustomerServiceRouterProtocols: AnyObject {
    func pushToAddCustomerService(from previousView: UIViewController)
    func pushToCreatCustomerService(from previousView: UIViewController)
    func pushToCustomerServiceDetail(from previousView: UIViewController, forModel model: Any)
}



