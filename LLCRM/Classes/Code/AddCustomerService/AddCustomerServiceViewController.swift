//
//  AddCustomerServiceViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit
import YYText_swift
import PKHUD
import HandyJSON

class AddCustomerServiceViewController: BaseViewController {
    
    var presenter: AddCustomerServicePresenterProtocols?
    
    var clientDiscardNo:String?
    var productTypeName:String?
    var productName:String?
    var productModel:String?
    var productCode:String?
    var discardCount:String?
    var specDesc:String?
    var badType:String?
    var badDesc:String?
    
    var specDescTextView:YYTextView?
    var badDescTextView:YYTextView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        navigationItem.title = "添加客退"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddCustomerServiceViewController.backClick))
        
        view.backgroundColor = bgColor
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        let baseInfos = [["*","客户退货单号："],["*","产品类别："],["*","产品名称："],["产品型号："],["产品编码："],["*","客退数量："],["规格描述："],["*","不良类别："],["不良类别描述："]]
        
        
        let contentInfos = ["请输入", "输入择", "请输入", "请输入", "请输入", "请输入", "", "请输入", ""]
        
//        var temp_view = UIView()
        
        for i in 0..<baseInfos.count{
            printTest("asdasdasdasd")
            printTest(i)
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    if i == 7{
                        make.top.equalToSuperview().offset(6*45+140)
                    }else if i == 8{
                        make.top.equalToSuperview().offset(7*45+140)
                    }else{
                        make.top.equalToSuperview().offset(i*45)
                    }
                    make.right.equalToSuperview().offset(-0)
                    if i == 8 || i == 6{
                        make.height.equalTo(140)
                    }else{
                        make.height.equalTo(45)
                    }
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                        
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
//                    make.centerY.equalToSuperview()
                    make.top.equalToSuperview().offset(15)
                }
            }
            
            if i == 0 || i == 3 || i == 7{
                let tagImageV = UIImageView().then { obj in
                    v.addSubview(obj)
//                    obj.backgroundColor = .red
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-15)
                        make.centerY.equalToSuperview()
//                        make.width.height.equalTo(15)
                    }
                }
            }
            
            if i == 6{
                let textView = UIView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = bgColor7
                    obj.layer.cornerRadius = 4
                    obj.layer.borderWidth = 0.5
                    obj.layer.borderColor = lineColor7.cgColor
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(5)
                        make.right.equalToSuperview().offset(-15)
                        make.top.equalToSuperview().offset(36)
                        make.bottom.equalToSuperview().offset(-15)
                    }
                }
                specDescTextView = YYTextView().then { obj in
                    textView.addSubview(obj)
                    obj.placeholderText = "输入"
                    obj.placeholderFont = kRegularFont(14)
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(10)
                        make.right.equalToSuperview().offset(-10)
                        make.top.equalToSuperview().offset(10)
                        make.bottom.equalToSuperview().offset(-10)
                    }
                }
            }else if i == 8{
                let textView = UIView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = bgColor7
                    obj.layer.cornerRadius = 4
                    obj.layer.borderWidth = 0.5
                    obj.layer.borderColor = lineColor7.cgColor
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(5)
                        make.right.equalToSuperview().offset(-15)
                        make.top.equalToSuperview().offset(36)
                        make.bottom.equalToSuperview().offset(-15)
                    }
                }
                badDescTextView = YYTextView().then { obj in
                    textView.addSubview(obj)
                    obj.placeholderText = "输入"
                    obj.placeholderFont = kRegularFont(14)
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(10)
                        make.right.equalToSuperview().offset(-10)
                        make.top.equalToSuperview().offset(10)
                        make.bottom.equalToSuperview().offset(-10)
                    }
                }
            }else{
                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = contentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    obj.delegate = self
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }
                
//                if i == 0 || i == 4 || i == 7{
//                    _ = UIButton().then({ obj in
//                        v.addSubview(obj)
//                        obj.tag = 1000
//                        obj.addTarget(self, action: #selector(companyClick(_ :)), for: .touchUpInside)
//                        obj.snp.makeConstraints { make in
//                            make.left.top.equalToSuperview().offset(r0)
//                            make.right.bottom.equalToSuperview().offset(-0)
//                        }
//                    })
//                }
            }

            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-130)
            }
        })
        
        let okButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("确定", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.cornerRadius = 2
            obj.tag = 1000
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-22)
                make.bottom.equalToSuperview().offset(-40)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
        }
        
        let cancelButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.setTitle("取消", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.titleLabel?.font = kMediumFont(11)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor15.cgColor
            obj.tag = 1001
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalTo(okButton.snp.left).offset(-10)
                make.bottom.equalToSuperview().offset(-40)
                make.width.equalTo(80)
                make.height.equalTo(30)
            }
        }
        
        cofing()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
//    @objc func companyClick(_ btn:UIButton){
//        btn.tag == 1001{
//            
//        }else if btn.tag == 4{
//            
//        }else if btn.tag == 7{
//            
//        }
//    }
    
//    var clientDiscardNo:String?
//    var productTypeName:String?
//    var productName:String?
//    var productModel:String?
//    var productCode:String?
//    var discardCount:String?
//    var specDesc:String?
//    var badType:String?
//    var badDesc:String?
    
    @objc func addClick(_ button:UIButton){
        if button.tag == 1000{
            presenter?.presenterRequestGetAddCustomerService(by: ["clientDiscardNo":clientDiscardNo,"productTypeName":productTypeName, "productName":productName, "productModel":productModel, "productCode":productCode,"discardCount":discardCount,"specDesc":specDescTextView?.text,"badType":badType, "badDesc":badDescTextView?.text])
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func cofing(){
        let router = AddCustomerServiceRouter()
        
        presenter = AddCustomerServicePresenter()
        
        presenter?.router = router
        
        let entity =  AddCustomerServiceEntity()
        
        let interactor =  AddCustomerServiceInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
//        presenter?.presenterRequestGetCustomerService(by: ["current":currentIndex,"size":20])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddCustomerServiceViewController: AddCustomerServiceViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetAddCustomerServicePresenterReceiveData(by params: Any?) {
        printTest(params)
        
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                HUD.flash(.label("添加成功"), delay: 2)
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "CustomerServiceNotificationKey"), object: nil, userInfo: nil)
                navigationController?.popViewController(animated: true)
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddCustomerServicePresenterReceiveError(error: MyError?) {
        
    }
    
    
}

//var clientDiscardNo:String?
//var productTypeName:String?
//var productName:String?
//var productModel:String?
//var productCode:String?
//var discardCount:String?
//var specDesc:String?
//var badType:String?
//var badDesc:String?

extension AddCustomerServiceViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            clientDiscardNo = textField.text
        }else if textField.tag == 1001{
            productTypeName = textField.text
        }else if textField.tag == 1002{
            productName = textField.text
        }else if textField.tag == 1003{
            productModel = textField.text
        }else if textField.tag == 1004{
            productCode = textField.text
        }else if textField.tag == 1005{
            discardCount = textField.text
        }else if textField.tag == 1006{
            specDesc = textField.text
        }else if textField.tag == 1007{
            badType = textField.text
        }else if textField.tag == 1008{
            badDesc = textField.text
        }
    }
}
