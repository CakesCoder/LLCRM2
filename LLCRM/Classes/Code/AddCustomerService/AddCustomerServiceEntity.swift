//
//  AddCustomerServiceEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class AddCustomerServiceEntity: AddCustomerServiceEntityProtocols {
    var interactor: AddCustomerServiceInteractorProtocols?
    
    func didAddCustomerServiceReceiveData(by params: Any?) {
        interactor?.didEntityAddCustomerServiceReceiveData(by: params)
    }
    
    func didAddCustomerServiceReceiveError(error: MyError?) {
        interactor?.didEntityAddCustomerServiceReceiveError(error: error)
    }
    
    func getAddCustomerServiceRequest(by params: Any?) {
        LLNetProvider.request(.addComplaintProduct(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddCustomerServiceReceiveData(by:jsonData)
            case .failure(_):
                self.didAddCustomerServiceReceiveError(error: .requestError)
            }
        }
    }
}
