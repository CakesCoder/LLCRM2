//
//  RepeatTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class RepeatTableViewCell: UITableViewCell {
    var nameLabel:UILabel?
    var selectedImageV:UIImageView?
    
    var titleStr:String?{
        didSet{
            self.nameLabel?.text = titleStr
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(14)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.centerY.equalToSuperview()
            }
        }
        
        selectedImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "selected")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
}
