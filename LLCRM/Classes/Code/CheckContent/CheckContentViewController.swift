//
//  CheckContentViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit
//import JXPagingView

extension CheckContentViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension CheckContentViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class CheckContentViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight+300)
//            obj.backgroundColor = bgColor
        }
        
        let checkView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(130)
            }
        }
        
        let checkHeadView = UIView().then { obj in
            checkView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-8)
//                make.height.equalTo()
            }
        }
        
        let checkheadTagView = UIView().then { obj in
            checkHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let checkheadLabel = UILabel().then { obj in
            checkHeadView.addSubview(obj)
            obj.text = "审批内容"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(checkheadTagView.snp.right).offset(5)
                make.centerY.equalTo(checkheadTagView)
            }
        }
        
        let checkaddButton = UIButton().then { obj in
            checkHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
//            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let checkDetailView = UIView().then { obj in
            checkHeadView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.backgroundColor = bgColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(checkheadLabel.snp.bottom).offset(10)
                make.bottom.equalToSuperview().offset(-8)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let checkDetailHintLabel = UILabel().then { obj in
            checkDetailView.addSubview(obj)
            obj.text = "报销审批"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(10)
            }
        }
        
        let checkDetailSubLabel = UILabel().then { obj in
            checkDetailView.addSubview(obj)
            obj.text = "新建费用报销单   20220516001"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor68
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(checkDetailHintLabel.snp.bottom).offset(10)
            }
        }
        
        let checkDetailStatuLabel = UILabel().then { obj in
            checkDetailView.addSubview(obj)
            obj.textColor = bgColor9
            obj.backgroundColor = bgColor14
            obj.layer.borderWidth = 0.5
            obj.layer.cornerRadius = 2
            obj.layer.borderColor = bgColor9.cgColor
            obj.font = kRegularFont(10)
            obj.textAlignment = .center
            obj.text = "已批准"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.width.equalTo(48)
                make.height.equalTo(22)
            }
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(130)
                make.right.equalToSuperview().offset(0)
                make.height.equalTo(11*45+10)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }

        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }

        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "基本信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["报销单号：", "报销事项：", "发生日期：", "客户名称：", "金额(元)：", "负责人：",  "负责部门：", "业务类型：", "生命状态：", "归属部门："]
        
        let contentInfos = ["20230524001", "宴请", "2023.05.24", "零瓴软件技术(深圳)有限公司", "￥1,000.00", "王小虎","研发部","招待/礼品/客户车费费用","正常","技术团队"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:contentInfos[i])
                if i == baseInfos.count-2{
                    hintAttribute.yy_color = bgColor9
                }else{
                    hintAttribute.yy_color = blackTextColor81
                }
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
               
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UIView().then({ obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
        
        let systemInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(baseInfosView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(0)
                make.height.equalTo(5*45)
            }
        }
        
        let systemInfosHeadView = UIView().then { obj in
            systemInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }

        let systemHeadTagView = UIView().then { obj in
            systemInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }

        let systemHeadLabel = UILabel().then { obj in
            systemInfosHeadView.addSubview(obj)
            obj.text = "系统信息"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(systemHeadTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let systemInfos = ["创建人：", "创建时间：", "最后修改人：", "最后修改时间："]
        
        let systemContentInfos = ["王小虎", "2023.05.24  12:30:10", "夏淼淼", "2023.05.24  14:20:00"]
        
        for i in 0..<systemInfos.count{
            let v = UIView().then { obj in
                systemInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:systemInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:systemContentInfos[i])
//                if i == 1{
//                    hintAttribute.yy_color = bgColor9
//                }else{
                    hintAttribute.yy_color = blackTextColor81
//                }
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(100)
                    make.centerY.equalToSuperview()
                }
               
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }

        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
