//
//  CRMNotificationTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class CRMNotificationTableViewCell: UITableViewCell {
    
    var nameLabel:UILabel?
    var tagLabel:UILabel?
    var areaLabel:UILabel?
    var timeLabel:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func buildUI(){
        self.selectionStyle = .none
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "crm_notification")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
                make.width.height.equalTo(15)
            }
        }
        
        nameLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "零瓴客户提醒"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor84
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(5)
//                make.centerX.equalTo(imageV)
                make.top.equalToSuperview().offset(15)
            }
        })
        
        tagLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor36
            obj.layer.cornerRadius = 2
            obj.textColor = .white
            obj.font = kRegularFont(8)
            obj.text = "未跟进"
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel!.snp.right).offset(8)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(36)
                make.height.equalTo(16)
            }
        })
        
        areaLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "深圳市南山区零瓴软件技术(深圳)有限公司"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        timeLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "2022.05.16  09:30"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(areaLabel!.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        })
        
        
        _ = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(12)
            }
        })
        
    }
}
