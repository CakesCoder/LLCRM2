//
//  ChangeRecordTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class ChangeRecordTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initUI(){
        self.selectionStyle = .none
        let lineView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        let outPointView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.layer.cornerRadius = 7.5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.top.equalToSuperview().offset(20)
                make.width.height.equalTo(15)
            }
        }
        
        let pointView = UIView().then { obj in
            outPointView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 3.5
            obj.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.width.height.equalTo(7)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "30分钟以前"
            obj.font = kRegularFont(13)
            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalTo(outPointView.snp.right).offset(5)
                make.centerY.equalTo(outPointView)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "夏淼淼 编辑 “报价单"
            obj.textColor = blackTextColor33
            obj.font = kRegularFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalTo(lineView.snp.right).offset(27)
                make.top.equalTo(timeLabel.snp.bottom).offset(15)
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .black
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-30)
                make.centerY.equalTo(nameLabel)
                make.width.height.equalTo(12)
            }
        }
        
        let recordContentView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor19
            obj.layer.cornerRadius = 6
            obj.snp.makeConstraints { make in
                make.left.equalTo(lineView.snp.right).offset(27)
                make.right.equalToSuperview().offset(-30)
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let contentLabel = UILabel().then { obj in
            recordContentView.addSubview(obj)
            obj.text = "修改 更新时间，由“2022.05.16  11：19"
            obj.numberOfLines = 2
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor33
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(10)
                make.right.bottom.equalToSuperview().offset(-10)
            }
        }
    }

}
