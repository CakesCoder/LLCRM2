//
//  SempleProgressDetailNoDataTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class SempleProgressDetailNoDataTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let lineView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor27
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        let outPointView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor27
            obj.layer.cornerRadius = 7.5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.top.equalToSuperview().offset(20)
                make.width.height.equalTo(15)
            }
        }
        
        let pointView = UIView().then { obj in
            outPointView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 3.5
            obj.snp.makeConstraints { make in
                make.center.equalToSuperview()
                make.width.height.equalTo(7)
            }
        }
        
        let infosLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            
            let hintAttribute = NSMutableAttributedString(string:"样品提供")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let timeCotentAttribute = NSMutableAttributedString(string:model.productList?[0].validTimeStart ?? "--")
//                timeCotentAttribute.yy_color = blackTextColor80
//                timeCotentAttribute.yy_font = kMediumFont(13)
//                timeHintAttribute.append(timeCotentAttribute)
//                obj.attributedText = timeHintAttribute
//            }else{
                let contentAttribute = NSMutableAttributedString(string:"王小虎/研发部")
            contentAttribute.yy_color = blackTextColor82
            contentAttribute.yy_font = kMediumFont(12)
            hintAttribute.append(contentAttribute)
                obj.attributedText = hintAttribute
//            obj.text = "30分钟以前"
//            obj.font = kRegularFont(13)
//            obj.textColor = blacktextColor52
            obj.snp.makeConstraints { make in
                make.left.equalTo(outPointView.snp.right).offset(5)
                make.centerY.equalTo(outPointView)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "暂无数据"
            obj.textColor = blackTextColor33
            obj.font = kRegularFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalTo(lineView.snp.right).offset(5)
                make.top.equalTo(infosLabel.snp.bottom).offset(15)
            }
        }
    }
}
