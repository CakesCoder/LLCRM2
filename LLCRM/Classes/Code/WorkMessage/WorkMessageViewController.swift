//
//  WorkMessageViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class WorkMessageViewController: BaseViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "样品"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(WorkMessageViewController.backClick))

        // Do any additional setup after loading the view.
        
        view.backgroundColor = lineColor
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(WorkMessageTableViewCell.self, forCellReuseIdentifier: "WorkMessageTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(73)
            }
        })
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WorkMessageViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkMessageTableViewCell", for: indexPath) as! WorkMessageTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
