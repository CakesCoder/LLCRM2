//
//  WorkMessageTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/12.
//

import UIKit

class WorkMessageTableViewCell: UITableViewCell {
    
    var nameLabel:UILabel?
    var phoneLabel:UILabel?
    var dataLabel:UILabel?
    var timeLabel:UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "crm_notification")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
                make.width.height.equalTo(15)
            }
        }
        
        nameLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "商机跟进提醒"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor84
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(5)
//                make.centerX.equalTo(imageV)
                make.top.equalToSuperview().offset(15)
            }
        })
        
        phoneLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "请及时跟进商机显盛4020-FI6000W"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        dataLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            let hintAttribute = NSMutableAttributedString(string:"数据对象  ")
            hintAttribute.yy_color = blackTextColor84
            hintAttribute.yy_font = kMediumFont(13)
//            if dataList!.count > 0{
//                let personNameCotentAttribute = NSMutableAttributedString(string:model.productList?[0].maker ?? "--")
//                personNameCotentAttribute.yy_color = blackTextColor80
//                personNameCotentAttribute.yy_font = kMediumFont(13)
//                personNameHintAttribute.append(personNameCotentAttribute)
//                personNameLabel?.attributedText = personNameHintAttribute
//            }else{
                let cotentAttribute = NSMutableAttributedString(string: "显盛4020-FI6000w")
                cotentAttribute.yy_color = bluebgColor
                cotentAttribute.yy_font = kMediumFont(13)
                hintAttribute.append(cotentAttribute)
                obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.top.equalTo(phoneLabel!.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        timeLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "2022.05.16  09:30"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(dataLabel!.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(35)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        })
        
        
        _ = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_push")
//            obj.backgroundColor = .reds
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(12)
            }
        })
        
    }

}
