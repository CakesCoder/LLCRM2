//
//  TimeShowView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class TimeShowView: UIView {
    
    var cancelBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func cancelClick(){
        self.cancelBlock?()
    }
    
    func buildUI(){
        
        _ = UIView().then { obj in
            self.addSubview(obj)
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(336)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "选择有效期"
            obj.font = kAMediumFont(16)
            obj.textColor = blacktextColor34
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(21)
            }
        }
        
        let lineView = UIView().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = lineColor7
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(titleLabel.snp.bottom).offset(12)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        }
        
        
        let oneHourView = UIView().then { obj in
            bottomView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lineView.snp.bottom).offset(10)
                make.height.equalTo(40)
            }
        }

        _ = UIView().then({ obj in
            oneHourView.addSubview(obj)
            obj.backgroundColor = lineColor7

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        })

        let oneHourLabel = UILabel().then { obj in
            oneHourView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "1小时"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.centerY.equalToSuperview()
            }
        }
        
        let oneHourImageV = UIImageView().then { obj in
            oneHourView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "btnSelcted")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-45)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }

        let oneHourButton = UIButton().then { obj in
            oneHourView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let twoHourView = UIView().then { obj in
            bottomView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(oneHourView.snp.bottom).offset(10)
                make.height.equalTo(40)
            }
        }

        _ = UIView().then({ obj in
            twoHourView.addSubview(obj)
            obj.backgroundColor = lineColor7

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        })

        let twoHourLabel = UILabel().then { obj in
            twoHourView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "2小时"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.centerY.equalToSuperview()
            }
        }
        
        let twoHourImageV = UIImageView().then { obj in
            twoHourView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "btnSelcted")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-45)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }

        let twoHourButton = UIButton().then { obj in
            twoHourView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let fourHourView = UIView().then { obj in
            bottomView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(twoHourView.snp.bottom).offset(10)
                make.height.equalTo(40)
            }
        }

        _ = UIView().then({ obj in
            fourHourView.addSubview(obj)
            obj.backgroundColor = lineColor7

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        })

        let fourHourLabel = UILabel().then { obj in
            fourHourView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "4小时"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.centerY.equalToSuperview()
            }
        }
        
        let fourHourImageV = UIImageView().then { obj in
            fourHourView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "btnSelcted")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-45)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }

        let fourHourButton = UIButton().then { obj in
            fourHourView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let currentHourView = UIView().then { obj in
            bottomView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(fourHourView.snp.bottom).offset(10)
                make.height.equalTo(40)
            }
        }

        _ = UIView().then({ obj in
            currentHourView.addSubview(obj)
            obj.backgroundColor = lineColor7

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        })

        let currentHourLabel = UILabel().then { obj in
            currentHourView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "今天之内"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.centerY.equalToSuperview()
            }
        }
        
        let currentHourImageV = UIImageView().then { obj in
            currentHourView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "btnSelcted")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-45)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }

        let currentHourButton = UIButton().then { obj in
            currentHourView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }

        let cancelView = UIView().then { obj in
            bottomView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(currentHourView.snp.bottom).offset(0)
                make.height.equalTo(40)
            }
        }

        let cancelLabel = UILabel().then { obj in
            cancelView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "取消"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.center.equalTo(cancelView)
            }
        }

        let cancelButton = UIButton().then { obj in
            cancelView.addSubview(obj)
            obj.addTarget(self, action: #selector(cancelClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
    }

}
