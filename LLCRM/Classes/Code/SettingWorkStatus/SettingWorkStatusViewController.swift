//
//  SettingWorkStatusViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit
import JFPopup
class SettingWorkStatusViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "设置工作状态"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SettingWorkStatusViewController.blackClick))
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let statusView = UIView().then { obj in
            headView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(50)
            }
        }
        
        let statusTextFiled = UITextField().then { obj in
            statusView.addSubview(obj)
            obj.placeholder = "输入当前工作状态"
            obj.font = kMediumFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.right.equalToSuperview().offset(-20)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let tagView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(statusView.snp.bottom).offset(0)
                make.height.equalTo(140)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            tagView.addSubview(obj)
            obj.text = "快速选择"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor54
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        }
        
        let tags = ["疯狂忙碌中", "会议轰炸中", "外出公干中", "参加培训中"]
        
        let tagBottomView = UIView().then { obj in
            tagView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(hintLabel.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let w = (kScreenWidth-30)/3
        
        for i in 0..<tags.count{
            let y = i/3
            let x = i%3
            let bgView = UIView().then { obj in
                tagBottomView.addSubview(obj)
                obj.frame = CGRect(x: Int(w)*x, y: y*45, width: Int(w), height: 45)
//                obj.backgroundColor = .red
            }
            
            let tagView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.layer.cornerRadius = 15
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(15)
                    make.width.equalTo(100)
                    make.height.equalTo(30)
                }
            }
            
            let tagButton = UIButton().then { obj in
                tagView.addSubview(obj)
                obj.setTitle(tags[i], for: .normal)
                obj.titleLabel?.font = kMediumFont(12)
                obj.setTitleColor(blacktextColor34, for: .normal)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
        }
        
        let statusTimeView = UIView().then { obj in
            headView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(tagView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        let statusTimeHintLabel = UILabel().then { obj in
            statusTimeView.addSubview(obj)
            obj.text = "状态有限期"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor55
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UILabel().then { obj in
            statusTimeView.addSubview(obj)
            obj.text = "请选择"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor56
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(statusTimeHintLabel.snp.right).offset(20)
                make.right.equalToSuperview().offset(-20)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ = UIButton().then({ obj in
            statusTimeView.addSubview(obj)
            obj.addTarget(self, action: #selector(timeClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
    }
    
    @objc func timeClick(){
        self.popup.bottomSheet { [weak self] in
            let showView = TimeShowView()
            showView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            showView.cancelBlock = { [weak self] in
                self?.popup.dismissPopup()
            }
            return showView
        }
    }
    
    @objc func blackClick(){
        navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
