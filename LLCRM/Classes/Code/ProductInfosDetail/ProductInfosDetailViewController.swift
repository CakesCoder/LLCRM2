//
//  ProductInfosDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit
//import JXPagingView

extension ProductInfosDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ProductInfosDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ProductInfosDetailViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var model:PriceDetailModel?
    var presenter: ProductInfosDetailPresenterProtocols?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
//            obj.backgroundColor = .red
        }
        
        let baseInfos = [["*","报价名称："],["报价单号："],["*","来源商机："],["*","来源客户："],["*","报价金额："],["*","税率: "],["含税价格："],["联系人："],["联系人手机："],["添加产品："] ]

        
        let contentInfos = ["请输入报价名称", "自动生成", "请选择来源商机", "请选择", "请输入报价金额（元）", "请选择", "请输入产品总金额（元）", "请选择联系人", "请输入联系人手机", "电解电容"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                        
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(82)
                    make.height.equalTo(20)
                }
            }
            
            
//            if i == 2 || i == 3 || i == 5 || i == 7{
//                let tagImageV = UIImageView().then { obj in
//                    v.addSubview(obj)
//                    obj.backgroundColor = .red
//                    obj.snp.makeConstraints { make in
//                        make.right.equalToSuperview().offset(-15)
//                        make.centerY.equalToSuperview()
//                        make.width.height.equalTo(15)
//                    }
//                }
//            }
            
//            if i == contentInfos.count - 1{
//                let addLabel = UILabel().then { obj in
//                    v.addSubview(obj)
//                    obj.font = kMediumFont(14)
//                    obj.textColor = bluebgColor
//                    obj.text = "添加"
//                    obj.snp.makeConstraints { make in
//                        make.right.equalToSuperview().offset(-15)
//                        make.centerY.equalToSuperview()
//                    }
//                }
//            }
                let contentLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    obj.font = kMediumFont(12)
                    let dataList = model?.productList
                    obj.textColor = blackTextColor81
                    if i == 0{
                        obj.text = model?.clientName ?? "--"
                    }else if i == 1{
                        if dataList != nil && dataList?.count ?? 0 > 0{
                            obj.text = model?.productList?[0].productNo ?? "--"
                        }else{
                            obj.text = "--"
                        }
                        
                    }else if i == 2{
                        obj.text = "商机商机"
                    }else if i == 3{
                        obj.text = "主题主题主题主题主题主题主题"
                    }else if i == 4{
                        if dataList != nil && dataList?.count ?? 0 > 0{
                            obj.text = "￥" + (model?.price ?? "--")
                        }else{
                            obj.text = "--"
                        }
                    }else if i == 5{
                        if dataList != nil && dataList?.count ?? 0 > 0{
                            obj.text = (model?.productList?[0].taxRate ?? "--") + "%"
                        }else{
                            obj.text = "--"
                        }
                    }else if i == 6{
                        obj.text = "￥" + (model?.price ?? "--")
                    }else if i == 7{
                        obj.text = model?.contactName ?? "--"
                    }else if i == 8{
                        obj.text = model?.contactPhone ?? "--"
                    }else if i == 9{
                        if dataList != nil && dataList?.count ?? 0 > 0{
                            obj.text = model?.productList?[0].productName ?? "--"
                        }else{
                            obj.text = "--"
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalTo(hintLabel.snp.right).offset(15)
                        make.centerY.equalToSuperview()
                    }
                }
            
            if i == baseInfos.count - 2{
                let phoneImageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = .red
                    obj.snp.makeConstraints { make in
                        make.left.equalTo(contentLabel.snp.right).offset(10)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
            }

            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let attachemenetView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(baseInfos.count*45)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(102)
            }
        }
        
        let attacheMentHintLabel = UILabel().then { obj in
            attachemenetView.addSubview(obj)
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.text = "附件上传"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        }
        
//        for i in 0..<2{
//            let v = UIView().then { obj in
//                attachemenetView.addSubview(obj)
//                obj.backgroundColor = bgColor13
//                obj.layer.cornerRadius = 2
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.top.equalToSuperview().offset(43+36*i)
//                    make.right.equalToSuperview().offset(-100)
//                    make.height.equalTo(26)
//                }
//            }
//            
//            let imageV = UIImageView().then { obj in
//                v.addSubview(obj)
////                obj.backgroundColor = .blue
//                obj.image = UIImage(named: "flie_pdf")
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(14)
//                    make.centerY.equalToSuperview()
//                    make.width.height.equalTo(17)
//                }
//            }
//            
//            let namelabel = UILabel().then { obj in
//                v.addSubview(obj)
//                obj.text = "报价信息表.PDF"
//                obj.textColor = bluebgColor
//                obj.font = kMediumFont(12)
//                obj.snp.makeConstraints { make in
//                    make.left.equalTo(imageV.snp.right).offset(10)
//                    make.centerY.equalToSuperview()
//                }
//            }
//            
//            _ = UIImageView().then { obj in
//                v.addSubview(obj)
////                obj.backgroundColor = .blue
//                obj.image = UIImage(named: "flie_delete")
//                obj.snp.makeConstraints { make in
//                    make.right.equalToSuperview().offset(-14)
//                    make.centerY.equalToSuperview()
//                    make.width.height.equalTo(17)
//                }
//            }
//        }
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        cofing()
    }
    
    func cofing(){
        let router = ProductInfosDetailRouter()
        
        presenter = ProductInfosDetailPresenter()
        
        presenter?.router = router
        
        let entity = ProductInfosDetailEntity()
        
        let interactor = ProductInfosDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        presenter?.presenterRequestGetProductInfosDetail(by: ["":""])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductInfosDetailViewController: ProductInfosDetailViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetProductInfosetailPresenterReceiveData(by params: Any?) {
        printTest(params)
    }
    
    func didGetProductInfosDetailPresenterReceiveError(error: MyError?) {
        
    }
}
