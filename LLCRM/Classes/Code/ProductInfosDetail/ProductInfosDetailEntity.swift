//
//  ProductInfosDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/2.
//

import UIKit

class ProductInfosDetailEntity: ProductInfosDetailEntityProtocols {
    var interactor: ProductInfosDetailInteractorProtocols?
    
    func didProductInfosDetailReceiveData(by params: Any?) {
        interactor?.didEntityProductInfosDetailReceiveData(by: params)
    }
    
    func didProductInfosDetailReceiveError(error: MyError?) {
        interactor?.didEntityProductInfosDetailReceiveError(error: error)
    }
    
    func getProductInfosDetailRequest(by params: Any?) {
        LLNetProvider.request(.offerMaterialList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didProductInfosDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didProductInfosDetailReceiveError(error: .requestError)
            }
        }
    }
    

}
