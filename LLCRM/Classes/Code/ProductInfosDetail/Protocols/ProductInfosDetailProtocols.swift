//
//  ProductInfosDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class ProductInfosDetailProtocols: NSObject {

}

protocol ProductInfosDetailViewProtocols: AnyObject {
    var presenter: ProductInfosDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetProductInfosetailPresenterReceiveData(by params: Any?)
    func didGetProductInfosDetailPresenterReceiveError(error: MyError?)
}

protocol ProductInfosDetailPresenterProtocols: AnyObject{
    var view: ProductInfosDetailViewProtocols? { get set }
    var router: ProductInfosDetailRouterProtocols? { get set }
    var interactor: ProductInfosDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetProductInfosDetail(by params: Any?)
    func didGetProductInfosDetailInteractorReceiveData(by params: Any?)
    func didGetProductInfosDetailInteractorReceiveError(error: MyError?)
}

protocol ProductInfosDetailInteractorProtocols: AnyObject {
    var presenter: ProductInfosDetailPresenterProtocols? { get set }
    var entity: ProductInfosDetailEntityProtocols? { get set }
    
    func presenterRequestProductInfosDetail(by params: Any?)
    func didEntityProductInfosDetailReceiveData(by params: Any?)
    func didEntityProductInfosDetailReceiveError(error: MyError?)
}

protocol ProductInfosDetailEntityProtocols: AnyObject {
    var interactor: ProductInfosDetailInteractorProtocols? { get set }
    
    func didProductInfosDetailReceiveData(by params: Any?)
    func didProductInfosDetailReceiveError(error: MyError?)
    func getProductInfosDetailRequest(by params: Any?)
}

protocol ProductInfosDetailRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController)
}
