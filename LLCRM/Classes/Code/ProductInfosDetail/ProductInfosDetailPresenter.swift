//
//  ProductInfosDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/2.
//

import UIKit

class ProductInfosDetailPresenter: ProductInfosDetailPresenterProtocols {
    var view: ProductInfosDetailViewProtocols?
    
    var router: ProductInfosDetailRouterProtocols?
    
    var interactor: ProductInfosDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetProductInfosDetail(by params: Any?) {
        interactor?.presenterRequestProductInfosDetail(by: params)
    }
    
    func didGetProductInfosDetailInteractorReceiveData(by params: Any?) {
        view?.didGetProductInfosetailPresenterReceiveData(by: params)
    }
    
    func didGetProductInfosDetailInteractorReceiveError(error: MyError?) {
        view?.didGetProductInfosDetailPresenterReceiveError(error: error)
    }
    

}
