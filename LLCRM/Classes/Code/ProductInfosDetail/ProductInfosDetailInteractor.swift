//
//  ProductInfosDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/2.
//

import UIKit

class ProductInfosDetailInteractor: ProductInfosDetailInteractorProtocols {
    var presenter: ProductInfosDetailPresenterProtocols?
    
    var entity: ProductInfosDetailEntityProtocols?
    
    func presenterRequestProductInfosDetail(by params: Any?) {
        entity?.getProductInfosDetailRequest(by: params)
    }
    
    func didEntityProductInfosDetailReceiveData(by params: Any?) {
        presenter?.didGetProductInfosDetailInteractorReceiveData(by: params)
    }
    
    func didEntityProductInfosDetailReceiveError(error: MyError?) {
        presenter?.didGetProductInfosDetailInteractorReceiveError(error: error)
    }
    

}
