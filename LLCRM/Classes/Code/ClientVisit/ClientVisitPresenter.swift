//
//  ClientVisitPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit

class ClientVisitPresenter: ClientVisitPresenterProtocols {
    var view: ClientVisitViewProtocols?
    
    var router: ClientVisitRouterProtocols?
    
    var interactor: ClientVisitInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
