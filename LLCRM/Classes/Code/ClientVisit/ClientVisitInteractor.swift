//
//  ClientVisitInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit

class ClientVisitInteractor: ClientVisitInteractorProtocols {
    var presenter: ClientVisitPresenterProtocols?
    
    var entity: ClientVisitEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
    

}
