//
//  ClientVisitLocalTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit
import PKHUD

class ClientVisitLocalTableViewCell: UITableViewCell {
    
    var addClientBlock: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        build()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func build(){
        self.selectionStyle = .none
        
        let addView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(113)
            }
        }
        
        let addTagImageV = UIImageView().then { obj in
            addView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "apply_go")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
                make.width.height.equalTo(16)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            addView.addSubview(obj)
            obj.text = "外勤对象"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor8
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(addTagImageV.snp.right).offset(3)
                make.centerY.equalTo(addTagImageV)
            }
        }
        
        _ = UILabel().then { obj in
            addView.addSubview(obj)
            obj.text = "客户"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor8
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(addTagImageV)
            }
        }
        
        let addButtonView = UIView().then { obj in
            addView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 1
            obj.layer.borderColor = lineColor5.cgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(hintLabel.snp.bottom).offset(20)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
        let addButtonTagImageV = UIImageView().then { obj in
            addButtonView.addSubview(obj)
            obj.backgroundColor = .red
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(13)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(13)
            }
        }
        
        let addButtonHintLabel = UILabel().then { obj in
            addButtonView.addSubview(obj)
            obj.text = "添加客户"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor63
            obj.snp.makeConstraints { make in
                make.left.equalTo(addButtonTagImageV.snp.right).offset(2)
                make.centerY.equalToSuperview()
            }
        }
        
        let addButtonPushImageV = UIImageView().then { obj in
            addButtonView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
//                make.width.height.equalTo(13)
            }
        }
        
        let addClientButton = UIButton().then { obj in
            addButtonView.addSubview(obj)
            obj.addTarget(self, action: #selector(addClientClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let localView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 2
            obj.snp.makeConstraints { make in
                make.top.equalTo(addView.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(60)
            }
        }
        
        let localImageV = UIImageView().then { obj in
            localView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "apply_local")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(12)
                make.top.equalToSuperview().offset(10)
                make.width.equalTo(8)
                make.height.equalTo(10)
            }
        }
        
        let localLabel = UILabel().then { obj in
            localView.addSubview(obj)
            obj.text = "南山区坪山一路2号大园工业区"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor63
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(localImageV.snp.right).offset(2)
                make.centerY.equalTo(localImageV)
            }
        }
        
        
        let localButton = UIButton().then { obj in
            localView.addSubview(obj)
            obj.setTitle("重新定位", for: .normal)
            obj.setTitleColor(blackTextColor64, for: .normal)
            obj.titleLabel?.font = kRegularFont(12)
            obj.addTarget(self, action: #selector(updateLoacalClick(_ :)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let qiandaoButton = UIButton().then { obj in
            localView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = bluebgColor
            obj.setTitle("签到", for: .normal)
            obj.titleLabel?.font = kRegularFont(11)
            obj.addTarget(self, action: #selector(qiandaoClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.centerY.equalToSuperview()
                make.width.equalTo(70)
                make.height.equalTo(30)
            }
        }
    }
    
    @objc func qiandaoClick(_ button:UIButton){
        HUD.flash(.label("待开发"), delay: 2)
    }
    
    @objc func updateLoacalClick(_ button:UIButton){
        HUD.flash(.label("待开发"), delay: 2)
    }
    
    @objc func addClientClick(_ button: UIButton){
        self.addClientBlock!()
    }
}
