//
//  ClientVisitRemarkTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit
import YYText_swift


class ClientVisitRemarkTableViewCell: UITableViewCell {
    var numLabel:UILabel?
    private var isLocationToEnd: Bool = false
    var maxWordsCount:Int = 1000
    var pictureView: UIView?
    var addbutton:UIButton?
    var addHintLabel:UILabel?
    var addHintLabel2:UILabel?
    
    var pictureBlcok: (() -> ())?
    
    var images:[Any]?{
        didSet{
            if images!.count > 0{
                for view in pictureView?.subviews as! [UIView]{
                    if view is UIButton{
                        continue
                    }
                    view.removeFromSuperview()
                    
                }
                let w = (kScreenWidth-30)/4-5
                for i in 0..<(images!.count){
                    _ = UIImageView().then { obj in
                        pictureView?.addSubview(obj)
                        obj.image = (images![i] as! UIImage)
                        obj.frame = CGRect(x: 5.0*CGFloat(i)+CGFloat(w)*CGFloat(i), y: 0, width: w, height: w+5)
                    }
                }
                addbutton!.frame = CGRect(x: (5+w)*CGFloat(images!.count), y: 0, width: w, height: w+5)
                addHintLabel!.snp.updateConstraints { make in
                    make.left.equalTo(addbutton!.snp.right).offset(5)
//                    make.top.equalToSuperview().offset(5)
                }
                addHintLabel2!.snp.updateConstraints { make in
                    make.left.equalTo(addbutton!.snp.right).offset(5)
//                    make.top.equalToSuperview().offset(18)
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let remarkView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(10)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }

        let remarkHintLabel = UILabel().then { obj in
            remarkView.addSubview(obj)
            obj.text = "执行外勤动作"
            obj.textColor = blackTextColor62
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalToSuperview().offset(15)
            }
        }

        let lineView = UIView().then { obj in
            remarkView.addSubview(obj)
            obj.backgroundColor = lineColor22
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(remarkHintLabel.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }

        let remarkLabel = UILabel().then { obj in
            remarkView.addSubview(obj)

            let attri = NSMutableAttributedString(string:"*")
            attri.yy_color = blackTextColor27

            let titleText = "备注"
            let userAttri = NSMutableAttributedString(string: titleText)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kMediumFont(14)
            attri.append(userAttri)

            obj.attributedText = attri
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(lineView.snp.bottom).offset(10)
            }
        }

        let inputView = UIView().then { obj in
            remarkView.addSubview(obj)
            obj.backgroundColor = bgColor6
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor13.cgColor

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(remarkLabel.snp.bottom).offset(10)
                make.height.equalTo(150)
            }
        }

        let textView = YYTextView().then { obj in
            inputView.addSubview(obj)
            obj.placeholderText = "请输入"
            obj.placeholderTextColor = blackTextColor3
            obj.placeholderFont = kRegularFont(14)
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }

        numLabel = UILabel().then({ obj in
            inputView.addSubview(obj)
            obj.textColor = blackTextColor28
            obj.font = kAMediumFont(14)
            obj.text = "0/1000"
            obj.snp.makeConstraints { make in
                make.right.bottom.equalToSuperview().offset(-15)
            }
        })

        let pictureLabel = UILabel().then { obj in
            remarkView.addSubview(obj)

            let attri = NSMutableAttributedString(string:"*")
            attri.yy_color = blackTextColor27

            let titleText = "图片"
            let userAttri = NSMutableAttributedString(string: titleText)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kMediumFont(14)
            attri.append(userAttri)

            obj.attributedText = attri
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(inputView.snp.bottom).offset(10)
            }
        }


        let w = (kScreenWidth-30)/4
        pictureView = UIView().then { obj in
            remarkView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(pictureLabel.snp.bottom).offset(10)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(w)
            }
        }

        addbutton = UIButton().then { obj in
            pictureView?.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.titleLabel?.font = kMediumFont(25)
            obj.setTitleColor(.black, for: .normal)
            obj.layer.cornerRadius = 3
            obj.layer.borderWidth = 1
            obj.layer.borderColor = lineColor23.cgColor
            obj.frame = CGRect(x: 5, y: 0, width: w, height: w)
            obj.addTarget(self, action:#selector(addPictureClick(_ :)), for: .touchUpInside)
        }

        addHintLabel = UILabel().then({ obj in
            pictureView?.addSubview(obj)
            obj.text = "上传照片"
            obj.font = kMediumFont(9)
            obj.textColor = blackTextColor65
            obj.snp.makeConstraints { make in
                make.left.equalTo(addbutton!.snp.right).offset(5)
                make.top.equalToSuperview().offset(5)
            }
        })

        addHintLabel2 = UILabel().then({ obj in
            pictureView?.addSubview(obj)
            obj.text = "大小不超过3M"
            obj.font = kMediumFont(9)
            obj.textColor = blackTextColor36
            obj.snp.makeConstraints { make in
                make.left.equalTo(addbutton!.snp.right).offset(5)
                make.top.equalToSuperview().offset(18)
            }
        })
    }
    
    @objc func addPictureClick(_ button:UIButton){
        self.pictureBlcok!()
    }

}

extension ClientVisitRemarkTableViewCell: YYTextViewDelegate{
    
    func textViewDidChange(_ textView: YYTextView) {
        numLabel?.text = "\(textView.text.count)/1000"
    }
    
    func textView(_ textView: YYTextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "" { // 删除字数
            return true
        }
            
        let inputMode = UITextInputMode.activeInputModes.first?.primaryLanguage
        if inputMode == "zh-Hans" || inputMode == "zh-Hant" { // 简体/繁体中文输入法(拼音、手写输入等)
            if let selectedRange = textView.markedTextRange,
                let _ = textView.position(from: selectedRange.start, offset: 0) { // 如果有高亮的文本，如拼音、手写
                // 拼音输入：联想的过程中，location时刻变化(高亮文本的尾部位置)，length始终为0
                // 手写输入：联想的过程中不会回调该代理方法，确认联想词后才会进入该代理方法
                if range.length > 0 { // 选中联想词后，location变回高亮文本的起始位置，length为高亮文本的长度，需要对字数限制字数
                    return !limitMaxWordsCount(textView, replacementText: text, isLocationToEnd: false)
                }
            } else { // 没有高亮选择的字，对已输入的文字进行字数限制
                if range.location >= maxWordsCount {
    //                        SVProgressHUD.showError(withStatus: "最多可输入\(maxWordsCount)字")
                    return false
                } else { // 处理粘贴引起的字数超限, 截取文本后需要自己去移动光标到尾部
                    return !limitMaxWordsCount(textView, replacementText: text, isLocationToEnd: true)
                }
            }
        } else { // 中文输入法以外的输入法直接进行字数限制
            if range.location >= maxWordsCount {
    //                    SVProgressHUD.showError(withStatus: "最多可输入\(maxWordsCount)字")
                return false
            } else { // 处理粘贴引起的字数超限, 截取文本后需要自己去移动光标到尾部
                return !limitMaxWordsCount(textView, replacementText: text, isLocationToEnd: true)
            }
        }
        return true
    }
    
    func textViewDidChangeSelection(_ textView: YYTextView) {
        let text = textView.text
        if isLocationToEnd, text.utf16.count >= maxWordsCount {
            textView.selectedRange = NSRange(location: maxWordsCount, length: 0)
            isLocationToEnd = false
        }
    }
    
    func limitMaxWordsCount(_ textView: YYTextView, replacementText text: String, isLocationToEnd: Bool) -> Bool {
        let string = textView.text + text
        if string.utf16.count > maxWordsCount {
            textView.text = String(string.prefix(maxWordsCount))
            self.isLocationToEnd = isLocationToEnd
            return true
        }
        return false
    }
}
