//
//  ClientVisitRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit

class ClientVisitRouter: ClientVisitRouterProtocols {
    func pushToMyClient(from previousView: UIViewController) {
        let vc = MyClientViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
}
