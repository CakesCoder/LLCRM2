//
//  ClientVisitViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

import ZLPhotoBrowser
import Photos
import PKHUD

class ClientVisitViewController: BaseViewController {
    
    var images:[Any]? = []
    var presenter: ClientVisitPresenterProtocols?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "客户拜访"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ClientVisitViewController.backClick))
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let footView = UIView().then { obj in
            view.addSubview(obj)
//            obj.backgroundColor = .red
            obj.frame = CGRectMake(0, 0, kScreenWidth, 200)
        }
        
        let commitButton = UIButton().then { obj in
            footView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("完成", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.layer.cornerRadius = 2
            obj.addTarget(self, action: #selector(commitClick(_ :)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(80)
                make.width.equalTo(kScreenWidth-30)
                make.height.equalTo(40)
            }
        }
        
        _ = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.ignoreScrollingAdjustment = true
//            obj.restoreContentOffset = true
            obj.register(ClientVisitLocalTableViewCell.self, forCellReuseIdentifier: "ClientVisitLocalTableViewCell")
            obj.register(ClientVisitRemarkTableViewCell.self, forCellReuseIdentifier: "ClientVisitRemarkTableViewCell")
            obj.tableFooterView = footView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = ClientVisitRouter()
        
        presenter = ClientVisitPresenter()
        presenter?.router = router
        
        let entity = ClientVisitEntity()
        
        let interactor = ClientVisitInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
    }
    
    @objc func commitClick(_ button:UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientVisitViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 195
        }
        return 396
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClientVisitRemarkTableViewCell", for: indexPath) as! ClientVisitRemarkTableViewCell
            cell.images = self.images
            cell.pictureBlcok = {
                let ps = ZLPhotoPreviewSheet()
                ps.selectImageBlock = { [weak self] results, isOriginal in
                    // your code
                    if (self?.images!.count)! > 1{
                        HUD.flash(.label("图片最多2张"), delay: 2)
                    }else{
                        self?.images?.append(results[0].image)
                        tableView.reloadData()
                    }
                }
                ps.showPreview(animate: true, sender: self)
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientVisitLocalTableViewCell", for: indexPath) as! ClientVisitLocalTableViewCell
        cell.addClientBlock = {
            self.presenter?.router?.pushToMyClient(from: self)
        }
        return cell
    }
}

extension ClientVisitViewController: ClientVisitViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterReceiveData() {
        
    }
    
    func didPresenterReceiveError() {
        
    }
}
