//
//  ClientVisitRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/19.
//

import UIKit

protocol ClientVisitViewProtocols: AnyObject {
    var presenter: ClientVisitPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol ClientVisitPresenterProtocols: AnyObject{
    var view: ClientVisitViewProtocols? { get set }
    var router: ClientVisitRouterProtocols? { get set }
    var interactor: ClientVisitInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol ClientVisitInteractorProtocols: AnyObject {
    var presenter: ClientVisitPresenterProtocols? { get set }
    var entity: ClientVisitEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol ClientVisitEntityProtocols: AnyObject {
    var interactor: ClientVisitInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol ClientVisitRouterProtocols: AnyObject {
    func pushToMyClient(from previousView: UIViewController)
}
