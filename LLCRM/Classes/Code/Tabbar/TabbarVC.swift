//
//  TabbarVC.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/11.
//

import UIKit
import ESTabBarController_swift

class TabbarVC: ESTabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let imageV = UIImageView()
        imageV.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: UIDevice.xp_tabBarFullHeight())
        imageV.backgroundColor = .white
        self.tabBar.insertSubview(imageV, at: 0)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

class JZTabBarBasicContentView: ESTabBarItemContentView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        renderingMode = UIImage.RenderingMode.automatic
    }
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class JZTabBarBouncesContentView: JZTabBarBasicContentView {
    
    public var duration = 0.3
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func selectAnimation(animated: Bool, completion: (() -> ())?) {
//        self.bounceAnimation()
        completion?()
    }
    
    override func reselectAnimation(animated: Bool, completion: (() -> ())?) {
//        self.bounceAnimation()
        completion?()
    }
    
    func bounceAnimation() {
        let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        impliesAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        impliesAnimation.duration = duration * 2
        impliesAnimation.calculationMode = CAAnimationCalculationMode.cubic
        imageView.layer.add(impliesAnimation, forKey: nil)
    }
}


class JZTabBarIrregularityBasicContentView: JZTabBarBouncesContentView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        textColor = UIColor(0x999999)
        highlightTextColor = UIColor(0xC7000A)
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

class JZTabBarIrregularityContentView: ESTabBarItemContentView {
    
    public init() {
        super.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth/5, height: kScreenWidth/5))
//        let width = PItem.circleWidth
//        super.init(frame: CGRect(0, 0, width, width))
//
//        self.layer.borderWidth = 1
//        self.layer.borderColor = UIColor.init(white: 235 / 255.0, alpha: 1.0).cgColor
//        self.layer.cornerRadius = width/2
//        self.clipsToBounds = true
//
//        self.insets = UIEdgeInsets.init(top: -24, left: 0, bottom: 0, right: 0)
//
//        self.superview?.bringSubviewToFront(self)
//        self.addSubview(PItem)
//
//        textColor = UIColor.init(white: 255.0 / 255.0, alpha: 1.0)
//        highlightTextColor = UIColor.init(white: 255.0 / 255.0, alpha: 1.0)
//
//        backdropColor = .white
//        highlightBackdropColor = .white
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func updateLayout() {
        super.updateLayout()
        self.imageView.sizeToFit()
        self.imageView.center = CGPoint.init(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
    }
    
    public override func selectAnimation(animated: Bool, completion: (() -> ())?) {
        let view = UIView.init(frame: CGRect.init(origin: CGPoint.zero, size: CGSize(width: 2.0, height: 2.0)))
        view.layer.cornerRadius = 1.0
        view.layer.opacity = 0.5
        view.backgroundColor = UIColor.init(red: 10/255.0, green: 66/255.0, blue: 91/255.0, alpha: 1.0)
        self.addSubview(view)
        playMaskAnimation(animateView: view, target: self.imageView, completion: {
            [weak view] in
            view?.removeFromSuperview()
            completion?()
        })
    }
    
    public override func reselectAnimation(animated: Bool, completion: (() -> ())?) {
        completion?()
    }
    
    public override func deselectAnimation(animated: Bool, completion: (() -> ())?) {
        completion?()
    }
    
    public override func highlightAnimation(animated: Bool, completion: (() -> ())?) {
        bounceAnimation()
    }
    
    public override func dehighlightAnimation(animated: Bool, completion: (() -> ())?) {
        bounceAnimation()
    }
    
    func bounceAnimation() {
        let impliesAnimation = CAKeyframeAnimation(keyPath: "transform.scale")
        impliesAnimation.values = [1.0 ,1.4, 0.9, 1.15, 0.95, 1.02, 1.0]
        impliesAnimation.duration = 0.6
        impliesAnimation.calculationMode = CAAnimationCalculationMode.cubic
//        PItem.imageView.layer.add(impliesAnimation, forKey: nil)
    }
    
    private func playMaskAnimation(animateView view: UIView, target: UIView, completion: (() -> ())?) {
    }
    
}

