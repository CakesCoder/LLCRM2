//
//  AddClientSelectedPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/14.
//

import UIKit

class AddClientSelectedPresenter: AddClientSelectedPresenterProtocols {
   
    var view: AddClientSelectedViewProtocols?
    
    var router: AddClientSelectedRouterProtocols?
    
    var interactor: AddClientSelectedInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddClientSelected(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddClientSelected(by: params)
    }
    
    func didGetAddClientSelectedInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddClientSelectedPresenterReceiveData(by: params)
    }
    
    func didGetAddClientSelectedInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientSelectedPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientCharge(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddClientCharge(by: params)
    }
    
    func didGetAddClientChargeInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddClientChargePresenterReceiveData(by: params)
    }
    
    func didGetAddClientChargeInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientChargePresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientDepartMent(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddClientDepartMent(by: params)
    }
    
    func didGetAddClientDepartMentInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddClientDepartMentPresenterReceiveData(by: params)
    }
    
    func didGetAddClientDepartMentInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientDepartMentPresenterReceiveError(error: error)
    }
    
    
    func presenterRequestGetAddClientArchives(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientArchives(by: params)
    }
    
    func didGetAddClientArchivesInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientArchivesPresenterReceiveData(by: params)
    }
    
    func didGetAddClientArchivesInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientArchivesPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientPricePerson(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientPricePerson(by: params)
    }
    
    func didGetAddClientPricePersonInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientPricePersonPresenterReceiveData(by: params)
    }
    
    func didGetAddClientPricePersonInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientPricePersonPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientBussinessNumber(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientBussinessNumber(by: params)
    }
    
    func didGetAddClientBussinessNumberInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientBussinessNumberPresenterReceiveData(by: params)
    }
    
    func didGetAddClientBussinessNumberInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientBussinessNumberPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientBussinessClientName(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientBussinessClientName(by: params)
    }
    
    func didGetAddClientBussinessClientNameInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientBussinessClientNamePresenterReceiveData(by: params)
    }
    
    func didGetAddClientBussinessClientNameInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientBussinessClientNamePresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientBussinessNature(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientBussinessNature(by: params)
    }
    
    func didGetAddClientBussinessNatureInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientBussinessClientNaturePresenterReceiveData(by: params)
    }
    
    func didGetAddClientBussinessNatureInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientBussinessClientNaturePresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientBussinessPhase(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientBussinessPhase(by: params)
    }
    
    func didGetAddClientBussinessPhaseInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientBussinessClientPhasePresenterReceiveData(by: params)
    }
    
    func didGetAddClientBussinessPhaseInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientBussinessClientPhasePresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientBussinessFlower(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientBussinessFlower(by: params)
    }
    
    func didGetAddClientBussinessFlowerInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientBussinessClientFlowerPresenterReceiveData(by: params)
    }
    
    func didGetAddClientBussinessFlowerInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientBussinessClientFlowerPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientBussinessStatus(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestAddClientBussinessStatus(by: params)
    }
    
    func didGetAddClientBussinessStatusInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetAddClientBussinessClientStatusPresenterReceiveData(by: params)
    }
    
    func didGetAddClientBussinessStatusInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientBussinessClientStatusPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientSellLeads(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientSellLeads(by: params)
    }
    
    func didGetAddClientSellLeadsInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientSellLeadsPresenterReceiveData(by: params)
    }
    
    func didGetAddClientSellLeadsInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientSellLeadsPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientSampleTechnology(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientSampleTechnology(by: params)
    }
    
    func didGetAddClientSampleTechnologyInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientSampleTechnologyPresenterReceiveData(by: params)
    }
    
    func didGetAddClientSampleTechnologyInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientSampleTechnologyPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientProductName(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientProductName(by: params)
    }
    
    func didGetAddClientProductNameInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientProductNamePresenterReceiveData(by: params)
    }
    
    func didGetAddClientProductNameInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientProductNameyPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetAddClientSampleCode(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientSampleCode(by: params)
    }
    
    func didGetAddClientSampleCodeInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientSampleCodePresenterReceiveData(by: params)
    }
    
    func didGetAddClientSampleCodeInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientSampleCodePresenterReceiveError(error: error)
    }
    
    func presenterRequestAddClientCertificationAsk(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientCertificationAsk(by: params)
    }
    
    func didEntityAddClientCertificationAskReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientCertificationAskPresenterReceiveData(by: params)
    }
    
    func didEntityAddClientCertificationAskReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientCertificationAskPresenterReceiveError(error: error)
    }
    
    func presenterRequestAddClientVisitMyClient(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestAddClientVisitMyClient(by: params)
    }
    
    func didEntityAddClientVisitMyClientReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetAddClientVisitMyClientPresenterReceiveData(by: params)
    }
    
    func didEntityAddClientVisitMyClientReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetAddClientVisitMyClientPresenterReceiveError(error: error)
    }
    
//    func presenterRequestGetAddClientInfos(by params: Any?) {
//        view?.showLoading()
//        interactor?.presenterRequestAddClientInfos(by: params)
//    }
//
//    func didGetAddClientInfosInteractorReceiveData(by params: Any?) {
//        view?.hideLoading()
//        view?.didGetAddClientInfosDetailPresenterReceiveData(by: params)
//    }
//
//    func didGetAddClientInfosInteractorReceiveError(error: MyError?) {
//        view?.hideLoading()
//        view?.showError()
//        view?.didGetAddClientInfosDetailPresenterReceiveError(error: error)
//    }
    
}
