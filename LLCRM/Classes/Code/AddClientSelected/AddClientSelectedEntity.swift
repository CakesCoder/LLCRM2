//
//  AddClientSelectedEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/14.
//

import UIKit

class AddClientSelectedEntity: AddClientSelectedEntityProtocols {
    var interactor: AddClientSelectedInteractorProtocols?
    
    func didAddClientSelectedReceiveData(by params: Any?) {
        interactor?.didEntityAddClientSelectedReceiveData(by: params)
    }
    
    func didAddClientSelectedReceiveError(error: MyError?) {
        interactor?.didEntityAddClientSelectedReceiveError(error: error)
    }
    
    func getAddClientSelectedRequest(by params: Any?) {
        LLNetProvider.request(.clientSelectConfig(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientSelectedReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientSelectedReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientChargeReceiveData(by params: Any?) {
        interactor?.didEntityAddClientChargeReceiveData(by: params)
    }
    
    func didAddClientChargeReceiveError(error: MyError?) {
        interactor?.didEntityAddClientChargeReceiveError(error: error)
    }
    
    func getAddClientChargeRequest(by params: Any?) {
        LLNetProvider.request(.addClientSelected(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientChargeReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientChargeReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientDepartMentReceiveData(by params: Any?) {
        interactor?.didEntityAddClientDepartMentReceiveData(by: params)
    }
    
    func didAddClientDepartMentReceiveError(error: MyError?) {
        interactor?.didEntityAddClientDepartMentReceiveError(error: error)
    }
    
    func getAddClientDepartMentRequest(by params: Any?) {
        LLNetProvider.request(.addClientDepartMent(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientDepartMentReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientDepartMentReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientArchivesReceiveData(by params: Any?) {
        interactor?.didEntityAddClientArchivesReceiveData(by: params)
    }
    
    func didAddClientArchivesReceiveError(error: MyError?) {
        interactor?.didEntityAddClientArchivesReceiveError(error: error)
    }
    
    func getAddClientArchivesRequest(by params: Any?) {
        LLNetProvider.request(.addClientArchives(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientArchivesReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientArchivesReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientPricePersonReceiveData(by params: Any?) {
        interactor?.didEntityAddClientPricePersonReceiveData(by: params)
    }
    
    func didAddClientPricePersonReceiveError(error: MyError?) {
        interactor?.didEntityAddClientPricePersonReceiveError(error: error)
    }
    
    func getAddClientPriccecPersonRequest(by params: Any?) {
        LLNetProvider.request(.addClientPricePerson(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientPricePersonReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientPricePersonReceiveError(error: .requestError)
            }
        }
    }

    func didAddClientBussinessNumberReceiveData(by params: Any?) {
        interactor?.didEntityAddClientBussinessNumberReceiveData(by: params)
    }
    
    func didAddClientBussinessNumberReceiveError(error: MyError?) {
        interactor?.didEntityAddClientBussinessNumberReceiveError(error: error)
    }
    
    func getAddClientBussinessNumberRequest(by params: Any?) {
        LLNetProvider.request(.addClientBussinessNumber(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientBussinessNumberReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientBussinessNumberReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientBussinessClientNameReceiveData(by params: Any?){
        interactor?.didEntityAddClientBussinessClientNameReceiveData(by: params)
    }
    
    func didAddClientBussinessClientNameReceiveError(error: MyError?){
        interactor?.didEntityAddClientBussinessClientNameReceiveError(error: error)
    }
    
    func getAddClientBussinessClientNameRequest(by params: Any?){
        LLNetProvider.request(.addClientBussinessClientName(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientBussinessClientNameReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientBussinessClientNameReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientBussinessNatureReceiveData(by params: Any?){
        interactor?.didEntityAddClientBussinessNatureReceiveData(by: params)
    }
    
    func didAddClientBussinessNatureReceiveError(error: MyError?){
        interactor?.didEntityAddClientBussinessNatureReceiveError(error: error)
    }
    
    func getAddClientBussinessNatureRequest(by params: Any?){
        LLNetProvider.request(.addClientBussinessNature(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientBussinessNatureReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientBussinessNatureReceiveError(error: .requestError)
            }
        }
    }
    
//    func didAddClientBussinessPhaseReceiveData(by params: Any?){
//        interactor?.didEntityAddClientBussinessPhaseReceiveData(by: params)
//    }
//    
//    func didAddClientBussinessPhaseReceiveError(error: MyError?){
//        interactor?.didEntityAddClientBussinessPhaseReceiveError(error: error)
//    }
//    
//    func getAddClientBussinessPhaseRequest(by params: Any?){
//        LLNetProvider.request(.addClientBussinessPhase(params as! [String : Any])) { result in
//        switch result{
//            case let .success(response):
//                let data = response.data
//                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
//                self.didAddClientBussinessPhaseReceiveData(by:jsonData)
//            case .failure(_):
//                self.didAddClientBussinessPhaseReceiveError(error: .requestError)
//            }
//        }
//    }
    
    func didAddClientBussinessPhaseReceiveData(by params: Any?){
        interactor?.didEntityAddClientBussinessPhaseReceiveData(by: params)
    }
    
    func didAddClientBussinessPhaseReceiveError(error: MyError?){
        interactor?.didEntityAddClientBussinessPhaseReceiveError(error: error)
    }
    
    func getAddClientBussinessPhaseRequest(by params: Any?){
        LLNetProvider.request(.addClientBussinessPhase(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientBussinessPhaseReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientBussinessPhaseReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientBussinessFlowerReceiveData(by params: Any?){
        interactor?.didEntityAddClientBussinessFlowerReceiveData(by: params)
    }

    func didAddClientBussinessFlowerReceiveError(error: MyError?){
        interactor?.didEntityAddClientBussinessFlowerReceiveError(error: error)
    }

    func getAddClientBussinessFlowerRequest(by params: Any?){
        LLNetProvider.request(.addClientBussinessFlower(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientBussinessFlowerReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientBussinessFlowerReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientBussinessStatusReceiveData(by params: Any?){
        interactor?.didEntityAddClientBussinessStatusReceiveData(by: params)
    }

    func didAddClientBussinessStatusReceiveError(error: MyError?){
        interactor?.didEntityAddClientBussinessStatusReceiveError(error: error)
    }

    func getAddClientBussinessStatusRequest(by params: Any?){
        LLNetProvider.request(.addClientBussinessCommitStatus(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientBussinessStatusReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientBussinessStatusReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientSellLeadsReceiveData(by params: Any?){
        interactor?.didEntityAddClientSellLeadsReceiveData(by: params)
    }

    func didAddClientSellLeadsReceiveError(error: MyError?){
        interactor?.didEntityAddClientSellLeadsReceiveError(error: error)
    }

    func getAddClientSellLeadsRequest(by params: Any?){
        LLNetProvider.request(.addClientSellLeads(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientSellLeadsReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientSellLeadsReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientSampleTechnologyReceiveData(by params: Any?){
        interactor?.didEntityAddClientSellLeadsReceiveData(by: params)
    }

    func didAddClientSampleTechnologyReceiveError(error: MyError?){
        interactor?.didEntityAddClientSellLeadsReceiveError(error: error)
    }

    func getAddClientSampleTechnologyRequest(by params: Any?){
        LLNetProvider.request(.addClientSampleTechnology(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientSampleTechnologyReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientSampleTechnologyReceiveError(error: .requestError)
            }
        }
    }
    
    
    func getAddClientProductNameRequest(by params: Any?) {
        LLNetProvider.request(.getPriceProductList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientProductNameReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientProductNameReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientProductNameReceiveData(by params: Any?){
        interactor?.didEntityAddClientProductNameReceiveData(by: params)
    }

    func didAddClientProductNameReceiveError(error: MyError?){
        interactor?.didEntityAddClientProductNameReceiveError(error: error)
    }
    
    func getAddClientSampleCodeRequest(by params: Any?) {
        LLNetProvider.request(.addClientSamleCode(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientSampleCodeReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientSampleCodeReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientSampleCodeReceiveData(by params: Any?){
        interactor?.didEntityAddClientSampleCodeReceiveData(by: params)
    }

    func didAddClientSampleCodeReceiveError(error: MyError?){
        interactor?.didEntityAddClientSampleCodeReceiveError(error: error)
    }
    
    func getAddClientCertificationAskRequest(by params: Any?) {
        LLNetProvider.request(.addClientCertificationAsk(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientCertificationAskReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientCertificationAskReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientCertificationAskReceiveData(by params: Any?){
        interactor?.didEntityAddClientCertificationAskReceiveData(by: params)
    }

    func didAddClientCertificationAskReceiveError(error: MyError?){
        interactor?.didEntityAddClientCertificationAskReceiveError(error: error)
    }
    
    func getAddClientVisitMyClientRequest(by params: Any?) {
        LLNetProvider.request(.addClientVisitMyClient(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddClientVisitMyClientReceiveData(by:jsonData)
            case .failure(_):
                self.didAddClientVisitMyClientReceiveError(error: .requestError)
            }
        }
    }
    
    func didAddClientVisitMyClientReceiveData(by params: Any?){
        interactor?.didEntityAddClientVisitMyClientReceiveData(by: params)
    }

    func didAddClientVisitMyClientReceiveError(error: MyError?){
        interactor?.didEntityAddClientVisitMyClientReceiveError(error: error)
    }
    
//    func didGetAddClientInfosReceiveData(by params: Any?) {
//        interactor?.didAddClientInfosReceiveData(by: params)
//    }
//
//    func didGetAddClientInfosReceiveError(error: MyError?) {
//        interactor?.didAddClientInfosReceiveError(error: error)
//    }
//
//    func getAddClientInfostRequest(by params: Any?) {
//        LLNetProvider.request(.getAddClientInfos(params as! [String : Any])) { result in
//        switch result{
//            case let .success(response):
//                let data = response.data
//                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
//                self.didGetAddClientInfosReceiveData(by:jsonData)
//            case .failure(_):
//                self.didGetAddClientInfosReceiveError(error: .requestError)
//            }
//        }
//    }
}
