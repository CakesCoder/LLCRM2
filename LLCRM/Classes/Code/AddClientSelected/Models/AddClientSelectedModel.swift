//
//  AddClientSelectedModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/14.
//

import UIKit

class AddClientSelectedListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedModel]?
    var message:String?
        
    required init() {}
}


class AddClientSelectedModel: BaseModel {
    var attrName:String?
    var attrTypeL:String?
    var id:String?
    var isSelected:Bool? = false
    var attrNum:Int?
        
    required init() {}
}

class AddClientSelectedPersonListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedPersonModel]?
    var message:String?
        
    required init() {}
}


class AddClientSelectedPersonModel: BaseModel {
    var name:String?
    var deptId:String?
    var companyId:String?
    var userId:String?
    var userName:String?
    var isSelected:Bool? = false
    required init() {}
}

class AddClientSelectedDepartMentListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedDepartMentModel]?
    var message:String?
        
    required init() {}
}


class AddClientSelectedDepartMentModel: BaseModel {
    var createName:String?
    var createTime:String?
    var companyId:String?
    var userId:String?
    var deleted:Int?
    var createUser:String?
    var deptName:String?
    var id:String?
    var parentId:String?
    var isSelected:Bool? = false
    var name:String?
    required init() {}
}

class AddClientSelectedArchivesListModel: BaseModel {
    var countId:String?
    var current:Int?
    var hitCount:Int?
    var maxLimit:String?
    var optimizeCountSql:Int?
    var orders:[Any]?
    var pages:Int?
    var records:[AddClientSelecteArchivesModel]?
    var searchCount:Int?
    var size:Int?
    var total:Int?
    required init() {}
}


class AddClientSelecteArchivesModel: BaseModel {
    var bankAccount:Int?
    var bankName:Int?
    var bankNo:Int?
    var billingAddress:String?
    var businessType:String?
    var clientId:Int?
    var clientName:String?
    var clientNameAbbr:String?
    var clientType:String?
    var clientTypeRemark:String?
    var companyId:String?
    var createTime:String?
    var creditCode:String?
    var creditLevel:String?
    var creditLevelRemark:String?
    var crmCode:Int?
    var deleted:Int?
    var deliveryCondition:String?
    var deliveryConditionDesc:String?
    var deliveryRemark:String?
    var file:String?
    var id:Int?
    var isHave:Int?
    var payCondition:Int?
    var payDays:Int?
    var payRemark:String?
    var phone:String?
    var receivingInfo:[Any]?
    var registerAddr:String?
    var isSelected:Bool? = false
    
    required init() {}
}

class AddClientSelectedNatureListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedNatureModel]?
    var message:String?
        
    required init() {}
}


class AddClientSelectedNatureModel: BaseModel {
    var name:String?
    var deptId:String?
    var companyId:String?
    var parentId:String?
    var specs:String?
    var isSelected:Bool? = false
    var createBy:String?
    var createTime:String?
    var whetherDefault:String?
    
    required init() {}
}

class AddClientSelectedPhaseListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedPhaseModel]?
    var message:String?
        
    required init() {}
}


class AddClientSelectedPhaseModel: BaseModel {
    var deleted:String?
    var id:String?
    var orderNumber:String?
    var stageName:String?
    var version:String?
    var isSelected:Bool? = false
    
    required init() {}
}

class AddClientSelectedBussinessNameListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedBussinessNameModel]?
    var message:String?
        
    required init() {}
}


class AddClientSelectedBussinessNameModel: BaseModel {
    var clientId:String?
    var clientName:String?
    var noCloseCount:String?
    var isSelected:Bool? = false
    required init() {}
}

class AddClientSelectedTechnologyListModel: BaseModel {
    var code:String?
    var data:[AddClientSelectedTechnologyModel]?
    var message:String?

    required init() {}
}


class AddClientSelectedTechnologyModel: BaseModel {
    var condition:String?
    var isRequired:String?
    var name:String?
    var isSelected:Bool? = false
    required init() {}
}

class AddClientVisitAddPersonListModel: BaseModel {
    var dataList:[AddClientVisitAddPersonModel]?
    required init() {}
}


class AddClientVisitAddPersonModel: BaseModel {
    var avatar:String?
    var checked:String?
    var code:String?
    var companyId:String?
    var companyLogo:String?
    var companyName:String?
    var contacts:String?
    var createTime:String?
    var creditCode:String?
    var demandClientId:String?
    var demandId:String?
    var departmentId:String?
    var departmentName:String?
    var endDate:String?
    var gradeId:String?
    var gradeName:String?
    var jobId:String?
    var jobName:String?
    var mailbox:String?
    var menuIds:[Any]?
    var password:String?
    var phoneNumber:String?
    var positionId:String?
    var positionName:String?
    var registeredAddress:String?
    var source:String?
    var staffId:String?
    var staffNumber:String?
    var startDate:String?
    var status:String?
    var titleId:String?
    var titleName:String?
    var userAccount:String?
    var userId:String?
    var userName:String?
    var isSelected:Bool? = false
    required init() {}
}

