//
//  AddClientSelectedProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/14.
//

import UIKit

protocol AddClientSelectedViewProtocols: AnyObject {
    var presenter: AddClientSelectedPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddClientSelectedPresenterReceiveData(by params: Any?)
    func didGetAddClientSelectedPresenterReceiveError(error: MyError?)
    
    func didGetAddClientChargePresenterReceiveData(by params: Any?)
    func didGetAddClientChargePresenterReceiveError(error: MyError?)
    
    func didGetAddClientDepartMentPresenterReceiveData(by params: Any?)
    func didGetAddClientDepartMentPresenterReceiveError(error: MyError?)
    
    func didGetAddClientArchivesPresenterReceiveData(by params: Any?)
    func didGetAddClientArchivesPresenterReceiveError(error: MyError?)
    
    func didGetAddClientPricePersonPresenterReceiveData(by params: Any?)
    func didGetAddClientPricePersonPresenterReceiveError(error: MyError?)
    
    func didGetAddClientBussinessNumberPresenterReceiveData(by params: Any?)
    func didGetAddClientBussinessNumberPresenterReceiveError(error: MyError?)
    
    func didGetAddClientBussinessClientNamePresenterReceiveData(by params: Any?)
    func didGetAddClientBussinessClientNamePresenterReceiveError(error: MyError?)
    
    func didGetAddClientBussinessClientNaturePresenterReceiveData(by params: Any?)
    func didGetAddClientBussinessClientNaturePresenterReceiveError(error: MyError?)
    
    func didGetAddClientBussinessClientPhasePresenterReceiveData(by params: Any?)
    func didGetAddClientBussinessClientPhasePresenterReceiveError(error: MyError?)
    
    func didGetAddClientBussinessClientFlowerPresenterReceiveData(by params: Any?)
    func didGetAddClientBussinessClientFlowerPresenterReceiveError(error: MyError?)
    
    func didGetAddClientBussinessClientStatusPresenterReceiveData(by params: Any?)
    func didGetAddClientBussinessClientStatusPresenterReceiveError(error: MyError?)
    
    func didGetAddClientSellLeadsPresenterReceiveData(by params: Any?)
    func didGetAddClientSellLeadsPresenterReceiveError(error: MyError?)
    
    func didGetAddClientSampleTechnologyPresenterReceiveData(by params: Any?)
    func didGetAddClientSampleTechnologyPresenterReceiveError(error: MyError?)
    
    func didGetAddClientProductNamePresenterReceiveData(by params: Any?)
    func didGetAddClientProductNameyPresenterReceiveError(error: MyError?)
    
    func didGetAddClientSampleCodePresenterReceiveData(by params: Any?)
    func didGetAddClientSampleCodePresenterReceiveError(error: MyError?)
    
    func didGetAddClientCertificationAskPresenterReceiveData(by params: Any?)
    func didGetAddClientCertificationAskPresenterReceiveError(error: MyError?)
    
    func didGetAddClientVisitMyClientPresenterReceiveData(by params: Any?)
    func didGetAddClientVisitMyClientPresenterReceiveError(error: MyError?)
    
//    func didGetAddClientInfosDetailPresenterReceiveData(by params: Any?)
//    func didGetAddClientInfosDetailPresenterReceiveError(error: MyError?)
}

protocol AddClientSelectedPresenterProtocols: AnyObject{
    var view: AddClientSelectedViewProtocols? { get set }
    var router: AddClientSelectedRouterProtocols? { get set }
    var interactor: AddClientSelectedInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddClientSelected(by params: Any?)
    func didGetAddClientSelectedInteractorReceiveData(by params: Any?)
    func didGetAddClientSelectedInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientCharge(by params: Any?)
    func didGetAddClientChargeInteractorReceiveData(by params: Any?)
    func didGetAddClientChargeInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientDepartMent(by params: Any?)
    func didGetAddClientDepartMentInteractorReceiveData(by params: Any?)
    func didGetAddClientDepartMentInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientArchives(by params: Any?)
    func didGetAddClientArchivesInteractorReceiveData(by params: Any?)
    func didGetAddClientArchivesInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientPricePerson(by params: Any?)
    func didGetAddClientPricePersonInteractorReceiveData(by params: Any?)
    func didGetAddClientPricePersonInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientBussinessClientName(by params: Any?)
    func didGetAddClientBussinessClientNameInteractorReceiveData(by params: Any?)
    func didGetAddClientBussinessClientNameInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientBussinessNumber(by params: Any?)
    func didGetAddClientBussinessNumberInteractorReceiveData(by params: Any?)
    func didGetAddClientBussinessNumberInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientBussinessNature(by params: Any?)
    func didGetAddClientBussinessNatureInteractorReceiveData(by params: Any?)
    func didGetAddClientBussinessNatureInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientBussinessPhase(by params: Any?)
    func didGetAddClientBussinessPhaseInteractorReceiveData(by params: Any?)
    func didGetAddClientBussinessPhaseInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientBussinessFlower(by params: Any?)
    func didGetAddClientBussinessFlowerInteractorReceiveData(by params: Any?)
    func didGetAddClientBussinessFlowerInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientBussinessStatus(by params: Any?)
    func didGetAddClientBussinessStatusInteractorReceiveData(by params: Any?)
    func didGetAddClientBussinessStatusInteractorReceiveError(error: MyError?)

    func presenterRequestGetAddClientSellLeads(by params: Any?)
    func didGetAddClientSellLeadsInteractorReceiveData(by params: Any?)
    func didGetAddClientSellLeadsInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientSampleTechnology(by params: Any?)
    func didGetAddClientSampleTechnologyInteractorReceiveData(by params: Any?)
    func didGetAddClientSampleTechnologyInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientProductName(by params: Any?)
    func didGetAddClientProductNameInteractorReceiveData(by params: Any?)
    func didGetAddClientProductNameInteractorReceiveError(error: MyError?)
    
    func presenterRequestGetAddClientSampleCode(by params: Any?)
    func didGetAddClientSampleCodeInteractorReceiveData(by params: Any?)
    func didGetAddClientSampleCodeInteractorReceiveError(error: MyError?)
    
    func presenterRequestAddClientCertificationAsk(by params: Any?)
    func didEntityAddClientCertificationAskReceiveData(by params: Any?)
    func didEntityAddClientCertificationAskReceiveError(error: MyError?)
    
    func presenterRequestAddClientVisitMyClient(by params: Any?)
    func didEntityAddClientVisitMyClientReceiveData(by params: Any?)
    func didEntityAddClientVisitMyClientReceiveError(error: MyError?)
    
//    func presenterRequestGetAddClientInfos(by params: Any?)
//    func didGetAddClientInfosInteractorReceiveData(by params: Any?)
//    func didGetAddClientInfosInteractorReceiveError(error: MyError?)
    
}

protocol AddClientSelectedInteractorProtocols: AnyObject {
    var presenter: AddClientSelectedPresenterProtocols? { get set }
    var entity: AddClientSelectedEntityProtocols? { get set }
    
    func presenterRequestAddClientSelected(by params: Any?)
    func didEntityAddClientSelectedReceiveData(by params: Any?)
    func didEntityAddClientSelectedReceiveError(error: MyError?)
    
    func presenterRequestAddClientCharge(by params: Any?)
    func didEntityAddClientChargeReceiveData(by params: Any?)
    func didEntityAddClientChargeReceiveError(error: MyError?)
    
    func presenterRequestAddClientDepartMent(by params: Any?)
    func didEntityAddClientDepartMentReceiveData(by params: Any?)
    func didEntityAddClientDepartMentReceiveError(error: MyError?)
    
    func presenterRequestAddClientArchives(by params: Any?)
    func didEntityAddClientArchivesReceiveData(by params: Any?)
    func didEntityAddClientArchivesReceiveError(error: MyError?)
    
    func presenterRequestAddClientPricePerson(by params: Any?)
    func didEntityAddClientPricePersonReceiveData(by params: Any?)
    func didEntityAddClientPricePersonReceiveError(error: MyError?)
    
    func presenterRequestAddClientBussinessNumber(by params: Any?)
    func didEntityAddClientBussinessNumberReceiveData(by params: Any?)
    func didEntityAddClientBussinessNumberReceiveError(error: MyError?)
    
    func presenterRequestAddClientBussinessClientName(by params: Any?)
    func didEntityAddClientBussinessClientNameReceiveData(by params: Any?)
    func didEntityAddClientBussinessClientNameReceiveError(error: MyError?)
    
    func presenterRequestAddClientBussinessNature(by params: Any?)
    func didEntityAddClientBussinessNatureReceiveData(by params: Any?)
    func didEntityAddClientBussinessNatureReceiveError(error: MyError?)
    
    func presenterRequestAddClientBussinessPhase(by params: Any?)
    func didEntityAddClientBussinessPhaseReceiveData(by params: Any?)
    func didEntityAddClientBussinessPhaseReceiveError(error: MyError?)
    
    func presenterRequestAddClientBussinessFlower(by params: Any?)
    func didEntityAddClientBussinessFlowerReceiveData(by params: Any?)
    func didEntityAddClientBussinessFlowerReceiveError(error: MyError?)
    
    func presenterRequestAddClientBussinessStatus(by params: Any?)
    func didEntityAddClientBussinessStatusReceiveData(by params: Any?)
    func didEntityAddClientBussinessStatusReceiveError(error: MyError?)
    
    func presenterRequestAddClientSellLeads(by params: Any?)
    func didEntityAddClientSellLeadsReceiveData(by params: Any?)
    func didEntityAddClientSellLeadsReceiveError(error: MyError?)
    
    func presenterRequestAddClientSampleTechnology(by params: Any?)
    func didEntityAddClientSampleTechnologyReceiveData(by params: Any?)
    func didEntityAddClientSampleTechnologyReceiveError(error: MyError?)
    
    func presenterRequestAddClientProductName(by params: Any?)
    func didEntityAddClientProductNameReceiveData(by params: Any?)
    func didEntityAddClientProductNameReceiveError(error: MyError?)
    
    func presenterRequestAddClientSampleCode(by params: Any?)
    func didEntityAddClientSampleCodeReceiveData(by params: Any?)
    func didEntityAddClientSampleCodeReceiveError(error: MyError?)
    
    func presenterRequestAddClientCertificationAsk(by params: Any?)
    func didEntityAddClientCertificationAskReceiveData(by params: Any?)
    func didEntityAddClientCertificationAskReceiveError(error: MyError?)
    
    func presenterRequestAddClientVisitMyClient(by params: Any?)
    func didEntityAddClientVisitMyClientReceiveData(by params: Any?)
    func didEntityAddClientVisitMyClientReceiveError(error: MyError?)
    
//    func presenterRequestAddClientInfos(by params: Any?)
//    func didAddClientInfosReceiveData(by params: Any?)
//    func didAddClientInfosReceiveError(error: MyError?)
}

protocol AddClientSelectedEntityProtocols: AnyObject {
    var interactor: AddClientSelectedInteractorProtocols? { get set }
    
    func didAddClientSelectedReceiveData(by params: Any?)
    func didAddClientSelectedReceiveError(error: MyError?)
    func getAddClientSelectedRequest(by params: Any?)
    
    func getAddClientChargeRequest(by params: Any?)
    func didAddClientChargeReceiveError(error: MyError?)
    func didAddClientChargeReceiveData(by params: Any?)
    
    func didAddClientDepartMentReceiveData(by params: Any?)
    func didAddClientDepartMentReceiveError(error: MyError?)
    func getAddClientDepartMentRequest(by params: Any?)
    
    func didAddClientArchivesReceiveData(by params: Any?)
    func didAddClientArchivesReceiveError(error: MyError?)
    func getAddClientArchivesRequest(by params: Any?)
    
    func didAddClientPricePersonReceiveData(by params: Any?)
    func didAddClientPricePersonReceiveError(error: MyError?)
    func getAddClientPriccecPersonRequest(by params: Any?)
    
    func didAddClientBussinessNumberReceiveData(by params: Any?)
    func didAddClientBussinessNumberReceiveError(error: MyError?)
    func getAddClientBussinessNumberRequest(by params: Any?)
    
//    func didAddClientBussinessClientNameReceiveData(by params: Any?)
//    func didAddClientBussinessNumberReceiveError(error: MyError?)
//    func getAddClientBussinessNumberRequest(by params: Any?)
    
    func didAddClientBussinessClientNameReceiveData(by params: Any?)
    func didAddClientBussinessClientNameReceiveError(error: MyError?)
    func getAddClientBussinessClientNameRequest(by params: Any?)
    
    func didAddClientBussinessNatureReceiveData(by params: Any?)
    func didAddClientBussinessNatureReceiveError(error: MyError?)
    func getAddClientBussinessNatureRequest(by params: Any?)
    
    func didAddClientBussinessPhaseReceiveData(by params: Any?)
    func didAddClientBussinessPhaseReceiveError(error: MyError?)
    func getAddClientBussinessPhaseRequest(by params: Any?)
    
    func didAddClientBussinessFlowerReceiveData(by params: Any?)
    func didAddClientBussinessFlowerReceiveError(error: MyError?)
    func getAddClientBussinessFlowerRequest(by params: Any?)
    
    func didAddClientBussinessStatusReceiveData(by params: Any?)
    func didAddClientBussinessStatusReceiveError(error: MyError?)
    func getAddClientBussinessStatusRequest(by params: Any?)
    
    func didAddClientSellLeadsReceiveData(by params: Any?)
    func didAddClientSellLeadsReceiveError(error: MyError?)
    func getAddClientSellLeadsRequest(by params: Any?)
    
    func didAddClientSampleTechnologyReceiveData(by params: Any?)
    func didAddClientSampleTechnologyReceiveError(error: MyError?)
    func getAddClientSampleTechnologyRequest(by params: Any?)
    
    func getAddClientProductNameRequest(by params: Any?)
    func didAddClientProductNameReceiveData(by params: Any?)
    func didAddClientProductNameReceiveError(error: MyError?)
    
    func getAddClientSampleCodeRequest(by params: Any?)
    func didAddClientSampleCodeReceiveData(by params: Any?)
    func didAddClientSampleCodeReceiveError(error: MyError?)
    
    func getAddClientCertificationAskRequest(by params: Any?)
    func didAddClientCertificationAskReceiveData(by params: Any?)
    func didAddClientCertificationAskReceiveError(error: MyError?)
    
    func getAddClientVisitMyClientRequest(by params: Any?)
    func didAddClientVisitMyClientReceiveData(by params: Any?)
    func didAddClientVisitMyClientReceiveError(error: MyError?)
    
//    func didGetAddClientInfosReceiveData(by params: Any?)
//    func didGetAddClientInfosReceiveError(error: MyError?)
//    func getAddClientInfostRequest(by params: Any?)
    
}

protocol AddClientSelectedRouterProtocols: AnyObject {
    func pushToMyClient(from previousView: UIViewController)
    func pushToVisitDetail(from previousView: UIViewController)
}



