//
//  AddClientSelectedViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/14.
//

import UIKit
import PKHUD
import HandyJSON

class AddClientSelectedViewController: BaseViewController {
    
    var listDatas:[Any]? = []
    var presenter: AddClientSelectedPresenterProtocols?
    var tableView:UITableView?
    var addClientDataBlock: ((AddClientSelectedModel) -> ())?
    var addClientPersonBlock: ((AddClientSelectedPersonModel) -> ())?
    var addClientSelectedDepartMentBlock: ((AddClientSelectedDepartMentModel) -> ())?
    var addClientSelectedArchivesBlock: ((AddClientSelecteArchivesModel) -> ())?
    var addClientSelectedNatureBlock: ((AddClientSelectedNatureModel) -> ())?
    var addClientSelectedPhaseBlock: ((AddClientSelectedPhaseModel) -> ())?
    var addClientSelectedBussinessNameBlock: ((AddClientSelectedBussinessNameModel) -> ())?
    var addClientSelectedTechnologyBlock: ((AddClientSelectedTechnologyModel) -> ())?
    var addClientSelectedProductNameBlock: ((MyClientModelData) -> ())?
    var addClientVisitAddPersonBlock:((AddClientVisitAddPersonModel) -> ())?
    
    var clientSelectConfig:Int? = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MyClientViewController.backClick))
            
        buildUI()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    func buildUI(){
//        let searchBgView = UIView().then { obj in
//            view.addSubview(obj)
//            obj.backgroundColor = bgColor
//
//            obj.snp.makeConstraints { make in
//                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
//                make.left.equalToSuperview().offset(0)
//                make.right.equalToSuperview().offset(-0)
//                make.height.equalTo(50)
//            }
//        }
//
//        let searchView = UIView().then { obj in
//            searchBgView.addSubview(obj)
//            obj.backgroundColor = .white
//            obj.layer.cornerRadius = 2
//            obj.layer.masksToBounds = true
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(15)
//                make.right.equalToSuperview().offset(-15)
//                make.top.equalToSuperview().offset(10)
//                make.height.equalTo(30)
//            }
//        }
//
//        let searchImageV = UIImageView().then { obj in
//            searchView.addSubview(obj)
//            //            obj.backgroundColor = .blue
//            obj.image = UIImage(named: "home_search_tag")
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(15)
//                make.centerY.equalToSuperview()
//                make.width.height.equalTo(15)
//            }
//        }
//
//        _ = UITextField().then { obj in
//            searchView.addSubview(obj)
//            obj.font = kRegularFont(12)
//            obj.placeholder = "搜索"
//
//            obj.snp.makeConstraints { make in
//                make.left.equalTo(searchImageV.snp.right).offset(0)
//                make.right.equalToSuperview().offset(-15)
//                make.top.equalToSuperview().offset(0)
//                make.bottom.equalToSuperview().offset(-0)
//            }
//        }
        
        tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true
            obj.register(MyClientTableViewCell.self, forCellReuseIdentifier: "MyClientTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
        
        if clientSelectConfig == 39{
            navigationItem.title = "客诉客户"
            presenter?.presenterRequestAddClientVisitMyClient(by: ["current":1,"size":50])
        }else  if clientSelectConfig == 38{
            navigationItem.title = "客诉类型"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":4])
        }else if clientSelectConfig == 37{
            navigationItem.title = "性质"
            for i in 0..<2{
                let model = AddClientSelectedModel()
                if i == 0{
                    model.attrName = "必要"
                }else{
                    model.attrName = "参考"
                }
                self.listDatas?.append(model)
            }
            self.tableView?.reloadData()
        }else if clientSelectConfig == 36{
            self.navigationItem.title = "认证类型"
            presenter?.presenterRequestAddClientCertificationAsk(by: ["type":0])
        }else if clientSelectConfig == 35{
            self.navigationItem.title = "认证机构"
            presenter?.presenterRequestAddClientCertificationAsk(by: ["type":1])
        }else if clientSelectConfig == 34{
            self.navigationItem.title = "环保类型"
            presenter?.presenterRequestAddClientCertificationAsk(by: ["type":2])
        }else if clientSelectConfig == 33{
            self.navigationItem.title = "物料编码"
            presenter?.presenterRequestGetAddClientSampleCode(by: ["":""])
        }else if clientSelectConfig == 32{
            self.navigationItem.title = "商机性质"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":21])
        }else if clientSelectConfig == 31{
            navigationItem.title = "单位"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":11])
        }else if clientSelectConfig == 30{
            navigationItem.title = "角度"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":10])
        }else if clientSelectConfig == 29{
            navigationItem.title = "用量"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":9])
        }else if clientSelectConfig == 28{
            navigationItem.title = "技术规格"
            presenter?.presenterRequestGetAddClientSampleTechnology(by: ["":""])
        }else if clientSelectConfig == 27{
            navigationItem.title = "线索规模"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":20])
        }else if clientSelectConfig == 26{
            navigationItem.title = "线索产业"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":19])
        }else if clientSelectConfig == 25{
            navigationItem.title = "线索产业"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":18])
        }else if clientSelectConfig == 24{
            navigationItem.title = "客户来源"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":2])
        }else if clientSelectConfig == 23{
            navigationItem.title = "客户属性"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":1])
        }else if clientSelectConfig == 22{
            navigationItem.title = "客户级别"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":17])
        }else if clientSelectConfig == 21{
            navigationItem.title = "所属公海"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":16])
        }else if clientSelectConfig == 20{
            navigationItem.title = "物料名称"
            presenter?.presenterRequestGetAddClientProductName(by: ["current":1,"size":50])
        }else if clientSelectConfig == 19{
            navigationItem.title = "优先级"
            presenter?.presenterRequestGetAddClientSelected(by:["type":8])
        }else if clientSelectConfig == 18{
            navigationItem.title = "原因"
            presenter?.presenterRequestGetAddClientSelected(by:["type":7])
        }else if clientSelectConfig == 17{
            navigationItem.title = "性别"
            for i in 0..<2{
                let model = AddClientSelectedModel()
                if i == 0{
                    model.attrName = "男"
                }else{
                    model.attrName = "女"
                }
                self.listDatas?.append(model)
            }
            self.tableView?.reloadData()
        }else if clientSelectConfig == 16{
            navigationItem.title = "成交状态"
            presenter?.presenterRequestGetAddClientSelected(by: ["type":22])
        }else if clientSelectConfig == 15{
            navigationItem.title = "商机可能性"
            for i in 0..<9{
                let model = AddClientSelectedModel()
                model.attrName = "\(i+1)0%"
                model.attrNum = i
                self.listDatas?.append(model)
            }
            self.tableView?.reloadData()
        }else if clientSelectConfig == 14{
            self.navigationItem.title = "商机跟随人"
            presenter?.presenterRequestGetAddClientBussinessFlower(by: ["":""])
        }else if clientSelectConfig == 13{
            self.navigationItem.title = "商机介入阶段"
            presenter?.presenterRequestGetAddClientBussinessPhase(by: ["":""])
        }else if clientSelectConfig == 12{
            self.navigationItem.title = "商机产品类型"
            presenter?.presenterRequestGetAddClientBussinessNature(by: ["":""])
        }else if clientSelectConfig == 11{
            self.navigationItem.title = "商机来源"
            presenter?.presenterRequestGetAddClientSelected(by:["type":5])
        }else if clientSelectConfig == 10{
            self.navigationItem.title = "客户名称"
            presenter?.presenterRequestGetAddClientBussinessClientName(by: ["":""])
        }else if clientSelectConfig == 9{
            presenter?.presenterRequestGetAddClientBussinessNumber(by: ["prefix":"BO"])
        }else if clientSelectConfig == 8{
            presenter?.presenterRequestGetAddClientPricePerson(by: ["current":1,"size":50])
        }else if clientSelectConfig == 7{
//            presenter?.presenterRequestGetAddClientPricePerson(by: ["current":1,"size":50])
            presenter?.presenterRequestGetAddClientSelected(by:["type":6])
        }else if clientSelectConfig == 6{
            presenter?.presenterRequestGetAddClientArchives(by: ["clientName":"","current":1,"size":50])
        }else if clientSelectConfig == 5{
            presenter?.presenterRequestGetAddClientDepartMent(by: ["":""])
        }else if clientSelectConfig == 4{
            presenter?.presenterRequestGetAddClientCharge(by: ["":""])
        }else{
            presenter?.presenterRequestGetAddClientSelected(by:["type":clientSelectConfig])
        }
    }
    
    func cofing(){
        let router = AddClientSelectedRouter()
        
        presenter = AddClientSelectedPresenter()
        
        presenter?.router = router
        
        let entity = AddClientSelectedEntity()
        
        let interactor = AddClientSelectedInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
 
//        presenter?.presenterRequestGetClientPerosnPrice(by: ["clientName":model?.clientName])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddClientSelectedViewController: AddClientSelectedViewProtocols{
//    func didGetAddClientInfosDetailPresenterReceiveData(by params: Any?) {
//        if let dict = params as? [String:Any]{
//            if dict["code"] as! Int == 200{
//                if let dataModel = JSONDeserializer<AddClinetNameModel>.deserializeFrom(json:toJSONString(dict: dict)) {
//                    for i in 0..<(dataModel.data?.count ?? 0) {
//                        let model = dataModel.data?[i] as? AddClinetNameDataModel
//                        if i == 0{
//                            model?.isSelected = true
//                        }else{
//                            model?.isSelected = false
//                        }
//                        self.listDatas?.append(model!)
//                    }
//                }
//                tableView?.reloadData()
//            }else{
//                HUD.flash(.label(dict["message"] as? String), delay: 2)
//            }
//        }else{
//            HUD.flash(.label("data为nil"), delay: 2)
//        }
//    }
//    
//    func didGetAddClientInfosDetailPresenterReceiveError(error: MyError?) {
//        
//    }
    
    
    func didGetAddClientVisitMyClientPresenterReceiveData(by params: Any?){
        printTest(params)
        
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
//                let datas = dict["data"]
                if let dataModel = JSONDeserializer<AddClientVisitAddPersonListModel>.deserializeFrom(json:toJSONString(dict:  dict["data"] as! [String : Any])) {
                    for i in 0..<(dataModel.dataList?.count ?? 0) {
                        let model = dataModel.dataList?[i] as? AddClientVisitAddPersonModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientVisitMyClientPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientCertificationAskPresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedDepartMentListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedDepartMentModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientCertificationAskPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientSampleCodePresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedArchivesListModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    for i in 0..<(dataModel.records?.count ?? 0) {
                        let model = dataModel.records?[i] as? AddClientSelecteArchivesModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientSampleCodePresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientProductNamePresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedArchivesListModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    for i in 0..<(dataModel.records?.count ?? 0) {
                        let model = dataModel.records?[i] as? AddClientSelecteArchivesModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientProductNameyPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientSampleTechnologyPresenterReceiveData(by params: Any?){
        printTest(params)
    }
    
    func didGetAddClientSampleTechnologyPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientSellLeadsPresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedTechnologyListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedTechnologyModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientSellLeadsPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientBussinessClientStatusPresenterReceiveData(by params: Any?) {
        printTest(params)
    }
    
    func didGetAddClientBussinessClientStatusPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddClientBussinessClientFlowerPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedPersonListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedPersonModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientBussinessClientFlowerPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddClientBussinessClientPhasePresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedPhaseListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedPhaseModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientBussinessClientPhasePresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddClientBussinessClientNaturePresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedNatureListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedNatureModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientBussinessClientNaturePresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddClientBussinessClientNamePresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedBussinessNameListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedBussinessNameModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
        
    }
    
    func didGetAddClientBussinessClientNamePresenterReceiveError(error: MyError?) {
        
    }
    
    
    func didGetAddClientPricePersonPresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
//                let datas = dict["data"]
                if let dataModel = JSONDeserializer<AddClientVisitAddPersonListModel>.deserializeFrom(json:toJSONString(dict:  dict["data"] as! [String : Any])) {
                    for i in 0..<(dataModel.dataList?.count ?? 0) {
                        let model = dataModel.dataList?[i] as? AddClientVisitAddPersonModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientPricePersonPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientArchivesPresenterReceiveData(by params: Any?){
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedArchivesListModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    for i in 0..<(dataModel.records?.count ?? 0) {
                        let model = dataModel.records?[i] as? AddClientSelecteArchivesModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientArchivesPresenterReceiveError(error: MyError?){
        
    }
    
    func didGetAddClientDepartMentPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedDepartMentListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedDepartMentModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientDepartMentPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddClientChargePresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedPersonListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedPersonModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientChargePresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetAddClientSelectedPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<AddClientSelectedListModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? AddClientSelectedModel
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddClientSelectedPresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetAddClientBussinessNumberPresenterReceiveData(by params: Any?) {
        printTest(params)
    }
    
    func didGetAddClientBussinessNumberPresenterReceiveError(error: MyError?) {
        
    }
}

extension AddClientSelectedViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDatas?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyClientTableViewCell", for: indexPath) as! MyClientTableViewCell
        if clientSelectConfig == 8{
            cell.addClientVisitAddPersonModel = self.listDatas?[indexPath.row] as? AddClientVisitAddPersonModel
        }else if clientSelectConfig == 39{
            cell.addClientVisitAddPersonModel = self.listDatas?[indexPath.row] as? AddClientVisitAddPersonModel
        }else if clientSelectConfig == 36{
            cell.addClientSelectedDepartMentModel2 = self.listDatas?[indexPath.row] as? AddClientSelectedDepartMentModel
        }else if clientSelectConfig == 35{
            cell.addClientSelectedDepartMentModel2 = self.listDatas?[indexPath.row] as? AddClientSelectedDepartMentModel
        }else if clientSelectConfig == 34{
            cell.addClientSelectedDepartMentModel2 = self.listDatas?[indexPath.row] as? AddClientSelectedDepartMentModel
        }else if clientSelectConfig == 33{
            cell.addClientSelecteArchivesModel = self.listDatas?[indexPath.row] as? AddClientSelecteArchivesModel
        }else if clientSelectConfig == 20{
            cell.addClientSelecteArchivesModel = self.listDatas?[indexPath.row] as? AddClientSelecteArchivesModel
        }else if clientSelectConfig == 28{
            cell.addClientSelectedTechnologyModel = self.listDatas?[indexPath.row] as? AddClientSelectedTechnologyModel
        }else if clientSelectConfig == 13{
            cell.addClientSelectedPhaseModel = self.listDatas?[indexPath.row] as? AddClientSelectedPhaseModel
        }else if clientSelectConfig == 12{
            cell.addClientSelectedNatureModel = self.listDatas?[indexPath.row] as? AddClientSelectedNatureModel
        }else if clientSelectConfig == 10{
            cell.addClientSelectedBussinessNameModel = self.listDatas?[indexPath.row] as? AddClientSelectedBussinessNameModel
        }else if clientSelectConfig == 6{
            cell.addClientSelecteArchivesModel = self.listDatas?[indexPath.row] as? AddClientSelecteArchivesModel
        }else if clientSelectConfig == 5{
            cell.addClientSelectedDepartMentModel = self.listDatas?[indexPath.row] as? AddClientSelectedDepartMentModel
        }else if clientSelectConfig == 4{
            cell.addClientPersonModel = self.listDatas?[indexPath.row] as? AddClientSelectedPersonModel
        }else if clientSelectConfig == 14{
            cell.addClientPersonModel = self.listDatas?[indexPath.row] as? AddClientSelectedPersonModel
        }else{
            cell.addClientModel = self.listDatas?[indexPath.row] as? AddClientSelectedModel
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if clientSelectConfig == 8{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientVisitAddPersonModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientVisitAddPersonModel
            self.addClientVisitAddPersonBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 39{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientVisitAddPersonModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientVisitAddPersonModel
            self.addClientVisitAddPersonBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 37{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedModel
            self.addClientDataBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 36{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedDepartMentModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedDepartMentModel
            self.addClientSelectedDepartMentBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 35{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedDepartMentModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedDepartMentModel
            self.addClientSelectedDepartMentBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 34{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedDepartMentModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedDepartMentModel
            self.addClientSelectedDepartMentBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 20{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelecteArchivesModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelecteArchivesModel
            self.addClientSelectedArchivesBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 28{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedTechnologyModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedTechnologyModel
            self.addClientSelectedTechnologyBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 13{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedPhaseModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedPhaseModel
            self.addClientSelectedPhaseBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 12{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedNatureModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedNatureModel
            self.addClientSelectedNatureBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 10{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedBussinessNameModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedBussinessNameModel
            self.addClientSelectedBussinessNameBlock?(model!)
            self.navigationController?.popViewController(animated: true)
            
        }else if clientSelectConfig == 33{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelecteArchivesModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelecteArchivesModel
            self.addClientSelectedArchivesBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 6{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelecteArchivesModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelecteArchivesModel
            self.addClientSelectedArchivesBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 5{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedDepartMentModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedDepartMentModel
            self.addClientSelectedDepartMentBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 4{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedPersonModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedPersonModel
            
            self.addClientPersonBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else if clientSelectConfig == 14{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedPersonModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedPersonModel
            
            self.addClientPersonBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }else{
            for i in 0..<(self.listDatas?.count ?? 0) {
                let model = self.listDatas![i] as? AddClientSelectedModel
                if i == indexPath.row{
                    model?.isSelected = true
                }else{
                    model?.isSelected = false
                }
            }
            tableView.reloadData()
            let model = self.listDatas![indexPath.row] as? AddClientSelectedModel
            self.addClientDataBlock?(model!)
            self.navigationController?.popViewController(animated: true)
        }
    }
}

