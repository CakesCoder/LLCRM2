//
//  AddClientSelectedInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/14.
//

import UIKit

class AddClientSelectedInteractor: AddClientSelectedInteractorProtocols {
    var presenter: AddClientSelectedPresenterProtocols?
    
    var entity: AddClientSelectedEntityProtocols?
    
    func presenterRequestAddClientSelected(by params: Any?) {
        entity?.getAddClientSelectedRequest(by: params)
    }
    
    func didEntityAddClientSelectedReceiveData(by params: Any?) {
        presenter?.didGetAddClientSelectedInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientSelectedReceiveError(error: MyError?) {
        presenter?.didGetAddClientSelectedInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientCharge(by params: Any?) {
        entity?.getAddClientChargeRequest(by: params)
    }
    
    func didEntityAddClientChargeReceiveData(by params: Any?) {
        presenter?.didGetAddClientChargeInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientChargeReceiveError(error: MyError?) {
        presenter?.didGetAddClientChargeInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientDepartMent(by params: Any?) {
        entity?.getAddClientDepartMentRequest(by: params)
    }
    
    func didEntityAddClientDepartMentReceiveData(by params: Any?) {
        presenter?.didGetAddClientDepartMentInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientDepartMentReceiveError(error: MyError?) {
        presenter?.didGetAddClientDepartMentInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientArchives(by params: Any?){
        entity?.getAddClientArchivesRequest(by: params)
    }
    
    func didEntityAddClientArchivesReceiveData(by params: Any?){
        presenter?.didGetAddClientArchivesInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientArchivesReceiveError(error: MyError?){
        presenter?.didGetAddClientArchivesInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientPricePerson(by params: Any?){
        entity?.getAddClientPriccecPersonRequest(by: params)
    }
    
    func didEntityAddClientPricePersonReceiveData(by params: Any?){
        presenter?.didGetAddClientPricePersonInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientPricePersonReceiveError(error: MyError?){
        presenter?.didGetAddClientPricePersonInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientBussinessNumber(by params: Any?){
        entity?.getAddClientBussinessNumberRequest(by: params)
    }
    
    func didEntityAddClientBussinessNumberReceiveData(by params: Any?){
        presenter?.didGetAddClientBussinessNumberInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientBussinessNumberReceiveError(error: MyError?){
        presenter?.didGetAddClientBussinessNumberInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientBussinessClientName(by params: Any?){
        entity?.getAddClientBussinessClientNameRequest(by: params)
    }
    
    func didEntityAddClientBussinessClientNameReceiveData(by params: Any?){
        presenter?.didGetAddClientBussinessClientNameInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientBussinessClientNameReceiveError(error: MyError?){
        presenter?.didGetAddClientBussinessClientNameInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientBussinessNature(by params: Any?){
        entity?.getAddClientBussinessNatureRequest(by: params)
    }
    
    func didEntityAddClientBussinessNatureReceiveData(by params: Any?){
        presenter?.didGetAddClientBussinessNatureInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientBussinessNatureReceiveError(error: MyError?){
        presenter?.didGetAddClientBussinessNatureInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientBussinessPhase(by params: Any?){
        entity?.getAddClientBussinessPhaseRequest(by: params)
    }
    
    func didEntityAddClientBussinessPhaseReceiveData(by params: Any?){
        presenter?.didGetAddClientBussinessPhaseInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientBussinessPhaseReceiveError(error: MyError?){
        presenter?.didGetAddClientBussinessPhaseInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientBussinessFlower(by params: Any?){
        entity?.getAddClientBussinessFlowerRequest(by: params)
    }
    
    func didEntityAddClientBussinessFlowerReceiveData(by params: Any?){
        presenter?.didGetAddClientBussinessFlowerInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientBussinessFlowerReceiveError(error: MyError?){
        presenter?.didGetAddClientBussinessFlowerInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientBussinessStatus(by params: Any?){
        entity?.getAddClientBussinessStatusRequest(by: params)
    }
    
    func didEntityAddClientBussinessStatusReceiveData(by params: Any?){
        presenter?.didGetAddClientBussinessStatusInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientBussinessStatusReceiveError(error: MyError?){
        presenter?.didGetAddClientBussinessStatusInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientSellLeads(by params: Any?){
        entity?.getAddClientSellLeadsRequest(by: params)
    }
    
    func didEntityAddClientSellLeadsReceiveData(by params: Any?){
        presenter?.didGetAddClientSellLeadsInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientSellLeadsReceiveError(error: MyError?){
        presenter?.didGetAddClientSellLeadsInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientSampleTechnology(by params: Any?){
        entity?.getAddClientSampleTechnologyRequest(by: params)
    }
    
    func didEntityAddClientSampleTechnologyReceiveData(by params: Any?){
        presenter?.didGetAddClientSampleTechnologyInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientSampleTechnologyReceiveError(error: MyError?){
        presenter?.didGetAddClientSampleTechnologyInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientProductName(by params: Any?){
        entity?.getAddClientProductNameRequest(by: params)
    }
    
    func didEntityAddClientProductNameReceiveData(by params: Any?){
        presenter?.didGetAddClientProductNameInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientProductNameReceiveError(error: MyError?){
        presenter?.didGetAddClientProductNameInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientSampleCode(by params: Any?){
        entity?.getAddClientSampleCodeRequest(by: params)
    }
    
    func didEntityAddClientSampleCodeReceiveData(by params: Any?){
        presenter?.didGetAddClientSampleCodeInteractorReceiveData(by: params)
    }
    
    func didEntityAddClientSampleCodeReceiveError(error: MyError?){
        presenter?.didGetAddClientSampleCodeInteractorReceiveError(error: error)
    }
    
    func presenterRequestAddClientCertificationAsk(by params: Any?){
        entity?.getAddClientCertificationAskRequest(by: params)
    }
    
    func didEntityAddClientCertificationAskReceiveData(by params: Any?){
        presenter?.didEntityAddClientCertificationAskReceiveData(by: params)
    }
    
    func didEntityAddClientCertificationAskReceiveError(error: MyError?){
        presenter?.didEntityAddClientCertificationAskReceiveError(error: error)
    }
    
    func presenterRequestAddClientVisitMyClient(by params: Any?){
        entity?.getAddClientVisitMyClientRequest(by: params)
    }
    
    func didEntityAddClientVisitMyClientReceiveData(by params: Any?){
        presenter?.didEntityAddClientVisitMyClientReceiveData(by: params)
    }
    
    func didEntityAddClientVisitMyClientReceiveError(error: MyError?){
        presenter?.didEntityAddClientVisitMyClientReceiveError(error: error)
    }
    
//    func presenterRequestAddClientInfos(by params: Any?) {
//        entity?.getAddClientInfostRequest(by: params)
//    }
//
//    func didAddClientInfosReceiveData(by params: Any?) {
//        presenter?.didGetAddClientInfosInteractorReceiveData(by: params)
//    }
//
//    func didAddClientInfosReceiveError(error: MyError?) {
//        presenter?.didGetAddClientInfosInteractorReceiveError(error: error)
//    }
    
}
