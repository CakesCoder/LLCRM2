//
//  ApplyAddWorkRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

protocol ApplyAddWorkViewProtocols: AnyObject {
    var presenter: ApplyAddWorkPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol ApplyAddWorkPresenterProtocols: AnyObject{
    var view: ApplyAddWorkViewProtocols? { get set }
    var router: ApplyAddWorkRouterProtocols? { get set }
    var interactor: ApplyAddWorkInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol ApplyAddWorkInteractorProtocols: AnyObject {
    var presenter: ApplyAddWorkPresenterProtocols? { get set }
    var entity: ApplyAddWorkEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol ApplyAddWorkEntityProtocols: AnyObject {
    var interactor: ApplyAddWorkInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol ApplyAddWorkRouterProtocols: AnyObject {
    func pushToClientVisit(from previousView: UIViewController)
    
}

