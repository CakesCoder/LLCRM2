//
//  ApplyAddWprkRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyAddWorkprkRouter: ApplyAddWorkRouterProtocols {
    func pushToClientVisit(from previousView: UIViewController) {
        let vc = ClientVisitViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
}
