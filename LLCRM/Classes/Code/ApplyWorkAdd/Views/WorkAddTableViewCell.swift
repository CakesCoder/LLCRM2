//
//  WorkAddTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class WorkAddTableViewCell: UITableViewCell {
    
    var nameStr:String?{
        didSet{
            nameLabel?.text = nameStr
        }
    }
    
    var model:AddWorkModel?{
        didSet{
            if model?.isSelected == true{
                nameLabel?.textColor = blackTextColor61
            }else{
                nameLabel?.textColor = blackTextColor62
            }
        }
    }
    
    var nameLabel:UILabel? = UILabel()

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        nameLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor62
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.centerY.equalToSuperview()
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
