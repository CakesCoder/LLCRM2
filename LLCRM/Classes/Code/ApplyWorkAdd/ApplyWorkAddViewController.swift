//
//  ApplyWorkAddViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyWorkAddViewController: BaseViewController {

    let datas = ["客户拜访", "异地出差", "异地出差", "异地出差", "异地出差", "异地出差","异地出差"]
    
    var listDatas:[AddWorkModel]? = []
    
    var presenter: ApplyAddWorkPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "外勤"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ApplyWorkAddViewController.backClick))
        
        for i in 0..<7{
            let model = AddWorkModel()
            if i == 0{
                model.isSelected = true
            }
            listDatas?.append(model)
        }
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headViewe = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 56)
            obj.backgroundColor = .white
        }
        
        let imageV = UIImageView().then { obj in
            headViewe.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "apply_go")
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(16)
            }
        }
        
        _ = UIButton().then({ obj in
            headViewe.addSubview(obj)
            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
        
        let hintLabel = UILabel().then { obj in
            headViewe.addSubview(obj)
            obj.textColor = blackTextColor61
            obj.font = kMediumFont(16)
            obj.text = "新建一个外勤"
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(3)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then { obj in
            headViewe.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.tableHeaderView = headViewe
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true

            obj.register(WorkAddTableViewCell.self, forCellReuseIdentifier: "WorkAddTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
    }
    
    func cofing(){
        let router = ApplyAddWorkprkRouter()
        
        presenter = ApplyAddWorkPresenter()
        presenter?.router = router
        
        let entity = ApplyAddWorkEntity()
        
        let interactor = ApplyAddWprkInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
    }
    
    @objc func addClick(_ button:UIButton){
        self.presenter?.router?.pushToClientVisit(from: self)
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ApplyWorkAddViewController: ApplyAddWorkViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterReceiveData() {
        
    }
    
    func didPresenterReceiveError() {
        
    }
}

extension ApplyWorkAddViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<datas.count{
            let model = self.listDatas![i]
            if i == indexPath.row{
                model.isSelected = true
            }else{
                model.isSelected = false
            }
        }
        
        tableView.reloadData()
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WorkAddTableViewCell", for: indexPath) as! WorkAddTableViewCell
        cell.nameStr = self.datas[indexPath.row]
        cell.model = self.listDatas![indexPath.row]
        return cell
    }
    
    
}
