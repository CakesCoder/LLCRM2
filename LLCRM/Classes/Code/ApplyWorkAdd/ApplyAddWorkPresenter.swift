//
//  ApplyAddWorkPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyAddWorkPresenter: ApplyAddWorkPresenterProtocols {
    var view: ApplyAddWorkViewProtocols?
    
    var router: ApplyAddWorkRouterProtocols?
    
    var interactor: ApplyAddWorkInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }

}
