//
//  ApplyAddWprkInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyAddWprkInteractor: ApplyAddWorkInteractorProtocols {
    var presenter: ApplyAddWorkPresenterProtocols?
    
    var entity: ApplyAddWorkEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
    

}
