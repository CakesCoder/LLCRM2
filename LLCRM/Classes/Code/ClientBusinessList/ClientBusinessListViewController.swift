//
//  ClientBusinessListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/1.
//

import UIKit
import PKHUD
import HandyJSON

class ClientBusinessListViewController: BaseViewController {
    
    var listDatas:[MyClientModelData]? = []
    var presenter: MyClientPresenterProtocols?
    var tableView:UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "我的商机"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ClientBusinessListViewController.backClick))
        
        buildUI()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    func buildUI(){
        let searchBgView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        let searchView = UIView().then { obj in
            searchBgView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            //            obj.backgroundColor = .blue
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        _ = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kRegularFont(12)
            obj.placeholder = "搜索"
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(0)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true
            obj.register(MyClientTableViewCell.self, forCellReuseIdentifier: "MyClientTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(searchBgView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        cofing()
        
        presenter?.presenterRequestGetMyBusiness(by:["clientName":""])
    }
    
    func cofing(){
        let router = MyClientRouter()
        
        presenter = MyClientPresenter()
        
        presenter?.router = router
        
        let entity = MyClientEntity()
        
        let interactor = MyClientInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
 
//        presenter?.presenterRequestGetClientPerosnPrice(by: ["clientName":model?.clientName])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientBusinessListViewController: MyClientViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    
    func didGetMyClientPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<MyClientModel>.deserializeFrom(json:toJSONString(dict: dict ?? ["":""])) {
                    for i in 0..<(dataModel.data?.count ?? 0) {
                        let model = dataModel.data?[i] as? MyClientModelData
                        if i == 0{
                            model?.isSelected = true
                        }else{
                            model?.isSelected = false
                        }
                        self.listDatas?.append(model!)
                    }
                }
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetMyClientPresenterReceiveError(error: MyError?) {
        
    }
}

extension ClientBusinessListViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 112
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listDatas?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyClientTableViewCell", for: indexPath) as! MyClientTableViewCell
        cell.model = self.listDatas![indexPath.row] as? MyClientModelData
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<(self.listDatas?.count ?? 0) {
            let model = self.listDatas![i]
            if i == indexPath.row{
                model.isSelected = true
            }else{
                model.isSelected = false
            }
        }
        tableView.reloadData()
        let model = self.listDatas![indexPath.row] as? MyClientModelData
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "BusinessAdddNotificationKey"), object: nil, userInfo: ["model":model])
        self.navigationController?.popViewController(animated: true)
    }
}

