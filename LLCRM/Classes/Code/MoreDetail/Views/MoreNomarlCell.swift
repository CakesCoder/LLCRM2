//
//  MoreNomarlCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/14.
//

import UIKit

class MoreNomarlCell: UITableViewCell {
    var editorBlock: (() -> ())?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){

        self.selectionStyle = .none

        let normalView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UIView().then({ obj in
            normalView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
        
        
        let nomarlTagView = UIView().then { obj in
            normalView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(35)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            normalView.addSubview(obj)
            obj.text = "常用"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalTo(nomarlTagView.snp.right).offset(5)
                make.centerY.equalTo(nomarlTagView)
            }
        }
        
        _ = UIView().then({ obj in
            normalView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(nomarlTagView.snp.bottom).offset(13)
                make.height.equalTo(0.5)
            }
        })
        
        for i in 0..<5{
            let button = UIButton().then { obj in
                normalView.addSubview(obj)
                if i == 0{
                    obj.setImage(UIImage(named: "smail_more_1"), for: .normal)
                }else if i == 1{
                    obj.setImage(UIImage(named: "smail_more_2"), for: .normal)
                }else if i == 2{
                    obj.setImage(UIImage(named: "smail_more_3"), for: .normal)
                }else if i == 3{
                    obj.setImage(UIImage(named: "smail_more_4"), for: .normal)
                }else if i == 4{
                    obj.setImage(UIImage(named: "smail_more_4"), for: .normal)
                }
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(66+i*34+i*20)
                    make.top.equalToSuperview().offset(23)
                    make.width.equalTo(34)
                    make.height.equalTo(34)
                }
            }
        }
        
        let settingButton = UIButton().then { obj in
            normalView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.setTitle("设置", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.addTarget(self, action: #selector(editorClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(32)
                make.right.equalToSuperview().offset(-15)
                make.width.equalTo(48)
                make.height.equalTo(24)
            }
        }
    }
    
    @objc func editorClick(){
        editorBlock?()
    }
}
