//
//  MoreClientCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/14.
//

import UIKit

class MoreClientCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let dataView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UIView().then({ obj in
            dataView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
        
        let dataTagView = UIView().then { obj in
            dataView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(35)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UIView().then({ obj in
            dataView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(dataTagView.snp.bottom).offset(13)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UILabel().then { obj in
            dataView.addSubview(obj)
            obj.text = "客户跟进"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalTo(dataTagView.snp.right).offset(5)
                make.centerY.equalTo(dataTagView)
            }
        }
        
        let dataContentView = UIView().then { obj in
            dataView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(dataTagView.snp.bottom).offset(9)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let w = (kScreenWidth-30)/3.0
        
        for i in 0..<13{
            let x = i%3
            let y = i/3
            let v = UIView().then { obj in
                dataContentView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(CGFloat(x)*w)
                    make.top.equalToSuperview().offset(CGFloat(y)*w)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo(w)
                }
            }
            
            let imageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.isUserInteractionEnabled = true
                if i == 0{
                    obj.image = UIImage(named: "more_7")
                }else if i == 1{
                    obj.image = UIImage(named: "more_8")
                }else if i == 2{
                    obj.image = UIImage(named: "more_9")
                }else if i == 3{
                    obj.image = UIImage(named: "more_10")
                }else if i == 4{
                    obj.image = UIImage(named: "more_11")
                }else if i == 5{
                    obj.image = UIImage(named: "more_12")
                }else if i == 6{
                    obj.image = UIImage(named: "more_13")
                }else if i == 7{
                    obj.image = UIImage(named: "more_14")
                }else if i == 8{
                    obj.image = UIImage(named: "more_15")
                }else if i == 9{
                    obj.image = UIImage(named: "more_16")
                }else if i == 10{
                    obj.image = UIImage(named: "more_17")
                }else if i == 11{
                    obj.image = UIImage(named: "more_18")
                }else if i == 12{
                    obj.image = UIImage(named: "more_19")
                }
                
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(30)
                    make.centerX.equalToSuperview()
                }
            }
            
            let nameLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.textAlignment = .center
                obj.font = kMediumFont(14)
                obj.textColor = blackTextColor
                if i == 0{
                    obj.text = "查重工具"
                }else if i == 1{
                    obj.text = "附近客户"
                }else if i == 2{
                    obj.text = "客户"
                }else if i == 3{
                    obj.text = "商机2.0"
                }else if i == 4{
                    obj.text = "打样申请"
                }else if i == 5{
                    obj.text = "联系人"
                }else if i == 6{
                    obj.text = "销售记录"
                }else if i == 7{
                    obj.text = "方案申请表"
                }else if i == 8{
                    obj.text = "高级外勤对象"
                }else if i == 9{
                    obj.text = "外勤图片"
                }else if i == 10{
                    obj.text = "公海"
                }else if i == 11{
                    obj.text = "报价单"
                }else if i == 12{
                    obj.text = "客户地址"
                }
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(15)
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                }
            }
            
            _ = UIButton().then({ obj in
                v.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            })
        }
    }

}
