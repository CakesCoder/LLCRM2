//
//  MoreRequestTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/14.
//

import UIKit

class MoreRequestTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let dataView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UIView().then({ obj in
            dataView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
        
        let dataTagView = UIView().then { obj in
            dataView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(35)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            dataView.addSubview(obj)
            obj.text = "物料申请"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalTo(dataTagView.snp.right).offset(5)
                make.centerY.equalTo(dataTagView)
            }
        }
        
        _ = UIView().then({ obj in
            dataView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(dataTagView.snp.bottom).offset(13)
                make.height.equalTo(0.5)
            }
        })
        
        let dataContentView = UIView().then { obj in
            dataView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(dataTagView.snp.bottom).offset(9)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let w = (kScreenWidth-30)/3.0
        
        for i in 0..<2{
            let v = UIView().then { obj in
                dataContentView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(CGFloat(i)*w)
                    make.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo(w)
                }
            }
            
            let imageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.isUserInteractionEnabled = true
                if i == 0{
                    obj.image = UIImage(named: "more_31")
                }else{
                    obj.image = UIImage(named: "more_32")
                }
                
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(30)
                    make.centerX.equalToSuperview()
                }
            }
            
            let nameLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.textAlignment = .center
                obj.font = kMediumFont(14)
                obj.textColor = blackTextColor
                if i == 0{
                    obj.text = "销售物料申请"
                }else{
                    obj.text = "销售物料申请明细"
                }
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(15)
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                }
            }
            
            _ = UIButton().then({ obj in
                v.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            })
        }
    }

}
