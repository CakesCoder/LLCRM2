//
//  MoreDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class MoreDetailViewController: BaseViewController {
    
    var isEditor:Bool? = false
    var tableView:UITableView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "全部对象"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MoreDetailViewController.backClick))
        // Do any additional setup after loading the view.
        
        view.backgroundColor = .white
        
        let searchHeadView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            searchHeadView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
  
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
//            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(MoreNomarlCell.self, forCellReuseIdentifier: "MoreNomarlCell")
            obj.register(MoreDataCell.self, forCellReuseIdentifier: "MoreDataCell")
            obj.register(MoreManagerCell.self, forCellReuseIdentifier: "MoreManagerCell")
            obj.register(MoreMarketCell.self, forCellReuseIdentifier: "MoreMarketCell")
            obj.register(MoreClientCell.self, forCellReuseIdentifier: "MoreClientCell")
            obj.register(MoreProductCell.self, forCellReuseIdentifier: "MoreProductCell")
            obj.register(MoreCommitTableViewCell.self, forCellReuseIdentifier: "MoreCommitTableViewCell")
            obj.register(MoreServerTableViewCell.self, forCellReuseIdentifier: "MoreServerTableViewCell")
            obj.register(MoreRequestTableViewCell.self, forCellReuseIdentifier: "MoreRequestTableViewCell")
            obj.register(MoreRequest2TableViewCell.self, forCellReuseIdentifier: "MoreRequest2TableViewCell")
            obj.register(MoreNomarl2Cell.self, forCellReuseIdentifier: "MoreNomarl2Cell")

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(searchHeadView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.isEditor = !(self.isEditor)!
        tableView!.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MoreDetailViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
}

extension MoreDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            if self.isEditor == true{
                let w = (kScreenWidth-30)/3.0
                return w*2+50
            }
            return 70
        }else if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3 || indexPath.row == 5 || indexPath.row == 7 || indexPath.row == 8{
            return 176
        }else if indexPath.row == 4{
            let w = (kScreenWidth-30)/3.0
            return w*5+50
        }else if indexPath.row == 6 || indexPath.row == 9{
            let w = (kScreenWidth-30)/3.0
            return w*2+50
        }
        return 630
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if self.isEditor == true{
                let cell = tableView.dequeueReusableCell(withIdentifier: "MoreNomarl2Cell", for: indexPath) as! MoreNomarl2Cell
                return cell
            }
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreNomarlCell", for: indexPath) as! MoreNomarlCell
            cell.editorBlock = { [weak self] in
                self?.isEditor = !(self?.isEditor)!
                tableView.reloadData()
            }
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreDataCell", for: indexPath) as! MoreDataCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreManagerCell", for: indexPath) as! MoreManagerCell
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreMarketCell", for: indexPath) as! MoreMarketCell
            return cell
        }else if indexPath.row == 4{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreClientCell", for: indexPath) as! MoreClientCell
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreProductCell", for: indexPath) as! MoreProductCell
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreCommitTableViewCell", for: indexPath) as! MoreCommitTableViewCell
            return cell
        }else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreServerTableViewCell", for: indexPath) as! MoreServerTableViewCell
            return cell
        }else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MoreRequestTableViewCell", for: indexPath) as! MoreRequestTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "MoreRequest2TableViewCell", for: indexPath) as! MoreRequest2TableViewCell
        return cell
    }
}

