//
//  PersonSelectedTimeViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/12.
//

import UIKit

class PersonSelectedTimeViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        buildUI()
    }
    
    func buildUI(){
        self.navigationItem.title = "选择时间段"
        
        let headView = UIView().then { obj in
            obj.backgroundColor = bgColor
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let hints = ["天", "周", "月", "季", "年"]
        let days = ["今天", "昨天", "明天"]
        let weeks = ["本周", "上周", "下周"]
        let months = ["本月", "上月", "下月"]
        let seasons = ["本季度", "上一季度", "下一季度"]
        let years = ["本年度\n上半年度", "上一年度\n下半年度", "下一年度"]
        
        for i in 0..<5{
            let bgView = UIView().then{ obj in
                headView.addSubview(obj)
                obj.frame = CGRectMake(0, CGFloat(i*80), kScreenWidth, 80)
                obj.backgroundColor = .white
            }
            
            for j in 0..<4{
                let featureView = UIView().then { obj in
                    bgView.addSubview(obj)
                    obj.frame = CGRectMake(CGFloat(j)*(kScreenWidth/4), 0, kScreenWidth/4, 80)
//                    obj.backgroundColor = .yellow
                }
                
                if j == 0{
                    let hintLabel = UILabel().then { obj in
                        featureView.addSubview(obj)
                        obj.textColor = blackTextColor40
                        obj.font = kAMediumFont(14)
                        obj.text = hints[i]
                        obj.snp.makeConstraints { make in
                            make.left.equalToSuperview().offset(23)
                            make.centerY.equalToSuperview()
                        }
                    }
                }else{
                    if i == 0{
                        let dayButton = UIButton().then { obj in
                            featureView.addSubview(obj)
                            
                            obj.setTitle(days[j-1], for: .normal)
                            obj.setTitleColor(blackTextColor3, for: .normal)
                            obj.titleLabel?.font = kARegularFont(14)
                            
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(23)
                                make.centerY.equalToSuperview()
                            }
                        }
                    }else if i == 1{
                        let weekButton = UIButton().then { obj in
                            featureView.addSubview(obj)
                            
                            obj.setTitle(weeks[j-1], for: .normal)
                            obj.setTitleColor(blackTextColor3, for: .normal)
                            obj.titleLabel?.font = kARegularFont(14)
                            
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(23)
                                make.centerY.equalToSuperview()
                            }
                        }
                    }else if i == 2{
                        let monthButton = UIButton().then { obj in
                            featureView.addSubview(obj)
                            
                            obj.setTitle(months[j-1], for: .normal)
                            obj.setTitleColor(blackTextColor3, for: .normal)
                            obj.titleLabel?.font = kARegularFont(14)
                            
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(23)
                                make.centerY.equalToSuperview()
                            }
                        }
                    }else if i == 3{
                        let seasonButton = UIButton().then { obj in
                            featureView.addSubview(obj)
                            
                            obj.setTitle(seasons[j-1], for: .normal)
                            obj.setTitleColor(blackTextColor3, for: .normal)
                            obj.titleLabel?.font = kARegularFont(14)
                            
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(23)
                                make.centerY.equalToSuperview()
                            }
                        }
                    }else{
                        let yearLabel = UILabel().then { obj in
                            featureView.addSubview(obj)
                            
                            obj.text = years[j-1]
                            obj.textColor = blackTextColor3
                            obj.font = kARegularFont(14)
                            obj.numberOfLines = 2
//                            obj.textAlignment = .center
//                            obj.setTitle(years[j-1], for: .normal)
//                            obj.setTitleColor(blackTextColor3, for: .normal)
//                            obj.titleLabel?.font = kARegularFont(14)
                            
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(23)
//                                make.centerY.equalToSuperview()
                                make.top.equalToSuperview().offset(0)
                                make.right.equalToSuperview().offset(-0)
                                make.bottom.equalToSuperview().offset(-0)
                            }
                        }
                    }
                }
            }
            
            _ = UIView().then({ obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor14
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let customView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(410)
                make.height.equalTo(70)
            }
        }
        
        let customTitleLabel = UILabel().then { obj in
            customView.addSubview(obj)
            obj.text = "自定义"
            obj.textColor = blackTextColor3
            obj.font = kRegularFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        let customHintLabel = UILabel().then { obj in
            customView.addSubview(obj)
            
            obj.text = "请选择"
            obj.textColor = blackTextColor41
            obj.font = kRegularFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(23)
                make.top.equalToSuperview().offset(40)
            }
        }
        
        _ = UIImageView().then { obj in
            obj.backgroundColor = .red
            customView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(12)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
