//
//  BusinessDetailOtherViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/11.
//

import UIKit
//import JXPagingView

extension BusinessDetailOtherViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension BusinessDetailOtherViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class BusinessDetailOtherViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var infoLabel1:UILabel?
    var infoLabel2:UILabel?
    var infoLabel3:UILabel?
    var infoLabel4:UILabel?
    var infoLabel5:UILabel?
    var infoLabel6:UILabel?
    var infoLabel7:UILabel?
    
    var model:BusinessListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "联系人"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["其他信息：", "本案主要竞争对手：", "已公关人员：", "已合作竞争对手购买式：", "是否来公司考察过：", "跟踪情况：", "备注："]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
//            if i == baseInfos.count-1{
//
//                let progressView = UIView().then { obj in
//                    v.addSubview(obj)
//                    obj.layer.cornerRadius = 8
//                    obj.backgroundColor = lineColor7
//                    obj.snp.makeConstraints { make in
//                        make.left.equalToSuperview().offset(120)
//                        make.centerY.equalToSuperview()
//                        make.width.equalTo(100)
//                        make.height.equalTo(10)
//                    }
//                }
//
//                let progressContentView = UIView().then { obj in
//                    v.addSubview(obj)
//                    obj.layer.cornerRadius = 8
//                    obj.backgroundColor = bgColor9
//                    obj.snp.makeConstraints { make in
//                        make.left.equalToSuperview().offset(120)
//                        make.centerY.equalToSuperview()
//                        make.width.equalTo(0)
//                        make.height.equalTo(10)
//                    }
//                }
//
//                let progressHintLabel = UILabel().then { obj in
//                    v.addSubview(obj)
//                    obj.textColor = bgColor9
//                    obj.font = kRegularFont(12)
//                    obj.text = "0% "
//                    obj.snp.makeConstraints { make in
//                        make.left.equalTo(progressView.snp.right).offset(5)
//                        make.centerY.equalToSuperview()
//                    }
//                }
//
//            }else{
                let contentLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    obj.font = kMediumFont(12)
                    obj.textColor = blackTextColor81
                    if i == 0{
                        infoLabel1 = obj
                        obj.text = model?.commercialExtraInfo?.otherInfo ?? "--"
                    }else if i == 1{
                        infoLabel2 = obj
                        obj.text = model?.commercialExtraInfo?.competitor ?? "--"
                    }else if i == 2{
                        infoLabel3 = obj
                        obj.text = model?.commercialExtraInfo?.publicist ?? "--"
                    }else if i == 3{
                        infoLabel4 = obj
                        obj.text = model?.commercialExtraInfo?.cooperateBuy ?? "--"
                    }else if i == 4{
                        infoLabel5 = obj
                        obj.text = model?.commercialExtraInfo?.goCompany ?? "--"
                    }else if i == 5{
                        infoLabel6 = obj
                        obj.text = model?.commercialExtraInfo?.trackState ?? "--"
                    }else if i == 6{
                        infoLabel7 = obj
                        obj.text = model?.commercialExtraInfo?.remark ?? "--"
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(120)
                        make.centerY.equalToSuperview()
                    }
                }
//            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    
    @objc func addClick(_ button:UIButton){
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
