//
//  AddApprovalInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddApprovalInteractor: AddApprovalInteractorProtocols {
    var presenter: AddApprovalPresenterProtocols?
    
    var entity: AddApprovalEntityProtocols?
    
    func interactorAddApprovalRetrieveData() {
        
    }
    
    func entityAddApprovalRetrieveData() {
        
    }
    
    func didEntityAddApprovalReceiveData() {
        
    }
    
    func didEntityAddApprovalReceiveError() {
        
    }
}
