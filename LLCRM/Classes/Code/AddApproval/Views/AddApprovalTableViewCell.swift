//
//  AddApprovalTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit
import YYText_swift

class AddApprovalTableViewCell: UITableViewCell {
    
    var nameLabel:UILabel?
    
    public var titleStr:String? {
        didSet{
            let attri = NSMutableAttributedString(string:"*")
            attri.yy_color = blackTextColor27
            
            let titleText = titleStr
            let userAttri = NSMutableAttributedString(string: titleText!)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kMediumFont(14)
            attri.append(userAttri)
            
            nameLabel?.attributedText = attri
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        nameLabel = UILabel().then {obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let textView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor13.cgColor
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
                make.height .equalTo(40)
            }
        }
        
        _ = UITextField().then { obj in
            textView.addSubview(obj)
           
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
    }

}
