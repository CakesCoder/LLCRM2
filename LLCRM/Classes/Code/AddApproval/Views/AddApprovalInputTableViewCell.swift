//
//  AddApprovalInputTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit
import YYText_swift

class AddApprovalInputTableViewCell: UITableViewCell {

    var numLabel:UILabel?
    private var isLocationToEnd: Bool = false
    var maxWordsCount:Int = 1000
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        let nameLabel = UILabel().then {obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            
            let attri = NSMutableAttributedString(string:"*")
            attri.yy_color = blackTextColor27
            
            let titleText = "项目进度"
            let userAttri = NSMutableAttributedString(string: titleText)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kMediumFont(14)
            attri.append(userAttri)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let inputView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor6
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor13.cgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
                make.height.equalTo(150)
            }
        }
        
        let textView = YYTextView().then { obj in
            inputView.addSubview(obj)
            obj.placeholderText = "请输入"
            obj.placeholderTextColor = blackTextColor3
            obj.placeholderFont = kRegularFont(14)
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        numLabel = UILabel().then({ obj in
            inputView.addSubview(obj)
            obj.textColor = blackTextColor28
            obj.font = kAMediumFont(14)
            obj.text = "0/1000"
            obj.snp.makeConstraints { make in
                make.right.bottom.equalToSuperview().offset(-15)
            }
        })
        
    }
}

extension AddApprovalInputTableViewCell: YYTextViewDelegate{
    
    func textViewDidChange(_ textView: YYTextView) {
        numLabel?.text = "\(textView.text.count)/1000"
    }
    
    func textView(_ textView: YYTextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "" { // 删除字数
            return true
        }
            
        let inputMode = UITextInputMode.activeInputModes.first?.primaryLanguage
        if inputMode == "zh-Hans" || inputMode == "zh-Hant" { // 简体/繁体中文输入法(拼音、手写输入等)
            if let selectedRange = textView.markedTextRange,
                let _ = textView.position(from: selectedRange.start, offset: 0) { // 如果有高亮的文本，如拼音、手写
                // 拼音输入：联想的过程中，location时刻变化(高亮文本的尾部位置)，length始终为0
                // 手写输入：联想的过程中不会回调该代理方法，确认联想词后才会进入该代理方法
                if range.length > 0 { // 选中联想词后，location变回高亮文本的起始位置，length为高亮文本的长度，需要对字数限制字数
                    return !limitMaxWordsCount(textView, replacementText: text, isLocationToEnd: false)
                }
            } else { // 没有高亮选择的字，对已输入的文字进行字数限制
                if range.location >= maxWordsCount {
    //                        SVProgressHUD.showError(withStatus: "最多可输入\(maxWordsCount)字")
                    return false
                } else { // 处理粘贴引起的字数超限, 截取文本后需要自己去移动光标到尾部
                    return !limitMaxWordsCount(textView, replacementText: text, isLocationToEnd: true)
                }
            }
        } else { // 中文输入法以外的输入法直接进行字数限制
            if range.location >= maxWordsCount {
    //                    SVProgressHUD.showError(withStatus: "最多可输入\(maxWordsCount)字")
                return false
            } else { // 处理粘贴引起的字数超限, 截取文本后需要自己去移动光标到尾部
                return !limitMaxWordsCount(textView, replacementText: text, isLocationToEnd: true)
            }
        }
        return true
    }
    
    func textViewDidChangeSelection(_ textView: YYTextView) {
        let text = textView.text
        if isLocationToEnd, text.utf16.count >= maxWordsCount {
            textView.selectedRange = NSRange(location: maxWordsCount, length: 0)
            isLocationToEnd = false
        }
    }
    
    func limitMaxWordsCount(_ textView: YYTextView, replacementText text: String, isLocationToEnd: Bool) -> Bool {
        let string = textView.text + text
        if string.utf16.count > maxWordsCount {
            textView.text = String(string.prefix(maxWordsCount))
            self.isLocationToEnd = isLocationToEnd
            return true
        }
        return false
    }
}
