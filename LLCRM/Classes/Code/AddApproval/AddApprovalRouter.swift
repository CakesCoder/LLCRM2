//
//  AddApprovalRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddApprovalRouter: AddApprovalRouterProtocols {
    func pushToSchedule(from previousView: UIViewController) {
        
        let nextView = ScheduleViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToMySecheduleDateVC(from previousView: UIViewController) {
        let vc = MyScheduleDateViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
}
