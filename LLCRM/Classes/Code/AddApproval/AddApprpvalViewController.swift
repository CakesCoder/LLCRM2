//
//  AddApprpvalViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit
import IQKeyboardManagerSwift
import YYText_swift

class AddApprpvalViewController: BaseViewController {
    
    let datas = ["客户名称", "项目名称", "来访人数", "预估总费用", "项目进度"]
    var presenter: AddApprovalPresenterProtocols?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "审批"
        
//        IQKeyboardManager.shared.registerTextFieldViewClass(YYTextView.self, didBeginEditingNotificationName: "TextViewTextDidBeginEditing", didEndEditingNotificationName: "TextViewTextDidEndEditing")
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddApprpvalViewController.backClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem.createcommitBarbuttonItem(name: "提交", target: self, action: #selector(AddApprpvalViewController.addClick))
        
        // Do any additional setup after loading the view.
        buildUI()
    }
    
    func buildUI(){
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true

            obj.register(AddApprovalTableViewCell.self, forCellReuseIdentifier: "AddApprovalTableViewCell")
            obj.register(AddApprovalInputTableViewCell.self, forCellReuseIdentifier: "AddApprovalInputTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(){
        presenter?.router?.pushToMySecheduleDateVC(from: self)
    }
    
    func cofing(){
        let router = AddApprovalRouter()
        
        presenter = AddApprovalPresenter()
        
        presenter?.router = router
        
        let entity = AddApprovalEntity()
        
        let interactor = AddApprovalInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddApprpvalViewController: AddApprovalViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterAddApprovalViewReceiveData() {
        
    }
    
    func didPresenterAddApprovalViewReceiveError() {
        
    }
}

extension AddApprpvalViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if datas.count - 1 == indexPath.row{
            return 250
        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if datas.count - 1 == indexPath.row{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AddApprovalInputTableViewCell", for: indexPath) as! AddApprovalInputTableViewCell
            return cell
            
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddApprovalTableViewCell", for: indexPath) as! AddApprovalTableViewCell
        cell.titleStr = datas[indexPath.row]
        return cell
    }
    
    
}
