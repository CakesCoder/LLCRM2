//
//  AddApprovalProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddApprovalProtocols: NSObject {

}

protocol AddApprovalViewProtocols: AnyObject {
    var presenter: AddApprovalPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterAddApprovalViewReceiveData()
    func didPresenterAddApprovalViewReceiveError()
}

protocol AddApprovalPresenterProtocols: AnyObject{
    var view: AddApprovalViewProtocols? { get set }
    var router: AddApprovalRouterProtocols? { get set }
    var interactor: AddApprovalInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    func didInteractorAddApprovalReceiveData()
    func didInteractorAddApprovalReceiveError()
}

protocol AddApprovalInteractorProtocols: AnyObject {
    var presenter: AddApprovalPresenterProtocols? { get set }
    var entity: AddApprovalEntityProtocols? { get set }
    
    func interactorAddApprovalRetrieveData()
    func entityAddApprovalRetrieveData()
    func didEntityAddApprovalReceiveData()
    func didEntityAddApprovalReceiveError()
}

protocol AddApprovalEntityProtocols: AnyObject {
    var interactor: AddApprovalInteractorProtocols? { get set }
    
    func retrieveAddApprovalData()
    func didAddApprovalReceiveData()
}

protocol AddApprovalRouterProtocols: AnyObject {
    func pushToSchedule(from previousView: UIViewController)
    func pushToMySecheduleDateVC(from previousView: UIViewController) 
}

