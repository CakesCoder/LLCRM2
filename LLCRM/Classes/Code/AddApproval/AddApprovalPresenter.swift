//
//  AddApprovalPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddApprovalPresenter: AddApprovalPresenterProtocols {
    var view: AddApprovalViewProtocols?
    
    var router: AddApprovalRouterProtocols?
    
    var interactor: AddApprovalInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorAddApprovalReceiveData() {
        
    }
    
    func didInteractorAddApprovalReceiveError() {
        
    }
}
