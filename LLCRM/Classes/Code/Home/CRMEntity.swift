//
//  Entity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import Foundation
import SwiftyJSON


class CRMEntity: CRMEntityProtocols{
    weak var interactor: CRMInteractorProtocols?
    
    func presenterRequestHomeClub(by params: Any?){
        LLNetProvider.request(.homeClue(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                printTest(jsonData)
                self.didEntityHomeClubReceiveData(by:jsonData)
            case .failure(_):
                self.didEntityHomeClubReceiveError(error: .requestError)
            }
        }
    }
    
    func didEntityHomeClubReceiveData(by params: Any?){
        interactor?.didEntityHomeClubReceiveData(by: params)
    }
    
    func didEntityHomeClubReceiveError(error: MyError?){
        interactor?.didEntityHomeClubReceiveError(error: error)
    }
    
    func presenterRequestHomeSchedule(by params: Any?){
        LLNetProvider.request(.homeSchedule(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                printTest(jsonData)
                self.didEntityHomeScheduleReceiveData(by:jsonData)
            case .failure(_):
                self.didEntityHomeScheduleReceiveError(error: .requestError)
            }
        }
    }
    
    func didEntityHomeScheduleReceiveData(by params: Any?){
        interactor?.didEntityHomeScheduleReceiveData(by: params)
    }
    
    func didEntityHomeScheduleReceiveError(error: MyError?){
        interactor?.didEntityHomeScheduleReceiveError(error: error)
    }
    
    func presenterRequestHomePersonalSelling(by params: Any?) {
        LLNetProvider.request(.personalSelling(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                printTest(jsonData)
                self.didEntityHomePersonalSellingReceiveData(by:jsonData)
            case .failure(_):
                self.didEntityHomePersonalSellingReceiveError(error: .requestError)
            }
        }
    }
    
    func didEntityHomePersonalSellingReceiveData(by params: Any?) {
        interactor?.didEntityHomePersonalSellingReceiveData(by: params)
    }
    
    func didEntityHomePersonalSellingReceiveError(error: MyError?) {
        interactor?.didEntityHomeCommercialReceiveError(error: error)
    }
    
    func presenterRequestHomeCommercial(by params: Any?){
        LLNetProvider.request(.homeCommercial(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                printTest(jsonData)
                self.didEntityHomeCommercialReceiveData(by:jsonData)
            case .failure(_):
                self.didEntityHomeCommercialReceiveError(error: .requestError)
            }
        }
    }
    
    func didEntityHomeCommercialReceiveData(by params: Any?) {
        interactor?.didEntityHomeCommercialReceiveData(by: params)
    }
    
    func didEntityHomeCommercialReceiveError(error: MyError?) {
        interactor?.didEntityHomeCommercialReceiveError(error: error)
    }
    
    func didMarketDataReceiveData(by params: Any?) {
        interactor?.didEntityMarketDataReceiveData(by: params)
    }
    
    func didMarketDataReceiveError(error: MyError?) {
        interactor?.didEntityMarketDataReceiveError(error: error)
    }
    
    func getMarketDataRequest(by params: Any?) {
        LLNetProvider.request(.marketData(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didMarketDataReceiveData(by:jsonData)
            case .failure(_):
                self.didMarketDataReceiveError(error: .requestError)
            }
        }
    }
    
    
    func retrieveData(by params: Any) {
        printTest("【调用顺序】step5: 通过entity发起 数据请求")
//        LLNetProvider.request(.login(params as! [String : Any])) { result in
//            switch result{
//            case let .success(response):
//                let data = response.data
//                let jsonData = try? JSON(data: data)
//
//                if let json = jsonData {
//
//                }
//            case .failure(_):
//                self.didReceiveError(error: .requestError)
//            }
//        }
//        MyViperNetProvider.request(.channel) { result in
//            printTest("【调用顺序】step6: 2秒后，数据回调")
//            switch result {
//            case let .success(response):
//                let data = response.data
//                let jsonData = try? JSON(data: data)
//                if let json = jsonData {
//                    let channelModelArray = JHChannelModelArray(json: json)
//                    self.didReceiveData(data: channelModelArray)
//                }
//
//            case .failure(_):
//                self.didReceiveError(error: .requestError)
//
//            }
//        }
    }
    
//    func didReceiveData(data: Any) {
//        printTest("【调用顺序】step7: 执行数据回调")
//        interactor?.didEntityReceiveData(data: data)
//    }
//
//    func didReceiveError(error: MyError) {
//        interactor?.didEntityReceiveError(error: error)
//    }
}
