//
//  Interactors.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import Foundation

class CRMInteractors: CRMInteractorProtocols {
    
    var presenter: CRMPresenterProtocols?
    var entity: CRMEntityProtocols?
    
//    func interactorRetrieveData(by params: Any) {
//        printTest("【调用顺序】step4: 通过entity发起 数据请求")
//        entity?.retrieveData(by: params)
//    }
//
//    func didEntityReceiveData(data: Any) {
//        printTest("【调用顺序】step8: interarctor执行数据回调")
//        presenter?.didInteractorRetrieveData(data: data)
//    }
//
//    func didEntityReceiveError(error: MyError) {
//        presenter?.didInteractorReceiveError(error: error)
//    }
    
    func presenterRequestHomePersonalSelling(by params: Any?){
        printTest("【调用顺序】step4: 通过entity发起 数据请求")
        entity?.presenterRequestHomePersonalSelling(by: params)
    }
    
    func didEntityHomePersonalSellingReceiveData(by params: Any?){
        presenter?.didInteractorHomePersonalSellingRetrieveData(data: params)
    }
    
    func didEntityHomePersonalSellingReceiveError(error: MyError?){
        presenter?.didInteractorHomePersonalSellingReceiveError(error: error!)
    }
    
    func presenterRequestHomeCommercial(by params: Any?){
        entity?.presenterRequestHomeCommercial(by: params)
    }
    
    func didEntityHomeCommercialReceiveData(by params: Any?){
        presenter?.didInteractorHomeCommercialRetrieveData(data:params)
    }
    
    func didEntityHomeCommercialReceiveError(error: MyError?){
        presenter?.didInteractorHomeCommercialReceiveError(error: error)
    }
    
    func presenterRequestHomeSchedule(by params: Any?) {
        entity?.presenterRequestHomeSchedule(by: params)
    }
    
    func didEntityHomeScheduleReceiveData(by params: Any?) {
        presenter?.didInteractorHomeClubRetrieveData(data: params)
    }
    
    func didEntityHomeScheduleReceiveError(error: MyError?) {
        presenter?.didInteractorHomeClubReceiveError(error: error)
    }
    
    func presenterRequestHomeClub(by params: Any?) {
        entity?.presenterRequestHomeClub(by: params)
    }
    
    func didEntityHomeClubReceiveData(by params: Any?) {
        presenter?.didInteractorHomeClubRetrieveData(data: params)
    }
    
    func didEntityHomeClubReceiveError(error: MyError?) {
        presenter?.didInteractorHomeClubReceiveError(error: error)
    }
    
    func presenterRequestMarketData(by params: Any?) {
        entity?.getMarketDataRequest(by: params)
    }
    
    func didEntityMarketDataReceiveData(by params: Any?) {
        presenter?.didGetMarketDataInteractorReceiveData(by: params)
    }
    
    func didEntityMarketDataReceiveError(error: MyError?) {
        presenter?.didGetMarketDataInteractorReceiveError(error: error)
    }
}
