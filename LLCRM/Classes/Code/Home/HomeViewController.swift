//
//  HomeViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/26.
//

import UIKit
import PKHUD
import HandyJSON

class HomeViewController: BaseViewController {
    var presenter: CRMPresenterProtocols?
    var tableView: HomeTableView?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.blueNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildUI()
    }
    
    func buildUI(){
        
        tableView = HomeTableView()
        view.addSubview(tableView!)
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(HomeViewController.backClick))
        
        tableView?.snp.makeConstraints({ make in
            make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
            make.left.equalToSuperview().offset(0)
            make.bottom.right.equalToSuperview().offset(-0)
        })
        
        cofing()
        block()
    }
    
    func cofing(){
        let router = CRMRouter()
        
        presenter = CRMPresenters()
        presenter?.router = router
        
        let entity = CRMEntity()
        
        let interactor = CRMInteractors()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
    }
    
    func block(){
        tableView?.didClickHomePatternCell = { [self] tag in
            if tag == 1000{
                self.presenter?.router?.pushToCRMPMessageList(from: self)
            }else if tag == 1001{
                self.presenter?.router?.pushToHomeClient(from: self)
            }else if tag == 1002{
                self.presenter?.router?.pushToSellLeadsV(from: self)
            }else if tag == 1003{
                self.presenter?.router?.pushDataCockpit(from: self)
            }else if tag == 1004{
                self.presenter?.router?.pushToBusinessList(from: self)
            }else if tag == 1005 {
                self.presenter?.router?.pushToHomeLinkMan(from: self)
            }else if tag == 1006 {
                self.presenter?.router?.pushToSampleList(from: self)
            }else if tag == 1007{
                self.presenter?.router?.pushToPriceList(from: self)
            }else if tag == 1008{
                self.presenter?.router?.pushCustomerService(from: self)
            }else if tag == 1010 {
                self.presenter?.router?.pushToMoreDetail(from: self)
            }else if tag == 1009 {
                self.presenter?.router?.pushToVisitList(from: self)
            }else if tag == 2000{
                self.presenter?.router?.pushToMarketData(from: self)
            }
        }
        tableView?.didClickCellHandler = { [self] model in
        }
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc private func backClick(){
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeViewController: CRMViewProtocols{
    func didGetHomeClubPresenterReceiveData(by params: Any?) {
        printTest("3333333333")
        printTest(params)
    }
    
    func didGetHomeClubPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetHomeSchedulePresenterReceiveData(by params: Any?) {
        printTest("111111111111")
        printTest(params)
    }
    
    func didGetHomeSchedulePresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetHomeCommercialPresenterReceiveData(by params: Any?) {
        printTest("22222222222222")
        printTest(params)
    }
    
    func didGetHomeCommercialPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetHomePersonalSellinglPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<HomeDataInfosModel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    printTest(dataModel )
                    tableView?.homeDataInfosModel = dataModel
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetHomePersonalSellingsssPresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetMarketDataPresenterReceiveData(by params: Any?){
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if dataDic != nil{
                    if let model = JSONDeserializer<MarkDataModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                        tableView?.markDataModel = model
                    }
                }
            }
        }
    }
    
    func didGetMarketDataPresenterReceiveError(error: MyError?){
        
    }
}



