//
//  CRMProtocals.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import Foundation

protocol CRMInteractorProtocols: AnyObject {
    //  ----------- Part 1: 对上游 presenter -----------
    // （实现的时候）弱引用presenter
    var presenter: CRMPresenterProtocols? { get set }
    // 可以强引用
    var entity: CRMEntityProtocols? { get set }
    
    // 上游主动发起 <请求数据> 方法
//    func interactorRetrieveData(sby params: Any)
    
    //  ----------- Part 2: 对下游 entity -----------
    
    // 下游回调方法 <收到数据回调>
//    func didEntityReceiveData(data: Any)
//    func didEntityReceiveError(error: MyError)
    
    func presenterRequestHomePersonalSelling(by params: Any?)
    func didEntityHomePersonalSellingReceiveData(by params: Any?)
    func didEntityHomePersonalSellingReceiveError(error: MyError?)
    
    
    func presenterRequestHomeCommercial(by params: Any?)
    func didEntityHomeCommercialReceiveData(by params: Any?)
    func didEntityHomeCommercialReceiveError(error: MyError?)
    
    func presenterRequestHomeSchedule(by params: Any?)
    func didEntityHomeScheduleReceiveData(by params: Any?)
    func didEntityHomeScheduleReceiveError(error: MyError?)
    
    func presenterRequestHomeClub(by params: Any?)
    func didEntityHomeClubReceiveData(by params: Any?)
    func didEntityHomeClubReceiveError(error: MyError?)
    
    func presenterRequestMarketData(by params: Any?)
    func didEntityMarketDataReceiveData(by params: Any?)
    func didEntityMarketDataReceiveError(error: MyError?)
}

protocol CRMPresenterProtocols: AnyObject {
    // ----------- Part 1: 对上游view -----------
    // (实现的时候)弱引用view
    var view: CRMViewProtocols? { get set }
    
    /*
     这些方法，就是presenter需要做的事，或者说司presenter应该具备的能力
     */
    // viewDidLoad需要处理的业务逻辑
    func viewDidLoad()
    
    // ----------- Part 2: 对平级 router ---------
    // 可以强引用router,因为router是被preseter单方面持有
    var router: CRMRouterProtocols? { get set }
    
    // ------------- Part 3: 对下游 interactor ------------
    // presenter作为interactor的上游，可以强引用
    var interactor: CRMInteractorProtocols? { get set }
    var result: [Any]? { get set }
    // 下游回调方法
    func presenterRequestGetHomeCommercial(by params: Any?)
    func didInteractorHomePersonalSellingRetrieveData(data: Any?)
    func didInteractorHomePersonalSellingReceiveError(error: MyError?)
    
    func presenterRequestGetHomePersonalSelling(by params: Any?)
    func didInteractorHomeCommercialRetrieveData(data: Any?)
    func didInteractorHomeCommercialReceiveError(error: MyError?)
    
    func presenterRequestGetSchedule(by params: Any?)
    func didInteractorHomeScheduleRetrieveData(data: Any?)
    func didInteractorHomeScheduleReceiveError(error: MyError?)
    
    func presenterRequestGetClub(by params: Any?)
    func didInteractorHomeClubRetrieveData(data: Any?)
    func didInteractorHomeClubReceiveError(error: MyError?)
    
    func presenterRequestGetMarketData(by params: Any?)
    func didGetMarketDataInteractorReceiveData(by params: Any?)
    func didGetMarketDataInteractorReceiveError(error: MyError?)
}

protocol CRMViewProtocols: AnyObject{
    // 持有一个presenter，帮我做事
    var presenter: CRMPresenterProtocols? { get set }
    
    /*
     这些方法，都是UI回调相关的方法
     让我的presenter帮我做事情，等presenter给我回调结果就好了
     */
    func showLoading()
    func showError()
    func hideLoading()
    func didGetHomePersonalSellinglPresenterReceiveData(by params: Any?)
    func didGetHomePersonalSellingsssPresenterReceiveError(error: MyError?)
    
    func didGetHomeCommercialPresenterReceiveData(by params: Any?)
    func didGetHomeCommercialPresenterReceiveError(error: MyError?)
    
    func didGetHomeSchedulePresenterReceiveData(by params: Any?)
    func didGetHomeSchedulePresenterReceiveError(error: MyError?)
    
    func didGetHomeClubPresenterReceiveData(by params: Any?)
    func didGetHomeClubPresenterReceiveError(error: MyError?)
    
    func didGetMarketDataPresenterReceiveData(by params: Any?)
    func didGetMarketDataPresenterReceiveError(error: MyError?)
    
}

protocol CRMRouterProtocols: AnyObject{
    
    func pushToHomeClient(from previousView: UIViewController)
    func pushToHomeLinkMan(from previousView: UIViewController)
    func pushToBusinessList(from previousView: UIViewController)
    func pushToCRMPMessageList(from previousView: UIViewController)
    func pushToSampleList(from previousView: UIViewController)
    func pushToMoreDetail(from previousView: UIViewController)
    func pushToMarketData(from previousView: UIViewController)
    func pushDataCockpit(from previousView: UIViewController)
    func pushCustomerService(from previousView: UIViewController)
    func pushToPriceList(from previousView: UIViewController)
    func pushToSellLeadsV(from previousView: UIViewController)
    func pushToVisitList(from previousView: UIViewController)
}

protocol CRMEntityProtocols:AnyObject{
    /*
     注意：这里如果不负责对interactor数据回调，应该不用引用interactor
          因为，通过一个请求数据方法，就可以满足需求了
     */
    // 对上游，interactor
    // 应该弱引用
    var interactor: CRMInteractorProtocols? { get set }
    // 请求数据
//    func retrieveData(by params: Any)
//    // 收到数据
//    func didReceiveData(data: Any)
//    func didReceiveError(error: MyError)
//    func didPersonalSellingReceiveData(by params: Any?)
    
    func presenterRequestHomePersonalSelling(by params: Any?)
    func didEntityHomePersonalSellingReceiveData(by params: Any?)
    func didEntityHomePersonalSellingReceiveError(error: MyError?)
    
    func presenterRequestHomeCommercial(by params: Any?)
    func didEntityHomeCommercialReceiveData(by params: Any?)
    func didEntityHomeCommercialReceiveError(error: MyError?)
    
    func presenterRequestHomeSchedule(by params: Any?)
    func didEntityHomeScheduleReceiveData(by params: Any?)
    func didEntityHomeScheduleReceiveError(error: MyError?)
    
    func presenterRequestHomeClub(by params: Any?)
    func didEntityHomeClubReceiveData(by params: Any?)
    func didEntityHomeClubReceiveError(error: MyError?)
    
    func didMarketDataReceiveData(by params: Any?)
    func didMarketDataReceiveError(error: MyError?)
    func getMarketDataRequest(by params: Any?)
}
