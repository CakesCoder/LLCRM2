//
//  Presenters.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import Foundation

class CRMPresenters: CRMPresenterProtocols {
    
    
    var view: CRMViewProtocols?
    var router: CRMRouterProtocols?
    var interactor: CRMInteractorProtocols?
    var result: [Any]? = []
    
    func viewDidLoad() {
        view?.showLoading()
        printTest("【调用顺序】step3: 通过presenter发起 数据请求")
        interactor?.presenterRequestHomePersonalSelling(by: ["source":1])
//        interactor?.presenterRequestHomeCommercial(by: ["id":2471])
//        interactor?.presenterRequestHomeSchedule(by: ["id":2471])
//        interactor?.presenterRequestHomeClub(by: ["id":2471])
        
        let now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateString = dateFormatter.string(from: now)
        presenterRequestGetMarketData(by: ["dateTime":dateString])
    }
    
    func presenterRequestGetHomePersonalSelling(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestHomePersonalSelling(by: ["source":1])
    }

    func presenterRequestGetHomeCommercial(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestHomeCommercial(by: params)
    }
    
    func presenterRequestGetSchedule(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestHomeSchedule(by: params)
    }

    func didInteractorHomePersonalSellingRetrieveData(data: Any?) {
        view?.hideLoading()
        printTest("【调用顺序】step9: preseter执行数据回调")
        view?.didGetHomePersonalSellinglPresenterReceiveData(by: data)
    }
    
    func didInteractorHomePersonalSellingReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetHomePersonalSellingsssPresenterReceiveError(error: error)
    }
    
    func didInteractorHomeCommercialRetrieveData(data: Any?){
        view?.hideLoading()
        view?.didGetHomeCommercialPresenterReceiveData(by: data)
    }
    
    func didInteractorHomeCommercialReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetHomeCommercialPresenterReceiveError(error: error)
    }
    
    func didInteractorHomeScheduleRetrieveData(data: Any?){
        view?.hideLoading()
        view?.didGetHomeSchedulePresenterReceiveData(by: data)
    }
    
    func didInteractorHomeScheduleReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetHomeSchedulePresenterReceiveError(error: error)
    }
    
    func presenterRequestGetClub(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestHomeClub(by: [:])
    }
    
    func didInteractorHomeClubRetrieveData(data: Any?) {
        view?.hideLoading()
        view?.didGetHomeClubPresenterReceiveData(by: data)
    }
    
    func didInteractorHomeClubReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetHomeClubPresenterReceiveError(error: error)
    }
    
    func presenterRequestGetMarketData(by params: Any?) {
//        view?.showLoading()
        interactor?.presenterRequestMarketData(by: params)
    }
    
    func didGetMarketDataInteractorReceiveData(by params: Any?) {
//        view?.hideLoading()
        view?.didGetMarketDataPresenterReceiveData(by: params)
    }
    
    func didGetMarketDataInteractorReceiveError(error: MyError?) {
//        view?.hideLoading()
//        view?.showError()
        view?.didGetMarketDataPresenterReceiveError(error: error)
    }
}
