//
//  HomeStatisticsCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/1.
//

import UIKit

class HomeStatisticsCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let statisticsImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_statistics")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
        
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "商机2.0数量/金额统计"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(statisticsImageV.snp.right).offset(5)
                make.centerY.equalTo(statisticsImageV)
            }
        })
        
        let pushButton = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(statisticsImageV)
            }
        }
        
        
        _ = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
            
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(statisticsImageV)
            }
        }
        
        let sumLabel = UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "商机总数："
            obj.textColor = blackTextColor11
            obj.font = kAMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(37)
                make.top.equalTo(titleLabel.snp.bottom).offset(18)
            }
        }
        
        let sumDataLabel = UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "66"
            obj.textColor = blackTextColor3
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(sumLabel.snp.right).offset(0)
                make.centerY.equalTo(sumLabel)
            }
        }
        
        let moneyLabel = UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "商机金额："
            obj.textColor = blackTextColor11
            obj.font = kAMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(37)
                make.top.equalTo(sumLabel.snp.bottom).offset(10)
            }
        }
        
        let moneyDataLabel = UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "888.00"
            obj.textColor = blackTextColor3
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(moneyLabel.snp.right).offset(0)
                make.centerY.equalTo(moneyLabel)
            }
        }
        
        let dateLabel =  UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-25)
                make.bottom.equalToSuperview().offset(-13)
            }
        }
        
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        
    }
}
