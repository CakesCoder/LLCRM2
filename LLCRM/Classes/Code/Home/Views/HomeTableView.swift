//
//  HomeTableView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import UIKit

class HomeTableView: UIView {
    typealias didClickCellClosure = (_ model: ChannelModel) -> Void
    public var didClickCellHandler: didClickCellClosure?
    
    typealias didClickHomePatternCell = (_ tag: Int) -> Void
    public var didClickHomePatternCell: didClickHomePatternCell?
    
    private var tableView: UITableView = UITableView()
    private var dataInfosModel = HomeDataInfosModel()
    var markDataModel: MarkDataModel?{
        didSet{
            tableView.reloadData()
        }
    }
    var homeDataInfosModel: HomeDataInfosModel?{
        didSet{
            tableView.reloadData()
        }
    }
    
    private let serialQueue = DispatchQueue(label: "com.HomeTableView")
    
    override init(frame: CGRect) {
        super.init(frame: frame)
         
        setUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func setData(data: HomeDataInfosModel){
        dataInfosModel = data
    }
    
    public func reloadView(){
        DispatchQueue.main.async { [self] in
            tableView.reloadData()
        }
    }
    
    private func setUI(){
        tableView = UITableView().then { obj in
            self.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            if #available(iOS 11.0, *) {
//                obj.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
            } else {
                // Fallback on earlier versions
            }
            obj.register(HomePatternCell.self, forCellReuseIdentifier: "HomePatternCell")
            obj.register(HomeDataInfosCell.self, forCellReuseIdentifier: "HomeDataInfosCell")
            obj.register(HomeSellDataCell.self, forCellReuseIdentifier: "HomeSellDataCell")
            obj.register(HomeTimeCell.self, forCellReuseIdentifier: "HomeTimeCell")
            obj.register(HomeStatisticsCell.self, forCellReuseIdentifier: "HomeStatisticsCell")
            obj.register(HomeReasonCell.self, forCellReuseIdentifier: "HomeReasonCell")
            obj.register(HomeTransformCell.self, forCellReuseIdentifier: "HomeTransformCell")
            obj.register(HomeVisitCell.self, forCellReuseIdentifier: "HomeVisitCell")
            obj.register(HomePersonOrderCell.self, forCellReuseIdentifier: "HomePersonOrderCell")
            obj.register(HomeSellMoneyCell.self, forCellReuseIdentifier: "HomeSellMoneyCell")
            obj.register(HomeVisitChartCell.self, forCellReuseIdentifier: "HomeVisitChartCell")
            obj.register(HomeDepartmentalCell.self, forCellReuseIdentifier: "HomeDepartmentalCell")
            obj.register(ReturnMoneyCell.self, forCellReuseIdentifier: "ReturnMoneyCell")
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
    }
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}

extension HomeTableView: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        if indexPath.row == 9{
            self.didClickHomePatternCell?(2000)
        }
    }
}

extension HomeTableView: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("self.dataSource.array.count = \(self.dataSource.array.count)")
//        return self.dataSource.array.count
        return 13
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return 295
        }else if indexPath.row == 1{
            return 66
        }else if indexPath.row == 2{
            return 215
        }else if indexPath.row == 3{
            return 345
        }else if indexPath.row == 4{
            return 115
        }else if indexPath.row == 5{
            return 262
        }else if indexPath.row == 6{
            return 317
        }else if indexPath.row == 7{
            return 194
        }else if indexPath.row == 8{
            return 325
        }else if indexPath.row == 9{
            return 332
        }else if indexPath.row == 10 || indexPath.row == 11{
            return 348
        }
        return 374
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomePatternCell", for: indexPath) as! HomePatternCell
            cell.didClickPatternCell = { [self] tag in
                self.didClickHomePatternCell?(tag)
            }
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDataInfosCell", for: indexPath) as! HomeDataInfosCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSellDataCell", for: indexPath) as! HomeSellDataCell
            cell.markDataModel = homeDataInfosModel
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTimeCell", for: indexPath) as! HomeTimeCell
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeStatisticsCell", for: indexPath) as! HomeStatisticsCell
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeReasonCell", for: indexPath) as! HomeReasonCell
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeTransformCell", for: indexPath) as! HomeTransformCell
            return cell
        }else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVisitCell", for: indexPath) as! HomeVisitCell
            return cell
        }else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomePersonOrderCell", for: indexPath) as! HomePersonOrderCell
            return cell
        }else if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeSellMoneyCell", for: indexPath) as! HomeSellMoneyCell
            cell.markDataModel = markDataModel
            return cell
        }else if indexPath.row == 10{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeVisitChartCell", for: indexPath) as! HomeVisitChartCell
            return cell
        }else if indexPath.row == 11{
            let cell = tableView.dequeueReusableCell(withIdentifier: "HomeDepartmentalCell", for: indexPath) as! HomeDepartmentalCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnMoneyCell", for: indexPath) as! ReturnMoneyCell
        return cell
    }
}
