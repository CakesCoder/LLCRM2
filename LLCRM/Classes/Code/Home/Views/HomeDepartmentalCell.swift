//
//  HomeDepartmentalCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/4.
//

import UIKit
//import AAInfographics

class HomeDepartmentalCell: UITableViewCell {
    
    var aaChartView: AAChartView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let departmentalImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_departmental")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
    
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "部门业绩目标完成率排行"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(departmentalImageV.snp.right).offset(5)
                make.centerY.equalTo(departmentalImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
//        let chartViewWidth  = Int(kScreenWidth)
//        let chartViewHeight = 300
//        aaChartView = AAChartView()
//        aaChartView?.frame = CGRect(x: 0,
//                                    y: 40,
//                                    width: chartViewWidth,
//                                    height: chartViewHeight)
//        self.addSubview(aaChartView!)
//
//        let dataElement = AADataElement()
//            .y(1.25)
//            .dataLabels(
//                AADataLabels()
//                    .enabled(true)
//                    .format("{y} 美元🇺🇸💲")
////                    .x(3)
////                    .verticalAlign(.middle)
//                    .style(AAStyle(color: AAColor.red, fontSize: 20, weight: .bold, outline: "1px 1px contrast").backgroundColor(AAColor.blue)))
//            .toDic()!
//
//        let singleSpecialData = AADataElement()
//            .color(AAGradientColor.freshPapaya)
//            .y(49.5)
//            .toDic()!
//
//        let formatStr = (
//            "<img src=https://www.highcharts.com/samples/graphics/sun.png>"
//          + "<span style=color:#FFFFFF;font-weight:thin;font-size:25px>{y}</span>"
//          + "<span style=color:#FFFFFF;font-weight:thin;font-size:17px> m</span>"
//        )
//
//        let singleSpecialData1 = AADataElement()
//            .dataLabels(AADataLabels()
//                .enabled(true)
//                .useHTML(true)
//                .format(formatStr)
//                .style(AAStyle()
//                    .fontWeight(.bold)
//                    .color(AAColor.white)
//                    .fontSize(16))
//                .y(-35)
//                .align(.center)
//                .verticalAlign(.top)
//                .overflow("none")
//                .crop(false))
//            .y(26.5)
//            .toDic()!
//
//        let chartModel = AAChartModel()
//                .chartType(.bar)//图表类型
//                .animationType(.bounce)
//                .dataLabelsEnabled(true)
//    //                .categories(["王小虎"])
//                    .tooltipEnabled(true)
//                    .categories(["10万","20万","30万","40万", "50万","60万"])
////                .dataLabelsStyle(AAStyle(color: "#FF7F00"))
////                .valueDecimals(2)
//                .series([
//                    AASeriesElement()
//                        .name("货币")
//                        .data([dataElement,singleSpecialData, singleSpecialData1])
////                        AASeriesElement()
////                            .name("目标值")
////                            .data([NSNull()])
////                            .color("#409EFF"),
////                        AASeriesElement()
////                            .name("王小虎")
////                            .data(["50万"])
////                            .dataLabels(AADataLabels().color("#3B3B3B"))
////                            .color("#31C675"),
////                        AASeriesElement()
////                            .name("完成率")
////                            .data([NSNull()])
////                            .color("#FF7F00"),
//                        ])
//
//        /*图表视图对象调用图表模型对象,绘制最终图形*/
//        aaChartView?.aa_drawChartWithChartModel(chartModel)
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 300
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        self.addSubview(aaChartView!)
        
        let chartModel = AAChartModel()
                .chartType(.bar)//图表类型
                .animationType(.bounce)
                .dataLabelsEnabled(true)
                .categories(["王小虎"])
                .dataLabelsStyle(AAStyle(color: "#525252", fontSize: 10).backgroundColor("#16B5AE").height(200))
//                .dataLabelsStyle(AAStyle(color: "#FF7F00"))
//                .valueDecimals(2)
                .series([
                        AASeriesElement()
                            .name("目标值")
                            .data([NSNull()])
                            .color("#409EFF"),
                        AASeriesElement()
                            .name("王小虎")
                            .data([0.9])
                            .color("#31C675"),
                        AASeriesElement()
                            .name("完成率")
                            .data([NSNull()])
                            .color("#FF7F00"),
                        ])
        
        /*图表视图对象调用图表模型对象,绘制最终图形*/
        aaChartView?.aa_drawChartWithChartModel(chartModel)
        
        
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
    }

}
