//
//  HomeVisitCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/2.
//

import UIKit

class HomeVisitCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
    
        self.selectionStyle = .none
    
        let statisticsImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_client")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
    
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "拜访客户数龙虎榜（按员工）"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(statisticsImageV.snp.right).offset(5)
                make.centerY.equalTo(statisticsImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(statisticsImageV)
            }
        }
    
        _ = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(statisticsImageV)
            }
        }
    
        let bgView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.borderColor = lineColor5.cgColor
            obj.layer.borderWidth = 1
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalTo(titleLabel.snp.bottom).offset(10)
                make.bottom.equalToSuperview().offset(-37)
                make.right.equalToSuperview().offset(-25)
            }
        }
    
        let headView = UIView().then({ obj in
            bgView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.height.equalTo(30)
                make.right.equalToSuperview().offset(-0)
            }
        })
    
        //  归属
        let ownershipLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "归属部门"
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(70)
            }
        }
        
        //归属分割线
        let ownershipLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.left.equalTo(ownershipLabel.snp.right).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        //拜访次数
        let callNumLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "拜访次数"
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.width.equalTo(140)
            }
        }
        
        //拜访分割线
        let callNumLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.right.equalTo(callNumLabel.snp.left).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        //负责人
        let chargeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "负责人"
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.left.equalTo(ownershipLineView.snp.right).offset(0)
                make.right.equalTo(callNumLineView.snp.left).offset(-0)
            }
        }
        
        _ = UIView().then({ obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.bottom.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(0.5)
            }
        })
        
        let sumView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = bgColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(30)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(headView.snp.bottom).offset(0)
            }
        }
        
        let sumNumLabel = UILabel().then { obj in
            sumView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "4"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor11
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.width.equalTo(140)
            }
        }
        
        //总计分割线
        let sumNumLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.right.equalTo(sumNumLabel.snp.left).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        _ = UILabel().then { obj in
            sumView.addSubview(obj)
            obj.text = "总计"
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor11
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalTo(sumNumLineView.snp.left).offset(-0)
            }
        }
        
        _ = UIView().then { obj in
            sumView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.bottom.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(0.5)
            }
        }
        
        let teamLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "欧荣俊团队"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(sumView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(70)
            }
        }
        
        //团队分割线
        let teamLineView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.left.equalTo(teamLabel.snp.right).offset(0)
                make.top.equalTo(sumView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        let nameView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalTo(teamLineView.snp.right).offset(0)
                make.top.equalTo(sumView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(30)
            }
        }
        
        let nameNumLabel = UILabel().then { obj in
            nameView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "4"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor11
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.width.equalTo(140)
            }
        }
        
        //分割线
        let nameNumLineView = UIView().then { obj in
            nameView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.right.equalTo(nameNumLabel.snp.left).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        let nameContentLabel = UILabel().then { obj in
            nameView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "王小虎"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.right.equalTo(nameNumLineView.snp.left).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
            }
        }
        
        _ = UIView().then { obj in
            nameView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.bottom.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(0.5)
            }
        }
        
        let subtotalView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = bgColor2
            obj.snp.makeConstraints { make in
                make.left.equalTo(teamLineView.snp.right).offset(0)
                make.top.equalTo(nameView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(30)
            }
        }
        
        let subtotalNumLabel = UILabel().then { obj in
            subtotalView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "4"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor11
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.width.equalTo(140)
            }
        }
        
        //分割线
        let subtotalNumLineView = UIView().then { obj in
            subtotalView.addSubview(obj)
            obj.backgroundColor = lineColor9
            obj.snp.makeConstraints { make in
                make.right.equalTo(subtotalNumLabel.snp.left).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        let subtotalContentLabel = UILabel().then { obj in
            subtotalView.addSubview(obj)
            obj.textAlignment = .center
            obj.text = "小计"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.right.equalTo(subtotalNumLineView.snp.left).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
            }
        }
        
    
        _ =  UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-25)
                make.bottom.equalToSuperview().offset(-13)
            }
        }
    
    
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
}
