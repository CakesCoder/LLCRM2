//
//  HomeDataInfosCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/30.
//

import UIKit

class HomeDataInfosCell: UITableViewCell {
    var nameLabel:UILabel?
    var userName:String? = ""
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        if let userInfosName = LLUserDefaultsManager.shared.readUserNameInfos(){
            userName = userInfosName
        }
        
        _ = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        nameLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = userName ?? "--"
            obj.font = kAMediumFont(14)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(25)
            }
        })
        
        _ = UIImageView().then { obj in
            self.addSubview(obj)
//            obj.backgroundColor = UIColor(0xD3D3D3)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "home_pull")
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel!.snp.right).offset(5)
                make.centerY.equalTo(nameLabel!)
//                make.width.height.equalTo(12)
            }
        }
        
        let dateLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "本月"
            obj.font = kAMediumFont(10)
            obj.textColor = blackTextColor2
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel!)
            }
        })
        
        _ = UIImageView().then { obj in
            self.addSubview(obj)
//            obj.backgroundColor = blackTextColor2
            obj.image = UIImage(named: "home_infos_date")
            obj.snp.makeConstraints { make in
                make.right.equalTo(dateLabel.snp.left).offset(-5)
                make.centerY.equalTo(nameLabel!)
//                make.width.height.equalTo(s
            }
        }
        
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
}
