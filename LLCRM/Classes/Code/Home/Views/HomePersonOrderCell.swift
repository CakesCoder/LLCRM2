//
//  HomePersonOrderCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/3.
//

import UIKit
//import AAInfographics

class HomePersonOrderCell: UITableViewCell {
    var aaChartView: AAChartView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let orderImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_personOrder")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
    
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "订单排行榜（个人）"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(orderImageV.snp.right).offset(5)
                make.centerY.equalTo(orderImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(orderImageV)
            }
        }
    
        _ = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(orderImageV)
            }
        }
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 250
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        self.addSubview(aaChartView!)
        
        let chartModel = AAChartModel()
                .chartType(.column)//图表类型
                .animationType(.bounce)
                .dataLabelsEnabled(false)
                .categories(["","","王小虎","",""])
                .colorsTheme(["#409EFF"])//主题颜色数组
                .tooltipEnabled(false)
                .series([
                        AASeriesElement()
                            .name("王小虎")
                            .color("#409EFF")
                            .data([NSNull(),NSNull(),0.9,NSNull(),NSNull()])
                        ])
        
        /*图表视图对象调用图表模型对象,绘制最终图形*/
        aaChartView?.aa_drawChartWithChartModel(chartModel)
        
        _ =  UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ =  UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "设备总金额的总计：0.80"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
}
