//
//  HomeTransformCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/2.
//

import UIKit

class HomeTransformCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let transformImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_transform")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
        
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "线索转化"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(transformImageV.snp.right).offset(5)
                make.centerY.equalTo(transformImageV)
            }
        })
        
        let headView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.right.equalToSuperview().offset(-20)
                make.top.equalTo(titleLabel.snp.bottom).offset(10)
                make.height.equalTo(30)
            }
        }
        
        let headtitleLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "销售线索总数：2个"
            obj.textColor = blackTextColor13
            obj.font = kAMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.centerY.equalToSuperview()
            }
        }
        
        let headPushButton = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setTitle(">", for: .normal)
            obj.setTitleColor(blackTextColor13, for: .normal)
            obj.titleLabel?.font = kAMediumFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.centerY.equalToSuperview()
            }
        }
        
        for i in 0..<3{
            let contentView = UIView().then { obj in
                self.addSubview(obj)
                if i == 0{
                    obj.frame = CGRect(x: 20, y: 70, width: Int(kScreenWidth)-40, height: 64)
//                    obj.backgroundColor = .blue
                }else if i == 1{
                    obj.frame = CGRect(x: 20, y:70+i*64, width: Int(kScreenWidth)-40, height: 94)
//                    obj.backgroundColor = .blue
                }else{
                    obj.frame = CGRect(x: 20, y:70+94+64, width: Int(kScreenWidth)-40, height: 94)
//                    obj.backgroundColor = .yellow
                }
            }
            
            let contentLeftView = UIView().then { obj in
                contentView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: 0, width: (Int(kScreenWidth)-40)/2, height: 64)
            }
            
            let titleLeftLabel = UILabel().then { obj in
                contentLeftView.addSubview(obj)
                obj.textColor = blackTextColor4
                obj.text = "转化为客户的线索数"
                obj.font = kAMediumFont(12)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(10)
                }
            }
            
            let numLeftLabel = UILabel().then { obj in
                contentLeftView.addSubview(obj)
                obj.text = "66"
                obj.textColor = blackTextColor3
                obj.font = kAMediumFont(18)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    make.top.equalTo(titleLeftLabel.snp.bottom).offset(10)
                }
            }
            
            let contentRightView = UIView().then { obj in
                contentView.addSubview(obj)
                obj.frame = CGRect(x: (Int(kScreenWidth)-40)/2, y: 0, width: (Int(kScreenWidth)-40)/2, height: 64)
            }
            
            let titleRightLabel = UILabel().then { obj in
                contentRightView.addSubview(obj)
                obj.textColor = blackTextColor4
                obj.text = "转化为客户的线索数"
                obj.font = kAMediumFont(12)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(10)
                }
            }
            
            let numRightLabel = UILabel().then { obj in
                contentRightView.addSubview(obj)
                obj.text = "0.00%"
                obj.textColor = blackTextColor3
                obj.font = kAMediumFont(18)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    make.top.equalTo(titleLeftLabel.snp.bottom).offset(10)
                }
            }
            
            if i > 0{
                let moneyLabel = UILabel().then { obj in
                    contentLeftView.addSubview(obj)
                    if i == 1{
                        obj.text = "商机金额：0.00元"
                    }else{
                        obj.text = "订单金额：0.00元"
                    }
                    
                    obj.textColor = blackTextColor14
                    obj.font = kRegularFont(12)
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(10)
                        make.top.equalTo(numLeftLabel.snp.bottom).offset(8)
                    }
                }
                
                if i == 2{
                    let refundLabel = UILabel().then { obj in
                        contentLeftView.addSubview(obj)
                        obj.text = "已回款：0.00元"
                        obj.textColor = blackTextColor14
                        obj.font = kRegularFont(12)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(moneyLabel.snp.right).offset(10)
                            make.centerY.equalTo(moneyLabel)
                        }
                    }
                }
            }
            
            let unitLeftLabel = UILabel().then { obj in
                contentLeftView.addSubview(obj)
                obj.text = "个"
                obj.textColor = blackTextColor4
                obj.font = kRegularFont(10)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(numLeftLabel.snp.right).offset(0)
                    make.centerY.equalTo(numLeftLabel)
                }
                
            }
            
            
            _ = UIView().then({ obj in
                contentView.addSubview(obj)
                obj.backgroundColor = lineColor8
                obj.snp.makeConstraints { make in
                    make.bottom.right.equalToSuperview().offset(-0)
                    make.left.equalToSuperview().offset(0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
