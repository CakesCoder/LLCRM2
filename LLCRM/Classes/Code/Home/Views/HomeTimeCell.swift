//
//  HomeTimeCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/1.
//

import UIKit

class HomeTimeCell: UITableViewCell {

    var dateLabel:UILabel?
    let weekArray = ["日","一","二","三","四","五","六"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        let timeImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_time")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
        
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "我的日程"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(timeImageV.snp.right).offset(5)
                make.centerY.equalTo(timeImageV)
            }
        })
        
        dateLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "2022 年 05 月"
            obj.textAlignment = .center
            obj.textColor = blackTextColor5
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(40)
                make.centerX.equalToSuperview()
            }
        })
        
        let weekView = UIView().then { obj in
            self.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(dateLabel!.snp.bottom).offset(30)
                make.height.equalTo(70)
            }
        }
        
        let weekWidth = (kScreenWidth-30) / 7
        for i in 0 ..< 7 {
            let weekLabel = UILabel.init(frame: CGRect.init(x: Int(Float(i)*Float(weekWidth)) , y: 0, width: Int(weekWidth), height: 40))
            weekLabel.backgroundColor = UIColor.clear
            weekLabel.text = weekArray[i]
            weekLabel.font = kAMediumFont(15)
            weekLabel.textAlignment = .center
            weekLabel.textColor = blackTextColor6
            weekView.addSubview(weekLabel)
        }
        
        let weekLineView = UIView().then { obj in
            weekView.addSubview(obj)
            obj.backgroundColor = lineColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(45)
                make.height.equalTo(0.5)
            }
        }
        
        for i in 0 ..< 7 {
            let weekButton = UIButton.init(frame: CGRect.init(x: Int(Float(i)*Float(weekWidth)) , y: 60, width: Int(weekWidth), height: 40))
            weekView.addSubview(weekButton)
            weekButton.setTitleColor(blackTextColor7, for: .normal)
            weekButton.setTitle(String(i), for: .normal)
            weekButton.addTarget(self, action: #selector(weekClick), for: .touchUpInside)
        }
        
        for i in 0 ..< 2{
            let h = 66
            
            let taskView = UIView().then { obj in
                self.addSubview(obj)
                obj.frame = CGRect(x: 0, y: 200+h*i, width: Int(kScreenWidth), height: h)
            }
            
            let blueView = UIView().then { obj in
                taskView.addSubview(obj)
                obj.layer.cornerRadius = 3
                obj.layer.masksToBounds = true
                obj.backgroundColor = bluebgColor
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(25)
                    make.width.height.equalTo(6)
                }
            }
            
            let taskTitleLabel = UILabel().then { obj in
                taskView.addSubview(obj)
                obj.text = "讨论APP页面设计及交互的跳转"
                obj.textColor = blackTextColor8
                obj.font = kAMediumFont(14)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(blueView.snp.right).offset(5)
                    make.centerY.equalTo(blueView)
                }
            }
            
            let conferenceButton = UIButton().then { obj in
                taskView.addSubview(obj)
                obj.setImage(UIImage(named: "home_conference"), for: .normal)
                obj.setImage(UIImage(named: "home_conference"), for: .selected)
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalTo(blueView)
                    
                }
            }
            
            let timeLabel = UILabel().then { obj in
                taskView.addSubview(obj)
                obj.text = "13:46  ~  14:46"
                obj.textColor = blackTextColor10
                obj.font = kAMediumFont(11)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(30)
                    make.top.equalTo(taskTitleLabel.snp.bottom).offset(10)
                }
            }
            
            _ = UILabel().then { obj in
                taskView.addSubview(obj)
                obj.text = "会议"
                obj.textColor = blackTextColor9
                obj.font = kAMediumFont(11)
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalTo(timeLabel)
                }
            }
            
            if i != 1{
                _ = UIView().then { obj in
                    taskView.addSubview(obj)
                    obj.backgroundColor = lineColor4
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.height.equalTo(0.5)
                        make.bottom.equalToSuperview().offset(-0)
                    }
                }
            }
        }
        
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
    
    @objc func weekClick(){
        printTest("asdasd")
    }

}
