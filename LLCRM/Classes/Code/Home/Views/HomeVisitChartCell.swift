//
//  HomeVisitChartCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/3.
//

import UIKit
//import AAInfographics

class HomeVisitChartCell: UITableViewCell {
    
    var aaChartView: AAChartView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let sellImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_visitchart")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
    
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "员工拜访目标完成率排行"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(sellImageV.snp.right).offset(5)
                make.centerY.equalTo(sellImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(sellImageV)
            }
        }
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(sellImageV)
            }
        }
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 300
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        contentView.addSubview(aaChartView!)
        
        let chartModel = AAChartModel()
                .chartType(.bar)//图表类型
                .animationType(.bounce)
                .dataLabelsEnabled(true)
                .categories(["王小虎"])
                .dataLabelsStyle(AAStyle(color: "#525252", fontSize: 10).backgroundColor("#16B5AE").height(200))
//                .dataLabelsStyle(AAStyle(color: "#FF7F00"))
//                .valueDecimals(2)
                .series([
                        AASeriesElement()
                            .name("目标值")
                            .data([NSNull()])
                            .color("#409EFF"),
                        AASeriesElement()
                            .name("王小虎")
                            .data([0.9])
                            .color("#31C675"),
                        AASeriesElement()
                            .name("完成率")
                            .data([NSNull()])
                            .color("#FF7F00"),
                        ])
        
        /*图表视图对象调用图表模型对象,绘制最终图形*/
        aaChartView?.aa_drawChartWithChartModel(chartModel)
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
    }
}
