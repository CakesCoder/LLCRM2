//
//  HomeSellMoneyCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/3.
//

import UIKit
//import AAInfographics

class HomeSellMoneyCell: UITableViewCell {
    private var aaChartView: AAChartView?
    private var chartView:UIView?
    private var colorsView:UIView?
    
    var colorList:[String]? = ["#3082FF", "#5EB8FE", "#16B5AE", "#54CEC7", "#36C777", "#71DBA1", "#9ACC50", "#B4DE88","#FCD232", "#5EB8FA", "#71DBA5", "#36C787"]
    
    var markDataModel: MarkDataModel?{
        didSet{
            for view in colorsView?.subviews as! [UIView]{
                view.removeFromSuperview()
            }
            
            if markDataModel?.personSaleList != nil{
                var dataList = [] as! [Any]
                for i in 0..<(markDataModel?.personSaleList!.count)!{
                    let personSaleModel = markDataModel?.personSaleList?[i] as! personSaleListModel
                    var dataInfos:[Any] = []
                    dataInfos.append(personSaleModel.saleMonth)
                    let money = Float(personSaleModel.realityMoney!)
                    dataInfos.append(money)
                    dataList.append(dataInfos)
                }
                let chartModel = AAChartModel()
                    .chartType(.funnel)
                    .yAxisVisible(true)
//                    .margin(right: kScreenWidth/2.0)
                    .series([
                        AASeriesElement()
                            .colors(colorList!)
                            .dataLabels(AADataLabels()
                                .x(1)
                                .enabled(false)
                                .verticalAlign(.middle)
                                .color(AAColor.black)
                                .softConnector(false)
                                .style(AAStyle()
                                    .fontSize(12)
                                )
                            )
                        
                            .data([
                                dataList
                            ])
                            .toDic()!,
                    ])
                
                /*图表视图对象调用图表模型对象,绘制最终图形*/
                aaChartView?.aa_drawChartWithChartModel(chartModel)
                
                if markDataModel?.personSaleList?.count ?? 0 > 12{
                    for j in 0..<(self.colorList?.count ?? 0){
                        //                                    let index = i as! Int
                        let view = UIView().then { obj in
                            colorsView?.addSubview(obj)
                            obj.backgroundColor = UIColor(hex:colorList?[j] ?? "")
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(20)
                                make.top.equalToSuperview().offset(5+15*j)
                                make.width.equalTo(20)
                                make.height.equalTo(10)
                            }
                        }
                        
                        let texts = dataList[j] as? [Any]
                        _ = UILabel().then({ obj in
                            colorsView?.addSubview(obj)
                            obj.text = texts?[0] as? String
                            obj.textColor = .black
                            obj.font = kRegularFont(0)
                            obj.snp.makeConstraints { make in
                                make.left.equalTo(view.snp.right).offset(10)
                                make.centerY.equalTo(view)
                            }
                        })
                    }
                }else{
                    for j in 0..<(markDataModel?.personSaleList?.count ?? 0){
                        let view = UIView().then { obj in
                            colorsView?.addSubview(obj)
                            obj.backgroundColor = UIColor(hex:colorList?[j] ?? "")
                            obj.snp.makeConstraints { make in
                                make.left.equalToSuperview().offset(30)
                                make.top.equalToSuperview().offset(35+15*j)
                                make.width.equalTo(20)
                                make.height.equalTo(10)
                            }
                        }
                        
                        let texts = dataList[j] as? [Any]
                        _ = UILabel().then({ obj in
                            colorsView?.addSubview(obj)
                            obj.text = texts?[0] as? String
                            obj.textColor = .black
                            obj.font = kRegularFont(0)
                            obj.snp.makeConstraints { make in
                                make.left.equalTo(view.snp.right).offset(10)
                                make.centerY.equalTo(view)
                            }
                        })
                    }
                }
            }else{
                let chartModel = AAChartModel()
                    .chartType(.funnel)
                    .yAxisVisible(true)
//                    .margin(right: kScreenWidth/2.0)
                    .series([
                        AASeriesElement()
                            .colors(colorList!)
                            .dataLabels(AADataLabels()
                                .x(1)
                                .enabled(false)
                                .verticalAlign(.middle)
                                .color(AAColor.black)
                                .softConnector(false)
                                .style(AAStyle()
                                    .fontSize(12)
                                )
                                )

                        .data([
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                            ["",1] as [Any],
                        ])
                        .toDic()!,
                    ])
                
                /*图表视图对象调用图表模型对象,绘制最终图形*/
                aaChartView?.aa_drawChartWithChartModel(chartModel)
                
                
                
                for i in 0..<(self.colorList?.count ?? 0){
                    let view = UIView().then { obj in
                        colorsView?.addSubview(obj)
                        obj.backgroundColor = UIColor(hex: self.colorList?[i] ?? "")
                        obj.snp.makeConstraints { make in
                            make.left.equalToSuperview().offset( 20)
                            make.top.equalToSuperview().offset(35+15*i)
                            make.width.equalTo(20)
                            make.height.equalTo(10)
                        }
                    }
                    
                    _ = UILabel().then({ obj in
                        colorsView?.addSubview(obj)
                        obj.text = "--"
                        obj.textColor = .black
                        obj.font = kRegularFont(0)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(view.snp.right).offset(10)
                            make.centerY.equalTo(view)
                        }
                    })
                }
            }
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate func extractedFunc() -> AAChartModel {
        return AAChartModel()
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let sellImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_sellmoney")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
    
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "销售漏斗（商机2.0金额）"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(sellImageV.snp.right).offset(5)
                make.centerY.equalTo(sellImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(sellImageV)
            }
        }
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(sellImageV)
            }
        }
        
        chartView = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(40)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(270)
            }
        })
        
        colorsView = UIView().then({ obj in
            chartView?.addSubview(obj)
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(kScreenWidth/2.0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
        
        let chartViewWidth  = Int(kScreenWidth/2.0)
        let chartViewHeight = 270
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 0,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        // 设置 aaChartView 的内容高度(content height)
//        aaChartView?.contentWidth = kScreenWidth/2.0
        aaChartView!.isScrollEnabled = true
        chartView?.addSubview(aaChartView!)
        
        
        
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(sellImageV)
            }
        }
        
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "设备总金额的总计：0.80"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
