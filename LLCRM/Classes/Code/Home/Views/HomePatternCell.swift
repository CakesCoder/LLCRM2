//
//  HomePatternCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import UIKit

class HomePatternCell: UITableViewCell {
    
    typealias didClickPatternCell = (_ tag: Int) -> Void
    public var didClickPatternCell: didClickPatternCell?
//    var collectionView: UICollectionView?
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
//    @objc func injected()  {
//       #if DEBUG
//       setupUI()
//       #endif
//    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupUI()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupUI(){
        self.selectionStyle = .none
        
        let imgs:[Dictionary<String , String>] = [["imageName": "home_type_1", "title":"CRM提醒"], ["imageName": "home_type_2", "title":"客户"], ["imageName": "home_type_3", "title":"销售线索"], ["imageName": "home_type_4", "title":"数据驾驶舱"], ["imageName": "home_type_5", "title":"商机"], ["imageName": "home_type_6", "title":"联系人"], ["imageName": "home_type_7", "title":"样品申请"], ["imageName": "home_type_8", "title":"报价单"], ["imageName": "home_type_9", "title":"客诉"], ["imageName": "home_type_10", "title":"客户拜访"], ["imageName": "home_type_11", "title":"更多功能"]]
        
        for i in 0...10{
            let t = Int(i)
            let w = kScreenWidth/4
            let x = CGFloat(t%4)
            let y = CGFloat(t/4)
            
            let patternView = UIView(frame: CGRectMake(x*w, y*w, w, w))
            contentView.addSubview(patternView)
            
            let imageV = UIImageView().then { obj in
                patternView.addSubview(obj)
                obj.image = UIImage(named: imgs[i]["imageName"]!)
                obj.isUserInteractionEnabled = true
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(22)
                    make.centerX.equalToSuperview()
                    make.width.height.equalTo(38)
                }
            }
            
            _ = UILabel().then({ obj in
                patternView.addSubview(obj)
                obj.text = imgs[i]["title"]!
                obj.textAlignment = .center
                obj.font = kAMediumFont(13)
                obj.textColor = blackTextColor
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(5)
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(18)
                }
            })
            
            _ = UIButton().then({ obj in
                patternView.addSubview(obj)
                obj.tag = 1000+i
                obj.addTarget(self, action: #selector(touchClick(_ :)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            })
        }
    }
    
    @objc func touchClick(_ button:UIButton){
//        printTest("asdasdasdas")
        self.didClickPatternCell?(button.tag)
    }
    
}

