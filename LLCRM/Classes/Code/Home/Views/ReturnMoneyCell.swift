//
//  ReturnMoneyCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/19.
//

import UIKit

class ReturnMoneyCell: UITableViewCell {
    
    var aaChartView: AAChartView?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let departmentalImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_money")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
    
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "回款率（回款/目标）"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(departmentalImageV.snp.right).offset(5)
                make.centerY.equalTo(departmentalImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 300
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        contentView.addSubview(aaChartView!)
        
        
        let aaOptions = AAOptions()
            .chart(AAChart()
                    .type(.solidgauge))
            .title(AATitle()
                    .text("")
                    .style(AAStyle()
                            .fontSize(0)))
//            .tooltip(AATooltip()
//                    .enabled(false))
            .colors(["#FF7F00"])
            .pane(AAPane()
                    .center(["50%", "60%"])
                    .size("60%")
                    .startAngle(-105)
                    .endAngle(105)
                    .background([
                        AABackgroundElement()
                            .backgroundColor("#DDE1EA")
                            .innerRadius("60%")
                            .outerRadius("100%")
                            .shape("arc")
                    ]))
            .plotOptions(AAPlotOptions()
                            .solidgauge(AASolidgauge()
                                            .dataLabels(AADataLabels()
                                                            .y(-0)
                                                            .borderWidth(0)
                                                            .useHTML(true)
                                                            .format(#"""
                                                                    <div style="text-align:center">
                                                                        <span style="font-size:24px">{y}%</span><br/>
                                                                        <span style="font-size:13px;opacity:0.4">回款率(回款/目标)</span>
                                                                    </div>
                                                                    """#.aa_toPureJSString2()))))
            .yAxis(AAYAxis()
//                    .min(0)
//                    .max(100)
//                    .tickWidth(0)
//                    .minorTickInterval(0)
//                    .tickAmount(0)
                    .labels(AALabels()
//                                .distance(0)
//                                .y(32)
                                .style(AAStyle()
                                        .fontSize(0)))
            )
            .series([
                AASeriesElement()
                    .type(.solidgauge)
                    .data([72])
            ])
        
        self.aaChartView?.aa_drawChartWithChartOptions(aaOptions)
        
        _ = UILabel().then({ obj in
            self.aaChartView?.addSubview(obj)
            obj.text = "已回款金额"
            obj.font = kMediumFont(13)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(53)
                make.top.equalToSuperview().offset(10)
            }
        })
        
        _ = UILabel().then({ obj in
            self.aaChartView?.addSubview(obj)
            obj.text = "25.02万"
            obj.font = kMediumFont(20)
            obj.textColor = blackTextColor89
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(48)
                make.top.equalToSuperview().offset(35)
            }
        })
        
        _ = UILabel().then({ obj in
            self.aaChartView?.addSubview(obj)
            obj.text = "目标值"
            obj.font = kMediumFont(13)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-53)
                make.top.equalToSuperview().offset(10)
            }
        })
        
        _ = UILabel().then({ obj in
            self.aaChartView?.addSubview(obj)
            obj.text = "50万"
            obj.font = kMediumFont(20)
            obj.textColor = blackTextColor89
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-48)
                make.top.equalToSuperview().offset(35)
            }
        })
        
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-50)
            }
        }
        
        
    }

}

public extension String {
    func aa_toPureJSString2() -> String {
        //https://stackoverflow.com/questions/34334232/why-does-function-not-work-but-function-does-chrome-devtools-node
        var pureJSStr = "\(self)"
        pureJSStr = pureJSStr.replacingOccurrences(of: "'", with: "\"")
        pureJSStr = pureJSStr.replacingOccurrences(of: "\0", with: "")
//        pureJSStr = pureJSStr.replacingOccurrences(of: "\n", with: "")
        pureJSStr = pureJSStr.replacingOccurrences(of: "\\", with: "\\\\")
        pureJSStr = pureJSStr.replacingOccurrences(of: "\"", with: "\\\"")
        pureJSStr = pureJSStr.replacingOccurrences(of: "\n", with: "\\n")
        pureJSStr = pureJSStr.replacingOccurrences(of: "\r", with: "\\r")
//        pureJSStr = pureJSStr.replacingOccurrences(of: "\u{000C}", with: "\\f")
//        pureJSStr = pureJSStr.replacingOccurrences(of: "\u{2028}", with: "\\u2028")
//        pureJSStr = pureJSStr.replacingOccurrences(of: "\u{2029}", with: "\\u2029")
        return pureJSStr
    }

}

