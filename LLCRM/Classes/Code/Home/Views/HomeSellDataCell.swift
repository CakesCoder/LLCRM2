//
//  HomeSellDataCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/30.
//

import UIKit

class HomeSellDataCell: UITableViewCell {
    var markDataModel: HomeDataInfosModel?{
        didSet{
            clientLabel?.text = markDataModel?.clientCount ?? "0"
            contactLabel?.text = markDataModel?.contactCount ?? "0"
            commercialLabel?.text = markDataModel?.commercialCount ?? "0"
            personSaleLabel?.text = markDataModel?.personSaleList ?? "0"
            visitLabel?.text = markDataModel?.visitCount ?? "0"
            quotationLabel?.text = markDataModel?.quotationCount ?? "0"
        }
    }
    var clientLabel:UILabel?
    var commercialLabel:UILabel?
    var contactLabel:UILabel?
    var personSaleLabel:UILabel?
    var quotationLabel:UILabel?
    var teamSaleLabel:UILabel?
    var visitLabel:UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        let sellImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_sell")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
        
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "销售简报"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(sellImageV.snp.right).offset(5)
                make.centerY.equalTo(sellImageV)
            }
        })
        
        let addView = UIView().then { obj in
            self.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalTo(titleLabel.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-30)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        for i in 0...5{
            let sellView = UIView().then { obj in
                addView.addSubview(obj)
                let w = CGFloat((kScreenWidth - 60)/3)
                let x = CGFloat(i%3)
                let y = CGFloat(i/3)
                let h = CGFloat(w-30)
                obj.frame = CGRect(x: x*w, y: y*h, width: w, height: h)
            }
            
            let dataLabel = UILabel().then { obj in
                sellView.addSubview(obj)
                obj.textAlignment = .center
                if i == 0{
//                    obj.text = "30"
                    clientLabel = obj
                }else if i == 1{
//                    obj.text = "59"
                    contactLabel = obj
                }else if i == 2{
//                    obj.text = "0"
                    commercialLabel = obj
                }else if i == 3{
//                    obj.text = "88"
                    personSaleLabel = obj
                }else if i == 4{
//                    obj.text = "60"
                    visitLabel = obj
                }else if i == 5{
//                    obj.text = "40"
                    quotationLabel = obj
                }
               
                obj.font =  kAMediumFont(26)
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(21)
                    make.centerX.equalToSuperview()
                }
            }
            
            _ = UILabel().then({ obj in
                sellView.addSubview(obj)
                obj.textAlignment = .center
//                obj.text = "阶段变化的商机"
                if i == 0{
                    obj.text = "新增客户"
                }else if i == 1{
                    obj.text = "新增联系人"
                }else if i == 2{
                    obj.text = "新增商机"
                }else if i == 3{
                    obj.text = "个人销售清单"
                }else if i == 4{
                    obj.text = "客户拜访"
                }else if i == 5{
                    obj.text = "报价单"
                }
                obj.textColor = blackTextColor4
                obj.font =  kRegularFont(12)
                obj.snp.makeConstraints { make in
                    make.top.equalTo(dataLabel.snp.bottom).offset(6)
                    make.centerX.equalToSuperview()
                }
            })
        }
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
