//
//  HomeReasonCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/1.
//

import UIKit

class HomeReasonCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.selectionStyle = .none
        
        let statisticsImageV = UIImageView().then({ obj in
            self.addSubview(obj)
            obj.image = UIImage(named: "home_reason")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
            }
        })
        
        let titleLabel = UILabel().then({ obj in
            self.addSubview(obj)
            obj.text = "商机2.0数量/金额统计"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(statisticsImageV.snp.right).offset(5)
                make.centerY.equalTo(statisticsImageV)
            }
        })
        
        let pushButton = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(statisticsImageV)
            }
        }
        
        
        _ = UIButton().then { obj in
            self.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
            
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(statisticsImageV)
            }
        }
        
        let bgView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.borderColor = lineColor5.cgColor
            obj.layer.borderWidth = 1
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalTo(titleLabel.snp.bottom).offset(10)
                make.bottom.equalToSuperview().offset(-37)
                make.right.equalToSuperview().offset(-25)
            }
        }
        
        let headView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(30)
            }
        }
        
        let headlineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-92)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(0.5)
            }
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "输单原因-多选"
            obj.textAlignment = .center
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor13
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalTo(headlineView.snp.left).offset(-0)
            }
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textAlignment = .center
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor13
            obj.text = "商机总数"
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.left.equalTo(headlineView.snp.right).offset(0)
            }
        }
        
        for i in 0..<5{
            let contentView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: 30*i+30, width:Int(kScreenWidth)-50, height: 30)
            }
            
            let contentlineView = UIView().then { obj in
                contentView.addSubview(obj)
                obj.backgroundColor = lineColor6
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-92)
                    make.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo(0.5)
                }
            }
            
            _ = UILabel().then { obj in
                contentView.addSubview(obj)
                obj.textAlignment = .center
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor11
                obj.text = "2"
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                    make.left.equalTo(contentlineView.snp.right).offset(0)
                }
            }
            
            _ = UILabel().then { obj in
                contentView.addSubview(obj)
                obj.text = "得到客户购买信息较晚，客户不愿多了解。"
                obj.textAlignment = .center
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor11
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.right.equalTo(contentlineView.snp.left).offset(-0)
                }
            }
            
            if i != 4{
                _ = UIView().then({ obj in
                    contentView.addSubview(obj)
                    obj.backgroundColor = lineColor7
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
        }
        _ =  UILabel().then { obj in
            self.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-25)
                make.bottom.equalToSuperview().offset(-13)
            }
        }
        
        _ = UIView().then({ obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
