//
//  CRMRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class CRMRouter: CRMRouterProtocols {
    func pushToHomeClient(from previousView: UIViewController) {
        let vc = HomeClientViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToHomeLinkMan(from previousView: UIViewController) {
        let vc = LinkManViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
        
    func pushToBusinessList(from previousView: UIViewController) {
        let vc = BusinessListViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToCRMPMessageList(from previousView: UIViewController) {
        let vc = CRMMessageViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToSampleList(from previousView: UIViewController) {
        let vc = SampleListViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToMoreDetail(from previousView: UIViewController){
        let vc = MoreDetailViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToMarketData(from previousView: UIViewController){
        let vc = MarketDataViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushDataCockpit(from previousView: UIViewController){
        let vc = DataCockpitViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushCustomerService(from previousView: UIViewController){
        let vc = CustomerServiceViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToPriceList(from previousView: UIViewController){
        let vc = PriceListViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToSellLeadsV(from previousView: UIViewController){
        let vc = SellLeadsViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToVisitList(from previousView: UIViewController){
        let vc = VisitListViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
}
