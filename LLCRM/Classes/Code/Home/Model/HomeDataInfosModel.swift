//
//  HomeDataInfosModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/26.
//

import UIKit

class HomeDataInfosModel: BaseModel {
    // 客户数量
    var clientCount: String?
    // 商业数量
    var commercialCount: String?
    // 商业钱数
    var commercialMoney: String?
    // 联系人数量
    var contactCount: String?
    // 个人销售清单
    var personSaleList: String?
    // 报价单 数量
    var quotationCount: String?
    // 团队销售清单
    var teamSaleList: String?
    // 总数
    var total: String?
    // 拜访数量
    var visitCount: String?
    
    required init() {}
}
