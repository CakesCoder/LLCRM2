//
//  DeviceManagerViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class DeviceManagerViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "设备管理"
        
        let headView = UIView().then { obj in
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let deviceHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(0)
            }
        }
        
        _ = UILabel().then { obj in
            deviceHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "已登录设备"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        for i in 0..<4{
            let bgView = UIView().then { obj in
                headView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: 50+i*80, width: Int(kScreenWidth), height: 80)
            }
            
            let iphoneImageV = UIImageView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalToSuperview().offset(20)
                    make.width.equalTo(11)
                    make.height.equalTo(17)
                }
            }
            
            let nameLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.text = "iPhone 13"
                obj.font = kMediumFont(14)
                obj.textColor = blackTextColor48
//                obj.backgroundColor = .green
                
                obj.snp.makeConstraints { make in
                    make.left.equalTo(iphoneImageV.snp.right).offset(10)
                    make.centerY.equalTo(iphoneImageV)
                }
            }
            
            let iphoneTagLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.layer.cornerRadius = 2
                obj.backgroundColor = bgColor9
                obj.text = "本机"
                obj.font = kMediumFont(12)
                obj.textAlignment = .center
                obj.textColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalTo(nameLabel.snp.right).offset(10)
                    make.centerY.equalTo(nameLabel)
                    make.width.equalTo(40)
                    make.height.equalTo(20)
                }
            }
            
            let statueLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = blackTextColor36
                obj.layer.cornerRadius = 2
                obj.text = " 注销 "
                obj.font = kMediumFont(12)
                obj.textAlignment = .center
                obj.textColor = .white
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalTo(nameLabel)
                    make.width.equalTo(40)
                    make.height.equalTo(20)
                }
            }
            
            let timeLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.text = "当前在线"
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor47
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(38)
                    make.top.equalTo(nameLabel.snp.bottom).offset(18)
                }
            }
            
            _ = UIView().then({ obj in
                headView.addSubview(obj)
                obj.backgroundColor = lineColor18
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let topLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(50+4*80)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let phoneLoginView = UIView().then { obj in
            headView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(60+4*80)
                make.height.equalTo(50)
            }
        }
        
        let phoneLoginHintLabel = UILabel().then { obj in
            phoneLoginView.addSubview(obj)
            obj.text = "查看已授权手机设备"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        let phoneLoginImageV = UIImageView().then { obj in
            phoneLoginView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-28)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(12)
            }
        }
        
        let phoneLoginLineView = UIView().then { obj in
            phoneLoginView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        _ = UIButton().then({ obj in
            phoneLoginView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let phoneLogView = UIView().then { obj in
            headView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(110+4*80)
                make.height.equalTo(50)
            }
        }
        
        let phoneLogHintLabel = UILabel().then { obj in
            phoneLogView.addSubview(obj)
            obj.text = "查看登录日志"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        let phoneLogImageV = UIImageView().then { obj in
            phoneLogView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-28)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(12)
            }
        }
        
        let phoneLogLineView = UIView().then { obj in
            phoneLogView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        _ = UIButton().then({ obj in
            phoneLogView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        _ = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(160+4*80)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let isLoginView = UIView().then { obj in
            headView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(170+4*80)
                make.height.equalTo(50)
            }
        }
        
        let isLoginHintLabel = UILabel().then { obj in
            isLoginView.addSubview(obj)
            obj.text = "仅允许一台手机设备登录"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor47
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        let isLoginSwitch = UISwitch().then { obj in
            isLoginView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then { obj in
            isLoginView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        _ = UIButton().then({ obj in
            isLoginView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
