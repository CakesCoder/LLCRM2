//
//  UserInfosModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class UserInfosModel: BaseModel {
    var companyName:String?
    var creditCode:NSInteger?
    var sysUserBo: sysUserBoModel?
    
    required init(){}
}

class sysUserBoModel: BaseModel{
    var admin: NSInteger?
    var avatar: String?
    var companyId: NSInteger?
    var companyLogo: String?
    var companyName: String?
    var creditCode: String?
    var departmentId: NSInteger?
    var departmentName: String?
    var phoneNumber: String?
    var staffNumber: NSInteger?
    var userAccount: NSInteger?
    var userId: NSInteger?
    var userName: String?
    var workStatus: NSInteger?
    var email:String?
    
    required init(){}
}
