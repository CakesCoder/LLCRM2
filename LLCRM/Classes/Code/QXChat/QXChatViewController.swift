//
//  QXChatViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit

class QXChatViewController: BaseViewController {
    
    var presenter: QXChatPresenterProtocols?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        printTest(LLUserDefaultsManager.shared.readLoginInfosTK())
        
        if LLUserDefaultsManager.shared.readCacheShowGudide() == ""{
            presenter?.router?.pushToGuide(from: self)
            LLUserDefaultsManager.shared.cacheIsShowGudide()
        }else if LLUserDefaultsManager.shared.readLoginInfosTK() == ""{
            presenter?.router?.pushToLogin(from: self)
        } else{
            
        }
        
        buildNavConfing()
        cofing()
        requestMethod()
    }
    
    func cofing(){
        let router = QXChatRouter()
        
        presenter = QXChatPresenter()
        
        presenter?.router = router
        
        let entity = QXChatEntity()
        
        let interactor = QXChatInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()    
    }
    
    func requestMethod(){
        if let tokenData = LLUserDefaultsManager.shared.readCacheLogin(){
            let userAccount = tokenData["user_account"] as! String
            presenter!.presenterRequestGetUserInfos(by: ["userAccount":userAccount, "userType":"0"])
        }
    }
    
    func buildNavConfing(){
        self.navigationItem.title = "企信"
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension QXChatViewController: QXChatViewProtocols{
    
    func reloadView() {
        
    }
    
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func outLogin(){
        LLUserDefaultsManager.shared.removeTK()
        presenter?.router?.pushToLogin(from: self)
    }
}
