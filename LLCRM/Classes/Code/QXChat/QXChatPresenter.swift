    //
//  QXChatPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import HandyJSON

class QXChatPresenter: QXChatPresenterProtocols {
    
    var view: QXChatViewProtocols?
    
    var router: QXChatRouterProtocols?
    
    var interactor: QXChatInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
        
    }
    
    func presenterRequestGetUserInfos(by params: Any?) {
        interactor?.entityGetUserInfosReceiveData(by: params)
    }

    func didGetUserInfosInteractorReceiveData(by params: Any?){
        if (params != nil){
            let dict = params as! [String:Any]
            if let dict = params as? [String:Any]{
                let dataDic = dict["data"] as? [String:Any]
                printTest(dataDic)
                if dict["code"] as! Int == 200{
                    if let dataModel = JSONDeserializer<UserInfosModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                        printTest(dataModel.sysUserBo?.creditCode)
                        LLUserDefaultsManager.shared.saveUserNameInfos(userName: dataModel.sysUserBo?.userName)
                        LLUserDefaultsManager.shared.saveUserDepartmentNameInfos(userName: dataModel.sysUserBo?.departmentName)
                        LLUserDefaultsManager.shared.saveUserCompanyNameInfos(userName: dataModel.sysUserBo?.companyName)
                        LLUserDefaultsManager.shared.saveUserEmailInfos(emil: dataModel.sysUserBo?.email)
                        LLUserDefaultsManager.shared.saveUserPhoneInfos(phone: dataModel.sysUserBo?.phoneNumber)
                        LLUserDefaultsManager.shared.saveUserCreditCodeInfos(code: dataModel.sysUserBo?.creditCode)
                    }
                }else if dict["code"] as! Int == 401{
                    view?.outLogin()
                }
            }
        }
    }
    func didGetUserInfosInteractorReceiveError(by params: Any?){
        
    }
    
    
}
