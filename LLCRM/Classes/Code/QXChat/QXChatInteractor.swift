//
//  QXChatInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

class QXChatInteractor: QXChatInteractorProtocols {
    var presenter: QXChatPresenterProtocols?
    
    var entity: QXChatEntityProtocols?
    
    func entityGetUserInfosReceiveData(by params: Any?){
        entity?.getUserInfosRequest(by: params)
    }
    
    func didEntityGetUserInfosReceiveData(by params: Any?){
        presenter?.didGetUserInfosInteractorReceiveData(by: params)
    }
    
    func didEntityUserInfosReceiveError(error: MyError?){
        presenter?.didGetUserInfosInteractorReceiveError(by: error)
    }

}
