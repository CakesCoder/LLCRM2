//
//  QXChatEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import Alamofire

class QXChatEntity: QXChatEntityProtocols {
    var interactor: QXChatInteractorProtocols?
    
    func didUserInfosReceiveData(by params: Any?) {
        interactor?.didEntityGetUserInfosReceiveData(by: params)
    }
    
    func didUserInfosReceiveError(error: MyError?) {
        interactor?.didEntityUserInfosReceiveError(error: error)
    }
    
    func getUserInfosRequest(by params: Any?) {
        LLNetProvider.request(.userInfos(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                printTest(result)
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didUserInfosReceiveData(by:jsonData)
            case .failure(_):
                self.didUserInfosReceiveError(error: .requestError)
            }
        }
    }
}

            
