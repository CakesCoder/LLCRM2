//
//  QXChatProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

protocol QXChatViewProtocols: AnyObject {
    // 持有一个presenter，帮我做事
    var presenter: QXChatPresenterProtocols? { get set }
    
    /*
     这些方法，都是UI回调相关的方法
     让我的presenter帮我做事情，等presenter给我回调结果就好了
     */
    func reloadView()
    func showLoading()
    func showError()
    func hideLoading()
    func outLogin()
}

protocol QXChatPresenterProtocols: AnyObject{
    var view: QXChatViewProtocols? { get set }
    var router: QXChatRouterProtocols? { get set }
    var interactor: QXChatInteractorProtocols? { get set }
    var params: Any? { get set }
    func viewDidLoad()
    func presenterRequestGetUserInfos(by params: Any?) 
    func didGetUserInfosInteractorReceiveData(by params: Any?)
    func didGetUserInfosInteractorReceiveError(by params: Any?)
}

protocol QXChatInteractorProtocols: AnyObject {
    var presenter: QXChatPresenterProtocols? { get set }
    var entity: QXChatEntityProtocols? { get set }
    
    func entityGetUserInfosReceiveData(by params: Any?)
    func didEntityGetUserInfosReceiveData(by params: Any?)
    func didEntityUserInfosReceiveError(error: MyError?)
}

protocol QXChatEntityProtocols: AnyObject {
    var interactor: QXChatInteractorProtocols? { get set }
    
    func getUserInfosRequest(by params: Any?)
    
    func didUserInfosReceiveData(by params: Any?)
    func didUserInfosReceiveError(error: MyError?)
    
}

protocol QXChatRouterProtocols: AnyObject {
    func pushToLogin(from previousView: UIViewController)
    func pushToGuide(from previousView: UIViewController)
}
