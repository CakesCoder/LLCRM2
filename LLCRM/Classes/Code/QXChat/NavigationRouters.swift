//
//  NavigationRouters.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/29.
//

import UIKit
import ESTabBarController_swift

class NavigationRouters: QXChatRouterProtocols {
    
    func pushToLogin(from previousView: UIViewController) {
        let vc = LoginViewController()
        vc.modalPresentationStyle = .fullScreen
        previousView.present(vc, animated: true)
    }
    
    func pushToGuide(from previousView: UIViewController) {
        let vc = LLGuideViewContoller()
//        vc.modalPresentationStyle =  UIModalPresentationStyle.fullScreen
        let nav = UINavigationController(rootViewController: vc)
        nav.modalPresentationStyle = .fullScreen
        previousView.present(nav, animated: true)
    }

    func createRootViewController(vc: UIViewController & QXChatViewProtocols) -> UIViewController & QXChatViewProtocols {
        // 1. view 和 presenter 互相持有 （注意，一个强引用，一个弱引用）
        let presenter = QXChatPresenter()
        vc.presenter = presenter
        presenter.view = vc
        
        // 2. presenter 持有 router
        presenter.router = QXChatRouter()
        
        // 3. presenter 和 interactor互相持有 （注意，一个强引用，一个弱引用）
        let interactor = QXChatInteractor()
        presenter.interactor = interactor
        interactor.presenter = presenter
        
        // 4. interactor 和 entity 互相持有 （注意，一个强引用，一个弱引用）
        let entity = QXChatEntity()
        interactor.entity = entity
        entity.interactor = interactor
        
        return vc
    }
    
    
    
    static func createTabBarVC() -> ESTabBarController {
        let tabBarVC = TabbarVC()
        
        let chatRootVC = NavigationRouters().createRootViewController(vc:QXChatViewController())
        let chatVC = QXChatNavigationViewController(rootViewController: chatRootVC)
        
        let workVC = WorkNavViewController()
        
        let crmVC =  HomeNavigationViewController()
        
        let applyVC = ApplyNavViewController()
        
        let meVC = MeNavViewController()

        
        chatVC.tabBarItem = ESTabBarItem.init(TabBarContentView(), title: "企信", image: UIImage(named: "chat_nor"), selectedImage: UIImage(named: "chat_sel"))
        
//        workVC.navigationItem.title = "工作台"r
        workVC.tabBarItem = ESTabBarItem.init(TabBarContentView(), title: "工作", image: UIImage(named: "work_nor"), selectedImage: UIImage(named: "work_sel"))
        
//        crmVC.title = "CRM"
        crmVC.tabBarItem = ESTabBarItem.init(TabBarContentView(), title: "CRM", image: UIImage(named: "crm_nor"), selectedImage: UIImage(named: "crm_sel"))
        
        applyVC.tabBarItem = ESTabBarItem.init(TabBarContentView(), title: "应用", image: UIImage(named: "apply_nor"), selectedImage: UIImage(named: "apply_sel"))
        
        meVC.tabBarItem = ESTabBarItem.init(TabBarContentView(), title: "我的", image: UIImage(named: "me_nor"), selectedImage: UIImage(named: "me_sel"))

        tabBarVC.viewControllers = [chatVC, workVC, crmVC, applyVC, meVC]

        
        return tabBarVC
    }
}
