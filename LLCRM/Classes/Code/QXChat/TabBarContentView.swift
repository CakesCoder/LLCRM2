//
//  JZTabBarIrregularityBasicContentView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import ESTabBarController_swift

class TabBarContentView: JZTabBarBouncesContentView {
    override init(frame: CGRect) {
        super.init(frame: frame)
        textColor = blackTextColor14
        highlightTextColor = themeColor
        
        backdropColor = .white
        
        imageView.frame.size.width = 30
        imageView.frame.size.height = 30
    }
    
    public required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
