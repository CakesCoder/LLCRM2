//
//  ClientPerosnDynamicViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit
//import JXPagingVierw

extension ClientPerosnDynamicViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ClientPerosnDynamicViewController: JXPagingViewListViewDelegate {
    
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return tableView!
    }
}

class ClientPerosnDynamicViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView?
    var tableView:UITableView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.register(WorkTableViewCell.self, forCellReuseIdentifier: "WorkTableViewCell")
            obj.register(StaffTableViewCell.self, forCellReuseIdentifier: "StaffTableViewCell")
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientPerosnDynamicViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return 20
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 830
        }
        return 550
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkTableViewCell", for: indexPath) as! WorkTableViewCell
            cell.moreBlock = {
                self.popup.bottomSheet { [weak self] in
                    let showView = WorkCreateView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                    showView.featureBlock = {[weak self] tag in
                        if tag == 1000{
//                            self?.presenter?.router?.pushToNextVC(from: self!)
                        }else if tag == 1001{
//                            self?.presenter?.router?.pushAddPlan(from: self!)
                        }else if tag == 1002{
//                            self?.presenter?.router?.pushAddApproval(from: self!)
                        }else if tag == 1003{
//                            self?.presenter?.router?.pushAddTask(from: self!)
                        }else if tag == 1004{
//                            self?.presenter?.router?.pushSchedule(from: self!)
                        }
                        else{
                            
                        }
                        self?.popup.dismissPopup()
                    }
                    return showView
                }
            }
            cell.forwardBlock = {
                let showView = WorkMoreView()
                
               
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffTableViewCell", for: indexPath)
        return cell
    }
}
