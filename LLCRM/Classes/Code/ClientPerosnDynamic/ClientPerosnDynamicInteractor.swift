//
//  ClientPerosnDynamicInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnDynamicInteractor: ClientPerosnDynamicInteractorProtocols {
    var presenter: ClientPerosnDynamicPresenterProtocols?
    
    var entity: ClientPerosnDynamicEntityProtocols?
    
    func presenterRequestAddClient(by params: Any?) {
        
    }
    
    func didEntityAddClientReceiveData(by params: Any?) {
        
    }
    
    func didEntityAddClientReceiveError(error: MyError?) {
        
    }
    

}
