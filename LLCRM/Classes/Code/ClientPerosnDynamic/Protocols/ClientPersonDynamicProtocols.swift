//
//  ClientPersonDynamicProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

protocol ClientPerosnDynamicViewProtocols: AnyObject {
    var presenter: ClientPerosnDynamicPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddClientPresenterReceiveData(by params: Any?)
    func didGetAddClientPresenterReceiveError(error: MyError?)
}

protocol ClientPerosnDynamicPresenterProtocols: AnyObject{
    var view: ClientPerosnDynamicViewProtocols? { get set }
    var router: ClientPerosnDynamicRouterProtocols? { get set }
    var interactor: ClientPerosnDynamicInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddClient(by params: Any?)
    func didGetAddClientInteractorReceiveData(by params: Any?)
    func didGetAddClientInteractorReceiveError(error: MyError?)
}

protocol ClientPerosnDynamicInteractorProtocols: AnyObject {
    var presenter: ClientPerosnDynamicPresenterProtocols? { get set }
    var entity: ClientPerosnDynamicEntityProtocols? { get set }
    
    func presenterRequestAddClient(by params: Any?)
    func didEntityAddClientReceiveData(by params: Any?)
    func didEntityAddClientReceiveError(error: MyError?)
}

protocol ClientPerosnDynamicEntityProtocols: AnyObject {
    var interactor: ClientPerosnDynamicInteractorProtocols? { get set }
    
    func didAddClientnReceiveData(by params: Any?)
    func didAddClientReceiveError(error: MyError?)
    func getAddClientRequest(by params: Any?)
}

protocol ClientPerosnDynamicRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
}


