//
//  ClientPerosnDynamicPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnDynamicPresenter: ClientPerosnDynamicPresenterProtocols {
    var view: ClientPerosnDynamicViewProtocols?
    
    var router: ClientPerosnDynamicRouterProtocols?
    
    var interactor: ClientPerosnDynamicInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddClient(by params: Any?) {
        
    }
    
    func didGetAddClientInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetAddClientInteractorReceiveError(error: MyError?) {
        
    }
    
    
}
