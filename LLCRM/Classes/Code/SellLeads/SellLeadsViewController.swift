//
//  SellLeadsViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
import PKHUD
import HandyJSON

class SellLeadsViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var tableView:UITableView?
    
    var presenter: SellLeadsPresenterProtocols?
//    var current:Int? = 0
//    var dataList:[ClientDetailDataModel]? = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "销售线索"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SellLeadsViewController.backClick))
        
        view.backgroundColor = .white
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
//        let listView = UIView().then { obj in
//            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
//            obj.backgroundColor = .red
//        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        view.addSubview(self.segmentedView!)
        
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(headView.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["全部","我认领的","我分配的","我转交的"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        

        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        view.addSubview(pagingView!)
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView)
            make.bottom.equalTo(-0)
        }
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
        
        let addButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        }
        
        
        
        cofing()
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
   
    @objc func addClick(_ button:UIButton){
        presenter?.router?.pushToAddSellLeads(from: self)
    }
    
    func cofing(){
        let router = SellLeadsRouter()
        
        presenter = SellLeadsPresenter()
        
        presenter?.router = router
        
        let entity =  SellLeadsEntity()
        
        let interactor =  SellLeadsInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
//        presenter?.presenterRequestGetSellLeads(by: ["current":current, "size":10])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SellLeadsViewController: SellLeadsViewProtocols{
    func showLoading() {
//        HUD.show(.progress)
    }
    
    func showError() {
//        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
//        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetSellLeadsPresenterReceiveData(by params: Any?) {
//        if let dict = params as? [String:Any]{
//            let dataDic = dict["data"] as? [String:Any]
//            if dict["code"] as! Int == 200{
//                if let datas = JSONDeserializer<HomeClientDetailModel>.deserializeFrom(json:toJSONString(dict:dataDic!)) {
//                    printTest(datas.dataList)
//                    dataList?.removeAll()
//                    dataList?.append(contentsOf: datas.dataList ?? [])
//                    tableView?.reloadData()
//                    current += 1
//                }
//            }else{
//                HUD.flash(.label(dict["message"] as? String), delay: 2)
//            }
//        }else{
//            HUD.flash(.label("data为nil"), delay: 2)
//        }
    }
    
    func didGetSellLeadsPresenterReceiveError(error: MyError?) {
        
    }
}

extension SellLeadsViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension SellLeadsViewController: JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        let vc = SellLeadsListViewController()
        vc.sellLeadsListdidSelectedCallback = {[self] model in
            presenter?.router?.pushToSellLeadsDetail(from: self, forModel: model)
        }
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 4
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }

    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}

