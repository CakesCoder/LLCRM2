//
//  ClientPersonDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/28.
//

import UIKit
//import JXSegmentedView
//import JXPagingView
import PKHUD
import HandyJSON

class ClientPersonDetailViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var presenter: DetailPersonDetailPresenterProtocols?
    
    var nameLabel:UILabel?
    var tagImaegV:UIImageView?
    var tagImaegV2:UIImageView?
    var timeLabel:UILabel?
    var codeLabel:UILabel?
    var personNameLabel:UILabel?
    var statusLabel:UILabel?
    var contentLabel:UILabel?
    var lastTimeLabel:UILabel?
    
    var headView:UIView?
    
    var model:ClientDetailDataModel?
    var infosModel:ClientPerosonDetailModel?
    
    
    
    var titles:[String] = ["跟进动态", "详细信息", "客户地址", "商机2.0", "联系人", "报价单", "销售订单", "流程列表", "审批流程", "修改记录", "费用报销", "设备管理", "附件","服务工单", "经营商设备", "设备", "方案申请表", "订单评审表", "线路门店", "打烊申请", "销售物料申请", "商机联系人"]

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "客户详情"
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ClientPersonDetailViewController.blackClick))
        
        let lineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
            obj.backgroundColor = .white
//            view.addSubview(obj)
        }
        
        nameLabel = UILabel().then { obj in
            headView?.addSubview(obj)
//            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        tagImaegV = UIImageView().then { obj in
            headView?.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "detail_business")
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel!)
//                make.width.height.equalTo(15)
            }
        }
        
        tagImaegV2 = UIImageView().then { obj in
            headView?.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "detail_local")
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.right.equalTo(tagImaegV!.snp.left).offset(-10   )
                make.centerY.equalTo(nameLabel!)
//                make.width.height.equalTo(15)
            }
        }
        
        timeLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        codeLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(timeLabel!.snp.bottom).offset(10)
            }
        }
        
        personNameLabel = UILabel().then { obj in
            headView?.addSubview(obj)
        
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(codeLabel!.snp.bottom).offset(10)
            }
        }
        
        statusLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            let hintAttribute = NSMutableAttributedString(string:"成交状态：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(personNameLabel!.snp.bottom).offset(10)
            }
        }
        
        contentLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.layer.cornerRadius = 2
            obj.font = kRegularFont(9)
            obj.textAlignment = .center
            obj.textColor = .white
            obj.text = "暂无"
            obj.snp.makeConstraints { make in
                make.left.equalTo(statusLabel!.snp.right).offset(0)
                make.centerY.equalTo(statusLabel!)
                make.width.equalTo(40)
                make.height.equalTo(20)
            }
        }
        
        lastTimeLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(statusLabel!.snp.bottom).offset(10)
            }
        }
        
        let lineView2 = UIView().then { obj in
            headView?.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lastTimeLabel!.snp.bottom).offset(10)
                make.height.equalTo(0.5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        headView?.addSubview(self.segmentedView!)
//        segmentedView?.backgroundColor = .red
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(lineView2.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = titles
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = segmentedTitleDataSource
        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        headView?.addSubview(pagingView!)
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
//        pagingView?.listContainerView?.customScrollView = segmentedView?.contentScrollView
//        segmentedView?.listContainer = pagingView?.listContainerView as? any JXSegmentedViewListContainer
        pagingView?.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView!)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        _ = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        let editorImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_editor")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }

        let editorLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "编辑"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(editorImageV.snp.bottom).offset(5)
                make.centerX.equalToSuperview()
            }
        }

        let editorButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.addTarget(self, action: #selector(editorClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalToSuperview()
            }
        }

        let recordImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_record")
            obj.snp.makeConstraints { make in
                make.right.equalTo(editorImageV.snp.left).offset(-68)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }

        let recordLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "沟通记录"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(recordImageV.snp.bottom).offset(5)
                make.centerX.equalTo(recordImageV)
            }
        }

        let recordButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(recordImageV)
            }
        }


        let moreImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_more")
            obj.snp.makeConstraints { make in
                make.left.equalTo(editorImageV.snp.right).offset(68)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }

        let moreLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "更多"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(moreImageV.snp.bottom).offset(5)
                make.centerX.equalTo(moreImageV)
            }
        }

        let moreButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(moreImageV)
            }
        }
        
//        segmentedView?.listContainer = pagingView?.listContainerView as? JXSegmentedViewListContainer
        cofing()
    }
//    class AddClientViewController: BaseViewController, MyDateViewDelegate {
    @objc func editorClick(){
        let vc = AddClientViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = DetailPersonDetailRouter()
        
        presenter = DetailPersonDetailPresenter()
        
        presenter?.router = router
        
        let entity =  DetailPersonDetailEntity()
        
        let interactor =  DetailPersonDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.presenterRequestGetDetailPersonDetail(by: ["id": model?.id ?? ""])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientPersonDetailViewController : JXSegmentedViewDelegate, JXPagingViewDelegate{
    
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = ClientPerosnDynamicViewController()
//            vc.pagingScrollView = pagingView.listContainerView.scrollView
            return vc
        }else if index == 1{
            let vc = ClientPerosnInfosViewController()
//            vc.pagingScrollView = pagingView.listContainerView.scrollView/
//            vc.pagingScrollView = segmentedView?.collectionView
            vc.model = infosModel
            return vc
        }else if index == 2{
            let vc = ClientPerosnAddressViewController()
//            vc.pagingScrollView = pagingView.listContainerView.scrollView
//            vc.pagingScrollView = segmentedView?.collectionView
            vc.model = infosModel
            return vc
        }else if index == 3{
            let vc = ClientPerosnBusinessViewController()
            vc.model = model
//            vc.pagingScrollView = segmentedView?.collectionView
            vc.pushAddPriceCallback = { [self] in
//                presenter?.router?.pushToAddPrice(from: self)
            }
            return vc
        }else if index == 4{
            let vc = ClientPerosnLinkManViewController()
//            vc.pagingScrollView = segmentedView?.collectionView
            vc.model = infosModel
            return vc
        }else if index == 5{
            let vc = ClientPerosnPriceViewController()
//            vc.pagingScrollView = segmentedView?.collectionView
            vc.tableViewDidSeletedCallback = { [self] model in
//                if index == 0{
                    presenter?.router?.pushToPriceDetail(from: self, forModel: model)
//                }else{
//                    presenter?.router?.pushToSpendDetail(from: self)
//                }
            }
            return vc
        }else if index == 6{
            let vc = SeilOrderViewController()
            vc.pagingScrollView = segmentedView?.collectionView
            return vc
        }else if index == 7{
            let vc = FlowListViewController()
            vc.pagingScrollView = segmentedView?.collectionView
            return vc
        }else if index == 8{
            let vc = ApprovalFlowViewController()
            vc.pagingScrollView = segmentedView?.collectionView
            return vc
        }else if index == 9{
            let vc = ChangeRecordViewController()
//            vc.pagingScrollView = segmentedView?.collectionView
            return vc
        }else if index == 10{
            let vc = ApplyDetailViewController()
//            vc.pagingScrollView = segmentedView?.collectionView
            return vc
        }else if index == 11{
            let vc = EquipmentManagerViewController()
            return vc
        }else if index == 12{
            let vc = ProductAttachementViewController()
            return vc
        }else if index == 13{
            let vc = WorkOrderViewController()
            return vc
        }else if index == 14{
            let vc = ManagerDeviceViewController()
            return vc
        }else if index == 15{
            let vc = EquipmentViewController()
            return vc
        }else if index == 16{
            let vc = CaseTableViewController()
            return vc
        }else if index == 17{
            let vc = OrderTableViewController()
            return vc
        }else if index == 18{
            let vc = RouterShopViewController()
            return vc
        }else if index == 19{
            let vc = ApplycloseViewController()
            return vc
        }else if index == 20{
            let vc = ApplyMaterialViewController()
            return vc
        }
        let vc = BusinessLinkManViewController()
        vc.model = infosModel
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return titles.count
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
    
//    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        printTest("aaaaaaaaaa11111")
//        pagingView?.listContainerView.collectionView.contentOffset = CGPoint(x: index*Int(kScreenWidth), y: 0)
//    }
    
//    func segmentedView(_ segmentedView: JXSegmentedView, didScrollSelectedItemAt index: Int) {
//        printTest(index)
////        pagingView(pagingView!, initListAtIndex: index)
//    }
}

extension ClientPersonDetailViewController: DetailPersonDetailViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetDetailPersonDetailPresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            printTest(dict)
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if let model = JSONDeserializer<ClientPerosonDetailModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                    nameLabel?.text = model.clientName ?? "暂无"

                    let timeHintAttribute = NSMutableAttributedString(string:"创建时间：")
                    timeHintAttribute.yy_color = blacktextColor52
                    timeHintAttribute.yy_font = kMediumFont(13)
                    let timeCotentAttribute = NSMutableAttributedString(string: model.assignDate ?? "暂无")
                    timeCotentAttribute.yy_color = blackTextColor80
                    timeCotentAttribute.yy_font = kMediumFont(13)
                    timeHintAttribute.append(timeCotentAttribute)
                    timeLabel!.attributedText = timeHintAttribute

                    let codeHintAttribute = NSMutableAttributedString(string:"客户编号：")
                    codeHintAttribute.yy_color = blacktextColor52
                    codeHintAttribute.yy_font = kMediumFont(13)
                    let codeCotentAttribute = NSMutableAttributedString(string:model.creditCode ?? "暂无")
                    codeCotentAttribute.yy_color = blackTextColor80
                    codeCotentAttribute.yy_font = kMediumFont(13)
                    codeHintAttribute.append(codeCotentAttribute)
                    codeLabel!.attributedText = codeHintAttribute

                    let accountHintAttribute = NSMutableAttributedString(string:"负责人：")
                    accountHintAttribute.yy_color = blacktextColor52
                    accountHintAttribute.yy_font = kMediumFont(13)
                    let accoubtCotentAttribute = NSMutableAttributedString(string:model.clientClueContactSimpleParamList?[0].name ?? "暂无")
                    accoubtCotentAttribute.yy_color = blackTextColor80
                    accoubtCotentAttribute.yy_font = kMediumFont(13)
                    accountHintAttribute.append(accoubtCotentAttribute)
                    personNameLabel!.attributedText = accountHintAttribute

                    let lastTimeHintAttribute = NSMutableAttributedString(string: "最后跟进时间：")
                    lastTimeHintAttribute.yy_color = blacktextColor52
                    lastTimeHintAttribute.yy_font = kMediumFont(13)
                    let lastTimeCotentAttribute = NSMutableAttributedString(string: model.claimDate ?? "暂无")
                    lastTimeCotentAttribute.yy_color = blackTextColor80
                    lastTimeCotentAttribute.yy_font = kMediumFont(13)
                    lastTimeHintAttribute.append(lastTimeCotentAttribute)
                    lastTimeLabel!.attributedText = lastTimeHintAttribute
                    
                    infosModel = model
                    
                    pagingView?.reloadData()
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetDetailPersonDetailPresenterReceiveError(error: MyError?) {
       
    }
    
}
