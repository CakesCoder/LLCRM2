//
//  ClientPersonDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

protocol DetailPersonDetailViewProtocols: AnyObject {
    var presenter: DetailPersonDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetDetailPersonDetailPresenterReceiveData(by params: Any?)
    func didGetDetailPersonDetailPresenterReceiveError(error: MyError?)
}

protocol DetailPersonDetailPresenterProtocols: AnyObject{
    var view: DetailPersonDetailViewProtocols? { get set }
    var router: DetailPersonDetailRouterProtocols? { get set }
    var interactor: DetailPersonDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetDetailPersonDetail(by params: Any?)
    func didGetDetailPersonDetailInteractorReceiveData(by params: Any?)
    func didGetDetailPersonDetailInteractorReceiveError(error: MyError?)
}

protocol DetailPersonDetailInteractorProtocols: AnyObject {
    var presenter: DetailPersonDetailPresenterProtocols? { get set }
    var entity: DetailPersonDetailEntityProtocols? { get set }
    
    func presenterRequestDetailPersonDetail(by params: Any?)
    func didEntityDetailPersonDetailReceiveData(by params: Any?)
    func didEntityDetailPersonDetailReceiveError(error: MyError?)
}

protocol DetailPersonDetailEntityProtocols: AnyObject {
    var interactor: DetailPersonDetailInteractorProtocols? { get set }
    
    func didDetailPersonDetailReceiveData(by params: Any?)
    func didDetailPersonDetailReceiveError(error: MyError?)
    func getDetailPersonDetailRequest(by params: Any?)
}

protocol DetailPersonDetailRouterProtocols: AnyObject {
//    func pushToAddLinkMan(from previousView: UIViewController)
    func pushToAddPrice(from previousView: UIViewController)
    func pushToPriceDetail(from previousView: UIViewController,forModel model: Any)
    func pushToSpendDetail(from previousView: UIViewController)
}
