//
//  DetailPersonDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class DetailPersonDetailEntity: DetailPersonDetailEntityProtocols {
    var interactor: DetailPersonDetailInteractorProtocols?
    
    func didDetailPersonDetailReceiveData(by params: Any?) {
        interactor?.didEntityDetailPersonDetailReceiveData(by: params)
    }
    
    func didDetailPersonDetailReceiveError(error: MyError?) {
        interactor?.didEntityDetailPersonDetailReceiveError(error: error)
    }
    
    func getDetailPersonDetailRequest(by params: Any?) {
        LLNetProvider.request(.getCompanyInfos(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didDetailPersonDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didDetailPersonDetailReceiveError(error: .requestError)
            }
        }
    }
}
