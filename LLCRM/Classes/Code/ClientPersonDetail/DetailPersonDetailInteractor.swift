//
//  DetailPersonDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class DetailPersonDetailInteractor: DetailPersonDetailInteractorProtocols {
    var presenter: DetailPersonDetailPresenterProtocols?
    
    var entity: DetailPersonDetailEntityProtocols?
    
    func presenterRequestDetailPersonDetail(by params: Any?) {
        entity?.getDetailPersonDetailRequest(by: params)
    }
    
    func didEntityDetailPersonDetailReceiveData(by params: Any?) {
        presenter?.didGetDetailPersonDetailInteractorReceiveData(by: params)
    }
    
    func didEntityDetailPersonDetailReceiveError(error: MyError?) {
        presenter?.didGetDetailPersonDetailInteractorReceiveError(error: error)
    }
    
    
}
