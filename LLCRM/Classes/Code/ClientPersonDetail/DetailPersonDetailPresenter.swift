//
//  DetailPersonDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class DetailPersonDetailPresenter: DetailPersonDetailPresenterProtocols {
    var view: DetailPersonDetailViewProtocols?
    
    var router: DetailPersonDetailRouterProtocols?
    
    var interactor: DetailPersonDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetDetailPersonDetail(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestDetailPersonDetail(by: params)
    }
    
    func didGetDetailPersonDetailInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetDetailPersonDetailPresenterReceiveData(by: params)
    }
    
    func didGetDetailPersonDetailInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetDetailPersonDetailPresenterReceiveError(error: error)
    }
    

}
