//
//  ClientPerosonDetailModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/3.
//

import UIKit
import HandyJSON
class ClientPerosonDetailModel: BaseModel {
    var address: String?
    var area: String?
    var assignDate: String?
    var assignStatus: String?
    var assignerName: String?
    var city: String?
    var claimDate: String?
    var claimStatus: String?
    var claimantName: String?
    var systemCode: String?
    var clientClueContactSimpleParamList:[clientClueContactSimpleParamModel]?
    var clientId: String?
    var clientLevel: String?
    var clientName: String?
    var clueNumber: String?
    var country: String?
    var createDept: String?
    var createrId: String?
    var createrName: String?
    var creditCode: String?
    var date: String?
    var highSeas: String?
    var id: Int?
    var industry: String?
    var intro: String?
    var parentCompany: String?
    var parentIndustry: String?
    var phone: String?
    var productLine: Int?
    var receiverName: String?
    var requirement: Int?
    var scale: String?
    var source: String?
    var transferDate: String?
    var transferName: String?
    var website: Int?
    
    required init() {}
}

class clientClueContactSimpleParamModel: BaseModel{
    var address:String?
    var clientId:String?
    var department:String?
    var email:String?
    var gender:String?
    var id:Int?
    var name:String?
    var phone:String?
    var position:String?
    
    
    required init() {}
}
