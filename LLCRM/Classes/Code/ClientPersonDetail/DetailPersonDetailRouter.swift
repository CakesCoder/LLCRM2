//
//  DetailPersonDetailRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class DetailPersonDetailRouter: DetailPersonDetailRouterProtocols {
    func pushToAddPrice(from previousView: UIViewController){
        let nextView = AddPriceViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToPriceDetail(from previousView: UIViewController,forModel model: Any){
        let nextView = PriceDetailViewController()
        nextView.recordModel = model as? recordsModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToSpendDetail(from previousView: UIViewController){
        let nextView = SpeakDetailViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
//    func pushToAddPrice(from previousView: UIViewController){
//        let nextView = AddPriceViewController()
//        previousView.navigationController?.pushViewController(nextView, animated: true)
//    }
}
