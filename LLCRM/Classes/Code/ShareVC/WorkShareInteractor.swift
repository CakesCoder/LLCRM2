//
//  WorkShareInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class WorkShareInteractor: WorkShareInteractorProtocols {
    var presenter: WorkSharePresenterProtocols?
    
    var entity: WorkShareEntityProtocols?
    
    func interactorWorkShareRetrieveData() {
        
    }
    
    func entityWorkShareRetrieveData() {
        
    }
    
    func didEntityWorkShareReceiveData() {
        
    }
    
    func didEntityWorkShareReceiveError() {
        
    }
    
    
}
