//
//  WorkShareProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class WorkShareProtocols: NSObject {

}

protocol WorkShareViewProtocols: AnyObject {
    var presenter: WorkSharePresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterWorkShareViewReceiveData()
    func didPresenterWorkShareViewReceiveError()
}

protocol WorkSharePresenterProtocols: AnyObject{
    var view: WorkShareViewProtocols? { get set }
    var router: WorkShareRouterProtocols? { get set }
    var interactor: WorkShareInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorWorkShareReceiveData()
    func didInteractorWorkShareReceiveError()
}

protocol WorkShareInteractorProtocols: AnyObject {
    var presenter: WorkSharePresenterProtocols? { get set }
    var entity: WorkShareEntityProtocols? { get set }
    
    func interactorWorkShareRetrieveData()
    func entityWorkShareRetrieveData()
    func didEntityWorkShareReceiveData()
    func didEntityWorkShareReceiveError()
}

protocol WorkShareEntityProtocols: AnyObject {
    var interactor: WorkShareInteractorProtocols? { get set }
    
    func retrieveWorkShareData()
    func didWorkShareReceiveData()
}

protocol WorkShareRouterProtocols: AnyObject {
    func pushAddTask(from previousView: UIViewController)
}
