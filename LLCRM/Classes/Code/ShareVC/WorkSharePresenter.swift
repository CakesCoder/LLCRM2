//
//  WorkSharePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class WorkSharePresenter: WorkSharePresenterProtocols {
    var view: WorkShareViewProtocols?
    
    var router: WorkShareRouterProtocols?
    
    var interactor: WorkShareInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorWorkShareReceiveData() {
        
    }
    
    func didInteractorWorkShareReceiveError() {
        
    }
}
