//
//  WorkShareRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class WorkShareRouter: WorkShareRouterProtocols {
    func pushAddTask(from previousView: UIViewController){
        let nextView = AddTaskViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
