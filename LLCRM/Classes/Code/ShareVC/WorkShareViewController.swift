//
//  WorkShareViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/7.
//

import UIKit
//import NextGrowingTextView
import YYText_swift

class WorkShareViewController: BaseViewController {
    
    var textView:YYTextView?
    var presenter: WorkSharePresenterProtocols?
    
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(animated)
//        
////        var nav = self.navigationController?.navigationBar
////        nav?.barStyle = UIBarStyle.black
////        nav?.tintColor = blackTextColor3
////        nav?.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.orange]
//    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "发分享"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(WorkShareViewController.backClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem.createcommitBarbuttonItem(name: "发送", target: self, action: #selector(WorkShareViewController.addClick))
        
        view.backgroundColor = .white
        // Do any additional setup after loading the view.
        buildUI()
    }
    
    func buildUI(){
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let customView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: 94, height: 30)
        }
        
        let button = UIButton().then { obj in
//            obj.titleLabel?.textColor = workTagColor
            customView.addSubview(obj)
            obj.frame = CGRect(x: 0, y: 0, width: 94, height: 30)
            obj.titleLabel?.font = kAMediumFont(12)
            obj.setTitle("发送范围", for: .normal)
            obj.setTitleColor(workTagColor, for: .normal)
            obj.backgroundColor = bgColor4
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor12.cgColor
            obj.layer.cornerRadius = 15
            obj.addTarget(self, action: #selector(addressClick), for: .touchUpInside)
        }
        
        
        
        let barButtonItem = UIBarButtonItem().then { obj in
            obj.customView = customView
        }
        
        let topView = UIToolbar().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 40)
            obj.items = [barButtonItem]
        }
        
        textView = YYTextView().then { obj in
            view.addSubview(obj)
            obj.placeholderFont = kAMediumFont(14)
            obj.placeholderText = "请填写分享内容"
            obj.placeholderTextColor = blackTextColor25
            obj.inputAccessoryView = topView
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.right.equalToSuperview().offset(-25)
                make.top.equalTo(topLineView.snp.bottom).offset(15)
                make.height.equalTo(100)
            }
        }
        
        cofing()
    }
    
    func cofing(){
        let router = WorkShareRouter()
        
        presenter = WorkSharePresenter()
        
        presenter?.router = router
        
        let entity = WorkShareEntity()
        
        let interactor = WorkShareInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    
    @objc func addressClick(){
        printTest("添加地址")
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(){
        presenter?.router?.pushAddTask(from: self)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        textView?.resignFirstResponder()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension WorkShareViewController: WorkShareViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterWorkShareViewReceiveData() {
        
    }
    
    func didPresenterWorkShareViewReceiveError() {
        
    }
}
