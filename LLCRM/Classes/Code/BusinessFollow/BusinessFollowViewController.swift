//
//  BusinessFollowViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/11.
//

import UIKit
//import JXPagingView

extension BusinessFollowViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension BusinessFollowViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class BusinessFollowViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var followDateLabel:UILabel?
    var followThemeLabel:UILabel?
    var followContent:UILabel?
    var perosonLabel:UILabel?
    var commitDateLabel:UILabel?
    var possibilityLabel:UILabel?
    
    var model:BusinessListModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "商机"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["跟进日期：", "跟进主题：", "跟进内容：", "负责人：", "完成日期：", "可能性："]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
                if i == 0{
                    followDateLabel = obj
                    obj.text = model?.createDate ?? "--"
                }else if i == 1{
                    followThemeLabel = obj
                    obj.text = "--"
                }else if i == 2{
                    followContent = obj
                    obj.text = model?.commercialName ?? "--"
                }else if i == 3{
                    perosonLabel = obj
                    obj.text = model?.createrName ?? "--"
                }else if i == 4{
                    commitDateLabel = obj
                    obj.text = model?.productDate ?? "--"
                }else {
                    possibilityLabel = obj
                    obj.text = model?.probability ?? "--"
                }
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(120)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        
        // Do any additional setup after loading the view.
    }
    
    @objc func addClick(_ button:UIButton){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
