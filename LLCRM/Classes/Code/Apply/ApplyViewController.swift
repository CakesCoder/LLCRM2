//
//  ApplyViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit

class ApplyViewController: BaseViewController {
    var presenter: ApplyPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.blueNavTheme()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .brown
        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "应用"
       
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
            obj.backgroundColor = lineColor
        }
        
        let topFeatureView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 4;
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(kScreenWidth-30)
                make.height.equalTo(133)
            }
        }
        
        let images = ["apply_loacal", "apply_attendance", "apply_inquire", "apply_card"]
        let names = ["外勤", "考勤", "工商查询", "扫名片"]
        for i in 0..<4{
            let w = (kScreenWidth-30)/4
            let bgView = UIView().then { obj in
                topFeatureView.addSubview(obj)
                obj.frame = CGRect(x: w*CGFloat(i), y: 0, width: w, height: 133)
            }
            
            let headImageV = UIImageView().then { obj in
                bgView.addSubview(obj)
                obj.image = UIImage(named: images[i])
                obj.snp.makeConstraints { make in
                    make.centerX.equalTo(bgView)
                    make.top.equalToSuperview().offset(0)
                }
            }
            
            _ = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.textColor = blackTextColor
                obj.font = kMediumFont(12)
                obj.text = names[i]
                obj.snp.makeConstraints { make in
                    make.top.equalTo(headImageV.snp.bottom).offset(-20)
                    make.centerX.equalTo(bgView)
                }
            }
            
            _ = UIButton().then({ obj in
                bgView.addSubview(obj)
                obj.tag = 1000+i
                obj.addTarget(self, action: #selector(topFeatureClick(_ :)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            })
        }
        
        let centerFeatureView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 4;
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(topFeatureView.snp.bottom).offset(15)
                make.width.equalTo(kScreenWidth-30)
                make.height.equalTo(258)
            }
        }
        
        let centerImages = ["apply_zhushou", "apply_mail", "apply_shoukuan", "apply_guanli", "apply_fuli", "apply_jifen", "apply_chaxun", "apply_yinxiao"]
        let centerNames = ["培训助手", "邮箱", "扫码收款", "假期管理", "福利", "员工积分", "设备查询", "营销通"]
        for i in 0..<centerImages.count{
            let w = (kScreenWidth-30)/4
            let x = i%4
            let y = i/4
            let bgView = UIView().then { obj in
                centerFeatureView.addSubview(obj)
                obj.frame = CGRect(x: Int(w)*x, y: 37+y*90, width: Int(w), height: 90)
            }
            
            let headImageV = UIImageView().then { obj in
                bgView.addSubview(obj)
                obj.image = UIImage(named: centerImages[i])
                obj.snp.makeConstraints { make in
                    make.centerX.equalTo(bgView)
                    make.top.equalToSuperview().offset(0)
                }
            }
            
            _ = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.textColor = blackTextColor
                obj.font = kMediumFont(12)
                obj.text = centerNames[i]
                obj.snp.makeConstraints { make in
                    make.top.equalTo(headImageV.snp.bottom).offset(10)
                    make.centerX.equalTo(bgView)
                }
            }
            
            _ = UIButton().then({ obj in
                bgView.addSubview(obj)
                obj.tag = 1004+i
                obj.addTarget(self, action: #selector(topFeatureClick(_ :)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            })
        }
        
        let plazeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 4;
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(centerFeatureView.snp.bottom).offset(15)
                make.width.equalTo(kScreenWidth-30)
                make.height.equalTo(50)
            }
        }
        
        let plazeImageV = UIImageView().then { obj in
            plazeView.addSubview(obj)
            obj.image = UIImage(named: "apply_guangchang")
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let plazeLabel = UILabel().then { obj in
            plazeView.addSubview(obj)
            obj.textColor = blackTextColor
            obj.font = kMediumFont(12)
            obj.text = "应用广场"
            obj.snp.makeConstraints { make in
                make.left.equalTo(plazeImageV.snp.right).offset(3)
                make.centerY.equalToSuperview()
            }
        }
        
        let plazeButton = UIButton().then { obj in
            plazeView.addSubview(obj)
            obj.tag = 1012
            obj.addTarget(self, action: #selector(topFeatureClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
    }
    
    func cofing(){
        let router = ApplyRouter()
        
        presenter = ApplyPresenter()
        presenter?.router = router
        
        let entity = ApplyEntity()
        
        let interactor = ApplyInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
    }
    
    @objc func topFeatureClick(_ button: UIButton){
        printTest("asdasdasd")
        printTest(button.tag)
        if button.tag == 1000{
            self.presenter?.router?.pushToAddWork(from: self)
        }else if button.tag == 1001{
            self.presenter?.router?.pushToPunchCard(from: self)
        }else if button.tag == 1002{
            self.presenter?.router?.pushToCommerceCheck(from: self)
        }else if button.tag == 1003{
            
        }else if button.tag == 1004{
            
        }else if button.tag == 1005{
            
        }else if button.tag == 1006{
            
        }else if button.tag == 1007{
            self.presenter?.router?.pushToVacationManage(from: self)
        }else if button.tag == 1008{
            
        }else if button.tag == 1009{
            
        }else if button.tag == 1010{
            
        }else if button.tag == 1011{
            self.presenter?.router?.pushToMarkering(from: self)
        }else if button.tag == 1012{
            self.presenter?.router?.pushToAppPlaza(from: self)
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ApplyViewController:ApplyViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
}
