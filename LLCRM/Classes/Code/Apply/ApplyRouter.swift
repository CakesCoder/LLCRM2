//
//  ApplyRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyRouter: ApplyRouterProtocols {
    
    
    
    func pushToVacationManage(from previousView: UIViewController) {
        let vc = VacationManageViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToAddWork(from previousView: UIViewController) {
        let vc = ApplyWorkDateViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToPunchCard(from previousView: UIViewController){
        let vc = PunchCardViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToCommerceCheck(from previousView: UIViewController) {
        let vc = CommerceCheckViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToAppPlaza(from previousView: UIViewController) {
        let vc = AppPlazaViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToMarkering(from previousView: UIViewController){
        let vc = MarketingViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
}
