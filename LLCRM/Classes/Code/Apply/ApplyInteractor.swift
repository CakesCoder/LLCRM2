//
//  ApplyInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyInteractor: ApplyInteractorProtocols {
    var presenter: ApplyPresenterProtocols?
    
    var entity: ApplyEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
    
}
