//
//  ApplyRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

protocol ApplyViewProtocols: AnyObject {
    // 持有一个presenter，帮我做事
    var presenter: ApplyPresenterProtocols? { get set }
    
    /*
     这些方法，都是UI回调相关的方法
     让我的presenter帮我做事情，等presenter给我回调结果就好了
     */
    func showLoading()
    func showError()
    func hideLoading()
}

protocol ApplyPresenterProtocols: AnyObject{
    var view: ApplyViewProtocols? { get set }
    var router: ApplyRouterProtocols? { get set }
    var interactor: ApplyInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol ApplyInteractorProtocols: AnyObject {
    var presenter: ApplyPresenterProtocols? { get set }
    var entity: ApplyEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol ApplyEntityProtocols: AnyObject {
    var interactor: ApplyInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol ApplyRouterProtocols: AnyObject {
    func pushToAddWork(from previousView: UIViewController)
    func pushToPunchCard(from previousView: UIViewController)
    func pushToCommerceCheck(from previousView: UIViewController)
    func pushToAppPlaza(from previousView: UIViewController)
    func pushToMarkering(from previousView: UIViewController)
    func pushToVacationManage(from previousView: UIViewController)
}

