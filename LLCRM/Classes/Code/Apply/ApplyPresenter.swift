//
//  ApplyPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/18.
//

import UIKit

class ApplyPresenter: ApplyPresenterProtocols {
    var view: ApplyViewProtocols?
    
    var router: ApplyRouterProtocols?
    
    var interactor: ApplyInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    
    
}
