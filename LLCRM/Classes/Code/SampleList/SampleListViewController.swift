//
//  SampleListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh

class SampleListViewController: BaseViewController {
    
    var presenter: SampleListPresenterProtocols?
    var current:Int = 1
    var dataList:[DeviceSampleVoListModel]? = []
    var tableView:UITableView?
    var dataModdel:SampleListModel?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "样品记录"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SampleListViewController.backClick))

        // Do any additional setup after loading the view.
        
        view.backgroundColor = lineColor
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(SampleListTableViewCell.self, forCellReuseIdentifier: "SampleListTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-10)
            }
        })
        
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
            presenter?.presenterRequestGetSampleList(by: ["current":current, "size":20])
            tableView?.stopFooterRefreshing()
        }))
        
        let addButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        }
        
        cofing()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedSampleListAction(_:)), name: Notification.Name(rawValue: "SampleListNotificationKey"), object: nil)
    }
    
    @objc func didSelectedSampleListAction(_ notification:NSNotification){
        current = 1
        presenter?.presenterRequestGetSampleList(by: ["current":current, "size":20])
    }
    
    @objc func addClick(_ button:UIButton){
        presenter?.router?.pushToAddSample(from: self)
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = SampleListRouter()
        
        presenter = SampleListPresenter()
        
        presenter?.router = router
        
        let entity = SampleListEntity()
        
        let interactor = SampleListInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.presenterRequestGetSampleList(by: ["current":current, "size":20])
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SampleListViewController: SampleListViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    
    func didGetSampleListPresenterReceiveData(by params: Any?) {
        printTest(params)
        if current == 1{
            dataList = []
        }
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let datas = dict["data"] as? [String:Any]{
                    if datas.count > 0{
                        if let dataModel = JSONDeserializer<SampleListModel>.deserializeFrom(json:toJSONString(dict: datas as! [String : Any])) {
                            self.dataList?.append(contentsOf: dataModel.deviceSampleVoList!)
                            self.dataModdel = dataModel
                        }
                        current += 1
                        tableView?.reloadData()
                    }
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetSampleListPresenterReceiveError(error: MyError?) {
        
    }
}

extension SampleListViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 141
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SampleListTableViewCell", for: indexPath) as! SampleListTableViewCell
        cell.model = dataList?[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.router?.pushToSampleDetail(from: self,forModel:  dataList?[indexPath.row],forListModdel:dataModdel)
    }
}
