//
//  SampleListTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class SampleListTableViewCell: UITableViewCell {
    
    var nameLabel:UILabel?
    var tagLabel:UILabel?
    var clientNameLabel:UILabel?
    var sampleCodeLabel:UILabel?
    var sampleTypeLabel:UILabel?
    var applyDateLabel:UILabel?
    
    var model:DeviceSampleVoListModel?{
        didSet{
            clientNameLabel?.text = model?.customerName ?? "暂无"
            
            let hintCodeAttribute = NSMutableAttributedString(string:"样品单号：")
            hintCodeAttribute.yy_color = blackTextColor84
            hintCodeAttribute.yy_font = kMediumFont(12)

            let cotentCodeAttribute = NSMutableAttributedString(string: model?.sampleNumber ?? "暂无")
            cotentCodeAttribute.yy_color = bluebgColor
            cotentCodeAttribute.yy_font = kMediumFont(12)
            hintCodeAttribute.append(cotentCodeAttribute)
            sampleCodeLabel?.attributedText = hintCodeAttribute
            
            let hintTypeAttribute = NSMutableAttributedString(string:"物料类别：")
            hintTypeAttribute.yy_color = blackTextColor84
            hintTypeAttribute.yy_font = kMediumFont(12)

            let cotentTypeAttribute = NSMutableAttributedString(string: model?.materialCategory ?? "暂无")
            cotentTypeAttribute.yy_color = bluebgColor
            cotentTypeAttribute.yy_font = kMediumFont(12)
            hintTypeAttribute.append(cotentTypeAttribute)
            sampleTypeLabel?.attributedText = hintTypeAttribute
            
            applyDateLabel?.text = model?.RFQCreateTime ?? "暂无"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "crm_notification")
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
                make.width.height.equalTo(15)
            }
        }
        
        nameLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "样品新建信息"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor84
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(5)
                make.top.equalToSuperview().offset(15)
            }
        })
        
        tagLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.layer.cornerRadius = 2
            obj.textColor = .white
            obj.font = kRegularFont(8)
            obj.text = "已提"
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel!.snp.right).offset(8)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(26)
                make.height.equalTo(16)
            }
        })
        
//        nameLabel = UILabel().then({ obj in
//            contentView.addSubview(obj)
//            obj.text = "商机跟进提醒"
//            obj.font = kRegularFont(12)
//            obj.textColor = blackTextColor84
//            obj.snp.makeConstraints { make in
//                make.left.equalTo(imageV.snp.right).offset(5)
////                make.centerX.equalTo(imageV)
//                make.top.equalToSuperview().offset(15)
//            }
//        })
        
        clientNameLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "客户名称：安高创新技术（深圳）有限公司"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(8)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        
        
        sampleCodeLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(clientNameLabel!.snp.bottom).offset(8)
                make.left.equalToSuperview().offset(35)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        sampleTypeLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(sampleCodeLabel!.snp.bottom).offset(8)
                make.left.equalToSuperview().offset(35)
                make.right.equalToSuperview().offset(-35)
            }
        })
        
        applyDateLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
//            obj.text = "2022.05.16  09:30"
            obj.textColor = blackTextColor84
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
//                make.top.equalTo(sampleCodeLabel!.snp.bottom).offset/(5)
//                make.left.equalToSuperview().offset(35)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-20)
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        })
        
        
        _ = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "home_push")
//            obj.backgroundColor = .reds
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(12)
            }
        })
    }

}
