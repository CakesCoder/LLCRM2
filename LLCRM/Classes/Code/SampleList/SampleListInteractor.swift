//
//  SampleListInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class SampleListInteractor: SampleListInteractorProtocols {
    var presenter: SampleListPresenterProtocols?
    
    var entity: SampleListEntityProtocols?
    
    func presenterRequestSampleList(by params: Any?) {
        entity?.getSampleListRequest(by: params)
    }
    
    func didEntitySampleListReceiveData(by params: Any?) {
        presenter?.didGetSampleListInteractorReceiveData(by: params)
    }
    
    func didEntitySampleListReceiveError(error: MyError?) {
        presenter?.didGetSampleListInteractorReceiveError(error: error)
    }
    

}
