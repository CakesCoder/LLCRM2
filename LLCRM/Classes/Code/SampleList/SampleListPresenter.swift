//
//  SampleListPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class SampleListPresenter: SampleListPresenterProtocols {
    var view: SampleListViewProtocols?
    
    var router: SampleListRouterProtocols?
    
    var interactor: SampleListInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetSampleList(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestSampleList(by: params)
    }
    
    func didGetSampleListInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetSampleListPresenterReceiveData(by: params)
    }
    
    func didGetSampleListInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetSampleListPresenterReceiveError(error: error)
    }
    

}
