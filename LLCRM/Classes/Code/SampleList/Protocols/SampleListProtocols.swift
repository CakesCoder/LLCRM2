//
//  SampleListProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit


protocol SampleListViewProtocols: AnyObject {
    var presenter: SampleListPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetSampleListPresenterReceiveData(by params: Any?)
    func didGetSampleListPresenterReceiveError(error: MyError?)
}

protocol SampleListPresenterProtocols: AnyObject{
    var view: SampleListViewProtocols? { get set }
    var router: SampleListRouterProtocols? { get set }
    var interactor: SampleListInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetSampleList(by params: Any?)
    func didGetSampleListInteractorReceiveData(by params: Any?)
    func didGetSampleListInteractorReceiveError(error: MyError?)
}

protocol SampleListInteractorProtocols: AnyObject {
    var presenter: SampleListPresenterProtocols? { get set }
    var entity: SampleListEntityProtocols? { get set }
    
    func presenterRequestSampleList(by params: Any?)
    func didEntitySampleListReceiveData(by params: Any?)
    func didEntitySampleListReceiveError(error: MyError?)
}

protocol SampleListEntityProtocols: AnyObject {
    var interactor: SampleListInteractorProtocols? { get set }
    
    func didSampleListReceiveData(by params: Any?)
    func didSampleListReceiveError(error: MyError?)
    func getSampleListRequest(by params: Any?)
}

protocol SampleListRouterProtocols: AnyObject {
    func pushToAddSample(from previousView: UIViewController)
    func pushToSampleDetail(from previousView: UIViewController,forModel model: Any, forListModdel dataModel:Any)
}


