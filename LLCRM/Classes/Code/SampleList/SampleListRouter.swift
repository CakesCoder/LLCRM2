//
//  SampleListRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class SampleListRouter: SampleListRouterProtocols {
    func pushToAddSample(from previousView: UIViewController) {
        let nextView = AddSamplePageViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToSampleDetail(from previousView: UIViewController,forModel model: Any, forListModdel dataModel:Any) {
        let nextView = SampleDetailViewController()
        nextView.model = model as? DeviceSampleVoListModel
        nextView.dataModel = dataModel as? SampleListModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
