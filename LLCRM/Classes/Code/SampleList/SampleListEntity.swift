//
//  SampleListEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit

class SampleListEntity: SampleListEntityProtocols {
    var interactor: SampleListInteractorProtocols?
    
    func didSampleListReceiveData(by params: Any?) {
        interactor?.didEntitySampleListReceiveData(by: params)
    }
    
    func didSampleListReceiveError(error: MyError?) {
        interactor?.didEntitySampleListReceiveError(error: error)
    }
    
    func getSampleListRequest(by params: Any?) {
        
        LLNetProvider.request(.deviceSummary(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didSampleListReceiveData(by:jsonData)
            case .failure(_):
                self.didSampleListReceiveError(error: .requestError)
            }
        }
    }
    

}
