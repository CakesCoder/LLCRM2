//
//  SampleListModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class SampleListModel: BaseModel {
    var current:Int?
    var deviceSampleVoList:[DeviceSampleVoListModel]?
    var pages:Int?
    var size:Int?
    var total:Int?
    required init() {}
}

class DeviceSampleVoListModel: BaseModel{
    var RFQ:String?
    var RFQCreateBy:String?
    var RFQCreateDept:String?
    var RFQCreateTime:String?
    var RFQCreateUserId:String?
    var RFQCurrency:String?
    var RFQLadder:String?
    var RFQNumber:String?
    var RFQTax:String?
    var RFQTaxRate:String?
    var applicationProject:String?
    var applyBy:String?
    var applyDate:String?
    var appointReason:String?
    var askType:String?
    var authAsk:String?
    var authRequirement:Int?
    var buyerId:String?
    var buyerName:String?
    var changeProject:String?
    var changeReason:String?
    var changeStatus:String?
    var chooseSupplierId:String?
    var closeState:Int?
    var companyId:Int?
    var createTime:String?
    var cusProject:CusProjectModel?
    var customerName:String?
    var deleted:Int?
    var demandDate:String?
    var demandPriority:String?
    var demandQuantity:String?
    var deptId:String?
    var deptName:String?
    var deviceMaintain:Int?
    var deviceSampleAuthList:[DeviceSampleAuthListModel]?
    var deviceSampleFileList:[DeviceSampleFileListModel]?
    var deviceSampleSpecsList:[DeviceSampleSpecsList]?
    var environmentProtectionRequirement:String?
    var environmentProtectionType:String?
    var estimateAmounts:String?
    var examineStatus:Int?
    var externalPushStatus:Int?
    var externalUserId:String?
    var functionAsk:String?
    var grade:String?
    var inspectionStatus:String?
    var issue:Int?
    var manufacturerModel:String?
    var materialApplyCause:String?
    var materialApplyNumber:String?
    var materialApplyReason:Int?
    var materialApplyReasonDetail:String?
    var materialAuth:Int?
    var materialBrand:Int?
    var materialBrandAppoint:String?
    var materialBrandRecommend:String?
    var materialCategory:String?
    var materialCategoryId:Int?
    var materialName:String?
    var materialSampleNumber:String?
    var newProject:String?
    var offerManageId:String?
    var otherAsk:String?
    var otherRequirement:String?
    var packingAsk:String?
    var paramAsk:String?
    var partNumber:String?
    var price:String?
    var principalBy:String?
    var principalDept:String?
    var productImages:String?
    var projectCode:String?
    var projectModel:String?
    var projectName:String?
    var promiseDate:String?
    var promiseQuantity:String?
    var provideStatus:String?
    var purchasePrincipal:String?
    var realization:String?
    var reason:Int?
    var recipientAddress:String?
    var recipientName:String?
    var recipientPhone:String?
    var recommendBrand:String?
    var referencePrice:String?
    var remark:String?
    var replaceMaterial:String?
    var replaceState:String?
    var reviewStatus:String?
    var rfqcreateTime:String?
    var saleIncharge:String?
    var saleInchargeName:String?
    var sampleNumber:String?
    var sampleReceived:Int?
    var sampleStage:Int?
    var sampleSupplierName:String?
    var scanner:String?
    var selectionRuleStatus:Int?
    var shipmentStatus:Int?
    var source:Int?
    var specs:String?
    var srmId:String?
    var srmTenantId:String?
    var supplierId:String?
    var supplierStatus:Int?
    var targetPrice:String?
    var termination:String?
    var transportAsk:String?
    var unit:String?
    var updateTime:String?
    var whetherCruxDevice:Int?
    var manufacturerName:String?
    var id:String?
    
    
    required init() {}
}

class CusProjectModel:BaseModel{
    var angle:String?
    var angleValue:String?
    var batchProductionDate:String?
    var code:String?
    var consumptionUnit:String?
    var endDate:String?
    var isTargetPrice:String?
    var lifeCycle:String?
    var lifeCycleUnit:String?
    var name:String?
    var startDate:String?
    var status:String?
    var targetPrice:String?
    var targetPriceUnit:String?
    var taxInclude:String?
    var taxRate:String?
    var timeUnit:String?
    
    required init() {}
}

class DeviceSampleFileListModel:BaseModel{
    var companyId:Int?
    var createTime:String?
    var fileName:String?
    var fileType:String?
    var fileUrl:String?
    var format:String?
    var handleId:String?
    var id:Int?
    var sampleId:String?
    var uploadBy:String?
    var uploadDate:String?
    var version:String?
    
    required init() {}
}

class DeviceSampleSpecsList:BaseModel{
    var accordWith:String?
    var actualCondition:String?
    var actualLeftValue:String?
    var actualRightValue:String?
    var companyId:Int?
    var condition:Int?
    var createTime:String?
    var handleId:String?
    var id:Int?
    var leftValue:String?
    var rightValue:String?
    var sampleId:String?
    var specsConfirm:String?
    var specsName:String?
    var unit:String?
    
    
    required init(){}
}

class DeviceSampleAuthListModel:BaseModel{
    var authMechanism:String?
    var authType:String?
    var companyId:String?
    var createTime:String?
    var handleId:String?
    var id:String?
    var nature:String?
    var remark:String?
    var sampleId:String?
    
    required init(){}
}
