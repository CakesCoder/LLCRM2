//
//  SpendDetailView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class SpendDetailView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    var closeBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let bgView = UIView().then { obj in
            self.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(340)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "审批意见"
            obj.font = kRegularFont(13)
            obj.textColor = blackTextColor13
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(15)
                make.centerX.equalToSuperview()
            }
        }
        
        let button = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.setImage(UIImage(named: "close"), for: .normal)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.centerY.equalTo(titleLabel)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "客户信息真实可靠，通过审批"
            obj.font = kRegularFont(13)
            obj.textColor = blackTextColor83
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(70)
            }
        }
        
        let saveButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("通过", for: .normal)
            obj.layer.cornerRadius = 4
            obj.titleLabel?.font = kMediumFont(12)
            obj.addTarget(self, action: #selector(closeClikc(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-31)
                make.height.equalTo(40)
            }
        }
    }
    
    @objc func closeClikc(_ button:UIButton){
        self.closeBlock?()
    }

}


