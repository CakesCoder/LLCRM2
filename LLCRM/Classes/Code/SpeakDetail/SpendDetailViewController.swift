//
//  SpeakDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit
//import JXSegmentedView
//import JXPagingView
import JFPopup

class SpeakDetailViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var presenter: SpendDetailPresenterProtocols?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "费用报销单详情"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SpeakDetailViewController.blackClick))
        
        // Do any additional setup after loading the view.
        
        _ = UIView().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.height.equalTo(10)
            }
        })
        
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "管理费用报销"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        let codeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let hintAttribute = NSMutableAttributedString(string:"报价单号：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            let cotentAttribute = NSMutableAttributedString(string:"RFQ20230516001")
            cotentAttribute.yy_color = blackTextColor80
            cotentAttribute.yy_font = kMediumFont(13)
            hintAttribute.append(cotentAttribute)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
            }
        }
        
        let productLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let hintAttribute = NSMutableAttributedString(string:"业务类型：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            let cotentAttribute = NSMutableAttributedString(string:"招待/礼品/客户车费费用")
            cotentAttribute.yy_color = blackTextColor80
            cotentAttribute.yy_font = kMediumFont(13)
            hintAttribute.append(cotentAttribute)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(codeLabel.snp.bottom).offset(10)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let hintAttribute = NSMutableAttributedString(string:"创建时间：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            let cotentAttribute = NSMutableAttributedString(string:"2023.05.16  14:20:10")
            cotentAttribute.yy_color = blackTextColor80
            cotentAttribute.yy_font = kMediumFont(13)
            hintAttribute.append(cotentAttribute)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(productLabel.snp.bottom).offset(10)
            }
        }
        
        let personNameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let hintAttribute = NSMutableAttributedString(string:"创建人：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            let cotentAttribute = NSMutableAttributedString(string:"王小虎")
            cotentAttribute.yy_color = blackTextColor80
            cotentAttribute.yy_font = kMediumFont(13)
            hintAttribute.append(cotentAttribute)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(timeLabel.snp.bottom).offset(10)
            }
        }
        
        let departmentLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let hintAttribute = NSMutableAttributedString(string:"创建部门：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            let cotentAttribute = NSMutableAttributedString(string:"研发部")
            cotentAttribute.yy_color = blackTextColor80
            cotentAttribute.yy_font = kMediumFont(13)
            hintAttribute.append(cotentAttribute)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(personNameLabel.snp.bottom).offset(10)
            }
        }
        
        
        
        
        let lastTimeLabel = UILabel().then { obj in
            headView.addSubview(obj)
            
            let hintAttribute = NSMutableAttributedString(string:"最后跟进时间：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            let cotentAttribute = NSMutableAttributedString(string:"2023.05.23  14:20:10")
            cotentAttribute.yy_color = blackTextColor80
            cotentAttribute.yy_font = kMediumFont(13)
            hintAttribute.append(cotentAttribute)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(departmentLabel.snp.bottom).offset(10)
            }
        }
        
        let lineView2 = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lastTimeLabel.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        headView.addSubview(self.segmentedView!)
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(lineView2.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["审批内容", "明细", "审批意见"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        //        segmentedView?.delegate = self
        //        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        
        headView.addSubview(pagingView!)
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        _ = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        let editorImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_editor")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let editorLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "编辑"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(editorImageV.snp.bottom).offset(5)
                make.centerX.equalToSuperview()
            }
        }
        
        let editorButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalToSuperview()
            }
        }
        
        let recordImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_record")
            obj.snp.makeConstraints { make in
                make.right.equalTo(editorImageV.snp.left).offset(-68)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let recordLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "沟通记录"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(recordImageV.snp.bottom).offset(5)
                make.centerX.equalTo(recordImageV)
            }
        }
        
        let recordButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(recordImageV)
            }
        }
        
        
        let moreImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "detail_more")
            obj.snp.makeConstraints { make in
                make.left.equalTo(editorImageV.snp.right).offset(68)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let moreLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "更多"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(moreImageV.snp.bottom).offset(5)
                make.centerX.equalTo(moreImageV)
            }
        }
        
        let moreButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(moreImageV)
            }
        }
        
        cofing()
        
        self.popup.bottomSheet { [weak self] in
            let showView = SpendDetailView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
            showView.closeBlock = { [weak self] in
                self?.presenter?.router?.pushToApprovalInformation(from: self!)
                self?.popup.dismissPopup()
            }
//            showView.featureBlock = {[weak self] tag in
//                if tag == 1000{
//                    self?.presenter?.router?.pushToNextVC(from: self!)
//                }else if tag == 1001{
//                    self?.presenter?.router?.pushAddPlan(from: self!)
//                }else if tag == 1002{
//                    self?.presenter?.router?.pushAddApproval(from: self!)
//                }else if tag == 1003{
//                    self?.presenter?.router?.pushAddTask(from: self!)
//                }else if tag == 1004{
//                    self?.presenter?.router?.pushSchedule(from: self!)
//                }
//                else{
//                    
//                }
//                self?.popup.dismissPopup()
//            }
            return showView
        }
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    func cofing(){
        let router = SpendDetailRouter()
        
        presenter = SpendDetailPresenter()
        
        presenter?.router = router
        
        let entity = SpendDetailEntity()
        
        let interactor = SpendDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SpeakDetailViewController: JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = CheckContentViewController()
            return vc
        }else if index == 1{
            let vc = ProductInfosViewController()
            return vc
        }
        
        let vc = CheckOpinionViewController()
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 3
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
//    zZ
    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}

extension SpeakDetailViewController: SpendDetailViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetSpendDetailPresenterReceiveData(by params: Any?) {
        
    }
    
    func didGetSpendDetailPresenterReceiveError(error: MyError?) {
        
    }
}
