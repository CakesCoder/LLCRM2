//
//  SpendDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

protocol SpendDetailViewProtocols: AnyObject {
    var presenter: SpendDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetSpendDetailPresenterReceiveData(by params: Any?)
    func didGetSpendDetailPresenterReceiveError(error: MyError?)
}

protocol SpendDetailPresenterProtocols: AnyObject{
    var view: SpendDetailViewProtocols? { get set }
    var router: SpendDetailRouterProtocols? { get set }
    var interactor: SpendDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetSpendDetail(by params: Any?)
    func didGetSpendDetailInteractorReceiveData(by params: Any?)
    func didGetSpendDetailInteractorReceiveError(error: MyError?)
}

protocol SpendDetailInteractorProtocols: AnyObject {
    var presenter: SpendDetailPresenterProtocols? { get set }
    var entity: SpendDetailEntityProtocols? { get set }
    
    func presenterRequestSpendDetail(by params: Any?)
    func didEntitySpendDetailReceiveData(by params: Any?)
    func didEntitySpendDetailReceiveError(error: MyError?)
}

protocol SpendDetailEntityProtocols: AnyObject {
    var interactor: SpendDetailInteractorProtocols? { get set }
    
    func didSpendDetailReceiveData(by params: Any?)
    func didSpendDetailReceiveError(error: MyError?)
    func getSpendDetailRequest(by params: Any?)
}

protocol SpendDetailRouterProtocols: AnyObject {
    func pushToApprovalInformation(from previousView: UIViewController)
}
