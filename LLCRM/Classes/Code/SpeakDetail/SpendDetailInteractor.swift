//
//  SpendDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class SpendDetailInteractor: SpendDetailInteractorProtocols {
    var presenter: SpendDetailPresenterProtocols?
    
    var entity: SpendDetailEntityProtocols?
    
    func presenterRequestSpendDetail(by params: Any?) {
        
    }
    
    func didEntitySpendDetailReceiveData(by params: Any?) {
        
    }
    
    func didEntitySpendDetailReceiveError(error: MyError?) {
        
    }
    

}
