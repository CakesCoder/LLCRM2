//
//  SpendDetailRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class SpendDetailRouter: SpendDetailRouterProtocols {
    func pushToApprovalInformation(from previousView: UIViewController) {
        let nextView = ApprovalInformationViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
