//
//  SpendDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class SpendDetailPresenter: SpendDetailPresenterProtocols {
    var view: SpendDetailViewProtocols?
    
    var router: SpendDetailRouterProtocols?
    
    var interactor: SpendDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetSpendDetail(by params: Any?) {
        
    }
    
    func didGetSpendDetailInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetSpendDetailInteractorReceiveError(error: MyError?) {
        
    }
    
    
}
