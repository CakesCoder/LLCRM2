//
//  SpendDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/31.
//

import UIKit

class SpendDetailEntity: SpendDetailEntityProtocols {
    var interactor: SpendDetailInteractorProtocols?
    
    func didSpendDetailReceiveData(by params: Any?) {
        
    }
    
    func didSpendDetailReceiveError(error: MyError?) {
        
    }
    
    func getSpendDetailRequest(by params: Any?) {
        
    }
    

}
