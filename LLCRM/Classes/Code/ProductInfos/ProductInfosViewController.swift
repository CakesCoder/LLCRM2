//
//  ProductInfosViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit
//import JXPagingView

extension ProductInfosViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ProductInfosViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ProductInfosViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var model:PriceDetailModel?
    var productId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let baseInfos = [["*","物料类别："],["物料编码："],["*","规格描述："],["*","规格描述："],["基本单位："],["制造商："],["制造商料号："]]

        
        let contentInfos = ["电子产品", "20230523001", "鼠标", "描述描述描述描述描述描述", "PCS", "华为", "382990192012"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                        
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(82)
                    make.height.equalTo(20)
                }
            }
 
            _ = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
//                obj.text = contentInfos[i]
                let dataList = model?.productList
                if i == 0{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].productModel ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 1{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].productNo ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 2{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].productName ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 3{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].productSpecs ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 4{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].unit ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 5{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].maker ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }else if i == 6{
                    if dataList != nil && dataList?.count ?? 0 > 0{
                        obj.text = model?.productList?[0].makerCode ?? "--"
                    }else{
                        obj.text = "--"
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalTo(hintLabel.snp.right).offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
