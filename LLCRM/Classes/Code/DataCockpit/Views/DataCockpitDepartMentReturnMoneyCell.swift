//
//  DataCockpitDepartMentReturnMoneyCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/20.
//

import UIKit

class DataCockpitDepartMentReturnMoneyCell: UITableViewCell {
    
    var aaChartView: AAChartView?
    var colorList:[String]? = ["#409EFF", "#9FCEFF"]
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let departmentalImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.layer.cornerRadius = 10
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        })
    
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "各部门完成业绩占比(回款)"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(departmentalImageV.snp.right).offset(5)
                make.centerY.equalTo(departmentalImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 270
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        self.addSubview(aaChartView!)
        
        let chartModel =  AAOptions()
            .chart(AAChart()
                .type(.pie))
            .title(AATitle()
                    .text("")
                    .style(AAStyle()
                            .fontSize(0)))
            .tooltip(AATooltip()
                .pointFormat("{series.name}: <b>{point.percentage:.1f}%</b>"))
            .plotOptions(AAPlotOptions()
                .pie(AAPie()
                    .allowPointSelect(true)
                    .cursor("pointer")
                    .dataLabels(AADataLabels()
                        .enabled(true)
                        .format("{point.percentage:.1f} %")
                        .style(AAStyle()
                            .color(AAColor.white))
                        .connectorColor("silver"))))
            .series([
                AASeriesElement()
                    .colors(colorList!)
                    .data([
                        ["", 80.0],
                        ["",20.0]
                    ])
            ])
        
        
        
        self.aaChartView?.aa_drawChartWithChartOptions(chartModel)
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "已回款金额的总计：25.02万"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }


}
