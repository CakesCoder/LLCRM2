//
//  DataCockpitSeilMoneyCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/20.
//

import UIKit

class DataCockpitSeilMoneyCell: UITableViewCell {
    var aaChartView: AAChartView?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let departmentalImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.layer.cornerRadius = 10
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(15)
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        })
    
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.text = "员工订单金额ToP5销售榜"
            obj.font = kAMediumFont(15)
            obj.textColor = blackTextColor
            obj.snp.makeConstraints { make in
                make.left.equalTo(departmentalImageV.snp.right).offset(5)
                make.centerY.equalTo(departmentalImageV)
            }
        })
    
        let pushButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_push"), for: .normal)
            obj.setImage(UIImage(named: "home_push"), for: .selected)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        _ = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "home_reload"), for: .normal)
            obj.setImage(UIImage(named: "home_reload"), for: .selected)
    
            obj.snp.makeConstraints { make in
                make.right.equalTo(pushButton.snp.left).offset(-10)
                make.centerY.equalTo(departmentalImageV)
            }
        }
        
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 270
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        self.addSubview(aaChartView!)
        
        let chartModel = AAChartModel()
                .chartType(.column)//图表类型
                .animationType(.bounce)
                .dataLabelsStyle(AAStyle(color: "#525252", fontSize: 10).backgroundColor("#16B5AE").height(200))
                .xAxisVisible(false)
                .yAxisTitle("万")
                .colorsTheme(["#D1D1D1"])
                .yAxisTickPositions([0, 10, 20, 30, 40, 50])
//                .categories(["","","目标值","",""])
                .series([
                        AASeriesElement()
                            .name("目标值")
                            .data([NSNull(),45,NSNull()])
                            .color("#409EFF")
                        ])
        
        self.aaChartView?.aa_drawChartWithChartModel(chartModel)
        
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "已回款金额的总计：25.02万"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        _ =  UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "今天 09：30 更新"
            obj.font = kAMediumFont(10)
            obj.textColor = dateColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        
    }
}
