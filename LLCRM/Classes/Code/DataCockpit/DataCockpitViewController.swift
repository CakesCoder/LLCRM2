//
//  DataCockpitViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class DataCockpitViewController: BaseViewController {
    
    var headView: UIView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "数据驾驶舱"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(DataCockpitViewController.backClick))
        
        headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, 60)
            obj.backgroundColor = bgColor
        }
        
        let infoView = UIView().then { obj in
            headView?.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            infoView.addSubview(obj)
            obj.textColor = blackTextColor
            obj.font = kMediumFont(14)
            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let nameButton = UIButton().then { obj in
            infoView.addSubview(obj)
            obj.backgroundColor = bgColor22
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel.snp.right).offset(5)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(11)
            }
        }
        
        let dateLabel = UILabel().then { obj in
            infoView.addSubview(obj)
            obj.text = "本月"
            obj.textColor = blackTextColor22
            obj.font = kMediumFont(10)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let imageV = UIImageView().then { obj in
            infoView.addSubview(obj)
            obj.backgroundColor = blackTextColor22
            obj.snp.makeConstraints { make in
                make.right.equalTo(dateLabel.snp.left).offset(-5)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        _ = UIView().then({ obj in
            infoView.addSubview(obj)
            obj.backgroundColor = lineColor2
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(DataCockpitYearCell.self, forCellReuseIdentifier: "DataCockpitYearCell")
            obj.register(DataCockpitDepartMentCell.self, forCellReuseIdentifier: "DataCockpitDepartMentCell")
            obj.register(DataCockpitDepartMentOrderCell.self, forCellReuseIdentifier: "DataCockpitDepartMentOrderCell")
            obj.register(DataCockpitDepartMentReturnMoneyCell.self, forCellReuseIdentifier: "DataCockpitDepartMentReturnMoneyCell")
            obj.register(MarketDataStatusTableViewCell.self, forCellReuseIdentifier: "MarketDataStatusTableViewCell")
            
            obj.register(DataCockpitOrderCell.self, forCellReuseIdentifier: "DataCockpitOrderCell")
            obj.register(DataCockpitOrderReturnMoneyCell.self, forCellReuseIdentifier: "DataCockpitOrderReturnMoneyCell")
            
            obj.register(ReturnMoneyCell.self, forCellReuseIdentifier: "ReturnMoneyCell")
            obj.register(DataCockpitSeilMoneyCell.self, forCellReuseIdentifier: "DataCockpitSeilMoneyCell")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DataCockpitViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 0 || indexPath.row == 1{
//            return 337
//        }else if indexPath.row == 2 || indexPath.row == 3{
//            return 337
//        }
//        return 178
        return 337
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 8
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitYearCell", for: indexPath) as! DataCockpitYearCell
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitDepartMentCell", for: indexPath) as! DataCockpitDepartMentCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitDepartMentOrderCell", for: indexPath) as! DataCockpitDepartMentOrderCell
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitDepartMentReturnMoneyCell", for: indexPath) as! DataCockpitDepartMentReturnMoneyCell
            return cell
        }else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitOrderCell", for: indexPath) as! DataCockpitOrderCell
            return cell
        }else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitOrderReturnMoneyCell", for: indexPath) as! DataCockpitOrderReturnMoneyCell
            return cell
        }else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ReturnMoneyCell", for: indexPath) as! ReturnMoneyCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DataCockpitSeilMoneyCell", for: indexPath) as! DataCockpitSeilMoneyCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        for i in 0..<(dataList?.count ?? 0){
//            let model = dataList?[i] as! ShowMarketModel
//            model.isSelected = false
//        }
//        let model = dataList?[indexPath.row] as! ShowMarketModel
//        model.isSelected = true
//        tableView.reloadData()
//        self.isHidden = true
    }
}
