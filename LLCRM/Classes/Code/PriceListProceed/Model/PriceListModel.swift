//
//  PriceListModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class PriceListModel: BaseModel {
    var dataList:[PriceDataModel]?
    var pages:Int?
    var total:Int?
    
    required init() {}
}


class PriceDataModel: BaseModel{
    var bidDemandNumber:String?
    var complete:Int?
    var createName:String?
    var createTime:String?
    var customerName:String?
    var id:String?
    var joinQuotationNumber:String?
}
