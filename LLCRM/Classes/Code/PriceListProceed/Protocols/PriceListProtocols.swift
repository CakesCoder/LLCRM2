//
//  PriceListProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/1.
//

import UIKit

class PriceListProtocols: NSObject {

}


protocol PriceListViewProtocols: AnyObject {
    var presenter: PriceListPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetPriceListPresenterReceiveData(by params: Any?)
    func didGetPriceListPresenterReceiveError(error: MyError?)
}

protocol PriceListPresenterProtocols: AnyObject{
    var view: PriceListViewProtocols? { get set }
    var router: PriceListRouterProtocols? { get set }
    var interactor: PriceListInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetPriceList(by params: Any?)
    func didGetPriceListInteractorReceiveData(by params: Any?)
    func didGetPriceListInteractorReceiveError(error: MyError?)
}

protocol PriceListInteractorProtocols: AnyObject {
    var presenter: PriceListPresenterProtocols? { get set }
    var entity: PriceListEntityProtocols? { get set }
    
    func presenterRequestPriceList(by params: Any?)
    func didEntityCreatCustomerServiceReceiveData(by params: Any?)
    func didEntityCreatCustomerServiceReceiveError(error: MyError?)
}

protocol PriceListEntityProtocols: AnyObject {
    var interactor: PriceListInteractorProtocols? { get set }
    
    func didPriceListReceiveData(by params: Any?)
    func didPriceListReceiveError(error: MyError?)
    func getPriceListRequest(by params: Any?)
}

protocol PriceListRouterProtocols: AnyObject {
//    func pushToAddCustomerService(from previousView: UIViewController)
//    func pushToCreatCustomerService(from previousView: UIViewController)
//    func pushToCustomerServiceDetail(from previousView: UIViewController, forModel model: Any)
//    func pushToMyClient(from previousView: UIViewController)
}



