//
//  PriceListInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/1.
//

import UIKit

class PriceListInteractor: PriceListInteractorProtocols {
    var presenter: PriceListPresenterProtocols?
    
    var entity: PriceListEntityProtocols?
    
    func presenterRequestPriceList(by params: Any?) {
        entity?.getPriceListRequest(by: params)
    }
    
    func didEntityCreatCustomerServiceReceiveData(by params: Any?) {
        presenter?.didGetPriceListInteractorReceiveData(by: params)
    }
    
    func didEntityCreatCustomerServiceReceiveError(error: MyError?) {
        presenter?.didGetPriceListInteractorReceiveError(error: error)
    }
    

}
