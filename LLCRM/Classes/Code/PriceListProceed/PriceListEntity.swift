//
//  PriceListEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/1.
//

import UIKit

class PriceListEntity: PriceListEntityProtocols {
    var interactor: PriceListInteractorProtocols?
    
    func didPriceListReceiveData(by params: Any?) {
        interactor?.didEntityCreatCustomerServiceReceiveData(by: params)
    }
    
    func didPriceListReceiveError(error: MyError?) {
        interactor?.didEntityCreatCustomerServiceReceiveError(error: error)
    }
    
    func getPriceListRequest(by params: Any?) {
        LLNetProvider.request(.priceList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didPriceListReceiveData(by:jsonData)
            case .failure(_):
                self.didPriceListReceiveError(error: .requestError)
            }
        }
    }
    

}
