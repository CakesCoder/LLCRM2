//
//  PriceListProceedTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class PriceListProceedTableViewCell: UITableViewCell {
        
    var model:PriceDataModel?{
        didSet{
            let codehintAttribute = NSMutableAttributedString(string:"报价单号：")
            codehintAttribute.yy_color = blackTextColor60
            codehintAttribute.yy_font = kMediumFont(12)
            
            let codeAttribute = NSMutableAttributedString(string: model?.bidDemandNumber ?? "暂无")
            codeAttribute.yy_color = bluebgColor
            codeAttribute.yy_font = kMediumFont(12)
            
            codehintAttribute.append(codeAttribute)
            
            codeLabel?.attributedText = codehintAttribute
            
            
            let hintAttribute = NSMutableAttributedString(string:"客户名称：")
            hintAttribute.yy_color = blackTextColor60
            hintAttribute.yy_font = kMediumFont(12)
            
            let personAttribute = NSMutableAttributedString(string: "暂无")
            personAttribute.yy_color = blackTextColor14
            personAttribute.yy_font = kMediumFont(12)
            
            hintAttribute.append(personAttribute)
            clientLabel?.attributedText = hintAttribute
            
            if model?.complete == 0{
                tagLabel?.text = "未启动"
            }else if model?.complete == 1{
                tagLabel?.text = "进行中"
            }else{
                tagLabel?.text = "已完成"
            }
            
            timeLabel?.text = model?.createTime ?? "暂无"
            nameLabel2?.text = model?.createName ?? "暂无"
            
            moneyLabel?.text = "暂无"
            numLabel?.text = "暂无"
        }
    }
    var nameLabel:UILabel?
    var codeLabel:UILabel?
    var clientLabel:UILabel?
    var timeLabel:UILabel?
    var nameLabel2:UILabel?
    var moneyLabel:UILabel?
    var numLabel:UILabel?
    var tagLabel:UILabel?
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "business_money")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        }
        
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "电子配件报价单"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor78
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        
        codeLabel = UILabel().then { obj in
            contentView.addSubview(obj)

            
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        tagLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = blackTextColor44
//            obj.text = "报价中"
            obj.textColor = .white
            obj.font = kRegularFont(8)
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.left.equalTo(codeLabel!.snp.right).offset(5)
                make.centerY.equalTo(codeLabel!)
                make.width.equalTo(26)
                make.height.equalTo(15)
            }
        }
        
        clientLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(codeLabel!.snp.bottom).offset(5)
            }
        }
        
        timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor70
            obj.font = kMediumFont(12)
//            obj.text = "2023.04.27  09:30"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
//                make.bottom.equalToSuperview().offset(-15)
                make.top.equalTo(clientLabel!.snp.bottom).offset(5)
            }
        }
        
        nameLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.font = kMediumFont(12)
//            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(codeLabel!)
            }
        }
        
        moneyLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "￥5,000.00"
            obj.textColor = blackTextColor36
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(clientLabel!)
            }
        }
        
        numLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.font = kMediumFont(12)
//            obj.text = "3次"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(timeLabel!)
            }
        }
        
//        let moneyLabel = UILabel().then { obj in
//            contentView.addSubview(obj)
//            obj.textColor = blackTextColor36
//            obj.text = "￥5000.00"
//            obj.font = kRegularFont(12)
//            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalTo(timeLabel)
//            }
//        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        }
    }


}
