//
//  PriceListPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/12/1.
//

import UIKit

class PriceListPresenter: PriceListPresenterProtocols {
    var view: PriceListViewProtocols?
    
    var router: PriceListRouterProtocols?
    
    var interactor: PriceListInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetPriceList(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestPriceList(by: params)
    }
    
    func didGetPriceListInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetPriceListPresenterReceiveData(by: params)
    }
    
    func didGetPriceListInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetPriceListPresenterReceiveError(error: error)
    }

}
