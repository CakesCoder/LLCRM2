//
//  MarketDataStatusViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/15.
//

import UIKit

class MarketDataStatusViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = "销售漏斗（商机金额）"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MarketDataViewController.backClick))
        // Do any additional setup after loading the view.
        
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(TMarketDataStatusNameableViewCell.self, forCellReuseIdentifier: "TMarketDataStatusNameableViewCell")
            obj.register(MarketDataStatusTimeTableViewCell.self, forCellReuseIdentifier: "MarketDataStatusTimeTableViewCell")
            obj.register(MarketDataStatusFlowTableViewCell.self, forCellReuseIdentifier: "MarketDataStatusFlowTableViewCell")
            obj.register(MarketDataStatusPhaseTableViewCell.self, forCellReuseIdentifier: "MarketDataStatusPhaseTableViewCell")
            obj.register(MarketDataStatusTableViewCell.self, forCellReuseIdentifier: "MarketDataStatusTableViewCell")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
    }
    
    @objc func backClick(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
extension MarketDataStatusViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 140
        }else if indexPath.row == 1{
            return 288
        }else if indexPath.row == 2{
            return 93
        }else if indexPath.row == 3{
            return 173
        }
        return 178
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "TMarketDataStatusNameableViewCell", for: indexPath) as! TMarketDataStatusNameableViewCell
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarketDataStatusTimeTableViewCell", for: indexPath) as! MarketDataStatusTimeTableViewCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarketDataStatusFlowTableViewCell", for: indexPath) as! MarketDataStatusFlowTableViewCell
            return cell
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "MarketDataStatusPhaseTableViewCell", for: indexPath) as! MarketDataStatusPhaseTableViewCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MarketDataStatusTableViewCell", for: indexPath) as! MarketDataStatusTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        for i in 0..<(dataList?.count ?? 0){
//            let model = dataList?[i] as! ShowMarketModel
//            model.isSelected = false
//        }
//        let model = dataList?[indexPath.row] as! ShowMarketModel
//        model.isSelected = true
//        tableView.reloadData()
//        self.isHidden = true
    }
}
