//
//  TMarketDataStatusNameableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/16.
//

import UIKit

class TMarketDataStatusNameableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let hintLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "负责人主属部门（属于）"
            obj.textColor = blackTextColor38
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(10)
                make.left.equalToSuperview().offset(15)
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(hintLabel)
                make.width.height.equalTo(8)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "王小虎"
            obj.textColor = bluebgColor
            obj.font = kMediumFont(10)
            obj.snp.makeConstraints { make in
                make.right.equalTo(tagImageV.snp.left).offset(-10)
                make.centerY.equalTo(hintLabel)
            }
        }
        
        
        let w = (kScreenWidth-40)/3
        let x = w+10
        for i in 0..<1{
            let nameView = UIView().then { obj in
                contentView.addSubview(obj)
                obj.backgroundColor = bgColor20
                obj.layer.cornerRadius = 2
                obj.layer.borderColor = bgColor20.cgColor
                obj.layer.borderWidth = 0.5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10+Int(x)*i)
                    make.top.equalToSuperview().offset(43)
                    make.width.equalTo(w)
                    make.height.equalTo(30)
                }
            }
            
            let namelabel = UILabel().then { obj in
                nameView.addSubview(obj)
                obj.textColor = bluebgColor
                obj.font = kMediumFont(10)
                obj.text = "王小虎"
                obj.snp.makeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.centerX.equalToSuperview().offset(-10)
                }
            }
            
            _ = UIButton().then({ obj in
                nameView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.top.left.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            })
            
            _ = UIButton().then({ obj in
                nameView.addSubview(obj)
                obj.backgroundColor = .blue
                obj.snp.makeConstraints { make in
                    make.left.equalTo(namelabel.snp.right).offset(5)
                    make.centerY.equalTo(namelabel)
                    make.width.height.equalTo(11)
                }
            })
            
        }
        
        let selectedButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setTitle("请选择 >", for: .normal)
            obj.titleLabel?.font = kRegularFont(10)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(80)
                make.width.equalTo(50)
                make.height.equalTo(15)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.top.equalTo(selectedButton.snp.bottom).offset(8)
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor70
            obj.font = kMediumFont(12)
            obj.text = "不包含离职员工"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(selectedButton.snp.bottom).offset(23)
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
