//
//  MarketDataStatusTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/16.
//

import UIKit

class MarketDataStatusTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let hintLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "生命状态"
            obj.textColor = blackTextColor38
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(10)
                make.left.equalToSuperview().offset(15)
            }
        }
        
        let subLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textAlignment = .center
            obj.backgroundColor = bgColor21
            obj.textColor = blackTextColor14
            obj.text = "属于"
            obj.font = kMediumFont(9)
            obj.snp.makeConstraints { make in
                make.left.equalTo(hintLabel.snp.right).offset(5)
                make.centerY.equalTo(hintLabel)
                make.width.equalTo(44)
                make.height.equalTo(18)
            }
        }
        
//        let tagImageV = UIImageView().then { obj in
//            contentView.addSubview(obj)
//            obj.backgroundColor = .red
//            obj.snp.makeConstraints { make in
//                c
//                make.centerY.equalTo(hintLabel)
//                make.width.height.equalTo(8)
//            }
//        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "正常、变更中"
            obj.textColor = bluebgColor
            obj.font = kMediumFont(10)
            obj.snp.makeConstraints { make in
//                make.right.equalTo(tagImageV.snp.left).offset(-10)
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(hintLabel)
            }
        }
        
        
        let w = (kScreenWidth-40)/3
        let l = w+10
        
        let names = ["未生效", "审核中", "正常", "变更中", "作废"]
        
        for i in 0..<names.count{
            let x = i%3
            let y = i/3
            let nameView = UIView().then { obj in
                contentView.addSubview(obj)
                if i == 0{
                    obj.backgroundColor = bgColor20
                    obj.layer.borderColor = bgColor20.cgColor
                }else{
                    obj.backgroundColor = bgColor21
                    obj.layer.borderColor = bgColor21.cgColor
                }
                
                obj.layer.cornerRadius = 2
                
                obj.layer.borderWidth = 0.5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10+Int(l)*x)
                    make.top.equalToSuperview().offset(43+y*40)
                    make.width.equalTo(w)
                    make.height.equalTo(30)
                }
            }
            
            let namelabel = UILabel().then { obj in
                nameView.addSubview(obj)
                if i == 0{
                    obj.textColor = bluebgColor
                }else{
                    obj.textColor = blackTextColor14
                }
                obj.font = kMediumFont(10)
                obj.text = names[i]
                obj.snp.makeConstraints { make in
                    make.center.equalToSuperview()
                }
            }
            
            _ = UIButton().then({ obj in
                nameView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.top.left.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            })
            
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
    
    

}
