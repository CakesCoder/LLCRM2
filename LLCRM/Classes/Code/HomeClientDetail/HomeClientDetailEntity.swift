//
//  HomeClientDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class HomeClientDetailEntity: HomeClientDetailEntityProtocols {
    var interactor: HomeClientDetailInteractorProtocols?
    
    func didClientDetailReceiveData(by params: Any?) {
        interactor?.didEntityClientDetailReceiveData(by: params)
    }
    
    func didClientDetailReceiveError(error: MyError?) {
        interactor?.didEntityClientDetailReceiveError(error: error)
    }
    
    func getClientDetailRequest(by params: Any?) {
        LLNetProvider.request(.clientDetail(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
//                let jsonData = try? JSON(data: data)
//                printTest(jsonData)
//                if let json:[String:Any] = jsonData?.rawValue {
//                    printTest(json)
//                }
//                printTest(jsonData)
                self.didClientDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didClientDetailReceiveError(error: .requestError)
            }
        }
    }
    
    func didReceiveData() {
        
    }
}
