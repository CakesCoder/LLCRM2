//
//  HomeClientDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh

class HomeClientDetailViewController: BaseViewController {
    
    var presenter: HomeClientDetailPresenterProtocols?
    var tableView: UITableView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "客户详情"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(HomeClientDetailViewController.blackClick))
        
        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(HomeClientDetailTableViewCell.self, forCellReuseIdentifier: "HomeClientDetailTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(headView.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
            presenter?.presenterRequestGetClientDetail(by: ["current": presenter?.current, "size": 10])
            tableView?.stopFooterRefreshing()
        }))
        
        cofing()
    }
    
    func cofing(){
        let router = HomeClientDetailRouter()
        
        presenter = HomeClientDetailPresenter()
        
        presenter?.router = router
        
        let entity = HomeClientDetailEntity()
        
        let interactor = HomeClientDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
        presenter?.current = 1
        presenter?.presenterRequestGetClientDetail(by: ["current": presenter?.current, "size": 10])
        
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension HomeClientDetailViewController: HomeClientDetailViewProtocols {
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetHomeClientDetailPresenterReceiveData(by params: Any?) {
        if presenter?.current == 1{
            presenter?.params?.removeAll()
        }
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataDic = dict["data"] as? [String:Any]{
                    if dataDic.count > 0{
                        if let datas = JSONDeserializer<HomeClientDetailModel>.deserializeFrom(json:toJSONString(dict:dataDic)) {
                            presenter?.params?.append(contentsOf: datas.dataList ?? [])
                            tableView?.reloadData()
                            presenter?.current! += 1
                        }
                    }
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetHomeClientDetailPresenterReceiveError(error: MyError?) {
        
    }
    
    
}

extension HomeClientDetailViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.params?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeClientDetailTableViewCell", for: indexPath) as! HomeClientDetailTableViewCell
        cell.model = presenter?.params?[indexPath.row] as? ClientDetailDataModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        self.presenter?.router?.pushToAddClient(from: self)
        self.presenter?.router?.pushToClientPersonDetail(from: self, forModel: presenter?.params?[indexPath.row])
    }
}
