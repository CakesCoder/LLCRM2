//
//  HomeClientDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

protocol HomeClientDetailViewProtocols: AnyObject {
    var presenter: HomeClientDetailPresenterProtocols? { get set }
    
    func showLoading()
    func showError()
    func hideLoading()
    func didGetHomeClientDetailPresenterReceiveData(by params: Any?)
    func didGetHomeClientDetailPresenterReceiveError(error: MyError?)
}

protocol HomeClientDetailPresenterProtocols: AnyObject{
    var view: HomeClientDetailViewProtocols? { get set }
    var router: HomeClientDetailRouterProtocols? { get set }
    var interactor: HomeClientDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    var current: NSInteger? {get set}
    func viewDidLoad()
    
    func presenterRequestGetClientDetail(by params: Any?)
    func didGetClientDetailInteractorReceiveData(by params: Any?)
    func didGetClientDetailInteractorReceiveError(error: MyError?)
}

protocol HomeClientDetailInteractorProtocols: AnyObject {
    var presenter: HomeClientDetailPresenterProtocols? { get set }
    var entity: HomeClientDetailEntityProtocols? { get set }
    
    func presenterRequestClientDetail(by params: Any?)
    func didEntityClientDetailReceiveData(by params: Any?)
    func didEntityClientDetailReceiveError(error: MyError?)
}

protocol HomeClientDetailEntityProtocols: AnyObject {
    var interactor: HomeClientDetailInteractorProtocols? { get set }
    
    func getClientDetailRequest(by params: Any?)
    func didClientDetailReceiveData(by params: Any?)
    func didClientDetailReceiveError(error: MyError?)
}

protocol HomeClientDetailRouterProtocols: AnyObject {
    func pushToAddClient(from previousView: UIViewController)
    func pushToClientPersonDetail(from previousView: UIViewController, forModel model: Any)
}

