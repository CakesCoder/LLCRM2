//
//  HomeClientDetailModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/26.
//

import UIKit
import HandyJSON


class HomeClientDetailModel: BaseModel{
    
    var dataList:[ClientDetailDataModel]?
    var pages: Int?
    var total: Int?
    required init() {}
}

class ClientDetailDataModel: BaseModel {
    var address: String?
    var area: String?
    var assignDate: String?
    var assignStatus: Int?
    var assignerName: String?
    var city: String?
    var claimDate: String?
    var claimStatus: Int?
    var claimantName: String?
    var clientClueContactSimpleParamList: String?
    var clientId: Int?
    var clientLevel: String?
    var clientName: String?
    var clueNumber: String?
    var country: String?
    var createDept: String?
    var createrId: Int?
    var createrName: String?
    var creditCode: String?
    var date: String?
    var highSeas: String?
    var id: String?
    var industry: String?
    var intro: String?
    var parentCompany: String?
    var parentIndustry: String?
    var phone: Int?
    var productLine: Int?
    var receiverName: String?
    var requirement: Int?
    var scale: String?
    var source: String?
    var transferDate: String?
    var transferName: String?
    var transferStatus: Int?
    var website: Int?
    var companyProperty:String?
    
    required init() {}
}
