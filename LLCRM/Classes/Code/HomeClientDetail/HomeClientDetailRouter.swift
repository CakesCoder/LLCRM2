//
//  HomeClientDetailRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class HomeClientDetailRouter: HomeClientDetailRouterProtocols {
    func pushToAddClient(from previousView: UIViewController) {
        let nextView = AddClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToClientPersonDetail(from previousView: UIViewController, forModel model: Any) {
        let nextView = ClientPersonDetailViewController()
        nextView.model = model as? ClientDetailDataModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
