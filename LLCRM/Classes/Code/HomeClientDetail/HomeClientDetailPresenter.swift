//
//  HomeClientDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class HomeClientDetailPresenter: HomeClientDetailPresenterProtocols {
    
    var current: NSInteger?
    
    var view: HomeClientDetailViewProtocols?
    
    var router: HomeClientDetailRouterProtocols?
    
    var interactor: HomeClientDetailInteractorProtocols?
    
    var params: [Any]? = []
    
    func viewDidLoad() {
        current = 1
    }
    
    func presenterRequestGetClientDetail(by params: Any?){
        view?.showLoading()
        interactor?.presenterRequestClientDetail(by: params)
    }
    
    func didGetClientDetailInteractorReceiveData(by params: Any?){
        view?.hideLoading()
        view?.didGetHomeClientDetailPresenterReceiveData(by: params)
    }
    
    func didGetClientDetailInteractorReceiveError(error: MyError?){
        view?.hideLoading()
        view?.showError()
        view?.didGetHomeClientDetailPresenterReceiveError(error: error)
    }
    

}
