//
//  HomeClientDetailTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class HomeClientDetailTableViewCell: UITableViewCell {
    
    var titleLabel:UILabel?
    var statusLabel: UILabel?
    var contentLabel: UILabel?
    var contentLabel1: UILabel?
    var contentLabel2: UILabel?
    var contentLabel3: UILabel?
        
    var model:ClientDetailDataModel?{
        didSet{
            if model?.transferStatus == 1{
                statusLabel?.text = "已转交"
            }else{
                statusLabel?.text = "未成交"
            }
            
            titleLabel?.text = model?.clientName
            
            let hintAttri = NSMutableAttributedString(string:"负责人：")
            hintAttri.yy_color = blackTextColor68
            hintAttri.yy_font = kMediumFont(12)
            
            let contentAttri = NSMutableAttributedString(string: "王小虎")
            contentAttri.yy_color = blackTextColor14
            contentAttri.yy_font = kMediumFont(12)
            hintAttri.append(contentAttri)
            
            contentLabel?.attributedText = hintAttri
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "client_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        })
        
        titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.textColor = blackTextColor78
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        statusLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor12
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor26.cgColor
            
            obj.textColor = blackTextColor36
            obj.font = kRegularFont(12)
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagImageV)
                make.width.equalTo(52)
                make.height.equalTo(22)
            }
        }
        
        contentLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(titleLabel!.snp.bottom).offset(15)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        contentLabel1 = UILabel().then { obj in
            contentView.addSubview(obj)
            let attri = NSMutableAttributedString(string:"负客户级别：")
            attri.yy_color = blackTextColor68
            attri.yy_font = kMediumFont(12)
            
            let attri2 = NSMutableAttributedString(string: "小型客户")
            attri2.yy_color = blackTextColor14
            attri2.yy_font = kMediumFont(12)
            attri.append(attri2)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(contentLabel!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        contentLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            let attri = NSMutableAttributedString(string:"电话：")
            attri.yy_color = blackTextColor68
            attri.yy_font = kMediumFont(12)
            
            let attri2 = NSMutableAttributedString(string: "13930390291")
            attri2.yy_color = blackTextColor14
            attri2.yy_font = kMediumFont(12)
            attri.append(attri2)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(contentLabel1!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        contentLabel3 = UILabel().then { obj in
            contentView.addSubview(obj)
            let attri = NSMutableAttributedString(string:"最后跟进时间：")
            attri.yy_color = blackTextColor68
            attri.yy_font = kMediumFont(12)
            
            let attri2 = NSMutableAttributedString(string: "2022.05.12 15:30")
            attri2.yy_color = blackTextColor14
            attri2.yy_font = kMediumFont(12)
            attri.append(attri2)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(contentLabel2!.snp.bottom).offset(5)
                make.right.equalToSuperview().offset(-35)
                make.height.equalTo(13)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
    }
}
