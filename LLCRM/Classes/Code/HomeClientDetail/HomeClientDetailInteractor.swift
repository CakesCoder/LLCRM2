//
//  HomeClientDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/24.
//

import UIKit

class HomeClientDetailInteractor: HomeClientDetailInteractorProtocols {
    var entity: HomeClientDetailEntityProtocols?
    var presenter: HomeClientDetailPresenterProtocols?
    
    func presenterRequestClientDetail(by params: Any?) {
        entity?.getClientDetailRequest(by: params)
    }
    
    func didEntityClientDetailReceiveData(by params: Any?) {
        presenter?.didGetClientDetailInteractorReceiveData(by: params)
    }
    
    func didEntityClientDetailReceiveError(error: MyError?) {
        presenter?.didGetClientDetailInteractorReceiveError(error: error)
    }
}
