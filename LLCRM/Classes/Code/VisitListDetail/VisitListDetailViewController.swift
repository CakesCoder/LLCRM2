//
//  VisitListDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/24.
//

import UIKit

class VisitListDetailViewController: BaseViewController {
    var model:VisiListDataModel?{
        didSet{
            clientTextFiled?.text = model?.visitPeopleName ?? ""
            clientTypeTextFiled?.text = model?.visitCategory ?? ""
            clientThemeTextFiled?.text = model?.visitTopic ?? ""
            clientGoalTextFiled?.text = model?.visitPurpose ?? ""
            personTextFiled?.text = model?.createrName ?? ""
            clientPersonTextFiled?.text = model?.clientPeoples ?? ""
            addPersonTextFiled?.text = model?.companyPeoples ?? ""
        }
    }
    var clientTextFiled:UITextField?
    var clientTypeTextFiled:UITextField?
    var clientThemeTextFiled:UITextField?
    var clientGoalTextFiled:UITextField?
    var personTextFiled:UITextField?
    var clientPersonTextFiled:UITextField?
    var addPersonTextFiled:UITextField?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "客户拜访详情"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddVisitViewController.backClick))
        
        
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        let tagImageV = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(30)
                make.width.height.equalTo(16)
            }
        }
        
        
        let visitHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "拜访对象"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor90
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(5)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let clientHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "客户"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor90
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let clientView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor5.cgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(68)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
        
//        let clientImageV = UIImageView().then { obj in
//            clientView.addSubview(obj)
//            obj.backgroundColor = .blue
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(12)
//                make.centerY.equalToSuperview()
//                make.width.height.equalTo(13)
//            }
//        }
        
        clientTextFiled = UITextField().then { obj in
            clientView.addSubview(obj)
//            obj.placeholder = "添加客户"
//            obj.text = "零瓴软件技术（深圳）有限公司"
            obj.isEnabled = false
            obj.isEnabled = false
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor14
            obj.snp.makeConstraints { make in
//                make.left.equalTo(clientImageV.snp.right).offset(5)
                make.left.equalToSuperview().offset(12)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        let clientPushImageV = UIImageView().then { obj in
            clientView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then({ obj in
            clientView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let clientTypeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let clientTypeHintLabel = UILabel().then { obj in
            clientTypeView.addSubview(obj)
            let clientTypeHintAttribute = NSMutableAttributedString(string: "*")
            clientTypeHintAttribute.yy_color = blackTextColor27
            clientTypeHintAttribute.yy_font = kMediumFont(13)
            let clientTypeCotentAttribute = NSMutableAttributedString(string: "拜访类型")
            clientTypeCotentAttribute.yy_color = blackTextColor
            clientTypeCotentAttribute.yy_font = kMediumFont(14)
            clientTypeHintAttribute.append(clientTypeCotentAttribute)
            obj.attributedText = clientTypeHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        clientTypeTextFiled = UITextField().then { obj in
            clientTypeView.addSubview(obj)
//            obj.placeholder = "请选择"
            obj.font = kRegularFont(12)
//            obj.text = "实地考察"
            
            obj.isEnabled = false
            obj.textColor = blackTextColor14
            obj.snp.makeConstraints { make in
                make.left.equalTo(clientTypeHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        let clientTypePushImageV = UIImageView().then { obj in
            clientTypeView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            clientView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let clientThemeView = UIView().then { obj in
            headView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.top.equalTo(clientTypeView.snp.bottom).offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(80)
            }
        }
        
        let clientThemeHintLabel = UILabel().then { obj in
            clientThemeView.addSubview(obj)
            
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor3
//            obj.backgroundColor = .brown
            obj.text = "拜访主题"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(60)
                make.height.equalTo(20)
            }
        }
        
        let clientThemeInputView = UIView().then { obj in
            clientThemeView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.layer.borderColor = lineColor7.cgColor
            obj.layer.borderWidth = 0.5
            obj.backgroundColor = bgColor23
//            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(40)
//                make.top.equalTo(clientThemeHintLabel.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-0)
//                make.height.equalTo(40)
            }
        }
        
        clientThemeTextFiled = UITextField().then { obj in
            clientThemeInputView.addSubview(obj)
            obj.text = "考察实际公司情况"
            obj.font = kRegularFont(12)
            obj.isEnabled = false
            obj.textColor = blackTextColor14
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.height.equalTo(35)
//                make.bottom.equalToSuperview().offset(-5)
            }
        }
        
        let clientGoalView = UIView().then { obj in
            headView.addSubview(obj)
//            obj.backgroundColor = .green
            obj.snp.makeConstraints { make in
                make.top.equalTo(clientThemeView.snp.bottom).offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(80)
            }
        }
        
        let clientGoalHintLabel = UILabel().then { obj in
            clientGoalView.addSubview(obj)
            obj.text = "拜访目的"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(60)
                make.height.equalTo(20)
            }
        }
        
        let clientGoalInputView = UIView().then { obj in
            clientGoalView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.layer.borderColor = lineColor7.cgColor
            obj.layer.borderWidth = 0.5
            obj.backgroundColor = bgColor23
//            obj.backgroundColor = .reds
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(clientGoalHintLabel.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        clientGoalTextFiled = UITextField().then { obj in
            clientGoalInputView.addSubview(obj)
            obj.placeholder = "考察实际公司情况"
            obj.font = kRegularFont(12)
            obj.isEnabled = false
            obj.textColor = blackTextColor14
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let personView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientGoalView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let personHintLabel = UILabel().then { obj in
            personView.addSubview(obj)
            let personHintAttribute = NSMutableAttributedString(string: "*")
            personHintAttribute.yy_color = blackTextColor27
            personHintAttribute.yy_font = kMediumFont(13)
            let personCotentAttribute = NSMutableAttributedString(string: "负责人")
            personCotentAttribute.yy_color = blackTextColor
            personCotentAttribute.yy_font = kMediumFont(14)
            personHintAttribute.append(personCotentAttribute)
            obj.attributedText = personHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        personTextFiled = UITextField().then { obj in
            personView.addSubview(obj)
//            obj.placeholder = "请选择"
            obj.textColor = blackTextColor33
            obj.font = kRegularFont(14)
            obj.isEnabled = false
//            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.left.equalTo(personHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        let personPushImageV = UIImageView().then { obj in
            personView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            personView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let clientPersonView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(personView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let clientPersonHintLabel = UILabel().then { obj in
            clientPersonView.addSubview(obj)
            let clientPersonHintAttribute = NSMutableAttributedString(string: "")
            clientPersonHintAttribute.yy_color = blackTextColor27
            clientPersonHintAttribute.yy_font = kMediumFont(13)
            let clientPersonCotentAttribute = NSMutableAttributedString(string: "客户人员")
            clientPersonCotentAttribute.yy_color = blackTextColor
            clientPersonCotentAttribute.yy_font = kMediumFont(14)
            clientPersonHintAttribute.append(clientPersonCotentAttribute)
            obj.attributedText = clientPersonHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        clientPersonTextFiled = UITextField().then { obj in
            clientPersonView.addSubview(obj)
            obj.textColor = blackTextColor33
            obj.font = kRegularFont(14)
            obj.isEnabled = false
//            obj.text = "蔡徐坤"
            obj.snp.makeConstraints { make in
                make.left.equalTo(clientPersonHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        let clientPersonPushImageV = UIImageView().then { obj in
            clientPersonView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            personView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        let addPersonView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(clientPersonView.snp.bottom).offset(0)
                make.height.equalTo(50)
            }
        }
        
        let addPersonHintLabel = UILabel().then { obj in
            addPersonView.addSubview(obj)
            let addPersonHintAttribute = NSMutableAttributedString(string: "*")
            addPersonHintAttribute.yy_color = blackTextColor27
            addPersonHintAttribute.yy_font = kMediumFont(13)
            let addPersonCotentAttribute = NSMutableAttributedString(string: "参与人")
            addPersonCotentAttribute.yy_color = blackTextColor
            addPersonCotentAttribute.yy_font = kMediumFont(14)
            addPersonHintAttribute.append(addPersonCotentAttribute)
            obj.attributedText = addPersonHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(123)
                make.centerY.equalToSuperview()
            }
        }
        
        addPersonTextFiled = UITextField().then { obj in
            addPersonView.addSubview(obj)
            obj.textColor = blackTextColor33
            obj.font = kRegularFont(14)
            obj.isEnabled = false
            obj.text = "王小虎、夏淼淼、王大虎"
            obj.snp.makeConstraints { make in
                make.left.equalTo(addPersonHintLabel.snp.right).offset(40)
                make.centerY.equalToSuperview()
                make.height.equalTo(15)
                make.right.equalToSuperview().offset(-40)
            }
        }
        
        let addPersonPushImageV = UIImageView().then { obj in
            addPersonView.addSubview(obj)
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            addPersonView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
        
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
