//
//  DateRecordsViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class DateRecordsViewController: BaseViewController {
    /// 视图日历
    var calendarView: CalendarView?
    
    /// 年月显示
    var yearOMonthLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    /// 日历顶部显示年份月份
    var yearOMonth: Date = Date() {
        didSet {
            //年份月份展示label
            self.yearOMonthLabel.text = self.formatYearOMonth(yearOMonth)
            //月份日期展示collectionview
            self.calendarView?.date = yearOMonth
        }
    }
    
    
    /// 上一个月份按钮
    var lastButton: UIButton = UIButton()
    /// 下一个月份按钮
    var nextButton: UIButton = UIButton()
    /// 上一年按钮
    var nextYearButton: UIButton = UIButton()
    /// 下一年按钮
    var lastYearButton: UIButton = UIButton()
    
    var cellWidth: CGFloat = 0.0
    var cellHeight: CGFloat = 0.0
    /// 日历外边框
    let contentView: UIView = {
        let view = UIView()
        return view
    }()
    
    /// 日历外边框宽度
    var contentWidth: CGFloat = 0.0
    
    /// 获取上个月的同一日期
    @objc func getLastMonth() {
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(-1, for: .month)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    /// 获取下个月的同一日期
    @objc func getNextMonth() {
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(+1, for: .month)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    
    /// 获取下一年的同一日期
    @objc func getNextYear(){
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(+1, for: .year)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    
    /// 获取上一年的同一日期
    @objc func getLastYear(){
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(-1, for: .year)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    
    
    /// 将日期展示为年月
    func formatYearOMonth(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy年MM月"
        let string = formatter.string(from: date)
        return string
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "夏淼淼的考勤记录"
        // Do any additional setup after loading the view.
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 350)
        }
        
        headView.addSubview(self.contentView)
        contentView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.top.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(-0)
        }
        
        
        contentView.addSubview(self.yearOMonthLabel)
        yearOMonthLabel.backgroundColor = .red
        yearOMonthLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(25)
        }
        
        contentView.addSubview(lastYearButton)
        lastYearButton.backgroundColor = .blue
        lastYearButton.addTarget(self, action: #selector(getLastYear), for: .touchUpInside)
        lastYearButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(5)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        
        contentView.addSubview(self.lastButton)
        lastButton.backgroundColor = .red
        lastButton.setImage(UIImage(named: "month_left"), for: .normal)
        lastButton.addTarget(self, action: #selector(getLastMonth), for: .touchUpInside)
        lastButton.snp.makeConstraints { make in
            make.left.equalTo(lastYearButton.snp.right).offset(25)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        
        contentView.addSubview(nextYearButton)
        nextYearButton.backgroundColor = .yellow
        nextYearButton.addTarget(self, action: #selector(getNextYear), for: .touchUpInside)
        nextYearButton.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-5)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        contentView.addSubview(nextButton)
        nextButton.backgroundColor = .brown
        nextButton.addTarget(self, action: #selector(getNextMonth), for: .touchUpInside)
        nextButton.snp.makeConstraints { make in
            make.right.equalTo(nextYearButton.snp.left).offset(-25)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        
        let weekView = WeekView(frame: CGRect(x: 0, y: 80, width: kScreenWidth - 30, height: 40))
        contentView.addSubview(weekView)
        
        cellWidth = ((kScreenWidth - 30)/7)
        cellHeight = 40
        yearOMonthLabel.text = self.formatYearOMonth(self.yearOMonth)
        
        let calendarViewFrame = CGRect(x: 0, y: 120, width: kScreenWidth-30, height: 225)
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: self.cellWidth, height: self.cellHeight)
        self.calendarView = CalendarView(frame: calendarViewFrame, collectionViewLayout: layout)
        self.contentView.addSubview(self.calendarView ?? UICollectionView())
        self.calendarView?.getDatesBlock = { (label) in
            label.textColor = .white
            label.backgroundColor = workTagColor
        }
        self.calendarView?.date = yearOMonth
        
        let footView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 200)
            obj.backgroundColor = bgColor
        }
        
        let nodataView = UIView().then { obj in
            footView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(75)
                make.width.equalTo(150)
                make.height.equalTo(80)
                make.centerX.equalToSuperview()
            }
        }
        
        let nodataLabel = UILabel().then { obj in
            footView.addSubview(obj)
            obj.text = "暂无记录哦"
            obj.textColor = blackTextColor22
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.top.equalTo(nodataView.snp.bottom).offset(10)
                make.centerX.equalToSuperview()
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.delegate = self
            obj.dataSource =  self
            obj.separatorStyle = .none
            obj.backgroundColor = bgColor
            obj.tableFooterView = footView
//            obj.register(ApplyWorkDateTableViewCell.self, forCellReuseIdentifier: "ApplyWorkDateTableViewCell")
            obj.register(ClockInTableViewCell.self, forCellReuseIdentifier: "ClockInTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension DateRecordsViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if indexPath.row == 1{
            return 142
//        }
//        return 231
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ClockInTableViewCell", for: indexPath) as! ClockInTableViewCell
            return cell
//        }
//        let cell = tableView.dequeueReusableCell(withIdentifier: "NoClockInTableViewCell", for: indexPath) as! NoClockInTableViewCell
//        return cell
    }
}

