//
//  VacationManageViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit
import JFPopup

class VacationManageViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "假期管理"
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(VacationManageViewController.backClick))
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = lineColor
        }
        
        let dateView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        let dateLabel = UILabel().then { obj in
            dateView.addSubview(obj)
            let attri = NSMutableAttributedString(string:"2022年 ")
            attri.yy_color = blackTextColor2
            attri.yy_font = kMediumFont(16)
            
            let titleText = "2022年1月-2022年12月"
            let userAttri = NSMutableAttributedString(string: titleText)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kRegularFont(16)
            attri.append(userAttri)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let imageV = UIImageView().then { obj in
            dateView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
//                make.width.height.equalTo(15)
            }
        }
        
        let dateButton = UIButton().then { obj in
            dateView.addSubview(obj)
            obj.addTarget(self, action: #selector(dateClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let yearVacationView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(dateView.snp.bottom).offset(10)
                make.height.equalTo(138)
            }
        }
        
        let blueView = UIView().then { obj in
            yearVacationView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
                make.width.equalTo(4)
                make.height.equalTo(13)
            }
        }
        
        let yearHintLabel = UILabel().then { obj in
            yearVacationView.addSubview(obj)
            obj.text = "年休假"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor2
            obj.snp.makeConstraints { make in
                make.left.equalTo(blueView.snp.right).offset(5)
                make.centerY.equalTo(blueView)
            }
        }
        
        let yearLineView = UIView().then { obj in
            yearVacationView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(yearHintLabel.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        let yearLineView2 = UIView().then { obj in
            yearVacationView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.top.equalTo(yearLineView.snp.bottom).offset(0)
                make.centerX.equalToSuperview()
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo(1)
            }
        }
        
        let yearLeftLabel = UILabel().then { obj in
            yearVacationView.addSubview(obj)
            
            let attri = NSMutableAttributedString(string:"总  ")
            attri.yy_color = blackTextColor2
            attri.yy_font = kMediumFont(14)
            
            let attri2 = NSMutableAttributedString(string: "5  ")
            attri2.yy_color = bluebgColor
            attri2.yy_font = kMediumFont(18)
            attri.append(attri2)
            
            let attri3 = NSMutableAttributedString(string:"天")
            attri3.yy_color = blackTextColor2
            attri3.yy_font = kMediumFont(14)
            attri.append(attri3)
            
            obj.attributedText = attri
            obj.textAlignment = .center
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(yearLineView.snp.bottom).offset(0)
                make.right.equalTo(yearLineView2.snp.left).offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let yearRightLabel = UILabel().then { obj in
            yearVacationView.addSubview(obj)
            
            let attri = NSMutableAttributedString(string:"剩余   ")
            attri.yy_color = blackTextColor2
            attri.yy_font = kMediumFont(14)
            
            let attri2 = NSMutableAttributedString(string: "5 ")
            attri2.yy_color = bluebgColor
            attri2.yy_font = kMediumFont(18)
            attri.append(attri2)
            
            let attri3 = NSMutableAttributedString(string:"天")
            attri3.yy_color = blackTextColor2
            attri3.yy_font = kMediumFont(14)
            attri.append(attri3)
            
            obj.attributedText = attri
            obj.textAlignment = .center
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(yearLineView2.snp.right).offset(0)
                make.top.equalTo(yearLineView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let typeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalTo(yearVacationView.snp.bottom).offset(10)
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let w = kScreenWidth/3.0
        let types = [["假期类型", "已请时长", "申请次数"],["事假","0天","0"],["病假","0天","0"],["调休","0天","0"],["年薪","0天","0"],["婚假","0天","0"],["生育假","0天","0"],["培产假","0天","0"],["丧假","0天","0"],["其它","0天","0"]]
        for i in 0..<types.count{
            let typeTitleView = UIView().then { obj in
                typeView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(50)
                    make.top.equalToSuperview().offset(i*50)
                }
            }
            
            for j in 0..<types[i].count{
                let typeTitleLabel = UILabel().then { obj in
                    typeTitleView.addSubview(obj)
                    obj.font = kMediumFont(13)
                    if i == 0{
                        obj.textColor = blackTextColor72
                    }else{
                        obj.textColor = blackTextColor73
                    }
                    obj.frame = CGRect(x: CGFloat(j)*w, y: 0, width: w, height: 50)
                    obj.text = types[i][j]
                    obj.textAlignment = .center
                }
                
                if i == 0{
                    if j < 2{
                        let lineView = UIView().then { obj in
                            typeTitleView.addSubview(obj)
                            obj.backgroundColor = lineColor18
                            obj.snp.makeConstraints { make in
                                make.right.equalToSuperview().offset(-0)
                                make.top.equalToSuperview().offset(6)
                                make.bottom.equalToSuperview().offset(-6)
                                make.height.equalTo(0.5)
                            }
                        }
                    }
                }
            }
            
            _ = UIView().then({ obj in
                typeTitleView.addSubview(obj)
                obj.backgroundColor = lineColor20
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.height.equalTo(0.5)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            })
        }
        
        
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        // Do any additional setup after loading the view.
    }
    
    @objc func dateClick(_ button:UIButton){
        self.popup.bottomSheet {
            let v = YearShowDialog()
            v.cancelBlock = { [weak self] in
                self?.popup.dismissPopup()
            }
            v.okBlock = { [weak self] in
                self?.popup.dismissPopup()
            }
            return v
        }
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
