//
//  YearShowDialog.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class YearShowDialog: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var dates:[String]? = []
    var cancelBlock: (() -> ())?
    var okBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: CGRectMake(0, 0, kScreenWidth, kScreenHeight))
        
        let now = Date()
        let calendar:Calendar = Calendar.current
        let year = calendar.component(.year, from: now)
        
        for i in 1990..<year+1{
            self.dates?.append("\(i)")
        }
        
        
        let v = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(361)
            }
        }
        
        let cancelButton = UIButton().then { obj in
            v.addSubview(obj)
            obj.setTitle("取消", for: .normal)
            obj.titleLabel?.font = kMediumFont(16)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.addTarget(self, action: #selector(cancelClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(20)
                make.width.equalTo(40)
                make.height.equalTo(22)
            }
        }
        
        let okButton = UIButton().then { obj in
            v.addSubview(obj)
            obj.setTitle("确定", for: .normal)
            obj.titleLabel?.font = kMediumFont(16)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.addTarget(self, action: #selector(okClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                //                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(20)
                make.right.equalToSuperview().offset(-30)
                make.width.equalTo(40)
                make.height.equalTo(22)
            }
        }
        
        _ = UITableView().then({ obj in
            v.addSubview(obj)
            obj.separatorStyle = .none
            obj.register(TimeShowCell.self, forCellReuseIdentifier: "TimeShowCell")
//            obj.backgroundColor = .red
            obj.delegate = self
            obj.dataSource = self
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(56)
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func cancelClick(){
        self.okBlock?()
    }
    
    @objc func okClick(){
        self.cancelBlock?()
    }
}

extension YearShowDialog: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dates!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TimeShowCell", for: indexPath) as! TimeShowCell
        cell.dateLabel?.text = self.dates![indexPath.row]
        return cell
    }
}


class TimeShowCell :UITableViewCell{

    var dateStr:String?
    var dateLabel:UILabel?{
        didSet{
            dateLabel?.text = dateStr
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        dateLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(13)
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
    }
}
