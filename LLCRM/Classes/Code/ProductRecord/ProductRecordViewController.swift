//
//  ProductRecordViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit
//import JXPagingView

extension ProductRecordViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ProductRecordViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ProductRecordViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(ProductRecordTableViewCell.self, forCellReuseIdentifier: "ProductRecordTableViewCell")
            obj.register(ProductRecordNoDataTableViewCell.self, forCellReuseIdentifier: "ProductRecordNoDataTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ProductRecordViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 108
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return (presenter?.params!.count)!
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 9{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProductRecordNoDataTableViewCell", for: indexPath) as! ProductRecordNoDataTableViewCell
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProductRecordTableViewCell", for: indexPath) as! ProductRecordTableViewCell
        return cell
    }
}

