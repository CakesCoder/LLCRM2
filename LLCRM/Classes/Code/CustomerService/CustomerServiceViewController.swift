//
//  CustomerServiceViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh
//import JXSegmentedView
//import JXPagingView

class CustomerServiceViewController: BaseViewController {
    var tableView:UITableView?
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var presenter: CustomerServicePresenterProtocols?
    var currentIndex:Int = 1
    var customerServiceissDataList:[CustomerServiceRecordsModel]? = []
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    lazy var noDataView:UIView = {
        let noDataV = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: UIDevice.xp_navigationFullHeight()+104, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let noDataImageV = UIImageView().then { obj in
            noDataV.addSubview(obj)
            obj.image = UIImage(named: "customer_noDdata")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(120)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            noDataV.addSubview(obj)
            obj.text = "还未有客诉信息"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor22
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(270)
                make.centerX.equalToSuperview()
            }
        }
        
        let addButton = UIButton().then { obj in
            noDataV.addSubview(obj)
//            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.setTitle("新建客诉", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(workTagColor, for: .normal)
            obj.layer.borderColor = workTagColor.cgColor
            obj.layer.borderWidth = 0.5
            obj.layer.cornerRadius = 15
            obj.addTarget(self, action: #selector(addCustomerClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(320)
                make.centerX.equalToSuperview()
                make.width.equalTo(90)
                make.height.equalTo(30)
            }
        }
        
        return noDataV
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationItem.title = "客诉处理"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(CustomerServiceViewController.backClick))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.createAddBarbuttonItem(name: "+", target: self, action: #selector(CustomerServiceViewController.addClick))
        
        view.backgroundColor = .white
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        view.addSubview(self.segmentedView!)
        
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(headView.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["全部","处理中","已处理"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
//        segmentedView?.delegate = self
//        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(CustomerServiceTableViewCell.self, forCellReuseIdentifier: "CustomerServiceTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(segmentedView!.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
        let noDataV = noDataView.then { obj in
            view.addSubview(obj)
            obj.isHidden = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(segmentedView!.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
            
            presenter?.presenterRequestGetCustomerService(by: ["current":currentIndex,"size":20])
            
            tableView?.stopFooterRefreshing()
        }))
        
        cofing()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedVisitAddAction(_:)), name: Notification.Name(rawValue: "CustomerServiceNotificationKey"), object: nil)
    }
    
    @objc func didSelectedVisitAddAction(_ notification:NSNotification){
        currentIndex = 1
        presenter?.presenterRequestGetCustomerService(by: ["current":currentIndex,"size":20])
    }
    
    
    func cofing(){
        let router = CustomerServiceRouter()
        
        presenter = CustomerServicePresenter()
        
        presenter?.router = router
        
        let entity =  CustomerServiceEntity()
        
        let interactor =  CustomerServiceInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
        
        presenter?.presenterRequestGetCustomerService(by: ["current":currentIndex,"size":20])
    }
    
    @objc func addCustomerClick(_ button:UIButton){
        self.presenter?.router?.pushToCreatCustomerService(from: self)
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(){
        self.presenter?.router?.pushToCreatCustomerService(from: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}

extension CustomerServiceViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customerServiceissDataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerServiceTableViewCell", for: indexPath) as! CustomerServiceTableViewCell
        cell.model = customerServiceissDataList![indexPath.row] as? CustomerServiceRecordsModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.router?.pushToCustomerServiceDetail(from: self, forModel: customerServiceissDataList![indexPath.row] as? CustomerServiceRecordsModel)
    }
}

extension CustomerServiceViewController: JXSegmentedViewDelegate{
//    func pagingView(_ pagingView: JXPagingView.JXPagingView, initListAtIndex index: Int) -> JXPagingView.JXPagingViewListViewDelegate {
//        return nil
//    }
//
//    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
//        return 0
//    }r
//
//    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
//        return 0
//    }
//
//    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func numberOfLists(in pagingView: JXPagingView) -> Int {
//        return 4
//    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
    
    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}


extension CustomerServiceViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        presenter?.presenterRequestGetCustomerService(by: ["current":currentIndex,"size":20,"search": textField.text])
        return true
    }
}

extension CustomerServiceViewController: CustomerServiceViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetCustomerServicePresenterReceiveData(by params: Any?) {
        if currentIndex == 1{
            self.customerServiceissDataList = []
        }
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataModel = JSONDeserializer<CustomerServiceModel>.deserializeFrom(json:toJSONString(dict: dict["data"] as! [String : Any])) {
                    printTest(dataModel.records)
                    self.customerServiceissDataList?.append(contentsOf: dataModel.records!)
                }
                currentIndex += 1
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetCustomerServicePresenterReceiveError(error: MyError?) {
        
    }
}

