//
//  CustomerServiceEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit

class CustomerServiceEntity: CustomerServiceEntityProtocols {
    var interactor: CustomerServiceInteractorProtocols?
    
    func didCustomerServiceReceiveData(by params: Any?) {
        interactor?.didEntityCustomerServiceReceiveData(by: params)
    }
    
    func didCustomerServiceReceiveError(error: MyError?) {
        interactor?.didEntityCustomerServiceReceiveError(error: error)
    }
    
    func getCustomerServiceRequest(by params: Any?) {
        LLNetProvider.request(.clientComplaintProductList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didCustomerServiceReceiveData(by:jsonData)
            case .failure(_):
                self.didCustomerServiceReceiveError(error: .requestError)
            }
        }
    }
    

}
