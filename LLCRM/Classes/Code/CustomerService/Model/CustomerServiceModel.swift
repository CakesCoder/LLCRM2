//
//  CustomerServiceModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/30.
//

import UIKit

class CustomerServiceModel: BaseModel {
    var countId:String?
    var current:Int?
    var hitCount:String?
    var maxLimit:String?
    var optimizeCountSql:String?
    var orders:[Any]?
    var pages:Int?
    var records:[CustomerServiceRecordsModel]?
    var searchCount:Int?
    var size:Int?
    var total:Int?
    
    required init() {}
}

class CustomerServiceRecordsModel: BaseModel{
    
    var badDesc:String?
    var records:String?
    var badType:String?
    var baseUnit:String?
    var clientDiscardNo:String?
    var clientId:String?
    var clientName:String?
    var complaintId:String?
    var complaintNo:String?
    var createTime:String?
    var deleted:Int?
    var discardCount:String?
    var id:Int?
    var productCode:String?
    var productModel:String?
    var productName:String?
    var productTypeName:String?
    var source:Int?
    var specDesc:String?
    
    required init() {}
}
