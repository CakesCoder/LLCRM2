//
//  CustomerServiceRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit

class CustomerServiceRouter: CustomerServiceRouterProtocols {
    func pushToAddCustomerService(from previousView: UIViewController) {
        let nextView = AddCustomerServiceViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToCreatCustomerService(from previousView: UIViewController) {
        let nextView = CreatCustomerServiceViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToCustomerServiceDetail(from previousView: UIViewController, forModel model: Any) {
        let nextView = CustomerServiceDetailViewController()
        nextView.recordModel = model as? CustomerServiceRecordsModel
        previousView.navigationController?.pushViewController(nextView, animated: true)

    }
}
