//
//  CustomerServiceTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit

class CustomerServiceTableViewCell: UITableViewCell {
    
    var model:CustomerServiceRecordsModel?{
        didSet{
            nameLabel?.text = model?.clientName ?? "--"
            
            let hintcodeAttribute = NSMutableAttributedString(string:"客诉单号：")
            hintcodeAttribute.yy_color = blackTextColor60
            hintcodeAttribute.yy_font = kMediumFont(12)
            
            let codeAttribute = NSMutableAttributedString(string: model?.clientDiscardNo ?? "--")
            codeAttribute.yy_color = bluebgColor
            codeAttribute.yy_font = kMediumFont(12)
            hintcodeAttribute.append(codeAttribute)
            codeLabel?.attributedText = hintcodeAttribute
            
            
            let hintthemdAttribute = NSMutableAttributedString(string:"客诉主题：")
            hintthemdAttribute.yy_color = blackTextColor60
            hintthemdAttribute.yy_font = kMediumFont(12)
            
            let themeAttribute = NSMutableAttributedString(string: model?.specDesc ?? "--")
            themeAttribute.yy_color = blackTextColor14
            themeAttribute.yy_font = kMediumFont(12)
            
            hintthemdAttribute.append(themeAttribute)
            
            themeLabel?.attributedText = hintthemdAttribute
            
            timeLabel?.text = model?.createTime ?? "--"
            
            nameLabel2?.text = "--"
            
        }
    }
    
    var nameLabel:UILabel?
    var codeLabel:UILabel?
    var themeLabel:UILabel?
    var timeLabel:UILabel?
    var nameLabel2:UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "business_money")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        }
        
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor78
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        let tagLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.text = "赢单"
            obj.textColor = .white
            obj.font = kRegularFont(8)
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel!.snp.right).offset(5)
                make.centerY.equalTo(tagImageV)
                make.width.equalTo(26)
                make.height.equalTo(15)
            }
        }
        
        codeLabel = UILabel().then { obj in
            contentView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        themeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(codeLabel!.snp.bottom).offset(5)
            }
        }
        
        timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor70
            obj.font = kMediumFont(12)
//            obj.text = "2023.04.27  09:30"
            obj.snp.makeConstraints { make in
                make.right.bottom.equalToSuperview().offset(-15)
            }
        }
        
        nameLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.font = kMediumFont(12)
//            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(codeLabel!)
            }
        }
        
//        let moneyLabel = UILabel().then { obj in
//            contentView.addSubview(obj)
//            obj.textColor = blackTextColor36
//            obj.text = "￥5000.00"
//            obj.font = kRegularFont(12)
//            obj.snp.makeConstraints { make in
//                make.right.equalToSuperview().offset(-15)
//                make.centerY.equalTo(timeLabel)
//            }
//        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        }
    }

}
