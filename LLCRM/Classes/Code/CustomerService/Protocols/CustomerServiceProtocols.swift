//
//  CustomerServiceProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit

class CustomerServiceProtocols: NSObject {

}

protocol CustomerServiceViewProtocols: AnyObject {
    var presenter: CustomerServicePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetCustomerServicePresenterReceiveData(by params: Any?)
    func didGetCustomerServicePresenterReceiveError(error: MyError?)
}

protocol CustomerServicePresenterProtocols: AnyObject{
    var view: CustomerServiceViewProtocols? { get set }
    var router: CustomerServiceRouterProtocols? { get set }
    var interactor: CustomerServiceInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetCustomerService(by params: Any?)
    func didGetCustomerServiceInteractorReceiveData(by params: Any?)
    func didGetCustomerServiceInteractorReceiveError(error: MyError?)
}

protocol CustomerServiceInteractorProtocols: AnyObject {
    var presenter: CustomerServicePresenterProtocols? { get set }
    var entity: CustomerServiceEntityProtocols? { get set }
    
    func presenterRequestCustomerService(by params: Any?)
    func didEntityCustomerServiceReceiveData(by params: Any?)
    func didEntityCustomerServiceReceiveError(error: MyError?)
}

protocol CustomerServiceEntityProtocols: AnyObject {
    var interactor: CustomerServiceInteractorProtocols? { get set }
    
    func didCustomerServiceReceiveData(by params: Any?)
    func didCustomerServiceReceiveError(error: MyError?)
    func getCustomerServiceRequest(by params: Any?)
}

protocol CustomerServiceRouterProtocols: AnyObject {
    func pushToAddCustomerService(from previousView: UIViewController)
    func pushToCreatCustomerService(from previousView: UIViewController)
    func pushToCustomerServiceDetail(from previousView: UIViewController, forModel model: Any) 
}



