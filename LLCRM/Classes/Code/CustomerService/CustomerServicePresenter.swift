//
//  CustomerServicePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit

class CustomerServicePresenter: CustomerServicePresenterProtocols {
    var view: CustomerServiceViewProtocols?
    
    var router: CustomerServiceRouterProtocols?
    
    var interactor: CustomerServiceInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetCustomerService(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestCustomerService(by: params)
    }
    
    func didGetCustomerServiceInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetCustomerServicePresenterReceiveData(by: params)
    }
    
    func didGetCustomerServiceInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetCustomerServicePresenterReceiveError(error: error)
    }
    
    
}
