//
//  CustomerServiceInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/21.
//

import UIKit

class CustomerServiceInteractor: CustomerServiceInteractorProtocols {
    var presenter: CustomerServicePresenterProtocols?
    
    var entity: CustomerServiceEntityProtocols?
    
    func presenterRequestCustomerService(by params: Any?) {
        entity?.getCustomerServiceRequest(by: params)
    }
    
    func didEntityCustomerServiceReceiveData(by params: Any?) {
        presenter?.didGetCustomerServiceInteractorReceiveData(by: params)
    }
    
    func didEntityCustomerServiceReceiveError(error: MyError?) {
        presenter?.didGetCustomerServiceInteractorReceiveError(error: error)
    }
    

}
