//
//  MeInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class MeInteractor: MeInteractorProtocols {
    var presenter: MePresenterProtocols?
    
    var entity: MeEntityProtocols?
    
    func entityGetUserInfosReceiveData(by params: Any?){
        entity?.getUserInfosRequest(by: params)
    }
    
    func didEntityGetUserInfosReceiveData(by params: Any?){
        presenter?.didGetUserInfosInteractorReceiveData(by: params)
    }
    
    func didEntityUserInfosReceiveError(error: MyError?){
        presenter?.didGetUserInfosInteractorReceiveError(by: error)
    }
}
