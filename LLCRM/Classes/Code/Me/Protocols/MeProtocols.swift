//
//  MeRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

protocol MeViewProtocols: AnyObject {
    var presenter: MePresenterProtocols? { get set }
    func didMePresenterReceiveData(by params: Any?)
    func didMePresenterReceiveError(error: MyError?)
    
    func showLoading()
    func showError()
    func hideLoading()
}

protocol MePresenterProtocols: AnyObject{
    var view: MeViewProtocols? { get set }
    var router: MeRouterProtocols? { get set }
    var interactor: MeInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    func presenterRequestGetUserInfos(by params: Any?)
    func didGetUserInfosInteractorReceiveData(by params: Any?)
    func didGetUserInfosInteractorReceiveError(by params: Any?)
}

protocol MeInteractorProtocols: AnyObject {
    var presenter: MePresenterProtocols? { get set }
    var entity: MeEntityProtocols? { get set }
    
    func entityGetUserInfosReceiveData(by params: Any?)
    func didEntityGetUserInfosReceiveData(by params: Any?)
    func didEntityUserInfosReceiveError(error: MyError?)
}

protocol MeEntityProtocols: AnyObject {
    var interactor: MeInteractorProtocols? { get set }
    
    
    func getUserInfosRequest(by params: Any?)
    
    func didUserInfosReceiveData(by params: Any?)
    func didUserInfosReceiveError(error: MyError?)
}

protocol MeRouterProtocols: AnyObject {
    func pushToInfosVC(from previousView: UIViewController)
    func pushToCollect(from previousView: UIViewController)
    func pushToTopic(from previousView: UIViewController)
    func pushToAbout(from previousView: UIViewController)
    func pushToSetting(from previousView: UIViewController)
    func pushToBingDingPhone(from previousView: UIViewController)
    func pushToWorkStatus(from previousView: UIViewController)
    func pushToSchedule(from previousView: UIViewController)
    func pushToMyCode(from previousView: UIViewController)
}
