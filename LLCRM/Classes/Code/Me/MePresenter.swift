//
//  MePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit
import HandyJSON

class MePresenter: MePresenterProtocols {
    var view: MeViewProtocols?
    
    var router: MeRouterProtocols?
    
    var interactor: MeInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        if let tokenData = LLUserDefaultsManager.shared.readCacheLogin(){
            let userAccount = tokenData["user_account"] as! String
            presenterRequestGetUserInfos(by: ["userAccount":userAccount, "userType":"0"])
        }
    }
    
    func presenterRequestGetUserInfos(by params: Any?) {
        interactor?.entityGetUserInfosReceiveData(by: params)
    }

    func didGetUserInfosInteractorReceiveData(by params: Any?){
        if (params != nil){
            let dict = params as! [String:Any]
            if let dict = params as? [String:Any]{
                let dataDic = dict["data"] as? [String:Any]
                printTest(dataDic)
                if dict["code"] as! Int == 200{
                    view?.didMePresenterReceiveData(by: dataDic)
                    if let dataModel = JSONDeserializer<UserInfosModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                        printTest(dataModel)
                        LLUserDefaultsManager.shared.saveUserNameInfos(userName: dataModel.sysUserBo?.userName)
                        LLUserDefaultsManager.shared.saveUserDepartmentNameInfos(userName: dataModel.sysUserBo?.departmentName)
                        LLUserDefaultsManager.shared.saveUserCompanyNameInfos(userName: dataModel.sysUserBo?.companyName)
                        LLUserDefaultsManager.shared.saveUserEmailInfos(emil: dataModel.sysUserBo?.email)
                        LLUserDefaultsManager.shared.saveUserPhoneInfos(phone: dataModel.sysUserBo?.phoneNumber)
                    }
                }
            }
        }
    }
    func didGetUserInfosInteractorReceiveError(by params: Any?){
//        view?.didMePresenterReceiveError(error: params)
    }
}
