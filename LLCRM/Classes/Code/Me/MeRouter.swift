//
//  MeRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class MeRouter: MeRouterProtocols {
    
    func pushToMyCode(from previousView: UIViewController) {
        let vc = MyCodeViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToInfosVC(from previousView: UIViewController) {
        let vc = PersonInfosViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToCollect(from previousView: UIViewController){
        let vc = CollectViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func pushToTopic(from previousView: UIViewController){
        let vc = TopicViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToAbout(from previousView: UIViewController){
        let vc = AboutViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToSetting(from previousView: UIViewController){
        let vc = SettingViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToBingDingPhone(from previousView: UIViewController){
        let vc = TopicViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToWorkStatus(from previousView: UIViewController){
        let vc = WorkStatusViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToSchedule(from previousView: UIViewController){
        let vc = ScheduleViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    
    
}
