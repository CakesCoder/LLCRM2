//
//  MeViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit
import HandyJSON
import PKHUD

class MeViewController: BaseViewController {
    
    var presenter: MePresenterProtocols?
    var userName:String? = ""
    var userDepartmentName:String? = ""
    var userCompanyNameName:String? = ""
    
    var nameLabel:UILabel?
    var subNameLabel:UILabel?
    var companyLabel:UILabel?
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        view.backgroundColor = .gray
        
        if let userInfosName = LLUserDefaultsManager.shared.readUserNameInfos(){
            userName = userInfosName
        }
        
        if let userInfosDepartmentName = LLUserDefaultsManager.shared.readUserDepartmentNameInfos(){
            userDepartmentName = userInfosDepartmentName
        }
        
        if let userInfosCompanyNameName = LLUserDefaultsManager.shared.readUserCompanyNameInfos(){
            userCompanyNameName = userInfosCompanyNameName
        }
        
        self.navigationController?.navigationBar.isHidden = true
        
        buildUI()
        
        cofing()
        
    }
    
    func cofing(){
        let router = MeRouter()
        
        presenter = MePresenter()
        
        presenter?.router = router
        
        let entity = MeEntity()
        
        let interactor = MeInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
        presenter?.viewDidLoad()
    }
    
    func buildUI(){
        
        
//        let gradient:CAGradientLayer = CAGradientLayer()
//        gradient.colors = [UIColor(0x4871C0).cgColor,UIColor(0x89B2FF).cgColor]
//        gradient.locations = [(0.3),(1.0)]
//        gradient.startPoint = CGPoint.init(x: 0, y: 0)
//        gradient.endPoint = CGPoint.init(x: 0, y: 1)
//        gradient.frame = CGRect.init(x: 0, y: 0, width: kScreenWidth, height: 141+UIDevice.xp_navigationFullHeight())
        
        view.backgroundColor = lineColor
        
        let headView = UIView().then { obj in
            obj.backgroundColor = lineColor
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        let blueView = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = themeColor
            obj.isUserInteractionEnabled = true
            obj.contentMode = .scaleAspectFit
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.height.equalTo(141+UIDevice.xp_navigationFullHeight()+10)
            }
        }
        
        
        let titleLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "我的"
            obj.textColor = .white
            obj.font = kAMediumFont(16)
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_statusBarHeight() + 20)
                make.centerX.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setImage(UIImage(named: "me_setting"), for: .normal)
            obj.setImage(UIImage(named: "me_setting"), for: .selected)
            
            obj.addTarget(self, action: #selector(settingClick(_:)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(titleLabel)
            }
        }
        
        let headImageV = UIImageView().then { obj in
            headView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "me_head")
            obj.layer.cornerRadius = 31.5
            obj.layer.borderWidth = 1
            obj.layer.borderColor = UIColor.white.cgColor
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(titleLabel.snp.bottom).offset(25)
                make.width.height.equalTo(63)
            }
        }
        
        
        let infosButton = UIButton().then { obj in
            headView.addSubview(obj)
            obj.addTarget(self, action: #selector(infosClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(titleLabel.snp.bottom).offset(25)
                make.width.height.equalTo(63)
            }
        }
        
        
        nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.font = kAMediumFont(16)
            obj.textColor = .white
            obj.text = userName ?? "--"
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(headImageV.snp.right).offset(12)
                make.top.equalTo(titleLabel.snp.bottom).offset(32)
            }
        }
        
        subNameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.font = kAMediumFont(12)
            obj.textColor = .white
            obj.text = "/ "+(userDepartmentName ?? "")
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel!.snp.right).offset(0)
                make.top.equalTo(titleLabel.snp.bottom).offset(35)
            }
        }
        
        companyLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = userCompanyNameName ?? "--"
            obj.font = kAMediumFont(12)
            obj.textColor = .white
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(headImageV.snp.right).offset(12)
                make.top.equalTo(nameLabel!.snp.bottom).offset(5)
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.image = UIImage(named: "me_tag")
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(headImageV)
            }
        }
        
        let featureView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 4
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(141+UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(10)
                make.width.equalTo(kScreenWidth - 20)
                make.height.equalTo(87)
            }
        }
        
        let images = ["me_communication", "me_schedule", "me_dynamic ", "me_card"]
        let names = ["通讯录", "日程", "我的动态", "我的名片"]
        for i in 0..<images.count{
            let featureBgView = UIView().then { obj in
                featureView.addSubview(obj)
                obj.frame = CGRectMake(CGFloat(i) * (kScreenWidth - 20)/4, 0, (kScreenWidth - 20)/4, 87)
            }
            
            let imageV = UIImageView().then { obj in
                featureBgView.addSubview(obj)
                
                obj.image = UIImage(named: images[i])
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(15)
                    make.centerX.equalToSuperview()
                }
            }
            
            let nameLabel = UILabel().then { obj in
                featureBgView.addSubview(obj)
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor
                obj.text = names[i]
                obj.snp.makeConstraints { make in
                    make.top.equalTo(imageV.snp.bottom).offset(5)
                    make.centerX.equalToSuperview()
                }
            }
            
            let button = UIButton().then { obj in
                featureBgView.addSubview(obj)
                obj.tag = 1000+i
                obj.addTarget(self, action: #selector(featureClick(_:)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        let bgView = UIView().then { obj in
            headView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(featureView.snp.bottom).offset(10)
                make.height.equalTo(419)
            }
        }
        
        let bottomImages = ["me_clock","me_work","me_collect","me_chart","me_topic","me_drafts"]
        let bottomNames = ["工作状态", "我的工作", "我的收藏", "我的业绩", "我的话题", "草稿箱"]
        
        for i in 0..<6{
            let featureView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.frame = CGRectMake(0, CGFloat(i*60), kScreenWidth - 20, 60)
//                obj.backgroundColor = .brown
            }
            
            let imageV = UIImageView().then { obj in
                featureView.addSubview(obj)
                obj.image = UIImage(named: bottomImages[i])
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let label = UILabel().then { obj in
                featureView.addSubview(obj)
                obj.textColor = blackTextColor
                obj.font = kARegularFont(16)
                obj.text = bottomNames[i]
                
                obj.snp.makeConstraints { make in
                    make.left.equalTo(imageV.snp.right).offset(5)
                    make.centerY.equalToSuperview()
                }
            }
            
            let rightImageV = UIImageView().then { obj in
                featureView.addSubview(obj)
//                obj.backgroundColor = .red
                obj.image = UIImage(named: "me_push")
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-5)
                    make.centerY.equalToSuperview()
                    make.width.height.equalTo(26)
                }
            }
            
            _ = UIView().then({ obj in
                featureView.addSubview(obj)
                obj.backgroundColor = lineColor16
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            let button = UIButton().then { obj in
                featureView.addSubview(obj)
                obj.tag = 1004+i
                obj.addTarget(self, action: #selector(featureClick(_:)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.backgroundColor = lineColor
            if #available(iOS 11.0, *) {
                obj.contentInsetAdjustmentBehavior = UIScrollView.ContentInsetAdjustmentBehavior.never
            } else {
                // Fallback on earlier versions
            }
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
       
    }
    
    @objc func infosClick(){
        self.presenter?.router?.pushToInfosVC(from: self)
    }
    
    @objc func featureClick(_ btn:UIButton){
        if btn.tag == 1000{
//            self.presenter?.router?.pushToTopic(from: self)
        }else if btn.tag == 1001{
            self.presenter?.router?.pushToSchedule(from: self)
        }else if btn.tag == 1002{
            
        }else if btn.tag == 1003{
            self.presenter?.router?.pushToMyCode(from: self)
        }else if btn.tag == 1004{
            self.presenter?.router?.pushToWorkStatus(from: self)
        }else if btn.tag == 1005{
            
        }else if btn.tag == 1006{
            self.presenter?.router?.pushToCollect(from: self)
//            self.presenter?.router?.pushToBingDingPhone(from: self)
        }else if btn.tag == 1007{
            
        }else if btn.tag == 1008{
            self.presenter?.router?.pushToTopic(from: self)
//            self.presenter?.router?.pushToBingDingPhone(from: self)
        }
    }
    
    @objc func settingClick(_ btn:UIButton){
        self.presenter?.router?.pushToSetting(from: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MeViewController: MeViewProtocols{
    func didMePresenterReceiveData(by params: Any?) {
        if let dataModel = JSONDeserializer<UserInfosModel>.deserializeFrom(json:toJSONString(dict: params! as! [String : Any])) {
            printTest(dataModel)
//            LLUserDefaultsManager.shared.saveUserNameInfos(userName: dataModel.sysUserBo?.userName)
//            LLUserDefaultsManager.shared.saveUserDepartmentNameInfos(userName: dataModel.sysUserBo?.departmentName)
//            LLUserDefaultsManager.shared.saveUserCompanyNameInfos(userName: dataModel.sysUserBo?.companyName)
//            LLUserDefaultsManager.shared.saveUserEmailInfos(emil: dataModel.sysUserBo?.email)
//            LLUserDefaultsManager.shared.saveUserPhoneInfos(phone: dataModel.sysUserBo?.phoneNumber)
            nameLabel?.text = dataModel.sysUserBo?.userName ?? "--"
            subNameLabel?.text = "/ "+(dataModel.sysUserBo?.departmentName ?? "")
            companyLabel?.text = dataModel.sysUserBo?.companyName ?? "--"
            
        }
    }
    
    func didMePresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
}
