//
//  CommerceCheckViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class CommerceCheckViewController: BaseViewController, UITextFieldDelegate {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    lazy var NoDataView:UIView = {
        let noDataView = UIView().then{ obj in
            obj.backgroundColor = .white
        }
        
        let noDataImageV = UIImageView().then { obj in
            noDataView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "apply_nodata")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(80)
                make.top.equalToSuperview().offset(166)
                make.right.equalToSuperview().offset(-80)
                make.height.equalTo(220)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            noDataView.addSubview(obj)
            obj.text = "暂无查询结果哦"
            obj.font = kRegularFont(14)
            obj.textColor = blackTextColor22
            obj.snp.makeConstraints { make in
                make.top.equalTo(noDataImageV.snp.bottom).offset(15)
                make.centerX.equalToSuperview()
            }
        }
        
        return noDataView
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "工商查询"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(CommerceCheckViewController.backClick))
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = lineColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let searchHeadView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.height.equalTo(60  )
            }
        }
        
        let searchView = UIView().then { obj in
            searchHeadView.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.layer.cornerRadius = 4
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.backgroundColor = .blue
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }
        
        _ = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kRegularFont(12)
            obj.placeholder = "搜索"
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(0)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(CommerceCheckTableViewCell.self, forCellReuseIdentifier: "CommerceCheckTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(searchHeadView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
//        let noDataView = NoDataView.then { obj in
//            view.addSubview(obj)
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(0)
//                make.right.equalToSuperview().offset(-0)
//                make.top.equalTo(searchHeadView.snp.bottom).offset(0)
//                make.bottom.equalToSuperview().offset(-0)
//            }
//        }
        
        // Do any additional setup after loading the view.
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension CommerceCheckViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommerceCheckTableViewCell", for: indexPath) as! CommerceCheckTableViewCell
        return cell
    }
}
