//
//  CommerceCheckTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class CommerceCheckTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.width.height.equalTo(50)
                make.centerY.equalToSuperview()
            }
        }
        
        let titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "零瓴软件技术（深圳）有限公司"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor13
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(10)
                make.top.equalToSuperview().offset(25)
            }
        }
        
        let reloadButton = UIButton().then { obj in
            contentView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "apply_refresh")
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(10)
                make.bottom.equalTo(imageV.snp.bottom).offset(-5)
                make.width.height.equalTo(12)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "2022-05-16更新"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor74
            obj.snp.makeConstraints { make in
                make.left.equalTo(reloadButton.snp.right).offset(5)
                make.centerY.equalTo(reloadButton)
            }
        }
        
        let statusLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor25.cgColor
            obj.text = "存续"
            obj.font = kRegularFont(9)
            obj.textColor = lineColor25
            obj.textAlignment = .center
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(titleLabel)
                make.width.equalTo(32)
                make.height.equalTo(15)
            }
        }
        
        let numLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.text = "4205"
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(timeLabel)
            }
        }
        
        let eyeImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "apply_look")
            obj.snp.makeConstraints { make in
                make.width.height.equalTo(12)
                make.right.equalTo(numLabel.snp.left).offset(-3)
                make.centerY.equalTo(timeLabel)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }
}
