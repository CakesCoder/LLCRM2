//
//  SellLeadsBusinessInfosDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
//import JXPagingView

extension SellLeadsBusinessInfosDetailViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SellLeadsBusinessInfosDetailViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class SellLeadsBusinessInfosDetailViewController: BaseViewController {
    
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var model:ClientDetailDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 1000)
        }
        
        let baseInfos = [["企业名称："],["*","统一社会信用代码："],["法定代表人："],["经营状态："],["成立日期："],["注册资本： "],["实缴资本："],["所属行业："], ["工商注册号："], ["组织机构代码："],["纳税人资质："],["营业期限："],["核准日期："],["登记机关："],["注册地址："],["经营范围："]]
        
        let contentInfos = ["零瓴软件技术（深圳）有限公司", "91440300MA5GR8QB58", "王火生", "开业", "2021-05-11", "1,000.00万元", "— —", "软件和信息技术服务行业", "440300213723999", "MA5GR8QB-5", "—", "2021-05-11至 无固定期限","2021-11-16","深圳市市场监督管理局","深圳市南山区桃源街道平山社区平山一路2号南山云谷创业园二期3栋207","一般经营项目是：软件技术开发、技术服务，企业咨询管理，企业管理软件开发；SaaS云产品的研发及销售；大数据分析、工业互联网、ERP、协同办公、客户管理CRM、集成供应链管理软件、集成产品开发软件、项目管理软件、人力资源管理类软件、计算机软、硬件及配件的研发与销售；网络工程的设计安装；智能制造MES系统的软件设计、研发、销售；计算机软、硬件及配件的批发及零售。（同意登记机关调整规范经营范围表述，以登记机关登记为准），许可经营项目是：无"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(82)
                    make.height.equalTo(20)
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
//                obj.text = contentInfos[i]
                if i == 0{
                    obj.text = model?.clientName ?? "--"
                }else if i == 1{
                    obj.text = model?.creditCode ?? "--"
                }else{
                    obj.text = "--"
                }
                obj.snp.makeConstraints { make in
                    make.left.equalTo(hintLabel.snp.right).offset(15)
                    make.centerY.equalToSuperview()
                    make.right.equalToSuperview().offset(-10)
                }
            }
    
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
