//
//  AddPlanTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit
import YYText_swift
class AddPlanTableViewCell: UITableViewCell {
    
    var nameLabel:UILabel?
    
    public var titleStr:String? {
        didSet{
            nameLabel?.text = titleStr
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        nameLabel = UILabel().then {obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let inputView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor7
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor7.cgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
                make.height.equalTo(93)
            }
        }
        
        let textView = YYTextView().then { obj in
            inputView.addSubview(obj)
            obj.placeholderText = "输入"
            obj.placeholderTextColor = blackTextColor32
            obj.placeholderFont = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
    }

}
