//
//  AddPlanViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit

class AddPlanViewController: BaseViewController {
    
    var names = ["今日工作总结", "明日工作计划", "工作心得体会"]
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "日计划"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddPlanViewController.backClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem.createcommitBarbuttonItem(name: "发送", target: self, action: #selector(AddPlanViewController.addClick))
        
        buildUI()
    }
    
    func buildUI(){
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let dateView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.height.equalTo(51)
            }
        }
        
        let leftDateButton = UIButton().then { obj in
            dateView.addSubview(obj)
            obj.setTitle("<", for: .normal)
            obj.setTitleColor(blackTextColor30, for: .normal)
            obj.titleLabel?.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let rightDateButton = UIButton().then { obj in
            dateView.addSubview(obj)
            obj.setTitle(">", for: .normal)
            obj.setTitleColor(blackTextColor30, for: .normal)
            obj.titleLabel?.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let dateShowView = UIView().then { obj in
            dateView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
                make.centerX.equalToSuperview()
                make.width.equalTo(200)
            }
        }
        
        let dateLabel = UILabel().then { obj in
            dateShowView.addSubview(obj)
            obj.text = "2022年05月05日 星期四"
            obj.font = kAMediumFont(14)
            obj.textColor = blackTextColor31
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-20)
            }
        }
        
        let dateButton = UIButton().then { obj in
            dateShowView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-10)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }
        
        let footView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 200)
//            obj.backgroundColor = .green
        }
        
        let infosBtn = UIButton().then { obj in
            footView.addSubview(obj)
            obj.setTitle("1位同事", for: .normal)
            obj.setTitleColor(workTagColor, for: .normal)
            obj.backgroundColor = bgColor4
            obj.layer.cornerRadius = 15
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor12.cgColor
            obj.titleLabel?.font = kRegularFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(20)
                make.width.equalTo(95)
                make.height.equalTo(30)
            }
        }
        
        let tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true
            obj.tableFooterView = footView

            obj.register(AddPlanTableViewCell.self, forCellReuseIdentifier: "AddPlanTableViewCell")
//            obj.register(AddApprovalInputTableViewCell.self, forCellReuseIdentifier: "AddApprovalInputTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(dateView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(){
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddPlanViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlanTableViewCell", for: indexPath) as! AddPlanTableViewCell
        cell.titleStr = names[indexPath.row]
        return cell
    }
    
    
}
