//
//  SellLeadsDetailProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsDetailProtocols: NSObject {

}

protocol SellLeadsDetailViewProtocols: AnyObject {
    var presenter: SellLeadsDetailPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetSellLeadsDetailPresenterReceiveData(by params: Any?)
    func didGetSellLeadsDetailPresenterReceiveError(error: MyError?)
}

protocol SellLeadsDetailPresenterProtocols: AnyObject{
    var view: SellLeadsDetailViewProtocols? { get set }
    var router: SellLeadsDetailRouterProtocols? { get set }
    var interactor: SellLeadsDetailInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetSellLeadsDetail(by params: Any?)
    func didGetSellLeadsDetailInteractorReceiveData(by params: Any?)
    func didGetSellLeadsDetailInteractorReceiveError(error: MyError?)
}

protocol SellLeadsDetailInteractorProtocols: AnyObject {
    var presenter: SellLeadsDetailPresenterProtocols? { get set }
    var entity: SellLeadsDetailEntityProtocols? { get set }
    
    func presenterRequestSellLeadsDetail(by params: Any?)
    func didEntitySellLeadsDetailReceiveData(by params: Any?)
    func didEntitySellLeadsDetailReceiveError(error: MyError?)
}

protocol SellLeadsDetailEntityProtocols: AnyObject {
    var interactor: SellLeadsDetailInteractorProtocols? { get set }
    
    func didSellLeadsDetailReceiveData(by params: Any?)
    func didSellLeadsDetailReceiveError(error: MyError?)
    func getSellLeadsDetailRequest(by params: Any?)
}

protocol SellLeadsDetailRouterProtocols: AnyObject {
    func pushToAddCustomerService(from previousView: UIViewController)
    func pushToCreatCustomerService(from previousView: UIViewController)
    func pushToAddSellLeads(from previousView: UIViewController) 
}



