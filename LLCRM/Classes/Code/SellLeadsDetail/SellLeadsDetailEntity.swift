//
//  SellLeadsDetailEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsDetailEntity: SellLeadsDetailEntityProtocols {
    var interactor: SellLeadsDetailInteractorProtocols?
    
    func didSellLeadsDetailReceiveData(by params: Any?) {
        interactor?.didEntitySellLeadsDetailReceiveData(by: params)
    }
    
    func didSellLeadsDetailReceiveError(error: MyError?) {
        interactor?.didEntitySellLeadsDetailReceiveError(error: error)
    }
    
    func getSellLeadsDetailRequest(by params: Any?) {
        LLNetProvider.request(.sampleDel(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didSellLeadsDetailReceiveData(by:jsonData)
            case .failure(_):
                self.didSellLeadsDetailReceiveError(error: .requestError)
            }
        }
    }
    

}
