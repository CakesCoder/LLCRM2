//
//  SellLeadsDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
//import JXSegmentedView
//import JXPagingView
import PKHUD
import HandyJSON

class SellLeadListsDetailViewController: BaseViewController {
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    
    var nameLabel:UILabel?
    var tagImaegV:UIImageView?
    var tagImaegV2:UIImageView?
    var timeLabel:UILabel?
    var codeLabel:UILabel?
    var personNameLabel:UILabel?
    var statusLabel:UILabel?
    var contentLabel:UILabel?
    var lastTimeLabel:UILabel?
    
    var headView:UIView?
    var model:ClientDetailDataModel?
    
    var presenter: SellLeadsDetailPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "销售线索详情"
        view.backgroundColor = .white
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(SellLeadListsDetailViewController.blackClick))
        
        let lineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
            obj.backgroundColor = .white
        }
        
        nameLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            obj.text = model?.clientName ?? "暂无"
            obj.font = kMediumFont(16)
            obj.textColor = blackTextColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        tagImaegV = UIImageView().then { obj in
            headView?.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "detail_business")
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(nameLabel!)
                make.width.height.equalTo(15)
            }
        }
        
        tagImaegV2 = UIImageView().then { obj in
            headView?.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "detail_local")
            obj.isUserInteractionEnabled = true
            obj.snp.makeConstraints { make in
                make.right.equalTo(tagImaegV!.snp.left).offset(-10   )
                make.centerY.equalTo(nameLabel!)
                make.width.height.equalTo(15)
            }
        }
        
        timeLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            let timeHintAttribute = NSMutableAttributedString(string:"创建时间：")
            timeHintAttribute.yy_color = blacktextColor52
            timeHintAttribute.yy_font = kMediumFont(13)
            let timeCotentAttribute = NSMutableAttributedString(string: "暂无")
            timeCotentAttribute.yy_color = blackTextColor80
            timeCotentAttribute.yy_font = kMediumFont(13)
            timeHintAttribute.append(timeCotentAttribute)
            obj.attributedText = timeHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
//                make.top.equalToSuperview().offset(15)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
                
            }
        }
        
        codeLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            
            let codeHintAttribute = NSMutableAttributedString(string:"线索编号：")
            codeHintAttribute.yy_color = blacktextColor52
            codeHintAttribute.yy_font = kMediumFont(13)
            let codeCotentAttribute = NSMutableAttributedString(string:model?.clueNumber ?? "暂无")
            codeCotentAttribute.yy_color = blackTextColor80
            codeCotentAttribute.yy_font = kMediumFont(13)
            codeHintAttribute.append(codeCotentAttribute)
            obj.attributedText = codeHintAttribute
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(timeLabel!.snp.bottom).offset(10)
            }
        }
        
        personNameLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            
            let accountHintAttribute = NSMutableAttributedString(string:"负责人：")
            accountHintAttribute.yy_color = blacktextColor52
            accountHintAttribute.yy_font = kMediumFont(13)
            let accoubtCotentAttribute = NSMutableAttributedString(string:"暂无")
            accoubtCotentAttribute.yy_color = blackTextColor80
            accoubtCotentAttribute.yy_font = kMediumFont(13)
            accountHintAttribute.append(accoubtCotentAttribute)
            obj.attributedText = accountHintAttribute
        
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(codeLabel!.snp.bottom).offset(10)
            }
        }
        
        statusLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            let hintAttribute = NSMutableAttributedString(string:"状态：")
            hintAttribute.yy_color = blacktextColor52
            hintAttribute.yy_font = kMediumFont(13)
            obj.attributedText = hintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(personNameLabel!.snp.bottom).offset(10)
            }
        }
        
        contentLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            obj.backgroundColor = bgColor9
            obj.layer.cornerRadius = 2
            obj.font = kRegularFont(9)
            obj.textAlignment = .center
            obj.textColor = .white
            obj.text = "暂无"
            obj.snp.makeConstraints { make in
                make.left.equalTo(statusLabel!.snp.right).offset(0)
                make.centerY.equalTo(statusLabel!)
                make.width.equalTo(40)
                make.height.equalTo(20)
            }
        }
        
        lastTimeLabel = UILabel().then { obj in
            headView?.addSubview(obj)
            let lastTimeHintAttribute = NSMutableAttributedString(string: "最后跟进时间：")
            lastTimeHintAttribute.yy_color = blacktextColor52
            lastTimeHintAttribute.yy_font = kMediumFont(13)
            let lastTimeCotentAttribute = NSMutableAttributedString(string: "暂无")
            lastTimeCotentAttribute.yy_color = blackTextColor80
            lastTimeCotentAttribute.yy_font = kMediumFont(13)
            lastTimeHintAttribute.append(lastTimeCotentAttribute)
            obj.attributedText = lastTimeHintAttribute
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(statusLabel!.snp.bottom).offset(10)
            }
        }
        
        let lineView2 = UIView().then { obj in
            headView?.addSubview(obj)
            obj.backgroundColor = lineColor4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lastTimeLabel!.snp.bottom).offset(10)
                make.height.equalTo(0.5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        headView?.addSubview(self.segmentedView!)
//        segmentedView?.backgroundColor = .red
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(lineView2.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["基本信息", "工商信息", "联系人信息"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = segmentedTitleDataSource
        
//        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        headView?.addSubview(pagingView!)
//        segmentedView?.contentScrollView = pagingView?.listContainerView.scrollView
//        segmentedView?.listContainer = pagingView?.listContainerView as? any JXSegmentedViewListContainer
        
        pagingView?.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView!)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
        
        _ = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })
        
        _ = UIView().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight())
            }
        })
        
        let editorImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "simple_detail_editor")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview().offset(-45)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let editorLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "编辑"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(editorImageV.snp.bottom).offset(5)
                make.centerX.equalToSuperview().offset(-45)
            }
        }
        
        let editorButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.addTarget(self, action: #selector(editorClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalToSuperview().offset(-45)
            }
        }
        
        let delImageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.image = UIImage(named: "simple_detail_delete")
            obj.snp.makeConstraints { make in
//                make.left.equalTo(editorImageV.snp.right).offset(68)
                make.centerX.equalToSuperview().offset(45)
                make.top.equalTo(tableView.snp.bottom).offset(5)
            }
        }
        
        let delLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "删除"
            obj.textColor = blackTextColor53
            obj.font = kMediumFont(10   )
            obj.snp.makeConstraints { make in
                make.top.equalTo(delImageV.snp.bottom).offset(5)
                make.centerX.equalTo(delImageV)
            }
        }
        
        let delButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.addTarget(self, action: #selector(delClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalTo(tableView.snp.bottom).offset(5)
                make.bottom.equalToSuperview().offset(-10)
                make.width.equalTo(50)
                make.centerX.equalTo(delImageV)
            }
        }
        
        cofing()
        // Do any additional setup after loading the view.
    }
    
    @objc func editorClick(){
        
    }
    
    @objc func delClick(){
        presenter?.presenterRequestGetSellLeadsDetail(by: ["id":model?.id ?? ""])
    }
    
    func cofing(){
        let router = SellLeadsDetailRouter()
        
        presenter = SellLeadsDetailPresenter()
        
        presenter?.router = router
        
        let entity = SellLeadsDetailEntity()
        
        let interactor = SellLeadsDetailInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    @objc func blackClick(){
        navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension SellLeadListsDetailViewController:SellLeadsDetailViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    
    func didGetSellLeadsDetailPresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
//                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SellLeadsListNotificationKey"), object: nil, userInfo: nil)
                navigationController?.popViewController(animated: true)
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetSellLeadsDetailPresenterReceiveError(error: MyError?) {
        
    }
    
    
    
}

extension SellLeadListsDetailViewController: JXSegmentedViewDelegate, JXPagingViewDelegate{
    
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = SellLeadsDetaiInfoslViewController()
            vc.model = model
            return vc
        }else if index == 1{
            let vc = SellLeadsBusinessInfosDetailViewController()
            vc.model = model
            return vc
        }
        let vc = SellLeadsLinkManInfosDetailViewController()
        vc.model = model
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 3
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
    
//    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
////        printTest("2222222222/2222222")
////        printTest(index)
////        pagingView?.listContainerView.currentIndex = index
//
////        pagingView?.listContainerView(pagingView, initListAt: index)
//
//    }
//
//    func segmentedView(_ segmentedView: JXSegmentedView, didScrollSelectedItemAt index: Int) {
//        printTest(index)
////        pagingView(pagingView!, initListAtIndex: index)
//    }
}
