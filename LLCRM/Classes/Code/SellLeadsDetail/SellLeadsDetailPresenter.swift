//
//  SellLeadsDetailPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsDetailPresenter: SellLeadsDetailPresenterProtocols {
    var view: SellLeadsDetailViewProtocols?
    
    var router: SellLeadsDetailRouterProtocols?
    
    var interactor: SellLeadsDetailInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetSellLeadsDetail(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestSellLeadsDetail(by: params)
    }
    
    func didGetSellLeadsDetailInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetSellLeadsDetailPresenterReceiveData(by: params)
    }
    
    func didGetSellLeadsDetailInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetSellLeadsDetailPresenterReceiveError(error: error)
    }
    
    
}
