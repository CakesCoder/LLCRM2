//
//  SellLeadsDetailInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit

class SellLeadsDetailInteractor: SellLeadsDetailInteractorProtocols {
    var presenter: SellLeadsDetailPresenterProtocols?
    
    var entity: SellLeadsDetailEntityProtocols?
    
    func presenterRequestSellLeadsDetail(by params: Any?) {
        entity?.getSellLeadsDetailRequest(by: params)
    }
    
    func didEntitySellLeadsDetailReceiveData(by params: Any?) {
        presenter?.didGetSellLeadsDetailInteractorReceiveData(by: params)
    }
    
    func didEntitySellLeadsDetailReceiveError(error: MyError?) {
        presenter?.didGetSellLeadsDetailInteractorReceiveError(error: error)
    }
    
    
}
