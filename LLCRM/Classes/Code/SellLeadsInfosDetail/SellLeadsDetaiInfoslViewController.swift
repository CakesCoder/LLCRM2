//
//  SellLeadsDetailViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
//import JXPagingView

extension SellLeadsDetaiInfoslViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension SellLeadsDetaiInfoslViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}


class SellLeadsDetaiInfoslViewController: BaseViewController {
    
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    var model:ClientDetailDataModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let baseInfos = [["*","客户名称："],["隶属集团："],["所属公海："],["客户级别："],["属性："],["来源： "],["产业："],["隶属行业："], ["规模："], ["国别："],["地址："]]

        
//        let contentInfos = ["零瓴软件技术（深圳）有限公司", "— —", "公海公海", "一级", "— —", "来源来源来源来源", "制造业", "制造业", "规模规模", "中国", "广东省深圳市南山区桃源街道"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                    }
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(25)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(82)
                    make.height.equalTo(20)
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor81
                if i == 0{
                    obj.text = model?.clientName ?? "--"
                }else if i == 1{
                    obj.text = model?.parentCompany ?? "--"
                }else if i == 2{
                    obj.text = model?.highSeas ?? "--"
                }else if i == 3{
                    obj.text = model?.clientLevel ?? "--"
                }else if i == 4{
                    obj.text = model?.companyProperty ?? "--"
                }else if i == 5{
                    obj.text = model?.highSeas ?? "--"
                }else if i == 6{
                    obj.text = model?.industry ?? "--"
                }else if i == 7{
                    obj.text = model?.parentIndustry ?? "--"
                }else if i == 8{
                    obj.text = model?.scale ?? "--"
                }else if i == 9{
                    let local = (model?.city ?? "")+(model?.area ?? "")+(model?.address ?? "")
                    obj.text = local
                }
                obj.snp.makeConstraints { make in
                    make.left.equalTo(hintLabel.snp.right).offset(15)
                    make.centerY.equalToSuperview()
                }
            }
    
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
