//
//  ChangeHourView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class ChangeHourView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: CGRectMake(0, 0, kScreenWidth, kScreenHeight))
        let v = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(361)
            }
        }
        
        let okButton = UIButton().then { obj in
            v.addSubview(obj)
            obj.setTitle("完成", for: .normal)
            obj.titleLabel?.font = kMediumFont(16)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.snp.makeConstraints { make in
                //                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(20)
                make.right.equalToSuperview().offset(-30)
                make.width.equalTo(40)
                make.height.equalTo(22)
            }
        }
        
        //创建日期选择器
        let datePicker = UIDatePicker(frame: CGRectMake(0, 56, kScreenWidth, 100))
        v.addSubview(datePicker)
        datePicker.locale = Locale(identifier: "zh_CN")
        datePicker.backgroundColor = .red
        datePicker.addTarget(self, action: #selector(dateChanged),
                      for: .valueChanged)
        datePicker.datePickerMode = .time
        datePicker.minimumDate = Date.init(timeIntervalSince1970: 946656000)    // 起点时间2000.01.01 00:00:00，早于这个时间不可用
        datePicker.maximumDate = Date.init(timeIntervalSinceNow: 0)             // 截止时间为现在，晚于这个时间不可用
//        datePicker.datePickerMode = .date
//        datePicker.datePickerStyle = .automatic
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //日期选择器响应方法
    @objc func dateChanged(datePicker : UIDatePicker){
        //更新提醒时间文本框
        let formatter = DateFormatter()
        //日期样式
        formatter.dateFormat = "yyyy年MM月dd日 HH:mm:ss"
        print(formatter.string(from: datePicker.date))
    }
    
}
