//
//  ChangePunchDateViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit
import JFPopup

class ChangePunchDateViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "打卡时间"
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = lineColor
        }
        
        let hints = ["上班时间", "下班时间"]
        let contents = ["请选择", "请选择"]
        for i in 0..<hints.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.frame = CGRect(x: 0, y: i*60, width: Int(kScreenWidth), height: 60)
            }
            
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
//                var attri =  NSMutableAttributedString(string:"")
//                if i < 5{
                let attri = NSMutableAttributedString(string:"*")
//                }
                attri.yy_color = blackTextColor27
                
                let titleText = hints[i]
                let userAttri = NSMutableAttributedString(string: titleText)
                userAttri.yy_color = blackTextColor3
                userAttri.yy_font = kMediumFont(14)
                attri.append(userAttri)
                obj.attributedText = attri
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let tagImageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.image = UIImage(named: "me_tag")
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                
                obj.textColor = blackTextColor3
                obj.font = kMediumFont(12)
                obj.text = contents[i]
                
                obj.snp.makeConstraints { make in
                    make.right.equalTo(tagImageV.snp.left).offset(-10)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            
//            _ = UIButton().then({ obj in
//                v.addSubview(obj)
//                obj.tag = 1000+i
//                obj.addTarget(self, action: #selector(touchClick(_ :)), for: .touchUpInside)
//                obj.snp.makeConstraints { make in
//                    make.left.top.equalToSuperview().offset(0)
//                    make.right.bottom.equalToSuperview().offset(-0)
//                }
//            })
        }
        
        _ = UIButton().then({ obj in
            headView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("保存", for: .normal)
            obj.layer.cornerRadius = 2
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(168)
                make.height.equalTo(40)
            }
        })
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight()+10)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        // Do any additional setup after loading the view.
    }
    
    @objc func touchClick(_ button:UIButton){
//        printTest("asdasdasdasdasdasdasd")
//        if button.tag == 1005{
//            self.popup.bottomSheet {
//                let v = ChangeHourView()
//                return v
//            }
//        }else{
//            self.popup.bottomSheet {
//                let v = RuleWeekView()
//                return v
//            }
//        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
