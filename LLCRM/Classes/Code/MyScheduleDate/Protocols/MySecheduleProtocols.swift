//
//  MySecheduleProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class MySecheduleProtocols: NSObject {

}

protocol MySecheduleViewProtocols: AnyObject {
    var presenter: MySechedulePresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol MySechedulePresenterProtocols: AnyObject{
    var view: MySecheduleViewProtocols? { get set }
    var router: MySecheduleRouterProtocols? { get set }
    var interactor: MySecheduleInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol MySecheduleInteractorProtocols: AnyObject {
    var presenter: MySechedulePresenterProtocols? { get set }
    var entity: MySecheduleEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol MySecheduleEntityProtocols: AnyObject {
    var interactor: MySecheduleInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol MySecheduleRouterProtocols: AnyObject {
    func pushToSchedule(from previousView: UIViewController)
    func pushToScheduleDetail(from previousView: UIViewController) 
}
