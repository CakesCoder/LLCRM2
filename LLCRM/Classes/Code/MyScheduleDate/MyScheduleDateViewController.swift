//
//  MyScheduleDateViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/10.
//

import UIKit

class MyScheduleDateViewController: BaseViewController {
    
    /// 视图日历
    var calendarView: CalendarView?
    var presenter: MySechedulePresenterProtocols?
    /// 年月显示
    var yearOMonthLabel: UILabel = {
       let label = UILabel()
        label.textAlignment = .center
        return label
    }()
    /// 日历顶部显示年份月份
    var yearOMonth: Date = Date() {
        didSet {
            //年份月份展示label
            self.yearOMonthLabel.text = self.formatYearOMonth(yearOMonth)
            //月份日期展示collectionview
            self.calendarView?.date = yearOMonth
        }
    }
    
    
    
    /// 上一个月份按钮
    var lastButton: UIButton = UIButton()
    /// 下一个月份按钮
    var nextButton: UIButton = UIButton()
    /// 上一年按钮
    var nextYearButton: UIButton = UIButton()
    /// 下一年按钮
    var lastYearButton: UIButton = UIButton()
    
    var cellWidth: CGFloat = 0.0
    var cellHeight: CGFloat = 0.0
    /// 日历外边框
    let contentView: UIView = {
        let view = UIView()
        return view
    }()
    /// 日期展示文本
//    var dateLabel: UILabel = {
//        let label = UILabel()
//        label.textColor = UIColor(red: 115/255, green: 201/255, blue: 188/255, alpha: 1)
//        label.textAlignment = .left
//        return label
//    }()
    /// 日历外边框宽度
    var contentWidth: CGFloat = 0.0
    
//    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
//    }
    
    /// 获取上个月的同一日期
    @objc func getLastMonth() {
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(-1, for: .month)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    /// 获取下个月的同一日期
    @objc func getNextMonth() {
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(+1, for: .month)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    
    /// 获取下一年的同一日期
    @objc func getNextYear(){
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(+1, for: .year)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    
    /// 获取上一年的同一日期
    @objc func getLastYear(){
        let calendar = Calendar.init(identifier: .gregorian)
        var comLast = DateComponents()
        comLast.setValue(-1, for: .year)
        self.yearOMonth = calendar.date(byAdding: comLast, to: self.yearOMonth)!
    }
    
    
    /// 将日期展示为年月
    func formatYearOMonth(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy年MM月"
        let string = formatter.string(from: date)
        return string
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "我的日程"
        // Do any additional setup after loading the view.
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(MyScheduleDateViewController.backClick))
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 350)
        }
        
        headView.addSubview(self.contentView)
        contentView.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.top.equalToSuperview().offset(0)
            make.bottom.equalToSuperview().offset(-0)
        }
        
        
        contentView.addSubview(self.yearOMonthLabel)
//        yearOMonthLabel.backgroundColor = .red
        yearOMonthLabel.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.top.equalToSuperview().offset(25)
        }
        
        contentView.addSubview(lastYearButton)
//        lastYearButton.backgroundColor = .blue
        lastYearButton.setImage(UIImage(named: "work_nextYear"), for: .normal)
        lastYearButton.addTarget(self, action: #selector(getLastYear), for: .touchUpInside)
        lastYearButton.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(5)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        
        contentView.addSubview(self.lastButton)
//        lastButton.backgroundColor = .red
        lastButton.setImage(UIImage(named: "month_left"), for: .normal)
        lastButton.addTarget(self, action: #selector(getLastMonth), for: .touchUpInside)
        lastButton.snp.makeConstraints { make in
            make.left.equalTo(lastYearButton.snp.right).offset(25)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        
        contentView.addSubview(nextYearButton)
//        nextYearButton.backgroundColor = .yellow
        nextYearButton.setImage(UIImage(named: "work_lastYear"), for: .normal)
        nextYearButton.addTarget(self, action: #selector(getNextYear), for: .touchUpInside)
        nextYearButton.snp.makeConstraints { make in
            make.right.equalToSuperview().offset(-5)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        contentView.addSubview(nextButton)
//        nextButton.backgroundColor = .brown
        nextButton.setImage(UIImage(named: "month_right2"), for: .normal)
        nextButton.addTarget(self, action: #selector(getNextMonth), for: .touchUpInside)
        nextButton.snp.makeConstraints { make in
            make.right.equalTo(nextYearButton.snp.left).offset(-25)
            make.centerY.equalTo(yearOMonthLabel)
            make.width.height.equalTo(30)
        }
        
        
        let weekView = WeekView(frame: CGRect(x: 0, y: 80, width: kScreenWidth - 30, height: 40))
        contentView.addSubview(weekView)
        
        cellWidth = ((kScreenWidth - 30)/7)
        cellHeight = 40
        yearOMonthLabel.text = self.formatYearOMonth(self.yearOMonth)
        
        let calendarViewFrame = CGRect(x: 0, y: 120, width: kScreenWidth-30, height: 225)
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 5
        layout.minimumInteritemSpacing = 0
        layout.itemSize = CGSize(width: self.cellWidth, height: self.cellHeight)
        self.calendarView = CalendarView(frame: calendarViewFrame, collectionViewLayout: layout)
        self.contentView.addSubview(self.calendarView ?? UICollectionView())
        self.calendarView?.getDatesBlock = { (label) in
            label.textColor = .white
            label.backgroundColor = workTagColor
        }
        self.calendarView?.date = yearOMonth
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.delegate = self
            obj.dataSource =  self
            obj.separatorStyle = .none
            obj.register(MyScheduleTableViewCell.self, forCellReuseIdentifier: "MyScheduleTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
        let addButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        }
        
        cofing()
    }
    
    func cofing(){
        let router = MySecheduleRouter()
        
        presenter = MySechedulePresenter()
        
        presenter?.router = router
        
        let entity = MySecheduleEntity()
        
        let interactor = MySecheduleInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }

    @objc func addClick(_ button:UIButton){
        presenter?.router?.pushToSchedule(from: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MyScheduleDateViewController: MySecheduleViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterReceiveData() {
        
    }
    
    func didPresenterReceiveError() {
        
    }
}

extension MyScheduleDateViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 66
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyScheduleTableViewCell", for: indexPath) as! MyScheduleTableViewCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter?.router?.pushToScheduleDetail(from: self)
    }
}
