//
//  MySecheduleRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class MySecheduleRouter: MySecheduleRouterProtocols {
    
    func pushToSchedule(from previousView: UIViewController) {
        
        let nextView = ScheduleViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToScheduleDetail(from previousView: UIViewController) {
        
        let nextView = ScheduleDetailViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
}
