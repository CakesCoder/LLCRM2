//
//  MySecheduleInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class MySecheduleInteractor: MySecheduleInteractorProtocols {
    var presenter: MySechedulePresenterProtocols?
    
    var entity: MySecheduleEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
    
    
}
