//
//  MyScheduleTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class MyScheduleTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        self.selectionStyle = .none
        
        let tagView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 3
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.height.equalTo(6)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "讨论APP页面设计及交互的跳转"
            obj.font = kAMediumFont(14)
            obj.textColor = blackTextColor8
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagView.snp.right).offset(5)
                make.centerY.equalTo(tagView)
            }
        }
        
        let personButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.setImage(UIImage(named: "schedule_person"), for: .normal)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagView)
                make.width.equalTo(20)
                make.height.equalTo(14)
            }
        }
        
        let timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(11)
            obj.textColor = blackTextColor10
            
            obj.text = "13:46  ~  14:46"
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(27)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let statusLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(11)
            obj.textColor = blackTextColor9
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(15)
                make.centerY.equalTo(timeLabel)
            }
        }
        
        let lineView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor6
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        }
    }

}
