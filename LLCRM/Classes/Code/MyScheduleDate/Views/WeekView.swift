//
//  WeekView.swift
//  CalendarDemo
//
//  Created by Janise on 2019/1/15.
//  Copyright © 2019年 Janise. All rights reserved.
//

import UIKit

class WeekView: UIView {

    let weekDays: [String] = ["日","一","二","三","四","五","六"]
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.white
        for (index,string) in self.weekDays.enumerated() {
            let dayLabel: UILabel = {
                let label = UILabel()
                label.text = string
                label.textColor = blackTextColor6
                label.textAlignment = .center
                return label
            }()
            dayLabel.frame = CGRect(x: CGFloat(index)*(frame.width/7), y: 0, width: frame.width/7, height: 30)
            self.addSubview(dayLabel)
        }
        
        _ = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = lineColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
