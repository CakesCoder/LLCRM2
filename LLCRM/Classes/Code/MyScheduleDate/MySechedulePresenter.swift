//
//  MySechedulePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class MySechedulePresenter: MySechedulePresenterProtocols {
    var view: MySecheduleViewProtocols?
    
    var router: MySecheduleRouterProtocols?
    
    var interactor: MySecheduleInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
