//
//  SafetyViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class SafetyViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "账号与安全"
        
        let headView = UIView().then { obj in
            obj.backgroundColor = bgColor
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let topLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let hints = ["绑定手机号", "修改密码", "设备管理", "已授权手机设备", "登录日志", "开启密码锁屏", "开启Touch ID解锁"]
        
        let accountView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 10, width: Int(kScreenWidth), height: 50)
        }
        
        let accountHintLabel = UILabel().then { obj in
            accountView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = hints[0]
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        let tagImageV = UIImageView().then { obj in
            accountView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
                make.width.equalTo(8)
                make.height.equalTo(13)
            }
        }

        let phoneLabel = UILabel().then { obj in
            accountView.addSubview(obj)
            obj.text = "138383891810"
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalTo(tagImageV.snp.left).offset(-5)
                make.centerY.equalToSuperview()
            }
        }

        let twoLineView = UIView().then { obj in
            accountView.addSubview(obj)
            obj.backgroundColor = bgColor

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
                make.top.equalTo(accountView.snp.bottom).offset(0)
            }
        }
        
        let accountButton = UIButton().then { obj in
            accountView.addSubview(obj)
            obj.tag = 1000
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        
        for i in 1..<5{
            let bgView = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.frame = CGRect(x: 0, y: 20 + i*50, width: Int(kScreenWidth), height: 50)
            }
            
            let hintLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.font = kRegularFont(16)
                obj.textColor = blackTextColor
                obj.text = hints[i]
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(28)
                    make.centerY.equalToSuperview()
                }
            }
            
            let tagImageV = UIImageView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalToSuperview()
                    make.width.equalTo(8)
                    make.height.equalTo(13)
                }
            }
            
            
            _ = UIView().then({ obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor18
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                    make.right.equalToSuperview().offset(-0)
                }
            })
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                obj.tag = 1000+i
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        
        let threeLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
                make.top.equalToSuperview().offset(270)
            }
        }
        
        let psdView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalTo(threeLineView.snp.bottom).offset(0)
            }
        }

        let psdHintLabel = UILabel().then { obj in
            psdView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = "开启密码锁屏"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }

        let psdSwitch = UISwitch().then { obj in
            psdView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }

        _ = UIView().then({ obj in
            psdView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
                make.right.equalToSuperview().offset(-0)
            }
        })
        
        let psdhintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(41)
                make.top.equalTo(psdView.snp.bottom).offset(0)
            }
        }
        
        let psdhintLabel = UILabel().then { obj in
            psdhintView.addSubview(obj)
            obj.text = "开启后，需使用登录密码解锁零瓴软件"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor46
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.left.equalToSuperview().offset(15)
                make.height.equalTo(41)
                make.top.equalToSuperview().offset(0)
            }
        }
        
        
        let touchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalTo(psdhintView.snp.bottom).offset(0)
            }
        }

        let touchHintLabel = UILabel().then { obj in
            touchView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = "开启Touch ID解锁"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }

        let touchSwitch = UISwitch().then { obj in
            touchView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }

        _ = UIView().then({ obj in
            touchView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
                make.right.equalToSuperview().offset(-0)
            }
        })
        
        let touchhintView = UIView().then { obj in
            touchView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.left.equalToSuperview().offset(0)
                make.height.equalTo(41)
                make.top.equalTo(touchView.snp.bottom).offset(0)
            }
        }
        
        let touchhintLabel = UILabel().then { obj in
            touchhintView.addSubview(obj)
            obj.text = "开启后，可使用Touch ID验证需要密码验证页面"
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor46
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.left.equalToSuperview().offset(15)
                make.height.equalTo(41)
                make.top.equalToSuperview().offset(0)
            }
        }
        
//        for i in 3..<6{
//            let bgView = UIView().then { obj in
//                headView.addSubview(obj)
//                obj.backgroundColor = .white
//                obj.frame = CGRect(x: 0, y: 30 + i*50, width: Int(kScreenWidth), height: 50)
//            }
//
//            let hintLabel = UILabel().then { obj in
//                bgView.addSubview(obj)
//                obj.font = kRegularFont(16)
//                obj.textColor = blackTextColor
//                obj.text = hints[i]
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(28)
//                    make.centerY.equalToSuperview()
//                }
//            }
//
//            _ = UIView().then({ obj in
//                bgView.addSubview(obj)
//                obj.backgroundColor = lineColor18
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.bottom.equalToSuperview().offset(-0)
//                    make.height.equalTo(0.5)
//                    make.right.equalToSuperview().offset(-0)
//                }
//            })
//
//            let button = UIButton().then { obj in
//                bgView.addSubview(obj)
//                obj.tag = 1000+i
//                obj.snp.makeConstraints { make in
//                    make.left.top.equalToSuperview().offset(0)
//                    make.right.bottom.equalToSuperview().offset(-0)
//                }
//            }
//        }
//
//        let fourLineView = UIView().then { obj in
//            headView.addSubview(obj)
//            obj.backgroundColor = bgColor
//
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(0)
//                make.right.equalToSuperview().offset(-0)
//                make.height.equalTo(10)
//                make.top.equalToSuperview().offset(330)
//            }
//        }
//
//        let aboutView = UIView().then { obj in
//            headView.addSubview(obj)
//            obj.backgroundColor = .white
//            obj.frame = CGRect(x: 0, y: 10 + 330, width: Int(kScreenWidth), height: 50)
//        }
//
//        let aboutHintLabel = UILabel().then { obj in
//            aboutView.addSubview(obj)
//            obj.font = kRegularFont(16)
//            obj.textColor = blackTextColor
//            obj.text = hints[2]
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(28)
//                make.centerY.equalToSuperview()
//            }
//        }
//
//        _ = UIView().then({ obj in
//            aboutView.addSubview(obj)
//            obj.backgroundColor = lineColor18
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(15)
//                make.bottom.equalToSuperview().offset(-0)
//                make.height.equalTo(0.5)
//                make.right.equalToSuperview().offset(-0)
//            }
//        })
//
//        let button = UIButton().then { obj in
//            aboutView.addSubview(obj)
//            obj.tag = 1000+5
//            obj.snp.makeConstraints { make in
//                make.left.top.equalToSuperview().offset(0)
//                make.right.bottom.equalToSuperview().offset(-0)
//            }
//        }
        
//        let footView = UIView().then { obj in
//            obj.backgroundColor = bgColor
//            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 200)
//        }
//
//        let outButton = UIButton().then { obj in
//            footView.addSubview(obj)
//            obj.layer.cornerRadius = 2
//            obj.backgroundColor = blackTextColor36
//            obj.setTitle("退出登录", for: .normal)
//            obj.setTitleColor(.white, for: .normal)
//            obj.titleLabel?.font = kMediumFont(15)
//            obj.snp.makeConstraints { make in
//                make.top.equalToSuperview().offset(90)
//                make.left.equalToSuperview().offset(15)
//                make.right.equalToSuperview().offset(-15)
//                make.height.equalTo(40)
//            }
//        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
        
        
        

        // Do any additional setup after loading the view.
    }
    

    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
