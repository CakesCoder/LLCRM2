//
//  LOginInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

class LoginInteractor: LoginInteractorProtocols {
    func entityLoginReceiveData(by params: Any?) {
        entity?.LoginRequest(by: params)
    }
    
    func entityGetCodeReceiveData(by params: Any?) {
        entity?.getCodeRequest(by: params)
    }
    
    func didEntityLoginReceiveData(by params: Any?) {
        presenter?.didLoginInteractorReceiveData(by: params)
    }
    
    func didEntityLoginReceiveError(error: MyError?) {
        presenter?.didLoginInteractorReceiveError(by: error)
    }
    
    func didEntityGetCodeReceiveData(by params: Any?) {
        presenter?.didGetCodeInteractorReceiveData(by: params)
    }
    
    func didEntityGetCodeReceiveError(error: MyError?) {
        presenter?.didGetCodeInteractorReceiveError(by: error)
    }
    
    var presenter: LoginPresenterProtocols?
    var entity: LoginEntityProtocols?
    
    func entityGetUserInfosReceiveData(by params: Any?){
        entity?.getUserInfosRequest(by: params)
    }
    
    func didEntityGetUserInfosReceiveData(by params: Any?){
        presenter?.didGetUserInfosInteractorReceiveData(by: params)
    }
    
    func didEntityUserInfosReceiveError(error: MyError?){
        presenter?.didGetUserInfosInteractorReceiveError(by: error)
    }
}
