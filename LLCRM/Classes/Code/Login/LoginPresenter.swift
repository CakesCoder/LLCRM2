//
//  LoginPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import HandyJSON

class LoginPresenter: LoginPresenterProtocols {
    func presenterRequestGetCode(by params: Any?) {
        view?.showLoading()
        interactor?.entityGetCodeReceiveData(by: params)
    }
    
    func presenterReqesutLoginData(by params: Any?) {
        view?.showLoading()
        interactor?.entityLoginReceiveData(by: params)
    }
    
    
    func didGetCodeInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetCodePresenterReceiveData(by: params)
    }
    
    func didGetCodeInteractorReceiveError(by params: Any?) {
        view?.hideLoading()
        view?.showError()
    }
    
    func didLoginInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didLoginPresenterReceiveData(by: params)
    }
    
    func didLoginInteractorReceiveError(by params: Any?) {
        view?.hideLoading()
        view?.showError()
    }
    
    var view: LoginViewProtocols?
    var router: LoginRouterProtocols?
    var interactor: LoginInteractorProtocols?
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetUserInfos(by params: Any?) {
        interactor?.entityGetUserInfosReceiveData(by: params)
    }

    func didGetUserInfosInteractorReceiveData(by params: Any?){
        if (params != nil){
            let dict = params as! [String:Any]
            if let dict = params as? [String:Any]{
                let dataDic = dict["data"] as? [String:Any]
                printTest(dataDic)
                if dict["code"] as! Int == 200{
                    if let dataModel = JSONDeserializer<UserInfosModel>.deserializeFrom(json:toJSONString(dict: dataDic!)) {
                        printTest(dataModel)
                        LLUserDefaultsManager.shared.saveUserNameInfos(userName: dataModel.sysUserBo?.userName)
                        LLUserDefaultsManager.shared.saveUserDepartmentNameInfos(userName: dataModel.sysUserBo?.departmentName)
                        LLUserDefaultsManager.shared.saveUserCompanyNameInfos(userName: dataModel.sysUserBo?.companyName)
                        LLUserDefaultsManager.shared.saveUserEmailInfos(emil: dataModel.sysUserBo?.email)
                        LLUserDefaultsManager.shared.saveUserPhoneInfos(phone: dataModel.sysUserBo?.phoneNumber)
                    }
                }
            }
        }
    }
    func didGetUserInfosInteractorReceiveError(by params: Any?){
        
    }
    
}
