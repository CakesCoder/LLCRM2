//
//  LoginRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

protocol LoginViewProtocols: AnyObject {
    var presenter: LoginPresenterProtocols? { get set }

    func didLoginPresenterReceiveData(by params: Any?)
    func didLoginPresenterReceiveError(error: MyError?)
    func didGetCodePresenterReceiveData(by params: Any?)
    func didGetCodePresenterReceiveError(error: MyError?)
    
    func showLoading()
    func showError()
    func hideLoading()
}

protocol LoginPresenterProtocols: AnyObject{
    var view: LoginViewProtocols? { get set }
    var router: LoginRouterProtocols? { get set }
    var interactor: LoginInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    func presenterReqesutLoginData(by params: Any?)
    func presenterRequestGetCode(by params: Any?)
    
    func didLoginInteractorReceiveData(by params: Any?)
    func didLoginInteractorReceiveError(by params: Any?)
    
    func didGetCodeInteractorReceiveData(by params: Any?)
    func didGetCodeInteractorReceiveError(by params: Any?)
    
    func presenterRequestGetUserInfos(by params: Any?)
    func didGetUserInfosInteractorReceiveData(by params: Any?)
    func didGetUserInfosInteractorReceiveError(by params: Any?)
}

protocol LoginInteractorProtocols: AnyObject {
    var presenter: LoginPresenterProtocols? { get set }
    var entity: LoginEntityProtocols? { get set }
    
    
    func entityLoginReceiveData(by params: Any?)
    func didEntityLoginReceiveData(by params: Any?)
    func didEntityLoginReceiveError(error: MyError?)
    
    func entityGetCodeReceiveData(by params: Any?)
    func didEntityGetCodeReceiveData(by params: Any?)
    func didEntityGetCodeReceiveError(error: MyError?)
    
    func entityGetUserInfosReceiveData(by params: Any?)
    func didEntityGetUserInfosReceiveData(by params: Any?)
    func didEntityUserInfosReceiveError(error: MyError?)
}

protocol LoginEntityProtocols: AnyObject {
    var interactor: LoginInteractorProtocols? { get set }
    
    func LoginRequest(by params: Any?)
    func getCodeRequest(by params: Any?)

    func didLoginReceiveData(by params: Any?)
    func didLoginReceiveError(error: MyError?)
    
    func didGetCodeReceiveData(by params: Any?)
    func didGetCodeReceiveError(error: MyError?)
    
    func getUserInfosRequest(by params: Any?)
    func didUserInfosReceiveData(by params: Any?)
    func didUserInfosReceiveError(error: MyError?) 
}


protocol LoginRouterProtocols: AnyObject {
    
}
