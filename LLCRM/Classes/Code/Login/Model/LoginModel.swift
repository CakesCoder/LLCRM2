//
//  LoginModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/17.
//

import UIKit
import HandyJSON
import SwiftyUserDefaults

class LoginModel: BaseModel{
    
    var access_token:String = ""
    var expires_in:Int = 0
    var user_account:Int = 0
}


class LoginModelUtil: NSObject{
    static func dictionaryToModel(_ dictionStr:[String:Any],_ modelType:HandyJSON.Type) -> LoginModel {
        if dictionStr.count == 0 {
            #if DEBUG
                print("dictionaryToModel:字符串为空")
            #endif
            return LoginModel()
        }
        return modelType.deserialize(from: dictionStr) as! LoginModel
    }
}
