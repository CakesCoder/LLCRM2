//
//  LoginViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/12.
//

import UIKit
import YYText_swift
import PKHUD

class LoginViewController: BaseViewController {
    var phoneButton: UIButton?
    var companyButton: UIButton?
    var codeButton: UIButton?
    var companyCodeButton: UIButton?
    var phoneLine: UIView?
    var companyLine: UIView?
    var codeTextFiled: UITextField?
    var accountTextFiled: UITextField?
    var personTextFiled: UITextField?
    var loginStatusButton: UIButton?
    var codeView: UIView?
    var companyInputView: UIView?
    var protocolButton: UIButton?
    var companypsdTextFiled:UITextField?
    
    var loginIndex: Int? = 0
    
    // 计时器
    var timer: Timer?
    //秒数
    var countSeocnds = 60
    
    // 计时器
    var companyTimer: Timer?
    //秒数
    var companyCountSeocnds = 60
    
    var headView: UIView?
    var isCompany: Bool?
    var isPsd: Bool!
//    var isHome:Bool?
    
    var presenter: LoginPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if (timer != nil) {
            //正在即时跳转到其他页面处理
            timer?.invalidate()
            timer = nil
        }
        
        if codeButton != nil {
            codeButton?.setTitle("获取验证码", for: .normal)
            codeButton?.setTitleColor(themeColor, for: .normal)
            codeButton?.isUserInteractionEnabled = true
        }
        
        if (companyTimer != nil){
            companyTimer?.invalidate()
            companyTimer = nil
        }
        
        if companyCodeButton != nil {
            companyCodeButton?.setTitle("获取验证码", for: .normal)
            companyCodeButton?.setTitleColor(themeColor, for: .normal)
            companyCodeButton?.isUserInteractionEnabled = true
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buildData()
        buildUI()
        cofing()
        
        accountTextFiled?.text = "15697587343"
        personTextFiled?.text = "99262490"
        companypsdTextFiled?.text = "311357"
    }
    
    func cofing(){
        let router = LoginRouter()
        
        presenter = LoginPresenter()
        presenter?.router = router
        
        let entity = LoginEntity()
        
        let interactor = LoginInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
    }
    
    
    func buildData(){
        isPsd = true
            
        //计时器监听
        NotificationCenter.default.addObserver(self, selector: #selector(didEnterBackground), name: UIApplication.didEnterBackgroundNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didBecomeActive), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    func buildUI(){
        backgroundView()
        loginHeader()
        loginPhone()
        loginCode()
        loginCompanyInput()
        loginAgreement()
        login()
        retrievePassword()
        passwordLogin()
    }
    
    func backgroundView(){
        let tableView = UITableView().then { obj in
            view.addSubview(obj)
            if #available(iOS 11.0, *) {
                obj.contentInsetAdjustmentBehavior = .never
            } else {
                self.automaticallyAdjustsScrollViewInsets = false
            }
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview()
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        headView = UIView().then({ obj in
            view.addSubview(obj)
            
            lazy var gradient: CAGradientLayer = [
                UIColor("#FD4340"),
                UIColor("#CE2BAE")
              ].gradient { gradient in
                gradient.speed = 0
                gradient.timeOffset = 0

                return gradient
              }
            
            obj.layer.insertSublayer(gradient, at: 1)
            
            tableView.tableHeaderView = obj
            
            if kScreenHeight > 812 {
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.width.equalTo(kScreenWidth)
                    make.height.equalTo(kScreenHeight)
                }
            }else{
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.width.equalTo(kScreenWidth)
                    make.height.equalTo(750)
                }
            }
        })
    }
    
    func loginHeader(){
        _ = UILabel().then({ obj in
            headView?.addSubview(obj)
            obj.text = "手机快捷登录"
            obj.font = UIFont.systemFont(ofSize: 25.0, weight:UIFont.Weight.medium)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(75 + 88)
            }
        })
        
        phoneButton = UIButton().then({ obj in
            headView?.addSubview(obj)
            obj.setTitle("手机号", for: .normal)
            obj.setTitleColor(UIColor(0x4871C0), for: .normal)
            obj.titleLabel?.font = kMediumFont(14)
            obj.addTarget(self, action: #selector(phoneLoginClick), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(150 + 88)
                make.width.equalTo(46)
                make.height.equalTo(20)
            }
        })
        
        phoneLine = UIView().then({ obj in
            headView?.addSubview(obj)
            obj.backgroundColor = UIColor(0x4871C0)
            obj.layer.cornerRadius = 10;
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(172+88)
                make.width.equalTo(56)
                make.height.equalTo(2)
            }
        })
        
        companyButton = UIButton().then({ obj in
            headView?.addSubview(obj)
            obj.setTitle("企业账号", for: .normal)
            obj.setTitleColor(UIColor(0x23262E), for: .normal)
            obj.titleLabel?.font = kMediumFont(14)
            obj.addTarget(self, action: #selector(companyLoginClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(85)
                make.top.equalToSuperview().offset(150 + 88)
                make.width.equalTo(60)
                make.height.equalTo(20)
            }
        })
        
        companyLine = UIView().then({ obj in
            headView?.addSubview(obj)
            obj.backgroundColor = UIColor(0x4871C0)
            obj.layer.cornerRadius = 10;
            obj.isHidden = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(80)
                make.top.equalToSuperview().offset(172+88)
                make.width.equalTo(68)
                make.height.equalTo(2)
            }
        })
    }
    
    func loginPhone(){
        let imageV = UIImageView().then({ obj in
            headView?.addSubview(obj)
            obj.image = UIImage(named: "phoneLogin")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(213 + 88)
                make.width.equalTo(12)
                make.height.equalTo(16)
            }
        })
        
        accountTextFiled = UITextField().then({ obj in
            headView?.addSubview(obj)
            obj.placeholder = "请输入手机号"
            obj.font = kAMediumFont(10)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.centerY.equalTo(imageV)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        })
        
        _ = UIView().then({ obj in
            headView?.addSubview(obj)
            obj.backgroundColor = UIColor(0xF4F5F5)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(241+88)
                make.height.equalTo(0.5)
            }
        })
    }
    
    func loginCode(){
        codeView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(264 + 88)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(25)
            }
        }
        
        let imageV = UIImageView().then({ obj in
            codeView?.addSubview(obj)
            obj.image = UIImage(named: "codeLogin")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.width.equalTo(12)
                make.height.equalTo(15)
            }
        })

        codeTextFiled = UITextField().then({ obj in
            codeView?.addSubview(obj)
            obj.placeholder = "请输入验证码"
            obj.font = UIFont.systemFont(ofSize: 10)
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.centerY.equalTo(imageV)
                make.right.equalToSuperview().offset(-90)
                make.height.equalTo(40)
            }
        })

        _ = UIView().then({ obj in
            codeView?.addSubview(obj)
            obj.backgroundColor = UIColor(0xF4F5F5)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(24)
                make.height.equalTo(0.5)
            }
        })

        codeButton = UIButton().then({ obj in
            codeView?.addSubview(obj)
            obj.setTitle("获取验证码", for: .normal)
            obj.setTitleColor(UIColor(0x409EFF), for: .normal)
            obj.titleLabel?.font = kMediumFont(10)
            obj.addTarget(self, action: #selector(getCodeClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.centerY.equalTo(codeTextFiled!)
                make.left.equalTo(codeTextFiled!.snp.right).offset(0)
                make.height.equalTo(25)
            }
        })
    }
    
    func loginCompanyInput(){
        companyInputView = UIView().then({ obj in
            view.addSubview(obj)
            obj.isHidden = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(264 + 88)
                make.right.equalToSuperview().offset(-30)
                make.height.equalTo(75)
            }
        })
        
        let imageV = UIImageView().then({ obj in
            companyInputView?.addSubview(obj)
            obj.image = UIImage(named: "loginPerson")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.width.equalTo(14)
                make.height.equalTo(15)
            }
        })

        personTextFiled = UITextField().then({ obj in
            companyInputView?.addSubview(obj)
            obj.placeholder = "请输入个人账号"
            obj.font = UIFont.systemFont(ofSize: 10)
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.centerY.equalTo(imageV)
                make.right.equalToSuperview().offset(-100)
                make.height.equalTo(40)
            }
        })

        _ = UIView().then({ obj in
            companyInputView?.addSubview(obj)
            obj.backgroundColor = UIColor(0xF4F5F5)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(24)
                make.height.equalTo(0.5)
            }
        })

        companyCodeButton = UIButton().then({ obj in
            companyInputView?.addSubview(obj)
            obj.setTitle("获取验证码", for: .normal)
            obj.setTitleColor(themeColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(10)
            obj.addTarget(self, action: #selector(getCompanyCodeClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.centerY.equalTo(personTextFiled!)
                make.left.equalTo(personTextFiled!.snp.right).offset(0)
                make.height.equalTo(25)
            }
        })
        
        let psdImageV = UIImageView().then({ obj in
            companyInputView?.addSubview(obj)
            obj.image = UIImage(named: "psdLogin")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(50)
                make.width.equalTo(14)
                make.height.equalTo(15)
            }
        })
        
        companypsdTextFiled = UITextField().then({ obj in
            companyInputView?.addSubview(obj)
            obj.placeholder = "请输入密码"
            obj.font = UIFont.systemFont(ofSize: 10)
            obj.snp.makeConstraints { make in
                make.left.equalTo(psdImageV.snp.right).offset(15)
                make.centerY.equalTo(psdImageV)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(40)
            }
        })
        
        _ = UIView().then({ obj in
            companyInputView?.addSubview(obj)
            obj.backgroundColor = UIColor(0xF4F5F5)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(74)
                make.height.equalTo(0.5)
            }
        })
    }
    
    func loginAgreement(){
        protocolButton = UIButton().then({ obj in
            headView?.addSubview(obj)
            obj.setImage(UIImage(named: "login_noAgain"), for: .normal)
            obj.setImage(UIImage(named: "login_again"), for: .selected)
            obj.layer.cornerRadius = 2;
            obj.addTarget(self, action: #selector(againClick(_:)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(306 + 88)
                make.width.height.equalTo(15)
            }
        })
        
        let protocolLabel = YYLabel().then({ obj in
            obj.textAlignment = .center
            obj.textColor = UIColor(0x666666)
            obj.font = kMediumFont(12)
            headView?.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(protocolButton!.snp.right).offset(5)
                make.centerY.equalTo(protocolButton!)
            }
        })
        
        let attri = NSMutableAttributedString(string:"我已阅读并同意")
        attri.yy_color = UIColor(0x666666)
        let location = attri.yy_rangeOfAll.length
        
        let userText = "《用户协议》"
        let userAttri = NSMutableAttributedString(string: userText)
        userAttri.yy_color = UIColor(0x4871C0)
        userAttri.yy_font = kMediumFont(12)
        attri.append(userAttri)
        
        let andText = "及"
        let andAttri = NSMutableAttributedString(string: andText)
        andAttri.yy_color = UIColor(0x666666)
        attri.append(andAttri)
        
        let privacyText = "《隐私政策》"
        let privacyAttri = NSMutableAttributedString(string: privacyText)
        privacyAttri.yy_color = UIColor(0x4871C0)
        attri.append(privacyAttri)
        
        protocolLabel.attributedText = attri
        
//        attri.yy_set(textHighlightRange: NSRange(location: location, length: userText.count), color: UIColor(0x4871C0), backgroundColor: .clear, userInfo: ["url": agreementUrl]) { view, _, _, _ in
//            let highlight = (view as! YYLabel).highlight
//            let url = highlight?.userInfo?["url"] as! String
//            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
//        }  longPress: { view, _, _, rect in
//            print("yy 长按了")
//            print("测试测试2222")
//        }
        
        attri.yy_set(textHighlightRange: NSRange(location: location, length: userText.count), color: UIColor(0xC82125), backgroundColor: .clear, userInfo: ["url": "https://www.baidu.com"]) { view, _, _, _ in
            let highlight = (view as! YYLabel).highlight
            let url = highlight?.userInfo?["url"] as! String
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        }  longPress: { view, _, _, rect in
            print("yy 长按了")
        }
        
        
        attri.yy_set(textHighlightRange: NSRange(location: location, length: privacyText.count), color: UIColor(0x4871C0), backgroundColor: .clear, userInfo: ["url": privacyUrl]) { view, _, _, _ in
            let highlight = (view as! YYLabel).highlight
            let url = highlight?.userInfo?["url"] as! String
            UIApplication.shared.open(URL(string: url)!, options: [:], completionHandler: nil)
        }  longPress: { view, _, _, rect in
            print("yy 长按了")
            print("测试测试11111")
        }
    }
    
    func login(){
        _ = UIButton().then({ obj in
            headView?.addSubview(obj)
            obj.backgroundColor = UIColor(0x4871C0)
            obj.layer.cornerRadius = 20;
            obj.titleLabel?.font = kMediumFont(15)
            obj.titleLabel?.textColor = UIColor(0xfff)
            obj.setTitle("立即体验", for: .normal)
            obj.addTarget(self, action: #selector(loginClick(_:)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(protocolButton!.snp.bottom).offset(36)
                make.height.equalTo(40)
            }
        })
    }
    
    func retrievePassword(){
        _ = UIButton().then({ obj in
            headView?.addSubview(obj)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(UIColor(0x4871C0), for: .normal)
            obj.setTitle("忘记密码？", for: .normal)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(protocolButton!.snp.bottom).offset(90)
                make.width.equalTo(70)
                make.height.equalTo(17)
            }
        })
    }
    
    func passwordLogin(){
        loginStatusButton = UIButton().then({ obj in
            headView?.addSubview(obj)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(UIColor(0x4871C0), for: .normal)
            obj.setTitle("密码登录", for: .normal)
            obj.addTarget(self, action: #selector(loginStatusClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(protocolButton!.snp.bottom).offset(90)
                make.width.equalTo(70)
                make.height.equalTo(17)
            }
        })
    }
    
    func codeResponseEvent(){
        if timer == nil{
            timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownButtonTimer), userInfo: self, repeats: true)
        }
    }
    
    func companyCodeResponseEvent(){
        if companyTimer == nil{
            companyTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(companyCountDownButtonTimer), userInfo: self, repeats: true)
        }
    }
    
    @objc func againClick(_ btn:UIButton){
        btn.isSelected = !btn.isSelected
    }
    
    @objc func phoneLoginClick() {
        if loginIndex == 0 {
            return
        }
        
        accountTextFiled?.text = "15697587343"
        personTextFiled?.text = "99262490"
        companypsdTextFiled?.text = "311357"
        
        //手机号
        phoneButton?.setTitleColor(UIColor(0x4871C0), for: .normal)
        companyButton?.setTitleColor(UIColor(0x23262E), for: .normal)
        phoneLine?.isHidden = false
        companyLine?.isHidden = true
        accountTextFiled?.placeholder = "请输入手机号"
        codeView?.isHidden = false
        companyInputView?.isHidden = true
        loginIndex = 0
        loginStatusButton?.isHidden = false
        protocolButton?.snp.updateConstraints({ make in
            make.top.equalToSuperview().offset(306 + 88)
        })
    }
    
    @objc func companyLoginClick(){
        if loginIndex == 1 {
            return
        }
        
        accountTextFiled?.text = "99262490"
        personTextFiled?.text = "15697587343"
        companypsdTextFiled?.text = "311357"
        
        //企业号
        phoneButton?.setTitleColor(UIColor(0x23262E), for: .normal)
        companyButton?.setTitleColor(UIColor(0x4871C0), for: .normal)
        phoneLine?.isHidden = true
        companyLine?.isHidden = false
        accountTextFiled?.placeholder = "请输入企业账号"
        codeView?.isHidden = true
        companyInputView?.isHidden = false
        loginIndex = 1
        loginStatusButton?.isHidden = true
        protocolButton?.snp.updateConstraints({ make in
            make.top.equalToSuperview().offset(356 + 88)
        })
    }
    
    @objc func loginStatusClick(){
        isPsd = !isPsd
        if isPsd{
            codeButton?.isHidden = false
            loginStatusButton?.setTitle("密码登录", for: .normal)
            codeTextFiled?.placeholder = "请输入验证码"
            codeTextFiled?.snp.updateConstraints({ make in
                make.right.equalToSuperview().offset(-60)
            })
        }else{
            codeButton?.isHidden = true
            codeTextFiled?.placeholder = "请输入密码"
            loginStatusButton?.setTitle("验证码登录", for: .normal)
            codeTextFiled?.snp.updateConstraints({ make in
                make.right.equalToSuperview().offset(-0)
            })
        }
    }
    
    @objc func getCodeClick(){
        if accountTextFiled?.text == ""{
            HUD.flash(.label("请输入账号"), delay: 1)
            return
        }
        printTest(accountTextFiled?.text)
        presenter?.presenterRequestGetCode(by: ["phone" : accountTextFiled?.text ])
        
    }
    
    @objc func getCompanyCodeClick(){
        if accountTextFiled?.text == ""{
            HUD.flash(.label("请输入账号"), delay: 1)
            return
        }
        printTest(accountTextFiled?.text)
        presenter?.presenterRequestGetCode(by: ["phone" : accountTextFiled?.text ])
    }
    
    //进入后台处理
    @objc func didEnterBackground(){
        if timer != nil{
            timer?.invalidate()
            timer = nil
            UserDefaults.standard.set(Date(), forKey: "stopRunTime")
        }
    }
    
    //进入前台处理
    @objc func didBecomeActive(){
        let lastRunTime = UserDefaults.standard.object(forKey: "stopRunTime")
        if lastRunTime != nil{
            let stopRunTime = lastRunTime as! Date
            //计算出上次与当前
            let different = Int(Date().timeIntervalSince(stopRunTime))
            countSeocnds -= different
            UserDefaults.standard.removeObject(forKey: "stopRunTime")
            UserDefaults.standard.synchronize()
            
            if countSeocnds <= 1{
                codeButton?.setTitle("获取验证码", for: .normal)
                codeButton?.setTitleColor(themeColor, for: .normal)
                codeButton?.isUserInteractionEnabled = true
                countSeocnds = 60
            }else{
                if timer != nil{
                    timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countDownButtonTimer), userInfo: self, repeats: true)
                }
                
                if companyTimer != nil{
                    companyTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(companyCountDownButtonTimer), userInfo: self, repeats: true)
                }
                
            }
        }
    }
    
    @objc func countDownButtonTimer(){
        if countSeocnds <= 1{
            timer?.invalidate()
            timer = nil
            codeButton?.setTitle("获取验证码", for: .normal)
            codeButton?.setTitleColor(themeColor, for: .normal)
            codeButton?.isUserInteractionEnabled = true
            countSeocnds = 60
            return
        }
        countSeocnds -= 1
        codeButton?.setTitle("重新获取(\(countSeocnds)s)", for: .normal)
        codeButton?.setTitleColor(themeColor, for: .normal)
        codeButton?.isUserInteractionEnabled = false
    }
    
    @objc func companyCountDownButtonTimer(){
        if companyCountSeocnds <= 1{
            companyTimer?.invalidate()
            companyTimer = nil
            companyCodeButton?.setTitle("获取验证码", for: .normal)
            companyCodeButton?.setTitleColor(themeColor, for: .normal)
            companyCodeButton?.isUserInteractionEnabled = true
            companyCountSeocnds = 60
            return
        }
        companyCountSeocnds -= 1
        companyCodeButton?.setTitle("重新获取(\(companyCountSeocnds)s)", for: .normal)
        companyCodeButton?.setTitleColor(themeColor, for: .normal)
        companyCodeButton?.isUserInteractionEnabled = false
    }
    
    @objc func loginClick(_ btn:UIButton){
        if accountTextFiled?.text == ""{
            HUD.flash(.label("请输入账号"), delay: 1)
            return
        }
        
        if !protocolButton!.isSelected {
            HUD.flash(.label("请同意用户协议和隐私政策"), delay: 1)
            return
        }
        
        if loginIndex == 0{
            if !isPsd {
                if codeTextFiled?.text == ""{
                    HUD.flash(.label("请输入密码"), delay: 1)
                    return
                }
                presenter?.presenterReqesutLoginData(by: ["type":1, "username": accountTextFiled?.text as Any, "password": codeTextFiled?.text as Any] as [String : Any])
                return
            }
            if codeTextFiled?.text == ""{
                HUD.flash(.label("请输入验证码"), delay: 1)
                return
            }
            presenter?.presenterReqesutLoginData(by: ["type": 3, "username": accountTextFiled?.text as? String, "code": codeTextFiled?.text as Any] as [String : Any])
            
        }else{
            if personTextFiled?.text == ""{
                HUD.flash(.label("请输企业账号"), delay: 1)
                return
            }
            if accountTextFiled?.text == ""{
                HUD.flash(.label("请输入个人账号"), delay: 1)
                return
            }
            if companypsdTextFiled?.text == ""{
                HUD.flash(.label("请输入密码"), delay: 1)
                return
            }
//            let username:String = (accountTextFiled?.text!)!
//            let companyCode:String = (personTextFiled?.text!)!
//            let password:String = (companypsdTextFiled?.text!)!
//
            presenter?.presenterReqesutLoginData(by: ["companyCode": accountTextFiled?.text as? String, "password": companypsdTextFiled?.text as? String, "type": 1, "username": personTextFiled?.text as? String] as [String : Any])
        }
        
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension LoginViewController: LoginViewProtocols{
    func didLoginPresenterReceiveData(by params: Any?) {
        guard let requestData = params as? [String : Any] else{
            HUD.flash(.label("data为nil"), delay: 2)
            return
        }
 
        if requestData["code"] as! Int != 200{
            HUD.flash(.label(requestData["message"] as? String), delay: 2)
        }else{
            let model:LoginModel = LoginModelUtil.dictionaryToModel(requestData["data"] as! [String : Any], LoginModel.self)
            printTest(model)
            
            let dataDic = requestData["data"] as! [String:Any]
            
            LLUserDefaultsManager.shared.saveLoginInfos(infos: dataDic)
            LLUserDefaultsManager.shared.saveLoginInfosTK(tk: dataDic["access_token"] as? String)
            
//            if let userAccount = dataDic["access_token"]{
//                presenter!.presenterRequestGetUserInfos(by: ["userAccount":userAccount, "userType":"0"])
//            }
//            
            self.dismiss(animated: true)
        }
    }
    
    func didLoginPresenterReceiveError(error: MyError?) {
        
    }
    
    func didGetCodePresenterReceiveData(by params: Any?) {
        guard let requestData = params as? [String : Any] else{
            HUD.flash(.label("data为nil"), delay: 2)
            return
        }
        if requestData["code"] as! Int != 200{
            HUD.flash(.label(requestData["message"] as? String), delay: 2)
        }else{
            HUD.flash(.label("验证码发送成功"), delay: 2)
            if self.loginIndex == 0{
                codeResponseEvent()
            }else{
                companyCodeResponseEvent()
            }
        }
        
        
    }
    
    func didGetCodePresenterReceiveError(error: MyError?) {
        
    }
    
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
}
