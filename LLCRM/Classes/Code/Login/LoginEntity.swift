//
//  LoginEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit
import SwiftyJSON

class LoginEntity: LoginEntityProtocols {

    var interactor: LoginInteractorProtocols?
    
    func didLoginReceiveData(by params: Any?) {
        interactor?.didEntityLoginReceiveData(by: params)
    }
    
    func didGetCodeReceiveData(by params: Any?) {
        interactor?.didEntityGetCodeReceiveData(by: params)
    }
    
    func didGetCodeReceiveError(error: MyError?) {
        interactor?.didEntityGetCodeReceiveError(error: error)
    }
    
    func didLoginReceiveError(error: MyError?) {
        interactor?.didEntityLoginReceiveError(error: error)
    }
    
    func getCodeRequest(by params: Any?) {
        LLNetProvider.request(.phoneCode(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didGetCodeReceiveData(by:jsonData)
            case .failure(_):
                self.didGetCodeReceiveError(error: .requestError)
            }
        }
    }
    
    func LoginRequest(by params: Any?){
        LLNetProvider.request(.login(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didLoginReceiveData(by:jsonData)
            case .failure(_):
                self.didLoginReceiveError(error: .requestError)
            }
        }
    }
    
    func getUserInfosRequest(by params: Any?) {
        LLNetProvider.request(.userInfos(params as! [String : Any])) { result in
            switch result{
            case let .success(response):
                printTest(result)
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didUserInfosReceiveData(by:jsonData)
            case .failure(_):
                self.didUserInfosReceiveError(error: .requestError)
            }
        }
    }
    
    func didUserInfosReceiveData(by params: Any?) {
        interactor?.didEntityGetUserInfosReceiveData(by: params)
    }
    
    func didUserInfosReceiveError(error: MyError?) {
        interactor?.didEntityUserInfosReceiveError(error: error)
    }
}
