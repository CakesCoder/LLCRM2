//
//  AddTaskViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit

class AddTaskViewController: BaseViewController {
    
    let datas = ["任务标题", "客户名称", "任务状态"]
    var presenter: AddTaskPresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddTaskViewController.backClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem.createcommitBarbuttonItem(name: "提交", target: self, action: #selector(AddTaskViewController.addClick))
        // Do any additional setup after loading the view.
        buildUI()
        
    }
    
    func buildUI(){
            
        self.navigationItem.title = "新建任务"
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        
        let footView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 300)
//            obj.backgroundColor = .blue
        }
        
        let footLineView = UIView().then { obj in
            footView.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        
        let personView = UIView().then { obj in
            footView.addSubview(obj)
//            obj.backgroundColor = .red
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.top.equalTo(footLineView.snp.bottom).offset(0)
                make.height.equalTo(62)
            }
        }
        
        let personImageV = UIImageView().then { obj in
            personView.addSubview(obj)
            obj.image = UIImage(named: "work_person")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let personHintLabel = UILabel().then { obj in
            personView.addSubview(obj)
            obj.text = "执行人"
            obj.font = kRegularFont(14)
            obj.textColor = blackTextColor29
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(personImageV.snp.right).offset(10)
                make.centerY.equalToSuperview()
            }
        }
        
        let personHintImageV = UIImageView().then { obj in
            personView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "me_push")
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
//                make.width.equalTo(12)
//                make.height.equalTo(18)
            }
        }
        
        let personContentLabel = UILabel().then { obj in
            personView.addSubview(obj)
            obj.textColor = blackTextColor29
            obj.font = kRegularFont(14)
            obj.text = "今天 星期五 23：59"
            
            obj.snp.makeConstraints { make in
                make.right.equalTo(personHintImageV.snp.left).offset(-10)
                make.centerY.equalToSuperview()
            }
        }
        
        let personLineView = UIView().then { obj in
            personView.addSubview(obj)
            
            obj.backgroundColor = blackTextColor29
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let personButton = UIButton().then { obj in
            personView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let remindView = UIView().then { obj in
            footView.addSubview(obj)
//            obj.backgroundColor = .red
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.top.equalTo(personView.snp.bottom).offset(0)
                make.height.equalTo(62)
            }
        }
        
        let remindImageV = UIImageView().then { obj in
            remindView.addSubview(obj)
            obj.image = UIImage(named: "work_clock")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let remindHintLabel = UILabel().then { obj in
            remindView.addSubview(obj)
            obj.text = "截止时间"
            obj.font = kRegularFont(14)
            obj.textColor = blackTextColor29
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(remindImageV.snp.right).offset(10)
                make.centerY.equalToSuperview()
            }
        }
        
        let remindHintImageV = UIImageView().then { obj in
            remindView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "me_push")
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
//                make.width.equalTo(12)
//                make.height.equalTo(18)
            }
        }
        
        let remindContentLabel = UILabel().then { obj in
            remindView.addSubview(obj)
            obj.textColor = blackTextColor29
            obj.font = kRegularFont(14)
            obj.text = "今天 星期五 23：59"
            
            obj.snp.makeConstraints { make in
                make.right.equalTo(remindHintImageV.snp.left).offset(-10)
                make.centerY.equalToSuperview()
            }
        }
        
        let remindLineView = UIView().then { obj in
            remindView.addSubview(obj)
            
            obj.backgroundColor = blackTextColor29
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let remindButton = UIButton().then { obj in
            remindView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let warningView = UIView().then { obj in
            footView.addSubview(obj)
//            obj.backgroundColor = .red
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.top.equalTo(remindView.snp.bottom).offset(0)
                make.height.equalTo(62)
            }
        }
        
        let warningImageV = UIImageView().then { obj in
            warningView.addSubview(obj)
            obj.image = UIImage(named: "work_warning")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let warningHintLabel = UILabel().then { obj in
            warningView.addSubview(obj)
            obj.text = "提醒"
            obj.font = kRegularFont(14)
            obj.textColor = blackTextColor29
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(warningImageV.snp.right).offset(10)
                make.centerY.equalToSuperview()
            }
        }
        
        let warningHintImageV = UIImageView().then { obj in
            warningView.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.image = UIImage(named: "me_push")
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
//                make.width.equalTo(12)
//                make.height.equalTo(18)
            }
        }
        
        let warningContentLabel = UILabel().then { obj in
            warningView.addSubview(obj)
            obj.textColor = blackTextColor29
            obj.font = kRegularFont(14)
            obj.text = "今天 星期五 23：59"
            
            obj.snp.makeConstraints { make in
                make.right.equalTo(warningHintImageV.snp.left).offset(-10)
                make.centerY.equalToSuperview()
            }
        }
        
        let warningLineView = UIView().then { obj in
            warningView.addSubview(obj)
            
            obj.backgroundColor = blackTextColor29
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(45)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
        
        let warningButton = UIButton().then { obj in
            remindView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let infosBtn = UIButton().then { obj in
            footView.addSubview(obj)
            obj.setTitle("1位同事", for: .normal)
            obj.setTitleColor(workTagColor, for: .normal)
            obj.backgroundColor = bgColor4
            obj.layer.cornerRadius = 15
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor12.cgColor
            obj.titleLabel?.font = kRegularFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(warningView.snp.bottom).offset(45)
                make.width.equalTo(95)
                make.height.equalTo(30)
            }
        }
        
        let tableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true
            obj.tableFooterView = footView

            obj.register(AddTaskTableViewCell.self, forCellReuseIdentifier: "AddTaskTableViewCell")
//            obj.register(AddApprovalInputTableViewCell.self, forCellReuseIdentifier: "AddApprovalInputTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
    }
    
    func cofing(){
        let router = AddTaskRouter()
        
        presenter = AddTaskPresenter()
        
        presenter?.router = router
        
        let entity = AddTaskEntity()
        
        let interactor = AddTaskInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(){
        presenter?.router?.pushToApproval(from: self)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddTaskViewController: AddTaskViewProtocols{
    func reloadData() {
        
    }
    
    func didPresenterAddTaskViewReceiveData() {
        
    }
    
    func didPresenterAddTaskViewReceiveError() {
        
    }
}

extension AddTaskViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        if datas.count - 1 == indexPath.row{
//            return 250
//        }
        return 100
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return datas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddTaskTableViewCell", for: indexPath) as! AddTaskTableViewCell
        cell.titleStr = datas[indexPath.row]
        return cell
    }
    
    
}
