//
//  AddTaskRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddTaskRouter: AddTaskRouterProtocols {
    func pushToApproval(from previousView: UIViewController) {
        let nextView = ApprovalViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
