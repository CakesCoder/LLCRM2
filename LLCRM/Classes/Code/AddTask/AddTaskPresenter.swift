//
//  AddTaskPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddTaskPresenter: AddTaskPresenterProtocols {
    var view: AddTaskViewProtocols?
    
    var router: AddTaskRouterProtocols?
    
    var interactor: AddTaskInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorAddTaskReceiveData() {
        
    }
    
    func didInteractorAddTaskReceiveError() {
        
    }
    

}
