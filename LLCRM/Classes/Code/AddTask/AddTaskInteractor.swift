//
//  AddTaskInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddTaskInteractor: AddTaskInteractorProtocols {
    var presenter: AddTaskPresenterProtocols?
    
    var entity: AddTaskEntityProtocols?
    
    func interactorAddTaskRetrieveData() {
        
    }
    
    func entityAddTaskRetrieveData() {
        
    }
    
    func didEntityAddTaskReceiveData() {
        
    }
    
    func didEntityAddTaskReceiveError() {
        
    }
    

}
