//
//  AddTaskProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/26.
//

import UIKit

class AddTaskProtocols: NSObject {

}

protocol AddTaskViewProtocols: AnyObject {
    var presenter: AddTaskPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterAddTaskViewReceiveData()
    func didPresenterAddTaskViewReceiveError()
}

protocol AddTaskPresenterProtocols: AnyObject{
    var view: AddTaskViewProtocols? { get set }
    var router: AddTaskRouterProtocols? { get set }
    var interactor: AddTaskInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    func didInteractorAddTaskReceiveData()
    func didInteractorAddTaskReceiveError()
}

protocol AddTaskInteractorProtocols: AnyObject {
    var presenter: AddTaskPresenterProtocols? { get set }
    var entity: AddTaskEntityProtocols? { get set }
    
    func interactorAddTaskRetrieveData()
    func entityAddTaskRetrieveData()
    func didEntityAddTaskReceiveData()
    func didEntityAddTaskReceiveError()
}

protocol AddTaskEntityProtocols: AnyObject {
    var interactor: AddTaskInteractorProtocols? { get set }
    
    func retrieveAddTaskData()
    func didAddTaskReceiveData()
}

protocol AddTaskRouterProtocols: AnyObject {
    func pushToApproval(from previousView: UIViewController)
}

