//
//  AboutViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class AboutViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "关于零瓴软件"
        
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let topView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(162)
            }
        }
        
        let iconImageV = UIImageView().then { obj in
            headView.addSubview(obj)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(30)
                make.centerX.equalToSuperview()
                make.width.height.equalTo(60)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "零瓴软件"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.top.equalTo(iconImageV.snp.bottom).offset(10)
                make.centerX.equalToSuperview()
            }
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor45
            obj.font = kMediumFont(12)
            obj.text = "版本号  V8.0.0"
            obj.snp.makeConstraints { make in
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
                make.centerX.equalToSuperview()
            }
        }
        
        let hints = ["新版本介绍", "给我评分", "扫描二维码下载", "客服电话", "隐私政策", "服务协议"]
        for i in 0..<6{
            let bgView = UIView().then { obj in
                headView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: 162 + i*50, width: Int(kScreenWidth), height: 50)
            }
            
            let hintLabel = UILabel().then { obj in
                bgView.addSubview(obj)
                obj.font = kRegularFont(16)
                obj.textColor = blackTextColor
                obj.text = hints[i]
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(28)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i == 3{
                let phoneLabel = UILabel().then { obj in
                    bgView.addSubview(obj)
                    obj.text = "400 112 2778"
                    obj.textColor = blackTextColor40
                    obj.font = kMediumFont(14)
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-15)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            _ = UIView().then({ obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor18
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                    make.right.equalToSuperview().offset(-0)
                }
            })
            
            let button = UIButton().then { obj in
                bgView.addSubview(obj)
                
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
