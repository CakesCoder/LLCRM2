//
//  TopicTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/15.
//

import UIKit

class TopicTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(17.5)
            }
        } 
        
        let titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor16
            obj.text = "#交机（切割）#"
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(10)
                make.centerY.equalToSuperview()
            }
        }
        
        let focusButton = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 4
            obj.titleLabel?.font = kRegularFont(11)
            obj.setTitle("关注", for: .normal)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
                make.width.equalTo(69)
                make.height.equalTo(30)
            }
        }
        
        let numLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "80578"
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalTo(focusButton.snp.left).offset(-20)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
    }

}
