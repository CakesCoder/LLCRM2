//
//  TopicViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit
//import JXSegmentedView
//import JXPagingView

class TopicViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationItem.title = "我的话题"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(TopicViewController.blackClick))
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        view.addSubview(self.segmentedView!)
//        segmentedView?.backgroundColor = .greenr
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["我的关注", "热门话题"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        segmentedTitleDataSource?.isItemSpacingAverageEnabled = false
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
//        pagingView!.backgroundColor = UIColor.blue
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView

        view.addSubview(pagingView!)
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.bottom.equalTo(self.view)
        }
        
        let lineView = UIView().then { obj in
            segmentedView?.addSubview(obj)
            obj.backgroundColor = lineColor6
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        }
    }
    
    @objc func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TopicViewController : JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        let list = HotTopicViewController()
        return list
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 2
    }


//    func pagingView(_ pagingView: JXPagingView, listIdentifierAtIndex index: Int) -> String? {
//        return "123123"
//    }
//
//
//    func tableHeaderViewHeight(in pagingView: JXSegmentedView) -> Int {
//        return 460
//    }
//
//    func tableHeaderView(in pagingView: JXSegmentedView) -> UIView {
//        return UIView()
//    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
}
