//
//  ClientPerosnPriceEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class ClientPerosnPriceEntity: ClientPerosnPriceEntityProtocols {
    var interactor: ClientPerosnPriceInteractorProtocols?
    
    func didClientPerosnPriceReceiveData(by params: Any?) {
        interactor?.didEntityClientPerosnPriceReceiveData(by: params)
    }
    
    func didClientPerosnPriceReceiveError(error: MyError?) {
        interactor?.didEntityClientPerosnPriceReceiveError(error: error)
    }
    
    func getClientPerosnPriceRequest(by params: Any?) {
        LLNetProvider.request(.getPricsList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didClientPerosnPriceReceiveData(by:jsonData)
            case .failure(_):
                self.didClientPerosnPriceReceiveError(error: .requestError)
            }
        }
    }
}
