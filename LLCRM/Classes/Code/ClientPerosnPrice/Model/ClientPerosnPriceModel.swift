//
//  ClientPerosnPriceModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/6.
//

import UIKit
import HandyJSON

class ClientPerosnPriceModel: BaseModel {
    
    var countId:String?
    var current:Int?
    var hitCount:Int?
    var clientName:String?
    var contactEmail:String?
    var contactPhone:String?
    var deliveryCondition:String?
    var id:Int?
    var payCondition:String?
    var price:String?
    var productList:[productModel]?
    var productModel:String?
    var productName:String?
    var productNo:String?
    var productSpecs:String?
    var quotationNo:String?
    var quotationTime:String?
    var remark:String?
    var saleEmail:String?
    var saleName:String?
    var salePhone:String?
    var search:String?
    var size:String?
    var stampPicture:String?
    var type:Int?
    var records:[recordsModel]?
    
    required init() {}
}

class recordsModel:BaseModel{
    var bidder:String?
    var clientName:String?
    var companyId:Int?
    var contactEmail:String?
    var contactId:String?
    var contactName:String?
    var contactPhone:String?
    var createDate:String?
    var deleted:Int?
    var deliveryCondition:String?
    var examineStatus:Int?
    var id:Int?
    var payCondition:String?
    var printOperator:String?
    var printTime:String?
    var processId:String?
    var quotationNo:String?
    var quotationTime:String?
    var remark:String?
    var saleEmail:String?
    var saleName:String?
    var salePhone:String?
    var source:Int?
    var stampPicture:String?
    
    required init() {}
}

class productModel: BaseModel{
    var companyId:Int?
    var deleted:Int?
    var id:Int?
    var maker:String?
    var makerCode:String?
    var price:String?
    var productModel:String?
    var productName:String?
    var productNo:String?
    var productSpecs:String?
    var quotationId:Int?
    var quotationUnit:String?
    var taxRate:String?
    var unit:String?
    var validTimeEnd:String?
    var validTimeStart:String?
    
    
    required init() {}
}
