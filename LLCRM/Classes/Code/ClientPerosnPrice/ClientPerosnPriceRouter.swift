//
//  ClientPerosnPriceRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class ClientPerosnPriceRouter: ClientPerosnPriceRouterProtocols {
    func pushToPriceDetail(from previousView: UIViewController,forModel model: Any) {
        let nextView = PriceDetailViewController()
        nextView.recordModel = model as! recordsModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
