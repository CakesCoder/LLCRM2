//
//  ClientPerosnPricePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class ClientPerosnPricePresenter: ClientPerosnPricePresenterProtocols {
    var view: ClientPerosnPriceViewProtocols?
    
    var router: ClientPerosnPriceRouterProtocols?
    
    var interactor: ClientPerosnPriceInteractorProtocols?
    
    var params: [Any]? = []
    
    var current:Int? = 0
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetClientPerosnPrice(by params: Any?) {
        interactor?.presenterRequestClientPerosnPrice(by: params)
    }
    
    func didGetClientPerosnPriceInteractorReceiveData(by params: Any?) {
        view?.didGetClientPerosnPricePresenterReceiveData(by: params)
    }
    
    func didGetClientPerosnPriceInteractorReceiveError(error: MyError?) {
        view?.didGetClientPerosnPricePresenterReceiveError(error: error)
    }
    
    
}
