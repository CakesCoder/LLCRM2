//
//  ClientPerosnPriceCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnPriceCell: UITableViewCell {
    
    var model:recordsModel?{
        didSet{
            //            printTest("aasdasdasdasdasdasd")
            companyLabel?.text = model?.clientName ?? "--"
            timeLabel?.text = model?.createDate ?? "--"
            
            if model?.examineStatus == 0{
                statusLabel?.backgroundColor = lineColor4
                statusLabel?.text = "未发起"
                statusLabel?.textColor = blackTextColor74
                statusLabel?.layer.borderWidth = 0.5
                statusLabel?.layer.borderColor = lineColor15.cgColor
                statusLabel?.layer.cornerRadius = 2
                statusLabel?.textAlignment = .center
            }else if model?.examineStatus == 1{
                statusLabel?.backgroundColor = bgColor16
                statusLabel?.text = "报价中"
                statusLabel?.textColor = blackTextColor44
                statusLabel?.layer.borderWidth = 0.5
                statusLabel?.layer.borderColor = blackTextColor44.cgColor
                statusLabel?.layer.cornerRadius = 2
                statusLabel?.textAlignment = .center

            }else{
                statusLabel?.backgroundColor = bgColor14
                statusLabel?.text = "已完成"
                statusLabel?.textColor = bgColor9
                statusLabel?.layer.borderWidth = 0.5
                statusLabel?.layer.borderColor = bgColor9.cgColor
                statusLabel?.layer.cornerRadius = 2
                statusLabel?.textAlignment = .center
            }
        }
    }
    var companyLabel:UILabel?
    var timeLabel:UILabel?
    var statusLabel:UILabel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        initUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initUI(){
        
        companyLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "电子配件报价单"
            obj.font = kMediumFont(14)
            obj.textColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.top.equalToSuperview().offset(15)
            }
        }
        
        timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "20230524001"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor68
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        statusLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            
            obj.font = kRegularFont(10)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-25)
                make.top.equalToSuperview().offset(15)
                make.width.equalTo(48)
                make.height.equalTo(22)
            }
        }

        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor5
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(0.5)
            }
        })
        
    }

}
