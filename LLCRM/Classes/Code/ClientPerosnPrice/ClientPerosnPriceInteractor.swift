//
//  ClientPerosnPriceInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class ClientPerosnPriceInteractor: ClientPerosnPriceInteractorProtocols {
    var presenter: ClientPerosnPricePresenterProtocols?
    
    var entity: ClientPerosnPriceEntityProtocols?
    
    func presenterRequestClientPerosnPrice(by params: Any?) {
        entity?.getClientPerosnPriceRequest(by: params)
    }
    
    func didEntityClientPerosnPriceReceiveData(by params: Any?) {
        presenter?.didGetClientPerosnPriceInteractorReceiveData(by: params)
    }
    
    func didEntityClientPerosnPriceReceiveError(error: MyError?) {
        presenter?.didGetClientPerosnPriceInteractorReceiveError(error: error)
    }
}
