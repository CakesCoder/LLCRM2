//
//  ClientPerosnPriceProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

protocol ClientPerosnPriceViewProtocols: AnyObject {
    var presenter: ClientPerosnPricePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetClientPerosnPricePresenterReceiveData(by params: Any?)
    func didGetClientPerosnPricePresenterReceiveError(error: MyError?)
}

protocol ClientPerosnPricePresenterProtocols: AnyObject{
    var view: ClientPerosnPriceViewProtocols? { get set }
    var router: ClientPerosnPriceRouterProtocols? { get set }
    var interactor: ClientPerosnPriceInteractorProtocols? { get set }
    var params: [Any]? { get set }
    var current: Int? { get set }
    func viewDidLoad()
    
    func presenterRequestGetClientPerosnPrice(by params: Any?)
    func didGetClientPerosnPriceInteractorReceiveData(by params: Any?)
    func didGetClientPerosnPriceInteractorReceiveError(error: MyError?)
}

protocol ClientPerosnPriceInteractorProtocols: AnyObject {
    var presenter: ClientPerosnPricePresenterProtocols? { get set }
    var entity: ClientPerosnPriceEntityProtocols? { get set }
    
    func presenterRequestClientPerosnPrice(by params: Any?)
    func didEntityClientPerosnPriceReceiveData(by params: Any?)
    func didEntityClientPerosnPriceReceiveError(error: MyError?)
}

protocol ClientPerosnPriceEntityProtocols: AnyObject {
    var interactor: ClientPerosnPriceInteractorProtocols? { get set }
    
    func didClientPerosnPriceReceiveData(by params: Any?)
    func didClientPerosnPriceReceiveError(error: MyError?)
    func getClientPerosnPriceRequest(by params: Any?)
}

protocol ClientPerosnPriceRouterProtocols: AnyObject {
    func pushToPriceDetail(from previousView: UIViewController,forModel model: Any)
}

