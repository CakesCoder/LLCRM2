//
//  ClientPerosnPriceViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit
//import JXPagingView
import HandyJSON
import PKHUD

extension ClientPerosnPriceViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ClientPerosnPriceViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ClientPerosnPriceViewController: BaseViewController {
    
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var tableViewDidSeletedCallback: ((recordsModel) -> ())?
    var presenter: ClientPerosnPricePresenterProtocols?
    var model:ClientPerosonDetailModel?
    var tableView:UITableView?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 45)
            obj.backgroundColor = .white
        }
        
        let headTagView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "报价单"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(ClientPerosnPriceCell  .self, forCellReuseIdentifier: "ClientPerosnPriceCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        
        cofing()
    }
    
    func cofing(){
        let router = ClientPerosnPriceRouter()
        
        presenter = ClientPerosnPricePresenter()
        
        presenter?.router = router
        
        let entity = ClientPerosnPriceEntity()
        
        let interactor = ClientPerosnPriceInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
 
        presenter?.presenterRequestGetClientPerosnPrice(by: ["clientName":model?.clientName])
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ClientPerosnPriceViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 68
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.params?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ClientPerosnPriceCell", for: indexPath) as! ClientPerosnPriceCell
        cell.model = presenter?.params?[indexPath.row] as? recordsModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.tableViewDidSeletedCallback?((presenter?.params![indexPath.row] as? recordsModel)!)
    }
}

extension ClientPerosnPriceViewController: ClientPerosnPriceViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetClientPerosnPricePresenterReceiveData(by params: Any?) {
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            printTest(dataDic)
            if dict["code"] as! Int == 200{
                if let datas = JSONDeserializer<ClientPerosnPriceModel>.deserializeFrom(json:toJSONString(dict:dataDic!)) {
                    presenter?.params?.removeAll()
                    presenter?.params?.append(contentsOf: datas.records ?? [])
                    tableView?.reloadData()
                    presenter?.current! += 1
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetClientPerosnPricePresenterReceiveError(error: MyError?) {
        
    }
}
