//
//  ScheduleRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit

protocol SecheduleViewProtocols: AnyObject {
    var presenter: SechedulePresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol SechedulePresenterProtocols: AnyObject{
    var view: SecheduleViewProtocols? { get set }
    var router: SecheduleRouterProtocols? { get set }
    var interactor: SecheduleInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol SecheduleInteractorProtocols: AnyObject {
    var presenter: SechedulePresenterProtocols? { get set }
    var entity: SecheduleEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol SecheduleEntityProtocols: AnyObject {
    var interactor: SecheduleInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol SecheduleRouterProtocols: AnyObject {
    func pushToMySecheduleDateVC(from previousView: UIViewController)
    func pushToRemindVC(from previousView: UIViewController)
    func pushToRepeatVC(from previousView: UIViewController)
}
