//
//  ScheduleViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit
import JFPopup

class ScheduleViewController: BaseViewController {
    var presenter: SechedulePresenterProtocols?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
        self.navigationController?.navigationBar.isHidden = false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.navigationItem.title = "我的日程"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(ScheduleViewController.backClick))
        
        presenter = SechedulePresenter()
        presenter?.router = SecheduleRouter()
        
//        let interactor = WorkInteractor()
//        presenter.interactor = interactor
//        interactor.presenter = presenter
        
        presenter?.viewDidLoad()
        
        buildUI()
    }
    
    func buildUI(){
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        _ = UITableView().then { obj in
            view.addSubview(obj)
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
//            obj.shouldIgnoreScrollingAdjustment = true
//            obj.shouldRestoreScrollViewContentOffset = true

            obj.register(ScheduleInputTableViewCell.self, forCellReuseIdentifier: "ScheduleInputTableViewCell")
            obj.register(ScheduleDateTableViewCell.self, forCellReuseIdentifier: "ScheduleDateTableViewCell")
            obj.register(ScheduleNameTableViewCell.self, forCellReuseIdentifier: "ScheduleNameTableViewCell")
            obj.register(ScheduleMoreTableViewCell.self, forCellReuseIdentifier: "ScheduleMoreTableViewCell")
            obj.register(ScheduleAllDayTableViewCell.self, forCellReuseIdentifier: "ScheduleAllDayTableViewCell")
            obj.register(ScheduleStarTimeTableViewCell.self, forCellReuseIdentifier: "ScheduleStarTimeTableViewCell")
            obj.register(ScheduleEndTimeTableViewCell.self, forCellReuseIdentifier: "ScheduleEndTimeTableViewCell")
            obj.register(ScheduleSecretTableViewCell.self, forCellReuseIdentifier: "ScheduleSecretTableViewCell")
            obj.register(ScheduleRemindTableViewCell.self, forCellReuseIdentifier: "ScheduleRemindTableViewCell")
            obj.register(ScheduleLoopTableViewCell.self, forCellReuseIdentifier: "ScheduleLoopTableViewCell")
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(UIDevice.xp_tabBarFullHeight())
            }
        }
        
        let bottomView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight())
            }
        }
        
        let commitBtn = UIButton().then { obj in
            bottomView.addSubview(obj)

            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 4
            obj.layer.masksToBounds = true
            obj.setTitle("提交", for: .normal)
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kRegularFont(11)

            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-30)
                make.width.equalTo(69)
                make.height.equalTo(30)
            }
        }
        
        let addBtn = UIButton().then { obj in
            bottomView.addSubview(obj)

            obj.backgroundColor = .white
            obj.layer.cornerRadius = 4
//            obj.layer.masksToBounds = true
            obj.layer.borderWidth = 1
            obj.layer.borderColor = lineColor14.cgColor
            obj.setTitle("提交并新建", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.titleLabel?.font = kRegularFont(11)

            obj.snp.makeConstraints { make in
                make.right.equalTo(commitBtn.snp.left).offset(-10)
                make.bottom.equalToSuperview().offset(-30)
                make.width.equalTo(69)
                make.height.equalTo(30)
            }
        }
        
        let saveBtn = UIButton().then { obj in
            bottomView.addSubview(obj)

            obj.backgroundColor = .white
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 1
            obj.layer.borderColor = lineColor15.cgColor
            obj.setTitle("保存草稿", for: .normal)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.titleLabel?.font = kRegularFont(11)
            obj.addTarget(self, action: #selector(saveClick), for: .touchUpInside)

            obj.snp.makeConstraints { make in
                make.right.equalTo(addBtn.snp.left).offset(-10)
                make.bottom.equalToSuperview().offset(-30)
                make.width.equalTo(69)
                make.height.equalTo(30)
            }
        }
    }
    
    @objc func saveClick(){
        self.popup.bottomSheet { [weak self] in
            let showView = ScheduleSaveShowView()
            showView.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
            showView.didSelecctedlBlock = { [weak self] index in
                self?.popup.dismissPopup()
            }
            return showView
        }
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }

}

extension ScheduleViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 120
        }else if indexPath.row == 3{
            return 45
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 5{
//            self.presenter?.router?.pushToMySecheduleDateVC(from: self)
        }else if indexPath.row == 8{
            self.presenter?.router?.pushToRemindVC(from: self)
        }else if indexPath.row == 9{
            self.presenter?.router?.pushToRepeatVC(from: self)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleInputTableViewCell", for: indexPath) as! ScheduleInputTableViewCell
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleDateTableViewCell", for: indexPath) as! ScheduleDateTableViewCell
            return cell
        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleNameTableViewCell", for: indexPath) as! ScheduleNameTableViewCell
            return cell
        }
        else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleMoreTableViewCell", for: indexPath) as! ScheduleMoreTableViewCell
            return cell
        }
        else if indexPath.row == 4{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleAllDayTableViewCell", for: indexPath) as! ScheduleAllDayTableViewCell
            return cell
        }
        else if indexPath.row == 5{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleStarTimeTableViewCell", for: indexPath) as! ScheduleStarTimeTableViewCell
            return cell
        }
        else if indexPath.row == 6{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleEndTimeTableViewCell", for: indexPath) as! ScheduleEndTimeTableViewCell
            return cell
        }
        else if indexPath.row == 7{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleSecretTableViewCell", for: indexPath) as! ScheduleSecretTableViewCell
            return cell
        }
        else if indexPath.row == 8{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleRemindTableViewCell", for: indexPath) as! ScheduleRemindTableViewCell
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduleLoopTableViewCell", for: indexPath) as! ScheduleLoopTableViewCell
        return cell
    }
}
