//
//  SecheduleInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/10.
//

import UIKit

class SecheduleInteractor: SecheduleInteractorProtocols {
    var presenter: SechedulePresenterProtocols?
    
    var entity: SecheduleEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
}
