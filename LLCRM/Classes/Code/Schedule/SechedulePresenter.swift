//
//  SechedulePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/10.
//

import UIKit

class SechedulePresenter: SechedulePresenterProtocols {
    var view: SecheduleViewProtocols?
    
    var router: SecheduleRouterProtocols?
    
    var interactor: SecheduleInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
