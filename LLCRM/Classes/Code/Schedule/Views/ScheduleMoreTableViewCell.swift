//
//  ScheduleMoreTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit

class ScheduleMoreTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        _ = UILabel().then {obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            obj.text = "查看参与人时间"
            obj.textColor = workTagColor
            
            obj.snp.makeConstraints { make in
                make.center.equalToSuperview()
            }
        }
    }
}
