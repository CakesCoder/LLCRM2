//
//  ScheduleSaveShowView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/11.
//

import UIKit

class ScheduleSaveShowView: UIView {
    var didSelecctedlBlock: ((Int) -> ())?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        
        _ = UIView().then { obj in
            self.addSubview(obj)
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(230)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "是否保存为草稿"
            obj.font = kAMediumFont(14)
            obj.textColor = blacktextColor34
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(15)
                make.centerX.equalToSuperview()
            }
        }
        
        
        let subLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.text = "图片/附件仅暂存三天，过期后请重新提交"
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor35
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(titleLabel.snp.bottom).offset(5)
                make.centerX.equalToSuperview()
            }
        }
        
        let noSaveView = UIView().then { obj in
            bottomView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(subLabel.snp.bottom).offset(11)
                make.height.equalTo(40)
            }
        }
        
        _ = UIView().then({ obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = lineColor7
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(subLabel.snp.bottom).offset(10)
                make.height.equalTo(1)
            }
        })
        
        let noSaveLabel = UILabel().then { obj in
            noSaveView.addSubview(obj)
            obj.textColor = blackTextColor36
            obj.text = "不保存"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.center.equalTo(noSaveView)
            }
        }
        
        let noSaveButton = UIButton().then { obj in
            noSaveView.addSubview(obj)
            obj.tag = 1000
            obj.addTarget(self, action: #selector(didSelectedClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UIView().then({ obj in
            noSaveView.addSubview(obj)
            obj.backgroundColor = lineColor7
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(1)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        let saveView = UIView().then { obj in
            bottomView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(noSaveView.snp.bottom).offset(0)
                make.height.equalTo(40)
            }
        }
        
        let saveLabel = UILabel().then { obj in
            saveView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "保存"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.center.equalTo(saveView)
            }
        }
        
        let saveButton = UIButton().then { obj in
            saveView.addSubview(obj)
            obj.tag = 1001
            obj.addTarget(self, action: #selector(didSelectedClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
        
        _ = UIView().then({ obj in
            saveView.addSubview(obj)
            obj.backgroundColor = lineColor7
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(1)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        let cancelView = UIView().then { obj in
            bottomView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(saveView.snp.bottom).offset(0)
                make.height.equalTo(40)
            }
        }
        
        let cancelLabel = UILabel().then { obj in
            cancelView.addSubview(obj)
            obj.textColor = blackTextColor13
            obj.text = "取消"
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.center.equalTo(cancelView)
            }
        }
        
        let cancelButton = UIButton().then { obj in
            saveView.addSubview(obj)
            obj.tag = 1002
            obj.addTarget(self, action: #selector(didSelectedClick(_:)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
            }
        }
    }
    
    @objc func didSelectedClick(_ button:UIButton){
        self.didSelecctedlBlock?(button.tag - 1000)
    }
    
}
