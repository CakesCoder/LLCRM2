//
//  ScheduleEndTimeTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit

class ScheduleEndTimeTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let nameLabel = UILabel().then {obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            
            let attri = NSMutableAttributedString(string:"*")
            attri.yy_color = blackTextColor27
            
            let titleText = "结束时间"
            let userAttri = NSMutableAttributedString(string: titleText)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kMediumFont(14)
            attri.append(userAttri)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
            }
        }
        
        let dateContentLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor33
            obj.font = kMediumFont(14)
            obj.text = "2022年05月05日 星期四 18：30"
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel.snp.right).offset(45)
                make.centerY.equalTo(nameLabel)
            }
        }
        
        let imageV = UIImageView().then { obj in
            contentView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "me_push")
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
//                make.width.equalTo(10)
//                make.height.equalTo(13)
            }
        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(1)
            }
        }
    }
}
