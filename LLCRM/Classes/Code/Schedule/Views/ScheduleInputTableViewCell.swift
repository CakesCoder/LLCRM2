//
//  ScheduleInputTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/9.
//

import UIKit
import YYText_swift

class ScheduleInputTableViewCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let nameLabel = UILabel().then {obj in
            contentView.addSubview(obj)
            obj.font = kAMediumFont(14)
            
            let attri = NSMutableAttributedString(string:"*")
            attri.yy_color = blackTextColor27
            
            let titleText = "日程内容"
            let userAttri = NSMutableAttributedString(string: titleText)
            userAttri.yy_color = blackTextColor3
            userAttri.yy_font = kMediumFont(14)
            attri.append(userAttri)
            
            obj.attributedText = attri
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
            }
        }
        
        let inputView = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor7
            obj.layer.cornerRadius = 4
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = lineColor7.cgColor
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(nameLabel.snp.bottom).offset(10)
                make.height.equalTo(87)
            }
        }
        
        let textView = YYTextView().then { obj in
            inputView.addSubview(obj)
            obj.placeholderText = "请输入"
            obj.placeholderTextColor = blackTextColor3
            obj.placeholderFont = kRegularFont(14)
//            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
    }

}
