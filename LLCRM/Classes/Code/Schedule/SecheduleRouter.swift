//
//  SecheduleRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/10.
//

import UIKit

class SecheduleRouter: SecheduleRouterProtocols {
    func pushToMySecheduleDateVC(from previousView: UIViewController) {
        let vc = MyScheduleDateViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToRemindVC(from previousView: UIViewController){
        let vc = RemindViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
    
    func pushToRepeatVC(from previousView: UIViewController){
        let vc = RepeatViewController()
        previousView.navigationController?.pushViewController(vc, animated: true)
    }
}
