//
//  AddSampleViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/13.
//

import UIKit
import PKHUD
import HandyJSON
import DNSPageView

extension AddSampleViewController: PageEventHandleable {
    
    func titleViewDidSelectSameTitle() {
        print("重复点击了标题，index：\(index)")
    }

    // 当前 controller 滑动结束的时候调用
    func contentViewDidEndScroll() {
        print("contentView滑动结束，index：\(index)")
    }
    
    func contentViewDidDisappear() {
        print("我消失了，index：\(index)")
    }
}


//extension AddSampleViewController: UITableViewDelegate, UITableViewDataSource{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 0
//    }
//
//    internal func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
////        return nil
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 0.1
//    }
//
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        index = Int(round(scrollView.contentOffset.x / scrollView.frame.width))
//    }
//}


class AddSampleViewController: BaseViewController,MyDateViewDelegate {
    
//    [["*","客户名称："],["*","选型原因："],["*","需求优先级别："],["*","物料编码："],["物料名称："],["制造商料号： "],["规格描述："],["*","需求日期："],["*","需求数量："],["*","跟随人："],["*","单位："],["*","物料品牌："]]
    var okStudyButton: UIButton?
    var noStudyButton: UIButton?
    
    var okCertificationButton:UIButton?
    var noCertificationButton:UIButton?
    
    var okEnvironmentalButton:UIButton?
    var noEnvironmentalButton:UIButton?
    
    var okAskButton:UIButton?
    var noAskButton:UIButton?
    
    var pictureView: UIView?
    var addbutton:UIButton?
    var addHintLabel:UILabel?
    
    var customerName:String?
    var materialApplyCause:String?
    
    var demandPriority:String?
    var sampleNumber:String?
    var materialName:String?
    var manufacturerModel:String?
    var specs:String?
    var demandDate:String?
    var demandQuantity:String?
    var unit:String?
    var materialBrand:String?
    var technicalSpecifications:String?
    
    var name:String?
    var code:String?
    var lifeCycle:String?
    var startDate:String?
    var endDate:String?
    var batchProductionDate:String?
    var timeUnit:String?
    var angle:String?
    var angleValue:String?
    var targetPrice:String?
    var taxRate:String?
    
//   [["认证要求："],["认证类型："],["认证机构："],["性质："],["备注："],["环保要求："],["要求类型："],["环保类型："]]
    var authType:String?
    var authMechanism:String?
    var nature:String?
    var remark:String?
    var environmentProtectionRequirement:String?
    var askType:Bool? = true
    var environmentProtectionType:Bool? = true
    
//  ["收件人姓名："],["收件人电话："],["收件人地址："]
    var recipientName:String?
    var recipientPhone:String?
    var recipientAddress:String?
    
    var specsConfirm:String?
    var specsName:String?
    
    var presenter: AddSamplePresenterProtocols?
    
    var clientPersonTextFiled:UITextField?
    var clientModel:MyClientModelData?
    
    var leaveTextFiled:UITextField?
    var leaveModel:AddClientSelectedModel?
    
    var timeTextFiled:UITextField?
    var startDateTextFiled:UITextField?
    var endDateTextFiled:UITextField?
    var batchProductionDateTextFiled:UITextField?
    var companyDateView: MyDateView!
    
    
    var timeType:Int? = 0
    var index: Int? = 0
    
    var reasonModel:AddClientSelectedModel?
    var reasonTextFiled:UITextField?
    var materialsNameTextFiled:UITextField?
    var materialsNameModel: AddClientSelecteArchivesModel?
    var materialsCodeTextFiled:UITextField?
    var materialsCodeModel: AddClientSelecteArchivesModel?
    
    
    var dosageTextFiled:UITextField?
    var dosageModel:AddClientSelectedModel?
    var angleTextFiled:UITextField?
    var angleModel:AddClientSelectedModel?
    var unitTextFiled:UITextField?
    var unitModel:AddClientSelectedModel?
    var unitTextFiled2:UITextField?
    var unitModel2:AddClientSelectedModel?
    var technologyTextFiled:UITextField?
    var technologyModel:AddClientSelectedTechnologyModel?
    
    var rateModel:AddClientSelectedModel?
    var archivesTextFiled:UITextField?
    
    var addClientSelectedBussinessNameModel:AddClientSelectedBussinessNameModel?
    
    var linkManModel: LinkManModel?
    var linkManeTextFiled:UITextField?
    
    var certificationAskTypeModel: AddClientSelectedDepartMentModel?
    var certificationAskTypeTextFiled:UITextField?
    
    var certificationAskBodyModel: AddClientSelectedDepartMentModel?
    var certificationAskBodyTextFiled:UITextField?
    
    var certificationAskNatureModel: AddClientSelectedModel?
    var certificationAskNatureTextFiled:UITextField?
    
    var environmentalTypeModel: AddClientSelectedDepartMentModel?
    var environmentalTypeTextFiled:UITextField?
    
    var productMoneyTextFiled:UITextField?
    var isGoalMoney:DarwinBoolean? = true

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        view.backgroundColor = lineColor
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        
        if index == 0{
            let baseInfos = [["*","客户名称："],["*","选型原因："],["*","需求优先级别："],["*","物料编码："],["物料名称："],["制造商料号： "],["规格描述："],["*","需求日期："],["*","需求数量："],["*","跟随人："],["*","单位："],["*","物料品牌："]]


            let contentInfos = ["请选择客户", "请选择原因", "请选择级别", "请选择品类", "从产品主数据选择", "请输入", "请输入", "请选择", "请输入", "请输入", "请选择", "请输入"]


            let baseInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45+baseInfos.count*45)
                }
            }

            for i in 0..<baseInfos.count{
                let v = UIView().then { obj in
                    baseInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }

                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<baseInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                        if baseInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }

                if i < 5 || i == 10{
                    let tagImageV = UIImageView().then { obj in
                        v.addSubview(obj)
                        obj.image = UIImage(named: "me_push")
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-15)
                            make.centerY.equalToSuperview()
                        }
                    }
                }

                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = contentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    obj.delegate = self
                    obj.textAlignment = .right
                    if i == 0{
                        obj.isEnabled = false
                        clientPersonTextFiled = obj
                    }else if i == 1{
                        obj.isEnabled = false
                        reasonTextFiled = obj
                    }else if i == 2{
                        obj.isEnabled = false
                        leaveTextFiled = obj
                    }else if i == 3{
                        obj.isEnabled = false
                        materialsCodeTextFiled = obj
                    }else if i == 4{
                        obj.isEnabled = false
                        materialsNameTextFiled = obj
                    }else if i == 7{
                        obj.isEnabled = false
                        timeTextFiled = obj
                    }else if i == 10{
                        obj.isEnabled = false
                        unitTextFiled2 = obj
                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }

                if i == 0 || i == 1 || i == 2 || i == 3 || i == 4 || i == 7 || i == 10{
                    let button = UIButton().then { obj in
                        v.addSubview(obj)
                        obj.tag = 1000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.right.bottom.equalToSuperview().offset(-0)
                        }
                    }
                }
//                else if i == 7{
//                    let button = UIButton().then { obj in
//                        v.addSubview(obj)
//                        obj.addTarget(self, action: #selector(timeClick), for: .touchUpInside)
//                        obj.snp.makeConstraints { make in
//                            make.left.top.equalToSuperview().offset(0)
//                            make.right.bottom.equalToSuperview().offset(-0)
//                        }
//                    }
//                }

                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
        }else if index == 1{
            
            let skillInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let skillInfos = [["技术规格"],
//                              ["规格参数"],
    //                          ["规格参数"]
            ]

            let skillcontentInfos = ["可选",
//                                     "请输入",
    //                                 "请输入"
            ]

//            let skillHeadView = UIView().then { obj in
//                skillInfosView.addSubview(obj)
//                obj.backgroundColor = .white
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(0)
//                    make.right.equalToSuperview().offset(-0)
//                    make.top.equalToSuperview().offset(0)
//                    make.height.equalTo(45)
//                }
//            }
//
//            let skillheadTagView = UIView().then { obj in
//                skillHeadView.addSubview(obj)
//                obj.backgroundColor = workTagColor
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.centerY.equalToSuperview()
//                    make.width.equalTo(3)
//                    make.height.equalTo(11)
//                }
//            }
//
//            let skillheadLabel = UILabel().then { obj in
//                skillHeadView.addSubview(obj)
//                obj.text = "技术规格"
//                obj.textColor = blackTextColor
//                obj.font = kMediumFont(15)
//                obj.snp.makeConstraints { make in
//                    make.left.equalTo(skillheadTagView.snp.right).offset(5)
//                    make.centerY.equalToSuperview()
//                }
//            }

            for i in 0..<skillInfos.count{
                let v = UIView().then { obj in
                    skillInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }

                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<skillInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:skillInfos[i][j])
                        if skillInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }

                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = skillcontentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 2000+i
                    obj.delegate = self
                    obj.isEnabled = false
                    technologyTextFiled = obj
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }
                
                let button = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.tag = 2000
                    obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.left.top.equalToSuperview().offset(0)
                        make.right.bottom.equalToSuperview().offset(-0)
                    }
                }
                
                
                let tagImageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-15)
                        make.centerY.equalToSuperview()
                    }
                }

                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
            
        }else if index == 2{
            
            let productInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(13*45)
                }
            }
            
            let productInfos = [["项目名称："],["项目代号："],["项目周期："],["项目启动日期："],["项目结束日期："],["项目量产日期："],["用量："],["角度："],["数量："],["单位："],["目标价格："],["目标价格："],["税率："]]

            let productcontentInfos = ["请输入","请输入","请输入","请输入","请输入","请输入","请选择","请选择","请输入","请选择","请输入","请输入","请选择",]

//            let productHeadView = UIView().then { obj in
//                productInfosView.addSubview(obj)
//                obj.backgroundColor = .white
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(0)
//                    make.right.equalToSuperview().offset(-0)
//                    make.top.equalToSuperview().offset(0)
//                    make.height.equalTo(45)
//                }
//            }
//
//            let productheadTagView = UIView().then { obj in
//                productHeadView.addSubview(obj)
//                obj.backgroundColor = workTagColor
//                obj.snp.makeConstraints { make in
//                    make.left.equalToSuperview().offset(15)
//                    make.centerY.equalToSuperview()
//                    make.width.equalTo(3)
//                    make.height.equalTo(11)
//                }
//            }
//
//            let productheadLabel = UILabel().then { obj in
//                productHeadView.addSubview(obj)
//                obj.text = "项目信息"
//                obj.textColor = blackTextColor
//                obj.font = kMediumFont(15)
//                obj.snp.makeConstraints { make in
//                    make.left.equalTo(productheadTagView.snp.right).offset(5)
//                    make.centerY.equalToSuperview()
//                }
//            }

            for i in 0..<productInfos.count{
                let v = UIView().then { obj in
                    productInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }

                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<productInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:productInfos[i][j])
                        if productInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }

                if i == 10{
                    okStudyButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = bgColor17
                        obj.addTarget(self, action: #selector(okStudyClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(hintLabel.snp.right).offset(0)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let okStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        obj.text = "有"
                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87
                        obj.snp.makeConstraints { make in
                            make.left.equalTo((okStudyButton?.snp.right)!).offset(5)
                            make.centerY.equalToSuperview()
                        }
                    })

                    noStudyButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = .white
                        obj.layer.borderWidth = 0.5
                        obj.layer.borderColor = lineColor28.cgColor
                        obj.addTarget(self, action: #selector(noStudyClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(okStudyLabel.snp.right).offset(5)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let noStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        obj.text = "无"
                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87
                        obj.snp.makeConstraints { make in
                            make.left.equalTo((noStudyButton?.snp.right)!).offset(10)
                            make.centerY.equalToSuperview()
                        }
                    })

                }else{
                    let textFiled = UITextField().then { obj in
                        v.addSubview(obj)
                        obj.placeholder = productcontentInfos[i]
                        obj.font = kMediumFont(12)
                        obj.tag = 3000+i
                        obj.delegate = self
                        if i == 3{
                            obj.isEnabled = false
                            startDateTextFiled = obj
                        }else if i == 4{
                            obj.isEnabled = false
                            endDateTextFiled = obj
                        }else if i == 5{
                            obj.isEnabled = false
                            batchProductionDateTextFiled = obj
                        }else if i == 6{
                            obj.isEnabled = false
                            dosageTextFiled = obj
                        }else if i == 7{
                            obj.isEnabled = false
                            angleTextFiled = obj
                        }else if i == 9{
                            obj.isEnabled = false
                            unitTextFiled = obj
                        }else if i == 11{
                            productMoneyTextFiled = obj
                        }else if i == 12{
                            obj.isEnabled = false
                            archivesTextFiled = obj
                        }
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-45)
                            make.centerY.equalToSuperview()
                        }
                    }

                    if i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 9 || i == 12{
                        let button = UIButton().then { obj in
                            v.addSubview(obj)
                            obj.tag = 3000+i
                            obj.addTarget(self, action: #selector(timeClick2(_ :)), for: .touchUpInside)
                            obj.snp.makeConstraints { make in
                                make.left.top.equalToSuperview().offset(0)
                                make.right.bottom.equalToSuperview().offset(-0)
                            }
                        }
                    }
                }
                
                if i == 3 || i == 4 || i == 5 || i == 6 || i == 7 || i == 9{
                    let tagImageV = UIImageView().then { obj in
                        v.addSubview(obj)
                        obj.image = UIImage(named: "me_push")
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-15)
                            make.centerY.equalToSuperview()
                        }
                    }
                }
                
                
                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
        }else if index == 3{
            let environmentInfosView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
//                    make.top.equalTo(productInfosView.snp.bottom).offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(9*45)
                }
            }

            let environmentInfos = [["认证要求："],["认证类型："],["认证机构："],["性质："],["备注："],["环保要求："],["要求类型："],["环保类型："]]

            let environmentcontentInfos = ["请选择","请选择","请选择","请选择","请输入","请输入","请输入","请选择"]

            for i in 0..<environmentInfos.count{
                let v = UIView().then { obj in
                    environmentInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }

                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<environmentInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:environmentInfos[i][j])
                        if environmentInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 1 || i == 2 || i == 3 || i == 7{
                    let tagImageV = UIImageView().then { obj in
                        v.addSubview(obj)
                        obj.image = UIImage(named: "me_push")
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-15)
                            make.centerY.equalToSuperview()
                        }
                    }
                }

                if i == 0{
                    okCertificationButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = bgColor17
                        obj.addTarget(self, action: #selector(okCertificationClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(hintLabel.snp.right).offset(0)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let okStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        if i == 0 || i == 5{
                            obj.text = "需要"
                        }else{
                            obj.text = "必要"
                        }

                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87

                        obj.snp.makeConstraints { make in
                            make.left.equalTo((okCertificationButton?.snp.right)!).offset(5)
                            make.centerY.equalToSuperview()
                        }
                    })

                    noCertificationButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = .white
                        obj.layer.borderWidth = 0.5
                        obj.layer.borderColor = lineColor28.cgColor
                        obj.addTarget(self, action: #selector(noCertificationClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(okStudyLabel.snp.right).offset(5)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let noStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        if i == 0 || i == 5{
                            obj.text = "不需要"
                        }else{
                            obj.text = "参考"
                        }
                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(noCertificationButton!.snp.right).offset(10)
                            make.centerY.equalToSuperview()
                        }
                    })

                }else if i == 5{
                    okEnvironmentalButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = bgColor17
                        obj.addTarget(self, action: #selector(okEnvironmentalClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(hintLabel.snp.right).offset(0)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let okStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        if i == 0 || i == 5{
                            obj.text = "需要"
                        }else{
                            obj.text = "必要"
                        }

                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87

                        obj.snp.makeConstraints { make in
                            make.left.equalTo((okCertificationButton?.snp.right)!).offset(5)
                            make.centerY.equalToSuperview()
                        }
                    })

                    noEnvironmentalButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = .white
                        obj.layer.borderWidth = 0.5
                        obj.layer.borderColor = lineColor28.cgColor
                        obj.addTarget(self, action: #selector(noEnvironmentalClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(okStudyLabel.snp.right).offset(5)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let noStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        if i == 0 || i == 5{
                            obj.text = "不需要"
                        }else{
                            obj.text = "参考"
                        }
                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(noCertificationButton!.snp.right).offset(10)
                            make.centerY.equalToSuperview()
                        }
                    })
                }else if i == 6{
                    okAskButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = bgColor17
                        obj.addTarget(self, action: #selector(okAskClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(hintLabel.snp.right).offset(0)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let okStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        if i == 0 || i == 5{
                            obj.text = "需要"
                        }else{
                            obj.text = "必要"
                        }

                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87

                        obj.snp.makeConstraints { make in
                            make.left.equalTo((okCertificationButton?.snp.right)!).offset(5)
                            make.centerY.equalToSuperview()
                        }
                    })

                    noAskButton = UIButton().then({ obj in
                        v.addSubview(obj)
                        obj.layer.cornerRadius = 7.5
                        obj.backgroundColor = .white
                        obj.layer.borderWidth = 0.5
                        obj.layer.borderColor = lineColor28.cgColor
                        obj.addTarget(self, action: #selector(noAskClick), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(okStudyLabel.snp.right).offset(5)
                            make.centerY.equalToSuperview()
                            make.width.height.equalTo(15)
                        }
                    })

                    let noStudyLabel = UILabel().then({ obj in
                        v.addSubview(obj)
                        if i == 0 || i == 5{
                            obj.text = "不需要"
                        }else{
                            obj.text = "参考"
                        }
                        obj.font = kMediumFont(14)
                        obj.textColor = blackTextColor87
                        obj.snp.makeConstraints { make in
                            make.left.equalTo(noCertificationButton!.snp.right).offset(10)
                            make.centerY.equalToSuperview()
                        }
                    })
                    
                }else{
                    let textFiled = UITextField().then { obj in
                        v.addSubview(obj)
                        obj.placeholder = environmentcontentInfos[i]
                        obj.font = kMediumFont(12)
                        obj.tag = 4000+i
                        obj.delegate = self
                        if i == 1{
                            obj.isEnabled = false
                            certificationAskTypeTextFiled = obj
                        }else if i == 2{
                            obj.isEnabled = false
                            certificationAskBodyTextFiled = obj
                        }else if i == 3{
                            obj.isEnabled = false
                            certificationAskNatureTextFiled = obj
                        }else if i == 7{
                            obj.isEnabled = false
                            environmentalTypeTextFiled = obj
                        }
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-45)
                            make.centerY.equalToSuperview()
                        }
                    }
                    
                    if i == 1 || i == 2 || i == 3 || i == 7 {
                        let button = UIButton().then { obj in
                            v.addSubview(obj)
                            obj.tag = 3000+i
                            obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                            obj.snp.makeConstraints { make in
                                make.left.top.equalToSuperview().offset(0)
                                make.right.bottom.equalToSuperview().offset(-0)
                            }
                        }
                    }
                }

                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }
        }else{
            let linkManInfosView = UIView().then { obj in
                headView.addSubview(obj)
//                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
//                    make.top.equalTo(environmentInfosView.snp.bottom).offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(3*45)
                }
            }

            let linkManInfos = [["收件人姓名："],["收件人电话："],["收件人地址："]]

            let linkMancontentInfos = ["请输入","请输入","请输入"]

            for i in 0..<linkManInfos.count{
                let v = UIView().then { obj in
                    linkManInfosView.addSubview(obj)
                    obj.backgroundColor = .white
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(0)
                        make.top.equalToSuperview().offset(i*45)
                        make.right.equalToSuperview().offset(-0)
                        make.height.equalTo(45)
                    }
                }
                
                if i == 0{
                    let tagImageV = UIImageView().then { obj in
                        v.addSubview(obj)
                        obj.image = UIImage(named: "me_push")
                        obj.snp.makeConstraints { make in
                            make.right.equalToSuperview().offset(-15)
                            make.centerY.equalToSuperview()
                        }
                    }
                }


                let hintLabel = UILabel().then { obj in
                    v.addSubview(obj)
                    var temp_hint = NSMutableAttributedString(string: "")
                    for j in 0..<linkManInfos[i].count{
                        let hintAttribute = NSMutableAttributedString(string:linkManInfos[i][j])
                        if linkManInfos[i].count == 1{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            obj.attributedText = hintAttribute
                        }else{
                            if j == 0{
                                hintAttribute.yy_color = blackTextColor27
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint = hintAttribute
                            }else{
                                hintAttribute.yy_color = blackTextColor3
                                hintAttribute.yy_font = kMediumFont(12)
                                temp_hint.append(hintAttribute)
                                obj.attributedText = temp_hint
                            }
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.centerY.equalToSuperview()
                    }
                }

                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = linkMancontentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 5000+i
                    obj.delegate = self
                    if i == 0{
                        obj.isEnabled = false
                        linkManeTextFiled = obj
                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-45)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 0{
                    let button = UIButton().then { obj in
                        v.addSubview(obj)
                        obj.tag = 5000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.right.bottom.equalToSuperview().offset(-0)
                        }
                    }
                }

                _ = UIView().then({ obj in
                    v.addSubview(obj)
                    obj.backgroundColor = lineColor5
                    obj.snp.makeConstraints { make in
                        make.left.equalToSuperview().offset(15)
                        make.right.equalToSuperview().offset(-15)
                        make.bottom.equalToSuperview().offset(-0)
                        make.height.equalTo(0.5)
                    }
                })
            }

            addHintLabel = UILabel().then({ obj in
                headView.addSubview(obj)
                obj.text = "上传照片"
                obj.font = kMediumFont(12)
                obj.textColor = blackTextColor3
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalTo(linkManInfosView.snp.bottom).offset(10)
                }
            })

            let w = (kScreenWidth-30)/4
            pictureView = UIView().then { obj in
                headView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.top.equalTo(addHintLabel!.snp.bottom).offset(5)
                    make.right.equalToSuperview().offset(-15)
                    make.height.equalTo(w)
                }
            }
            
            addbutton = UIButton().then { obj in
                pictureView?.addSubview(obj)
                obj.setTitle("+", for: .normal)
                obj.titleLabel?.font = kMediumFont(25)
                obj.setTitleColor(.black, for: .normal)
                obj.layer.cornerRadius = 3
                obj.layer.borderWidth = 1
                obj.layer.borderColor = lineColor23.cgColor
                obj.frame = CGRect(x: 0, y: 0, width: w, height: w)
                obj.addTarget(self, action:#selector(addPictureClick(_ :)), for: .touchUpInside)
            }
        }

        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        })

        let bottomView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight())
            }
        }

        let saveButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.setTitle("保存", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 4
            obj.addTarget(self, action: #selector(saveClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(15)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }

        cofing()

        // 日期选择
        companyDateView = MyDateView.init()
        //myDateView.isShowDay = false
        companyDateView.delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedClientLinkManAction(_:)), name: Notification.Name(rawValue: "ClientListDetailNotificationKey"), object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func didSelectedClientLinkManAction(_ notification:NSNotification){
        printTest(notification)
        if let notifiModel = notification.userInfo{
            linkManModel = notifiModel["ClietLineManModel"] as? LinkManModel
            linkManeTextFiled?.text = linkManModel!.name!
        }
    }
    
    
    
    @objc func addPictureClick(_ button:UIButton){
//        self.pictureBlcok!()
    }
    
    @objc func timeClick(){
        timeType = 0
        companyDateView.showView()
    }
    
    @objc func timeClick2(_ button:UIButton){
        if button.tag == 3003{
            timeType = 1
            companyDateView.showView()
        }else if button.tag == 3004{
            timeType = 2
            companyDateView.showView()
        }else if button.tag == 3005{
            timeType = 3
            companyDateView.showView()
        }else if button.tag == 3006{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 29
            vc.addClientDataBlock = {[self] model in
                reasonModel = model
                dosageTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3007{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 30
            vc.addClientDataBlock = {[self] model in
                angleModel = model
                angleTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3009{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 31
            vc.addClientDataBlock = {[self] model in
                unitModel = model
                unitTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3012{
            if isGoalMoney == true{
                let vc = AddClientSelectedViewController()
                vc.clientSelectConfig = 7
                vc.addClientDataBlock = {[self] model in
                    rateModel = model
                    archivesTextFiled?.text =  model.attrName
                }
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
    
    func pickDateView(year: Int, month: Int, day: Int) {
        if timeType == 0{
            timeTextFiled?.text =  "\(year)年\(month)月\(day)日 00:00:00"
            demandDate =  "\(year)年\(month)月\(day)日 00:00:00"
        }else if timeType == 1{
            startDate = "\(year)年\(month)月\(day)日 00:00:00"
            startDateTextFiled?.text = "\(year)年\(month)月\(day)日 00:00:00"
        }else if timeType == 2{
            endDate = "\(year)年\(month)月\(day)日 00:00:00"
            endDateTextFiled?.text = "\(year)年\(month)月\(day)日 00:00:00"
        }else if timeType == 3{
            batchProductionDate = "\(year)年\(month)月\(day)日 00:00:00"
            batchProductionDateTextFiled?.text = "\(year)年\(month)月\(day)日 00:00:00"
        }
    }
    
    @objc func saveClick(){
        presenter?.presenterRequestGetAddSample(by: ["materialName":materialName, "specs":specs, "demandQuantity":demandQuantity, "deliveryTime":demandDate, "unit":unit, "materialBrand":materialBrand, "technicalSpecifications":technicalSpecifications, "sampleNumber":sampleNumber,"cusProject":["name":name, "code":code,"lifeCycle":lifeCycle,"startDate":startDate,"endDate":endDate,"batchProductionDate":batchProductionDate,"timeUnit":timeUnit,"angle":angle,"targetPrice":targetPrice,"taxRate":taxRate],"authType":authType,"authMechanism":authMechanism,"nature":nature,"remark":remark,"askType":askType == true ? 1: 0, "environmentProtectionType":environmentProtectionType == true ? 1 : 0,"recipientName":recipientName,"recipientPhone":recipientPhone,"recipientAddress":recipientAddress])
    }
    
    @objc func addPersonClick(_ button:UIButton){
        if button.tag == 2000{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 28
            vc.addClientSelectedTechnologyBlock = {[self] model in
                technologyModel = model
                technologyTextFiled?.text = model.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1000{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 10
            vc.addClientSelectedBussinessNameBlock = {[self] model in
                addClientSelectedBussinessNameModel = model
                clientPersonTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1001{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 18
            vc.addClientDataBlock = {[self] model in
                reasonModel = model
                reasonTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1002{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 19
            vc.addClientDataBlock = {[self] model in
                leaveModel = model
                leaveTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1003{
            //物料
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 33
            vc.addClientSelectedArchivesBlock = {[self] model in
                materialsCodeModel = model
                materialsCodeTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1004{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 20
            vc.addClientSelectedArchivesBlock = {[self] model in
                materialsNameModel = model
                materialsNameTextFiled?.text = model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1007{
            timeType = 0
            companyDateView.showView()
        }else if button.tag == 1010{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 31
            vc.addClientDataBlock = {[self] model in
                unitModel2 = model
                unitTextFiled2?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3001{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 36
            vc.addClientSelectedDepartMentBlock = {[self] model in
                certificationAskTypeModel = model
                certificationAskTypeTextFiled?.text = model.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3002{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 35
            vc.addClientSelectedDepartMentBlock = {[self] model in
                certificationAskBodyModel = model
                certificationAskBodyTextFiled?.text = model.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3003{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 37
            vc.addClientDataBlock = {[self] model in
                certificationAskNatureModel = model
                certificationAskNatureTextFiled?.text = model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 3007{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 34
            vc.addClientSelectedDepartMentBlock = {[self] model in
                environmentalTypeModel = model
                environmentalTypeTextFiled?.text = model.name
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 5000{
            let vc = LinkManViewController()
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func cofing(){
        let router = AdddSampleRouter()
        
        presenter = AddSamplePresenter()
        
        presenter?.router = router
        
        let entity = AddSampleEntity()
        
        let interactor = AddSampleInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
//        presenter?.presenterRequestGetSampleList(by: ["current":current, "size":10])
    }
    
    @objc func okStudyClick(){
        
        okStudyButton?.backgroundColor = bgColor17
        
        noStudyButton?.backgroundColor = .white
        noStudyButton?.layer.borderWidth = 0.5
        noStudyButton?.layer.borderColor = lineColor28.cgColor
        
        productMoneyTextFiled?.text = ""
        productMoneyTextFiled?.placeholder = "请输入"
        productMoneyTextFiled?.isEnabled = true
        
        archivesTextFiled?.text = ""
        archivesTextFiled?.placeholder = "请选择"
        
        isGoalMoney = true
    }
    
    @objc func noStudyClick(){
        noStudyButton?.backgroundColor = bgColor17
        
        okStudyButton?.backgroundColor = .white
        okStudyButton?.layer.borderWidth = 0.5
        okStudyButton?.layer.borderColor = lineColor28.cgColor
        
        productMoneyTextFiled?.text = ""
        productMoneyTextFiled?.placeholder = "系统默认"
        productMoneyTextFiled?.isEnabled = false
        
        archivesTextFiled?.text = ""
        archivesTextFiled?.placeholder = "系统默认"
        
        isGoalMoney = false
    }
    
    @objc func okCertificationClick(){
        okCertificationButton?.backgroundColor = bgColor17
        
        noCertificationButton?.backgroundColor = .white
        noCertificationButton?.layer.borderWidth = 0.5
        noCertificationButton?.layer.borderColor = lineColor28.cgColor
    }
    
    @objc func noCertificationClick(){
        noCertificationButton?.backgroundColor = bgColor17
        
        okCertificationButton?.backgroundColor = .white
        okCertificationButton?.layer.borderWidth = 0.5
        okCertificationButton?.layer.borderColor = lineColor28.cgColor
    }
    
    @objc func okEnvironmentalClick(){
        okEnvironmentalButton?.backgroundColor = bgColor17
        
        noEnvironmentalButton?.backgroundColor = .white
        noEnvironmentalButton?.layer.borderWidth = 0.5
        noEnvironmentalButton?.layer.borderColor = lineColor28.cgColor
        environmentProtectionType = true
    }
    
    @objc func noEnvironmentalClick(){
        noEnvironmentalButton?.backgroundColor = bgColor17
        
        okEnvironmentalButton?.backgroundColor = .white
        okEnvironmentalButton?.layer.borderWidth = 0.5
        okEnvironmentalButton?.layer.borderColor = lineColor28.cgColor
        environmentProtectionType = false
    }
    
    @objc func okAskClick(){
        okAskButton?.backgroundColor = bgColor17
        
        noAskButton?.backgroundColor = .white
        noAskButton?.layer.borderWidth = 0.5
        noAskButton?.layer.borderColor = lineColor28.cgColor
        askType = true
    }
    
    @objc func noAskClick(){
        noAskButton?.backgroundColor = bgColor17
        
        okAskButton?.backgroundColor = .white
        okAskButton?.layer.borderWidth = 0.5
        okAskButton?.layer.borderColor = lineColor28.cgColor
        askType = false
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddSampleViewController: AdddSampleViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    
    func didGetAddSamplePresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "SampleListNotificationKey"), object: nil, userInfo: nil)
                navigationController?.popViewController(animated: true)
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddSamplePresenterReceiveError(error: MyError?) {
        
    }
}

extension AddSampleViewController: UITextFieldDelegate{
    
    //    [["*","客户名称："],["*","选型原因："],["*","需求优先级别："],["*","物料编码："],["物料名称："],["制造商料号： "],["规格描述："],["*","需求日期："],["*","需求数量："],["*","跟随人："],["*","单位："],["*","物料品牌："]]
//        var okStudyButton: UIButton?
//        var noStudyButton: UIButton?
//        var pictureView: UIView?
//        var addbutton:UIButton?
//        var addHintLabel:UILabel?
//
//        var customerName:String?
//        var demandPriority:String?
//        var materialName:String?
//        var manufacturerModel:String?
//        var specs:String?
//        var demandDate:String?
//        var demandQuantity:String?
//        var unit:String?
//        var lifeCycleUnit:String?
        
    //    [["项目名称："],["项目代号："],["项目周期："],["项目启动日期："],["项目结束日期："],["项目量产日期："],["用量："],["角度："],["数量："],["单位："],["目标价格："],["目标价格："],["税率："]]
//        var name:String?
//        var code:String?
//        var lifeCycle:String?
//        var startDate:String?
//        var endDate:String?
//        var batchProductionDate:String?
//        var angle:String?
//        var targetPrice:String?
//        var taxRate:String?
        
    //   [["认证要求："],["认证类型："],["认证机构："],["性质："],["备注："],["环保要求："],["要求类型："],["环保类型："]]
//        var authType:String?
//        var authMechanism:String?
//        var nature:String?
//        var remark:String?
//        var environmentProtectionRequirement:String?
//        var environmentProtectionType:String?
        
    //  ["收件人姓名："],["收件人电话："],["收件人地址："]
//        var recipientName:String?
//        var recipientPhone:String?
//        var recipientAddress:String?
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
//            customerName = textField.text
        }else if textField.tag == 1001{
            materialApplyCause = textField.text
        }else if textField.tag == 1002{
            demandPriority = textField.text
        }else if textField.tag == 1003{
            sampleNumber = textField.text
        }else if textField.tag == 1004{
            materialName = textField.text
        }else if textField.tag == 1005{
            manufacturerModel = textField.text
        }else if textField.tag == 1006{
            specs = textField.text
        }else if textField.tag == 1007{
            demandDate = textField.text
        }else if textField.tag == 1008{
            demandQuantity = textField.text
        }else if textField.tag == 1009{
            
        }else if textField.tag == 1010{
            unit = textField.text
        }else if textField.tag == 1011{
            materialBrand = textField.text
        }else if textField.tag == 2000{
            technicalSpecifications = textField.text
        }else if textField.tag == 2001{
            specsName = textField.text
        }else if textField.tag == 2002{
            
        }else if textField.tag == 3000{
            name = textField.text
        }else if textField.tag == 3001{
            code = textField.text
        }else if textField.tag == 3002{
            lifeCycle = textField.text
        }else if textField.tag == 3003{
            startDate = textField.text
        }else if textField.tag == 3004{
            endDate = textField.text
        }else if textField.tag == 3005{
            batchProductionDate = textField.text
        }else if textField.tag == 3006{
            timeUnit = textField.text
        }else if textField.tag == 3007{
            angle = textField.text
        }else if textField.tag == 3008{
            angleValue = textField.text
        }else if textField.tag == 3009{
            
        }else if textField.tag == 3010{
            targetPrice = textField.text
        }else if textField.tag == 3011{
            taxRate = textField.text
        }else if textField.tag == 4000{
            
        }else if textField.tag == 4001{
            authType = textField.text
        }else if textField.tag == 4002{
            authMechanism = textField.text
        }else if textField.tag == 4003{
            nature = textField.text
        }else if textField.tag == 4004{
            remark = textField.text
        }else if textField.tag == 4005{
            environmentProtectionRequirement = textField.text
        }else if textField.tag == 4006{
            
        }else if textField.tag == 4007{
//            environmentProtectionType = textField.text
        }else if textField.tag == 5000{
            recipientName = textField.text
        }else if textField.tag == 5001{
            recipientPhone = textField.text
        }else if textField.tag == 5002{
            recipientAddress = textField.text
        }
    }
}
