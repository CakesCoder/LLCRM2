//
//  AddSampleViewProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

protocol AdddSampleViewProtocols: AnyObject {
    var presenter: AddSamplePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddSamplePresenterReceiveData(by params: Any?)
    func didGetAddSamplePresenterReceiveError(error: MyError?)
}

protocol AddSamplePresenterProtocols: AnyObject{
    var view: AdddSampleViewProtocols? { get set }
    var router: AdddSampleRouterProtocols? { get set }
    var interactor: AddSampleInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddSample(by params: Any?)
    func didGetAddSampleInteractorReceiveData(by params: Any?)
    func didGetAddSampleInteractorReceiveError(error: MyError?)
}

protocol AddSampleInteractorProtocols: AnyObject {
    var presenter: AddSamplePresenterProtocols? { get set }
    var entity: AddSampleEntityProtocols? { get set }
    
    func presenterRequestAddSample(by params: Any?)
    func didEntityAddSampleReceiveData(by params: Any?)
    func didEntityAddSampleReceiveError(error: MyError?)
    
    
}

protocol AddSampleEntityProtocols: AnyObject {
    var interactor: AddSampleInteractorProtocols? { get set }
    
    func didAddSampleReceiveData(by params: Any?)
    func didAddSampleReceiveError(error: MyError?)
    func getAddSampleRequest(by params: Any?)
    
    
}

protocol AdddSampleRouterProtocols: AnyObject {
    func pushToAddSample(from previousView: UIViewController)
    func pushToSampleDetail(from previousView: UIViewController)
}
