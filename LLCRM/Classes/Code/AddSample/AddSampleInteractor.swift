//
//  AddSampleInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class AddSampleInteractor: AddSampleInteractorProtocols {
    var presenter: AddSamplePresenterProtocols?
    
    var entity: AddSampleEntityProtocols?
    
    func presenterRequestAddSample(by params: Any?) {
        entity?.getAddSampleRequest(by: params)
    }
    
    func didEntityAddSampleReceiveData(by params: Any?) {
        presenter?.didGetAddSampleInteractorReceiveData(by: params)
    }
    
    func didEntityAddSampleReceiveError(error: MyError?) {
        presenter?.didGetAddSampleInteractorReceiveError(error: error)
    }
    
    
}
