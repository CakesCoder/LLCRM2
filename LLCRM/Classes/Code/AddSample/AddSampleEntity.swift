//
//  AddSampleEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class AddSampleEntity: AddSampleEntityProtocols {
    var interactor: AddSampleInteractorProtocols?
    
    func didAddSampleReceiveData(by params: Any?) {
        interactor?.didEntityAddSampleReceiveData(by: params)
    }
    
    func didAddSampleReceiveError(error: MyError?) {
        interactor?.didEntityAddSampleReceiveError(error: error)
    }
    
    func getAddSampleRequest(by params: Any?) {
        LLNetProvider.request(.sampleAdd(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddSampleReceiveData(by:jsonData)
            case .failure(_):
                self.didAddSampleReceiveError(error: .requestError)
            }
        }
    }
    

}
