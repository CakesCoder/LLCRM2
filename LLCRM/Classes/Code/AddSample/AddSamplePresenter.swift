//
//  AddSamplePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class AddSamplePresenter: AddSamplePresenterProtocols {
    var view: AdddSampleViewProtocols?
    
    var router: AdddSampleRouterProtocols?
    
    var interactor: AddSampleInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddSample(by params: Any?) {
        interactor?.presenterRequestAddSample(by: params)
    }
    
    func didGetAddSampleInteractorReceiveData(by params: Any?) {
        view?.didGetAddSamplePresenterReceiveData(by: params)
    }
    
    func didGetAddSampleInteractorReceiveError(error: MyError?) {
        view?.didGetAddSamplePresenterReceiveError(error: error)
    }
    
    
}
