//
//  RuleWeekView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit

class RuleWeekView: UIView {

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var weeks:[String]? = ["星期一","星期二","星期三","星期四","星期五","星期六","星期日"]
    override init(frame: CGRect) {
        super.init(frame: CGRectMake(0, 0, kScreenWidth, kScreenHeight))
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let v = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(361)
            }
        }
        
        let cancelButton = UIButton().then { obj in
            v.addSubview(obj)
            obj.setTitle("取消", for: .normal)
            obj.titleLabel?.font = kMediumFont(16)
            obj.setTitleColor(blackTextColor8, for: .normal)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(20)
                make.width.equalTo(40)
                make.height.equalTo(22)
            }
        }
        
        let okButton = UIButton().then { obj in
            v.addSubview(obj)
            obj.setTitle("确定", for: .normal)
            obj.titleLabel?.font = kMediumFont(16)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.snp.makeConstraints { make in
                //                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(20)
                make.right.equalToSuperview().offset(-30)
                make.width.equalTo(40)
                make.height.equalTo(22)
            }
        }
        
        _ = UITableView().then({ obj in
            v.addSubview(obj)
            obj.separatorStyle = .none
            obj.register(WeekShowCell.self, forCellReuseIdentifier: "WeekShowCell")
//            obj.backgroundColor = .red
            obj.delegate = self
            obj.dataSource = self
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(56)
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
    }

}

extension RuleWeekView: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return weeks!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeekShowCell", for: indexPath) as! WeekShowCell
        cell.dateLabel?.text = self.weeks![indexPath.row]
        return cell
    }
}

class WeekShowCell :UITableViewCell{

    var dateStr:String?
    var dateLabel:UILabel?{
        didSet{
            dateLabel?.text = dateStr
        }
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        dateLabel = UILabel().then({ obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(13)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.centerY.equalToSuperview()
            }
        })
        
        let button = UIButton().then { obj in
            contentView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.backgroundColor = .red
            obj.isSelected = false
            obj.addTarget(self, action: #selector(touchClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-40)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(20)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
    }
    
    @objc func touchClick(_ button:UIButton){
        button.isSelected = !button.isSelected
        if button.isSelected{
            button.backgroundColor = .blue
        }else{
            button.backgroundColor = .red
        }
    }
}

