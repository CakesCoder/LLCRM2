//
//  RuleViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/21.
//

import UIKit
import JFPopup

class RuleViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "考勤规则"
        
        let topLineView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor4
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
//            obj.backgroundColor = .red
        }
        
        let hints = ["规则名称", "打卡人员", "打卡范围", "打卡时间", "打卡地点", "打卡wifi"]
        let contents = ["请输入名称", "请选择打卡人员", "请选择打卡范围", "请选择时间", "请选择地点", "请选择地点"]
        for i in 0..<hints.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.frame = CGRect(x: 0, y: i*60, width: Int(kScreenWidth), height: 60)
            }
            
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var attri =  NSMutableAttributedString(string:"")
                if i < 5{
                    attri = NSMutableAttributedString(string:"*")
                }
                attri.yy_color = blackTextColor27
                
                let titleText = hints[i]
                let userAttri = NSMutableAttributedString(string: titleText)
                userAttri.yy_color = blackTextColor3
                userAttri.yy_font = kMediumFont(14)
                attri.append(userAttri)
                obj.attributedText = attri
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let tagImageV = UIImageView().then { obj in
                v.addSubview(obj)
                obj.image = UIImage(named: "me_tag")
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-15)
                    make.centerY.equalToSuperview()
                }
            }
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                
                obj.textColor = blackTextColor3
                obj.font = kMediumFont(12)
                obj.text = contents[i]
                
                obj.snp.makeConstraints { make in
                    make.right.equalTo(tagImageV.snp.left).offset(-10)
                    make.centerY.equalToSuperview()
                }
            }
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor7
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
            
            
            _ = UIButton().then({ obj in
                v.addSubview(obj)
                obj.tag = 1000+i
                obj.addTarget(self, action: #selector(touchClick(_ :)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            })
            
        }
        
        _ = UILabel().then { obj in
            headView.addSubview(obj)
            obj.text = "打卡地点和打卡wifi满足任意一项即可打卡"
            obj.textColor = blackTextColor36
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(360)
            }
        }
        
        _ = UILabel().then({ obj in
            headView.addSubview(obj)
            obj.text = "补卡申请"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(404)
            }
        })
        
        _ = UISwitch().then({ obj in
            headView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(85)
                make.top.equalToSuperview().offset(398)
            }
        })
        
        
        _ = UIButton().then({ obj in
            headView.addSubview(obj)
            obj.backgroundColor = bluebgColor
            obj.setTitle("保存", for: .normal)
            obj.layer.cornerRadius = 2
            obj.setTitleColor(.white, for: .normal)
            obj.titleLabel?.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(442)
                make.height.equalTo(40)
            }
            
        })
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.tableHeaderView = headView
//            obj.delegate = self
//            obj.dataSource = self
//            obj.register(CommerceCheckTableViewCell.self, forCellReuseIdentifier: "CommerceCheckTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(topLineView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
            
        // Do any additional setup after loading the view.
    }
    
    @objc func touchClick(_ button:UIButton){
//        printTest("asdasdasdasdasdasdasd")
        if button.tag == 1005{
            self.popup.bottomSheet {
                let v = RuleWifiView()
                return v
            }
        }else{
            self.popup.bottomSheet {
                let v = RuleWeekView()
                return v
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
