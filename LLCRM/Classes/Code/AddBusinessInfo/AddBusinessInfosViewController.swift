//
//  AddBusinessInfosViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit
//import JXPagingView

extension AddBusinessInfosViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension AddBusinessInfosViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}


class AddBusinessInfosViewController: BaseViewController {
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        let baseInfos = [["商机编号："],["*","客户名称："],["*","商机名称："],["商机来源："],["*","商机性质："],["*","商机金额： "],["产品类型："],["*","介入阶段："],["客户成交状态："],["*","跟随人："],["*","期望完成时间："],["可能性："]]

        
        let contentInfos = ["BO230613001", "请选择客户", "请输入", "请选择", "请选择商机性质", "请输入商机金额（元）", "请选择", "请选择", "请选择", "请选择", "请选择预计成交日期", "请选择"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                        
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i == 1 || i == 3 || i == 4 || i == 6 || i == 7 || i == 8 || i == 9 || i == 10 || i == 11{
                let tagImageV = UIImageView().then { obj in
                    v.addSubview(obj)
//                    obj.backgroundColor = .red
                    obj.image = UIImage(named: "me_push")
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-15)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(15)
                    }
                }
            }

            let textFiled = UITextField().then { obj in
                v.addSubview(obj)
                obj.placeholder = contentInfos[i]
                obj.font = kMediumFont(12)
                obj.tag = 1000+i
                obj.delegate = self
                obj.snp.makeConstraints { make in
                    make.right.equalToSuperview().offset(-45)
                    make.centerY.equalToSuperview()
                }
            }

            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-20)
            }
        })
        
        let bottomView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(UIDevice.xp_tabBarFullHeight()+20)
            }
        }
        
        let isAgreementButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(15)
                make.width.height.equalTo(15)
            }
        }
        
        let agreementLabel = UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.textColor = blackTextColor3
            obj.font = kMediumFont(14)
            obj.text = "此商机共享"
            obj.snp.makeConstraints { make in
                make.left.equalTo(isAgreementButton.snp.right).offset(5)
                make.centerY.equalTo(isAgreementButton)
            }
        }
        
        let saveButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.setTitle("保存", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(50)
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(40)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddBusinessInfosViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            
        }else if textField.tag == 1001{
            
        }else if textField.tag == 1002{
            
        }else if textField.tag == 1003{
            
        }else if textField.tag == 1004{
            
        }else if textField.tag == 1005{
            
        }else if textField.tag == 1006{
            
        }else if textField.tag == 1007{
            
        }else if textField.tag == 1008{
            
        }else if textField.tag == 1009{
            
        }
    }
}

