//
//  ConventionViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/13.
//

import UIKit

class ConventionViewController: BaseViewController {
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.navigationItem.title = "常规"
        
        let headView = UIView().then { obj in
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let topLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let languageView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 10, width: Int(kScreenWidth), height: 50)
        }
        
        let languageHintLabel = UILabel().then { obj in
            languageView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "多语言"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        let languageButton = UIButton().then { obj in
            languageView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let twoLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(60)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let discriminateView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(70)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            discriminateView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "来电身份识别"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UILabel().then({ obj in
            discriminateView.addSubview(obj)
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor46
            obj.text = "已关闭"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-28)
                make.centerY.equalToSuperview()
            }
        })
        
        _ = UIButton().then({ obj in
            discriminateView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let discriminateHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(120)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        let discriminateHintLabel = UILabel().then { obj in
            discriminateHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "开启来电身份识别，你的同事/你的客户打来电话都可以自动识别显示姓名，帮助你更快调整接通状态。点击去设"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let networkView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(170)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            networkView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "网络诊断"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            networkView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UIButton().then({ obj in
            networkView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let logView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(220)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            logView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "上传日志"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            logView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UIButton().then({ obj in
            networkView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let threeLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(270)
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let clearView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(280)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            clearView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "清除本地缓存"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            clearView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UIButton().then({ obj in
            clearView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let checkView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(330)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            checkView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "上传考外勤日志"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            checkView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UIButton().then({ obj in
            checkView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let getWechatView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(380)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            getWechatView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "手动拉取企信列表"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            getWechatView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
        
        _ = UIButton().then({ obj in
            getWechatView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
