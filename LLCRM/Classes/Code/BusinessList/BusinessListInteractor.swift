//
//  BusinessListInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class BusinessListInteractor: BusinessListInteractorProtocols {
    var presenter: BusinessListPresenterProtocols?
    
    var entity: BusinessListEntityProtocols?
    
    func presenterRequestBusinessList(by params: Any?) {
        entity?.getBusinessListRequest(by: params)
    }
    
    func didEntityBusinessListReceiveData(by params: Any?) {
        presenter?.didGetBusinessListInteractorReceiveData(by: params)
    }
    
    func didEntityBusinessListReceiveError(error: MyError?) {
        presenter?.didGetBusinessListInteractorReceiveError(error: error)
    }
    

}
