//
//  BusinessListProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/1.
//

import UIKit

protocol BusinessListViewProtocols: AnyObject {
    var presenter: BusinessListPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetBusinessListPresenterReceiveData(by params: Any?)
    func didGetBusinessListPresenterReceiveError(error: MyError?)
}

protocol BusinessListPresenterProtocols: AnyObject{
    var view: BusinessListViewProtocols? { get set }
    var router: BusinessListRouterProtocols? { get set }
    var interactor: BusinessListInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetBusinessList(by params: Any?)
    func didGetBusinessListInteractorReceiveData(by params: Any?)
    func didGetBusinessListInteractorReceiveError(error: MyError?)
}

protocol BusinessListInteractorProtocols: AnyObject {
    var presenter: BusinessListPresenterProtocols? { get set }
    var entity: BusinessListEntityProtocols? { get set }
    
    func presenterRequestBusinessList(by params: Any?)
    func didEntityBusinessListReceiveData(by params: Any?)
    func didEntityBusinessListReceiveError(error: MyError?)
}

protocol BusinessListEntityProtocols: AnyObject {
    var interactor: BusinessListInteractorProtocols? { get set }
    
    func didBusinessListReceiveData(by params: Any?)
    func didBusinessListReceiveError(error: MyError?)
    func getBusinessListRequest(by params: Any?)
}

protocol BusinessListRouterProtocols: AnyObject {
    func pushToMyBusinessList(from previousView: UIViewController)
    func pushToMyAddBusinessList(from previousView: UIViewController)
    func pushToBusinessListDetail(from previousView: UIViewController , forModel model: Any)
}

