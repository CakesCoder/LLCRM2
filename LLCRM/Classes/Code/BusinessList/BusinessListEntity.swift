//
//  BusinessListEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class BusinessListEntity: BusinessListEntityProtocols {
    var interactor: BusinessListInteractorProtocols?
    
    func didBusinessListReceiveData(by params: Any?) {
        interactor?.didEntityBusinessListReceiveData(by: params)
    }
    
    func didBusinessListReceiveError(error: MyError?) {
        interactor?.didEntityBusinessListReceiveError(error: error)
    }
    
    func getBusinessListRequest(by params: Any?) {
        LLNetProvider.request(.getBusinessList(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didBusinessListReceiveData(by:jsonData)
            case .failure(_):
                self.didBusinessListReceiveError(error: .requestError)
            }
        }
    }
    
}
