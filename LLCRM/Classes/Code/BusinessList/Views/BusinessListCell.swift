//
//  BusinessListCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/1.
//

import UIKit

class BusinessListCell: UITableViewCell {
    
    var model:BusinessListModel?{
        didSet{
            
            let hintAttribute = NSMutableAttributedString(string:"商机编号：")
            hintAttribute.yy_color = blackTextColor60
            hintAttribute.yy_font = kMediumFont(12)
            let personAttribute = NSMutableAttributedString(string: model?.businessOpportunity ?? "--")
            personAttribute.yy_color = bluebgColor
            personAttribute.yy_font = kMediumFont(12)
            hintAttribute.append(personAttribute)
            
            nameLabel?.text = model?.commercialName ?? "--"
            codeLabel?.attributedText = hintAttribute
            moneyLabel?.text = model?.commercialMoney ?? "--"
            nameLabel2?.text = model?.createrName ?? "--"
            timeLabel?.text = "日期："+(model?.createDate ?? "--")
        }
    }
    var nameLabel:UILabel?
    var codeLabel:UILabel?
    var nameLabel2:UILabel?
    var moneyLabel:UILabel?
    var timeLabel:UILabel?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then { obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "business_money")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        }
        
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor78
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
//        let tagLabel = UILabel().then { obj in
//            contentView.addSubview(obj)
//            obj.backgroundColor = bgColor9
//            obj.text = "赢单"
//            obj.textColor = .white
//            obj.font = kRegularFont(8)
//            obj.textAlignment = .center
//            obj.snp.makeConstraints { make in
//                make.left.equalTo(nameLabel.snp.right).offset(5)
//                make.centerY.equalTo(tagImageV)
//                make.width.equalTo(26)
//                make.height.equalTo(15)
//            }
//        }
        
        codeLabel = UILabel().then { obj in
            contentView.addSubview(obj)

            
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(nameLabel!.snp.bottom).offset(10)
            }
        }
        
        timeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
//            obj.text = "日期：2023.04.27  09:30"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(35)
                make.top.equalTo(codeLabel!.snp.bottom).offset(10)
            }
        }
        
        nameLabel2 = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = bluebgColor
            obj.font = kMediumFont(12)
//            obj.text = "王小虎"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(codeLabel!)
            }
        }
        
        moneyLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.textColor = blackTextColor36
//            obj.text = "￥5000.00"
            obj.font = kRegularFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(timeLabel!)
            }
        }
        
        _ = UIView().then { obj in
            contentView.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(5)
            }
        }
    }

}
