//
//  BusinessListRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class BusinessListRouter: BusinessListRouterProtocols {
    func pushToMyBusinessList(from previousView: UIViewController) {
        let nextView = MyBusinessListViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
        
    func pushToMyAddBusinessList(from previousView: UIViewController) {
        let nextView = AddBusinessPageViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToBusinessListDetail(from previousView: UIViewController, forModel model: Any) {
        let nextView = BusinessDetailViewController()
        nextView.model = model as! BusinessListModel
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
