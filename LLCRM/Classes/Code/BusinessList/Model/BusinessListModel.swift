//
//  BusinessListModel.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/27.
//

import UIKit

class BusinessListDataModdel: BaseModel{
    var code:Int?
    var data:[BusinessListModel]?
    var message:String?
    
    required init() {}
}

class BusinessListModel: BaseModel {
    var accessoryList:[Any]?
    var amountUnit:String?
    var businessOpportunity:String?
    var clientClueContactSimpleParam:ClueContactSimpleParamModel?
    var clientId:Int?
    var clientName:String?
    var commercialExtraInfo:CommercialExtraInfoModel?
    var commercialInfo:String?
    var commercialMoney:String?
    var commercialMoneyUnit:String?
    var commercialName:String?
    var commercialTimes:Int?
    var createDate:String?
    var createDept:String?
    var createrId:Int?
    var createrName:String?
    var customerDealStatus:String?
    var followerId:Int?
    var followerName:String?
    var id:Int?
    var interventionStage:String?
    var joinPeople:String?
    var moneyUnit:String?
    var probability:String?
    var productAmount:String?
    var productDate:String?
    var productInfo:String?
    var productMoney:String?
    var productName:String?
    var productStage:String?
    var productTypeName:String?
    var property:String?
    var result:String?
    var shareType:String?
    var source:Int?
    var stageName:String?
    var stageVoList:[stageVoListModel]? = []
    var version:String?
    var winnerMoney:String?
    
    required init() {}
}

class stageVoListModel:BaseModel{
    var companyId:Int?
    var deleted:Int?
    var id:Int?
    var orderNumber:Int?
    var stageName:String?
    var version:String?
    
    required init() {}
}

class ClueContactSimpleParamModel:BaseModel{
    var address:String?
    var clientId:String?
    var department:String?
    var email:String?
    var gender:String?
    var id:String?
    var name:String?
    var phone:String?
    var position:String?
    
    required init(){}
}

class CommercialExtraInfoModel: BaseModel{
    var commercialId:String?
    var competitor:String?
    var cooperateBuy:String?
    var goCompany:String?
    var id:String?
    var otherInfo:String?
    var publicist:String?
    var remark:String?
    var trackState:String?
    required init(){}
}
