//
//  BusinessListPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/8.
//

import UIKit

class BusinessListPresenter: BusinessListPresenterProtocols {
    var view: BusinessListViewProtocols?
    
    var router: BusinessListRouterProtocols?
    
    var interactor: BusinessListInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetBusinessList(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestBusinessList(by: params)
    }
    
    func didGetBusinessListInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetBusinessListPresenterReceiveData(by: params)
    }
    
    func didGetBusinessListInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetBusinessListPresenterReceiveError(error: error)
    }
    

}
