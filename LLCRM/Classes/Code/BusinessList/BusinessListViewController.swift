//
//  BusinessListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/1.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh

class BusinessListViewController: BaseViewController {
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    var presenter: BusinessListPresenterProtocols?
    var currentIndex:Int = 1
    var businissDataList:[BusinessListModel]? = []
    var tableView:UITableView?
    var selectedIndex:Int? = 0
    
//    let header = MJRefreshNormalHeader()
//    let footer = MJRefreshBackNormalFooter()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "商机"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(BusinessListViewController.blackClick))
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        _ = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        view.addSubview(self.segmentedView!)
        
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(headView.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["进行中","项目中止","未完成","已完成"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView

        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(BusinessListCell.self, forCellReuseIdentifier: "BusinessListCell")
//            if #available(iOS 11.0, *) {
            obj.estimatedRowHeight = 0
            obj.estimatedSectionFooterHeight = 0
            obj.estimatedSectionHeaderHeight = 0
//            }
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(segmentedView!.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
//        tableView?.setRefreshHeader(MZRefreshNormalHeader(beginRefresh: { [self] in
//            // 请求数据，请求到数据后记得停止刷新动画
//            currentIndex = 0
//            if selectedIndex == 0{
//                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":3])
//            }else if selectedIndex == 1{
//                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":2])
//            }else if selectedIndex == 2{
//                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":0])
//            }else{
//                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":1])
//            }
//            tableView?.stopHeaderRefreshing()
//        }))
        
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
            if selectedIndex == 0{
                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":3])
            }else if selectedIndex == 1{
                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":2])
            }else if selectedIndex == 2{
                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":0])
            }else{
                presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":1])
            }
            tableView?.stopFooterRefreshing()
        }))

        
//        tableView?.li.header = NormalRefreshHeader.headerWithRefreshing(block: {[weak self] in
//            self?.currentIndex = 0
//            if self?.selectedIndex == 0{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "status":3])
//            }else if self?.selectedIndex == 1{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "status":2])
//            }else if self?.selectedIndex == 2{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "state":0])
//            }else{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "state":1])
//            }
//        })
//        tableView?.li.footer = NormalRefreshFooter.footerWithRefreshing(block: {[weak self] in
//            if self?.selectedIndex == 0{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "status":3])
//            }else if self?.selectedIndex == 1{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "status":2])
//            }else if self?.selectedIndex == 2{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "state":0])
//            }else{
//                self?.presenter?.presenterRequestGetBusinessList(by: ["current": self?.currentIndex, "size": 20, "state":1])
//            }
//        })
        
        _ = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        cofing()
        
        currentIndex = 1
        presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":3])
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedBusinessListAction(_:)), name: Notification.Name(rawValue: "BusinessListNotificationKey"), object: nil)
    }
    
    @objc func didSelectedBusinessListAction(_ notification:NSNotification){
        currentIndex = 1
        if selectedIndex == 0{
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":3])
        }else if selectedIndex == 1{
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":2])
        }else if selectedIndex == 2{
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":0])
        }else{
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":1])
        }
    }
    
    func cofing(){
        let router = BusinessListRouter()
        
        presenter = BusinessListPresenter()
        
        presenter?.router = router
        
        let entity = BusinessListEntity()
        
        let interactor = BusinessListInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    @objc func addClick(_ button:UIButton){
        presenter?.router?.pushToMyAddBusinessList(from: self)
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//extension BusinessListViewController: UIScrollViewDelegate{
//    func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        let y = scrollView.contentOffset.y
////        printTest(y)
////        printTest("\(scrollView.contentSize.height) asddasdasds")
//        if scrollView.contentSize.height - 100 == y{
////            printTest("asdasadsd")
//            if self.selectedIndex == 0{
//                self.presenter?.presenterRequestGetBusinessList(by: ["current": self.currentIndex, "size": 20, "status":3])
//            }else if self.selectedIndex == 1{
//                self.presenter?.presenterRequestGetBusinessList(by: ["current": self.currentIndex, "size": 20, "status":2])
//            }else if self.selectedIndex == 2{
//                self.presenter?.presenterRequestGetBusinessList(by: ["current": self.currentIndex, "size": 20, "state":0])
//            }else{
//                self.presenter?.presenterRequestGetBusinessList(by: ["current": self.currentIndex, "size": 20, "state":1])
//            }
//        }
//    }
//}

extension BusinessListViewController: BusinessListViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    
    func didGetBusinessListPresenterReceiveData(by params: Any?) {
//        printTest(params)
        if currentIndex == 1{
            self.businissDataList = []
        }
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                if let dataList = JSONDeserializer<BusinessListDataModdel>.deserializeFrom(json:toJSONString(dict: dict)) {
                    self.businissDataList?.append(contentsOf: dataList.data!)
                }
                currentIndex += 1
                tableView?.reloadData()
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
//        tableView?.spr_endRefreshing()
    }
    
    func didGetBusinessListPresenterReceiveError(error: MyError?) {
//        tableView?.spr_endRefreshing()
    }
    
    
}

extension BusinessListViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
//        self.searchText = textField.text
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        return true
    }
}

extension BusinessListViewController: JXSegmentedViewDelegate{
//    func pagingView(_ pagingView: JXPagingView.JXPagingView, initListAtIndex index: Int) -> JXPagingView.JXPagingViewListViewDelegate {
//        return nil
//    }
//
//    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
//        return 0
//    }r
//
//    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
//        return 0
//    }
//
//    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
//        return UIView()
//    }
//
//    func numberOfLists(in pagingView: JXPagingView) -> Int {
//        return 4
//    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }
    
    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
        currentIndex = 0
        if index == 0{
            selectedIndex = 0
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":3])
        }else if index == 1{
            selectedIndex = 1
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "status":2])
        }else if index == 2{
            selectedIndex = 2
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":0])
        }else{
            selectedIndex = 3
            presenter?.presenterRequestGetBusinessList(by: ["current": currentIndex, "size": 20, "state":1])
        }
    }
}

extension BusinessListViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 106
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.businissDataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BusinessListCell", for: indexPath) as! BusinessListCell
        if self.businissDataList?[indexPath.row] != nil{
            cell.model = self.businissDataList?[indexPath.row]
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if indexPath.row == 0{
//            self.presenter?.router?.pushToMyBusinessList(from: self)
//            return
//        }
        self.presenter?.router?.pushToBusinessListDetail(from: self, forModel: self.businissDataList?[indexPath.row])
    }
}
