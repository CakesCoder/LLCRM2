//
//  HomeClientEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class HomeClientEntity: HomeClientEntityProtocols {
    var interactor: HomeClientInteractorProtocols?
    
    func didClientListReceiveData(by params: Any?) {
        interactor?.didEntityClientListReceiveData(by: params)
    }
    
    func didClientListReceiveError(error: MyError?) {
        interactor?.didEntityClientListReceiveError(error: error)
    }
    
    func getClientListRequest(by params: Any?) {
        LLNetProvider.request(.clientDetail(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didClientListReceiveData(by:jsonData)
            case .failure(_):
                self.didClientListReceiveError(error: .requestError)
            }
        }
    }

}
