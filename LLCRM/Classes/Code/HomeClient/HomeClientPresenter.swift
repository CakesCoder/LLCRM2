//
//  HomeClientPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class HomeClientPresenter: HomeClientPresenterProtocols {
    var view: HomeClientViewProtocols?
    
    var router: HomeClientRouterProtocols?
    
    var interactor: HomeClientInteractorProtocols?
    
    var params: Any?
    
    func viewDidLoad() {
//        self.presenterRequestGetClientList()
    }
    
    func presenterRequestGetClientList(by params: Any?) {
        view?.showLoading()
        interactor?.presenterRequestClientList(by: params)
    }
    
    func didGetClientListInteractorReceiveData(by params: Any?) {
        view?.hideLoading()
        view?.didGetClientListPresenterReceiveData(by: params)
    }
    
    func didGetClientListInteractorReceiveError(error: MyError?) {
        view?.hideLoading()
        view?.showError()
        view?.didGetClientListPresenterReceiveError(error: error)
    }
}
