//
//  HomeClientInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class HomeClientInteractor: HomeClientInteractorProtocols {
    var presenter: HomeClientPresenterProtocols?
    
    var entity: HomeClientEntityProtocols?
    
    func presenterRequestClientList(by params: Any?) {
        entity?.getClientListRequest(by: params)
    }
    
    func didEntityClientListReceiveData(by params: Any?) {
        presenter?.didGetClientListInteractorReceiveData(by: params)
    }
    
    func didEntityClientListReceiveError(error: MyError?) {
        presenter?.didGetClientListInteractorReceiveError(error: error)
    }
    
}
