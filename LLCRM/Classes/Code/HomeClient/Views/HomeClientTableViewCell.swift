//
//  HomeClientTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class HomeClientTableViewCell: UITableViewCell {
    
    var titleLabel:UILabel?
    var dateLabel:UILabel?
    var nameLabel:UILabel?
    var typeLabel:UILabel?
    
    
    var model:ClientDetailDataModel?{
        didSet{
            titleLabel?.text = model?.clientName ?? "暂无"
            dateLabel?.text = "创建时间："+(model?.date ?? "暂无")
            nameLabel?.text = model?.createrName ?? "暂无"
            typeLabel?.text = model?.createDept ?? "暂无"
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        let tagImageV = UIImageView().then({ obj in
            contentView.addSubview(obj)
            obj.image = UIImage(named: "client_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(13)
            }
        })
        
        titleLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "零瓴软件技术(深圳)有限公司"
            obj.textColor = blackTextColor78
            obj.font = kMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        dateLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "创建时间：2022.05.12  09:30"
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(tagImageV.snp.right).offset(3)
                make.top.equalTo(titleLabel!.snp.bottom).offset(10)
            }
            
        }
        
        nameLabel = UILabel().then { obj in
            contentView.addSubview(obj)
//            obj.text = "王小虎"
            obj.textColor = bluebgColor
            obj.font = kMediumFont(13)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagImageV)
            }
        }
        
        typeLabel = UILabel().then { obj in
            contentView.addSubview(obj)
            obj.text = "小型客户"
            obj.textColor = blackTextColor60
            obj.font = kMediumFont(12)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(titleLabel!.snp.bottom).offset(10)
            }
        }
        
        _ = UIView().then({ obj in
            contentView.addSubview(obj)
            obj.backgroundColor = lineColor7
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
    }

}
