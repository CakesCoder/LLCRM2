//
//  SegmentedView.swift
//  SwiftDropMenuView
//
//  Created by xiaoyuan on 2021/7/19.
//

import UIKit

class SegmentedView: UIView {
    
    lazy var nearbyBtn = SwiftDropMenuControl(frame: .zero, listView: MapFilterDropMenuListView()).with {
        $0.setTitle("最近联系时间", for: .normal)
        $0.setTitleColor(blackTextColor77, for: .normal)
        $0.setTitleColor(blackTextColor77, for: .normal)
        $0.setTitleColor(bluebgColor, for: .selected)
        $0.titleLabel?.font = kMediumFont(12)
        $0.setImage(UIImage(named: "icon_sort_arrow_down_1"), for: .normal)
        $0.setImage(UIImage(named: "icon_sort_arrow_up_1"), for: .selected)
        $0.contentHorizontalAlignment = .left
//        $0.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        $0.animateDuration = 0.1
    }
    
    lazy var nearbyBtn2 = SwiftDropMenuControl(frame: .zero, listView: MapFilterDropMenuListView()).with {
        $0.setTitle("客户状态", for: .normal)
        $0.setTitleColor(blackTextColor77, for: .normal)
        $0.setTitleColor(blackTextColor77, for: .normal)
        $0.setTitleColor(bluebgColor, for: .selected)
        $0.titleLabel?.font = kMediumFont(12)
        $0.setImage(UIImage(named: "icon_sort_arrow_down_1"), for: .normal)
        $0.setImage(UIImage(named: "icon_sort_arrow_up_1"), for: .selected)
        $0.contentHorizontalAlignment = .center
//        $0.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        $0.animateDuration = 0.1
    }
    
    lazy var nearbyBtn3 = SwiftDropMenuControl(frame: .zero, listView: MapFilterDropMenuListView()).with {
        $0.setTitle("高级筛选", for: .normal)
        $0.setTitleColor(blackTextColor77, for: .normal)
        $0.setTitleColor(blackTextColor77, for: .normal)
        $0.setTitleColor(bluebgColor, for: .selected)
        $0.titleLabel?.font = kMediumFont(12)
        $0.setImage(UIImage(named: "icon_sort_arrow_down_1"), for: .normal)
        $0.setImage(UIImage(named: "icon_sort_arrow_up_1"), for: .selected)
        $0.contentHorizontalAlignment = .right
//        $0.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 50)
        $0.animateDuration = 0.1
    }
    
    var selectedBtn: UIButton? {
        didSet {
            selectedBtn?.isSelected = true
            oldValue?.isSelected = false
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(nearbyBtn)
        addSubview(nearbyBtn2)
        addSubview(nearbyBtn3)
        
        let w = (kScreenWidth-30)/3.0
        nearbyBtn.frame = CGRect(x: 15, y: 0, width: w, height: 20)
        
        nearbyBtn3.frame = CGRect(x: kScreenWidth-15-w, y: 0, width: w, height: 20)
        
        nearbyBtn2.frame = CGRect(x: kScreenWidth/2.0-w/2.0, y: 0, width: w, height: 20)
        
        nearbyBtn.addTarget(self, action: #selector(nearbyBtnAction(sender:)), for: .touchUpInside)
        nearbyBtn2.addTarget(self, action: #selector(nearbyBtnAction2(sender:)), for: .touchUpInside)
        nearbyBtn3.addTarget(self, action: #selector(nearbyBtnAction3(sender:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc private func nearbyBtnAction(sender: UIButton) {
        selectedBtn = sender
    }
    @objc private func nearbyBtnAction2(sender: UIButton) {
        selectedBtn = sender
    }
    @objc private func nearbyBtnAction3(sender: UIButton) {
        selectedBtn = sender
    }
    
}
