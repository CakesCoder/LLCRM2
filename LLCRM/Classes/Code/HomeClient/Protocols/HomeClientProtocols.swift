//
//  HomeClientProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

protocol HomeClientViewProtocols: AnyObject {
    var presenter: HomeClientPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetClientListPresenterReceiveData(by params: Any?)
    func didGetClientListPresenterReceiveError(error: MyError?)
}

protocol HomeClientPresenterProtocols: AnyObject{
    var view: HomeClientViewProtocols? { get set }
    var router: HomeClientRouterProtocols? { get set }
    var interactor: HomeClientInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    func presenterRequestGetClientList(by params: Any?)
    func didGetClientListInteractorReceiveData(by params: Any?)
    func didGetClientListInteractorReceiveError(error: MyError?)
}

protocol HomeClientInteractorProtocols: AnyObject {
    var presenter: HomeClientPresenterProtocols? { get set }
    var entity: HomeClientEntityProtocols? { get set }
    
    func presenterRequestClientList(by params: Any?)
    func didEntityClientListReceiveData(by params: Any?)
    func didEntityClientListReceiveError(error: MyError?)
}

protocol HomeClientEntityProtocols: AnyObject {
    var interactor: HomeClientInteractorProtocols? { get set }
    func didClientListReceiveData(by params: Any?)
    func didClientListReceiveError(error: MyError?)
    func getClientListRequest(by params: Any?)
}

protocol HomeClientRouterProtocols: AnyObject {
    func pushToClientDetail(from previousView: UIViewController)
    func pushToAddClient(from previousView: UIViewController) 
}
