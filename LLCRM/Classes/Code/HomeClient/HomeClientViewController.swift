//
//  HomeClientViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit
import PKHUD
import HandyJSON
import MZRefresh

class HomeClientViewController: BaseViewController {
    var titles = ["选项一\n1","选项二\n2","选项三\n3","选项四\n4", "选项五\n5", "选项六\n5", "选项七\n7"]
    var selectedIndex: Int?
    var current:Int? = 1
    weak var dropMenu1: SwiftDropMenuListView?
    weak var dropMenu2: SwiftDropMenuListView?
    var presenter: HomeClientPresenterProtocols?
    var dataList:[ClientDetailDataModel]? = []
    var tableView:UITableView?
    
    var segmentView: SegmentedView?
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "客户"
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(82)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
        let hintView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(headView.snp.bottom).offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(30)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            hintView.addSubview(obj)
            obj.text = "共10条"
            obj.textColor = blackTextColor70
            obj.font = kMediumFont(10)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        tableView = UITableView().then({ obj in
            view.addSubview(obj)
//            obj.tableHeaderView = headView
            obj.separatorStyle = .none
            obj.delegate = self
            obj.dataSource = self
            obj.register(HomeClientTableViewCell.self, forCellReuseIdentifier: "HomeClientTableViewCell")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(hintView.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        
        /// 自定义`listView`
        let segmentView = SegmentedView(frame: CGRect(x: 0, y: UIDevice.xp_navigationFullHeight()+38+15, width: kScreenWidth, height: 20))
        view.addSubview(segmentView)
        self.segmentView = segmentView
        segmentView.nearbyBtn.delegate = self
        
        
//        let w = (kScreenWidth-30)/3.0
//        let menuControlTime = SwiftDropMenuControl(frame: CGRect(x: 15, y: UIDevice.xp_navigationFullHeight()+38+15, width: w, height: 20), listView: MapFilterDropMenuListView())
//        menuControlTime.setTitle("最近联系时间", for: .normal)
//        menuControlTime.setTitleColor(blackTextColor77, for: .normal)
//        menuControlTime.setTitleColor(blackTextColor77, for: .normal)
//        menuControlTime.setTitleColor(bluebgColor, for: .selected)
//        menuControlTime.titleLabel?.font = kMediumFont(12)
////        menuControlTime.titleLabel?.textAlignment = .left
//        menuControlTime.setImage(UIImage(named: "icon_sort_arrow_down_1"), for: .normal)
//        menuControlTime.setImage(UIImage(named: "icon_sort_arrow_up_1"), for: .selected)
//        menuControlTime.contentHorizontalAlignment = .left
//        menuControlTime.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
//        menuControlTime.animateDuration = 0.1
//        menuControlTime.delegate = self
////        menuControlTime.sizeToFit()
//        view.addSubview(menuControlTime)
//
//        let menuControlAdvanced = SwiftDropMenuControl(frame: CGRect(x: kScreenWidth-15-w, y: UIDevice.xp_navigationFullHeight()+38+15, width: w, height: 20), listView: MapFilterDropMenuListView())
////        menuControlAdvanced.backgroundColor = .green
//        menuControlAdvanced.setTitleColor(blackTextColor77, for: .normal)
//        menuControlAdvanced.setTitleColor(blackTextColor77, for: .normal)
//        menuControlAdvanced.setTitleColor(bluebgColor, for: .selected)
//        menuControlAdvanced.titleLabel?.font = kMediumFont(12)
//        menuControlAdvanced.setImage(UIImage(named: "icon_sort_arrow_down_1"), for: .normal)
//        menuControlAdvanced.setImage(UIImage(named: "icon_sort_arrow_up_1"), for: .selected)
//        menuControlAdvanced.setTitle("高级筛选", for: .normal)
//        menuControlAdvanced.contentHorizontalAlignment = .right
//        menuControlAdvanced.animateDuration = 0.1
//        menuControlAdvanced.delegate = self
////        menuControlAdvanced.sizeToFit()
//        view.addSubview(menuControlAdvanced)
//
//        let menuControlStatus = SwiftDropMenuControl(frame: CGRect(x: view.frame.width/2.0-w/2.0, y: UIDevice.xp_navigationFullHeight()+38+15, width: w, height: 20), listView: MapFilterDropMenuListView())
////        menuControlStatus.backgroundColor = .red
//        menuControlStatus.setTitleColor(blackTextColor77, for: .normal)
//        menuControlStatus.setTitleColor(blackTextColor77, for: .normal)
//        menuControlStatus.setTitleColor(bluebgColor, for: .selected)
//        menuControlStatus.titleLabel?.font = kMediumFont(12)
//        menuControlStatus.setImage(UIImage(named: "icon_sort_arrow_down_1"), for: .normal)
//        menuControlStatus.setImage(UIImage(named: "icon_sort_arrow_up_1"), for: .selected)
//        menuControlStatus.setTitle("客户状态", for: .normal)
//        menuControlStatus.contentHorizontalAlignment = .center
////        menuControlStatus.contentHorizontalAlignment = .right
////        menuControlStatus.contentVerticalAlignment = .right
////        menuControlStatus.contentEdgeInsets = UIEdgeInsets(top: 0, left: w, bottom: 0, right: -w)
//        menuControlStatus.animateDuration = 0.1
//        menuControlStatus.delegate = self
////        menuControlStatus.sizeToFit()
//        view.addSubview(menuControlStatus)
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(HomeClientViewController.blackClick))
        
        let addButton = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.addTarget(self, action: #selector(addClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        }
        
        tableView?.setRefreshFooter(MZRefreshNormalFooter(beginRefresh: { [self] in
            presenter?.presenterRequestGetClientList(by: ["current":current, "size":10])
            tableView?.stopFooterRefreshing()
        }))
        
        cofing()
        
        current = 1
        presenter?.presenterRequestGetClientList(by: ["current":current, "size":10])
    }
    
    func cofing(){
        let router = HomeClientRouter()
        
        presenter = HomeClientPresenter()
        presenter?.router = router
        
        let entity = HomeClientEntity()
        
        let interactor = HomeClientInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
    }
    
    @objc func addClick(_ button:UIButton){
        presenter?.router?.pushToAddClient(from: self)
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension HomeClientViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 85
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return 10
        return dataList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeClientTableViewCell", for: indexPath) as! HomeClientTableViewCell
        cell.model = dataList?[indexPath.row] as? ClientDetailDataModel
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.presenter?.router?.pushToClientDetail(from: self)
    }
}

extension HomeClientViewController: SwiftDropMenuListViewDataSource {
    func numberOfItems(in menu: SwiftDropMenuListView) -> Int {
        return self.titles.count
    }
    
    func dropMenu(_ menu: SwiftDropMenuListView, titleForItemAt index: Int) -> String {
        return self.titles[index]
    }
    
    func heightOfRow(in menu: SwiftDropMenuListView) -> CGFloat {
        return 40.0
    }
    
    func indexOfSelectedItem(in menu: SwiftDropMenuListView) -> Int {
        return selectedIndex ?? 0
    }
    
    func numberOfColumns(in menu: SwiftDropMenuListView) -> Int {
        if menu == self.dropMenu1 {
            return 1
        }
        return 3
    }
}

extension HomeClientViewController: SwiftDropMenuListViewDelegate {
    func dropMenu(_ menu: SwiftDropMenuListView, didSelectItem: String?, atIndex index: Int) {
        self.selectedIndex = index
    }
}

extension HomeClientViewController: HomeClientViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }
    
    func didGetClientListPresenterReceiveData(by params: Any?){
        printTest(params)
        if let dict = params as? [String:Any]{
            let dataDic = dict["data"] as? [String:Any]
            if dict["code"] as! Int == 200{
                if current == 1{
                    dataList?.removeAll()
                }
                if let datas = JSONDeserializer<HomeClientDetailModel>.deserializeFrom(json:toJSONString(dict:dataDic ?? ["":""])) {
                    printTest(datas.dataList)
                    dataList?.append(contentsOf: datas.dataList ?? [])
                    tableView?.reloadData()
                    current! += 1
                }
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetClientListPresenterReceiveError(error: MyError?){
        
    }

}

