//
//  HomeClientRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/23.
//

import UIKit

class HomeClientRouter: HomeClientRouterProtocols {
    func pushToClientDetail(from previousView: UIViewController) {
        let nextView = HomeClientDetailViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToAddClient(from previousView: UIViewController) {
        let nextView = AddClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    
}
