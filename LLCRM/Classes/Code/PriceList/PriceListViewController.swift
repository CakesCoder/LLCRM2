//
//  PriceListViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/22.
//

import UIKit
//import JXSegmentedView
//import JXPagingView

class PriceListViewController: BaseViewController {
    
    var segmentedTitleDataSource: JXSegmentedTitleDataSource?
    var segmentedMixcellDataSource: JXSegmentedTitleDataSource? = JXSegmentedTitleDataSource()
    var segmentedView: JXSegmentedView?
    var pagingView: JXPagingView?
    
    lazy var noDataView:UIView = {
        let noDataV = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: UIDevice.xp_navigationFullHeight()+104, width: kScreenWidth, height: kScreenHeight)
            obj.backgroundColor = .white
        }
        
        let noDataImageV = UIImageView().then { obj in
            noDataV.addSubview(obj)
            obj.image = UIImage(named: "price_nodata")
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.top.equalToSuperview().offset(120)
            }
        }
        
        let hintLabel = UILabel().then { obj in
            noDataV.addSubview(obj)
            obj.text = "还未创建报价单据"
            obj.font = kMediumFont(14)
            obj.textColor = blackTextColor22
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(270)
                make.centerX.equalToSuperview()
            }
        }
        
        let addButton = UIButton().then { obj in
            noDataV.addSubview(obj)
//            obj.setImage(UIImage(named: "sechedule_add"), for: .normal)
            obj.setTitle("新建报价", for: .normal)
            obj.titleLabel?.font = kMediumFont(12)
            obj.setTitleColor(workTagColor, for: .normal)
            obj.layer.borderColor = workTagColor.cgColor
            obj.layer.borderWidth = 0.5
            obj.layer.cornerRadius = 15
//            obj.addTarget(self, action: #selector(addCustomerClick(_ :)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(320)
                make.centerX.equalToSuperview()
                make.width.equalTo(90)
                make.height.equalTo(30)
            }
        }
        
        return noDataV
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        navigationItem.title = "报价单"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(PriceListViewController.backClick))
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.createAddBarbuttonItem(name: "+", target: self, action: #selector(PriceListViewController.addClick))
        
        view.backgroundColor = .white
        
        let headView = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(60)
            }
        }
        
        let searchView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor11
            obj.layer.cornerRadius = 4
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(15)
                make.height.equalTo(30)
            }
        }
        
        let searchImageV = UIImageView().then { obj in
            searchView.addSubview(obj)
            obj.image = UIImage(named: "home_search_tag")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.height.equalTo(15)
            }
        }
        
        let searchTextFiled = UITextField().then { obj in
            searchView.addSubview(obj)
            obj.font = kMediumFont(14)
            obj.placeholder = "搜索"
            obj.delegate = self
            obj.snp.makeConstraints { make in
                make.left.equalTo(searchImageV.snp.right).offset(5)
                make.right.bottom.equalTo(-5)
                make.top.equalTo(5)
            }
        }
        
//        let listView = UIView().then { obj in
//            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
//            obj.backgroundColor = .red
//        }
        
        segmentedView = JXSegmentedView()
        segmentedView?.delegate = self
        view.addSubview(self.segmentedView!)
        
        segmentedView?.snp.makeConstraints({ make in
            make.top.equalTo(headView.snp.bottom).offset(0)
            make.left.equalToSuperview().offset(0)
            make.right.equalToSuperview().offset(-0)
            make.height.equalTo(43)
        })
        
        //segmentedDataSource一定要通过属性强持有，不然会被释放掉
        segmentedTitleDataSource = JXSegmentedTitleDataSource()
        //配置数据源相关配置属性
        segmentedTitleDataSource?.titles = ["进行中","未完成","已完成","报价对比"]
        segmentedTitleDataSource?.isTitleColorGradientEnabled = true
        segmentedTitleDataSource?.isTitleZoomEnabled = true
        segmentedTitleDataSource?.titleNormalFont = kMediumFont(12)
        segmentedTitleDataSource?.titleNormalColor = blackTextColor60
        segmentedTitleDataSource?.titleSelectedColor = bluebgColor
        //关联dataSource
        segmentedView?.dataSource = self.segmentedTitleDataSource
        
        let indicator = JXSegmentedIndicatorLineView()
        indicator.indicatorWidth = 31
        indicator.indicatorColor = bluebgColor
        
        segmentedView?.indicators = [indicator]
        
        pagingView = JXPagingView(delegate: self)
        view.addSubview(pagingView!)
        pagingView!.snp.makeConstraints { make in
            make.top.equalTo(segmentedView!.snp.bottom).offset(0)
            make.left.right.equalTo(headView)
            make.bottom.equalTo(UIDevice.xp_tabBarFullHeight())
        }
        
        segmentedView?.contentScrollView = pagingView?.listContainerView.customScrollView
        
        let noDataV = noDataView.then { obj in
            view.addSubview(obj)
            obj.isHidden = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalTo(segmentedView!.snp.bottom).offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
    }
    
    @objc func backClick(){
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addClick(){
        let vc = AddPriceViewController()
        self.navigationController?.pushViewController(vc, animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PriceListViewController: UITextFieldDelegate{
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
}

extension PriceListViewController: JXSegmentedViewDelegate, JXPagingViewDelegate{
    func pagingView(_ pagingView: JXPagingView, initListAtIndex index: Int) -> JXPagingViewListViewDelegate {
        if index == 0{
            let vc = PriceListProceedViewController()
            vc.priceListdidSelectedCallback = {[self] model in
                let vc = PriceDetailViewController()
                vc.pricecId = model.id
//                vc.model = modelss
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return vc
        }else if index == 1{
            let vc = PriceYetViewController()
            vc.priceListdidSelectedCallback = {[self] model in
                let vc = PriceDetailViewController()
                vc.pricecId = model.id
//                vc.model = model
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return vc
        }else if index == 2{
            let vc = AllPeiceListViewController()
            vc.priceListdidSelectedCallback = {[self] model in
                let vc = PriceDetailViewController()
                vc.pricecId = model.id
//                vc.model = model
                self.navigationController?.pushViewController(vc, animated: true)
            }
            return vc
        }
        let vc = ContrastPriceViewController()
        return vc
    }
    
    func tableHeaderViewHeight(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func tableHeaderView(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func heightForPinSectionHeader(in pagingView: JXPagingView) -> Int {
        return 0
    }

    func viewForPinSectionHeader(in pagingView: JXPagingView) -> UIView {
        return UIView()
    }

    func numberOfLists(in pagingView: JXPagingView) -> Int {
        return 4
    }

    func heightForPinSectionHeader(in pagingView: JXSegmentedView) -> Int {
        return 43
    }

    func viewForPinSectionHeader(in pagingView: JXSegmentedView) -> UIView {
        return segmentedView!
    }

    func segmentedView(_ segmentedView: JXSegmentedView, didSelectedItemAt index: Int) {
//        segmentedView.collectionView.contentOffset = CGPoint(x: Int(kScreenWidth)*index, y: 0)
//        printTest(ZZzz)
//        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = (index == 0)
    }
}
