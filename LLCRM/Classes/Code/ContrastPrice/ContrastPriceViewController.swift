//
//  ContrastPriceViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/23.
//

import UIKit
//import JXPagingView

extension ContrastPriceViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ContrastPriceViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ContrastPriceViewController: BaseViewController {
    var aaChartView: AAChartView?
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let imageV = UIImageView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(10)
                make.width.height.equalTo(8)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "报价统计分析"
            obj.font = kMediumFont(12)
            obj.textColor = blackTextColor38
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(5)
                make.centerY.equalTo(imageV)
            }
        }
        
        let chartViewWidth  = Int(kScreenWidth)
        let chartViewHeight = 270
        aaChartView = AAChartView()
        aaChartView?.frame = CGRect(x: 0,
                                    y: 40,
                                    width: chartViewWidth,
                                    height: chartViewHeight)
        view.addSubview(aaChartView!)
        
        let chartModel = AAChartModel()
            .chartType(.spline)
//            .title("Disable Spline Chart Marker Hover Effect")
            .categories([
                "1", "2", "3", "4", "5", "6",
                "7", "8", "9", "10", "11", "12"
            ])
            .markerRadius(0)//marker点半径为0个像素
            .yAxisLineWidth(0)
            .yAxisGridLineWidth(0)
            .legendEnabled(false)
            .series([
                AASeriesElement()
//                    .name("Tokyo Hot")
                    .lineWidth(0.5)
                    .color(AARgba(64, 158, 255, 1))//猩红色, alpha 透明度 1
                    .marker(AAMarker()
                        .states(AAMarkerStates()
                            .hover(AAMarkerHover()
                                .enabled(false))))
                    .data([7.0, 6.9, 2.5, 14.5, 18.2, 21.5, 5.2, 26.5, 23.3, 45.3, 13.9, 9.6])
            ])
        self.aaChartView?.aa_drawChartWithChartModel(chartModel)
        
        let timeLabel = UILabel().then { obj in
            view.addSubview(obj)
            obj.text = "今天  09：30  更新"
            obj.textColor = dateColor
            obj.font = kMediumFont(10)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(40+270)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
