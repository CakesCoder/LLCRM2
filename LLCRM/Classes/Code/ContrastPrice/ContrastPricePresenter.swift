//
//  ContrastPricePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class ContrastPricePresenter: ContrastPricePresenterProtocols {
    var view: ContrastPriceViewProtocols?
    
    var router: ContrastPriceRouterProtocols?
    
    var interactor: ContrastPriceInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetContrastPrice(by params: Any?) {
        
    }
    
    func didGetContrastPriceInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetContrastPriceInteractorReceiveError(error: MyError?) {
        
    }
    
    
}
