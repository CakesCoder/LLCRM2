//
//  ContrastPriceEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class ContrastPriceEntity: ContrastPriceEntityProtocols {
    var interactor: ContrastPriceInteractorProtocols?
    
    func didContrastPriceReceiveData(by params: Any?) {
        
    }
    
    func didContrastPriceReceiveError(error: MyError?) {
        
    }
    
    func getContrastPriceRequest(by params: Any?) {
        
    }
    
    
}
