//
//  ContrastPriceInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class ContrastPriceInteractor: ContrastPriceInteractorProtocols {
    var presenter: ContrastPricePresenterProtocols?
    
    var entity: ContrastPriceEntityProtocols?
    
    func presenterRequestContrastPrice(by params: Any?) {
        
    }
    
    func didEntityContrastPriceReceiveData(by params: Any?) {
        
    }
    
    func didEntityContrastPriceReceiveError(error: MyError?) {
        
    }
    
    
}
