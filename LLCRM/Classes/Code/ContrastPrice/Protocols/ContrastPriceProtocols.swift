//
//  ContrastPriceProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/29.
//

import UIKit

class ContrastPriceProtocols: NSObject {

}

protocol ContrastPriceViewProtocols: AnyObject {
    var presenter: ContrastPricePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetContrastPricePresenterReceiveData(by params: Any?)
    func didGetContrastPricePresenterReceiveError(error: MyError?)
}

protocol ContrastPricePresenterProtocols: AnyObject{
    var view: ContrastPriceViewProtocols? { get set }
    var router: ContrastPriceRouterProtocols? { get set }
    var interactor: ContrastPriceInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetContrastPrice(by params: Any?)
    func didGetContrastPriceInteractorReceiveData(by params: Any?)
    func didGetContrastPriceInteractorReceiveError(error: MyError?)
}

protocol ContrastPriceInteractorProtocols: AnyObject {
    var presenter: ContrastPricePresenterProtocols? { get set }
    var entity: ContrastPriceEntityProtocols? { get set }
    
    func presenterRequestContrastPrice(by params: Any?)
    func didEntityContrastPriceReceiveData(by params: Any?)
    func didEntityContrastPriceReceiveError(error: MyError?)
}

protocol ContrastPriceEntityProtocols: AnyObject {
    var interactor: ContrastPriceInteractorProtocols? { get set }
    
    func didContrastPriceReceiveData(by params: Any?)
    func didContrastPriceReceiveError(error: MyError?)
    func getContrastPriceRequest(by params: Any?)
}

protocol ContrastPriceRouterProtocols: AnyObject {
    func pushToSellLeadsDetail(from previousView: UIViewController, forModel model: Any)
    func pushToAddSellLeads(from previousView: UIViewController)
}



