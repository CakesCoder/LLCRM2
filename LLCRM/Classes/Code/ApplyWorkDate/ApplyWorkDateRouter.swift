//
//  ApplyWorkDateRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class ApplyWorkDateRouter: ApplyWorkDateRouterProtocols {
    func pushToApplyWorkAdd(from previousView: UIViewController) {
        let nextView = ApplyWorkAddViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToMyAddBusinessList(from previousView: UIViewController) {
        
    }
    
    func pushToBusinessListDetail(from previousView: UIViewController) {
        
    }
}
