//
//  ApplyWorkDateInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class ApplyWorkDateInteractor: ApplyWorkDateInteractorProtocols {
    var presenter: ApplyWorkDatePresenterProtocols?
    
    var entity: ApplyWorkDateEntityProtocols?
    
    func presenterRequestApplyWorkDate(by params: Any?) {
        
    }
    
    func didEntityApplyWorkDateReceiveData(by params: Any?) {
        
    }
    
    func didEntityApplyWorkDateReceiveError(error: MyError?) {
        
    }
}
