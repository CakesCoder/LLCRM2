//
//  ApplyWorkDateProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/27.
//

import UIKit

class ApplyWorkDateProtocols: NSObject {

}

protocol ApplyWorkDateViewProtocols: AnyObject {
    var presenter: ApplyWorkDatePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetApplyWorkDatePresenterReceiveData(by params: Any?)
    func didGetApplyWorkDatePresenterReceiveError(error: MyError?)
}

protocol ApplyWorkDatePresenterProtocols: AnyObject{
    var view: ApplyWorkDateViewProtocols? { get set }
    var router: ApplyWorkDateRouterProtocols? { get set }
    var interactor: ApplyWorkDateInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetApplyWorkDate(by params: Any?)
    func didGetApplyWorkDateInteractorReceiveData(by params: Any?)
    func didGetApplyWorkDateInteractorReceiveError(error: MyError?)
}

protocol ApplyWorkDateInteractorProtocols: AnyObject {
    var presenter: ApplyWorkDatePresenterProtocols? { get set }
    var entity: ApplyWorkDateEntityProtocols? { get set }
    
    func presenterRequestApplyWorkDate(by params: Any?)
    func didEntityApplyWorkDateReceiveData(by params: Any?)
    func didEntityApplyWorkDateReceiveError(error: MyError?)
}

protocol ApplyWorkDateEntityProtocols: AnyObject {
    var interactor: ApplyWorkDateInteractorProtocols? { get set }
    
    func didApplyWorkDateReceiveData(by params: Any?)
    func didApplyWorkDateReceiveError(error: MyError?)
    func getApplyWorkDateRequest(by params: Any?)
}

protocol ApplyWorkDateRouterProtocols: AnyObject {
    func pushToApplyWorkAdd(from previousView: UIViewController)
    func pushToMyAddBusinessList(from previousView: UIViewController)
    func pushToBusinessListDetail(from previousView: UIViewController)
}

