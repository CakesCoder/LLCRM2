//
//  ApplyWorkDatePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class ApplyWorkDatePresenter: ApplyWorkDatePresenterProtocols {
    var view: ApplyWorkDateViewProtocols?
    
    var router: ApplyWorkDateRouterProtocols?
    
    var interactor: ApplyWorkDateInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetApplyWorkDate(by params: Any?) {
        
    }
    
    func didGetApplyWorkDateInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetApplyWorkDateInteractorReceiveError(error: MyError?) {
        
    }
}
