//
//  ApplyWorkDateEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/11/28.
//

import UIKit

class ApplyWorkDateEntity: ApplyWorkDateEntityProtocols {
    var interactor: ApplyWorkDateInteractorProtocols?
    
    func didApplyWorkDateReceiveData(by params: Any?) {
        
    }
    
    func didApplyWorkDateReceiveError(error: MyError?) {
        
    }
    
    func getApplyWorkDateRequest(by params: Any?) {
        
    }
}
