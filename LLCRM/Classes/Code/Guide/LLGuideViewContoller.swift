//
//  LLGuideViewContoller.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/11.
//

import UIKit

class LLGuideViewContoller: UIViewController, UIScrollViewDelegate {

    var scrollView:UIScrollView?
    var pageControl:UIPageControl?
    var startButton:UIButton?
    var presenter: GuidePresenter?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        presenter = GuidePresenter()
        presenter?.router = GuideRouter()
        
        presenter?.viewDidLoad()
        
        buildUI()
    }
    
    // ScrollView Delegate
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let offset = scrollView.contentOffset
        pageControl?.currentPage = Int(offset.x/kScreenWidth)
        
        if(pageControl?.currentPage == 2){
            startButton?.isHidden = false
        }else{
            startButton?.isHidden = true
        }
    }
    
    func buildUI(){
        scrollView = UIScrollView().then{ obj in
            obj.isPagingEnabled = true
            obj.showsHorizontalScrollIndicator = false
            obj.showsVerticalScrollIndicator = false
//            obj.isScrollEnabled = false
//            obj.isPagingEnabled = false
            obj.contentSize = CGSizeMake(3*kScreenWidth, kScreenHeight-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            obj.delegate = self
//            obj.backgroundColor = .red
            view.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.top.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        firstGuidePage()
        secondGuidePage()
        thirdGuidePage()
        
        pageControl = UIPageControl().then({ obj in
            obj.numberOfPages = 3
            obj.currentPage = 0
            obj.currentPageIndicatorTintColor = UIColor(0x4871C0)
            obj.pageIndicatorTintColor = UIColor(0xE3E3E3)
            obj.addTarget(self, action: #selector(pageChanged(_:)),
                                          for: .valueChanged)
            view.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.bottom.equalToSuperview().offset(-122-UIDevice.xp_tabBarFullHeight())
                make.centerX.equalToSuperview()
                make.width.equalTo(150)
                make.height.equalTo(30)
            }
        })
        
        startButton = UIButton().then({ obj in
            view.addSubview(obj)
            obj.backgroundColor = UIColor(0x4871C0)
            obj.titleLabel?.font = kMediumFont(15)
            obj.titleLabel?.textColor = UIColor(0xfff)
            obj.setTitle("立即体验", for: .normal)
            obj.isHidden = true
            obj.layer.cornerRadius = 6
            obj.addTarget(self, action: #selector(experienceClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.bottom.equalToSuperview().offset(-42-UIDevice.xp_tabBarFullHeight())
                make.centerX.equalToSuperview()
                make.width.equalTo(110)
                make.height.equalTo(40)
            }
        })
        
    }
    
    func firstGuidePage(){
        let firstGuideView = UIView().then { obj in
            scrollView?.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.height.equalTo(kScreenHeight)
            }
        }
        
        _ = UILabel().then { obj in
            firstGuideView.addSubview(obj)
            obj.text = "共享信息，提升客户满意度"
            obj.font = kMediumFont(24)
            obj.textColor = UIColor(0x4871C0)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(100)
                make.centerX.equalToSuperview()
            }
        }
        
        _ = UILabel().then({ obj in
            firstGuideView.addSubview(obj)
            obj.font = UIFont.systemFont(ofSize: 14.0)
            obj.text = "整合客户信息，提供更周到的服务。"
            obj.textColor = UIColor(0x999999)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(140)
                make.centerX.equalToSuperview()
            }
        })
        
        _ = UIImageView().then { obj in
            firstGuideView.addSubview(obj)
            obj.image = UIImage(named: "guideOne")
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(252)
                make.centerX.equalToSuperview()
            }
        }
    }
    
    func secondGuidePage(){
        let secondGuideView = UIView().then { obj in
            scrollView?.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(kScreenWidth)
                make.width.equalTo(kScreenWidth)
                make.height.equalTo(kScreenHeight)
            }
        }
        
        _ = UILabel().then { obj in
            secondGuideView.addSubview(obj)
            obj.text = "降低企业成本、提升员工效率"
            obj.font = kMediumFont(24)
            obj.textColor = UIColor(0x4871C0)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(100)
                make.centerX.equalToSuperview()
            }
        }
        
        _ = UILabel().then({ obj in
            secondGuideView.addSubview(obj)
            obj.font = kAMediumFont(14)
            obj.text = "有效管理客户，提升员工工作效率。"
            obj.textColor = UIColor(0x999999)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(140)
                make.centerX.equalToSuperview()
            }
        })
        
        _ = UIImageView().then { obj in
            secondGuideView.addSubview(obj)
            obj.image = UIImage(named: "guideTwo")
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(252)
                make.centerX.equalToSuperview()
            }
        }
    }
    
    func thirdGuidePage(){
        let thirdGuideView = UIView().then { obj in
            scrollView?.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(kScreenWidth*2)
                make.width.equalTo(kScreenWidth)
                make.height.equalTo(kScreenHeight)
            }
        }
        
        _ = UILabel().then { obj in
            thirdGuideView.addSubview(obj)
            obj.text = "专注于企业的CRM系统"
            obj.font = kMediumFont(24)
            obj.textColor = UIColor(0x4871C0)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(100)
                make.centerX.equalToSuperview()
            }
        }
        
        _ = UILabel().then({ obj in
            thirdGuideView.addSubview(obj)
            obj.font = kAMediumFont(14)
            obj.text = "有效管理客户，提升员工工作效率。"
            obj.textColor = UIColor(0x999999)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(140)
                make.centerX.equalToSuperview()
            }
        })
        
        _ = UIImageView().then { obj in
            thirdGuideView.addSubview(obj)
            obj.image = UIImage(named: "guideThree")
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(252)
                make.centerX.equalToSuperview()
            }
        }
    }
    
    @objc func experienceClick(){
        self.presenter?.router?.pushToLogin(from: self)
    }
    
    //点击页控件时事件处理
    @objc func pageChanged(_ sender:UIPageControl) {
        scrollView?.setContentOffset(CGPointMake(kScreenWidth*CGFloat(sender.currentPage), 0), animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
