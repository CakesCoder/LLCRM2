//
//  GuideProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

protocol GuideViewProtocols: AnyObject {
    // 持有一个presenter，帮我做事
    var presenter: GuidePresenterProtocols? { get set }
    
    /*
     这些方法，都是UI回调相关的方法
     让我的presenter帮我做事情，等presenter给我回调结果就好了
     */
    func reloadView()
    func showLoading()
    func showError()
    func hideLoading()
}

protocol GuidePresenterProtocols: AnyObject{
    var view: GuideViewProtocols? { get set }
    var router: GuideRouterProtocols? { get set }
    var interactor: GuideInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol GuideInteractorProtocols: AnyObject {
    var presenter: GuidePresenterProtocols? { get set }
    var entity: GuideEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol GuideEntityProtocols: AnyObject {
    var interactor: GuideInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol GuideRouterProtocols: AnyObject {
    func pushToLogin(from previousView: UIViewController)
}

