//
//  GuideRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

class GuideRouter: GuideRouterProtocols {
    func pushToLogin(from previousView: UIViewController) {
        let loginVC = LoginViewController()
        previousView.navigationController?.pushViewController(loginVC, animated: true)
    }
}
