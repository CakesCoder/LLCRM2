//
//  GuidePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/16.
//

import UIKit

class GuidePresenter: GuidePresenterProtocols {
    var view: GuideViewProtocols?
    var router: GuideRouterProtocols?
    var interactor: GuideInteractorProtocols?
    var params: Any?
    
    func viewDidLoad() {
        
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
