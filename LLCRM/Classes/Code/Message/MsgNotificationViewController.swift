//
//  MsgNotificationViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/14.
//

import UIKit

class MsgNotificationViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "新消息通知"
        
        let headView = UIView().then { obj in
            obj.backgroundColor = .white
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight)
        }
        
        let topLineView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = bgColor
            
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let msgView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        _ = UILabel().then { obj in
            msgView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor42
            obj.text = "新消息通知"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UILabel().then({ obj in
            msgView.addSubview(obj)
            obj.font = kRegularFont(12)
            obj.textColor = blackTextColor46
            obj.text = "已开启"
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-28)
                make.centerY.equalToSuperview()
            }
        })
        
        _ = UIButton().then({ obj in
            msgView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        })
        
        let msgHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(60)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
            }
        }
        
        let msgHintLabel = UILabel().then { obj in
            msgHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "请在iPhone的“设置”-“通知”功能中，找到应用程序“零瓴软件”更改。"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let msgDetailView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(110)
            }
        }

        let msgDetailTitleLabel = UILabel().then { obj in
            msgDetailView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = "通知显示信息详情屏"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }

        let msgDetailSwitch = UISwitch().then { obj in
            msgDetailView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let msgDetailHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(160)
            }
        }
        
        _ = UILabel().then { obj in
            msgDetailHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "关闭后，当收到消息时，通知提示将不显示发信人和内容摘要。"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let soundView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(210)
            }
        }

        let soundHintLabel = UILabel().then { obj in
            soundView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = "声音"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }

        let soundSwitch = UISwitch().then { obj in
            soundView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        _ = UIView().then({ obj in
            soundView.addSubview(obj)
            obj.backgroundColor = lineColor18
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
                make.height.equalTo(0.5)
            }
        })
        
        let shakeView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(260)
            }
        }

        let shakeTitleLabel = UILabel().then { obj in
            shakeView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = "振动"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }

        let shakeSwitch = UISwitch().then { obj in
            shakeView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let shakeHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(310)
            }
        }
        
        _ = UILabel().then { obj in
            shakeHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "当APP运行时，你可以设置是否需要声音和震动。"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        let disturbView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(360)
            }
        }
        
        let disturbTitleLabel = UILabel().then { obj in
            disturbView.addSubview(obj)
            obj.font = kRegularFont(16)
            obj.textColor = blackTextColor
            obj.text = "振动"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(28)
                make.centerY.equalToSuperview()
            }
        }
        
        let disturbSwitch = UISwitch().then { obj in
            disturbView.addSubview(obj)
            obj.onTintColor = bluebgColor
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let disturbHintView = UIView().then { obj in
            headView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(50)
                make.top.equalToSuperview().offset(410)
            }
        }
        
        _ = UILabel().then { obj in
            disturbHintView.addSubview(obj)
            obj.numberOfLines = 2
            obj.textColor = blackTextColor46
            obj.font = kRegularFont(12)
            obj.text = "开启后，在设定的时间范围内，手机不会震动与发出提示音。"
            
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(0)
                make.left.equalToSuperview().offset(15)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-15)
            }
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.right.bottom.equalToSuperview().offset(-0)
            }
        })
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
