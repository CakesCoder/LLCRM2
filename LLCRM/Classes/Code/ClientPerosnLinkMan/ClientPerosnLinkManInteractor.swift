//
//  ClientPerosnLinkManInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnLinkManInteractor: ClientPerosnLinkManInteractorProtocols {
    var presenter: ClientPerosnLinkManPresenterProtocols?
    
    var entity: ClientPerosnLinkManEntityProtocols?
    
    func presenterRequestClientPerosnLinkMan(by params: Any?) {
        
    }
    
    func didEntityClientPerosnLinkManReceiveData(by params: Any?) {
        
    }
    
    func didEntityClientPerosnLinkManReceiveError(error: MyError?) {
         
    }
    
    
}
