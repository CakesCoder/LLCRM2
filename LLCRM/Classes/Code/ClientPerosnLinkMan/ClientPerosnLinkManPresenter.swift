//
//  ClientPerosnLinkManPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

class ClientPerosnLinkManPresenter: ClientPerosnLinkManPresenterProtocols {
    var view: ClientPerosnLinkManViewProtocols?
    
    var router: ClientPerosnLinkManRouterProtocols?
    
    var interactor: ClientPerosnLinkManInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetClientPerosnLinkMan(by params: Any?) {
        
    }
    
    func didGetClientPerosnLinkManInteractorReceiveData(by params: Any?) {
        
    }
    
    func didGetClientPerosnLinkManInteractorReceiveError(error: MyError?) {
        
    }
    
}
