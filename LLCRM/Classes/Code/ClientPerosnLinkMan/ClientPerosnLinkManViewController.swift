//
//  ClientPerosnLinkManViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit
//import JXPagingView

extension ClientPerosnLinkManViewController: UIScrollViewDelegate {
    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.listViewDidScrollCallback?(scrollView)
    }
}

extension ClientPerosnLinkManViewController: JXPagingViewListViewDelegate {
    public func listView() -> UIView {
        return self.view
    }
    
    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
        self.listViewDidScrollCallback = callback
    }

    public func listScrollView() -> UIScrollView {
        return self.pagingScrollView!
    }
}

class ClientPerosnLinkManViewController: BaseViewController {
    
    var listViewDidScrollCallback: ((UIScrollView) -> ())?
    var pagingScrollView:UIScrollView? = UIScrollView()
    var presenter: ClientPerosnLinkManPresenterProtocols?
    var model:ClientPerosonDetailModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 5*45)
            obj.backgroundColor = bgColor
        }
        
        let baseInfosView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(5*45)
            }
        }
        
        let baseInfosHeadView = UIView().then { obj in
            baseInfosView.addSubview(obj)
            obj.backgroundColor = .white
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalToSuperview().offset(0)
                make.height.equalTo(45)
            }
        }
        
        let headTagView = UIView().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.backgroundColor = workTagColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.centerY.equalToSuperview()
                make.width.equalTo(3)
                make.height.equalTo(11)
            }
        }
        
        let headLabel = UILabel().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.text = "联系人"
            obj.textColor = blackTextColor
            obj.font = kMediumFont(15)
            obj.snp.makeConstraints { make in
                make.left.equalTo(headTagView.snp.right).offset(5)
                make.centerY.equalToSuperview()
            }
        }
        
        let addButton = UIButton().then { obj in
            baseInfosHeadView.addSubview(obj)
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(blackTextColor, for: .normal)
            obj.titleLabel?.font = kMediumFont(24)
            obj.addTarget(self, action: #selector(addClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalToSuperview()
            }
        }
        
        let baseInfos = ["姓名：", "职位：", "手机号：", "电话号：", "邮箱："]
        
//        let contentInfos = ["王小虎/男", "采购经理", "138 9209 9839", "0393-9009 9090", "628983128@qq.com"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                baseInfosView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(45+i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            _ = UILabel().then { obj in
                v.addSubview(obj)
                let hintAttribute = NSMutableAttributedString(string:baseInfos[i])
                hintAttribute.yy_color = blackTextColor68
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                    make.width.equalToSuperview().offset(100)
                    make.height.equalToSuperview().offset(20)
                }
            }
            
            
            let contentLabel = UILabel().then { obj in
                v.addSubview(obj)
                var contentStr = "--"
                let dataList = model?.clientClueContactSimpleParamList
                if i == 0{
                    if dataList != nil{
                        contentStr = (model?.clientClueContactSimpleParamList?[0].name ?? "")+"/"+(model?.clientClueContactSimpleParamList?[0].gender ?? "")!
                    }
                }else if i == 1{
                    if dataList != nil{
                        contentStr = (model?.clientClueContactSimpleParamList?[0].position) ?? "--"
                    }
                }else if i == 2{
                    if dataList != nil{
                        contentStr = (model?.clientClueContactSimpleParamList?[0].phone) ?? "--"
                    }
                }else if i == 3{
                    contentStr = "--"
                }else if i == 4{
                    if dataList != nil{
                        contentStr = (model?.clientClueContactSimpleParamList?[0].email) ?? "--"
                    }
                }
                let hintAttribute = NSMutableAttributedString(string:contentStr)
                if i == baseInfos.count-1{
                    hintAttribute.yy_color = bluebgColor
                }else{
                    hintAttribute.yy_color = blackTextColor81
                }
                hintAttribute.yy_font = kMediumFont(12)
                obj.attributedText = hintAttribute
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(120)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i == 2{
                _ = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.backgroundColor = .red
                    obj.snp.makeConstraints { make in
                        make.left.equalTo(contentLabel.snp.right).offset(15)
                        make.centerY.equalToSuperview()
                        make.width.height.equalTo(20)
                    }
                }
            }
            
            
            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        _ = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight()-UIDevice.xp_navigationFullHeight())
            }
        })
        
        cofing()
    }
    
    @objc func addClick(_ button: UIButton){
        
    }
    
    
    func cofing(){
        let router = ClientPerosnLinkManRouter()
        
        presenter = ClientPerosnLinkManPresenter()
        
        presenter?.router = router
        
        let entity = ClientPerosnLinkManEntity()
        
        let interactor = ClientPerosnLinkManInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension ClientPerosnLinkManViewController: ClientPerosnLinkManViewProtocols{
    func showLoading() {
        
    }
    
    func showError() {
        
    }
    
    func hideLoading() {
        
    }
    
    func didGetClientPerosnLinkManPresenterReceiveData(by params: Any?) {
        
    }
    
    func didGetClientPerosnLinkManPresenterReceiveError(error: MyError?) {
        
    }
}
