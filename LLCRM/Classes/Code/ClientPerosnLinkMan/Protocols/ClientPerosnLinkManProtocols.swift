//
//  ClientPerosnLinkManProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/29.
//

import UIKit

protocol ClientPerosnLinkManViewProtocols: AnyObject {
    var presenter: ClientPerosnLinkManPresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetClientPerosnLinkManPresenterReceiveData(by params: Any?)
    func didGetClientPerosnLinkManPresenterReceiveError(error: MyError?)
}

protocol ClientPerosnLinkManPresenterProtocols: AnyObject{
    var view: ClientPerosnLinkManViewProtocols? { get set }
    var router: ClientPerosnLinkManRouterProtocols? { get set }
    var interactor: ClientPerosnLinkManInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetClientPerosnLinkMan(by params: Any?)
    func didGetClientPerosnLinkManInteractorReceiveData(by params: Any?)
    func didGetClientPerosnLinkManInteractorReceiveError(error: MyError?)
}

protocol ClientPerosnLinkManInteractorProtocols: AnyObject {
    var presenter: ClientPerosnLinkManPresenterProtocols? { get set }
    var entity: ClientPerosnLinkManEntityProtocols? { get set }
    
    func presenterRequestClientPerosnLinkMan(by params: Any?)
    func didEntityClientPerosnLinkManReceiveData(by params: Any?)
    func didEntityClientPerosnLinkManReceiveError(error: MyError?)
}

protocol ClientPerosnLinkManEntityProtocols: AnyObject {
    var interactor: ClientPerosnLinkManInteractorProtocols? { get set }
    
    func didClientPerosnLinkManReceiveData(by params: Any?)
    func didClientPerosnLinkManReceiveError(error: MyError?)
    func getClientPerosnLinkManRequest(by params: Any?)
}

protocol ClientPerosnLinkManRouterProtocols: AnyObject {
    func pushToAddPrice(from previousView: UIViewController)
}
