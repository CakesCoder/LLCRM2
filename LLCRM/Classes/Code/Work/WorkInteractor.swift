//
//  WorkInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit

class WorkInteractor: WorkInteractorProtocols {
    var presenter: WorkPresenterProtocols?
    
    var entity: WorkEntityProtocols?
    
    func interactorRetrieveData() {
        
    }
    
    func entityRetrieveData() {
        
    }
    
    func didEntityReceiveData() {
        
    }
    
    func didEntityReceiveError() {
        
    }
    
}
