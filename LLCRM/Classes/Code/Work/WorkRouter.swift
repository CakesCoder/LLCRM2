//
//  WorkRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import Foundation
import UIKit

class WorkRouter: WorkRouterProtocols {
    
    func pushToNextVC(from previousView: UIViewController) {
        let nextView = WorkShareViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushApproval(from previousView: UIViewController) {
        let nextView = ApprovalViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushAddApproval(from previousView: UIViewController) {
//        let nextView = AddApprpvalViewController()
        let nextView = ApprovalViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushAddTask(from previousView: UIViewController){
        let nextView = AddTaskViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
        
    func pushAddPlan(from previousView: UIViewController){
        let nextView = AddPlanViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushSchedule(from previousView: UIViewController){
        let nextView = ScheduleViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
