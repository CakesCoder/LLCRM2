//
//  WorkPresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit

class WorkPresenter: WorkPresenterProtocols {
    var view: WorkViewProtocols?
    var router: WorkRouterProtocols?
    var interactor: WorkInteractorProtocols?
    var params: Any?

    func viewDidLoad() {
    }
    
    func didInteractorReceiveData() {
        
    }
    
    func didInteractorReceiveError() {
        
    }
    

}
