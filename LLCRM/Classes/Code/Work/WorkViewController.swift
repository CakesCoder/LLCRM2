//
//  WorkViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit
import JFPopup

class WorkViewController: BaseViewController {
    var presenter: WorkPresenterProtocols?
    var dateLabel: UILabel?
    let weekArray = ["日","一","二","三","四","五","六"]
    var mainTableView: UITableView?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.blueNavTheme()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        navigationItem.title = "工作台"
        self.title = "工作台"
        
        self.navigationController?.whiteNavTheme()
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(WorkViewController.backClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem.createAddBarbuttonItem(name: "+", target: self, action: #selector(WorkViewController.addClick))
        
        buildUI()
        
        presenter = WorkPresenter()
        presenter?.router = WorkRouter()
        
        presenter?.viewDidLoad()
    }
    
    @objc func backClick(){
        
    }
    
    @objc func addClick(){
        
    }
    
    func buildUI(){
        
        mainTableView = UITableView().then { obj in
            view.addSubview(obj)
            obj.tableHeaderView = workHeadView
            obj.register(WorkTableViewCell.self, forCellReuseIdentifier: "WorkTableViewCell")
            obj.register(StaffTableViewCell.self, forCellReuseIdentifier: "StaffTableViewCell")
            obj.delegate = self
            obj.dataSource = self
            obj.separatorStyle = .none
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight())
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        _ = UIButton().then { obj in
            view.addSubview(obj)
            obj.setImage(UIImage(named: "addButton"), for: .normal)
            obj.setImage(UIImage(named: "addButton"), for: .selected)
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-UIDevice.xp_tabBarFullHeight())
            }
        }
    }
    
    lazy var noDataView: UIView = {
        let noDataView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: kScreenWidth, height: 230)
        }
        
        let noDataImageV = UIImageView().then { obj in
            noDataView.addSubview(obj)
            obj.image = UIImage(named: "noData")
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(38)
                make.centerX.equalTo(noDataView)
            }
        }
        
        _ = UIButton().then({ obj in
            noDataView.addSubview(obj)
            obj.setTitle("马上安排", for: .normal)
            obj.titleLabel?.font = kRegularFont(12)
            obj.setTitleColor(workTagColor, for: .normal)
            obj.layer.cornerRadius = 15
            obj.layer.borderWidth = 0.5
            obj.layer.borderColor = workTagColor.cgColor
            obj.snp.makeConstraints { make in
                make.top.equalTo(noDataImageV.snp.bottom).offset(25)
                make.centerX.equalTo(noDataView)
                make.width.equalTo(90)
                make.height.equalTo(30)
            }
        })
        
        return noDataView
    }()
    
    lazy var dataShowView: UIView = {
        let dataView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, 125)
        }
        
        _ = UIView().then({ obj in
            dataView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        })
        
        let contentView = UIView().then { obj in
            dataView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(30)
                make.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-30)
                make.bottom.equalToSuperview().offset(-35)
            }
        }
        
        for i in 0..<4{
            let featureView = UIView().then { obj in
                contentView.addSubview(obj)
                let w = (kScreenWidth-60)/4
                obj.frame = CGRect(x: CGFloat(w*CGFloat(i)), y: 0, width: w, height: w)
            }
            
            let numLabel = UILabel().then { obj in
                featureView.addSubview(obj)
                obj.text = "10"
                obj.font = kAMediumFont(18)
                obj.textColor = blackTextColor16
                obj.snp.makeConstraints { make in
                    make.top.equalToSuperview().offset(35)
                    make.centerX.equalToSuperview()
                }
            }
            
            if  i < 3{
                let lineView = UIView().then { obj in
                    featureView.addSubview(obj)
                    obj.backgroundColor = lineColor10
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-0)
                        make.top.equalToSuperview().offset(44)
                        make.width.equalTo(0.5)
                        make.height.equalTo(28)
                    }
                }
            }
            
            let nameLabel = UILabel().then { obj in
                featureView.addSubview(obj)
                if i == 0{
                    obj.text = "日志"
                }else if i == 1{
                    obj.text = "会议"
                }else if i == 2{
                    obj.text = "审批"
                }else{
                    obj.text = "任务"
                }
                obj.font = kRegularFont(12)
                obj.textColor = blackTextColor4
                obj.snp.makeConstraints { make in
                    make.top.equalTo(numLabel.snp.bottom).offset(10)
                    make.centerX.equalToSuperview()
                }
            }
        }
        
        return dataView
    }()
    
    
    lazy var workHeadView: UIView = {
        let headView = UIView().then { obj in
            obj.frame = CGRect(x: 0, y: 0, width: Int(kScreenWidth), height: 543)
        }
        
        let tagView = UIView().then { obj in
            obj.backgroundColor = workTagColor
            obj.layer.cornerRadius = 2
            headView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(21)
                make.width.equalTo(2)
                make.height.equalTo(11)
            }
        }
        
        let titleLabel = UILabel().then { obj in
            headView.addSubview(obj)
            obj.textColor = blackTextColor15
            obj.text = "我的日程"
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.centerY.equalTo(tagView)
            }
        }
        
        let lookButton = UIButton().then { obj in
            headView.addSubview(obj)
            obj.setTitle("查看更多 >", for: .normal)
            obj.titleLabel?.font = kRegularFont(11)
            obj.setTitleColor(bluebgColor, for: .normal)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagView)
            }
        }
        
        dateLabel = UILabel().then({ obj in
            headView.addSubview(obj)
            obj.text = "2022 年 05 月"
            obj.textAlignment = .center
            obj.textColor = blackTextColor5
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(50)
                make.centerX.equalToSuperview()
            }
        })
        
        let weekView = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(dateLabel!.snp.bottom).offset(30)
                make.height.equalTo(90)
                make.width.equalTo(kScreenWidth-30)
            }
        }

        let weekWidth = (kScreenWidth-30) / 7
        for i in 0 ..< 7 {
            let weekLabel = UILabel.init(frame: CGRect.init(x: Int(Float(i)*Float(weekWidth)) , y: 0, width: Int(weekWidth), height: 40))
            weekLabel.backgroundColor = UIColor.clear
            weekLabel.text = weekArray[i]
            weekLabel.font = kAMediumFont(15)
            weekLabel.textAlignment = .center
            weekLabel.textColor = blackTextColor6
            weekView.addSubview(weekLabel)
        }

        let weekLineView = UIView().then { obj in
            weekView.addSubview(obj)
            obj.backgroundColor = lineColor3
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalToSuperview().offset(45)
                make.height.equalTo(0.5)
            }
        }

        for i in 0 ..< 7 {
            let weekButton = UIButton.init(frame: CGRect.init(x: Int(Float(i)*Float(weekWidth)) , y: 60, width: Int(weekWidth), height: 40))
            weekView.addSubview(weekButton)
            weekButton.setTitleColor(blackTextColor7, for: .normal)
            weekButton.setTitle(String(i), for: .normal)
        }

        let noDataView = noDataView.then{ obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(weekView.snp.bottom).offset(7)
                make.height.equalTo(230)
            }
        }

        let lineView = UIView().then({ obj in
            headView.addSubview(obj)
            obj.backgroundColor = lineColor
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(noDataView.snp.bottom).offset(0)
                make.height.equalTo(10)
            }
        })

        let dataShowView = dataShowView.then{ obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.width.equalTo(kScreenWidth)
                make.top.equalTo(lineView.snp.bottom).offset(0)
                make.height.equalTo(125)
            }
        }

        return headView
    }()
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}


extension WorkViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("self.dataSource.array.count = \(self.dataSource.array.count)")
//        return self.dataSource.array.count
        return 20
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0{
            return 830
        }
        return 550
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "WorkTableViewCell", for: indexPath) as! WorkTableViewCell
            cell.moreBlock = {
                self.popup.bottomSheet { [weak self] in
                    let showView = WorkCreateView(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
                    showView.featureBlock = {[weak self] tag in
                        if tag == 1000{
                            self?.presenter?.router?.pushToNextVC(from: self!)
                        }else if tag == 1001{
                            self?.presenter?.router?.pushAddPlan(from: self!)
                        }else if tag == 1002{
                            self?.presenter?.router?.pushAddApproval(from: self!)
                        }else if tag == 1003{
                            self?.presenter?.router?.pushAddTask(from: self!)
                        }else if tag == 1004{
                            self?.presenter?.router?.pushSchedule(from: self!)
                        }
                        else{
                            
                        }
                        self?.popup.dismissPopup()
                    }
                    showView.cancelBlock = { [weak self] in
                        self?.popup.dismissPopup()
                    }
                    return showView
                }
            }
            cell.forwardBlock = {
                self.popup.bottomSheet { [weak self] in
                    let showView = WorkMoreView()
                    showView.cancelBlock = { [weak self] in
                        self?.popup.dismissPopup()
                    }
                    return showView
                    
                }
            }
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "StaffTableViewCell", for: indexPath)
        return cell
    }
}

//extension WorkViewController: ViewProtocols{
//    
//    func reloadView(){
//        if let presenter = presenter as? Presenters,
//           let data = presenter.result as? ChannelModelArray {
////            tableView?.setData(data: data)
//        }
//    }
//    
//    func showLoading(){
////        PKHUD.sharedHUD.show(onView: view)
//    }
//    
//    func showError(){
////        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
//    }
//    
//    func hideLoading(){
////        PKHUD.sharedHUD.hide(true)
//    }
//}
