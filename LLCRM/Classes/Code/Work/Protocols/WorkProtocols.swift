//
//  WorkRouterProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/8.
//

import UIKit

protocol WorkViewProtocols: AnyObject {
    var presenter: WorkPresenterProtocols? { get set }
    func reloadData()
    
    func didPresenterReceiveData()
    func didPresenterReceiveError()
}

protocol WorkPresenterProtocols: AnyObject{
    var view: WorkViewProtocols? { get set }
    var router: WorkRouterProtocols? { get set }
    var interactor: WorkInteractorProtocols? { get set }
    var params: Any? { get set }
    
    func viewDidLoad()
    
    
    func didInteractorReceiveData()
    func didInteractorReceiveError()
}

protocol WorkInteractorProtocols: AnyObject {
    var presenter: WorkPresenterProtocols? { get set }
    var entity: WorkEntityProtocols? { get set }
    
    func interactorRetrieveData()
    func entityRetrieveData()
    func didEntityReceiveData()
    func didEntityReceiveError()
}

protocol WorkEntityProtocols: AnyObject {
    var interactor: WorkInteractorProtocols? { get set }
    
    func retrieveData()
    func didReceiveData()
}

protocol WorkRouterProtocols: AnyObject {
    func pushToNextVC(from previousView: UIViewController)
    func pushApproval(from previousView: UIViewController)
    func pushAddApproval(from previousView: UIViewController)
    func pushAddTask(from previousView: UIViewController)
    func pushAddPlan(from previousView: UIViewController)
    func pushSchedule(from previousView: UIViewController)
}

