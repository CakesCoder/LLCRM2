//
//  WorkTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/5.
//

import UIKit

class WorkTableViewCell: UITableViewCell {
    
    var PopMenView:UIView?
    var moreBlock: (() -> ())?
    var forwardBlock: (() -> ())?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    lazy var popMenView: UIView = {
        let menView = UIView().then { obj in
            obj.frame = CGRect(x: kScreenWidth - 15 - 98, y: 30, width: 81, height: 98)
            // shadowCode
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.09).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 4
            obj.backgroundColor = .blue
        }
        
//        let arrowView=UIView(frame: CGRect(x: 0, y: 10, width: 50, height: 50))
        
//        let context = UIGraphicsGetCurrentContext()
//        context?.beginPath()
//        context?.move(to: CGPoint(x: 20, y: 50))
//        context?.addLine(to: CGPoint(x: 10, y: 30))
//        context?.addLine(to: CGPoint(x: 60, y: 20))
//        context?.closePath()
//        context?.fillPath()
//        context?.drawPath(using: .stroke)
        
//        for i in 0..<4{
//            let popButton = UIButton().then { obj in
//                menView.addSubview(obj)
//                obj.frame = CGRect(x: 0, y: i*98, width: 81, height: 98)
//                if i == 0{
//                    obj.setTitle("全部", for: .normal)
//                }else if i == 1{
//                    obj.setTitle("我发出的", for: .normal)
//                }else if i == 2{
//                    obj.setTitle("部门发出", for: .normal)
//                }else{
//                    obj.setTitle("部门收到", for: .normal)
//                }
//
//            }
//        }
        
        return menView
    }()
    
    func buildUI(){
        self.selectionStyle = .none
        
        let bgView = UIView().then { obj in
            contentView.addSubview(obj)
            // shadowCode
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 8
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(5)
                make.right.equalToSuperview().offset(-5)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let tagView = UIView().then { obj in
            obj.backgroundColor = workTagColor
            obj.layer.cornerRadius = 2
            bgView.addSubview(obj)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(21)
                make.width.equalTo(2)
                make.height.equalTo(11)
            }
        }

        let titleLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.textColor = blackTextColor15
            obj.text = "全部"
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(20)
                make.centerY.equalTo(tagView)
            }
        }
        
        let lookButton = UIButton().then { obj in
            bgView.addSubview(obj)
            obj.setImage(UIImage(named: "work_status"), for: .normal)
            obj.setImage(UIImage(named: "work_status"), for: .selected)
            obj.addTarget(self, action: #selector(statusClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-15)
                make.centerY.equalTo(tagView)
            }
        }
        
        let popView = popMenView.then { obj in
            obj.frame = CGRect(x: kScreenWidth - 15 - 98, y: 30, width: 81, height: 98)
            obj.backgroundColor = .white
            bgView.addSubview(obj)
        }
        
        for i in 0..<4{
            let btn = UIButton().then { obj in
                popView.addSubview(obj)
                obj.frame = CGRectMake(0, CGFloat(i*24), 81, 24)
                obj.titleLabel?.font = kRegularFont(10)
                obj.setTitleColor(blackTextColor3, for: .normal)
                if i == 0{
                    obj.setTitle("全部", for: .normal)
                }else if i == 1{
                    obj.setTitle("我发出的", for: .normal)
                }else if i == 2{
                    obj.setTitle("部门发出", for: .normal)
                }else if i == 3{
                    obj.setTitle("部门收到", for: .normal)
                }
            }
        }
        
        let imageV = UIImageView().then { obj in
            bgView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "work_icon")
            obj.layer.cornerRadius = 4
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(50)
                make.width.height.equalTo(44)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "等风来"
            obj.textColor = blackTextColor17
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.top.equalToSuperview().offset(50)
            }
        }
        
        let subLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.textColor = blackTextColor4
            obj.text = "零瓴团队团队"
            obj.font = kRegularFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel.snp.right).offset(5)
                make.centerY.equalTo(nameLabel)
            }
        }
        
        let dateLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.textColor = blackTextColor18
            obj.font = kRegularFont(12)
            obj.text = "今天09：35  来自iPhone"
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.top.equalTo(nameLabel.snp.bottom).offset(5)
            }
        }
        
        let clientTitleLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "客户拜访"
            obj.textColor = blackTextColor8
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(imageV.snp.bottom).offset(15)
            }
        }
        
        let clientView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = blackTextColor19
            obj.layer.cornerRadius = 6
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.top.equalTo(clientTitleLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
            }
        }
        
        let clientContentLabel = UILabel().then { obj in
            clientView.addSubview(obj)
            obj.textColor = blackTextColor17
            obj.font = kAMediumFont(12)
            obj.text = "拜访情况说明"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalToSuperview().offset(12)
                make.bottom.equalToSuperview().offset(-15)
            }
        }
        
        let visithintLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "走访"
            obj.textColor = blackTextColor8
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(clientView.snp.bottom).offset(15)
            }
        }
        
        let visitView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = blackTextColor19
            obj.layer.cornerRadius = 6
            obj.layer.masksToBounds = true
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(visithintLabel.snp.bottom).offset(5)
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
            }
        }
        
        let visitHintImageV = UIImageView().then { obj in
            visitView.addSubview(obj)
            obj.backgroundColor = UIColor("4A4A4A")
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(5)
                make.top.equalToSuperview().offset(12)
                make.width.height.equalTo(14)
            }
        }
        
        let visitTitleLabel = UILabel().then { obj in
            visitView.addSubview(obj)
            obj.text = "外勤动作"
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(12)
            obj.snp.makeConstraints { make in
                make.left.equalTo(visitHintImageV.snp.right).offset(3)
                make.centerY.equalTo(visitHintImageV)
            }
        }
        
        let lineView = UIView().then { obj in
            visitView.addSubview(obj)
            obj.backgroundColor = lineColor11
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(visitTitleLabel.snp.bottom).offset(10)
                make.height.equalTo(0.5)
            }
        }
        
        let visitContentImageV =  UIImageView().then { obj in
            visitView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "work_image")
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalTo(lineView.snp.bottom).offset(15)
                make.width.height.equalTo(80)
            }
        }
        
        let lineView2 = UIView().then { obj in
            visitView.addSubview(obj)
            obj.backgroundColor = lineColor11
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(visitContentImageV.snp.bottom).offset(15)
                make.height.equalTo(0.5)
            }
        }
        
        let detailView = UIView().then { obj in
            visitView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(lineView2.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        var t_view:UIView? = nil
        for i in 0..<4{
            let detailContentView = UIView().then { obj in
                detailView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    if i == 0{
                        make.top.equalToSuperview().offset(10)
                    }else{
                        make.top.equalTo(t_view!.snp.bottom).offset(10)
                    }
                    make.right.equalToSuperview().offset(-10)
                    if i == 3{
                        make.bottom.equalToSuperview().offset(-10)
                    }else{
                        make.height.equalTo(20)
                    }
                }
                t_view = obj
            }
            
            let headLabel = UILabel().then { obj in
                detailContentView.addSubview(obj)
                obj.textColor = blackTextColor17
                obj.font = kAMediumFont(12)
                obj.text = "1、外勤照片"
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    make.top.equalToSuperview().offset(2)
                    make.bottom.equalToSuperview().offset(-2)
                }
            }
            
            _ = UILabel().then({ obj in
                detailContentView.addSubview(obj)
                obj.textColor = blackTextColor17
                obj.font = kAMediumFont(12)
                obj.text = "拜访情况说明：走访"
                obj.snp.makeConstraints { make in
                    make.left.equalTo(headLabel.snp.right).offset(27)
                    make.centerY.equalTo(headLabel)
                }
            })
            
            let signView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = blackTextColor19
                obj.layer.cornerRadius = 6
                obj.layer.masksToBounds = true
                
                obj.snp.makeConstraints { make in
                    make.top.equalTo(visitView.snp.bottom).offset(10)
                    make.left.equalToSuperview().offset(10)
                    make.right.equalToSuperview().offset(-10)
                    make.height.equalTo(172)
                }
            }
            
            let topSignView = UIView().then { obj in
                signView.addSubview(obj)
                
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(86)
                }
            }
            
            let topSignHintImageV = UIImageView().then { obj in
                signView.addSubview(obj)
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(6)
                    make.top.equalToSuperview().offset(12)
                    make.width.equalTo(13)
                    make.height.equalTo(15)
                }
            }
            
            let topSignHintTitleLabel = UILabel().then { obj in
                signView.addSubview(obj)
                obj.text = "签到"
                obj.textColor = blackTextColor16
                obj.font = kAMediumFont(12)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(topSignHintImageV.snp.right).offset(3)
                    make.centerY.equalTo(topSignHintImageV)
                }
            }
            
            let topSignAddressLabel = UILabel().then { obj in
                signView.addSubview(obj)
                obj.text = "地点：广东省深圳市南山区桃源街道平山社区云谷二期"
                obj.textColor = blackTextColor17
                obj.font = kAMediumFont(12)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.top.equalTo(topSignHintTitleLabel.snp.bottom).offset(10)
                }
            }
            
            let topSignTimeLabel = UILabel().then { obj in
                signView.addSubview(obj)
                obj.text = "2022-04-29  09：20"
                obj.textColor = blackTextColor20
                obj.font = kAMediumFont(12)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.top.equalTo(topSignAddressLabel.snp.bottom).offset(10)
                }
            }
            
            let topSignLineView = UIView().then { obj in
                signView.addSubview(obj)
                obj.backgroundColor = blackTextColor19
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.top.equalTo(topSignView.snp.bottom).offset(0)
                    make.height.equalTo(0.5)
                }
            }
            
            let bottomSignView = UIView().then { obj in
                signView.addSubview(obj)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(86)
                    make.top.equalTo(topSignLineView.snp.bottom).offset(0)
                }
            }
            
            let bottomSignHintImageV = UIImageView().then { obj in
                bottomSignView.addSubview(obj)
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(6)
                    make.top.equalToSuperview().offset(12)
                    make.width.equalTo(13)
                    make.height.equalTo(15)
                }
            }
            
            let bottomSignHintTitleLabel = UILabel().then { obj in
                bottomSignView.addSubview(obj)
                obj.text = "签退"
                obj.textColor = blackTextColor16
                obj.font = kAMediumFont(12)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(bottomSignHintImageV.snp.right).offset(3)
                    make.centerY.equalTo(bottomSignHintImageV)
                }
            }
            
            let bottomSignAddressLabel = UILabel().then { obj in
                bottomSignView.addSubview(obj)
                obj.text = "地点：广东省深圳市南山区桃源街道平山社区云谷二期"
                obj.textColor = blackTextColor17
                obj.font = kAMediumFont(12)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.top.equalTo(bottomSignHintTitleLabel.snp.bottom).offset(10)
                }
            }
            
            let bottomSignTimeLabel = UILabel().then { obj in
                bottomSignView.addSubview(obj)
                obj.text = "2022-04-29  09：20"
                obj.textColor = blackTextColor20
                obj.font = kAMediumFont(12)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.top.equalTo(bottomSignAddressLabel.snp.bottom).offset(10)
                }
            }
            
            let crmView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = blackTextColor19
                obj.layer.cornerRadius = 6
                obj.layer.masksToBounds = true
                
                obj.snp.makeConstraints { make in
                    make.top.equalTo(signView.snp.bottom).offset(10)
                    make.left.equalToSuperview().offset(10)
                    make.right.equalToSuperview().offset(-10)
                    make.height.equalTo(57)
                }
            }
            
            let crmHintImageV = UIImageView().then { obj in
                crmView.addSubview(obj)
                obj.backgroundColor = .red
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(6)
                    make.top.equalToSuperview().offset(13)
                    make.width.height.equalTo(10)
                }
            }
            
            let crmTitleLabel = UILabel().then { obj in
                crmView.addSubview(obj)
                obj.text = "CRM"
                obj.textColor = blackTextColor16
                obj.font = kAMediumFont(12)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(crmHintImageV.snp.right).offset(3)
                    make.centerY.equalTo(crmHintImageV)
                }
            }
            
            let crmContentLabel = UILabel().then { obj in
                crmView.addSubview(obj)
                obj.text = "客户：安高创新技术（深圳）有限公司"
                obj.textColor = blackTextColor17
                obj.font = kAMediumFont(12)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(20)
                    make.right.equalToSuperview().offset(-20)
                    make.top.equalTo(crmTitleLabel.snp.bottom).offset(10)
                }
            }
            
            let lineView = UIView().then { obj in
                bgView.addSubview(obj)
                obj.backgroundColor = lineColor11
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.right.equalToSuperview().offset(-0)
                    make.top.equalTo(crmView.snp.bottom).offset(10)
                    make.height.equalTo(0.5)
                }
            }
            
            let bottomFeatureView = UIView().then { obj in
                bgView.addSubview(obj)
                
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    make.right.equalToSuperview().offset(-10)
                    make.top.equalTo(lineView.snp.bottom).offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                }
            }
            
            let moreView = UIView().then { obj in
                bottomFeatureView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo((kScreenWidth - 20)/4)
                }
            }
            
            let moreImageV = UIImageView().then { obj in
                moreView.addSubview(obj)
                obj.image = UIImage(named: "work_more")
                
                obj.snp.makeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.centerX.equalToSuperview().offset(-15)
                }
            }
            
            let moreLabel = UILabel().then { obj in
                moreView.addSubview(obj)
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor21
                obj.text = "更多"
                obj.snp.makeConstraints { make in
                    make.left.equalTo(moreImageV.snp.right).offset(5)
                    make.centerY.equalTo(moreImageV)
                }
            }
            
            _ = UIButton().then { obj in
                moreView.addSubview(obj)
                obj.addTarget(self, action: #selector(moreClick), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
            
            let forwardView = UIView().then { obj in
                bottomFeatureView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(moreView.snp.right).offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo((kScreenWidth - 20)/4)
                }
            }
            
            let forwardImageV = UIImageView().then { obj in
                forwardView.addSubview(obj)
                obj.image = UIImage(named: "work_forward")
                
                obj.snp.makeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.centerX.equalToSuperview().offset(-15)
                }
            }
            
            let forwardLabel = UILabel().then { obj in
                forwardView.addSubview(obj)
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor21
                obj.text = "转发"
                obj.snp.makeConstraints { make in
                    make.left.equalTo(forwardImageV.snp.right).offset(5)
                    make.centerY.equalTo(forwardImageV)
                }
            }
            
            _ = UIButton().then { obj in
                forwardView.addSubview(obj)
                obj.addTarget(self, action: #selector(forwardClick), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
            
            let goodView = UIView().then { obj in
                bottomFeatureView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(forwardView.snp.right).offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo((kScreenWidth - 20)/4)
                }
            }
            
            let goodImageV = UIImageView().then { obj in
                goodView.addSubview(obj)
                obj.image = UIImage(named: "work_good")
                
                obj.snp.makeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.centerX.equalToSuperview().offset(-15)
                }
            }
            
            let goodLabel = UILabel().then { obj in
                goodView.addSubview(obj)
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor21
                obj.text = "点赞"
                obj.snp.makeConstraints { make in
                    make.left.equalTo(goodImageV.snp.right).offset(5)
                    make.centerY.equalTo(goodImageV)
                }
            }
            
            _ = UIButton().then { obj in
                goodView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
            
            let commitView = UIView().then { obj in
                bottomFeatureView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalTo(goodView.snp.right).offset(0)
                    make.top.equalToSuperview().offset(0)
                    make.bottom.equalToSuperview().offset(-0)
                    make.width.equalTo((kScreenWidth - 20)/4)
                }
            }
            
            let commitImageV = UIImageView().then { obj in
                commitView.addSubview(obj)
                obj.image = UIImage(named: "work_msg")
                
                obj.snp.makeConstraints { make in
                    make.centerY.equalToSuperview()
                    make.centerX.equalToSuperview().offset(-15)
                }
            }
            
            let commitLabel = UILabel().then { obj in
                commitView.addSubview(obj)
                obj.font = kAMediumFont(12)
                obj.textColor = blackTextColor21
                obj.text = "回复"
                obj.snp.makeConstraints { make in
                    make.left.equalTo(commitImageV.snp.right).offset(5)
                    make.centerY.equalTo(commitImageV)
                }
            }
            
            _ = UIButton().then { obj in
                commitView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.bottom.right.equalToSuperview().offset(-0)
                }
            }
        }
    }
    
    @objc func statusClick(){

    }
    
    @objc func moreClick(){
        moreBlock?()
    }
    
    @objc func forwardClick(){
        forwardBlock?()
    }

}
