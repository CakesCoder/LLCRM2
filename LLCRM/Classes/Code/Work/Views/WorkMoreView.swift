//
//  WorkMoreView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/7.
//

import UIKit

class WorkMoreView: UIView {
    var cancelBlock: (() -> ())?
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override init(frame: CGRect) {
        super.init(frame: CGRect(x: 0, y: 0, width: kScreenWidth, height: kScreenHeight))
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        let bgView = UIView().then { obj in
            self.addSubview(obj)
//            obj.backgroundColor = .blue
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = bgColor3
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(215)
            }
        }
        
        UILabel().then { obj in
            bottomView.addSubview(obj)
            obj.textColor = blackTextColor24
            obj.text = "更多操作"
            obj.font = kAMediumFont(13)
            obj.snp.makeConstraints { make in
                make.top.equalToSuperview().offset(7)
                make.centerX.equalToSuperview()
            }
        }
        
        let contentView = UIView().then { obj in
            bottomView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalToSuperview().offset(50)
                make.height.equalTo(90)
            }
        }
        
        let featureView = UIView().then { obj in
            contentView.addSubview(obj)
        }
        
        let images = ["work_collect", "work_foucs", "work_remind", "work_task"]
        let names = ["收藏", "关注", "提醒", "设为任务"]
        
        for i in 0..<images.count{
            let featureView = UIView().then { obj in
                contentView.addSubview(obj)
                let x = i%4
                let y = i/4
                let w = (kScreenWidth-20)/4
//                let h = (kScreenWidth-20)/4 + 10
                obj.frame = CGRect(x: CGFloat(x)*w, y: 0, width: w, height: w - 10)
            }
            
            let imageV = UIImageView().then { obj in
                featureView.addSubview(obj)
                obj.image = UIImage(named: images[i])
                
                obj.snp.makeConstraints { make in
                    make.centerX.equalToSuperview()
                    make.top.equalToSuperview().offset(0)
                }
            }
            
            let nameLabel = UILabel().then { obj in
                featureView.addSubview(obj)
                obj.textColor = blackTextColor16
                obj.font = kAMediumFont(14)
                obj.text = names[i]
                
                obj.snp.makeConstraints { make in
                    make.centerX.equalToSuperview()
                    make.top.equalTo(imageV.snp.bottom).offset(10)
                }
            }
        }
        
        let closeButton = UIButton().then { obj in
            bottomView.addSubview(obj)
            obj.setTitle("取消", for: .normal)
            obj.titleLabel?.font = kARegularFont(18)
            obj.setTitleColor(blackTextColor3, for: .normal)
//            obj.backgroundColor = .red
            obj.addTarget(self, action: #selector(cancelShowClick(_:)), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
                make.height.equalTo(70)
            }
        }
    }
    
    @objc func cancelShowClick(_ button:UIButton){
        self.cancelBlock?()
    }
}
