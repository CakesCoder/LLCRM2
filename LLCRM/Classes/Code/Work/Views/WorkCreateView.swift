//
//  WorkCreateView.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/7.
//

import UIKit
//import SwiftMessages

class WorkCreateView: UIView {
    

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    var featureBlock: ((_ :Int) -> ())?
    var cancelBlock: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        _ = UIView().then { obj in
            self.addSubview(obj)
            obj.alpha = 0.8
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let bottomView = UIView().then { obj in
            self.addSubview(obj)
            obj.backgroundColor = .white
            obj.layer.cornerRadius = 20.0
            obj.layer.maskedCorners = CACornerMask(rawValue: CACornerMask.layerMinXMinYCorner.rawValue | CACornerMask.layerMaxXMinYCorner.rawValue)

            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.bottom.equalToSuperview().offset(-0)
                make.height.equalTo(388)
            }
        }
        
        let bottomContentView = UIView().then { obj in
            bottomView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(20)
                make.bottom.equalToSuperview().offset(-0)
                make.right.equalToSuperview().offset(-20)
            }
        }
        
        let images = ["work_alert_share", "work_alert_log", "work_alert_audit", "work_alert_task", "work_alert_date", "work_alert_command"]
        let names = ["分享", "日志", "审核", "任务", "日程", "指令"]
        
        for i in 0..<images.count{
            let featureView = UIView().then { obj in
                bottomContentView.addSubview(obj)
                let x = i%3
                let y = i/3
                let w = (kScreenWidth-40)/3
                let h = (kScreenWidth-40)/3 + 10
                obj.frame = CGRect(x: CGFloat(x)*w, y: CGFloat(y)*h, width: w, height: w - 10)
            }
            
            let imageV = UIImageView().then { obj in
                featureView.addSubview(obj)
                obj.image = UIImage(named: images[i])
                obj.snp.makeConstraints { make in
                    make.centerX.equalToSuperview()
                    make.top.equalToSuperview().offset(20)
                }
            }
            
            _ = UILabel().then { obj in
                featureView.addSubview(obj)
                obj.textColor = blackTextColor16
                obj.font = kAMediumFont(14)
                obj.text = names[i]
                
                obj.snp.makeConstraints { make in
                    make.centerX.equalToSuperview()
                    make.top.equalTo(imageV.snp.bottom).offset(10)
                }
            }
            
            _ = UIButton().then { obj in
                featureView.addSubview(obj)
                obj.tag = 1000 + i
                obj.addTarget(self, action: #selector(featureClick(_:)), for: .touchUpInside)
                obj.snp.makeConstraints { make in
                    make.left.top.equalToSuperview().offset(0)
                    make.right.bottom.equalToSuperview().offset(-0)
                }
            }
        }
        
        _ = UIButton().then { obj in
            bottomContentView.addSubview(obj)
            
            let image = UIImage(named: "work_alert_close")
            
            obj.setImage(image, for: .normal)
            obj.setImage(image, for: .selected)
            obj.addTarget(self, action: #selector(cancelShowClick(_:)), for: .touchUpInside)
            
            obj.snp.makeConstraints { make in
                make.centerX.equalToSuperview()
                make.width.equalTo(image?.size.width ?? 117)
                make.height.equalTo(image?.size.height ?? 117)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
    }
    
    @objc func featureClick(_ button:UIButton){
        featureBlock?(button.tag)
    }
    
    @objc func cancelShowClick(_ button:UIButton){
        self.cancelBlock?()
    }
}
