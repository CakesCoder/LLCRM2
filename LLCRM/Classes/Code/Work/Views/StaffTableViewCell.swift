//
//  StaffTableViewCell.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/6.
//

import UIKit

class StaffTableViewCell: UITableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        buildUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func buildUI(){
        self.selectionStyle = .none
        
        let bgView = UIView().then { obj in
            contentView.addSubview(obj)
            // shadowCode
            obj.layer.shadowColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            obj.layer.shadowOffset = CGSize(width: 0, height: 0)
            obj.layer.shadowOpacity = 1
            obj.layer.shadowRadius = 6
            // fill
            obj.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            obj.layer.cornerRadius = 8
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(5)
                make.right.equalToSuperview().offset(-5)
                make.top.equalToSuperview().offset(10)
                make.bottom.equalToSuperview().offset(-10)
            }
        }
        
        let imageV = UIImageView().then { obj in
            bgView.addSubview(obj)
//            obj.backgroundColor = .red
            obj.image = UIImage(named: "work_icon")
            obj.layer.cornerRadius = 4
            obj.layer.masksToBounds = true
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(14)
                make.width.height.equalTo(44)
            }
        }
        
        let nameLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "等风来"
            obj.textColor = blackTextColor17
            obj.font = kAMediumFont(16)
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.top.equalToSuperview().offset(14)
            }
        }
        
        let subLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.textColor = blackTextColor4
            obj.text = "零瓴团队团队"
            obj.font = kRegularFont(12)
            
            obj.snp.makeConstraints { make in
                make.left.equalTo(nameLabel.snp.right).offset(5)
                make.centerY.equalTo(nameLabel)
            }
        }
        
        let dateLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.textColor = blackTextColor18
            obj.font = kRegularFont(12)
            obj.text = "今天09：35  来自iPhone"
            obj.snp.makeConstraints { make in
                make.left.equalTo(imageV.snp.right).offset(15)
                make.top.equalTo(nameLabel.snp.bottom).offset(5)
            }
        }
        
        let statusLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "已点评"
            obj.textColor = blackTextColor22
            obj.font =  kAMediumFont(10)
            
            obj.snp.makeConstraints { make in
                make.right.equalToSuperview().offset(-20)
                make.top.equalToSuperview().offset(20)
            }
        }
        
        let typeLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "日志"
            obj.textColor = blackTextColor17
            obj.font =  kAMediumFont(10)
            obj.snp.makeConstraints { make in
                make.right.equalTo(statusLabel.snp.left).offset(-10)
                make.centerY.equalTo(statusLabel)
            }
        }
        
        let starImageV = UIImageView().then { obj in
            bgView.addSubview(obj)
            obj.image = UIImage(named: "work_star")
            
            obj.snp.makeConstraints { make in
                make.top.equalTo(typeLabel.snp.bottom).offset(4)
                make.right.equalToSuperview().offset(-20)
            }
        }
        
        let contentLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "2021年10月09日  日计划，由刘经理点评"
            obj.textColor = blackTextColor23
            obj.font =  kAMediumFont(12)
            obj.snp.makeConstraints { make in
                make.top.equalTo(imageV.snp.bottom).offset(4)
                make.right.equalToSuperview().offset(-10)
                make.left.equalToSuperview().offset(10)
            }
        }
        
        let nowHintLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "今日工作总结"
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(contentLabel.snp.bottom).offset(10)
            }
        }
        
        let workContentView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.backgroundColor = blackTextColor19
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(nowHintLabel.snp.bottom).offset(5)
            }
        }
        
        var t_view:UIView? = nil
        for i in 0..<4{
            let detailContentView = UIView().then { obj in
                workContentView.addSubview(obj)
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    if i == 0{
                        make.top.equalToSuperview().offset(10)
                    }else{
                        make.top.equalTo(t_view!.snp.bottom).offset(10)
                    }
                    make.right.equalToSuperview().offset(-10)
                    if i == 3{
                        make.bottom.equalToSuperview().offset(-10)
                    }else{
                        make.height.equalTo(20)
                    }
                }
                t_view = obj
            }
            
            let contentLabel = UILabel().then { obj in
                detailContentView.addSubview(obj)
                obj.textColor = blackTextColor17
                obj.font = kAMediumFont(12)
                obj.text = "1、设计UI界面，及图标绘制"
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(10)
                    make.top.equalToSuperview().offset(2)
                    make.bottom.equalToSuperview().offset(-2)
                }
            }
        }
        
        let todayHintLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "明日工作计划"
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(workContentView.snp.bottom).offset(10)
            }
        }
        
        let todayContentView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.backgroundColor = blackTextColor19
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(todayHintLabel.snp.bottom).offset(5)
            }
        }
        
        let todayContentLabel = UILabel().then { obj in
            todayContentView.addSubview(obj)
            obj.textColor = blackTextColor17
            obj.font = kAMediumFont(12)
            obj.text = "继续设计UI界面，及图标绘制"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(12)
                make.bottom.equalToSuperview().offset(-15)
                make.right.equalToSuperview().offset(-10)
            }
        }
        
        let experienceHintLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "工作心得体会"
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(todayContentView.snp.bottom).offset(10)
            }
        }
        
        let experienceView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.backgroundColor = blackTextColor19
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(experienceHintLabel.snp.bottom).offset(5)
            }
        }
        
        let experienceContentLabel = UILabel().then { obj in
            experienceView.addSubview(obj)
            obj.textColor = blackTextColor17
            obj.font = kAMediumFont(12)
            obj.text = "请输入工作心得"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(12)
                make.bottom.equalToSuperview().offset(-15)
                make.right.equalToSuperview().offset(-10)
            }
        }
        
        let remarkHintLabel = UILabel().then { obj in
            bgView.addSubview(obj)
            obj.text = "点评"
            obj.textColor = blackTextColor16
            obj.font = kAMediumFont(14)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalTo(experienceView.snp.bottom).offset(10)
            }
        }
        
        let remarkView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.layer.cornerRadius = 2
            obj.layer.masksToBounds = true
            obj.backgroundColor = blackTextColor19
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(remarkHintLabel.snp.bottom).offset(5)
            }
        }
        
        let remarkContentLabel = UILabel().then { obj in
            remarkView.addSubview(obj)
            obj.textColor = blackTextColor17
            obj.font = kAMediumFont(12)
            obj.text = "刘经理：工作做完成的很好"
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.top.equalToSuperview().offset(12)
                make.bottom.equalToSuperview().offset(-15)
                make.right.equalToSuperview().offset(-10)
            }
        }
        
        let lineView = UIView().then { obj in
            bgView.addSubview(obj)
            obj.backgroundColor = lineColor11
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.top.equalTo(remarkView.snp.bottom).offset(10)
                make.height.equalTo(0.5)
            }
        }
        
        let bottomFeatureView = UIView().then { obj in
            bgView.addSubview(obj)
            
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(10)
                make.right.equalToSuperview().offset(-10)
                make.top.equalTo(lineView.snp.bottom).offset(0)
                make.bottom.equalToSuperview().offset(-0)
            }
        }
        
        let moreView = UIView().then { obj in
            bottomFeatureView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo((kScreenWidth - 20)/4)
            }
        }
        
        let moreImageV = UIImageView().then { obj in
            moreView.addSubview(obj)
            obj.image = UIImage(named: "work_more")
            
            obj.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.centerX.equalToSuperview().offset(-15)
            }
        }
        
        let moreLabel = UILabel().then { obj in
            moreView.addSubview(obj)
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor21
            obj.text = "更多"
            obj.snp.makeConstraints { make in
                make.left.equalTo(moreImageV.snp.right).offset(5)
                make.centerY.equalTo(moreImageV)
            }
        }
        
        _ = UIButton().then { obj in
            moreView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let forwardView = UIView().then { obj in
            bottomFeatureView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalTo(moreView.snp.right).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo((kScreenWidth - 20)/4)
            }
        }
        
        let forwardImageV = UIImageView().then { obj in
            forwardView.addSubview(obj)
            obj.image = UIImage(named: "work_forward")
            
            obj.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.centerX.equalToSuperview().offset(-15)
            }
        }
        
        let forwardLabel = UILabel().then { obj in
            forwardView.addSubview(obj)
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor21
            obj.text = "转发"
            obj.snp.makeConstraints { make in
                make.left.equalTo(forwardImageV.snp.right).offset(5)
                make.centerY.equalTo(forwardImageV)
            }
        }
        
        _ = UIButton().then { obj in
            forwardView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let goodView = UIView().then { obj in
            bottomFeatureView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalTo(forwardView.snp.right).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo((kScreenWidth - 20)/4)
            }
        }
        
        let goodImageV = UIImageView().then { obj in
            goodView.addSubview(obj)
            obj.image = UIImage(named: "work_good")
            
            obj.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.centerX.equalToSuperview().offset(-15)
            }
        }
        
        let goodLabel = UILabel().then { obj in
            goodView.addSubview(obj)
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor21
            obj.text = "点赞"
            obj.snp.makeConstraints { make in
                make.left.equalTo(goodImageV.snp.right).offset(5)
                make.centerY.equalTo(goodImageV)
            }
        }
        
        _ = UIButton().then { obj in
            goodView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
        let commitView = UIView().then { obj in
            bottomFeatureView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalTo(goodView.snp.right).offset(0)
                make.top.equalToSuperview().offset(0)
                make.bottom.equalToSuperview().offset(-0)
                make.width.equalTo((kScreenWidth - 20)/4)
            }
        }
        
        let commitImageV = UIImageView().then { obj in
            commitView.addSubview(obj)
            obj.image = UIImage(named: "work_msg")
            
            obj.snp.makeConstraints { make in
                make.centerY.equalToSuperview()
                make.centerX.equalToSuperview().offset(-15)
            }
        }
        
        let commitLabel = UILabel().then { obj in
            commitView.addSubview(obj)
            obj.font = kAMediumFont(12)
            obj.textColor = blackTextColor21
            obj.text = "回复"
            obj.snp.makeConstraints { make in
                make.left.equalTo(commitImageV.snp.right).offset(5)
                make.centerY.equalTo(commitImageV)
            }
        }
        
        _ = UIButton().then { obj in
            commitView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.bottom.right.equalToSuperview().offset(-0)
            }
        }
        
    }
}
