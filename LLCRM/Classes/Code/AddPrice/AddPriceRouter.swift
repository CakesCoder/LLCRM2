//
//  AddPriceRouter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class AddPriceRouter: AddPriceRouterProtocols {
    func pushToPriceProduct(from previousView: UIViewController) {
        let nextView = MyProductsListViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToPriceBusiness(from previousView: UIViewController) {
        let nextView = ClientBusinessListViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToPriceLinkMan(from previousView: UIViewController) {
        let nextView = MyLineManListViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
    
    func pushToPriceMyClient(from previousView: UIViewController){
        let nextView = MyClientViewController()
        previousView.navigationController?.pushViewController(nextView, animated: true)
    }
}
