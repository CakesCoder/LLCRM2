//
//  AddPricePresenter.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class AddPricePresenter: AddPricePresenterProtocols {
    var view: AddPriceViewProtocols?
    
    var router: AddPriceRouterProtocols?
    
    var interactor: AddPriceInteractorProtocols?
    
    var params: [Any]?
    
    func viewDidLoad() {
        
    }
    
    func presenterRequestGetAddPrice(by params: Any?) {
        interactor?.presenterRequestAddPrice(by: params)
    }
    
    func didGetAddPriceInteractorReceiveData(by params: Any?) {
        view?.didGetAddPricePresenterReceiveData(by: params)
    }
    
    func didGetAddPriceInteractorReceiveError(error: MyError?) {
        view?.didGetAddPricePresenterReceiveError(error: error)
    }
    
    
}
