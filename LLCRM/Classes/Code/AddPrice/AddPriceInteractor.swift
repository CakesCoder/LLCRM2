//
//  AddPriceInteractor.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class AddPriceInteractor: AddPriceInteractorProtocols {
    var presenter: AddPricePresenterProtocols?
    
    var entity: AddPriceEntityProtocols?
    
    func presenterRequestAddPrice(by params: Any?) {
        entity?.getAddPriceRequest(by: params)
    }
    
    func didEntityAddPriceReceiveData(by params: Any?) {
        presenter?.didGetAddPriceInteractorReceiveData(by: params)
    }
    
    func didEntityAddPriceReceiveError(error: MyError?) {
        presenter?.didGetAddPriceInteractorReceiveError(error: error)
    }
    

}
