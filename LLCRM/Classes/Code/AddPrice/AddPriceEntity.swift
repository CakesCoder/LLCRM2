//
//  AddPriceEntity.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

class AddPriceEntity: AddPriceEntityProtocols {
    var interactor: AddPriceInteractorProtocols?
    
    func didAddPriceReceiveData(by params: Any?) {
        interactor?.didEntityAddPriceReceiveData(by: params)
    }
    
    func didAddPriceReceiveError(error: MyError?) {
        interactor?.didEntityAddPriceReceiveError(error: error)
    }
    
    func getAddPriceRequest(by params: Any?) {
        LLNetProvider.request(.commitPrice(params as! [String : Any])) { result in
        switch result{
            case let .success(response):
                let data = response.data
                let jsonData  = try? JSONSerialization.jsonObject(with: data, options: [])
                self.didAddPriceReceiveData(by:jsonData)
            case .failure(_):
                self.didAddPriceReceiveError(error: .requestError)
            }
        }
    }
    
    
}
