//
//  AddPriceProtocols.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit

protocol AddPriceViewProtocols: AnyObject {
    var presenter: AddPricePresenterProtocols? { get set }
    func showLoading()
    func showError()
    func hideLoading()
    func didGetAddPricePresenterReceiveData(by params: Any?)
    func didGetAddPricePresenterReceiveError(error: MyError?)
}

protocol AddPricePresenterProtocols: AnyObject{
    var view: AddPriceViewProtocols? { get set }
    var router: AddPriceRouterProtocols? { get set }
    var interactor: AddPriceInteractorProtocols? { get set }
    var params: [Any]? { get set }
    
    func viewDidLoad()
    
    func presenterRequestGetAddPrice(by params: Any?)
    func didGetAddPriceInteractorReceiveData(by params: Any?)
    func didGetAddPriceInteractorReceiveError(error: MyError?)
}

protocol AddPriceInteractorProtocols: AnyObject {
    var presenter: AddPricePresenterProtocols? { get set }
    var entity: AddPriceEntityProtocols? { get set }
    
    func presenterRequestAddPrice(by params: Any?)
    func didEntityAddPriceReceiveData(by params: Any?)
    func didEntityAddPriceReceiveError(error: MyError?)
}

protocol AddPriceEntityProtocols: AnyObject {
    var interactor: AddPriceInteractorProtocols? { get set }
    
    func didAddPriceReceiveData(by params: Any?)
    func didAddPriceReceiveError(error: MyError?)
    func getAddPriceRequest(by params: Any?)
}

protocol AddPriceRouterProtocols: AnyObject {
    func pushToPriceProduct(from previousView: UIViewController)
    func pushToPriceBusiness(from previousView: UIViewController)
    func pushToPriceLinkMan(from previousView: UIViewController)
    func pushToPriceMyClient(from previousView: UIViewController)
}
