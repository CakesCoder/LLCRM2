//
//  AddPriceViewController.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/10/30.
//

import UIKit
import ZLPhotoBrowser
import Photos
import PKHUD
//import JXPagingView
//
//extension AddPriceViewController: UIScrollViewDelegate {
//    public func scrollViewDidScroll(_ scrollView: UIScrollView) {
//        self.listViewDidScrollCallback?(scrollView)
//    }
//}
//
//extension AddPriceViewController: JXPagingViewListViewDelegate {
//    public func listView() -> UIView {
//        return self.view
//    }
//
//    public func listViewDidScrollCallback(callback: @escaping (UIScrollView) -> ()) {
//        self.listViewDidScrollCallback = callback
//    }
//
//    public func listScrollView() -> UIScrollView {
//        return self.pagingScrollView!
//    }
//}


class AddPriceViewController: BaseViewController {
    
//    var listViewDidScrollCallback: ((UIScrollView) -> ())?
//    var pagingScrollView:UIScrollView? = UIScrollView()
    var presenter: AddPricePresenterProtocols?
    
    var priceNameStr:String? = ""
    var priceCodeStr:String? = ""
    var priceSourceStr:String? = ""
    var priceBusinessStr:String? = ""
    var priceClientStr:String? = ""
    var priceMoneyStr:String? = ""
    var priceTaxrateStr:String? = ""
    var priceTaxrateMoneyStr:String? = ""
    var priceLinkManStr:String? = ""
    var priceLinkManPhoneStr:String? = ""
    var AddProduct:String? = ""
    
    var pictureContent:UIView?
    var addButton:UIButton?
    var images:[Any]? = []
    
    var bussinessTextFiled:UITextField?
    var clientTextFiled:UITextField?
    var linkManTextFiled:UITextField?
    var productButton:UIButton?
    var rateTextFiled:UITextField?
    var clientModel:MyClientModelData?
    var businessModel:MyClientModelData?
    var linkManModel:MyClientModelData?
    var productModel:MyClientModelData?
    
    var sourceModel:AddClientSelectedModel?
    var archivesModel:AddClientSelecteArchivesModel?
    var linkManModel2:AddClientVisitAddPersonModel?
    var rateModel:AddClientSelectedModel?
    
    
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.whiteNavTheme()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "添加报价单"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem.createBarbuttonItem(name: "nav_back", target: self, action: #selector(AddPriceViewController.blackClick))
        
        _ = UIView().then { obj in
            view.addSubview(obj)
            obj.backgroundColor = bgColor
            obj.snp.makeConstraints { make in
                make.left.top.equalToSuperview().offset(0)
                make.right.equalToSuperview().offset(-0)
                make.height.equalTo(10)
            }
        }
        
        let headView = UIView().then { obj in
            obj.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight)
        }
        
        let baseInfos = [["*","报价名称："],["报价单号："],["*","来源商机："],["*","来源客户："],["*","报价金额："],["*","税率: "],["含税价格："],["联系人："],["联系人手机："],["添加产品："] ]

        
        let contentInfos = ["请输入报价名称", "请输入", "请选择来源商机", "请选择", "请输入报价金额（元）", "请选择", "请输入产品总金额（元）", "请选择联系人", "请输入联系人手机", "添加"]
        
        for i in 0..<baseInfos.count{
            let v = UIView().then { obj in
                headView.addSubview(obj)
                obj.backgroundColor = .white
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(0)
                    make.top.equalToSuperview().offset(i*45)
                    make.right.equalToSuperview().offset(-0)
                    make.height.equalTo(45)
                }
            }
            
            let hintLabel = UILabel().then { obj in
                v.addSubview(obj)
                obj.isUserInteractionEnabled = true
                var temp_hint = NSMutableAttributedString(string: "")
                for j in 0..<baseInfos[i].count{
                    let hintAttribute = NSMutableAttributedString(string:baseInfos[i][j])
                    if baseInfos[i].count == 1{
                        hintAttribute.yy_color = blackTextColor3
                        hintAttribute.yy_font = kMediumFont(12)
                        obj.attributedText = hintAttribute
                    }else{
                        if j == 0{
                            hintAttribute.yy_color = blackTextColor27
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint = hintAttribute
                        }else{
                            hintAttribute.yy_color = blackTextColor3
                            hintAttribute.yy_font = kMediumFont(12)
                            temp_hint.append(hintAttribute)
                            obj.attributedText = temp_hint
                        }
                        
                    }
                    
                }
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.centerY.equalToSuperview()
                }
            }
            
            if i == 2 || i == 3 || i == 5 || i == 7{
                let tagImageV = UIImageView().then { obj in
                    v.addSubview(obj)
                    obj.image = UIImage(named: "me_push")
                    obj.isUserInteractionEnabled = true
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
            }
            
            if i == contentInfos.count - 1{
                productButton = UIButton().then { obj in
                    v.addSubview(obj)
                    obj.titleLabel?.font = kMediumFont(14)
                    obj.setTitleColor(bluebgColor, for: .normal)
                    obj.setTitle("添加", for: .normal)
                    obj.addTarget(self, action: #selector(productClick), for: .touchUpInside)
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-10)
                        make.centerY.equalToSuperview()
                    }
                }
            }else{
                let textFiled = UITextField().then { obj in
                    v.addSubview(obj)
                    obj.placeholder = contentInfos[i]
                    obj.font = kMediumFont(12)
                    obj.tag = 1000+i
                    obj.delegate = self
                    obj.textAlignment = .right
                    obj.isUserInteractionEnabled = true
                    if i == 2 || i == 3 || i == 5 || i == 7{
                        obj.isEnabled = false
                        if i == 2{
                            bussinessTextFiled = obj
                        }else if i == 3{
                            clientTextFiled = obj
                        }else if i == 5{
                            rateTextFiled = obj
                        }else{
                            linkManTextFiled = obj
                        }
                    }
                    obj.snp.makeConstraints { make in
                        make.right.equalToSuperview().offset(-35)
                        make.centerY.equalToSuperview()
                    }
                }
                
                if i == 2 || i == 3 || i == 5 || i == 7{
                    _ = UIButton().then { obj in
                        v.addSubview(obj)
                        obj.tag = 1000+i
                        obj.addTarget(self, action: #selector(addPersonClick(_ :)), for: .touchUpInside)
                        obj.snp.makeConstraints { make in
                            make.left.top.equalToSuperview().offset(0)
                            make.right.bottom.equalToSuperview().offset(-0)
                        }
                    }
                }
            }

            _ = UIView().then({ obj in
                v.addSubview(obj)
                obj.backgroundColor = lineColor5
                obj.snp.makeConstraints { make in
                    make.left.equalToSuperview().offset(15)
                    make.right.equalToSuperview().offset(-15)
                    make.bottom.equalToSuperview().offset(-0)
                    make.height.equalTo(0.5)
                }
            })
        }
        
        let clientPictureHintLabel = UILabel().then { obj in
            headView.addSubview(obj)
            let clientPictureHintAttribute = NSMutableAttributedString(string: "")
            clientPictureHintAttribute.yy_color = blackTextColor27
            clientPictureHintAttribute.yy_font = kMediumFont(13)
            let clientPictureCotentAttribute = NSMutableAttributedString(string: "上传附件")
            clientPictureCotentAttribute.yy_color = blackTextColor
            clientPictureCotentAttribute.yy_font = kMediumFont(14)
            clientPictureHintAttribute.append(clientPictureCotentAttribute)
            obj.attributedText = clientPictureHintAttribute
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.top.equalToSuperview().offset(baseInfos.count * 45 + 15)
            }
        }
        
        let w = (kScreenWidth-50)/3
        pictureContent = UIView().then { obj in
            headView.addSubview(obj)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(25)
                make.right.equalToSuperview().offset(-25)
                make.top.equalTo(clientPictureHintLabel.snp.bottom).offset(10)
                make.height.equalTo(w)
            }
        }
        
        addButton = UIButton().then({ obj in
            pictureContent?.addSubview(obj)
            obj.frame = CGRectMake(0, 0, w, w)
            obj.layer.borderColor = lineColor23.cgColor
            obj.layer.borderWidth = 0.5
            obj.setTitle("+", for: .normal)
            obj.setTitleColor(.black, for: .normal)
            obj.titleLabel?.font = kMediumFont(25)
            obj.tag = 2000
            obj.addTarget(self, action: #selector(addPhotoClick), for: .touchUpInside)
        })
        
//        let hintLabel = UILabel().then { obj in
//            pictureView.addSubview(obj)
//            let hintAttribute = NSMutableAttributedString(string:"附件上传")
//            hintAttribute.yy_color = blackTextColor3
//            hintAttribute.yy_font = kMediumFont(12)
//            obj.attributedText = hintAttribute
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(15)
//                make.top.equalToSuperview().offset(10)
//            }
//        }
//        
//        let w = (kScreenWidth-50)/3
//        pictureContent = UIView().then { obj in
//            pictureView.addSubview(obj)
////            obj.backgroundColor = .blue
//            obj.snp.makeConstraints { make in
//                make.left.equalToSuperview().offset(25)
//                make.right.equalToSuperview().offset(-25)
//                make.top.equalTo(hintLabel.snp.bottom).offset(10)
//                make.height.equalTo(w)
//            }
//        }
//        
//        addButton = UIButton().then({ obj in
//            pictureContent?.addSubview(obj)
//            obj.frame = CGRectMake(0, 0, w, w)
//            obj.layer.borderColor = lineColor23.cgColor
//            obj.layer.borderWidth = 0.5
//            obj.setTitle("+", for: .normal)
//            obj.titleLabel?.font = kMediumFont(25)
//            obj.addTarget(self, action: #selector(addClick), for: .touchUpInside)
//        })
        
        
        _ = UIButton().then({ obj in
            headView.addSubview(obj)
            obj.setTitle("保存", for: .normal)
            obj.layer.cornerRadius = 4
            obj.setTitleColor(.white, for: .normal)
            obj.backgroundColor = bluebgColor
            obj.titleLabel?.font = kMediumFont(12)
            obj.addTarget(self, action: #selector(saveClick), for: .touchUpInside)
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(15)
                make.right.equalToSuperview().offset(-15)
                make.top.equalTo(pictureContent!.snp.bottom).offset(15)
                make.height.equalTo(40)
            }
        })
        
        
        let tableView = UITableView().then({ obj in
            view.addSubview(obj)
            obj.tableHeaderView = headView
            obj.snp.makeConstraints { make in
                make.left.equalToSuperview().offset(0)
                make.top.equalToSuperview().offset(UIDevice.xp_navigationFullHeight() + 10)
                make.right.equalToSuperview().offset(-0)
                make.bottom.equalToSuperview().offset(-0)
            }
        })
        
        cofing()
        
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedClientBusinessAction(_:)), name: Notification.Name(rawValue: "BusinessAdddNotificationKey"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedClientLinkManAction(_:)), name: Notification.Name(rawValue: "LinkManAddNotificationKey"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedClientProductAction(_:)), name: Notification.Name(rawValue: "ProductAddNotificationKey"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(didSelectedClientAction(_:)), name: Notification.Name(rawValue: "VisitAddNotificationKey"), object: nil)
    }
    
    @objc func didSelectedClientBusinessAction(_ notification:NSNotification){
        let dict = notification.userInfo as? [String:Any]
        let model = dict!["model"] as? MyClientModelData
        businessModel = model
        bussinessTextFiled?.text = model?.clientName
    }
    
    @objc func didSelectedClientLinkManAction(_ notification:NSNotification){
        let dict = notification.userInfo as? [String:Any]
        let model = dict!["model"] as? MyClientModelData
        linkManModel = model
        linkManTextFiled?.text = model?.clientName
    }
    
    @objc func didSelectedClientProductAction(_ notification:NSNotification){
        let dict = notification.userInfo as? [String:Any]
        let model = dict!["model"] as? MyClientModelData
        productModel = model
        productButton?.setTitle(productModel?.clientName, for: .normal)
    }
    
    @objc func didSelectedClientAction(_ notification:NSNotification){
        let dict = notification.userInfo as? [String:Any]
        let model = dict!["model"] as? MyClientModelData
        clientModel = model
        clientTextFiled?.text = clientModel?.clientName
    }
    
    
    func cofing(){
        let router = AddPriceRouter()
        
        presenter = AddPricePresenter()
        
        presenter?.router = router
        
        let entity = AddPriceEntity()
        
        let interactor = AddPriceInteractor()
        interactor.entity = entity
        interactor.presenter = presenter
        
        entity.interactor = interactor
        
        presenter?.interactor = interactor
        presenter?.view = self
 
    }
    
    @objc private func blackClick(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func addPersonClick(_ button:UIButton){
        if button.tag == 1002{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 2
            vc.addClientDataBlock = {[self] model in
                sourceModel = model
                bussinessTextFiled?.text =  model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1003{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 6
            vc.addClientSelectedArchivesBlock = {[self] model in
                archivesModel = model
                clientTextFiled?.text =  model.clientName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1005{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 7
            vc.addClientDataBlock = {[self] model in
                rateModel = model
                rateTextFiled?.text =  model.attrName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }else if button.tag == 1007{
            let vc = AddClientSelectedViewController()
            vc.clientSelectConfig = 8
            vc.addClientVisitAddPersonBlock = {[self] model in
                linkManModel2 = model
                linkManTextFiled?.text =  model.companyName
            }
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @objc func saveClick(){
        presenter?.presenterRequestGetAddPrice(by: ["clientName":priceNameStr, "quotationNo":priceCodeStr, "price": priceMoneyStr, "taxRate":rateModel?.attrName, "contactName":linkManModel2?.companyName, "contactId":linkManModel2?.companyId, "productName":productModel?.clientName, "productNo":productModel?.clientId, "productModel":""])
    }
    
    @objc func productClick(){
        presenter?.router?.pushToPriceProduct(from: self)
        
    }
    
    @objc func addPhotoClick(){
        let ps = ZLPhotoPreviewSheet()
        ps.selectImageBlock = { [weak self] results, isOriginal in
            // your code
            if (self?.images!.count)! > 1{
                self?.addButton?.isHidden = true
            }else{
                let w = (kScreenWidth-50)/3
                for i in 0..<results.count{
                    if self?.images?.count ?? 0 >= 3{
                        self?.addButton?.isHidden = true
                        break
                    }
                    if i == 0{
                        for view in self?.pictureContent?.subviews as! [UIView]{
                            if view is UIButton{
                                continue
                            }
                            view.removeFromSuperview()
                        }
                    }
                    self?.images?.append(results[i].image)
                    self?.addButton?.isHidden = false
                    if i == 2{
                        self?.addButton?.isHidden = true
                    }else{
                        self?.addButton?.frame = CGRect(x: CGFloat(w)*CGFloat(i+1), y: 0, width: w, height: w)
                        self?.addButton?.isHidden = false
                    }
                    
                    _ = UIImageView().then { obj in
                        self?.pictureContent?.addSubview(obj)
                        obj.image = (self?.images![i] as! UIImage)
                        obj.frame = CGRect(x: CGFloat(w)*CGFloat(i), y: 0, width: w - 5, height: w)
                    }
                }
            }
        }
        ps.showPreview(animate: true, sender: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AddPriceViewController: UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField.tag == 1000{
            priceNameStr = textField.text
        }else if textField.tag == 1001{
            priceCodeStr = textField.text
        }else if textField.tag == 1002{
            
        }else if textField.tag == 1003{
            
        }else if textField.tag == 1004{
            priceMoneyStr = textField.text
        }else if textField.tag == 1005{
            priceTaxrateStr = textField.text
        }else if textField.tag == 1006{
            priceTaxrateMoneyStr = textField.text
        }else if textField.tag == 1007{
            
        }else if textField.tag == 1008{
            priceLinkManPhoneStr = textField.text
        }else if textField.tag == 1009{
            
        }
    }
}

extension AddPriceViewController: AddPriceViewProtocols{
    func showLoading() {
        HUD.show(.progress)
    }
    
    func showError() {
        HUD.flash(.labeledError(title: "请求失败", subtitle: "网络错误，请求检查网络"), delay: 2)
    }
    
    func hideLoading() {
        PKHUD.sharedHUD.hide(true)
    }

    
    func didGetAddPricePresenterReceiveData(by params: Any?) {
        printTest(params)
        if let dict = params as? [String:Any]{
            if dict["code"] as! Int == 200{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "PriceListNotificationKey"), object: nil, userInfo: nil)
                self.navigationController?.popViewController(animated: true)
            }else{
                HUD.flash(.label(dict["message"] as? String), delay: 2)
            }
        }else{
            HUD.flash(.label("data为nil"), delay: 2)
        }
    }
    
    func didGetAddPricePresenterReceiveError(error: MyError?) {
        
    }
}
