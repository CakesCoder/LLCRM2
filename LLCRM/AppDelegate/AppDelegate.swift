//
//  AppDelegate.swift
//  LLCRM
//
//  Created by 阿炮 on 2023/9/10.
//

import UIKit
import IQKeyboardManagerSwift


@available(iOS 15.0, *)
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        window = UIWindow(frame: UIScreen.main.bounds)
        
//        LLUserDefaultsManager.shared.clearCachedUserInfo()
        window?.rootViewController = NavigationRouters.createTabBarVC()
        
        window?.makeKeyAndVisible()
        
        return true
    }

}

